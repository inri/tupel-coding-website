---
layout: page
title: Kontakt
permalink: /contact/
headline: Kontakt
description: "Information about how to get in Contact with me."
tags: [TupelCoding]
---

Ihr habt verschiedene Möglichkeiten mich zu erreichen.

Schreibt mir eine kurze Nachricht
{:.no_toc}

<div class="contact-form">
	<form id="contact" name="contact" action="#" method="post">
	    <label for="email">Deine Email:</label>
	    <input type="email" id="email" name="email" class="txt">
	    <label for="msg">Deine Nachricht:</label>
	    <textarea id="msg" name="msg" class="txtarea"></textarea>
	    <button id="send" class="btn btn-inverse" style="margin-top:1em;margin-bottom:0">Nachricht senden</button>
	</form>
</div>

#### Twitter

Ihr könnt mich auch gerne auf [Twitter](http://twitter.com/ingosleben) anschreiben. 