---
layout: page
permalink: /about/
title: About
tags: [TupelCoding, Persönlich]
modified: 2016-02-19
---
        
Mein Name ist Ingo und meine Website nennt sich *Tupel Coding*.
Ich bin 27 Jahre alt und Absolvent des Hasso-Plattner-Instituts in Potsdam.    
Der Name *Tupel Coding* bedeutet nichts im Speziellen. Meine Freundin mag den Begriff *Tupel* und da ich Softwareentwickler bin, passt das *Coding* ganz gut dazu. Vielleicht denke ich mir später noch eine tolle Geschichte dazu aus und lasse den Namen schützen.

## Was gibt es hier?

Ich schreibe eigentlich kaum über das Programmieren, das können andere viel besser. Ich diskutiere lieber Themen rund um die IT-Welt, mit dem Fokus auf Apple. Aber es bleibt noch genug Zeit für andere Gebiete und Interessen, deshalb: Keine Angst vor Informatikthemen hier.  Die verschiedenen Themen sind in vier Kategorien eingeteilt.      

Wenn Ihr mich kontaktieren möchtet, geht einfach auf [Kontakt]({{ site.url }}/contact/)

## Design und Layout

Diese Website basiert auf das [So Simple Theme](http://mmistakes.github.io/so-simple-theme/) von Michael Rose.   
[In dieser Übersicht](https://github.com/mojombo/jekyll/wiki/Sites) finden sich einige hundert Seiten und Blogs, die mit Jekyll erstellt wurden. 

## Lizenz

Der Inhalt dieser Website (Texte und Fotos) steht unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Unported Lizenz</a>.

Das bedeutet: Ihr könnt meine Texte und Fotos weitergeben und verändern, solange ihr auf mich als Urheber verweist und es nicht im kommerziellen Rahmen macht. Aber falls ihr an einer kommerziellen Nutzung interessiert seid, [kontaktiert mich]({{ site.url }}/contact/). Ich werde mit großer Wahrscheinlichkeit nicht ablehnen.

[Das sagt Wikipedia zu Creative Commons.](http://de.wikipedia.org/wiki/Creative_Commons#Die_sechs_aktuellen_Lizenzen)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png" /></a>