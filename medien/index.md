---
layout: page
title: Über Filme, Serien, Musik
excerpt: "Ein Archiv meiner Blog-Artikel mit dem Fokus auf Filme, Serien und auch Musik"
search_omit: true
---

<ul class="post-list">
{% for post in site.categories.medien %} 
  <li><article><a href="{{ site.url }}{{ post.url }}">{{ post.title }} <span class="entry-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%d. %B %Y" }}</time></span>{% if post.excerpt %} <span class="excerpt">{{ post.excerpt | remove: '\[ ... \]' | remove: '\( ... \)' | markdownify | strip_html | strip_newlines | escape_once }}</span>{% endif %}</a></article></li>
{% endfor %}
</ul>