Tupel Coding
=============

This is my Jekyll webiste, based on the great [so-simple-theme of mmistakes](https://github.com/mmistakes/so-simple-theme).

I write about stuff like mobile systems and devices (Android, iOS, Apple, Google,...), films, tv series, coding and in general about everything I like to write about.

[Check out the webiste here!](http://ingorichter.org)
