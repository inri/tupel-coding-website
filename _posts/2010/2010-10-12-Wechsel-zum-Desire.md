---
layout: post
title: "Wechsel vom HD2 zum Desire"
description: "Wie ich vom HTC HD2 mit Windows Mobile zum HTC Desire mit Android wechselte."
date: 2010-10-12
tags: [Phone]
published: true
category: it
comments: false
share: true
image: 
  feature: 2010-htc_hd_2.jpg
  credit: HTC
  creditlink: http://htc.com
---

Ich wechselte von Nokia zu HTC und bleibe dabei. 


Angefangen hat es mit dem HTC HD2 vor knapp einem Jahr. Ich wollte gerne ein richtige Smartphone haben und das HD2 war vor einem Jahr eben das beste Gerät auf dem Markt. Dummerweise hatte es Windows Mobile 6 als Betriebssystem und das ist einfach nur Mist. Verglichen mit iOS und Android ist es sogar richtig nutzloser Mist. Kaum Programme, nein sogar schlechte Programme und miese Performance.

Ich betete zum Himmel dass möglichst in kurzer Zeit ein Android-Mod für das HD2 gebastelt wird, aber das gestaltete sich anscheinend nicht so einfach und so zog es sich immer wieder um Monate hin. Vor knapp einem Jahr zeichnete sich ab, dass Android ein richtig dickes Ding werden wird. Die ersten Handys mit diesem Betriebssystem räumten gute Bewertungen ab und es wurden noch mehr Android Phones angekündigt.   
Hochinteressant war und ist in diesem Zeitraum das HTC Desire.

Es war leistungsfähiger als das HD2 und zum Glück auch kleiner. Mir machte die stattliche Größe zwar nicht so viel aus, aber mittlerweile bin ich mit dem kleineren Desire sehr zufrieden. Neben Leistung, Android und Größe, hat das Desire auch noch einen verdammt guten AMOLED Bildschirm, den sie in den neusten Modellen des Desires gar nicht mehr verbauen (vermutlich zu teuer in der Produktion). In diesem Zusammenhang fällt mir auch wieder die sehr fragwürdige Bestenliste der Zeitschrift Chip ein. Da rangieren stets Nokia Handys ganz oben. Darüber diskutiere ich demnächst mal.

Mein Papa, der auch schon sehr mit dem HD2 liebäugelte, schwenkte dann zum Desire um und fand das richtig gut. Ich sammelte in der Zwischenzeit auch Erfahrung mit Android und kaufte mir ein HTC Magic. Das Betriebssystem überzeugte mich sofort, vor allem die großen Möglichkeiten und das Potential, das in ihm steckt. Als ich dann noch mit Papas Desire rumspielte, war meine Entscheidung klar.      
Das HD2 landete zusammen mit dem Magic auf Ebay und als nächstes kam ein Desire ins Haus! Eine sehr gute Entscheidung, wie sich bald herausstellen sollte.