---
layout: post
title: "Zehn Tage und ein iPad (Teil 2)"
description: ""
date: 2010-11-22
tags: [iPad] 
published: true
category: it
comments: false
share: true
---

Nachdem der erste Eindruck von der Bedienung positiv war, geht es nun mit den Apps weiter.

Die Fotos anzuschauen macht mit dem iPad riesig Spaß. Der Bildschirm hat wirklich klasse Farben und gibt die Fotos sehr gut wieder. Das Zoomen in die Bilder geht flüssig vonstatten und auch das Drehen reagiert sehr zügig.

Die iPod App wurde von Apple ebenfalls solide umgesetzt. Musik hören eben. Sehr dankbar bin ich, dass ich im Hintergrund meine Musik weiter hören kann, auch wenn ich im Internet surfe oder etwas spiele (wie viel mehr Multitasking braucht man noch?).     
E-Mail und Kalender lassen sich superleicht direkt vom Mac synchronisieren. Da ich auf meinem PC, Mac, Android Phone und nun dem iPad stets Google via IMAP verwende, habe ich überall die Mails verfügbar und sie werden z.b. als gelesen oder Entwurf markiert. Einzig beim Kalender suche ich noch nach der Möglichkeit meinen Uni-Kalender und weitere einzubinden. Aber noch ist alles neu und noch ist es nicht dringend.

Anderen Kram, den man so braucht, zieht man am besten über iTunes auf den Mac. Bilder zu Bilder, Musik zu Musik. Macht man sich die Mühe und konvertiert seine PDFs ins EPUB Format, dann kann man auch diese zu iBooks schieben. Oder aber man nutzt Programme, die auch PDFs öffnen können. Unter dem Apps-Menü wählt man dann die Dateien aus, die diesem und jenen Programm zugeordnet werden. Zum Beispiel diverse Videoarten werden dem VLC Player zugeordnet, wenn man es denn möchte.   

Auf dem iPad hat dann auch wirklich nur dieses eine Programm Zugriff auf diese Dateien. Klingt vielleicht etwas umständlich, aber hat man einmal ein gutes App gefunden, bleibt man dabei. Die Anordnung der Apps auf den Seiten kann man entweder via iTunes tätigen oder direkt auf dem iPad. Wobei ich dabei noch nicht herausgefunden habe, wie man ein App auf eine andere Seite schieben kann. Aber in der Realität macht man das sowieso nicht so oft.

Für die Videos nutze ich VLC. Serien (ca. 500 MB) spielt er zügig ab, auch mit Untertitel. Normalgroße Filme sollten auch funktionieren und an 720p und 1080p Filme probiere ich mich noch.    
Ich habe iBook ausprobiert und die Anordnung einer Buchseite ist echt gut und schön lesbar. Die Schrift ist eben entsprechend groß, wie es sein soll. Liest man nur aus einem PDF (z.b. über den GoodReader), ist die Schrift leider für A4 dimensioniert und man kann es schwer lesen oder muss jede Seite zoomen. Allerdings geht dies zügig. iBooks muss irgendwie immer die Bücher erst laden und das dauert dann schon mal einige Minuten. Ich teste das mal weiter aus bei Gelegenheit.