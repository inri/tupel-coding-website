---
layout: post
title: "weTab vs. iPad"
description: "Ein kurzer Vergleich zwischen dem iPad und dem weTab."
date: 2010-05-05
tags: [iPad, Tablet] 
published: true
category: it
comments: false
share: true
image: 
  feature: 2010-wetab-lg.jpg
  credit: WeTab GmbH
  creditlink: http://s361213037.online.de
---

Man soll diese Geräte eigentlich nicht vergleichen, das weTab distanziert sich extra von dem Namen des iPad-Killers (aus wePad wurde weTab). Andererseits stellt sich dann die Frage: Wieso standen eine gewisse zeitlang die technischen Eckpunkte auf der weTab-Seite im Vergleich zum iPad? 


Letztlich sieht es doch so aus: Es sind die ersten Tablets auf dem Markt, zumindest in dieser Größe und der Preisklasse.   

Ich verfolge die Entwicklung seit einem halben Jahr sehr gespannt und habe unzählige Testberichte von beiden Geräten gelesen. Leider besitze ich keines, aber zumindest das iPad habe ich in diversen Elektronikgeschäften öfter befingert. Ich bewerte also aus der Sicht eines sich informierenden Kunden.

### Hardware

Die Eckdaten geben schon mal einen netten Überblick. Das weTab ist ein Angeber, deshalb wurde anfangs von Seiten der Neofonie (Firma hinter dem weTab) mit dem iPad verglichen. Erst einmal hört sich die Technik ja auch sehr gut an, denn der Bildschirm des weTab ist größer, es hat vier mal so viel RAM, der Prozessor ist flinker und USB/SD-Kartenslot sind auch dabei. Würde es nicht auf Design und das Betriebssystem (OS) ankommen, wäre die Entscheidung natürlich einfach. Doch abgesehen von diesen Punkten, gibt es durchaus gute Gründe, wieso man sich bei Apple für dieses und jenes entschieden hat. 
 
Bei mobilen Geräten kommt es eben nicht drauf an, ob der Prozessor 300MHz schneller taktet, sondern in erster Linie wie lange der Prozessor überhaupt takten kann, also die Laufzeit des Akkus. Wer will sein Tablett schon gerne alle 3 Stunden aufladen? Um auf eine lange Akkulaufzeit zu kommen, muss das Gerät optimiert werden. Nun flüstert mir ein Gefühl, dass eine große Firma wie Apple bestimmt nicht auf gut Glück ihre Geräte zusammenschraubt. Da wird garantiert jede Kleinigkeit getestet. Bei einigen dieser Tests kam anscheinend heraus, dass man ohne USB und SD-Slot eine halbe Stunde längere Laufzeit erreichen kann. Vielleicht auch mehr oder weniger, was weiß ich.

Daneben machte man sich Gedanken darüber was der Benutzer braucht und was nicht. Ich weiß, die ersten schreien wieder nach der Entmündigung des Benutzers, aber bitte, welche Firma macht das nicht so oder so ähnlich? Apple macht dies eben nur sehr gründlich und gut. Man grübelte in Kalifornien vielleicht über den Sinn eines Tablets, was kann es machen, was soll es machen und was will der Benutzer damit machen.  
So kam man zu dem Schluss, dass auch ein 1GB RAM übertrieben ist und viel zu viel Strom frisst. Lieber weniger einbauen und dafür die Programme optimieren. 

Wozu USB- und SD-Slots? Geräte kann man mittlerweile wireless verbinden, das ist kein Problem mehr. Foto, etc. kann man entweder via Mac übertragen oder einen Adapter kaufen. Das ist das einzige, was nach Geldschneiderei riecht, aber andererseits, wie oft muss man denn SD Karten in ein iPad stecken? Bei maximal 64GB internen Speicher? Das iPad soll kein Datengrab sein.

Ich finde z.b. auch 9,7 Zoll als Größe für den Bildschirm okay und ausreichend. Je größer es wird, desto schwerer wird es auch. Das weTab wiegt locker 300 Gramm mehr als das iPad und viele beschweren sich schon, dass man dieses oft ablegen muss. Dafür ist sicherlich auch der Bildschirm verantwortlich.  

Für die sichtlichen Schwächen des iPads, auf die jeder zweitklassige Technik-Blog zeigt, gibt es meines Erachtens relativ nachvollziehbare Gründe. Man darf auch nicht unter den Tisch fallen lassen, dass das weTab ein halbes Jahr später auf den Markt gekommen ist. Wer weiß was Apple in sechs Monaten vorstellt?  Dann ist das weTab vielleicht softwareseitig endlich zu gebrauchen, aber Apple wird ein neues Tablett vorstellen, welches leistungsstärker als das iPad ist und das weTab in der Hinsicht auch degradiert.

Es offenbarten sich auf Seiten des weTab aber auch einige Schwächen. Der Bildschirm von Apple hat knackigere Farben, einen sehr weiten Blickwinkel und eine fettabweisende Schicht. Da sieht man die Fingerabdrücke zwar auch, aber das weTab schmiert viel schneller zu. Das iPad ist leichter und sieht besser aus. Design ist eben Design.  
Dank des starken Prozessors, braucht das weTab einen Lüfter, der zwar leise sein kann, aber wohl öfter ordentlich los röhrt. Auch wird das weTab ganz schön warm. Das iPad ist &quot;cool&quot;. Und man hat dank der fehlenden Anschlüsse und nicht übertriebenen Hardware auch locker sieben bis neun Stunden Spaß damit. Das weTab schafft gerade einmal halb so viel, was allerdings kein großes Wunder ist.

### Betriebssystem

Im ersten Teil wurden die Äußer- und Innerlichkeiten der Tablets verglichen. Nun gehen wir noch ein Stück tiefer und schauen uns die Betriebssysteme an. Apple hat sein iPod/iPhone OS erweitert und ein iOS daraus gemacht. Wie bei allem vom Apple, ist auch hier die Software sehr genau und sehr gut auf die Hardware angepasst. Das Ergebnis ist ein schnelles System, welches zügig auf die Fingerberührung des Nutzers reagiert und auch die Programme flink starten lässt.

Das weTab hat eine lange Vorgeschichte. Zuerst vermutete man ein angepasstes Android, was aber später &quot;nur&quot; eine Linux-Bastard wurde (obwohl Android auch auf Linux basiert). Die Entwickler hielten sich lange Zeit bedeckt. Linux sei wohl die Grundlage des System und es wird an etwas Neuem gearbeitet. Wenn ich mir jetzt so die Software des weTab anschaue, bin ich mir nicht sicher, ob die Entwickler vor einem halben Jahr wussten, was sie entwickeln sollen.  

Letztlich herausgekommen ist der Linux-Bastard von Nokia: Meego. Lieber Gott, wer braucht noch ein mobiles System? Apple und Android tragen diesen Kampf doch alleine aus. Meego ist die Grundfläche und darauf hat die Nebenfirma von Neofonie (Teeto?) eine Oberfläche gebastelt. Die Idee und das Konzept sind vielleicht nicht schlecht, aber die Umsetzung ist grausig.  

Viele Programme funktionieren nicht oder nur fehlerhaft. Der Email-Client ist nicht zu gebrauchen (kann nicht mit Gmail eingerichtet werden). Genauso auch der Kalender. Fotos anschauen war anfangs auch ganz schlimm und soll mittlerweile wohl besser gehen. Multitouch gibt es auch nur bei den Fotos. Das Drehen des Bildschirm klappt nicht so recht. Es gibt Aussetzer, es gibt Verzweiflung und vermutlich auch Tränen.

Es ist ja ganz toll für die Nerds, dass sie ein Tablet bekommen, dass &quot;frei&quot; ist, aber wenn es nicht funktioniert, ist es nutzlos. Laut vielen Kritiken kann es einfach nichts besonderes. Nichts, was nicht auch ein Netbook kann. Die Oberfläche vieler Programme ist einfach noch nicht auf Fingerberührung angepasst. Apps gibt es kaum welche, ein Developer Kit wurde auch nicht veröffentlicht. Der Android Market sollte implementiert werden, fehlt nun aber auch. Die Aufzählung ließe sich eine ganze Weile weiterführen.  
Auf den Punkt gebracht kann man sagen: Neofonie hat einen Prototypen auf den Markt gebracht. Eine Beta, wenn nicht sogar Alpha Version.

### Fazit

Für die Kunden des weTabs ist das natürlich ein Desaster. Nun laufen die zwei Wochen Rückgabefrist ab und es wird bestimmt eine heftige Welle von Rücksendungen geben. Ich bin erstaunt wie unsere eigentlich so kritischen Technikmedien dem weTab großes Potential angerechnet haben. Dagegen beim iPad war die Skepsis wesentlich größer.

Ich weiß nicht, ob durch weitere Updates noch etwas zu retten ist. Es krankt an den einfachsten Dingen. Was haben die Softwaretechniker da gemacht? Der Geschäftsführer schreibt unter falschem Namen positive Rezensionen bei Amazon und tritt zurück. Er hat anscheinend auch keine Lust mehr.   
Ich verfolgte einen ganz guten Blog über das weTab, aber der Nutzer hat nun, nach zwölf Tagen, auch kapituliert und schickt es zurück. Das wäre auch mein Tipp, zurückschicken oder gar nicht erst kaufen. Apple mag vielleicht etwas teurer sein, aber da funktioniert auch alles.