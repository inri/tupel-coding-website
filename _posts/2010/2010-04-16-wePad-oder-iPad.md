---
layout: post
title: "wePad oder iPad"
description: "Das wePad ist noch gar nicht da und wird trotzdem schon mit dem iPad verglichen und das nicht gerade sanft."
date: 2010-04-16
tags: [Apple, Tablet, iPad] 
published: true
category: it
comments: false
share: true
---

Jaja, ich bin auch ganz neugierig auf die ersten Tablet-PCs. Apple hat den Weg wieder mal geebnet und das finde ich nicht besonders gut. Selbst wenn sie den größten Mist als *neues, superinnovatives Produkt* vorstellen würde, wären viele Menschen begeistert davon und würden sich darum reißen. 


Dabei wird am wePad schon längere Zeit gearbeitet und das, obwohl die [Firma Neofonie](http://www.neofonie.de/) keine Milliarden Dollar an Forschungsetat hat. Die Vergleiche mit Apple sind in meinen Augen eher murks.

Aber genau das wird jetzt eben gemacht. Das wePad wird technisch dem iPad überlegen sein, das war aber auch kein Wunder. Was das OS angeht, bin ich gespannt. Sollte es das wePad wirklich schaffen, Android Apps zu integrieren, wäre das eine ziemlich geile Sache. Ansonsten heißt es abwarten und Tee trinken. Ja, die Pressekonferenz war nicht so richtig optimal, aber was soll's. Das wollen sie Ende April nachholen und dann sieht man vielleicht mehr vom Gerät. 

Ärgerlich sind [solche Skeptiker](http://meedia.de/nc/background/meedia-blogs/stefan-winterbauer/stefan-winterbauer-post/article/ein-medienphantom-namens-wepad_100027366.html). Das wePad nur ein Phantom, nur ein Aprilscherz? Ja, weil sie eben nicht so viel Geld für die Entwicklung ausgeben können, geht es eben auch nicht so schnell. Warum sollte es so unglaublich sein? So superneu ist die Idee mit dem Tablets ja nicht.  
Amazon mit dem Kindle und Sony mit seinem eReader haben es auf die Ökotour (elektronische Tinte) versucht. Technisch waren die Geräte weniger interessant, doch gleichzeitig boomten auf dem Mobilfunkmarkt die Touchscreen-Handys (auch dank Apple). Liegt doch nahe, dass man eine Metamorphose zwischen beiden entwickelt. 

Neofonie tüftelt angeblich nicht erst seit der iPad-Präsentation von Apple an dem wePad. Auch noch so ein Pseudo-Kritikpunkt aus dem Artikel: Was interessiert mich die Bekanntschaften dieser und jener Geschäftsführer? Vitamin B in der Industrie! Oh Gott! Neben Internetspielereien und Foto-Video-Kram, machen Zeitungen und Zeitschriften doch Sinn und da kann man auch zusammenarbeiten.  
Apples restriktive Politik in diesem Bereich ist ja bekannt und wird kritisiert. Warum nicht eine Alternative starten. Außerdem wird keine Firma auf der Welt so viel Vitamin B zusammenkratzen können, um nur annähernd diese Unmengen an kostenloser Publicity zu bekommen, wie sie Apple seit Jahren erfährt.

Das wePad ist ein wenig spät dran. Das ist die einzige Kritik meinerseits. Und vielleicht noch der Preis, aber die UVPs sinken ja meist noch. Bei den anderen Dingen, würde ich einfach mal positiv denken. Und falls es doch ein schlechtes Produkt wird, gibt es bestimmt bald gute Alternativen.

