---
layout: post
title: "SSDs sind klasse"
description: "SSDs sind zwar teuer, aber nutzen auch viel. Wer einen schnellen Rechner haben möchte, der sollte in eine SSD investieren."
date: 2010-11-16
tags: [Hardware]
published: true
category: it
comments: false
share: true
---

Es ist ja immer schwierig mit Leuten zu diskutieren, die eine grundsätzlich andere Meinung in einem Diskussionspunkt vertreten und ihre Meinung auf *Teufel-komm-raus* verteidigen. Ich mache das auch sehr gerne, versuche aber trotzdem meine Meinung vorsichtig zu bilden. Denn wenn sie dann da ist, meine Meinung, bekommt man sie leider nur schwer wieder weg.

Zur Geschichte: Ein Kommilitone will ein neues Notebook. Er grübelte über ein MacBook Pro und schwankt nun doch wieder zu einem Windows-System. Begründung von ihm, es hat ein besseres Preis-Leistung-Verhältnis. Das mag sein, aber es gibt eben auch dicke Vorteile eines MacBooks, die das Manko durchaus ausbügeln können. Man muss wissen was man möchte und was einem wichtig ist.    

Der Kommilitone möchte leistungsstarke PCs. Hat eine neue Nvidia-Grafikkarte, sein CPU ist auch noch recht neu und generell lässt er da gerne viel Geld. Über Umwege kamen wir auch auf SSD-Festplatten zu sprechen.  Das ist nämlich eine Sache, an die er sich noch nicht traut. Sie sind relativ teuer, bringen aber viel Leistung. Ich spreche aus Erfahrung, denn ich nutze seit einem Jahr eine SSD als System und Programmplatte. Windows startet blitzschnell, auch alle anderen Programm haben keine großartigen Ladezeiten und z.b. Installationen sind auch sehr schnell fertig.

## Wie entsteht Performance

Wenn ein PC-Nutzer einfach nur einen flinken PC haben möchte, sollte er meines Erachtens eine SSD einbauen. Festplatten sind nach wie vor der Flaschenhals jedes Systems. Man kann sich ruhig verschiedene Komponenten durch den Kopf gehen lassen. 

RAM ist wichtig, aber es gibt eine Obergrenze, die bei normalen Nutzern nicht überschritten wird. Das sind meiner Meinung nach zur Zeit 4 GB, alles darüber ist nur für dicke Programme (wie Videorendering...) und HighEnd-Games sinnvoll. Die CPU sollte auch stark sein, aber ob es nun 500MHz mehr oder weniger sind, merkt der Nutzer nie im Leben. Die meiste Zeit langweilen sich die Prozessoren sowieso. Und Grafikkarten sind auch nur für eine bestimme Gruppe von Anwendern wichtig, nämlich Gamer.        

Doch all diese Komponenten müssen irgendwie mit dem Kram arbeiten, der auf der Festplatte liegt und das dauert bei normalen Platten eben manchmal etwas länger. Das sind die ganzen Minuten, die man wartet und Däumchen dreht.

Wie gesagt, dem Kommilitonen sind SSD noch nicht ausgereift genug und zu teuer. Die Preise werden aber die nächsten Monate bestimmt nicht rapide fallen. Wir stritten uns, weil ich ihm versuchte zu erklären, dass das bisschen Mehr an Leistung, dass er in einem Sony Vaio hat, nicht so irre ausschlaggebend ist, im Vergleich zu einem normalen MacBook Pro.     
Das sei totaler Quatsch, das Vaio hat ja einen i5 drin und der spielt in einer ganz anderen Liga als ein Schnöder Core2Duo. Und dann meinte er zu mir, dass ich nicht über Leistung zu sprechen brauche, da ich ja im Windows PC eine SSD hätte und mir gerne eine in den Mac einbauen würde, was auch nicht viel bringt.   

## Nachteile von SSDs

In der Tat, ich würde sehr gerne eine SSD einbauen, dann geht der Mac nämlich richtig ab. Das neuen MacBook Air z.b. erreicht mit einer SSD fast die gleiche Leistung wie ein MacBook Pro, obwohl schwächere CPU und keine Grafikkarte verbaut ist. Dann verwies mich der Kommilitone auf seine Datenbankvorlesung und den ganzen Nachteilen einer SSD, neben dem Preis. Die Leistung sinkt nach Jahren enorm, die haben eine geringe Lebenszeit, und so weiter.

Ich habe mir die besagte Vorlesung online angeschaut und kann nur mit dem Kopf schütteln. Der Dozent hat zwar diese Nachteile aufgezählt, aber eben im Kontext eines Datenbankmanagementsystems.    
Was die Lebenszeit angeht, weiß man heute noch gar nicht so viel drüber, weil es eben die Dinger noch nicht wirklich lange gibt. Man geht davon aus, dass sie nicht so lange wie normale Disks halten, aber bisher gibt es anscheinend keine handfesten Vorfälle. Erst recht nicht bei normalen Endnutzern, die eine SSD als System/Programmplatte haben (wie ich). Vielleicht stellt man dieses Manko in 10 bis 15 Jahren fest [^1]. Oh mein Gott.

Es gibt einige Experimente zum Performance-Verlust, aber auch keine handfesten Erfahrungen. Bei einem DBMS mag dieses Phänomen unerwünscht sein, aber das nutzt eine SSD auch sehr stark ab. Es ist also kein Wunder. Aber ich mit meinem paar Schreiboperationen? Kurz gesagt, die Nachteile sind keine, zumindest nicht für normale Endnutzer. SSDs werden noch einige Jahre lang eine Zwischenschicht bilden (zwischen RAM und HDD) und für Endnutzer wie wir, erfüllen sie ihren Zweck sehr gut.

## Fazit

Was ich eigentlich sagen wollte: Statt 100€ mehr für einen Prozessor, 50€ mehr für 2GB mehr RAM und vielleicht noch 50€ mehr für eine stärkere Grafikkarte auszugeben, kauft euch lieber eine SSD.

[^1]: Nachtrag Juli 2014: Mittlerweile gibt es neue Erkenntnisse und die sagen ganz klar, dass SSDs wesentlich länger durchhalten als man erwartet hat. Im Preis sind sie auch enorm gefallen. Und der Kommilitone möchte auch nicht mehr ohne seine SSD leben.