---
layout: post
title: "Zehn Tage und ein iPad (Teil 1)"
description: "Ich habe ein iPad bestellt und erkunde neugierig dieses neue Gerät."
date: 2010-10-09
tags: [iPad] 
published: true
category: it
comments: false
share: true
image: 
  feature: 2010-ipad-1-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Angestiftet von einem Blog über das weTab, schreibe ich nun auch einige Berichte über meine ersten Gehversuche mit dem iPad. 


## Wieso ein iPad?

Ich bin Informatikstudent und auch sonst ziemlich begeistert von neuer Technik. Die Aufregung um das iPad und das Drama des weTabs habe ich gespannt verfolgt und will nun endlich aus der Rolle des Verfolgers heraus. Momentan gibt es nur zwei Möglichkeiten: iPad oder weTab.

Nach meinem letzten Bericht sollte klar sein, dass ich nicht scharf darauf bin ein Beta-Tester zu werden. Ein iPad sollte es also sein. Mein Handy hat UMTS, darauf kann ich beim iPad also verzichten (Ersparnis: 100€). 16 GB Speicher könnten eventuell etwas knapp werden. Einige Filme, Serien und Musik wollte ich auch ausprobieren. Also die 32 GB Version und nicht die großzügige 64er Version des iPads (noch mal Ersparnis: 100€, wobei es auch durchaus 200€ hätten sein können).     
Am schnellsten bekommt man das iPad sicherlich direkt online bei Apple. Nachmittags bestellt, am nächsten Tag war es da. Eine schicke Verpackung, wenig Zubehör, wie man es eben von Apple kennt.

Folgendes sollte noch erwähnt werden: Ich bin größtenteils ein Windows-User. Nur mobil arbeite ich seit einigen Monaten mit einem MacBook. Ich hatte niemals einen iPod Touch oder ein iPhone, bin also von den ganzen Apps noch völlig unberührt. Mir geht es in diesen zwei Wochen um die Frage, ob dieses Gerät für diesen Preis auch einen entsprechenden Nutzen bringt. Falls nicht, kann man sich zum Spielen auch einen halb so teuren iPod Touch kaufen.

## Es geht los

Das iPad muss über iTunes freigeschaltet werden. Einige regt das auf, mir ist es egal. Soll Apple wegen mir meine Kreditkartendaten haben. Diesem Konzern vertraue ich mehr als meiner eigenen Regierung.      
Ist alles eingerichtet und auf den neusten Stand gebracht, geht es los mit den Apps. Relativ bequem kann man durch den AppStore surfen. Ich würde zu diversen iPad-Fanseiten raten, da gibt es eine gute Auflistung von brauchbaren Apps. Apple brüstet sich immer mit der hohen Anzahl von Apps, aber wirklich gut sind nur einige wenige. Und diese zu finden, darin besteht die hohe Kunst des Apps sammeln.

Mit (oder auch ohne) extra Apps, stöpsel ich das iPad ab, lümmele mich auf meine Couch und benutze es! Es ist erstaunlich leicht. Klar, ich will es nicht stundenlang mit einer Hand halten, aber die 700 Gramm sind akzeptabel. Meistens legt man das iPad sowieso irgendwo ab. Es ist auch sehr flach gehalten und sieht, Überraschung!, todschick aus. In den nächsten Jahren werden die Tablets sicherlich noch dünner und leichter, aber für starke Männerarme wie die meinigen, ist es auch momentan schon okay.

Der Bildschirm reagiert blitzschnell und präzise. Das Wischen an sich macht schon sehr viel Spaß und man merkt schnell: Apple hat die Fingerbedienung optimal umgesetzt. Auch der gesamte Bildschirm dreht sich sofort, wenn man ihn neigt, da ruckelt nichts, das geht alles sehr flüssig.