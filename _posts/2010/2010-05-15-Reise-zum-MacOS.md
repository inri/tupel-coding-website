---
layout: post
title: "OS X 10.5.8 auf MacMini"
description: "Der lange Weg einen alten Mac Mini von 10.4 auf 10.5 zu updaten."
date: 2010-05-15
tags: [Apple, macOS, OS X] 
published: true
category: it
comments: false
share: true
image: 
  feature: 2010-macmini-g4-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Leute, Leute, Leute… das war eine interessante Tour durch die Welt des Apfels. Ich will vorerst nichts überstürzen, darum eine Sache nach der anderen.    

## Wieso?

Wir sollen uns vom Studium aus ein wenig mit anderen Betriebssystemen beschäftigen. In meinem Fall bedeutete das Linux und Mac OS X. Nach zwei eher unglücklichen Installationen von Ubuntu als Zweitpartition, beschloss ich Linux in einer VM laufen zu lassen. Ich konnte quasi auf dem linken Bildschirm mein Windows 7 bedienen und ohne Probleme mit der Maus auf den rechten Bildschirm zu Ubuntu navigieren. Sehr angenehm und sehr cool.

OS X wollte ich als illegale Installation nur ungern probieren, zumal es mit meinem AMD-Vierkerner sowieso schwierig geworden wäre. Aber einen neuen Mac kaufen? So richtig günstig sind die Teile ja nicht. Ich informierte mich, sammelte Weisheiten aus dem Internet zusammen und beschloss, einen alten Mac Mini zu kaufen, bei Ebay natürlich.

Es war ein Mac Mini mit einem PowerPC G4 Prozessor. Es lief standardmäßig Mac OS 10.4, mittlerweile gibt es schon 10.6, welches ich aber gar nicht installieren konnte, wie ich feststellen musste. Das neuste OS X unterstützt nämlich die PowerPCs nicht mehr. Okay, bittere Pille, aber das 10.5. wird es doch auch tun, redete ich mir ein. Doch nun findet erst einmal eine Version von Leopard (10.5). Snow Leopard (10.6.) ist schon eine Weile draußen und 95% aller Mac OS Downloads sind eben auch nur 10.6.  

## 1. Versuch

Ich wurde letztlich fündig und brannte die .dmg-Datei auf einer Double Layer DVD. Wo bekommt man sonst noch eine DVD her? Apple selber verkauft sie nicht mehr und ansonsten bleibt ja nur noch Ebay, wo die Preise aber nicht so angenehm sind (halb so teuer wie mein Mini!). Ich schob die DVD in den dezenten Schlitz des Minis und darauf folgte Ernüchterung. &quot;*Datenträger kann nicht gelesen werden.*&quot; Ich gab die DVD einem befreundeten Mac-Kenner und sein Mac bestätigte die Diagnose des Minis. Woran lag es? Falsches Brennprogramm? Falsch gebrannt? Doofer Brenner?

Er verwies mich auf eine andere Download-Seite für Mac Software und dort lud ich eine weitere .dmg von Leopard runter. Derweil probierte ich die erste .dmg mit Nero zu Brennen (vorher war es CDBurnerXP) und auch diesmal endete der Brennvorgang *erfolgreich*. Diesmal konnte der Mini den Datenträger sogar lesen, aber die .dmg Datei hatte anscheinend einen Fehler und konnte nicht entpackt oder eingehangen werden (was ein Mac mit diesen verrückten Dateien eben so macht). Also die dritte DoubleLayer-DVD in den Brenner und die neue .dmg brennen. 

## 2. Versuch

Erfreulicherweise erkannte der Mini diese auch wieder und entpackte sie sogar. Es wurde spannend, eine Stunde lang. Es tat sich ein neues Fenster auf und man konnte &quot;*Installieren von Mac OS 10.5.*&quot; anklicken. Gelesen, getan. Bitte Neustart und dann kam genau gar nichts. Blub. Das System startete normal, nichts war geschehen. Er bootet also irgendwie nicht mit der Installation.      
Diese .dmg wurde irgendwo auf die Platte verfrachtet und naja, vielleicht ist das ja das Problem. Sie muss gebootet werden. Also erzwang ich ein Booten von der DVD, was aber ebenfalls nicht von Erfolg gekrönt war. Lag es an der momentanen OS-Installation? Da hieß der Benutzer auch “testtest”, vielleicht sollte ich einfach mal sauber neu installieren. 

Derweil verbreitete ich meine Leidensgeschichte und ein anderer Mac-Kenner wurde hellhörig und hatte ein Präsent für mich.  

## 3. Versuch

Eine originale Mac OS 10.5.5. Installation DVD. Juhu! Sofort probierte ich diese aus, er bootete auch brav von der DVD, doch dann stellte er fest, dass ich diese Version gar nicht installieren kann. Warum? Bis 10.5.8. werden PowerPCs doch unterstützt! Lag es an meine Version 10.4? Muss ich erst alle Updates bis zur 10.4.11. installieren oder brauche ich sogar eine normale 10.5er? Ich wusste einfach nicht weiter.     

Da fiel mir ein, dass ich irgendwo gelesen hatte, dass man mit den .dmg Dateien irgendwie ein System aufsetzen kann. Und ich erinnerte mich, dass der Autor zwei leere Partitionen als Anforderungen stellte. Nun war ich schon mal im Bootmenü der DVD, da konnte ich auch gleich die Festplatte teilen. Gedacht, getan.

## 4. Versuch

Dann war ein Neustart geplant, der allerdings in die Hose ging. Der Mac Mini bootete wieder von der DVD. Mehrmals versuchte ich ihn zu überreden, es doch nicht zu tun, aber er ließ sich nicht einschüchtern. Dann ahnte ich das Dilemma. Bei der Teilung der Festplatte, wurden alle Partitionen platt gemacht und nun gab es kein Betriebssystem mehr. Darum ließ er auch die nutzlose 10.5.5er DVD nicht mehr raus (ein Mac Mini hat nur einen Einschaltknopf, keinen DVD-Rauswurf o.ä.). Nach ein wenig Suchen im Internet, überlistete ich das System (beim Starten des Systems linke Maustaste gedrückt halten!) und installierte das Mac OS 10.4. zum zweiten Mal neu. Diesmal auch mit allen Updates auf die 10.4.11er Version.     

Wenn ich nun schon die zwei leeren Partitionen hatte, konnte ich zumindest damit etwas machen. Die Anleitung, auf die sich die zwei leeren Partitionen stützten, fand ich nicht mehr. 

## 5. Versuch

Aber eine andere, die erklärte, wie man die .dmg behandelt. Ich entpackte also diese .dmg (mit 10.5) auf eine der beiden Partitionen und das dauerte auch wieder mal ewig. So stark ist der Mini nun mal nicht. Am Ende hatte ich quasi den gleich Inhalt auf der Partition, wie ganz am Anfang, als ich diese .dmg direkt von DVD entpackte. Voller erwarten drückte ich auf die Installationsaufforderung, aber sofort kam die Ernüchterung: &quot;*…kann diese Version nicht auf diesem Mac installieren…*&quot;.

Okay. Geht nicht. Aber vielleicht funktioniert die DVD mit der 10.5.5. nun doch, schließlich ist meine 10.4. auf den neusten Stand (10.4.11.). Selbst mit der aktuellsten 10.4er weigerte sich die DVD ihren Dienst zu tun. Kacke. Vielleicht ist die Version zu hoch, oder die DVD hat erkannt, dass sie eigentlich für ein MacBook gedacht war? Diesmal zerschoss ich zumindest nicht das System mit Festplattengemurkse. Man beendet die Installationsumgebung entweder durch Neustart, oder indem man eine BootPartition auswählt. 

## Die Lösung

Lass ich doch zum Spaß –ich war mit meinem Latein längst am Ende- das System von der Partition booten, die die entpackten .dmg Dateien beinhaltet.      
**It was Magic**. Die Installationsroutine für Mac OS 10.5 begann und wurde auch nicht abgebrochen. Hurra! Ich installierte das neue System über dem alten, damit ja nicht eine Programmdatei auf die Idee kam, vielleicht doch von der alten Installation zu booten. Die Installation dauerte ewig. Auch die 10.5er danach auf den aktuellsten Stand zu bringen (10.5.8) dauerte mindestens noch mal so lange, aber nun läuft alles und ich bin glücklich.

Hätte Microsoft das komplizierter machen können?