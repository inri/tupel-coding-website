---
layout: post
title: "Wieder einmal Tablets"
description: "Einige nachträglichen Worte zum weTab-Desaster und welche Rolle Neofonie gespielt haben könnte."
date: 2010-11-21
tags: [Tablet]
published: true
category: it
comments: false
share: true
---

Beim Aufräumen meines Macs bin ich auf die restlichen iPad-Texte gestoßen. Fertig sind sie so gut wie und deshalb werden sie auch bald veröffentlich. Versprochen.

Aber ich wollte eigentlich über etwas anderes schreiben: weTab. Einige fragen sich vielleicht, was das nun wieder sein soll und das verstehe ich sogar. Von dem guten alten weTab ist ja nun nichts mehr zu hören. Eigentlich. Eine Randmeldung gab es vor einigen Tagen und diese finde ich höchst interessant: [weTab ohne Neofonie](http://www.heise.de/mobil/meldung/WeTab-macht-ohne-Neofonie-weiter-1136768.html)

Weiß jemand, was das eigentlich bedeutet? Vor allem aus welchen Betrachtungswinkel die gesamte Blamage um das weTab nun neu zu bewerten ist. Ich rekonstruiere mal.   
Anfang letzten Jahres oder sogar schon wesentlich eher, setzten sich einige Hard- und Softwareentwickler (namens *4tiitoo*) zusammen und beschlossen ein Tablet zu entwickeln. Die Idee war nicht neu, aber die Hardware dank der Netbooks und leistungsstarken Handys so sehr verkleinert worden, dass die Idee nun endlich umgesetzt werden konnte. Sie tüftelten vor sich hin und streckten ihre Fühler sicherlich zu größeren Firmen aus, die das vermarkten sollen.

Sie hatten kein glückliches Händchen, denn sie kamen mit dem Herrn Ankershofen von Neofonie zusammen. Er hatte anfangs vielleicht sogar noch ritterliche Absichten, sollten doch die armen Zeitungsverlage gerettet werden. Und genau das eben mit einer Software von Neofonie und das auf dem damals noch wePad genannten Gerät, welches sich aber Ende 2009 noch in der Entwicklung befand.    
Schön für eine deutsche Firma, dass sie auf diese Idee gekommen war. Nur leider im April musste man einsehen, dass der weiße Apfel aus Amerika schneller war. Was nun machen? 

Und genau ab diesen Zeitpunkt lief alles aus dem Ruder. Vermutlich der Ankershofen von Neofonie, schlug eine dicke Marketingaktion vor. Obwohl vom Gerät und der Software noch nichts zu sehen war, musste man es der deutschen Medienwelt präsentieren. Dabei stand nur Neofonie in Vordergrund, von den eigentlichen Entwicklern 4tiitoo hörte man gar nichts.

Ich stelle mir folgenden Dialog vor:   

Ankershofen: *Hej Jungs! Wir müssen das wePad jetzt publik machen!*

4tiitoo: *Nee, wir haben gerade mal Designentwürfe und können nicht abschätzen, wann es fertig sein wird.* 
       
A.: *Ach Jungs, kommt schon. Das kann ja wohl nicht so schwer sein. Wir stellen einen Prototypen mit Windows 7 vor, das merkt bestimmt niemand, und kündigen das Gerät für Juni an!*
        
4t: *Für Juni? Das sind ja nur drei Monate! Wie sollen wir das schaffen?!* 
       
A.: *Keine Sorge, ich vertraue euch da. Wir, öh, können ja der Presse sagen, dass das Gerät von uns, der Neofonie, kommt. So sehr vertraue ich euch.*   
      
4t: *Das schaffen wir nicht.* 
         
A.: *Na gut. Juli? August? Einige erste Geräte für die Presse im August und der Rest dann im September. Das wird super! Und wenn nicht, dann fälsche ich einfach einige Bewertungen bei Amazon.*        

4t: *Das wird doch nichts...*

Und es wurde auch nichts. Die ganze Pressearbeit war schon murks. Anfangs der Vergleich mit Apple, dann diverse Ankündigungen über Zusammenarbeit mit Zeitschriften. Nichts von dem haben sie hinbekommen. Der Releasetermin wurde noch verschoben und später dann absolut unfertige Geräte ausgeliefert.      
Die armen Jungs von 4tiitoo konnten das vermutlich gar nicht schaffen. Ankershofen ging nach der Amazon-Blamage und nun hat sich auch die gesamte Neofonie zurückgezogen. Schwupps erscheinen Entwicklertools für das weTab und weiterhin immer mal wieder neue Updates.

Trotzdem, das Gerät hätte so schick werden können.