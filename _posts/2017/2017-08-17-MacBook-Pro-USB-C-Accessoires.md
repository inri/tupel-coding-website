---
layout: post
title: "USB-C Accessoires für das 15er MacBook Pro"
date: 2017-08-17
category: it
tags: [Apple, MacBook, Review, Technik]
published: true
comments: false
share: true
image: 
  feature: 2017-mbp-top-lg.jpg 
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Nach einigen Tagen mit USB-C bin ich überzeugt: USB-C ist die Zukunft. 

Das MacBook Pro ist (nur noch) mit 4 USB-C Ports ausgestattet und quasi von Strom über Datenverbindung bis hin zu externen Displays läuft alles über USB-C. Doch leider ist die Zukunft noch nicht da und die Kabel- und Adapter-Situation etwas durchwachsen.

## Laden

Auf Arbeit und Zuhause muss mein MacBook laden. Und den PowerAdapter möchte ich nicht ständig transportieren, vor allem weil er Zuhause an einem Verlängerungskabel hängt. Also benötigte ich neben dem Apple PowerAdapter einen weiteren und die Wahl fiel auf den [Anker PowerPort+ 5 Premium 60W (50€)](http://amzn.to/2vrsb06). 
Der Vorteil: Man kann neben dem MacBook noch 4 weitere Geräte über USB laden und er kostet nur halb so viel wie der PowerAdapter von Apple. Der Nachteil: 60W laden das MacBook nur sehr langsam auf. Deshalb nutze ich den PowerAdapter auch auf Arbeit, da ich dort in der Regel 8 bis 10 Stunden an einem Ort sitze und es egal ist, ob das Laden 2 oder 6 Stunden dauert.

<figure>
	<img src="{{ site.url }}/images/2017-mbp-power.jpg" alt="Anker PowerAdapter 60W">
	<figcaption>60W für 4 USB- und einen USB-C-Anschluss zum Laden</figcaption>
</figure>

Der mitgelieferte [Apple 87W PowerAdapter (ohne Kabel 89€)](https://www.apple.com/de/shop/product/MNF82Z/A/apple-87w-usb‑c-power-adapter?fnode=8b) hat deutlich mehr Power und lädt entsprechend schneller. Leider ist das [Apple Netzteilverlängerungskabel (25€)](https://www.apple.com/de/shop/product/MK122D/A/netzteil-verlängerungskabel?fnode=8b) nicht mehr dabei und muss bei Bedarf separat gekauft werden.
Mitgeliefert wird aber das [USB-C Kabel von Apple (2m, 25€)](https://www.apple.com/de/shop/product/MLL82ZM/A/usb-c-ladekabel-2-m?fnode=8b), aber auch hier gibt es günstigere Alternativen, z.b. [zwei USB-C auf USB-C Kabel (16€](http://amzn.to/2vr7XUm)

Einige Worte zum Laden mit USB-C: Ich war anfangs etwas skeptisch und bedauerte den Verlust des MagSafe-Anschlusses. Aber es gibt u.a. bei [Kickstarter einen MagSafe-Ersatz namens Snapnator (30$)](https://www.kickstarter.com/projects/436147229/snapnator-your-macbook-snap-feature-is-back), den ich zwar noch nicht erhalten habe, aber der die fehlende Funktion bietet. Daneben finde ich die Freiheit, den PowerAdapter ohne nerviges Kabel zu transportieren, ziemlich gut, denn jedes USB-C Kabel funktioniert mit dem PowerAdapter! Mein Bruder zerlegt alle 2 Jahre einen alten MagSafe-PowerAdapter und muss ihn komplett ersetzen, für viel Geld. Das wird mit dem Neuen kaum passieren, denn man kann deutlicher billiger das Kabel austauschen.

## Adapter

Ende 2016 bot Apple diverse Kabel und Adapter rund um USB-C etwas vergünstigt an und dabei sicherte ich mir zwei [USB-C auf USB Adapter (25€)](https://www.apple.com/de/shop/product/MJ1M2ZM/A/usb-c-auf-usb-adapter?fnode=8b). Die braucht man immer mal, wenn ein *alter* USB-Stick daherkommt oder so.
Ebenfalls sehr praktisch ist der [SD und microSD Adapter auf USB-C und sogar USB/microUSB in einem (13€)](http://amzn.to/2ut3tPU).

<figure>
	<img src="{{ site.url }}/images/2017-mbp-sd.jpg" alt="SD und microSD via USB-C und USB und microUSB">
	<figcaption>Multifunktionaler Adapter für SD und microSD via USB-C, USB und microUSB</figcaption>
</figure>

Auf Arbeit und Zuhause habe ich Bildschirme mit eingebautem USB-Verteiler. Dafür benötigt man ein Kabel mit USB-B-Stecker und es gibt natürlich auch welche mit [USB-C auf USB-B (9€)](http://amzn.to/2vTGBJE). Damit spart man sich einen separaten USB-Verteiler und erhält bspw. 4 USB-Anschlüsse. Ich kann ohne Probleme Maus und Tastatur anschließen und zwei USB-Sticks oder so. 

Da ich noch einige SSDs herumliegen habe, besorgte ich mir ein
[externes 2,5"-Festplattem-Gehäuse mit USB-C (18€)](http://amzn.to/2vJIBU7). Die SSD werde ich verschlüsseln und zu meinem Fotospeicher machen. Ich verspreche mir dank des USB-C so gute Übertragungsraten, dass ich ohne Probleme die Bilder auf der externen Festplatte direkt in Lightroom bearbeiten kann. Und eventuell sogar Videos.

## Bildschirme

Die richtigen Kabel zu finden, ist keine leichte Aufgabe. Wie hoch ist die Auflösung des Displays und welche Anschlüsse bietet es? Perfekt sind Displays mit USB-C respektive Thunderbolt 3, aber davon gibt es nicht besonders viele und die kosten entsprechend Geld. 

<figure>
	<img src="{{ site.url }}/images/2017-mbp-left.jpg" alt="DisplayPort und Strom">
	<figcaption>Kabel für DisplayPort und Strom</figcaption>
</figure>

Die anderen beiden einzigen Alternativen dazu sind DisplayPort (DP) und HDMI. 
*Aufgepasst: DP schafft bei einem 4K-Display 60Hz, letzteres nur noch 30Hz, und das ist nicht empfehlenswert.*  Deshalb habe ich mich beim 4K-Display für das [USB-C auf DisplayPort Kabel (23€/25€)](http://amzn.to/2wyV8r6) entschieden und das funktioniert prima.  
Bei den beiden FullHD-Bildschirmen auf Arbeit reichen eigentlich HDMI-Kabel, aber ich fahre zweigleisig und habe neben dem USB-C auf DP noch dieses [USB-C auf HDMI Kabel (20€)](https://www.amazon.de/gp/product/B01MZBA8XO/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1). Das war allerdings mein zweiter HDMI-Kabel Kauf. Das erste funktionierte nämlich nicht und tatsächlich fand sich auf den zweiten Blick dieser Hinweis auch in der Beschreibung, leider im sehr schlechten Deutsch verpackt und definitiv nicht leicht zu lesen. Außerdem las ich vorher eine Bewertungen von jemand, der meinte dass es bei ihm angeblich funktionierte.

<figure>
	<img src="{{ site.url }}/images/2017-mbp-right.jpg" alt="HDMI und USB-B">
	<figcaption>Kabel für HDMI und USB-B</figcaption>
</figure>

Das ist ein genereller Tip: Immer die Fragen und Bewertungen bei Amazon überfliegen und hoffen, dass jemand vor dir das Zubehör ausprobiert hat.


### Kosten Zuhause

Ein USB-C auf USB Adapter, ein DP-Kabel, SD/microSD-Adapter, ein USB-C auf USB-B Kabel und das Festplattengehäuse für insgesamt **ca. 80€**.

### Kosten Arbeit

Ein PowerAdapter, ein USB-C Ladekabel, ein USB-C auf USB-B Kabel, ein DP-Kabel, ein HDMI-Kabel und ein USB-C auf USB Adapter für insgesamt **ca. 120€**.


### Warum keine Hubs oder Docking Stations?

Die meisten Hubs sind hässlich und bieten einfach nicht genug Funktionen für ihr Geld. Zuhause brauche ich einen DP-Anschluss und auf Arbeit entweder einen DP und HDMI oder zwei HDMI - diese Kombis gibt es aber nicht.
USB-Sticks etc. und SD/micoSD-Karten schließen ich so selten an, dass theoretisch das USB-B Kabel schon ausreichend wäre und ich mit den paar zusätzlichen Adaptern sehr gut hinkomme.

Die Docking Stations bieten meist viele Funktionen, die ich bei Weitem nicht alle brauche, und kosten dabei aber noch mal deutlich mehr als profane Hubs. Und es gibt kaum Docking Stations, die genug Power haben um ein 15er MacBook zu laden. 

Ideal sind die UltraFine Displays von LG ([5K](https://www.apple.com/de/shop/product/HKN62ZM/A/lg-ultrafine-5k-display?fnode=8a) und [4K](https://www.apple.com/de/shop/product/HKMY2ZM/A/lg-ultrafine-4k-display?fnode=8a)), über die man das MacBook mit einem USB-C/Thunderbolt 3 Kabel laden kann und drei USB-Anschlüsse am Display erhält. Was will man mehr? Vielleicht einen etwas geringeren Preis.

### Fragen und Hinweise?

Ihr könnt immer Fragen stellen, aber bei diesem relativ neuem Thema möchte ich gerne explizit darauf hinweisen und bin natürlich für jede Art von Tipp und Hinweis dankbar.


