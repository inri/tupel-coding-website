---
layout: post
title: "Hackintosh adé"
date: 2017-08-15
category: it
tags: [macOS, Apple]
published: true
comments: false
share: true
---

Nach nicht ganz einem Jahr trage ich das Experiment Hackintosh zu Grabe. Ich verstehe warum es viele Fans hat, für mich funktionierte es leider nicht.

Als ich vor einigen Wochen ein Security-Update auf meinen Arbeits-Hackintosh für OS X El Capitan installierte, ahnte ich nicht, dass es sein letztes Update sein würde. Denn das System bootete nach dem Update nicht mehr und weder konnte ich das Problem finden noch lösen und so blieb nur eine Neuinstallation. Eigentlich begann mein Experimentieren mit dem Hackintosh relativ erfolgreich. 

Die Community rund um [tonymacx86.com](https://www.tonymacx86.com) hat den Prozess, einen Hackintosh zusammenzustellen und einzurichten, grundsätzlich deutlich vereinfacht. Sie bieten nicht nur eine monatlich aktualisierte Liste mit funktionierenden Komponenten, sondern auch eine Vielzahl an Schritt-für-Schritt-Anleitungen an. 
Nach einer dieser Anleitungen inklusive Komponenten-Empfehlung stellte ich einen Heimarbeitsrechner zusammen und mir gelang sogar die Einrichtung von macOS Sierra. Funktionierte dieser Hackintosh problemlos? Nein. Die Soundausgabe bekam ich nicht zum Laufen und, wie auch bei dem späteren Arbeits-Hackintosh, wurde bei jedem Boot ein neuer Eintrag im Boot-Menü des BIOS hinzugefügt. Das allein war nicht so nicht schlimm, aber nervig und störend, besonders in Anbetracht des auf der zweiten SSD installierten Windows.

Einige Wochen später wiederholte ich den ganzen Spaß mit dem Arbeits-Hackintosh und etwas anderen, aber eigentlich kompatiblen, Komponenten. Diesmal bekam ich ihn nicht zum Laufen bzw. bekam ich ihn nicht mit zwei Displays unter macOS Sierra zum Laufen. Eventuell wäre das bei dem anderen auch ein Problem gewesen, aber dieser musste nur mit einem 4K-Display auskommen.    
Eine Neuinstallation auf das ein Jahr ältere OS X El Capitan löste das Problem mit den zwei Displays, allerdings blieben die Nachteile (Soundausgabe, Boot-Menü). Aber er lief immerhin! 

Den heimischen Hackintosh stampfte ich wiederum einige Zeit später ein und arbeitete nur noch mit Windows. Der Systemwechsel zwischen macOS und Windows war beim Hackintosh problematisch und ich wollte mir die Challenge auferlegen, zu Hause nur mit Windows 10 zu arbeiten.    
Der Arbeits-Hackintosh lief aber relativ problemlos, fast ein ganzes Jahr. An das Ab- und Anstöpseln des zweiten Displays bei jedem Start gewöhnte ich mich relativ schnell und die Angst vor jedem Sicherheitsupdate wurde auch kleiner.

Mit Windows 10 wurde ich jedoch nie so richtig warm. Wenn ich meine Abneigung formulieren müsste, ich würde Windows 10 im Vergleich zu macOS als *unelegant* bezeichnen. 

### Was sind die Probleme mit Hackintoshs?

Man hat keine FileVault-Verschlüsselung. 

Man hat dafür ständig Update-Angst - also dass das nächste System-Update den Hackintosh lahmlegt. 

Viele kleine Dinge können u.U. nicht funktionieren, wie z.b. die Soundausgabe, gewisse iClouc-Funktionen, Anschluss von Geräten wie Displays,...

Als Arbeitsrechner kann ich einen Hackintosh leider nur empfehlen, wenn man unbedingt ein leistungsstarken Mac benötigt. Das Einrichten kann mehrere Tage dauern und ein vermeintlich harmloses Systemupdate kann ihn komplett abschießen. 

Privat sollte man sich aufgrund der fehlenden FileVault-Verschlüsselung nur zum Experimentieren einen Hackintosh anschaffen. Und dann sollte man ein gewisses Durchhaltevermögen besitzen um die ganzen kleinen Problemchen zu lösen oder zu ignorieren. 

Das war alles spannend, aber nun tippe ich diese Zeilen wieder auf einen richtigen Macintosh von Apple. 