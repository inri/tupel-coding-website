---
layout: post
title: "Fettlogik, Abnehmen, Diät"
date: 2016-03-10
category: blog
tags: [Gesundheit, Fitness, Review]
published: true
comments: false
share: true
---

Wir müssen uns mal über den Themenkomplex *Ernährung-Diät-Körpergewicht* unterhalten. Der Anstoß ist das großartige Buch &quot;*Fettlogik überwinden*&quot; von Nadja Hermann. Was man darin erfährt, lässt sich als unbequeme Wahrheit bezeichnen und wird deshalb nicht bei jedem auf Begeisterung stoßen. Aber es ist wichtig, denn es geht um eure Gesundheit, verdammt noch mal!


Ich muss etwas ausholen. Gehen wir zurück ins Jahr 2013. 

### Kalorien zählen anno 2013

In einem [älteren Artikel](http://ingorichter.org/blog/Kalorien-zaehlen-mit-Lifesum) beschrieb ich bereits den Webdienst *Lifesum*, damals noch *ShapeUpClub*. In einem Studiumprojekt wollten wir einen solchen Webdienst bauen und bei der Recherche nach bereits vorhandenen Lösungen, stieß ich auf den *ShapeUpClub*. Anfang 2013 beschloß ich mit Hilfe von *Lifesum* abzunehmen. Kurz gesagt: Man zählt Kalorien und isst weniger, so dass man unter einer bestimmten tägliche Gesamtkalorienanzahl bleibt. 

Diese Anzahl ist von Mensch zu Mensch unterschiedlich und hängt vor allem vom Körpergewicht und der sportlichen Aktivität ab. *Lifesum* setzt den maximal möglichen Gewichtsverlust pro Woche auf 1kg und das bedeutete für mich, ich konnte zwischen 1700 und 2000 Kalorien täglich essen. Ist das viel? Ich merkte schnell, dass ich weniger aß, aber es war auch keine unmenschliche Einschränkung. Und das beste? Das Abnehmen funktionierte! Ich verlor in 2,5 Monaten, also bis Ende März, 10kg und hielt das mehr oder weniger bis Ende April. Dann begann die Prüfungszeit und das ständige Kalorien Zählen war mir zu aufwendig.

In den folgenden Monaten nahm ich meine alten Essgewohnheit wieder auf und an Gewicht zu. Ende des Sommers war ich wieder bei meinem Ausgangsgewicht angekommen.    


### Fettlogiken

Mir war schon klar, dass das Zählen von Kalorien scheinbar ganz gut funktionierte. Aber es war anstrengend, obwohl die App von *Lifesum* eine große Hilfe war und ist. In den letzten Jahren nahm ich mir öfter mal vor, wieder zu Zählen, doch nach einigen Tagen brach ich das ganze ab. Am meisten ärgerte mich meine Lust aufs Essen.  

Ich lese nicht in Abnehm-Foren oder bin in solchen Communities aktiv. Trotzdem stieß ich irgendwann auf einen Artikel über das richtige Körpergewicht, das &quot;genetische Wohlfühlgewicht&quot;. Es ist eine super Begründung wieso viele Menschen nicht auf ihr Traumgewicht kommen und nach Diäten wieder zunehmen. Der Körper strebt immer einem bestimmten Gewicht entgegen, da kann man quasi nichts gegen machen. Es sei sogar geradezu ungesund, wenn man ständig gegen dieses Gewicht kämpft. Das leuchtete mir ein. 

Natürlich war auch von unterschiedlich aktiven Stoffwechseln die Rede und auch das konnte ich nachvollziehen. Einer meiner besten Freunde kann Essen wie ein Loch und ist trotzdem sehr schlank. Auch eine andere Freundin nimmt nicht zu, obwohl sie sich nach eigenen Angaben ungesund ernährt und auch viele Süßigkeiten isst. Ich muss mein Gewicht akzeptieren. Das bisschen Übergewicht schadet doch nicht…


### Glauben

Jeder der beginnt sich über solche Fettlogiken zu informieren, wird sehr schnell viele weitere finden. Einige klingen albern, andere absolut logisch. Hier und da wird auch eine Studie verwiesen und man fühlt sich bestätigt.     
Wenn ich heute darüber nachdenke, schäme ich mich über meine Denkweise. Es ist in gewisser Weise eine religiöse Denkweise.

Ich wurde nicht gläubig erzogen und lehne jede Form von Glauben ab. Meiner Meinung nach sollte man nur an sich selbst glauben und an die Familie und Freunde. Trotzdem glaubte ich unbewusst an eine Art Gott des Fettes. Ich habe unhinterfragt diese Fettlogiken akzeptiert und mich meinem Schicksal ergeben.      
&quot;Ich kann leider nichts gegen mein Übergewicht tun, mein Körper will dick sein!&quot; &quot;Meine Gene bestimmten das!&quot; &quot;Ich habe eben Pech gehabt.&quot; &quot;Und außerdem ist es doch gar nicht so ungesund.&quot; &quot;Die Schmerzen im rechten Knie, deren Ursache kein Arzt gefunden hat?&quot; &quot;Meine mangelhafte Kondition, bspw. beim Treppensteigen?&quot; &quot;Die geringe Auswahl an passenden Hosen in den meisten Läden?&quot; Istebenso.

Und ich gebe zu, ich war zu dieser Zeit gar nicht mal so unglücklich.


### Familie und Freunde

Bevor ich zu dem Buch von Nadja Hermann komme, einige Worte zu meinem Umfeld. Meine besten Freunde sind schlank und sportlich. Ich war schon immer derjenige mit einem kleinen Bäuchlein, nur dass dieses Bäuchlein in den letzten Jahren immer größer wurde. Meine Freunde kommentierten mein Gewicht kaum. Hier und da gab es eine Stichelei ab und zu.     
Meine Familie hielt sich auch zurück. Die Mutti sagte manchmal, ich müsse mal etwas abnehmen und der kleine Bruder (sehr schlank) stichelte auch ab und zu. 

Meine Verlobte hat eigene Komplexe mit ihrem Körper, die keiner außer sie selbst so richtig versteht. Sie sieht wundervoll aus, ist schlank und hat ein perfektes Gewicht. Dass ich fast doppelt so viel wiege wie sie, sieht sie wesentlich unkritischer. Natürlich möchte Sophie, dass ich abnehme. Doch weil sie mit sich selbst und ihrem Körper nicht zufrieden ist, möchte sie andere nicht für ihren Körper kritisieren. Mich leider miteingeschlossen. 

Zusammenfassend: Aus meinem Umfeld blieb harte Kritik aus.
    
Ein Erlebnis hatte ich vor einigen Wochen, als ein Trainingsplan für mich erstellt wurde. Ich hatte bis dahin 2 kg abgenommen, war also mit 97 kg noch deutlich übergewichtig, aber mein Trainer fand, dass dieses Gewicht für einen 1,90 m großen Mann doch okay sei. Naja.


### &quot;Fettlogik überwinden&quot; von Nadja Hermann

Dank ihrer witzigen und klugen Comics stolperte ich über Nadja Hermanns Buch. Zugegeben, Bücher übers Abnehmen interessieren mich nicht. Ich halte bzw. hielt die gesamte Diät-Industrie für mehr oder weniger kriminell. Das differenziere ich mittlerweile. Ich dachte mir so: &quot;*Wenn &quot;Fettlogik überwinden&quot; nur halb so intelligent ist wie ihre Comics, dann ist es auf jeden Fall lesenswert.*&quot; Und was soll ich sagen, es war verdammt lesenswert! 

Was zeichnet dieses Buch aus? Es ist ehrlich. Die Autorin war selbst adipös (150 kg bei 1,75 m) und sie fand sich damals mit ihrem Gewicht ab (dank Fettlogiken). Erst ein körperliches Leiden war der Anstoß für ein Umdenken. 

Dazu ein Zitat aus ihrem ebenfalls [sehr lesenswerten Blog](https://fettlogik.wordpress.com/):

&quot;*Ich fing an, all mein vermeintliches “Wissen” über Übergewicht kritisch zu hinterfragen. Seit 2013 las ich jeden Fitzel Information, der mir zu den Themen Übergewicht, Abnehmen, Diäten, Sport und Gesundheit in die Hände fiel, hauptsächlich wissenschaftliche Artikel über Stoffwechsel usw. – und ich musste einen großen Teil meiner “Glaubenssätze” über Bord werfen.*&quot;

Dass Dr. Nadja Hermann etwas mit wissenschaftlichen Artikeln anfangen kann, beweist ihr Doktor in Psychologie. Es gibt auch kluge Menschen mit Realschulabschluss, Abitur oder Hochschulabschluss, aber den Umgang mit wissenschaftlichen Texten muss man lernen. Wenn man sich die Berichterstattung unserer Medien über Studienergebnisse anschaut, stellt man leider fest, dass Journalisten den Umgang mit solcher Art Literatur meist nicht beherrschen. Und Diät-Gurus gleich gar nicht.

Mich hat auch ihre Herangehensweise überzeugt. Die Grundlage des Buches ist Wissenschaft und darauf bauen ihre eigenen Erfahrungen auf.    
Oft ist es bei den Diät-Gurus umgekehrt: Die eigenen Erfahrungen sind die Grundlage und darauf basierend werden Fettlogiken formuliert oder Diäten entworfen. Eine Überprüfung findet kaum statt und bestenfalls biegt man sich irgendwelche Studien zurecht. Mit viel Charisma ausgestattet, bringt man den eigenen Abnehm-Glauben an die Leute und so manifestieren sich Fettlogiken.       

Diesen Weg ging die Autorin nicht. Denn sie ist nicht nur ehrlich mit sich selbst, sondern auch mit dem Leser. Er wird mit seiner eigenen Verantwortung konfrontiert. Es gibt keinen Gott des Fettes, es gibt keine Entschuldigung. **Du** bist für dein Gewicht verantwortlich, weil **du** sehr wahrscheinlich zu viel isst und dich zu wenig bewegst.     
Diese Einsicht ist anfangs vielleicht kein tolles Gefühl. Verantwortung, auch für sich selbst, wiegt erst einmal schwer. Wenn man aber begreift, dass das keine Bürde ist, sondern eine Chance, dann wirkt es befreiend. 

Und es gibt wirklich keine Entschuldigung. Keine Gene, keine Medikamente, nicht der Stoffwechsel, nicht dein Körper - **du allein bist für dein Gewicht verantwortlich**. Die besagten Faktoren spielen eine kleine Nebenrolle, so dass einige Menschen einfach gerne essen oder bestimmte Medikamente den Appetit anregen. Und ja, das ist nicht gerecht, aber auch das Leben ist nicht gerecht. Es sind desto trotz keine unüberwindbaren Hindernisse, denn mit Willenskraft kann man sie bezwingen.

Nach dem ersten Paukenschlag (*Du bist selbst verantwortlich*) folgt der zweite: **Übergewicht ist ungesund**. So richtig ungesund und gefährlich. Der BMI ist ein ganz guter Indikator und für viele Menschen gibt er sogar positivere Ergebnisse zurück. Also du hast einen BMI im normalen Bereich, treibst aber nie Sport? Vermutlich hast du ein höheres gesundheitliches Risiko als du ahnst.     
Viele Menschen denken in Bezug auf ihr Gewicht und ihre Gesundheit nicht langfristig. Dabei geht es nicht um ein paar Lebensjahre weniger, sondern viel mehr um die Frage, wie deine letzten Lebensjahre sein werden. 

Ich kann hier nur einige Punkte aus dem großartigen Buch anreißen. Tut euch einen Gefallen und [lest &quot;Fettlogik überwinden&quot;](http://www.amazon.de/Fettlogik-überwinden-Nadja-Hermann/dp/3548376517/ref=sr_1_1?ie=UTF8&qid=1457601866&sr=8-1&keywords=fettlogik+überwinden). Es ist auch definitiv nicht nur übergewichtigen Menschen zu empfehlen. Eigentlich gehört es auf den Lehrplan jeder Schule. 

In den nächsten Tagen veröffentliche ich einen weiteren Artikel, in dem ich beschreibe, wie mein Abnehm-Projekt 2016 läuft. Kleiner Spoiler: In den letzten 9 Wochen habe ich 13 kg abgenommen.