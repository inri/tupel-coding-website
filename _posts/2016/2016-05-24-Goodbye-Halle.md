---
layout: post
title: "Goodbye Halle"
date: 2016-05-24
category: blog
tags: [Halle]
published: true
comments: false
share: true
---

&quot;*Ich hoffe es wird für Sie demnächst ruhiger*&quot; sage ich zum Objektbetreuer, der auch für unsere 55qm-Wohnung in Halle Neustadt zuständig ist, und wir geben uns die Hand zum Abschied. Ein kurzer Blick auf meine Apple Watch bestätigt den Verdacht: Mir bleiben nur 35 Minuten um mit dem Fahrrad zum Hauptbahnhof Halle zu radeln, eine Fahrkarte zu kaufen und in den Zug Richtung Potsdam zu steigen.   

Mir macht das Kaufen einer Fahrkarte sorgen. Für ein Bahnticket via Bahn App muss man sich ausweisen können. Das kann ich leider nicht, denn meine Verlobte ist gestern mit den zerbrechlichsten Gegenständen **und** meinem Portmonee nach Potsdam zu unserer neuen Wohnung gefahren. Etwas Bargeld habe ich zum Glück (danke Elisabeth!).

Während ich in die Pedalen trete und in den nächsten Gang meines Fahrrads schalte, ruft von oben eine Anwohnerin nach dem Objektbetreuer. Er stöhnt, verdreht die Augen und bleibt zurück. Ich nehme eine linke Kurve, eine rechte Kurve und lasse meinen Wohnort der letzten zwei Jahre endlich hinter mich.     
In den kommenden Wochen wird hier ein Sicherheitsdienst angestellt und dann dafür sorgen, dass das Objekt Daniel-Pöppelmann-Straße 3 bis 9 nicht noch mehr beschädigt wird. In letzter Zeit wurde zu viel randaliert, zu viel kaputt gemacht und zu wenig dagegen getan. Sogar eine der eher mittelmäßigen Einbauküchen wurde aus einer leerstehenden Wohnung geklaut.

Meine Verlobte und ich haben zwei Jahre lang nicht nur in einer problematischen Gegend gewohnt, unser Block war auch ein ganz spezieller. Dafür war die Miete günstig. Verdammt günstig. Wo bekommt man sonst noch für knapp über 4€ pro Quadratmeter eine Wohnung, abgesehen von ländlichen Einöden?       
Ich fahre die Magistrale runter in Richtung Zentrum. Links und rechts stehen Blöcke, große und kleine. Hier und da tut sich eine Lücke auf, wo ein leerstehender Block bereits abgerissen wurde. In den nächsten Jahren werden es mehr Lücken werden.  

Die meiste Zeit folge ich den Schienen für die Tram. Über den schönen Marktplatz und das niedliche Zentrum von Halle fahre ich leider kein letztes Mal, das wäre ein Umweg und den kann ich mir nicht leisten. Es ist der letzte richtig heiße Tag im Sommer 2015. Die Temperaturen klettern wieder über 30° Celsius und das erleichtert die hügelige Fahrradfahrt nicht gerade. An roten Fahrradampeln bleibe ich nur stehen, wenn ein Auto kommt. Wenige Sekunden Stillstand in der glühenden Sonne sind schon unerträglich. 

Abgehetzt stehe ich im Bahnhof und meine vor Schweiß glitschigen Finger rutschen über den Touchscreen des Fahrkartenautomaten. Eilig habe ich es nicht mehr, denn ich bin schnell geradelt. Nach und nach sucht sich der Schweiß seinen Weg aus meinem Körper. An meinen Armen bilden sich Tropfen und meine Augenbrauen können ihn nicht mehr aufhalten. Wenig später im Zug wird es auch nicht weniger dank fehlender Klimaanlage. Mein einziger Trost sind die vielen Packungen Taschentücher, die ich aus Mangel einer letzten Rolle Klopapier kaufen musste. 

So sitze ich in der Sauna namens Regionalbahn und wische mir den Schweiß aus dem Gesicht. Doch das stört mich nicht. Von Weitem könnte es so aussehen, als ob ich mir Tränen von den Wangen wische. Und wenn es Tränen sind, dann Freudentränen.     

Goobye Halle.     

Hello again Potsdam!