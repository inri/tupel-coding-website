---
layout: post
title: "Goodbye GitHub Pages"
date: 2016-02-22
category: blog
tags: [Web, Tupel Coding]
published: true
comments: false
share: true
---

Vor über 10 Jahren auf blog.de begonnen, nach einer längeren Pause auf Blogger neu gestartet um dann nach einigen Jahren auf Uberspace umzuziehen und selbst zu hosten. Nur um etwas später wiederum auf GitHub Pages zu setzen. Und jetzt der Wechsel zurück zu Uberspace.


Lasst mich das in Ruhe erklären. Ich mochte die GitHub Pages eigentlich ganz gerne. Ich mag zwar die public-only Regel bei GitHub nicht, aber das automatische Bauen der Website bei jedem Push war eine wertvolle Arbeitserleichterung.     

Das Problem ist die mangelnde Verschlüsselung von GitHub Pages Webseiten.

## Keine Verschlüsselung via GitHub

Es kann sein, dass der obige Satz nicht ganz korrekt ist. Mir geht es nicht unbedingt nur um meinen Blog, sondern auch um meine Subdomains. Damit meine GitHub Page über eine eigene Domain erreichbar sein konnte, mussten die A Records auf GitHub zeigen. Subdomains funktionierten unabhängig von diesem Mechanismus, abgesehen von der Verschlüsselung.

Dank *Let’s Encrypt* ist es sehr einfach und vor allem kostengünstig geworden, die eigene Website zu verschlüsseln. Leider geht die besagte Verschlüsselung auf Uberspace nur ganz (Domain + Subdomains) oder gar nicht. 

## Back to Uberspace

Also ist mein Blog wieder zu Uberspace gezogen. Gleichzeitig zog mein Blog Projekt auf GitHub zu GitLab, wo [es zwar zur Zeit noch öffentlich ist](https://gitlab.com/inri/tupel-coding-website), aber jederzeit auf privat gesetzt werden kann. Dazu später mehr.

Aktuell lade ich die generierten Website-Dateien per Hand selbst hoch. Demnächst muss ich mir das wieder schön einrichten.

Geändert hat sich für euch nicht viel. Das Design wurde dem aktuellen Design des So Simple Theme angepasst und deshalb hat die Website auch die coole Preview-Funktion. 

Wenn ihr möchtet verspreche ich euch in Zukunft wieder mehr Artikel. 
