---
layout: post
title: "9 Stunden mit der Apple Watch"
date: 2015-05-12
category: it
tags: [Apple Watch]
published: true
comments: false
share: true
image: 
  feature: 2015-Watch-am-Arm-lg.jpg 
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Neun Minuten nach 12 Uhr klingelte UPS bei mir und keine halbe Stunde später umschlang schon die Apple Watch, Sport-Edition in 42mm und Space Gray, mein linkes Handgelenk. Da ist sie also, die Smartwatch von Apple, die nicht so genannt werden möchte. 

Vor über 4 Wochen bestellt, kam sie endlich 18 Tage nach offiziellem Auslieferungsbeginn und damit hielt Apple das Versprechen zum Lieferzeitpunkt.

Viele Reviews wurden über die Apple Watch schon geschrieben und bis ich über neue Erkenntnisse berichten kann, vergehen sowieso noch einige Tage oder sogar Wochen. Wenn überhaupt. Deshalb gibt es an dieser Stelle nur eine lose Auflistung von Dingen, die mir  in den ersten 9 Stunden mit der Apple Watch aufgefallen sind.

### Äußerlichkeiten
* Das Paket wog 1kg und ist damit mehr als 18x so schwer wie die Apple Watch an sich (55g)
* Die (größere) 42mm-Version der Watch gefällt mir und ist definitiv **nicht** zu groß, auch an meinem eher kleinen Handgelenk (Umfang 18cm).
* Allerdings ist die 42mm-Version zu groß für die zierlichen Hände  (14cm) meiner Verlobten Sophie.
* Im Lieferumfang gibt es ja zwei Loch-Bänder, ein langes mit 14cm und ein kurzes mit 12cm. Beim langen Band ist das 3. Loch angenehm, doch dann sind gute 3cm Band zu viel und überlappen. Beim kleineren ist das Äquivalent dazu das 5. Loch, so gut wie ohne Überlappung. Aktuell tendiere ich zum kleineren Band.
* Die Apple Watch ist in der Tat etwas dick. Die Höhe ist signifikant, vor allem im Vergleich zu meiner Uhr von *Skagen*.
* Wenn man vorher keine Uhr getragen hat, oder nur eine eher kleine und schlichte, bemerkt man das Gewicht der Apple Watch am Handgelenk. Aber nach einer Stunde gewöhnt man sich daran.
* Das Space Gray als Farbe gefällt mir sehr gut. Es harmoniert mit Bildschirm und Band und die Mattigkeit verleiht der Watch eine gewisse Wertigkeit (die sie bei einem Preis von 450€ definitiv auch hat).
* Hätte Apple das Material, aus dem die Bänder der Sport-Edition gefertigt sind, nicht einfach Gummi oder Silikon anstatt Fluorelastomer nennen können? Nicht so richtig, denn es ist eben nicht bloß Gummi. Ich kann es leider auch nicht anders beschreiben als dass sich dieses Fluorelastomer einfach weicher und wertiger anfühlt, als normale Gummibänder. Das Band vom MiBand ist bspw. steifer und hat eine gummiartigere Oberfläche.

<figure>
	<img src="{{ site.url }}/images/2015-Skagen-und-Watch.jpg" alt="Skagen und Apple Watch">
	<figcaption>Skagen Uhr und die Apple Watch</figcaption>
</figure>

### Interaktion
* Die Tap Engine, das feinere Vibrieren, ist in der Tat wie ein kleiner Klaps mit dem Finger. Beispielsweise beim Timer gibt es aber auch zusätzlich noch ein stärkeres Vibrieren, wie man es vom Smartphone kennt oder auch beim MiBand findet.
* Die Krone lässt sich super bedienen, wie nicht anders erwartet.
* Beim Force Touch freue ich mich jetzt schon auf die Implementierung in zukünftigen iPhones und iPads. Es ähnelt dem LongClick, aber das stärkere Drücken gegen den Bildschirm und das haptische Feedback der Tap Engine machen diese Geste wesentlich interaktiver.
* Das automatischen Anzeigen der Uhrzeit beim Heben des Arms empfand ich als ausreichend schnell. Und ich gehe stark davon aus, dass zukünftige Updates das noch verbessern werden.
* Mikrofon-Aufnahmen klingen nicht großartig, aber sind trotzdem verständlich.

### Software
* Das Einrichten über das iPhone ging schnell und problemlos, wie nicht anders erwartet. Ich habe das automatische Installieren aller Watch-Erweiterungen durch Apps untersagt. Ich wähle mir nützliche Apps lieber nach und nach selbst zusammen[^1].
* An die Steuerung allgemein muss man sich gewöhnen, das ist schließlich kein kleines iPhone. Force Touch und Krone sind aber definitiv richtige und wichtige neue Eingabemöglichkeiten. 
* (Immer noch) nicht okay: Apple Apps lassen sich wie auch beim iPhone und iPad nicht löschen. Ich brauche keine Aktien! 
* Die stündlichen Erinnerungen zum Aufstehen sind sehr nützlich.
* Ich betreibe die Uhr quasi lautlos und werde bei Meldungen nur sanft am Arm getippt. Das ist meist so sanft, dass ich mir nicht immer sicher bin, ob die Watch mich angetippt hat, oder nur ein Muskel zuckte. Bisher war es immer die Watch.
* Auf dem iPhone empfange ich nur sehr wenige Meldungen, denke aber nach wenigen Stunden mit der Watch darüber nach, wieder mehr zuzulassen. Für die Watch natürlich. 

### Vermischtes
* Auf dem stillen Örtchen beschäftige ich mich gerne mit meinem iPhone. Wird es als mein Toilettengang-Begleiter durch die Apple Watch ersetzt? Vielleicht. Beim ersten Feldversuch tat der linke (Watch-)Arm nach zwei Minuten bereits weh, weil man ihn etwas drehen muss um den Bildschirm zu sehen. Eventuell ist das aber auch nur eine Frage der Gewöhnung.
* Duschen ist kein Problem, wie erwartet und bereits mehrfach ausführlich von anderen Watch-Besitzern getestet. Wasserfest.
* Nach einer Stunde Crosstrainer kann ich bestätigen, die Pulsmessung ist akkurat und deckt sich mit dem Brustgurt. Wie nicht anders erwartet.
* Auch das Tragen der Uhr beim Sport war angenehm. 
* Der Akku stand zu Beginn bei 60% und sank nach 10 Stunden auf 10%. Sehr wahrscheinlich habe ich mich mit der Watch heute intensiver beschäftigt, als man es im Alltag tun würde.

## Fazit
Schon nach knapp einem Tag ist klar, Apple hat vieles gut durchdacht und richtig umgesetzt. Die Watch fühlt sich **nicht** wie die erste Version einer Produktkategorie an, sondern wesentlich reifer. Man darf gespannt sein, in welche Richtung sich die Technik an unserem Arm weiterentwickelt.    
Die Apps werde ich in den kommenden Tagen weiter erkunden. In der Watch App Liste tauchen allerdings schon Apps auf, von denen ich mir gar nicht vorstellen kann, welche Bereicherung sie am Handgelenk bringen sollen.

[^1]: Meine bleibenden Watch Apps bisher: Wunderlist, Lifesum, 1Password, Authy, Workflow, Peak, Keynote, Day One, CalcBot, DB. Bereits gelöscht: Evernote, Tinder