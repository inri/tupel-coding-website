---
layout: post
title: "Die iPhones 6 & 6 Plus sind zu groß(?)"
date: 2015-04-18
category: it
tags: [iPhone]
published: false
comments: true
share: true
---

Es vergeht kein halbes Jahr mehr, bis eine neue Generation von iPhones das Licht der Welt erblickt. Im September 2015 wird das iPhone 5s sein 2-jähriges und das iPhone 5C sein 3-jähriges bestehen ohne Nachfolger feiern. Und vielleicht bleiben sie auch die letzten iPhone mit einer Bildschirmgröße für Menschen mit normal-großen Händen?

Wenn ich euch sage, dass die iPhones 6 und 6 Plus zu groß sind, fragt ihr mich, woher ich das denn wissen wolle. Schließlich habe ich immer noch ein iPhone 5s. Darauf erwidere ich, dass ihr Recht haben könntet und ich deshalb die folgenden Links zu Meinungen über die Größe der iPhones gesammelt habe.

- [iPhone 6 Plus is still huge](http://www.manton.org/2015/02/iphone-6-plus-is-still-huge.html)

- [Well, I switched to the iPhone 6 Plus](http://www.512pixels.net/blog/2015/4/well-i-switched-to-the-iphone-6-plus)

- [Why I Left My iPhone 6 For an iPhone 5S](http://gizmodo.com/why-i-left-my-iphone-6-for-an-iphone-5s-1664185520)

- [The iPhone 6 is my new phone.](http://sethclifford.me/2015/02/the-iphone-6-is-my-new-phone/)

- [Why I’m trading in my iPhone 6 for a 5s](http://duckrowing.com/2015/02/08/why-im-trading-in-my-iphone-6-for-a-5s/)

- [An iPhone 6 owner lives with the iPhone 6 Plus](http://www.marco.org/2015/04/06/life-with-the-iphone-6-plus)

- [Why I Returned My iPhone 6](http://www.mcelhearn.com/why-i-returned-my-iphone-6/)

- [Two weeks with the iPhone 6 Plus](http://sixcolors.com/post/2015/04/two-weeks-with-the-iphone-6-plus/)

- [No perfect iPhone size](http://www.manton.org/2015/04/no-perfect-iphone-size.html)

Beim Lesen der Titel dürfte aufgefallen sein, dass beide Meinungen vertreten sind. Die einen sagen, die 2014er iPhones sind zu groß, die anderen finden ihre Größe genau richtig. Als erstes ist die Tatsache interessant, dass es nach dem Wechsel vom 3,5- zum 4-Zoll-Dipsplay (iPhone 4S > iPhone 5/5C) so gut wie keine solcher Meinungen zu hören war. Die neue Größe wurde damals, 2013, gelobt. 

Dass es in absehbarer Zeit auch größere iPhones geben würde, war 2014 dann keine Überraschung mehr. Doch dass Apple lediglich größere iPhones als das 5s vorstellen würde und keinen Nachfolger (6s?) mit aktualisierter Hardware, scheint die iPhone-Besitzer zu beschäftigen. Für mich sieht die Lage so aus:

* Verschiedene Display-Größen haben ihre Berechtigung
* 4,7 Zoll sind nicht das Optimum
* Nicht nur ich warte auf einen 5s Nachfolger

Und alles andere ist reine Spekulation…