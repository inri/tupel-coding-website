---
layout: post
title: "Kurz vor dem Apple Event"
date: 2015-09-09
category: it
tags: [Apple, iPhone, iPad]
published: true
comments: false
share: true
---

In weniger als einem halben Tag stellt Apple vermutlich neue iPhones, iPads, Macs und einen neuen Apple TV vor. Das ist eine ganze Menge. 

In den letzten Jahren gab es stets zwei Herbst-Events und [Gruber war sich bis zuletzt](http://daringfireball.net/2015/09/prelude_to_hey_siri_event) nicht sicher, ob sich die Gerüchte um ein iPad Pro bewahrheiten.      
Sehr wahrscheinlich wird es ein langes Event, denn die Neuvorstellungen benötigen viel Zeit. Ich empfand die Zweiteilung der Events nie besonders elegant.

Das Problem daran ist das gemeinsame Betriebssystem von iPhone und iPad: iOS. Die jährliche Aktualisierung folgt nach dem ersten Event, dem iPhone-Event, und das heißt dass die alten iPads in den Händen der Kunden ebenfalls das neue iOS erhalten, mit allen Features. Beim zweiten Event, dem iPad/Mac-Event, steht dann nur noch die Hardware der neuen iPads im Vordergrund, denn die Software wurde einige Wochen vorher entzaubert.      
Dazu kommt, dass auch OS X und iOS immer näher zusammenrücken und einige Features sinnigerweise zusammen vorgestellt werden sollten.      
Also ein Event für alles.

## iPhone

Im Juli [habe ich meine iPhone-Wunschliste vorgestellt](http://ingorichter.org/article/iPhone-Wunschliste/) und bis einen Tag vor dem Event gibt es schon einige Informationen. Gleiches Design, größere Kamera, 16 GB Speicher als Einstieg. Zum schnellen oder sogar kontaktlosen Laden und NFC gibt es keine konkreten Hinweise. Mehr als eine bessere Kamera und den üblichen Upgrades sollte zu erwarten sein.    
Ich beäuge neugierig das 6s Plus. Wieso, weshalb, warum, schreibe ich später.

## iPad

Der Fokus wird auf dem iPad Pro liegen, einem vergrößertem iPad.  Ob ich das besonders interessant finde, weiß ich noch nicht. Ein  iPad Air für Fotobearbeitung fände ich nicht uninteressant, aber ob sich das für den Preis lohnt? Einige Gerüchte gehen davon aus, dass das iPad Pro in der höchsten Konfiguration preislich locker an ein Macbook rankommt. Das aktuelle iPad Air 2 schafft es &quot;nur&quot; auf 809€.      
Gespannt bin ich, ob das iPad Mini 4 so eine große Enttäuschung wird wie letztes Jahr das Mini 3. Hoher Preis, quasi kein signifikantes Upgrade.    

Die iPad-Verkäufe sinken und ob ein Macbook-teures iPad Pro oder ein überteuertes Mini mit alter Hardware die Käufer locken, bleibt abzuwarten.

## Apple TV

Wurde ja auch höchste Zeit.

## Mac

Retina für den 21er iMac, neue Chips in Macbooks?

## iOS / OS X

Siehe WWDC Keynote vom Sommer.