---
layout: post
title: "Das Fotoformat meiner Wahl"
date: 2015-03-28
category: blog
tags: [Fotografie]
published: true
comments: false
share: true
---

Wenn man beginnt sich mit dem Fotografieren zu beschäftigen, stellt sich auch irgendwann die Frage nach dem Format der Fotos. Die einfachste Lösung: Das nutzen, was zu Beginn in der Kamera eingestellt ist. Das ist aber unter Umständen keine gute Idee.

Es gibt mindestens zwei Herangehensweisen um die Frage nach dem *richtigen* Format zu beantworten. 

### Das Motiv bestimmt das Format
Bevor man über das Verhältnis der Seitenlängen spricht, legt man fest ob die Breite länger als die Höhe ist, oder umgekehrt. Ob man also ein Hochformat oder Querformat möchte. Beispielsweise das Porträt einer Person fotografiert man meist im Hochformat, weil es einfach besser aussieht und der menschlichen Statur besser entspricht, siehe die gängigen Passfotos.      

Man kann das weiter spezifizieren und entscheiden, wie breit oder schmal das Bild sein soll. Die unteren drei Bilder zeigen das gleiche Motiv, aber in verschiedenen Formaten. Ich denke man erkennt, dass sich für Landschaften, besonders mit markanten Horizontlinien, nicht selten breite Formate anbieten.  

<figure>
	<a href="/images/2015-format-quer-3-2-2400.jpg">
	<img src="{{ site.url }}/images/2015-format-quer-3-2-800.jpg" alt="Querformat 3:2"></a>
	<figcaption>Querformat in 3:2</figcaption>
</figure>

Beim klassischen 3:2 Format wirkt der Horizont des Wattenmeeres gar nicht richtig. Der graue Himmel nimmt zu viel Fläche ein und die Augen des Betrachters folgen der Spur im Sand.

<figure>
	<a href="/images/2015-format-quer-16-9-2400.jpg">
	<img src="{{ site.url }}/images/2015-format-quer-16-9-800.jpg" alt="Querformat 16:9"></a>
	<figcaption>Querformat in 16:9</figcaption>
</figure>

Im 16:9 Format rückt nun auch der Horizont besser ins Bild und der Steg reicht bis zum goldenen Schnitt. Der Fokus liegt auf den Spuren im Sand und dem Horizont.

<figure>
	<a href="/images/2015-format-quer-16-5-2400.jpg">
	<img src="{{ site.url }}/images/2015-format-quer-16-5-800.jpg" alt="Querformat 16:5"></a>
	<figcaption>Querformat in 16:5</figcaption>
</figure>

Dadurch, dass die Spuren im Sand teils abgeschnitten sind, rückt der Horizont in den Fokus und das 16:5 Format unterstützt diesen Effekt. Ein Verlagern des Horizonts nach unten wäre durchaus auch im 16:9 Format möglich gewesen, wobei man eine große Fläche mit Himmel geschaffen hätte.

Um die beste Wirkung zu erzielen, kann man je nach Motiv das Bildformat anpassen. Es gibt nur ein Problem: Bei den meisten Kameras kann man das Bildformat nicht mit einem Knopfdruck ändern. Zwangsläufig muss man sich für ein Format entscheiden, oder schließt enge Freundschaft mit den Kamera-Einstellungen. Wenn wir davon ausgehen, dass man nicht ständig das Format wechseln möchte, führt das zur zweiten Herangehensweise.

### Das Format bestimmt das Motiv

Bevor man auf den Auslöser drückt, schaut man durch den Sucher oder auf den kleinen Bildschirm der Kamera. Die Vorschau des späteren Fotos betrachtet man im ausgewähltem Format und passt entsprechend den Bildausschnitt an.    

Warum man später nicht einfach zwischen verschiedenen Formaten wechseln kann, zeigt das folgende Foto:

<figure>	
<a href="/images/2015-format-quer-schnitt-1600.jpg">
	<img src="{{ site.url }}/images/2015-format-quer-schnitt-800.jpg" alt="Ein 16:9 aus einem 3:2 Foto">
	<figcaption>Ein 16:9 aus einem 3:2 Foto geschnitten</figcaption></a>
</figure>

Ursprünglich waren sowohl die Palmenblätter, als auch der Stamm komplett auf dem Bild, denn es wurde im 3:2 Format fotografiert. Doch beim Scheiden ins 16:9 Format, mussten diese Bildelemente daran glauben. Auch mein Papa, der im Begriff ist aufzustehen, ist zu weit unten im Bild positioniert.    
Umgekehrt muss man unter Umständen Bildelemente auf der linken oder rechten Seite wegschneiden, wenn man aus einem 16:9 ein 3:2 macht. 

### Meine Wahl: 16:9

Es wäre eine gute Idee, das am meist verbreitetste Bildformat zu wählen. Doch durch welche Kriterien legt man das fest? Ich habe mir die Frage gestellt: Auf welche Art und Weise werden meine Fotos betrachtet? Da ich so gut wie keine Fotos mehr ausdrucke, fallen die analogen Möglichkeiten weg. Es bleiben übrig: PC, MacBook/Notebook, Smartphone, Tablet, TV. Auf so gut wie allen Bildschirmen hat sich das Format 16:9 durchgesetzt[^1].    

Bei mir hat es nach der ersten Panama-Reise klick gemacht. Wir schauten uns die Fotos auf dem Fernseher an und sie waren einfach klein und hatten zwei schwarze Ränder rechts und links. Ich ärgerte mich und begann die Fotos im Format 16:9 zu schneiden. Leider war das Ergebnis nicht immer perfekt, aber die Fotos auf der kompletten Fläche des Fernsehers sehen zu können war großartig. Seitdem nutze ich nur noch 16:9. 

Ich weiß, dass ich damit den tollen Bildsensor meiner Kamera nicht voll ausnutze, aber in der Praxis hilft es ungemein, das spätere Format im Sucher zu haben. Es gibt auch Nachteile, wie z.b. keine guten Fotos im Hochformat machen zu können. Dafür ist 16:9 einfach zu schmal. Allerdings schaut man sich Hochformate sowieso nur selten an (Smartphone und Ausdruck) und auf einem Fernseher sehen sie geradezu lächerlich winzig aus. 

Welches Format das *richtige* für euch ist, bleibt euch überlassen. Sicher ist allerdings, dass das 16:9 Format nicht so schnell verschwinden wird. 

Zum Abschluss ein weiteres Beispiel, wie die äußeren Rahmenbedingung das Fotoformat beeinflussen und wieso es nicht unbedingt gut aussieht, wenn man sich nicht daran hält.

#### Beispiel: Instagram

Instagram ist ein soziales Netzwerk, auf welchem man Fotos hochlädt. Man kann bei einem Upload ein Foto aus dem eigenen Foto-Ordner auswählen, oder direkt mit Instagram ein Foto aufnehmen. Bei letzteren fotografiert man im 1:1 Format, also ein Quadrat. Und tatsächlich ist das Design der Instagram App auf quadratische Fotos ausgelegt. Wenn man ein Foto im Format 16:9 hochlädt, setzt Instagram über und unter dem Foto weiße Streifen. Auch ein Porträt-Foto bekommt nicht die gesamte Breite, sondern links und rechts weiße Streifen.

Man kann jetzt über Sinn und Unsinn streiten, aber aktuell sieht das Design von Instagram so aus. Ich würde deshalb niemals ein nicht-quadratisches Foto auf Instagram laden. Im Hochformat sieht es nur etwas seltsam aus, aber besonders im Querformat wirken die Fotos überhaupt nicht. Machen kann man es aber trotzdem.    

[^1]: Die meisten PC-Monitore gibt es nur noch im Format: 16:9. MacBooks und Notebooks haben Bildschirme im Format: 16:9. Seit dem iPhone 5 und viele Android Smartphones hatten schon früher das Format: 16:9. Die gängigen TV-Geräte kommen im Format: 16:9. Das iPad bildet eine Ausnahme, aber viele Android Tablets haben auch das Format: 16:9. 