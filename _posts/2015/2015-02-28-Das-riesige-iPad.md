---
layout: post
title: "Das riesige iPad"
date: 2015-02-28
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Ich besaß für zwei Jahre ein iPad 2, bevor ich es 2012 durch ein iPad Mini der ersten Generation ersetzte. Ich mag das Mini noch immer sehr, besonders seine kompakte Größe. Mich hat aber auch das iPad Air 2 beeindruckt und ich überlegte, ob es nicht nach fast drei Jahren an der Zeit wäre, wieder zu einem größeren iPad zu wechseln. 

Ich dachte über einen Wechsel zu einem der neuen Retina Minis nach, aber das 2014er Update war mir einfach zu mau. Eine schlechte Performance bei iOS 9 im kommenden Jahr ist fast sicher. Wer will das schon? Dass es quasi fast baugleich zum 2013er Modell ist, mag auch eine Rolle spielen. Die ca. 8 Zoll des Minis sind meist sehr angenehm, aber manchmal wünscht man sich eben doch mehr. Beispielsweise Fotobearbeitung auf dem iPad würde ich gerne ausprobieren. Das lohnt sich mit dem non-Retina Mini gar nicht. Unterwegs ein Film schauen wäre in Groß auch besser. Skizzen und Notizen hätten auch mehr Platz auf dem Bildschirm. Zusammengefasst: Warum nicht wieder ein großes iPad?

## Besuch im Apple Store Berlin

Ja, ich nutzte zwei Jahre lang ein im Vergleich zum aktuellen iPad Air 2 relativ klobiges iPad 2 und müsste wissen, wie sich 9,7 Zoll anfühlen. Doch ich war gerade in Berlin und spazierte den Kurfürstendamm entlang. Wieso also nicht in den Apple Store gehen und sich das gute Teil einfach mal anschauen? Gedacht, getan.      

&quot;*Wieso sind die so groß? Das iPad Pro ist doch nur ein Gerücht?*&quot; - Das war mein erster Gedanke, als ich an den Tisch mit den iPads herantrat. Ich nahm ein Gerät in die Hand und staunte nicht schlecht. Im Vergleich zum Mini war es geradezu monströs. Nach der Vorstellung des iPad Air (2013), hatte ich irgendwo gelesen, dass es zwar auf dem Papier nur einige Gramm leichter als sein Vorgänger ist (600g vs. 470g), aber sich durch das dünne Gehäuse (8,8mm vs. 7,5mm) wesentlich leichter anfühlt. Beim nochmals dünnerem und leichteren iPad Air 2 (440g &amp; 6,1mm) wurden Vergleiche zum dickeren, aber leichteren iPad Mini gezogen und dass man kaum einen Unterschied merke. Davon kann aber meiner Meinung nach keine Rede sein!   

Nach der langen Zeit mit dem Mini, fühlt sich das Air 2 sehr groß und auch definitiv schwerer an, obwohl es nur 130 Gramm schwerer ist. Und da stellt sich auch die Frage: Wer würde ein iPad Pro mit mehr als 12 Zoll wollen?    
Ich bin mir nach meinem Besuch im Apple Store nicht mehr so sicher, ob ein Wechsel zurück zum großen iPad eine gute Idee ist. Definitiv jetzt noch nicht. In einigen Monaten gibt es womöglich ein noch dünneres und leichteres iPad Air 3. Dann denke ich noch einmal drüber nach.