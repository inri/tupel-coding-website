---
layout: post
title: "Apple Watch adé und ein Fazit"
date: 2015-05-28
category: it
tags: [Apple Watch]
published: true
comments: false
share: true
---

Es folgen weitere Beobachtungen aus dem Alltag mit einer Apple Watch.

## Vorher…

* Die Watch ca. 9 Stunden **nicht** zu tragen (z.b. Schlafen), verbraucht ungefähr 13% Akkukapazität. 
* Die Ladezeit ist relativ kurz. Die Apple Watch benötigt also nicht die komplette Nacht zum Laden. Tragen möchte man sie im Schlaf jedoch auch nicht. 
* Meine Watch hatte am Ende eines Tages stets mehr als 30% verbleibende Akkukapazität. Entweder nutzte ich sie einfach zu wenig und müsste mehr Notifications auf die Watch leiten. Oder die kleinere 38mm-Version mit kleinerem Akku würde mir auch ausreichen. Ob sie mir gut steht?

* Die Watch achtet ja darauf, dass ihr Träger 12x am Tag innerhalb einer Stunde mindestens 1 Minute lang steht. Die Aufforderung erhielt ich aber nur sehr selten, weil ich scheinbar relativ oft aufstehe. Mir wäre es lieber, wenn ich diese 1 Minute auf z.b. 5 Minuten verlängern könnte. Oder noch mehr. Genauso auch die Trainingsdauer (30 Minuten), also Aktivitäten mit erhöhtem Puls. Leider kann man nur die normalen Aktivitätskalorien (für normale Bewegung) anpassen. 

* Meine am häufigsten benutzten Apps, exklusive Uhrzeit: Wetter, Timer, Workout und Nachrichten/Emails via Notifications.

* Ich habe 4 Erfolge in der Aktivität App errungen, aber wurde nie  benachrichtigt. Vielleicht ist das so gewollt, oder ich habe es übersehen.

* Am Ende eines Tages: Benutzung 3 Stunden vs. Standby von 15 Stunden. Grandios. Das Akku-Management hat Apple sehr gut hinbekommen. Auf meinem iPhone funktioniert das leider nicht.

* Beim Sport ist die Uhr sehr praktisch, weil man schnell nützliche Infos erhält und einfache Aktionen ausführen kann (z.b. zurückgelegte Strecke einsehen oder zum nächsten Song wechseln). Wenn man stattdessen das iPhone benutzt, hofft man erst einmal, dass der schweißige Daumen vom Touch ID Sensor akzeptiert wird (klappt meistens). Den Watch-Arm ein wenig zu heben geht trotzdem schneller. Beim Outdoor Sport, wenn das iPhone sich in einer Hülle befindet, ist es ganz vorbei. Meine Fahrradhalterung ist klasse, aber ich muss jedes Mal den Pin-Code eintippen. In der gummierten Hülle zum Joggen wurde die Touch-Bedienung sehr ungenau und machte keinen Spaß.

* Bei drei Bluetooth-Geräten ist das iPhone etwas überfordert. Beim Training trug ich neben der Watch auch einen Brustgurt und hörte Musik über eine UE Boom. Dabei stotterte die Musik ca. einmal pro Minute kurz. Sehr nervig. Ich las auch schon einen Bericht, in welchem ebenfalls von Verzögerungen zwischen Watch und Bluetooth-Kopfhörern die Rede war.

* Neben ganz guten Apps, die man aber nur selten nutzt, gibt es auch einige wirklich sinnlose, wie die von Amazon zum Beispiel. Wer möchte nicht gerne auf einem winzigen Bildschirm via Siri nach Produkten suchen und diese kaufen? App-Fazit: Es ist noch Luft nach oben.

* Viele Apps benötigen einige Sekunden zum Laden, auch Apps von Apple selbst. Woran das genau liegt, ist noch nicht ganz klar. Das erste Update für die Watch konnte ich in dieser Hinsicht noch nicht ausgiebig genug testen. Evtl. ist der S1 Chip einfach nicht stark genug.

## Nachher… und ein Fazit

Vor etwas über zwei Wochen öffnete ich das Paket mit meiner Apple Watch darin. Vor zwei Tagen öffnete ein Mitarbeiter bei Apple in den Niederlanden dasselbe Paket und kontrollierte den Inhalt. Ich habe meine Watch wieder zurückgeschickt und vermisse sie. Wieso, weshalb, warum?
 
Ich versuchte die Apple Watch danach zu bewerten, welchen Mehrwert sie mir persönlich bringt und in welchem Verhältnis dieser zum Preis steht [^1]. Die Apple Watch versüßte mir jeden Tag und sicherlich wird die aktuell noch überschaubare Anzahl an nützlichen Funktionen mit der Zeit wachsen. Auch als moderne Armbanduhr sieht die Watch wirklich gut aus, ist klasse verarbeitet und fühlt sich auch am Arm toll an.      
Aber das alles reicht mir persönlich für 450€ nicht aus. Ein Großteil des Mehrwerts liegt definitiv im Fashion-Faktor und das hat Apple auch stets so kommuniziert. Nicht selten hört man vom *Prestige-Objekt* iPhone und das mag teilweise auch stimmen. Doch die Apple Watch ist aktuell viel mehr Prestige als das iPhone. Oder anders gesagt: Wer das Potential seines iPhone nicht ausschöpft, ist in erster Linie selbst daran Schuld. Dagegen das Potential der Watch beginnt sich erst zu entfalten, aber das bedeutet eben auch, dass man mit gewissen Limitierungen leben muss. 

Fashion ist mir nicht völlig egal, aber ich bezahle nicht jeden Preis dafür. Trotzdem vermisse ich die Watch. Nicht so sehr, dass ich ständig einen Phantomschmerz am linken Arm verspüre oder ihn 20x am Tag hebe um überrascht auf meine Armhaare zu starren. Doch besonders die Aktivitäten-Funktionen vermisse ich. Man hat stets einen Überblick darüber, wie viel Sport und Bewegung noch zu absolvieren sind und dabei war die Watch nicht penetrant. Sie spornte mich an.       
Nicht zuletzt waren es auch kleine Dinge, die in Summe das Benutzen der Watch vergnüglich machten: Schnell einen Timer stellen, zum achten Mal am Tag das Wetter checken, oder eben mit der Wunderlist einkaufen. Wenn *Tweetbot* schon eine Watch-Erweiterung gehabt hätte, mein Akku wäre am Ende eines Tag sicherlich unter 30% gewesen.

Kurz gesagt: Die Watch fühlt sich nicht wie die erste Generation einer neuen Geräteklasse an. Die Kritik ist überschaubar und die Weiterentwicklung wird selbstredend enorm schnell voranschreiten. Die Watch wird im kommenden Jahr ihren Preis behalten und dann wird er auch mehr gerechtfertigt sein als zur Zeit noch. Zumindest für mich.


[^1]: Nehmen wir mal als Beispiel das iPhone, dessen Preis bei 700€ beginnt. Es gibt billigere Smartphones, aber das iPhone 4S und das iPhone 5s sind für mich jeden einzelnen Euro wert. Es gibt kein technisches Gerät in meinem Haushalt, welches ich intensiver nutze. Auf Platz 2 steht das MacBook. Aber das iPhone ist mein treuer Begleiter. Bei einer Nutzung von 2 Jahren verteilen sich die 700€ für ein iPhone auf *nur* 1€ pro Tag. Für das iPhone 5s mit 64GB (Neupreis 900€) würde ich zur Zeit sogar noch 350€ bei reBuy bekommen. Ein Verkauf lohnt sich und würde die Kosten pro Tag nochmals senken würde.