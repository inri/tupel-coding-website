---
layout: post
title: "iPhone 4S/5s und Akku"
date: 2015-04-07
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Lange Zeit war es für einen iPhone-Besitzer kaum möglich Informationen zum verbauten Akku zu erhalten. Apps für das iPhone können diese Funktionalität nicht bieten. 

Eine Möglichkeit ist der Besuch in einem Apple Store. Da in Deutschland deren Verbreitung allerdings dürftig ist, hilft ein Verweis auf die Mac App [coconutBattery](http://www.coconut-flavour.com) weiter. Bis zur Version 3.2 informierte sie *nur* über den Akku im MacBook, kann neuerdings aber auch iPhone und iPad Akkus auswerten. 

### iPhone 4S

Ende 2013 gab ich mein iPhone 4S an meine Freundin weiter. So sehr sie sich auch über ein gutes Smartphone freute (Vorgänger war ein [HTC Wildfire](https://www.google.de/search?q=htc+wildfire&client=safari&rls=en&source=lnms&tbm=isch&sa=X&ei=keYiVbKwOIa07gbNq4HwBg&ved=0CAcQ_AUoAQ&biw=1440&bih=788)), ärgerte sie sich über die geringe Akkulaufzeit. Ich optimierte zwar das 4S und gab Tips für die Benutzung, aber es besserte sich nicht. Ein Jahr später, Ende 2014, hielt es kaum noch einen halben Tag durch, obwohl sie es nicht intensiv benutzte. Die Lösung: Der Austausch des Akkus.   

Ich bestellte für 25€ bei [ifixit](https://www.ifixit.com/iPhone-Parts/iPhone-4S-Replacement-Battery/IF115-005-2) einen neuen Akku und tauschte ihn ohne Schwierigkeiten aus. Das Ergebnis war eine sehr zufriedene Freundin, denn der Akku hielt nun locker wieder einen Tag, auch mit häufiger Benutzung.     
Ich begann über mein iPhone 5s nachzudenken.

### iPhone 5s

Wenn ich mal länger unterwegs bin, ist ein externer Akku mein Begleiter. Ich benötige ihn nicht oft und zwingend, aber er gibt mir eine gewisse Sicherheit.      
An Ostern hatte ich ihn leider nicht dabei. An einem Tag wollten wir das Fliegen mit der AR.Drone üben und den Spaß natürlich Filmen. Der Akku des 5s stand zu Beginn bei ca. 80%. Es war kühl und sehr windig, das iPhone kühlte im Verlauf von einer knappen Viertelstunde auf vielleicht 5° ab, wobei ich es in den Händen hielt und versuchte es vor dem Wind zu schützen. Ich filmte ca. 10 Minuten und dann verabschiedete sich das iPhone. Zurück im Warmen schaute ich auf den Akkustand und war erstaunt: 60%. 

Nach knapp zwei Jahren bemerkte ich diesen Effekt auch schon beim 4S. Auch wenn der Akkustand deutlich mehr als 15% auswies, beschloss das iPhone den Dienst zu quittieren. Am Osterwochende ist mir das mit dem 5s und in Verbindung mit geringen Temperaturen insgesamt drei Mal passiert. Und der Akku war deutlich über 30%.

### Zahlen von coconutBattery

- iPhone 5s nach 542 Ladezyklen: 78,4% der ursprünglichen Designkapazität von 1550mAh [^1] (Maximum Charge [^2]: 1121-1543mAh)

Wie soll man diese Zahlen nun bewerten? Ich schaute mir den neuen Akku des 4S an und baute schnell mal den alten Akku ein. Das Ergebnis überraschte mich.

- iPhone 4S nach 925 Ladezyklen: 78,5% der ursprünglichen Designkapazität von 1430mAh (Maximum Charge: 1122-1220mAh)

- iPhone 4S nach 55 Ladezyklen: >100% der ursprünglichen Designkapazität von 1430mAh (Maximum Charge: 1461-1511mAh)

Es fallen verschiedene Dinge auf:

1. Der neue Akku hat eine höhere Kapazität als die Designkapazität (1430mAh vs. 1511mAh)
2. Obwohl der alte Akku im 4S im Alltag eine deutlich schlechtere Figur machte, hat er eine fast identische %-Kapazität als mein jüngeres 5s (78,5% vs. 78,4%)
3. Trotz gleicher %-Kapazität ist der bis-Wert des Maximum Charge  beim 5s höher als beim 4S (98,8% von 1550mAh va. 85% von 1430mAh)

Punkt 1 ist leicht zu erklären: Glück gehabt. Die Kapazität von Akkus kann man nicht exakt abmessen und deshalb ist der neue Akku eben etwas größer.     
Punkt 2 würde ich mit nicht ganz korrekten %-Kapazitäten erklären. Da der alte Akku des 4S nicht mehr im Dienst ist, kann ich eine Veränderung nur beim 5s protokollieren [^4].    
Punkt 3 unterstützt die These von Punkt 2. 

Was tun, wenn der Akku meines 5s sich weiterhin verschlechtert? Es besitzt hat zwar Apple Care, aber das greift leider noch nicht [^3]. Es wird wohl auf eine weitere Bestellung bei ifixit hinauslaufen, die 30€ sind es auf jeden Fall wert.

**Nachtrag 28.04.15:** Heute Morgen nach dem Aufwachen blätterte ich auf dem iPhone durch meine Emails, als es plötzlich aus ging. Es war am Abend zuvor nicht komplett aufgeladen und über Nacht hing ich es auch nicht ran. Nachdem es nun wieder am Strom war und neu startete, erstaunten mich die verbliebenen 39% Akku doch sehr. Im Zimmer war es nicht besonders kalt, vielleicht 18 Grad.     
Die aktuellen Daten von CoconutBattery sind regelrecht beeindruckend: 

* iPhone 5s nach 561 Ladezyklen: 87,7% der ursprünglichen Designkapazität von 1550mAh (Maximum Charge: 1351-1552mAh). 

Es hat fast 10% Akkukapazität wiederhergestellt. Wie? Ich weiß es nicht.


[^1]: Die Designkapazität ist die theoretische Größe des Akkus. Bei einem 5s sind das bspw. 1550mAh. Ein nagelneues iPhone 5s sollte plus/minus einige mAh diese Kapazität besitzen. Mit der Zeit verliert ein Akku an Kapazität und wenn es von den ursprünglichen 1550mAh nur noch 1130 hat, sind das 73% der ursprünglichen Designkapazität.

[^2]: Diese Kapazität kann der Akku aktuell erreichen. Der von-Wert ist der schlechteste Wert und ziemlich genau der berechnete Wert der verbliebenen Kapazität. Der bis-Wert ist nur unter perfekten Bedingungen zu erreichen. Generell sollte der tatsächliche Wert höher als der von-Wert sein.

[^3]: Erst wenn die Akku-Kapazität unter 50% der Designkapazität liegt tauscht Apple den Akku aus.

[^4]: Die erste Messung betrug 72,6%.