---
layout: post
title: "Sinkende Technikbegeisterung im Zug"
date: 2015-04-17
category: it
tags: [Reisen, Technologie]
published: true
comments: false
share: true
---

Um per Bahn von Halle in die Heimat zu fahren, sitze ich die erste Hälfte der Strecke in einem Zug des MDV (nicht DB). 

Oft kaufe ich mein Fahrschein über die DB App und halte den Zugbegleitern einen QR-Code auf meinem iPhone-Display entgegen. Bei der DB scannt man diesen Code und hat alle Informationen zu meinem Fahrschein. Bis Sommer 2014 konnte man beim MDV mein QR-Fahrschein aber nicht elektronisch verarbeiten. Die netten Zugbegleiterinnen mussten die Fahrschein-Informationen mühsam lesen. Oft hörte ich in den Frühlingsmonaten die freudige Erwartung, dass man bald neue Lesegeräte bekommt und dann auch die QR-Fahrschein der DB verarbeiten kann.

So geschah es auch. In den ersten Wochen merkte man ihnen den Spaß beim Herausholen des Geräts und Einscannen des QR-Codes wirklich an. Das hielt ein gutes halbes Jahr. Die letzten Male begnügten sie sich wieder mit der manuellen Lesemethode (Augen). Eventuell sind die Geräte auch nur defekt, doch das hat man mir im Sommer immerhin noch entschuldigend mitgeteilt.

Aber auch bei der DB interessieren sich die Zugbegleiter nicht immer für meinen QR-Code und winken ab.    
Ich glaube der herkömmliche Fahrschein wäre ihnen am liebsten.