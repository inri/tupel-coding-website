---
layout: post
title: "Panama 2: Panama City"
date: 2015-01-08
category: reisen
tags: [Reisen, Panama]
published: true
comments: false
share: true
image: 
  feature: 2015-panama-title-skyline-lg.jpg 
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Am späten Abend fuhren wir vom Flughafen direkt nach Panama City. Zwar befand sich unser Beach Resort für die restliche Woche süd-westlich der Hauptstadt, aber es war schon zu spät um noch dahin zu fahren.

Das ist der zweite Teil einer Reihe über meine kurze Reise nach Panama im März 2014. [Teil 1](http://ingorichter.org/article/Panama-Hinflug/), [Teil 3](http://ingorichter.org/article/Panama-Bijao-Beach/), [Teil 4](http://ingorichter.org/article/Panama-Panamakanal/), [Teil 5](http://ingorichter.org/article/Panama-Abschluss/)

## Vorwort

Bevor es in das Hotel für eine Nacht ging, konnten wir auf der Fahrt noch die nächtliche Skyline von Panama City bestaunen. Doch wie sehr die Stadt von Wolkenkratzern durchsetzt sein würde, ahnten wir nicht.

Vom 23. Stock unseres Hotelzimmers hatten wir einen grandiosen Blick und konnten am Morgen beobachten, wie die Hochhäuser Stück für Stück von der aufgehenden Sonne leuchteten.     
Nach einer Nacht im klimatisiertem Zimmer dachte ich schon gar nicht mehr an das tropische Klima. Die Temperaturen waren jetzt schon höher als in der Nacht zuvor, die hohe Luftfeuchtigkeit dennoch deutlich spürbar. 

<figure>
	<img src="{{ site.url }}/images/2015-panama-11-city.jpg" alt="Wolkenkratzer in Panama City">
	<figcaption>Ein Wohnhaus für die gutbetuchteren Einwohner</figcaption>
</figure>

Für eine ausführliche Erkundung der Hauptstadt war leider keine Zeit. Am ersten Tag ging es für uns direkt zum Bijao Beach Resort und am letzten Tag machten wir auf dem Weg zum Flughafen eine kleine Tour durch die Rote Zone. Alle Fotos sind leider *nur* Schnappschüsse aus dem fahrenden Auto. 

### Red Zone

Es gibt auch ärmere Gegenden in Panama City. Diese Gebiete nannten unsere Gastgeber die *Red Zone* und das bedeutet so viel wie dass man als Tourist einen Bogen darum machen sollte. Zu Fuß waren wir deshalb nicht unterwegs, aber aus dem Auto heraus empfand ich das Stadtviertel nicht besonders spektakulär. Ja, die Häuser sind verfallen oder gar nicht fertiggestellt und man sieht hier und da nicht die vertrauenswürdigsten Gesichter. Doch es waren auf den Straßen viele Menschen unterwegs und darunter auch Touristen.

<figure>
	<img src="{{ site.url }}/images/2015-panama-12-poor.jpg" alt="Unfertiges Wohnhaus in Panama City">
	<figcaption>Ein unfertiges Wohnhaus bewohnt von ärmeren Menschen</figcaption>
</figure>

Die Armenviertel in Panama City werden nach und nach von großen Bauvorhaben verdrängt und so wächst die Armut ins Land hinein. Wir sind noch durch viele kleine *Ortschaften* gefahren, der dritten Welt näher als unserer.

Ich hoffe bei einem zukünftigen Besuch in Panama bleibt uns mehr  Zeit um die Stadt zu erkunden.

[Auf Flickr gibt es alle Bilder von der Reise.](https://flic.kr/s/aHsk7niJD9)