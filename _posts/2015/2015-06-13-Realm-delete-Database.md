---
layout: post
title: "Realm (0.82.1) delete complete Database"
date: 2015-06-13
category: it
tags: [Coding, Java, Android]
published: true
comments: false
share: true
---

If you are like me and don’t like to work with SQLite directly when programming for Android or iOS, you could be very happy to discover [Realm](https://realm.io). So grab some information and come back here if you did some starter tutorials. I’ll waiting. 

At some point you might ask what is the best way to delete the whole database.     
In an earlier version of this article I described a method with clearing the complete class. But you have to check if the class exists, so the whole code gets bloated.     

A better way is to clear the matches of a query (namely RealmResults).   

	Real realm = Realm.getDefaultInstance();

	realm.beginTransaction();

	realm.where(ExampleClass.class).findAll().clear();
	
	mRealm.commitTransaction();

	mRealm.close();

So its pretty easy to delete the whole database by querying all elements of every class and clear all these matches.