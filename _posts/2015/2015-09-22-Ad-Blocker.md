---
layout: post
title: "Ad-Blocker gegen Werbung auf Webseiten"
date: 2015-09-22
category: it
tags: [Web, iOS]
published: true
comments: false
share: true
---

Mein Senf zur aktuellen Diskussion (&quot;*Sind Ad-Blocker gut oder böse oder kommt es drauf an?*&quot;) darf natürlich nicht fehlen. Spoiler: Ich hasse Werbung.     

Also:  
  
- Ich verstehe, dass viele Menschen von Werbung in Internet genervt sind und sie deshalb blocken.     
- Ich verstehe, dass Werbung im Internet eine wichtige Einnahmequelle für Betreiber von Webseiten ist.     
- Ich verstehe, dass Werbung nicht gleich Werbung ist, sondern es durchaus ästhetische Unterschiede gibt und ein Banner weniger störend sein kann als ein anderer.      
- Ich verstehe auch, dass ein [Marco Arment](http://www.marco.org/2015/09/18/just-doesnt-feel-good) sich in einer moralischen Zwickmühle befindet, wenn er einerseits einen (erfolgreichen) mobilen Ad-Blocker baut, der jede Art von Werbung blockt, und andererseits Kumpels hat, die auf die Einnahmen der nicht-störenden Werbung auf ihren Webseiten angewiesen sind. 

**Trotzdem ist mir das alles egal. Ich nutze auf all meinen internetfähigen Geräten (Mac, iPhone, PC) Ad-Blocker. Ich finde Werbung störend und habe keinerlei Verständnis, mag sie noch so dezent sein.**

Damit man mich nicht falsch versteht, ich empfinde Werbung auch im *Real Life* als Belästigung. Egal ob in Zeitschriften, für die ich bezahle, im Fernsehprogramm, für das ich HD-Gebühren und GEZ hinblättern muss, oder auf Plakatwänden, nach denen mich keiner gefragt hat.       
Wenn ich könnte, ich würde Werbung für mich überall ausblenden. 

Fragt mich **nicht** nach einer besseren Methode um Geld mit Webseiten zu verdienen. Das ist weder meine Aufgabe, noch mein Ziel.     
Seid kreativ und habt keine Angst Neues auszuprobieren.