---
layout: post
title: "iPhone 2015 Wunschliste"
date: 2015-07-19
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Alle Jahre dasselbe Spiel. Ich stelle mir völlig utopische Neuerungen beim kommenden iPhone vor, bin dann enttäuscht, wenn sie kaum umgesetzt werden, um kurz darauf doch vom neuen iPhone begeistert zu sein. Wie blöd bin ich eigentlich...

#### Lightning vs. USB-C oder Schnelles Laden

Dass zukünftige iPhones mehr Akkukapazität erhalten werden, ist eher unwahrscheinlich. Bestenfalls in Form von neuer Batterietechnologie (mehr Kapazität bei gleicher Größe) können wir damit rechnen, aber wohl kaum in 2015. Wesentlich dicker wird das iPhone nicht mehr, so viel ist sicher. Eine Lösung für das Problem der zu geringen Kapazität und daraus resultierender Benutzungsdauer, wäre ein schnelles Aufladen von Akkus. Wer brachte mich auf die Idee? Die Apple Watch.

Die Watch hat einen sehr kleinen Akku und lädt selbst über kabelloses Laden sehr schnell. Wenn man das iPhone innerhalb von 20 Minuten um beispielsweise 75% aufladen könnte, würde auch eine geringe Gesamtkapazität kaum noch stören. Einmal am Tag kurz am Strom und schon gibt es kaum noch Gejammer.      
Der neue USB-C Standard soll in Zukunft ein wesentlich schnelleres Laden von Akkus ermöglichen (Google sagt 3 bis 5-mal schneller). Ich denke nicht dass Apple dieses Jahr bei iPhones und iPads auf USB-C wechselt, aber früher oder später wird dieser Schritt vermutlich kommen. Dann gäbe es vom iPhone bis zum MacBook einen universellen Anschluss und hoffentlich auch schnelles Laden.

#### Kontaktloses Aufladen

Wo wir gerade beim Laden des Akkus waren, wieso nicht die Möglichkeit schaffen das iPhone auch kontaktlos laden zu lassen? Die Apple Watch kann das und sie lässt sich auch durch Systeme anderer Hersteller laden.      
An meinem Nachttisch hängt irgendwo ein Lightning-Kabel, aber ich schließe das iPhone nur sehr selten dort an. Das Kabel einzustecken ist einfach zu viel Aufwand und das Dock meiner Wahl steht auf dem Schreibtisch. Eigentlich bräuchte ich 2 von der Sorte...

Das immer wieder zu hörende Argument *Die anderen können es doch auch und zwar schon eine ganze Weile!* finde ich nicht in allen Fällen valide. Nicht alles was man technisch machen kann, sollte man auch sofort machen. Insgesamt wäre aber kontaktloses Laden eine nette neuerung und würde den Komfort deutlich steigern.  

#### NFC (API)

Bleiben wir gleich mal beim *Die anderen können es doch auch und zwar schon eine ganze Weile!* und widmen uns dem NFC. Seit dem letzten Jahr kann das iPhone NFC, denn es wird in Apple Pay verwendet. Für Entwickler gibt es aber keine API und das obwohl es durchaus so einige interessante Anwendungsmöglichkeiten gäbe.

Ich habe mich vor einiger Zeit mit NFC bei Android beschäftigt und der Stand war ziemlich gruselig. NFC gibt es schon eine ganze Weile, aber erst seit vielleicht 1 bis 2 Jahren gibt es Geräte, die auch nützliches können (wie z.b. einen NFC Tag emulieren). Der Android API fehlt aber der richtige Kick und das, was aktuell möglich ist, ist eher peinlich (Stichwort Beam). 

Apple, zeig uns wie es geht! 

#### Mehr als 8 MP Kamera

Das iPhone hat seit Jahren in Sachen Fotografie die Nase deutlich vor den Android-Konkurrenten. Der Grund ist im besonderen nicht die Hardware, denn längst übertrumpfen sich Samsung und HTC mit Megapixeln jenseits der 8 Megapixel, die das iPhone vorzuweisen hat. Nein, die gute Software von Apple (Stichwort RAW-Verarbeitung) ist der ausschlaggebende Punkt. Trotzdem ist es nun langsam an der Zeit auch mal etwas größere Bilder mit dem iPhone knipsen zu können. Mir geht es dabei wirklich nur um die Größe der Bilder und den damit verbundenen Möglichkeiten. 

Gruber meint, dass eine solch großer Sprung bei der Kamera eher 2016 zu erwarten ist, beim komplett überarbeiteten iPhone. Aber das 4S bekam auch erstmalig mehr Megapixel als sein Vorgänger und letztes Jahr bekam nur das 6+ ein Bildstabilisator. Was soll dieses Jahr die Neuerung sein? Wieder ein angeblich besserer Sensor? So what? Ich finde, ein Kamera-Upgrade würde dieses Jahr sehr wohl passen.

Im gleichem Atemzug kann man auch über 4K-Videos nachdenken, aber das wird vor 2016 oder sogar 2018 womöglich gar nicht wichtig.

#### 32 GB als Einstieg

Letztes Jahrs schüttelte nicht nur ich verwundert meinen Kopf über die 16 GB Speicherkapazität bei allen iPhones. Womöglich weiß Apple mehr als wir und für die meisten Käufer ist das ausreichend Speicherplatz. Trotzdem gab es Berichte darüber, dass einige Nutzer keinen Platz für das iOS 8 Update hatten. In Zukunft sollen Updates kleiner und speziell komprimiert werden, aber ob das die Lösung ist?      
Der nächste Sprung sind 64 GB, also das 4-fache, wieso also nicht die 16 GB komplett herausschmeißen? Ist jeder Cent Einsparung wichtiger als die Reputation nur die besten Produkte herzustellen? (Hinweis: Nein.)

#### Ein dünneres iPhone

Nein, ist nur Spaß. Oder...?