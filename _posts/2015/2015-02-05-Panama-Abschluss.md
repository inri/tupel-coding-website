---
layout: post
title: "Panama 5: Abschließende Eindrücke"
date: 2015-02-05
category: reisen
tags: [Reisen, Panama]
published: true
comments: false
share: true
image: 
  feature: 2015-panama-title-beach-lg.jpg 
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Panama fand ich ziemlich beeindruckend, doch leider kann ich all die neuen Eindrücke nicht so beschreiben, wie sie es verdient hätten. Und mittlerweile ist auch fast ein Jahr vergangen und der erste Eindruck sowieso verblasst.

Das ist der fünfte Teil einer Reihe über meine kurze Reise nach Panama im März 2014. [Teil 1](http://ingorichter.org/article/Panama-Hinflug/), [Teil 2](http://ingorichter.org/article/Panama-City/), [Teil 3](http://ingorichter.org/article/Panama-Bijao-Beach/), [Teil 4](http://ingorichter.org/article/Panama-Panamakanal/)

<figure>
	<img src="{{ site.url }}/images/2015-panama-19-child
.jpg" alt="Kind auf dem Arm">
	<figcaption>Auch Kinder schauen skeptisch</figcaption>
</figure>

Da wären zum Beispiel die Menschen, die Panameos. Die sind erst einmal kleiner als wir. Das musste ich auch feststellen als ich auf der Suche nach Flip Flops war und sich kaum etwas über Schuhgröße 42 finden ließ. Wir hatten ansonsten eher wenig direkten Kontakt mit Panameos und erfuhren nur durch unsere Betreuer vor Ort etwas über sie. Die Menschen in Panama sind einfach, aber ehrlich. Meine Freundin würde sie als *dropsig* beschreiben.

<figure>
	<img src="{{ site.url }}/images/2015-panama-18-gameboy.jpg" alt="Junge spielt mit GameBoy">
	<figcaption>Junge spielt unter einer Markttheke mit seinem Game Boy</figcaption>
</figure>

Die Bildung der breiten Bevölkerungsschicht ist in Panama nicht besonders gut. Nach der Schule ist meist schon Schluss. Die meisten Panameos gehen einfachen Berufen wie Automechaniker und Elektriker nach und haben diese nicht im Rahmen einer Ausbildung gelernt, wie es in Deutschland üblich ist. Man beschließt etwas zu tun und sucht sich jemand, der einem das mehr oder weniger beibringt. Und irgendwann kann man es, oder auch nicht so richtig. Läuft irgendwie.     
Dementsprechend ist die Armut besonders außerhalb der Großstädte hoch, was man deutlich an den *Häusern* sehen kann. Wir schauten uns mal eine kleine Siedlung näher an, die sich direkt am Strand und quasi im Schatten einer Hotelanlage befand. Da trafen zwei völlig unterschiedliche Welten aufeinander. Nach und nach werden die Strandgrundstücke verkauft und die Bevölkerung von dort vertrieben. Alles für den Tourismus.

<figure>
	<img src="{{ site.url }}/images/2015-panama-20-car.jpg" alt="Panama Auto">
	<figcaption>Außerhalb der Großstädte sehen nicht wenige Autos so aus</figcaption>
</figure>

### Flora und Fauna

Erst einmal sieht in Panama alles grüner und lebendiger aus, trotz der brütenden Hitze. Und dann sage ich einfach mal: **Palmen!** Palmen sind toll und wenn ein paar Kokosnüsse dran hängen, was will man mehr?    
Es gibt auch definitiv mehr Obstbäume. Nicht unbedingt in der Stadt, aber außerhalb sieht man viel Obst, welches bei uns sonst in der exotischen Abteilung zu finden ist.      
Auch auf den Märkten kann man sich die heimischen Obstsorten genauer anschauen und staunt nicht schlecht, wenn eine riesige Bananenstaude nur 3$ kosten soll oder die Ananas eine andere Farbe als bei uns hat. 

<figure>
	<img src="{{ site.url }}/images/2015-panama-17-fruits.jpg" alt="Obst- und Gemüsestand">
	<figcaption>Obst und Gemüse auf dem Markt</figcaption>
</figure>

Exotische Tiere sind fast genauso toll wie exotisches Obst. Wir haben mit Geckos und Kolibris Bekanntschaft gemacht. Erstere zeigen sich sobald es dunkel wird überall an Hauswänden. Die krabbeln hin und her und schnappen sich Insekten. Die schüchternen Kolibris sind dagegen etwas seltener und ich habe tatsächlich nur drei gesehen. Beim letzten hatte ich Kamera und iPhone bereit und mir gelang eine relativ gute Zeitlupenaufnahme.

<figure>
	<img src="{{ site.url }}/images/2015-panama-22-gecko.jpg" alt="Gecko">
	<figcaption>Ein Gecko auf Beutezug</figcaption>
</figure>

<figure>
	<img src="{{ site.url }}/images/2015-panama-21-folklore.jpg" alt="Folkloristische Tänze">
	<figcaption>Zum Abschluss: Folkloristische Tänze als Abendprogramm im Resort</figcaption>
</figure>

[Auf Flickr gibt es alle Bilder von der Reise.](https://flic.kr/s/aHsk7niJD9)
