---
layout: post
title: "Die heute-show oder ich?"
date: 2015-03-30
category: medien
tags: [TV, Kritik]
published: true
comments: false
share: true
---

Die heute-show entdeckte ich für mich irgendwann am Anfang des Studiums, vermutlich circa 2010, als schon die 2. Staffel lief. Die satirische Nachrichtensendung war damals im deutschen Fernsehen etwas Neues, ja geradezu frech und erfrischend, möchte man sagen. 


Auch in meinem Freundes- und Bekanntenkreis wurde sie schnell beliebt und nicht selten schrieb man sich am Wochenende Nachrichten mit der Empfehlung zur letzten Sendung am Freitag. 

Von 2011 bis 2013 habe ich nur selten eine Folge verpasst. In den letzten Jahren nahm das ab. Am Samstagmorgen schaute ich zum Frühstück die Folge vom Vortag, dem 13. März. Ich habe zwei Mal gekichert und mich deutlich öfter über die Sendung geärgert. Das war keinesfalls neu, denn schon seit Monaten habe ich längst kein besonders großes Vergnügen bei der angeblichen Nachrichtensatire. Und nicht zum ersten Mal stellte ich mir die Frage: Habe ich mich verändert, oder die heute-show?     

In der gesamten Sendung wurden 5 Themen angesprochen:

- Griechenland
- Russland/Ukraine
- Fussball-Weltmeisterschaft der Männer in Katar
- Mietpreisbremse
- (Buch von Peer Steinbrück)

Dabei sind die Themen gar nicht das Problem, denn es geht ja schließlich um Nachrichten und das sind alles aktuelle Themen aus dem tagespolitischen Geschehen. Das Problem liegt in dem, was die heute-show aus dem Themen macht.    

#### Griechenland 
Griechenland wurde für seine Forderung nach Reparationszahlungen und der angeblichen Unentschlossenheit bezüglich der Hilfskredite kritisiert. Ich gebe gerne zu, dass ich zwar die Nachrichten oberflächlich verfolgt habe, aber mir die Hintergründe nicht weiter bekannt sind. Die heute-show konnte daran auch nichts ändern. Wenn man mich gebeten hätte einen humoristischen Beitrag für einen Stammtisch zu entwerfen, besser als die heute-show hätte ich es nicht machen können.    
An der einen oder anderen Stelle blitzte mal ein doppeldeutiger Kommentar auf, doch wie viele Zuschauer das überhaupt mitbekommen? Man kann die Antwort auf diese Frage erahnen, wenn man dem Studiopublikum beim Klatschen zuhört. Das Hauptmerkmal des Griechenland-Beitrags: Die Haltung und Sichtweise der seriösen Nachrichten wurden übernommen und vermeintlich witzig gemacht.

#### Putin, Ukraine, Russia Today

Der Russland-Beitrag legt einen klassischen Fehlstart hin, weil er auf einen (bewussten?) Übersetzungsfehler basiert[^1]. Das ist nicht allein die Schuld der heute-show, aber sie lassen sich eine grandiose Chance entgehen, ihre Kollegen aus Fernsehen und Presse endlich mal wieder satirisch zu behandeln. Zum Beispiel wäre die richtigere, aber nicht unbedingt bessere Übersetzung *Wiedervereinigung mit der Krim* ein toller Anlass unsere glorreiche Wiedervereinigung von 1989 als *Annexion der DDR an Westdeustchland* zu bezeichnen. Nur ein Vorschlag.     
Dann muss man natürlich noch über Russia Today herziehen, als ob irgendjemand ernsthaft glaubt, dass deren Nachrichten weniger von Propaganda und russischen Ansichten geprägt sind. Kann man machen, ist aber weder besonders witzig, noch interessant.

#### Katar

Bei diesem Beitrag waren einige O-Töne recht witzig. Das Thema insgesamt aber nicht sehr neu. Bestechung bei der FIFA? Menschenunwürdige Arbeitsbedingungen? Die Erkenntnis, dass Personen, die mit Fussball zu tun haben, die WM in Katar nicht so richtig kritisieren möchten? Der gesamte Beitrag wirkte auf mich eher wie ein Lückenfüller.

#### Mietpreisbremse  

Aktuell war das Thema, weil sie vor einigen Tagen durchgewunken wurde und sich die große Koalition dafür natürlich auf die Schultern klopfte. Inhaltlich war das meiste aus einer Dokumentation zur *Deutsche Annington* übernommen.     
Ein Unternehmen, finanziert von dubiosen Investoren, kauft massenweise Mietwohnungen auf, lässt die Gebäude verkommen und erhöht die Mieten. Dazu kommt die Tatsache, dass es sich oft um Sozialwohnungen handelt, deren Bewohner über Hartz IV bzw. den Staat bzw. uns allen ihre Mieten abdrücken. In dieser Konstellation kann man noch besser bescheißen und wird kaum entdeckt. Die Investoren freuen sich über die Rendite.     
Die heute-show hat das im Kontext zur Mietpreisbremse gesetzt und ein wenig lustig gemacht. 

#### Peer Steinbück

Zugegeben, das war nur ein Beitrag von Dieter Wischmeyer. Die sind eigentlich immer gut, egal wie egal das Thema ist.

#### Tourismus-Messe

Als galanten Abschluss versuchte es die heute-show mit entlarvenden Interviews auf einer Tourismus-Messe. Was man sich von Tourismus-Vertretern aus Katar, Russland und der Ukraine für neue Erkenntnisse erhoffte, ist mir schleierhaft. Dafür haben die Befragten erstaunlich souverän reagiert.     
*Wieso machen russische Soldaten in der Ukraine Urlaub, wenn es so schön in Russland ist?* war eine der plumpen Frage. Das wäre noch erträglich gewesen, wäre man zwei Stände weiter gegangen und hätte z.b. mit den den US-amerikanischen Vertreter gesprochen.     

*Welche Städte sollte man als schwarzer Tourist meiden wenn man nicht von einem weißen Polizisten erschossen werden möchte?*. 
    
Ihr merkt schon, ich mache das nicht professionell. Aber das ist die Frage, ob Katar nicht ein toller Urlaubsort für schwule Masochisten sei, weil dort schließlich Homosexuelle zur Strafe ausgepeitscht werden, auch nicht.

### Last Week Tonight

Und dann wird dir ein Beitrag[^2] von einer der US-amerikanischen Vorbild-Sendung der heute-show empfohlen und du fällst aus allen Wolken. Nicht nur, dass man sich ganze 16 Minuten einem Thema widmet, das Thema gut recherchiert ist und interessant und äußerst humorvoll vorgetragen wird. Nein, jeder Zuschauer nimmt auch noch ein Stück Erkenntnisgewinn mit nach Hause.     
Wann gab es diese Qualität das letzte Mal bei der heute-show? Ich weiß es nicht mehr. 

Die Macher sagen natürlich, die Sendung hat sich nicht verändert. Und überhaupt und sowieso, sind die Quoten so hoch wie noch nie. 
Was für ein Glück, dass das Niveau der heute-show so weit unten angekommen ist, dass der Pöbel der Sendung problemlos folgen kann und einschaltet. Ein Hoch auf die Quote!

[^1]: [Politisches Wörterbuch: Wenn Putin Присоединение sagt](http://www.politplatschquatsch.com/2015/03/politisches-worterbuch-wenn-putin-sagt.html)
[^2]: [Tobacco](https://www.youtube.com/watch?v=6UsHHOCH4q8) 
