---
layout: post
title: "Vorschau auf die Apple Watch Teil 2"
date: 2015-03-10
category: it
tags: [Apple Watch]
published: true
comments: false
share: true
---

Nach exakt einem halben Jahr liegen nun gut über 80% aller Puzzleteile auf den Tisch und das Bild der Apple Watch ist fast vollständig. 


Es werden noch knapp zwei Monate vergehen bis Ende April die ersten Apple Watches an den Handgelenken ihrer neuen Besitzer Platz finden. Und bis unzählige Meinungen über Apples neue Produktkategorie formuliert werden.

Wir Neugierigen sind erst einmal zufrieden und haben endlich Gewissheit: der Akku soll einen ganzen Tag durchhalten, Spritzwasser ist kein Problem, die Preise für die *normalen* Varianten reichen von 400€ bis 1.250€ und den S1-Chip wird man wohl auch nicht gegen einen Aufpreis austauschen können. Auch nicht, wenn man über 11.000€ für die *Edition*-Modelle mit Gold-Gehäuse übrig hat.

### Das 2. Apple Watch Event

Wieso es auch leugnen, mir hat der Apple Watch-Teil des Events nicht besonders gut gefallen. Die App-Demo hatte kein Elan und bis auf die fancy Videos über Aluminium und Stahl gab es keinen *Oho!*-Moment. Mich beeindruckte im September die Krone der Apple Watch. Das war und ist ein außergewöhnliches Detail und Beweis für Apples Innovationskraft.      
Beim *Spring Forward* Event waren vor allem die Preise beeindruckend, wenn auch nicht unbedingt positiv. Teils wurden alte Informationen wiederholt (wechselbare Ziffernblätter) oder zumindest fühlte es sich so an, als ob man das schon mal gehört und gesehen hatte.

### Fashion hat ihren Preis und ist die Zukunft

Mit der 350$ Einsteiger-Apple Watch *Sport* hat Apple die Messlatte bereits höher gelegt als die Hersteller anderer Uhren-ähnlicher Smart-Produkte. Ein Vergleich zwischen Qualität, Design und Preis lässt trotzdem schlussfolgern: Passt. Kann man machen.     
Dass die Standard-Variante der Apple Watch mit den zahlreichen schicken Uhrenbändern zur Auswahl teurer wird, war zu erwarten. Dass der Wechsel von Aluminium+Glas+Kunststoff hin zu Stahl+Saphir+Leder mit zusätzlichen 350€ zu Buche schlägt, lässt mich dann doch etwas erstaunen. Wohlgemerkt: An der Leistung der Apple Watch ändert die Konfiguration nichts (abgesehen von weniger Kratzern auf dem Bildschirm vielleicht).

Auch ohne die *Edition*-Modelle zu betrachten zeigt sich deutlich dass die Apple Watch zu einem nicht geringen Teil ein Mode-Produkt ist. Wahrscheinlich sind wir Zeugen einer neuen Entwicklung bei Apple. Ob iPhone, iPad oder Mac, in all diesen Bereichen ist Apple im Massenmarkt etabliert und erfolgreich. Ein zukünftiges Elektro-Auto von Apple wird sicherlich nicht weniger kosten als ein Tesla (ab 70.000€) und kann daher dem Luxussegment zugeordnet werden. Doch vorher muss man die Kundschaft anfixen und Apple als Luxus-Marke etablieren. Den Anfang macht die Apple Watch, nächstes Jahr folgt ein vergoldetes iPhone und so weiter. 

### Apple Watch Konkurrenz

Wenn man eine Smartwatch begehrt und ein iPhone besitzt, hat man eigentlich eine Wahl? Die Pebble macht erstaunliche Fortschritte, aber an einem grundsätzlichen Punkt wird sie stets schlechter abschneiden als die Apple Watch: Und zwar bei der Integration zum iOS-System.      
Das hat vor allem zwei Gründen. Zum einen öffnet Apple (aktuell) nicht die Türen für die Anbindung anderer Watches. Dadurch bleiben die Möglichkeiten für Drittanbieter stark begrenzt. Zum anderen zieht es App-Entwickler zu Apple. Wer die Kritik der Entwickler in den letzten Monaten verfolgt hat, weiß, dass ihnen u.a. die Entwicklung ein wenig zu schnell geht. Man muss sich alle paar Monate auf neue Geräte und neue APIs einstellen [^1], wer hat dann noch Lust sich mit dem Entwickler-SDK von Pebble herumzuschlagen? 

Ich sehe keinen Konkurrenten für die Apple Watch im eigenen iOS-Ökosystem.     
Android Wear ist leider nicht die Alternative. Mich reizt keine der bisher vorgestellten Watches und es ist sowieso schon zu spät. Ich stecke bis zum Hals in Apple-Hardware und fühle mich sehr wohl. Ich behalte die anderen Systeme trotzdem im Auge.

### Meine Wahl

Hätte ich gerne einen Saphir-Bildschirm? Ja! Hätte ich gerne ein schickes Lederarmband? Ja! Bin ich bereit dafür 750€ zu bezahlen? Oder 650€? Oder 550€? Nein.      
Ich kann die Preise der Apple Watch grundsätzlich nachvollziehen, doch gut finde ich diese Entwicklung nicht und möchte sie auch nicht unterstützen. Neugierig bin ich trotzdem und besonders einige Hands-On Videos, aufgenommen nach dem Event, haben mich schwach werden lassen. 

Ich werde am 10. April keine Apple Watch vorbestellen. Im Juni reise ich mit meiner Verlobten nach New York City und werde mir vielleicht dort eine kaufen. Je nach Wechselkurs kostet die Apple Watch *Sport* in der 42mm-breiten Ausführung ca. 390€ [^2]. Das sind 60€ weniger als in Deutschland und weil das zum Glück unter der Grenze von 430€ liegt, kommen auch keine 17,5% Steuern hinzu (wie es bei einem iPhone der Fall wäre). Und die Apple Garantie gilt auch weltweit. 

Aber bis dahin bin ich gespannt auf die ersten Meinungen und Erfahrungsberichte.
  
[^1]: Und ab und zu mit einer neuen Programmiersprache.
[^2]: Falls meinem Handgelenk sogar die kleinere 38mm-Variante steht, sind es ca. 340€.