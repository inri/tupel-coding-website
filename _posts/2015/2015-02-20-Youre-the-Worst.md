---
layout: post
title: "You’re the Worst, Staffel 1"
date: 2015-02-20
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2015-youre-the-worst-lg.jpg 
  credit: FX
  creditlink: http://www.fxnetworks.com
---

Mir ist eine weitere Comedy-Perle über den Weg gelaufen: **Your’re the Worst**. Wieso soll es in Film und Fernsehen immer nur sympathische Charaktere geben? Das dachten sich auch die Macher von **You’re the Worst** und konzentrieren sich auf ein sympathisch unsympathisches Liebespaar und ihren ebenfalls ständig verpeilten Freunden. 

Er, ein britischer Schriftsteller anfangs 30 und hauptberuflich Misanthrop, wohnt mit einem Irak-Kriegsveteranen zusammen und beschimpft in der ersten Episode seine Ex-Freundin auf ihrer Hochzeit. Dabei trifft er eine Freundin von ihr, die nicht nur als PR-Beraterin für einen jungen Rapper arbeitet, sondern auch voll und ganz mit ihm harmoniert. Sie schlafen miteinander, mehrmals und irgendwie entwickelt sich etwas festes zwischen den beiden. Oder doch nicht? 

**You’re the Worst** lebt von ihren herrlich schrägen Charakteren, die ständig seltsame Situationen heraufbeschwören und sich dabei daneben benehmen.     
Sie ist eine frische und vor allem freche Serie, die man jedem Freund schwarzen und bösen Humors empfehlen kann. 

Die erste Staffel ist in den USA 2014 gelaufen. Hoffentlich geht es bald weiter!