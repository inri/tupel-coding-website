---
layout: post
title: "Inhalt von Star Wars Episode 4 bis 6"
date: 2015-10-25
category: medien
tags: [Film]
published: true
comments: false
share: true
---

Freut ihr euch auch so sehr auf Star Wars VII wie ich? Am 17. Dezember erwacht die Macht und kommt die Fortsetzung der ersten Trilogie in die Kinos, also 38 Jahre nach dem ersten Star Wars Film (Episode IV) und immerhin noch 32 Jahre nach Star Wars Episode VI.     
Doch was ist eigentlich in der ersten Trilogie passiert? 

Um das herauszufinden gibt es 3 Möglichkeiten:

1.	Lest die detailreiche Zusammenfassung auf [Wikipedia](https://de.wikipedia.org/wiki/Star_Wars#Episode_IV.E2.80.93VI)
2.	Schaut euch die Filme einfach noch einmal an, bis Dezember habt ihr Zeit.
3. 	Lest hier bei mir weiter! Inklusive Referenzen auf die Teaser und Trailer[^1].

## Episode IV - Eine neue Hoffnung(, mit neuen Charakteren)
Der erste Star Wars Film stellt die Charaktere der Trilogie großartig vor. Durch Zufall oder durch die Macht trifft Luke auf die von Leia losgeschickten Droiden (C3PO und R2D2) mit den Bauplänen des (ersten) Todessterns, was ihn wiederum zu Obi-Wan Kenobi führt. Mit Han Solo und Chewbacca geht es nach Alderaan, wo zwar der Planet nicht mehr existiert (weil zerstört vom Todesstern), aber der Todesstern mit Dart Vader wartet.      
Die jungen Helden retten Leia aus der Gefangenschaft und Obi-Wan stellt sich Darth Vader in einem Kampf, den er mehr oder weniger mit Absicht verliert. Von da an existiert er als eine Art Jedi-Geist weiter und redet ab und zu mit Luke?          
Sie reisen zur Basis der Rebellen und werden vom Todesstern verfolgt. Dank der Erkenntnisse aus den Plänen, gelingt es Luke mit Hilfe der Macht und einem Kampfjet den Todesstern zu zerstören.

## Episode V - Das Imperium schlägt zurück(, und geht in Führung)
Die Rebellen verstecken sich auf dem Eisplaneten Hoth und werden dort vom Imperium entdeckt. Luke fliegt zum Yedi-Meister Yoda nach Dagobah und wird von ihm unterrichtet, während Leia und Han zu einem alten Freund fliehen. Lando, der Freund, verrät sie aber an Darth Vader. Luke kommt ihnen zur Hilfe und verliert bei DEM Kampf mit Darth Vader zwar seine Hand, gewinnt im Gegenzug aber die Erkenntnis um seinen Vater, Darth Vader.   
Han Solo wird eingefroren und nach Tatooine gebracht und Lando findet sein Gewissen wieder und hilft Leia und Luke bei der Flucht. 

## Episode VI - Die Rückkehr der Jedi-Ritter(, bzw. des einen Jedi)
Zu Beginn befreit die Gruppe Han Solo aus den glitschigen Klauen von Jabba the Hutt. Dann fliegt Luke zurück zu Meister Yoda, der ihn allerdings nur noch sagen kann, dass er eine Schwester hat (Leia!) und daraufhin stirbt und genauso verschwindet wie Obi-Wan.      
Es gibt mittlerweile einen neuen, noch unfertigen Todesstern, den es zu zerstören gilt. Die Helden wollen dafür das Kraftfeld auf dem Teddybär-Planeten Endor ausschalten. Vor dem Manöver haben Bruder und Schwester Skywalker ein Gespräch, welches im 2. Teaser zu Star Wars 7 [^1] zu hören ist, zumindest in leicht veränderter Form. 
Luke geht zu seinem Vater und sieht das Gute in ihm. Dieser  bringt ihn aber zum Imperator, welcher ihn auf die dunkle Seite der Macht ziehen will. Gleichzeitig läuft die Operation der Rebellen, die eigentlich eine Falle des Imperators war. Mit Hilfe der Ewoks (Teddybären) und Luke, wendet sich das Blatt aber.     
Im finalen Kampf mit seinem Vater besiegt Luke diesen, weigert sich jedoch ihn töten oder gegen den Imperator zu kämpfen. Dieser grillt Luke daraufhin mit den blauen Macht-Blitzen bis es Darth Vader nicht mehr aushält und wiederum den Imperator in einen Abgrund wirft. Luke sieht ein letztes Mal das echte Gesicht seines Vaters und flieht mit dessen Leiche vom Todesstern, der daraufhin zerstört wird.  

Am Ende verbrennt Luke seinen Vater, wobei der Helm nicht vollständig schmilzt (siehe die Teaser und Trailer von Episode 7). Und natürlich gibt es ein Siegerfest, bei dem Luke die Geister von Obi-Wan, Yoda und seinem Vater (in Gestalt des Episode 2&3 Schauspielers Hayden Christensen) sieht. 

## Mutmaßungen und Gedanken zum Epsiode 7 Trailer
- In der alten Trilogie gab es eigentlich nur 5 Personen mit Zugang zur Macht, von denen bis zum Ende 4 gestorben sind. Luke war quasi der letzte Jedi-Ritter. Ob und wie viel Macht Leia hat, wurde nie genauer spezifiziert, aber scheinbar spricht Luke zu einem Nachkommen der Skywalker-Familie. Tochter, Nichte, Enkelin?
- Ein farbiger Stormtrooper spielt eine wichtige Rolle, er scheint seine Ziele zu hinterfragen und die Seiten zu wechseln. Ob er die Macht in sich entdeckt, schließlich hält er auch kurz ein blaues Laserschwert in den Händen.
- Han Solo sagt den beiden Helden (Skywalker? und Ex-Stormtrooper?) dass die Geschichten, die man gehört hat, alle wahr sind (die dunkle Seite und die Jedi). Scheinbar sind Jedi und Sith auch nach der ersten Trilogie keine häufige Erscheinung und überhaupt sind die Ereignisse der ersten Trilogie nicht weiter bekannt.
- Tatooine ist wieder ein Schauplatz.
- Ein Sith spricht zum halb geschmolzenen Helm von Darth Vader und verspricht das zu beenden, was dieser begonnen hat. Was genau soll das sein? Das Imperium zum Sieg zu verhelfen? Die Jedi auszulöschen?
- Die verhüllte Person, die mit ihrer künstlichen Hand R2D2 streichelt, ist vermutlich Luke Skywalker. Auf dem Filmposter zu Episode 7 ist er zwar nicht zu sehen, aber womöglich ist seine Rolle erst in Episode 8 größer. 

Natürlich gibt es im Netz weitere Mutmaßungen und einige sind ziemlich phantasievoll. Nach dem erneuten Anschauen der ersten Trilogie und der Trailer zu Episode 7, sind das meine bescheidenen Gedanken.

[^1]:Die Videos: [Teaser #1](https://www.youtube.com/watch?v=erLk59H86ww), [Teaser #2](https://www.youtube.com/watch?v=ngElkyQ6Rhs), [Trailer](https://www.youtube.com/watch?v=sGbxmsDFVnE)



