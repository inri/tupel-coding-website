---
layout: post
title: "Panama 3: Bijao Beach Resort"
date: 2015-01-11
category: reisen
tags: [Reisen, Panama]
published: true
comments: false
share: true
image: 
  feature: 2015-panama-title-bijao-lg.jpg 
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Meine Reise nach Panama war eine Dienstreise und vielleicht ist der eine oder andere Leser über unsere Hotel-Wahl verwundert. Sie hat eine ganz praktische Bewandtnis: Unser Kontakt in Panama wohnt nur wenige Autominuten von diesem Resort entfernt. 

Das ist der dritte Teil einer Reihe über meine kurze Reise nach Panama im März 2014. [Teil 1](http://ingorichter.org/article/Panama-Hinflug/), [Teil 2](http://ingorichter.org/article/Panama-City/), [Teil 4](http://ingorichter.org/article/Panama-Panamakanal/), [Teil 5](http://ingorichter.org/article/Panama-Abschluss/) 

Andere Hotels gibt es in dieser Gegend nicht und die Alternative wären mehrere Stunden Autofahrt zusätzlich jeden Tag. Deshalb nächtigten wir im **Sheraton Bijao Beach Resort** und zugegeben, diesem Zwang haben wir uns gerne gebeugt. Das [Sheraton](http://www.starwoodhotels.com/sheraton/property/overview/index.html?propertyID=3693) ist ein typisches All-Inclusive-Hotel. Genauer gesagt war es *mein* erstes All-Inclusive und insgesamt eine interessante Erfahrung. 

Von Panama-City fährt man knapp 90 Minuten westlich die Pazifikküste entlang um zu diesem herrlichen Ort zu gelangen. Schon wenn man die Rezeption betritt, weht eine Brise vom Meer herüber und man staunt nicht schlecht wenn man sieht woher der Wind weht. Das Titelfoto zeigt die Aussicht aus einem Zimmer mit, naja, Meerblick eben.

Natürlich findet man hier alles, was das All-Inclusive-Urlauber-Herz begehrt. Pools, ein toller Strand und herrliche Buffets. Im Resort ging es auch gesittet zu, ein gewisses Niveau schlug sich bei den Gästen nieder. Alle sind höflich und nett. Es herrscht eine gewisse Ruhe und Entspannung. Vielleicht ist das in den meisten Anlagen der Fall, aber mir fiel es auf jeden Fall positiv auf.     

<figure>
	<img src="{{ site.url }}/images/2015-panama-14-girls.jpg" alt="Sonnenbaden am Strand">
	<figcaption>Das Foto entstand zwar nicht beim Hotelstrand, steht aber stellvertretend für die Sonnenanbeter, die bei gefühlt 90° in der Sonne knusprig braun werden wollen.</figcaption>
</figure>

Der Strand besteht größtenteils aus weißem Sand und etwas schwarzem Sand aus Vulkangestein. Das Wasser ist sehr angenehm. So angenehm, dass man gar nicht mehr herausgehen gehen möchte. Richtige Ortschaften gibt es in der Nähe nicht und die Menge an Hotelanlagen hält sich auch in Grenzen. Vorerst, denn in den nächsten Jahren werden weitere Anlagen entstehen. Wir konnten morgens in Ruhe die aufsteigende Sonne fotografieren und hatten den Strand für uns allein. Noch reihen sich keine Hotels aneinander.

Gerne würde ich das Gefühl beschreiben, wie es ist, sich in eine Hängematte zwischen Palmen zu lümmeln und einfach die Seele baumeln zu lassen. Die Sonne zu genießen, das tolle Klima und letztlich auch die Ruhe und Entspannung. Besonders zur Abenddämmerung wenn die meisten Gäste zum Büffet stürmen und der Strand sich leert, ist es sehr angenehm.   

<figure>
	<img src="{{ site.url }}/images/2015-panama-13-sun.jpg" alt="Sonnenuntergang">
	<figcaption>Klassischer Sonnenuntergang der in Wirklichkeit ein Sonnenaufgang ist</figcaption>
</figure>

### Dienstreise
 
Erwähnen möchte ich aber auch, dass wir nicht von morgens mit abends im Resort entspannt haben. Gefrühstückt wurde gleich bei Öffnung des Buffets und danach ging es zu diversen Meetings, von denen wir erst abends wieder zurückkehrten. Auch die anderen Angebote des Resorts nahmen wir nicht in Anspruch. Das bedeutet aber nicht, dass ich mich nicht auf eine reine Urlaubswoche dort freuen würde.

[Auf Flickr gibt es alle Bilder von der Reise.](https://flic.kr/s/aHsk7niJD9)