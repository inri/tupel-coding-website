---
layout: post
title: "Panama 4: Panamakanal"
date: 2015-01-14
category: reisen
tags: [Reisen, Panama]
published: true
comments: false
share: true
image: 
  feature: 2015-panama-title-kanal-lg.jpg 
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Keine Reise nach Panama kommt ohne die Erwähnung des berühmten Panamakanals aus. Und zufällig feierte er in 2014 auch noch sein 100-jähriges Bestehen.   

Das ist der vierte Teil einer Reihe über meine kurze Reise nach Panama im März 2014. [Teil 1](http://ingorichter.org/article/Panama-Hinflug/), [Teil 2](http://ingorichter.org/article/Panama-City/), [Teil 3](http://ingorichter.org/article/Panama-Bijao-Beach/), [Teil 5](http://ingorichter.org/article/Panama-Abschluss/)

Wir haben uns die Durchquerung eines Schiffes an der Miraflores-Schleuse, eine der drei Schleusen des Kanals, angeschaut. Schiffe müssen sie als erstes durchqueren, wenn wie vom Pazifik kommen und zum Atlantik möchten.     

<figure>
	<img src="{{ site.url }}/images/2015-panama-15-shipcrew.jpg" alt="Schiff in Schleuse">
	<figcaption>Der Vorgang sieht nicht nur von außen interessant aus, auch die Schiffsbesatzung fotografiert begeistert.</figcaption>
</figure>

Das Prinzip einer Schleuse ist bekannt, doch wenn man sich diesen Vorgang direkt vor Ort anschaut, sind die Dimensionen ziemlich beeindruckend. Ich zitiere [Wikipedia](http://de.wikipedia.org/wiki/Panamakanal):

> Durch die Schleusen und die Puente de las Américas ist die Größe der Schiffe auf die Panamax-Maße beschränkt. Schiffe, die dementsprechend maximal 294,3 Meter lang und exakt 32,3 Meter breit sind, haben in den Schleusen auf beiden Seiten noch 61 Zentimeter Abstand zu den Wänden der Schleusenkammer.    

Auf dem Titelfoto ist übrigens eine Treidellok zu sehen. Auf jeder Seite der Schleuse ziehen diese fleißigen *Mulis* die Schiffe hinein und hinaus.

<figure>
	<img src="{{ site.url }}/images/2015-panama-16-exit.jpg" alt="In Richtung des Pazifiks">
	<figcaption>Hier kommen die Schiffe zur Schleuse aus der Richtung des Pazifiks. Oder fahren dahin, je nachdem.</figcaption>
</figure>


[Auf Flickr gibt es alle Bilder von der Reise.](https://flic.kr/s/aHsk7niJD9)