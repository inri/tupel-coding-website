---
layout: post
title: "Kamera im Nexus 6P vs. iPhone 6S"
date: 2015-09-29
category: it
tags: [iPhone, Nexus]
published: true
comments: false
share: true
---

Das heute vorgestellte Nexus 6P ist sehr interessant. Besonders auf die Kamera wurde in der Präsentation viel wert gelegt und auch kein Vergleich mit dem neuen iPhone gescheut. Kann man die beiden miteinander vergleichen?

Folgende technische Details ließen mich aufhorchen:       
Das Nexus 6P hat einen Sensor von **12,3 Megapixel**, eine **Blende von f/2.0** und die Pixel des Sensors sind **1,55µm** groß.

Zum Vergleich:     
Das neue iPhone 6S (und 6S Plus) hat **12 Megapixel mit 1,22µm** großen Pixeln und einer Blende von **f/2.2**.

Was bedeutet das? Wichtig sind hier die Pixel des Sensors, denn 1,55µm sind größer als 1,22µm und größere Pixel fangen mehr Licht ein und das resultiert in bessere Fotos. Apple selbst betonte vor 2 Jahren die Vergrößerung der Pixel von 1,4µm auf 1,5µm im iPhone 5S und kritisierte die Mitbewerber, die zwar immer mehr Megapixel in ihre Smartphone unterbringen, dabei aber die Pixelgröße sinken lassen. Und die überragende Qualität von iPhone-Fotos gab ihnen Recht.    
Je kleiner der Wert der Blende, desto mehr Licht lässt sie zum Sensor, deshalb sind kleinere Blenden besser und deshalb ist auch die Blende vom Nexus besser (Das noch als Anmerkung).

## Wieso nur 1,22µm?

So. Nun hat Apple vor zwei Wochen ein neues iPhone vorgestellt und man fragt sich, wieso verbauen sie nicht den besseren Sony-Sensor wie Google bzw. Huawei im neuen Nexus?     

Ich denke nicht dass finanzielle oder technische Gründe eine überwiegende Rolle bei der Entscheidung gespielt haben. Also dass sie nicht viel Geld einsparen konnten oder der Sensor gar nicht so viel besser ist wie die rohen Daten vermuten lassen. 

Der Grund wird die Skalierung der Produktion sein. Ein deutlicher Hinweis dafür ist beispielsweise der verbaute A9-Chip im neuen iPhone. Dieser wird von Samsung und von TSMC produziert. Dabei sind beide Chips nicht identisch, sondern Samsungs Chip ist 10% kleiner, da im 14nm-Verfahren hergestellt[^1]. Es gibt keinen guten Grund zwei unterschiedliche Chips zu verwenden, außer man ist dazu gezwungen weil ein Hersteller alleine nicht in der Lage ist die riesigen Mengen zu produzieren, die Apple für das iPhone benötigt. Allein am ersten Verkaufswochenende wurden 13 Millionen iPhones abgesetzt(!)

Bei allen Bauteilen spielt die Verfügbarkeit großer Mengen eine wichtige Rolle. Deshalb glaube ich, dass Sony schlichtweg nicht in der Lage gewesen wäre bis September die benötigten Mengen an *besseren* Kamera-Sensoren herzustellen. Beim Mac und iPad ist der Release-Schedule relativ locker, doch ein neues iPhone wird jedes Jahr im September vorgestellt und verkauft. Der zeitliche Spielraum ist minimal.

## Fazit

Vermutlich sind die Unterschiede zwischen der Nexus- und iPhone-Kamera nur minimal und im Alltag weniger spürbar. Peinlicher für Apple ist viel mehr, dass die beiden neuen Nexus sich im Gegensatz zu den neuen iPhones schnell aufladen lassen (&quot;*doppelt so schnell wie das iPhone*&quot;). Dieses Feature präsentiert Apple sehr wahrscheinlich erst nächstes Jahr.


[^1]: Die Leistung beider Chips soll allerdings ziemlich identisch sein.