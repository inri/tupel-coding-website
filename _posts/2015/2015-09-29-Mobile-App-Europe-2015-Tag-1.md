---
layout: post
title: "Mobile App Europe 2015 Tag 1"
date: 2015-09-30
category: it
tags: [Konferenz, Technologie]
published: true
comments: false
share: true
---

Anfang der Woche war ich auf der Mobile App Europe Konferenz in Potsdam. Wie der Name es schon sagt, ging es um Themen rund um den mobilen Bereich. In zwei Berichten stelle ich euch meine Zusammenfassung der einzelnen Talks vor. Hier ist der erste.

### Appium - How to Start an Open Source Project (by Dan Cuellar)
Ein sehr guter Einstieg als Keynote und überhaupt ersten Talk bei der Konferenz. Dan war locker, humorvoll und erzählte begeistert über die Anfänge von Appium. Garniert war das ganze mit interessanten und wissenswerten Anekdoten über die Open Source Szene.     
Insgesamt war der Talk eine gute Motivation Open Source Projekte zu starten und gab hilfreiche Tips für die ersten Schritte.

### Understanding Your Mobile Users (Stephen Janaway)
Stephen war auch ein guter Sprecher, aber für mich persönlich war inhaltlich nicht viel Neues dabei. Er beschrieb ein paar gute Herangehensweisen wie man Nutzer einer App spezifiziert (Personas nutzen) und betonte eine der Grundregeln unserer Zeit: Daten über die Nutzer sammeln bzw. Daten über ihr Verhalten wenn sie deine App benutzen.     
Aber am Ende darf man nicht vergessen mit realen Personen zu reden und zu testen.    
       
### Death to Passwords (Tim Messerschmidt)
Um das Thema mit meinen eigenen Worten auf den Punkt zu bringen: Passwörter sind scheiße. Sich ständig neue Passwörter auszudenken ist scheiße. Sich Passwörter zu merken ist scheiße. Es gibt keinen Aspekt an Passwörtern, der auch nur ansatzweise toll ist.    
Tim untermauerte diese These mit deprimierenden Zahlen aus Passwort-Hacks vergangener Jahre. So erlangt man schon mit den Top 1000 Passwörtern Zugang zu 91% aller Benutzerkonten diverser Plattformen. Das ist einfach nur katastrophal. Wir brauchen neue Lösungen. Der beste Ansatz ist im Moment das Kombinieren von Passwörtern mit anderen Sicherheitsmerkmalen. Stichwort Zwei-Faktor-Authentifizierung mit vertrauenswürdigen Geräten (eigenes Smartphone) und oder biometrischen Merkmalen (Fingerabdruck). 

Insgesamt ein interessanter Talk mit einigen guten Hinweisen und Verweise auf nützliche Tools.

### Kickstart your Mobile Testing Career (by Ajay Balamurugadas, IBM)
Das war ein 3-Stunden-Workshop über Testing. Testing im Sinne von Software-Produkte testen und nicht Testen von Code. Ajay gab uns ein Dutzend Exercises, die einzeln oder in Gruppen bearbeitet und dann gemeinsam besprochen wurden.    
Einige Aspekte waren mir bekannt, aber ich lernte auch viel Neues. Eventuell hätte man das Tempo auch anziehen können und wäre dann bei 150 Minuten gelandet.   

### Ersatz-Talk von Klaus Enzenhofer
Der letzte Talk des Tages fiel aus und Klaus sprang spontan ein und referierte über Node.js und Performance von Webseiten mit Blick auf SEO. Er nutzte seine Folien zum Talk &quot;*With metrics to success!*&quot;.      
Die neueren Erkenntnisse rund um SEO waren interessant. Der Talk an sich war hier und da für meinen Geschmack mit zu viel Pathos geschmückt und Klaus machte öfter mal zu lange Kunstpausen. Aber für einen improvisierten Talk war das trotzdem super.