---
layout: post
title: "Wechsel zu Wunderlist 3"
date: 2015-01-26
category: it
tags: [App]
published: true
comments: false
share: true
---

Es waren 8 Monate seit der ersten Vorstellung von iOS 7 auf der Entwicklerkonferenz von Apple vergangen und es sollte noch weitere sieben Monate dauern, bis Wunderlist 3 endlich das Licht auf unseren Geräten erblickte.      

In der Zwischenzeit wechselte ich zu Apples Erinnerungen und Notizen. Das Synchronisieren funktionierte zwar besser als zu Zeiten von Wunderlist 2, aber die Möglichkeiten waren eingeschränkt. Es sind leider nur sehr rudimentäre Apps.  

Und dann kam endlich Wunderlist 3 und natürlich war es toll. Das Design war frisch, auf allen Geräten. Noch viel beeindruckender war aber die Synchronisation und vor allem dessen Geschwindigkeit. Sie haben ein fancy Video dazu veröffentlicht und man denkt vielleicht &quot;*Ist ja nur Werbung*&quot; doch in der Realität war es einfach genauso schnell. Starkes Ding!    
Die Verzögerung lag in der Tat mit der koordinierten Veröffentlichung auf den vielen Systemen, die sie unterstützen, zusammen. Diese Herangehensweise ist verständlich. Für iPhone Benutzer war es trotzdem ärgerlich.  

Seit Juli nutze ich wieder Wunderlist 3 und freue mich. Bis zum neuen Redesign wird noch etwas Zeit sein und vielleicht sehen wir ein gänzlich neues Produkt.