---
layout: post
title: "Mobile App Europe 2015 Tag 2"
date: 2015-10-26
category: it
tags: [Konferenz, Technologie]
published: true
comments: false
share: true
---

Am zweiten Tag besuchte ich vier Vorträge und einen Workshop. Es war wieder sehr abwechslungsreich, begonnen bei faszinierenden Erkenntnissen von Groupon, über inspirierende Gedanken zu Nebenprojekten, zukunftsweisender Forschung (VR!) und einigen guten Tips und Hinweise um Produktideen zu testen. 

### Design, Build, Upload and what now? Maintaining an app (by Danny Preussler)
Der zweite Konferenztag begann mit einer höchst interessanten und vor allem sehr guten Keynote. Es ist keine Überraschung, dass auf einer *Mobile*-Konferenz die Wichtigkeit dieser Klasse von Programmen betont wird, doch die harten Zahlen sind beeindruckend. Groupon, Zalando und ebay Kleinanzeigen generieren die meisten Umsätze über ihre mobilen Apps und nicht mehr über ihre Webseiten.      
Es trifft vielleicht nicht für alle Bereiche der IT-Welt zu und für einige erst in einigen Jahren, aber früher oder später muss man sich mit der Entwicklung von mobilen Lösungen beschäftigen. Sonst tun es die anderen. Dabei ist mobile Entwicklung ein stetiger Prozess und kein Feature auf dem Jahresplan. Eine App ist nicht irgendwann fertig, sie muss ständig gepflegt und weiterentwickelt werden, wenn man Erfolg haben will.

### Passion Projects – what I’ve learned so far (Johannes Ippen)
Johannes stellte einige seiner Nebenprojekte vor und philosophierte über solche Art von Projekten. Sehr inspirierend Ratschläge waren dabei und er war auch ein toller Gesprächspartner nach dem Talk. 

### Mobile Strategy? Why? Who? I ? (Thomas Balduff, Adobe Systems GmbH)
Der Talk von Thomas Balduff war zwar ans Management und Marketing adressiert, doch ich hatte mir trotzdem mehr für mich als Entwickler erwartet. Es war sehr allgemein, sehr *catchphrase-ig* gehalten und steuerte auf die logische Schlussfolgerung am Ende hinaus: Adobe bietet tolle Lösungen. Das mag vielleicht auch sein, war mir trotzdem etwas zu selbstbeweihräucherisch und zu wenig kritisch[^1].    

Eine amüsante Anekdote zur Erkenntnis, dass niemand weiß was das nächste große Ding sein wird, gab es aber: Stichwort Augmented Reality (AR). Seit über sechs Jahren bietet er bei Kunden auch AR-Lösungen an und verspricht, dass nächstes Jahr der große Durchbruch für diese Technologie kommen wird. Bisher blieb dieser aber aus, wie er selbst feststellen musste. Tja. 

### Mobile + gesture input + AR (Miho Tanaka)
Vom Ausbleiben des Durchbruchs für AR ging es zur Grundlagenforschung von Gesten-Input bei AR. Das ist kein Thema, das mich besonders interessiert, aber die Sprecherin war wirklich gut und sehr charmant. Und ich musste feststellen, das Thema ist interessant.      
Die AR fand ich als Erweiterung der Realität nie besonders verlockend. Wenn man aber mit virtuellen Objekten interagieren kann, wird das ganze deutlich spannender und nützlicher. Wer sich damit beschäftigt und ein paar gute Erkenntnisse gewinnt, wird mit Sicherheit von einem der großen Player aufgekauft. Die sind ganz heiß auf diese Technologie.  

### From idea to testable prototype in 90 mins (Roman Eude)
Zum Abschluss ein Workshop, der aber weniger Workshop als Talk war und trotzdem sehr gut und voll mit wichtige Erkenntnissen. Wenn man möchte, dass Studenten mutig sein und ihre Ideen verwirklichen sollen (Stichwort: Startup), gehören diese 90 Minuten von Roman ins erste Semester.     
Er zeigte uns, wie man mit verhältnismäßig wenig Aufwand und vor allem keinerlei Programmierung Prototypen von Ideen erstellen und testen kann. 

Wieso will man das? Womöglich gibt es Ideen, deren Umsetzung viel erfolgreicher ist als sie während eines Tests erahnen ließ. Die meisten Ideen kann man vor der Umsetzung wunderbar prüfen und bekommt heraus, ob überhaupt ein Markt für sie da ist, wie groß und erfolgreich die Konkurrenz ist und ob sich die Mühe lohnt. Oft nämlich nicht. Spätestens wenn es um die Suche nach Investoren geht, sind diese Erkenntnisse eine Notwendigkeit.

[^1]:Ein wenig ärgerte ich mich über seine Begeisterung für fragwürdige Business-Modelle. Er verglich die Marktwerte von DHL und [Uber (in Zusammenhang mit deren neueste Idee UberCARGO)](http://newsroom.uber.com/hong-kong/2015/01/a-ride-for-your-goods-introducing-ubercargo/), stellte fest, dass beide den gleichen Wert besitzen und wollte daraus eine Erkenntnis bzgl. Businessmodelle, in deren Zentrum eine mobile Anwendung/Dienst ist, ziehen. Naja. Auch dass die guten alte Taxi-Unternehmen bald verschwinden, weil Uber ja so viel innovativer ist, halte ich persönlich für eine interessante Interpretation von Innovation.     
Irgendwo taucht zwar eine App auf, aber die Businessmodelle solcher modernen und schnell wachsenden Unternehmen sind oft nicht innovativ, sondern beuten Menschen aus.

