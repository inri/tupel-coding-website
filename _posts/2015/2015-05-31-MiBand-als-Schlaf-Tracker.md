---
layout: post
title: "MiBand als Schlaf-Tracker"
date: 2015-05-31
category: it
tags: [Review, Fitness, Technik]
published: true
comments: false
share: true
---

Hand aufs Herz, wozu taugt ein 15€-Fitness-Band?     
Das Tracken der täglichen Schritte überlasse ich dem MiBand nicht, denn es ist etwas übermotiviert. Wie sieht es beim Tracken von Schritten auf einem Crosstrainer aus oder das Tracken des eigenen Schlafs?

### Crosstrainer-Schritte

Das iPhone zählt die Bewegung auf einem Crosstrainer nicht als Schritte. Die App *Runtastic* schluckt fleißig die Daten meines Pulsmessers, aber registriert keine Bewegung. Das ist nur halb-tragisch, da der Crosstrainer selbst den zurückgelegten Weg zählt und ich diese Information nachtragen kann. Das MiBand registriert die Armbewegung beim Laufen auf dem Crosstrainer als Schritte und liegt damit sogar mal ganz richtig. Ich habe einige Minuten lang meine Schritte manuell mitgezählt und auf die Gesamtzeit hochgerechnet. Passt. Allerdings ist ein &quot;Crosstrainer-Schritt&quot; kein *richtiger* Schritt, von daher muss man sich überhaupt fragen, wie sinnvoll das ist. 

Mit der Apple Watch kann man u.a. auch eine Crosstrainer Aktivität aufzeichnen und die Infos werden in der entsprechenden Abteilung in Apple Health gesichert. Manuell kann man keine Aktivitäten nachtragen und ich weiß auch nicht, welche Apps das bisher können. Der Clou ist im Prinzip, dass ein erhöhter Puls vorhanden.

Das MiBand registriert Schritte auf dem Crosstrainer, kann aber die zurückgelegte Strecke nicht errechnen und den Widerstand des Sportgeräts auch nicht erfassen. Also muss man wohl oder übel die Daten vom Crosstrainer abschreiben, oder warten bis die Sportgeräte smarter und bezahlbarer werden.

### Schlaf-Tracking

Wenn man mit dem iPhone den eigenen Schlaf tracken möchte, startet man eine Schlaf-Tracking App und legt das iPhone am Kopfende des Bettes unter das Bettlaken. Meist liegt auch das Kopfkissen in diesem Bereich und es stellt sich durchaus die Frage, wie genau die Messungen sind.          
Das MiBand ist perfekt um diesen Ablauf zu vereinfachen. Man muss nur das Band anlegen, wenn man es nicht sowieso schon den ganzen Tag trägt, und ins Bett gehen. Auch dank der langen Akkulaufzeit muss man sich keine Gedanken über das regelmäßige Aufladen machen. Wenn in der Nacht meine Apple Watch wieder zu Kräften kommt, wacht das MiBand über meinen Schlaf.

Ich tracke meinen Schlaf seit April mit dem MiBand und bin prinzipiell ganz zufrieden. Einige *Aber* gibt es trotzdem. An dieser Stelle eine kurze Kritik an zwei Schlaf-Tracking Apps. Zuerst entdeckte ich *Sleep Cycle* und ersetzte es später mit dem mich mehr ansprechenden *Sleep Better*. Beide sind in der Lage meinen Schlaf zu überwachen, die Schlaf-Daten in *Apple Health* zu schreiben und tolle Auswertungen darzustellen. Aber sie können nur die eigenen Daten lesen und leider nicht die Schlafdaten vom MiBand lesen bzw. aus *Apple Health*. Das ist sehr schade.

Beim Vergleich zwischen der *Sleep Better* App und dem MiBand wurden alle Werte den jeweiligen Apps entnommen.     
Was die generelle Genauigkeit des MiBands angeht, liegt es mit *Sleep Better* ziemlich gleichauf. Die Differenz meiner Einschlafzeit betrug 11 Minuten und beim Aufwachen 21 Minuten. Meist registrierte das MiBand die jeweilige Aktion einige Minuten später, ob es eine Genauigkeit oder Ungenauigkeit ist, lässt sich schwer sagen.      
Kurios sind die Unterschiede bei der Einteilung zwischen Leicht- und Tiefschlafphasen. Das MiBand kommt zusammen immer auf 100%, *Sleep Better* im Schnitt auf 94%. Ich habe einen recht festen Schlaf und wache zwischendrin eigentlich nicht auf. Aber ob das immer in 100% resultiert? Mein leichter Schlafanteil betrug 24% bei *Sleep Better*, jedoch 52% beim MiBand. Tief waren es dann 70% (*Sleep Better*) vs. 48% (MiBand). Da gefallen mit die MiBand-Werte gar nicht.

Insgesamt spielt die Einteilung der Schlafphasen für Apple Health keine Rolle. Dort wird nur gespeichert, von wann bis wann man geschlafen hat. Womöglich ist die Einteilung sowieso nicht sehr genau.     
Ein zusätzlicher Kritikpunkt am MiBand ist die fehlende Möglichkeit einzelne Datenpunkte nachträglich bearbeiten zu können. In knapp zwei Monaten ist es nur 2x passiert, aber wenn dein Schlaf angeblich 6 Stunden länger war als er wirklich gedauert hat, verzerrt das die Statistik und Diagramme. Wie ärgerlich. In Apple Health kann ich diesen Datenpunkt korrigieren, in der MiBand App direkt aber nicht.

### Weck mich sanft II

Einige Worte zum Wecken hatte ich schon im ersten Artikel zum MiBand geschrieben. Das Vibrieren ist zwar nicht so sanft wie bei der Apple Watch, aber dafür wird man auch wach davon. Allerdings würde ich mich nicht allein auf das MiBand verlassen, denn mir ist es schon mehrmals passiert, dass die in der App eingestellte Weckzeit scheinbar nicht zum Band synchronisiert wurde. Also gab es auch kein Wecken. Sehr ärgerlich.    

### Fazit 

Ich würde davon abraten das MiBand den ganzen Tag zu tragen, denn es zählt Armbewegungen als Schritte und das resultiert in fast doppelt so vielen Schritten als man tatsächlich gelaufen ist. Für das Tracken von Bewegungen im Allgemeinen ist es aber okay, wenn man den Wert zu interpretieren weiß. 

Als Schlaf-Tracker und Weckarmband ist es aber toll und nützlich. So gerne ich auch meine Schlaf-Gewohnheiten im Blick haben will, jede Nacht eine Schlaf-Tracking App zu aktivieren und das iPhone unter das Bettlaken schieben… zu viel Aufwand! Ich habe es ständig vergessen. Das MiBand zum Schlafen anzulegen vergesse ich nur noch sehr selten. 

Der Preis ist einfach unschlagbar und deshalb ist das MiBand zumindest als Schlaf-Tracker sehr zu empfehlen!