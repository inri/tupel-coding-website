---
layout: post
title: "Ground Floor, Staffel 1 &amp; 2"
date: 2015-02-27
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2015-ground-floor-lg.jpg 
  credit: TBS
  creditlink: http://www.tbs.com
---

Mit **Brooklyn Nine-Nine** [habe ich eine sehr gute Comedy Serie vorgestellt](http://ingorichter.org/article/Brooklyn-Nine-Nine-Staffel-1/), die mit Scrubs vergleichbar ist. Doch auch **Ground Floor** ist nicht nur mit Scrubs vergleichbar und ebenfalls sehr gut, sondern hat sogar noch konkrete Parallelen zu Scrubs: Nämlich John C. McGinley (Dr. Cox) als Schauspieler und Bill Lawrence als Creator und Produzent.

**Ground Floor** ist ein amüsantes Romeo und Julia in einem Business-Skyscraper. Klingt etwas komisch, trifft den Plot aber ganz gut. Der junge aufstrebende Banker Brody verliebt sich in die im Haus-Management arbeitende Jenny. Sein Arbeitsplatz ist natürlich ganz oben und ihrer ganz unten im Gebäude, im Ground Floor. Die Kollegen der beiden sticheln mal hier und mal da, im Zentrum steht die &quot;Klassen&quot;-Differenz zwischen beiden und das ist ziemlich witzig. Sein Chef, Dr. Cox, spielt im Prinzip die gleiche Rolle wie bei Scrubs und ich finde ihn dabei teils sogar noch besser.

Die ersten und leider auch letzten beiden Staffeln sind in den USA bereits gelaufen. Ob es eine deutsche Ausstrahlung gibt, steht in den Sternen.     
Trotzdem, wer es noch nicht geahnt hat: Eine dicke Empfehlung für **Ground Floor**, genau wie für **Brooklyn Nine-Nine** an dieser Stelle noch einmal. Definitiv zwei Comedy-Highlights in 2014 und 2015.