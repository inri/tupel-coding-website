---
layout: post
title: "Feste Aktivitätsziele mit der Apple Watch"
date: 2015-08-27
category: it
tags: [Apple Watch]
published: true
comments: false
share: true
---

Ein alternativer und reißerischer Titel wäre vielleicht auch &quot;*Apple bevormundet Kunden!*&quot;. Vordergründig geht es um die Aktivität App, die ein Bestandteil der Apple Watch ist. Dabei wird meine Aktivität, Training und Stehen überwacht. Das liest sich vielleicht komisch, ist aber ganz nützlich.     

Eine Einheit *Stehen* wird erreicht in dem man innerhalb von einer Stunde mindestens 1 Minute lang steht statt zu sitzen. *Training* ist die Gesamtdauer jeder Art von Aktivität bei der der Puls erhöht ist, was beispielsweise schon bei einem flotten Spaziergang der Fall ist. Die *Aktivität* wiederum ist jede Art von Bewegung, gemessen in verbrannten Kalorien.

Apple hat für jeden Aspekt standardmäßig fest definierte Ziele vorgegeben. Bewegen soll man sich für 30 Minuten am Tag, Stehen mindestens 12 Stunden und generell 1000kcal bei Aktivitäten verbrennen. Nur diese 1000kcal lassen sich vom Nutzer verändern. Die 30 Minuten Bewegung und 12 Mal täglich Stehen sind fest voreingestellt. Und das ist problematisch. 

[Marcel Wichmann mag](https://twitter.com/UARRR/status/636449411707633664) [die Notifications der Watch](https://twitter.com/UARRR/status/636449778776285185) [nicht mehr sehen.](https://twitter.com/UARRR/status/636449948930867200) Auch ich habe das Gefühl, dass sie überflüssig geworden sind.     
12 Mal am Tag Stehen schafft man relativ problemlos. Vielleicht hat mich die Watch bereits erzogen und ich stehe unbewusst öfter auf? So oder so, in meinen 17 wachen Stunden pro Tag, kann ich mir 5 *Stehstunden* entgehen lassen. Wieso darf ich die Anforderungen an mich selbst nicht anpassen? Wieso nicht einfach mal 24 *Stehstunden* einstellen und dann eben jede halbe Stunde aufstehen?     
Das gleiche beim Training. Jemand der regelmäßig Sport treibt, schafft an den Trainingstagen ein vielfaches vom Bewegungs-Ziel.   

Die Motivation ist der Kreis, den ich immer im Blick habe, wenn ich auf die Uhrzeit der Watch schaue. Wenn er zu schnell voll wird oder ich am Nachmittag weiß, dass ich bis zum Ende des Tages gar nicht die benötigten Kalorien verbrennen kann, verabschiedet sich auch die Motivation.     
Mit dem nächsten Software-Update der Watch will ich mehr Kontrolle über meine sportlichen Ziele. 

Bezüglich der alternativen Überschrift: Man kann darüber philosophieren, was sich Apple bei den festen Werten gedacht hat. Willkürlich werden sie vermutlich nicht sein.     
Ich bin bei dieser Art von Apple-typischer Bevormundung immer hin- und hergerissen. Einerseits wird dem Nutzer ein Stück Entscheidungsfreiheit genommen, andererseits gibt es auch bei Software einfach Aspekte, bei denen die meisten Nutzer für sich selbst schlechte Entscheidungen treffen (und dann die Schuld auf die Software schieben). 

Apple nimmt eine konservative Stellung ein und das Ergebnis ist dann beispielsweise feste 30 Minuten Training pro Tag. Wenn ich als eher unsportlicher Typ dieses Ziel fast immer erreiche, ist es für sportliche Menschen irrelevant. Und das Stehziel meist auch. Und das ist insgesamt sehr schade.