---
layout: post
title: "Joylent und Ernährung"
date: 2015-04-08
category: blog
tags: [Ernährung]
published: true
comments: false
share: true
image: 
  feature: 2015-joylent-lg.jpg 
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Wieso machen wir uns eigentlich die Mühe jeden Tag Nahrung in den vielfältigsten Formen zu uns zu nehmen? Mittlerweile weiß man dank der Ernährungswissenschaft ziemlich genau, was unser Körper benötigt. Wie viel Kohlenhydrate, Eiweiß, Fett, Vitamine, Eisen… und so weiter. 

Auf den Verpackungen von Nahrungsmitteln sind genau diese Werte angegeben, einmal im Verhältnis zu 100g des Lebensmittels und einmal im Verhältnis zur empfohlenen Tagesmenge. Wieso gibt es nicht *ein* Lebensmittel, welches diese Stoffe in den von uns benötigten Mengen vereint? Das wäre in unser hektischen Welt doch eine praktische Sache.

### Soylent und Joylent
   
Seit Anfang 2014 macht [*Soylent*](http://www.soylent.me/#/) in den USA auf sich aufmerksam. Ursprünglich war Soylent eine Art flüssiges Lebensmittel, bestehend aus Wasser, etwas Öl und dem nährstoffreichem Pulver. Man mixte sich die gewünschte Menge zusammen und hatte eine Mahlzeit - schnell und unkompliziert. Die Idee fand viele Anhänger und mittlerweile wartet man 4 Wochen auf eine Bestellung.     
Die Formel ist aus Transparenzgründen Open Source, also für jeden einsehbar, und wird ständig weiterentwickelt. Aktuell hat sie die Versionsnummer 1.4 und das benötigte Fett ist nun auch in Pulverform vorhanden. 

Leider wird Soylent bisher nur in den USA vertrieben, doch dank der offenen Rezepturformel, kann sich theoretisch jeder das Pulver mixen. Ein Herr aus den Niederlanden tat genau dies und nannte es [*Joylent*](https://www.joylent.eu).     
Joylent kann man ohne Probleme aus Deutschland kaufen und wartet auch nicht auf die Lieferung. Ich habe das getan und nun liegen einige Wochen mit Joylent als gelegentlicher Mahlzeitenersatz hinter mir.     

### Flüssige Mahlzeit

Joylent zu mixen geht schnell und einfach. Das Ergebnis ist ein mehr oder weniger dickflüssiger Shake. Man hat die Auswahl zwischen den Sorten Erdbeere, Schokolade, Vanille und Banane, wobei ein gewisser Grundgeschmack an Haferbrei erinnert. Grundsätzlich ist der Shake relativ süß und ich könnte niemals einen ganzen Tag, geschweige denn mehrere Tage hintereinander nur Joylent zu mir nehmen. Zum Frühstück oder mal als Mittagessen ist er aber ideal und das Zubereiten ist vor allem  so schön schnell. 
     
<figure>
	<img src="{{ site.url }}/images/2015-joylent-meal.jpg" alt="Eine Mahlzeit Joylent im Shaker">
	<figcaption>Eine Mahlzeit Joylent, ca. 160g, im Shaker.</figcaption>
</figure>

Ein kompletter Shake, also ca. 160g (1/3 einer Packung) mit 400ml Wasser, ist eine ziemlich beachtliche Mahlzeit. Bei mir stellt sich schon nach etwas über der Hälfte ein Sättigungsgefühl ein. Wie lange Joylent tatsächlich satt hält, kann ich noch nicht so richtig einschätzen. Letztens hatte ich nach knapp 3 Stunden wieder Hunger, was für eine 700kcal-Mahlzeit nicht besonders viel ist. Heute dagegen waren 4 Stunden kein Problem.

Ich mag an Joylent, dass es nicht auf dieser Diät-Welle schwimmt. Es soll Mahlzeiten ersetzen und dich nicht schlanker machen. Das erreicht man mit Joylent genauso wie mit anderen Lebensmitteln: FDH - Friss die Hälfte.     
Meine Verlobte und ich ersetzen gelegentlich eine Mahlzeit mit Joylent, meist das Frühstück. Mit 2€ pro Mahlzeit kommt neben dem zeitlichen Vorteil der schnellen Zubereitung auch ein finanzieller hinzu. 

<figure>
	<img src="{{ site.url }}/images/2015-joylent-day.jpg" alt="Eine Tagesportion Joylent">
	<figcaption>Eine Tagesportion Joylent hat ca. 500g</figcaption>
</figure>

### Erstes Fazit

Die SciFi-Utopie der einfachen Ernährung rückt in greifbarer Nähe. Joylent schmeckt und ist besonders ein guter Frühstücks-Ersatz. Welches Potential in dem Pulver steckt, bleibt abzuwarten. Einen interessanten Artikel zu den Inhaltsstoffen findet man bei [Fitness Spartacus](http://www.fitness-spartacus.de/joylent-soylent-rezept). Ich schaue mir gerade andere *Pulver* und deren Inhalte an, denn die Idee der flüssigen Ernährung ist nicht neu.

<figure>
	<img src="{{ site.url }}/images/2015-joylent-flavors.jpg" alt="4 verschiedene Geschmacksrichtungen: Erdbeere, Schokolade, Vanille und Banane">
	<figcaption>Joylent gibt es in vier verschiedene Geschmacksrichtungen: Erdbeere, Schokolade, Vanille und Banane</figcaption>
</figure>