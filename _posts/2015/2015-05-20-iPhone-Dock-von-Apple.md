---
layout: post
title: "iPhone Dock von Apple"
date: 2015-05-20
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Vor einigen Monaten kaufte [ich via Kickstarter das Dock von Kero](http://ingorichter.org/article/Mein-1-Kickstarter-Projekt/) und war nicht sehr zufrieden. Das Dock hält zwar alles, was es auch verspricht, besonders praktisch ist es trotzdem nicht. 

Aufgrund der Kabelführung steht das iPhone nicht ganz gerade auf dem Dock. Bedienen kann man es einhändig gleich gar nicht, auch nicht hochnehmen. Für den Nachttisch nicht zu gebrauchen und auch nicht optimal für den Schreibtisch.

Also kaufte ich mir das HiRise von Twelve South, ohne Deluxe, und bin sehr zufrieden. Und das [denkt John Gruber darüber](http://daringfireball.net/linked/2015/05/19/iphone-docks):

> On my desk I use a black Twelve South HiRise Deluxe. It’s a bit fiddly to set up, but that’s because it’s adjustable to perfectly fit any iPhone or iPad Mini. […] It’s lightweight, but it’s still easy to undock the phone one-handed. (Be sure to get the the HiRise Deluxe, not the regular HiRise. I have one of those, too, and the Deluxe model is definitely better. Twelve South should just discontinue the regular one.)

Ich weiß leider nicht, was ihn konkret an der non-Deluxe Variante stört und wieso er sogar fordert, dass es gar nicht mehr verkauft werden sollte. Noch dazu kostet die Deluxe Variante fast doppelt so viel.            
Mein iPhone 5s steht stabil, ich kann es einhändig bedienen und dem Dock entnehmen. Ein iPad Mini ist schon wackeliger, aber es geht immer noch. Mit dem normalen HiRise sind auch bei [Amazon viele Kunden zufrieden](http://www.amazon.de/Twelve-South-12-1404-Desktop-schwarz/dp/B00JP11TPU/ref=sr_1_4?ie=UTF8&qid=1432107152&sr=8-4&keywords=hirise).  

John Gruber sagt nein, Ingo sagt ja.     
Die Deluxe Variante ist aber sicherlich auch nicht verkehrt.