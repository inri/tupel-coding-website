---
layout: post
title: "Das grosse Tracking"
date: 2015-08-24
category: it
tags: [App, Technologie, Fitness, iPhone]
published: true
comments: false
share: true
---

Das sogenannte *Self-Tracking* ist wahrscheinlich **der** Trend unserer Zeit. Was die Datensammelwut angeht, stehen normale Menschen der NSA, Facebook und Co in nichts nach. Der einzige Unterschied: Ich entscheide selbst welche Daten ich über mein Leben und mich selbst sammeln will, und wem ich Zugriff auf diese Daten gebe.     

Im Folgenden möchte ich meine Sammlung aus *Tracking-Helfern* vorstellen. Dass das *Self-Tracking* zusammen mit dem Siegeszug des Smartphone stattfindet, ist übrigens keine Überraschung, besonders wenn man sich die Anzahl Apps vergegenwärtigt. 

### Puls

Das Messen des eigenen Pulses ist wahrlich nicht besonders neu, jedoch das einfache und bequeme Aufzeichnen der Daten. Ich benutze bei sportlichen Aktivitäten einen Brustgurt von Polar. Der sendet die Daten nicht nur via Bluetooth an mein iPhone, sondern funkt sie auch dem Crosstrainer und vermutlich anderer solcher Sportgeräte.

Bisher sammelte ich beim Sport meine Pulsdaten über Trainingseinheiten in Runtastic. Seit der Apple Watch überlasse ich ihr nicht nur den Puls beim Sport, sondern sie misst auch im Laufe des Tages meinen Puls und schreibt diese Daten in *Apple Health*.

### Sport 

Es gibt dutzende Sport Apps und welche man bevorzugt, ist persönlicher Geschmack. Ich habe mich vor einer Weile mit [Runtastic](https://www.runtastic.com/) angefreundet. Allerdings missfällt mir, dass sie ihr eigenes Süppchen braut und die Website viel zu überladen mit Werbung und Unwichtigem ist.      
Seit Apple auch sportliche Aktivitäten zu *Apple Health* hinzufügt und sich mit der Watch eben solche aufzeichnen lassen, würde ich alle meine sportlichen Aktivitäten gerne dort sehen. Von mir aus kann Runtastic weiterhin detailliertere Informationen sammeln und diese dem Runtastic-Benutzer auf der Website zeigen. Das *Apple Health* Minimum sollte aber auch gespeichert werden, sonst muss ich bei jeder sportlichen Aktivität Runtastic und die Watch mittracken lassen.

Bis es so weit ist, wird Runtastic bei mir ruhen und die Aufzeichnungen laufen über die Watch. Beides parallel wäre auch möglich.   

### Schritte

Wie viel Schritte man pro Tag zurücklegt, ist eine wirklich nette Information und dank moderner Smartphones und Fitness-Armbändern in allen Preisklassen, auch einfach zu Tracken.

Die App [Moves](https://www.moves-app.com) ist ein simpler wie genialer Ansatz und zählt nicht nur Schritte (Gehen, Laufen, Verkehrsmittel,...), sondern trackt auch grob die Orte, an denen man sich länger aufgehalten hat. Seit dem eingebautem Schrittzähler im iPhone 5s sind die Daten auch sehr zuverlässig.     
Im Frühjahr kaufte ich mein erstes Fitness-Armband, das MiBand. Es stellte sich heraus, dass es gerade zum Schritte zählen weniger geeignet war (überambitioniert), deshalb mehr zu dessen nützliche Funktion unter *Schlaf*.     
Und wieder mal kommt man nicht umhin die Apple Watch zu erwähnen. Sie verbessert sich im Zählen der Schritte durch den Abgleich Schrittzahl vs. zurückgelegte Kilometer. Nach einer kurzen Lernzeit ist sie so gut, dass sie die Schritte auch ohne ein in der Nähe befindliches iPhones, welches sie für die meisten Informationen benötigt, zählen kann. 

Während unseres 2-wöchigen Urlaubs in New York habe ich auf 4 unterschiedliche Arten meine Schritte gezählt. Die genaue Auswertung folgt später, aber das Kurzfazit lautet: Die Apple Watch zählt am genauesten.

### Produktivität

Weg vom Körperlichen und einen anderen Aspekt des Self-Tracking erkundet. Was macht man eigentlich den ganzen lieben langen Tag, bzw. was erarbeitet man? Immerhin eine wichtige Frage der kapitalistischen Gesellschaft.     
Mein erster Ansatz, um eine Antwort auf diese Frage zu finden, ist die sehr gute Mac und iOS App [Tyme](http://tyme-app.com). Darin legt man Projekte und Unteraufgaben fest und stoppt die Zeit, die man mit dem Erledigen derselben verbringt. Heraus kommen schicke Statistiken und Zahlen.     
Das ganze hat einen Haken: Man muss sich selbst zwingen den Zeitstopper zu aktivieren. Tymes macht es dem Benutzer sehr einfach, aber der Benutzer ist eben vergesslich. Nachtragen von Arbeitseinheiten ist zwar auch möglich, wird aber genauso vergessen. Zumindest von mir. 

Ein Lösungsansatz könnte [RescueTime](https://www.rescuetime.com) sein. Dieses Programm läuft im Hintergrund und protokolliert die Zeit der aktiven Fenster des Benutzers. Ja ich weiß, das wird jetzt etwas gruselig. Es erkennt die jeweils benutzten Programme, registriert die Hauptdomain einer besuchten Website und sortiert das alles halbautomatisch in Kategorien, denen jeweils ein bestimmtes Produktivitätslevel zugeordnet ist. Bei Bedarf kann man nachjustieren.    

Generell ist es mit RescueTime möglich, zu sehen womit man sich am Mac so beschäftigt hat und vor allem wie lange. Etwas NSA-gruselig, aber interessant. Ich nutzte es nicht sehr lange und war mir nicht ganz sicher, wie es die Zeit stoppt. Wenn ich mal 3 Stunden am Stück am Mac war und maximal 2 Minuten für eine Pullerpause einlegte, zeigt es trotzdem niemals komplette 60 Minuten getrackte Zeit und Aktivitäten an. Meist sind es zwischen 45 und 56 Minuten. Eventuell bemerkt es, wenn für eine gewisse Zeit der Mauszeiger nicht bewegt wurde, also keine Aktivität stattfand. Das passiert leider öfter mal, wenn ich am Whiteboard brainstorme.

Wie auch immer. Grundsätzlich sind die Infos von RescueTime interessant und können dabei helfen Tyme regelmäßiger zu benutzen. Aber der Preis ist die komplette Überwachung aller meiner Aktivitäten am Mac. Mir persönlich war dieser Preis zu hoch.

Und was ist mit dem iPhone? Für das Tracken der iPhone-Benutzung gibt es die [Moment](https://inthemoment.io) App. Sie zeichnet auf, wann und wie lange man das iPhone benutzt hat, aber nicht, was man in dieser Zeit gemacht hat. Könntet ihr sagen, wie oft ihr am Smartphone hängt? Bei mir sind es täglich 2 bis 3 Stunden. Ein ausführlicherer Artikel dazu folgt vielleicht noch.

### Lebensmittel

Kommen wir zurück zum Körper. [Lifesum](https://lifesum.com), former known as *ShapeUp Club*, habe ich vor zwei Jahren schon vorgestellt. Am grundsätzlichen Ansatz hat sich in dieser Zeit nicht viel verändert. Ich trage in die App ein, was ich gegessen habe und die App sagt mir wie viel Kalorien das sind und ob ich meinen täglichen Kalorienbedarf nicht vielleicht schon überschritten habe. Genial, wenn man denn immer alles einträgt.      
Dazu kommen weitere Infos wie Gewicht und Körperfett, die Lifesum, neben der Kalorienaufnahme, auch in *Apple Health* schreibt. 

### Schlaf

Dem Schlaf-Tracking widmete ich mich anfangs nur mit Apps und iPhone. Aber auch hierbei ist die Faulheit ein entscheidender Faktor. Man denkt eben nicht jeden Abend daran, das iPhone unter das Bettlaken zu legen. Das MiBand brachte eine enorme Erleichterung. Entweder man trägt es ständig, dann ist alles gut, oder man macht es nur zum Schlafen um (marginale Gefahr).      
Wie verlässlich das Schlaf-Tracking ist, kann ich nicht beurteilen.  Die Apps und das MiBand sind sich ziemlich ähnlich. Eine Korrelation zwischen der Dauer der Tiefschlafphasen und meiner Laune nach dem Aufstehen, konnte ich noch nicht feststellen. Die getrackte Dauer meines Schlafs ist allerdings ziemlich korrekt.

### GPS

Vielleicht ist GPS nicht die richtige Bezeichnung dafür. Mit [Fog of World](http://de.fogofworld.com) legt sich über die virtuelle Weltkarte ein Nebel und überall dort, wo ich gewesen bin, lichtet sich der Nebel. Im Prinzip frisst die App nur GPS-Koordinaten und entfernt an den entsprechenden Stellen den virtuellen Nebel. Einfach wie genial.

### Weiteres

Das ist noch lange nicht alles, was möglich ist. Wir haben bspw. eine Körperfettwaage, müssen alle Daten aber manuell in bspw. Lifesum eingeben. Dabei gibt es schon Waagen mit Bluetooth, die die Werte bequem synchronisieren.     
Je nach Größe des Geldbeutels, sind noch ganz andere Tracking-Spielzeuge erhältlich. Wer weiß schon, wann die Grenze erreicht ist?

### Fazit

Es gibt nicht nur wesentlich mehr Möglichkeiten als früher sich selbst zu tracken, in den kommenden Jahren werden noch viel mehr dazukommen. Kleiner werdende Computer und bessere Batterietechnologien befeuern diesen Trend.

Am tollsten sind die automatischen Dienste, bei denen man als Nutzer fast nicht in Aktion treten muss. Bei allen anderen vergesse ich ständig &quot;*den Knopf zu drücken*&quot;. Automatisch hin oder her, man sollte stets im Auge behalten *wer* auf die eigenen Daten zugreifen kann. Vielleicht muss man auch nicht jede Kleinigkeit tracken.

Wie (sinnvoll) man die Daten selbst nutzt, steht auf einem anderen Blatt. 