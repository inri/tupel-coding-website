---
layout: post
title: "Die besten Sitzplätze im Airbus A330"
date: 2015-02-12
category: reisen
tags: [Reisen]
published: true
comments: false
share: true
image: 
  feature: 2015-airbus-a330-seats-lg.png 
  credit: AIR transat
  creditlink: http://airtransat.ca
---

In den letzten 12 Monaten war ich auf sechs Langstreckenflügen unterwegs. Davor war es bestenfalls ein Kurzstreckenflug alle zwei Jahre. Neben der Verschlechterung meines ökologischen Fußabdrucks, habe ich aber auch einiges an Erfahrung mit dem wundervollen Transportmittel *Flugzeug* sammeln können. 

Man merkt sich mit der Zeit wann man wo welche Dinge vorzeigen, was man bei Kontrollen ausziehen und auspacken muss, und natürlich erahnt man eine Antwort auf die wichtige Frage: Wo sitzt man im Flugzeug (in der *Economy Class*) am besten?

Bei Flügen unter 2 Stunden ist es mir fast egal. In 3er-Reihen sitzt es sich nicht so toll zwischen zwei fremden Personen. Gang ist gut für die Füße zum Ausstrecken, aber Fenster für Fotos auch ganz brauchbar. Aber ich wollte ja etwas über den Airbus A330 erzählen…

## Generelles

Im Airbus A330 passen viele Passagiere hinein und deshalb wird er meist bei Langstreckenflügen genutzt. Bei großen Maschinen auf langen Flügen würde ich immer das hintere Drittel empfehlen. Die Airlines vergeben die Plätze von vorn nach hinten. Ist der Flug nicht ausgebucht, erwischt man vielleicht den Hauptgewinn: eine eigene 4er-Reihe (= Schlafen im Liegen!). Sitzt man weiter vorne, ist man zwar zeitiger aus dem Flugzeug raus, aber beim Zoll und Gepäckband, warten dann doch wieder alle.    
      
Selbstverständlich sind die Plätze in der Notausgang-Reihe toll, denn die haben die meiste Beinfreiheit. Die kann man aber nicht immer reservieren (und wenn doch, für viel Geld). Meist bekommen Familien mit Kleinkindern diese Plätze, oder sie werden spontan von den Flugbegleitern belegt.   
Auch sind dort die Toiletten. Ständig laufen Leute an dir vorbei oder halten dir ihre Hinterteile wartend ins Gesicht. 

## Von 4 auf 3 Sitzen

Mein Geheimtip für bequeme Sitze im Airbus A330 ist eine hintere Reihe und zwar die erste Reihe, in der sich in der Mitte statt 4 nur noch 3 Sitzplätze befinden. Im oberen Bild wäre das die Reihe 48 und die Plätze D E G, aber bei Air Berlin ist es bspw. die Reihe 47. Da gibt es scheinbar Unterschiede von Airline zu Airline bzw. dem genauen Modell der Maschine.     
Das Flugzeug wird im hinteren Teil etwas schmaler und deshalb muss eben ein Sitzplatz raus. Daraus resultiert ein Vorteil für die inneren Gangplätze der ersten drei 3er-Reihen: Der Gang ist breiter und man kann auch mal ein Fuß heraushängen lassen, ohne dass dieser den Weg des Getränkewagens blockiert. 

Viel interessanter ist aber die erste 3er-Reihe. Man hat nicht nur am Gang mehr Platz, sondern auch 10 bis 15cm mehr Beinfreiheit nach vorne [^1]. Das mag nicht nach viel klingen, aber es ist ein Unterschied wie Tag und Nacht! Ich, als 1,90m großer Mann, kann sogar gemütlich meine Beine übereinanderschlagen.     
Als weiterer kleiner Vorteil ist vielleicht der Ausklapptisch aus der Armlehne zu nennen. Ein Macbook lässt sich viel besser darauf positionieren, als auf diesen blöden Tischen an der Rücklehne des Vordermanns. Ein minimaler Nachteil ist die Position der Bildschirme an den Vordersitzen, denn man hat ja keinen direkten Vordermann, sondern sitzt zwischen zwei Plätzen. Mich stört das Drehen des Kopfs um 10° nicht. 

<figure>
	<img src="{{ site.url }}/images/2015-sitzplatz.jpg" alt="Beinfreiheit in der geheimen Reihe">
	<figcaption>Man kann es vielleicht erahnen, es war verhältnismäßig viel Platz.</figcaption>
</figure>

Das war es auch schon von meiner Weisheit. Wer mal 10 Stunden eingezwängt im Flugzeug gesessen hat, weiß die zusätzlichen Zentimeter sehr zu schätzen.     
Meist sind die Plätze für das Web-Check-In (30 Stunden vor dem Flug) noch frei. Für unseren New York Flug im Sommer, habe ich sie aber schon reserviert. 25€ ist einer dieser Plätze wirklich wert.

 [^1]: Die Beinfreiheit eines normalen Sitzplatz im Flugzeug ist am besten mit der in den neuen DB-Zügen vergleichbar, die 2er-Plätze natürlich. Da habe ich auch nur 2cm Luft zwischen Knie und Vordersitz.