---
layout: post
title: "Ein Kostenbescheid ist kein Kaufvertrag"
date: 2015-08-26
category: blog
tags: [Deutschland, Tips]
published: true
comments: false
share: true
---

Eine Erkenntnis über deutsche Behörden und Kostenbescheide.

&quot;*Der Kaufvertrag ist nach deutschem Schuldrecht ein gesetzlich normierter Vertragstyp. Er besteht aus zwei aufeinander bezogenen, inhaltlich korrespondierenden Willenserklärungen, die als Angebot und Annahme bezeichnet werden.*&quot;

Aha. Wenn man bei der Meldebehörde per Email nach einer digitalen Kopie der Meldebescheinigung fragt, erhält man einige Tage später eine **schriftliche** Kopie und einen **Kostenbescheid** in Höhe von 6 Euro.

Nein, man wird nicht darüber informiert, dass es keine digitale Kopie gibt und dass eine schriftliche Kopie 6 Euro kostet. Man muss vielleicht sogar froh darüber sein, dass es nicht mehr als 6 Euro sind.     
Scheinbar geht dem Kostenbescheid eine Sonderform eines Kaufvertrags voraus, denn die Willenserklärung war auf jeden Fall einseitig. Wer mehr darüber weiß, bitte eine Mail an mich.