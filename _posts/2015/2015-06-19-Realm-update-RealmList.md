---
layout: post
title: "Realm (0.81.1) update a RealmList"
date: 2015-06-19
category: it
tags: [Coding, Java, Android]
published: true
comments: false
share: true
---

The task is not too fancy. You have an object *Person* and this object got a RealmList *Pets*. Some time after you created a person and initially added a pets RealmList with a pet, you want to add another pet to the list. But suddenly there are just errors and the pets RealmList won’t take any more pets. 

Let’s begin with the Person class:

	@RealmClass
	public class Person extends RealmObject{       
		private String name;    
		private RealmList<Pet> pets;

		public String getName(){
			return name;}    
		public void setName(String name){
			name = name;}     
		public RealmList<Pet> getPets(){
			return pets;}     
		public void setPets(RealmList<Pet> pets){
			this.pets = pets;}     
	}

And of course the Pet Class:

	@RealmClass
	public class Pet extends RealmObject{

		@PrimaryKey
		private String key;
		private String name;

		public String getKey(){
			return key;}    
		public void setKey(String key){
			this.key = key;}       
		public String getName(){
			return name;}      
		public void setName(String name){
			this.name = name;}       
	}

You start with creating a Person object with a RealmList of pets:

	Person p = new Person();
	p.setName("Ingo");

	RealmList<Pet> pets = new RealmList<>();

	Pet d = new Pet();
	d.setKey(UUID.randomUUID().toString());
	d.setName("Dog");
	Pet c = new Pet();
	c.setKey(UUID.randomUUID().toString());
	c.setName("Cat");

	pets.add(d);
	pets.add(c);
            
	p.setPets(pets);

	realm.beginTransaction();
	realm.copyToRealm(p);
	realm.commitTransaction();

Here we creating 3 objects and at first all 3 objects are **not** in the realm yet. Finally when we do *realm.copyToRealm(p)* they are copied. Note that together with the Person object *p* both Pet objects *d* and *c* will be copied in realm too.

But how we add another pet to the *pets* RealmList later? 

One might guess something like this: 

	Pet newPet = new Pet();
	newPet.setKey(UUID.randomUUID().toString());
	newPet.setName("Bird");

	realm.beginTransaction();
	p.getPets().add(newPet);
	realm.commitTransaction();

Looks good, but sadly the result is a **java.lang.NullPointerException**. The problem is that the realm doesn’t know the new pet object. Okay, than just copy it with *realm.copyToRealm(newPet)* and add it to the RealmList afterwards (*p.getPets().add(newPet)*)! Won’t work either because *newPet* **is not** in the realm, but **a** new pet object is and we just have to find it. 

So this works: 

	Pet newPet = new Pet();
	newPet.setKey(UUID.randomUUID().toString());
	newPet.setName("Bird");

	realm.beginTransaction();
	realm.copyToRealm(newPet);
	p.getPets().add(realm.where(Pet.class).equalTo("key", newPet.getKey()).findFirst());
	realm.commitTransaction();

I hope you see that the *key* is the key. When adding a new object to a RealmList of an object, which is already in the realm, you have to first copy the new object to the realm and then get the copied object from the realm. I use the created UUID for this.

I don’t know what you think about this, but its not very intuitive. Realm is still work in progress. Pretty great work though, but some things are strange.


**Tip**: Even if you don’t have any objects for a RealmList of an object yet, create an empty RealmList and add it to the object. So you  don’t have to check for *null* every time you want to use the RealmList. Then you just test for *RealmList.size()*.