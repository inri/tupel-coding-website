---
layout: post
title: "MiBand von Xiaomi"
date: 2015-04-11
category: it
tags: [Review, Fitness, Technik]
published: true
comments: false
share: true
image: 
  feature: 2015-miband-lg.jpg 
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Soll ich im Aufmacher einen absurden Vergleich zwischen dem MiBand und der Apple Watch unterbringen? Ein Vergleich der andeutet, dass man für eine Apple Watch knapp 30 MiBänder kaufen könnte? Und dabei die Unterschiede in der Funktionsvielfalt still und heimlich unter den Tisch fallen lasse? Lieber nicht, das wäre unprofessionell.

### Das MiBand

Das MiBand ist ein einfaches Fitness-Armband. Seine Funktionen kann man an einer Hand abzählen: Schritte zählen, Schlaf überwachen, Wecker und Anruf-Benachrichtigung. Wenn man bei diversen chinesischen Social Media Diensten angemeldet ist, lassen sich diese in irgendeiner Weise auch einbinden (habe ich nicht getestet).     
Den ganzen Spaß bekommt man schon für ca. **15€ bis 25€**. 

Für diesen Preis erwartet man wenig und bekommt überraschend viel. Das Silikonarmband gibt einen Vorgeschmack auf die Sportarmbänder der Apple Watch. Angenehm zu tragen ist das MiBand auf jeden Fall, besonders dank des geringen Gewichts. Das  herausnehmbare Herzstück mit silberner Aluminium-Oberseite besitzt die MacBook-typische matte Oberfläche und hat abgeschliffen-polierte Kanten wie das iPhone 5/5s. Insgesamt ist das Design minimalistisch gehalten und gefällt gut.

Das MiBand benötigt ein Partner-Gerät: iPhone oder Android. Per Bluetooth verbunden, werden im Hintergrund Daten synchronisiert, aber erst nachdem es mit der dazugehörigen App eingerichtet wurde. 

<figure>
	<img src="{{ site.url }}/images/2015-miband-water.jpg" alt="Das MiBand unter Wasser">
	<figcaption>Das MiBand ist wasserfest, aber nicht wasserdicht </figcaption>
</figure>

### (Zu viele) Schritte zählen

Seit über einer 2 Wochen tragen meine Freundin und ich (je) ein MiBand. Abgelegt haben wir es in dieser Zeit fast nie. Der Akku hält sehr lange und nach bspw. 5 Tagen sind nur 20% verbraucht.    
Die Schritte, die gezählt werden, sind ein interessantes Thema. Wir tracken unseren zurückgelegten Wege ja auch mit der Moves App und können daher deren Schritte und zurückgelegte Kilometer mit denen des MiBands vergleichen.

Im Durchschnitt zählt das MiBand ca. 1,8x so viele Schritte wie die *Moves* App. Am 1. April waren es bspw. 4508 Schritte mit *Moves* und 7646 Schritte mit dem MiBand. Der Unterschied ist größer je weniger man tatsächlich läuft. Auch wenn man die wenigen Aktivitäten abzieht, bei denen sich das iPhone nicht am Körper befindet. Irgendwie leuchtet das sogar ein, denn das MiBand befindet sich am Handgelenk und registriert Bewegungen des Arms. Das iPhone hat zwar auch einen Schritte-Sensor, aber das trägt man nicht ständig am Handgelenk.      
Mein erster Gedanke war, dass das MiBand zwar *Schritte* sagt, aber eher so etwas wie *Bewegungseinheiten* meint. Allerdings kommuniziert es die Schritte mit Apple Health und in dieser Konstellation gibt Apple die Definition von *Schritten* vor.

Deshalb darf das MiBand keine Bewegungsdaten in mein Apple Health schreiben. Es zählt einfach zu viel und will oder kann einfache Armbewegungen nicht herausrechnen.    

### Weck mich sanft

Mein absolutes Lieblings-Feature ist das Wecken per Vibration am Armgelenk. Einerseits ist die neue non-akustische Art des Weckens interessant und andererseits die eingebaute Snooze-Funktion des MiBands. Bleibt man nach dem 4. kurzen Vibrieren einfach liegen (=bewegt den Arm nicht), folgt nach ca. 5 Minuten ein erneutes Vibrieren.        
Übrigens ist es für den Mitschlafenden im selben Bett eine Wohltat, wenn vor dessen Aufstehzeit noch kein Wecker läutet. Das Vibrieren ist relativ geräuschlos und auch nicht zu stark. Ob es mit dem sanftem Vibrieren der Apple Watch vergleichbar ist, darf bezweifelt werden, aber wie ein vibrierendes iPhone auf dem Nachttisch klingt es auch nicht.

Man kann das Vibrieren ansonsten nur für einen Anruf aktivieren. Eventuell folgt in dieser Richtung mehr, das wäre toll.

<figure>
	<img src="{{ site.url }}/images/2015-miband-arm.jpg" alt="MiBand am schlankem Arm">
	<figcaption>Das MiBand am zierlichen Handgelenk meiner Verlobten</figcaption>
</figure>

### to be contniued…

Es folgt mindestens noch ein weiter Artikel über das MiBand. Es zählt nicht nur Schritte, sondern auch die Schlafzeit und -phasen. Das wollen wir mit den Ergebnissen anderer Schlaf-Apps vergleichen.