---
layout: post
title: "Garantiefall SD-Karte"
date: 2015-08-23
category: blog
tags: [Technik]
published: true
comments: false
share: true
---

Während unseres Urlaubs in New York ist der ärgerlichste Fall für einen Fotografen eingetreten: Abends beim Durchschauen der Fotos befanden sich auf der SD-Karte plötzlich keine Bilder mehr.

Zum Glück betraf der Verlust nur einen Urlaubstag, weil ich jeden Abend ein Backup machte. Und eigentlich gab es dank meiner Wiederherstellungsmagie gar keinen Verlust. So oder so, die SD-Karte war defekt, auch wenn man im Fall der Fälle eine langwierige Wiederherstellung durchführen konnte.

Zurück in Deutschland begann ich zu recherchieren, wie man einen Garantiefall anmelden kann. SD-Karten und solche Datenträger haben ja meist eine &quot;lebenslange Garantie&quot;, die in der Realität immerhin noch 5 Jahre beträgt.     
Meine SDHC-Karte ist von Lexar (Micron) und wurde bei Amazon Frankreich gekauft. Da der Kauf knapp 2 Jahre zurückliegt, konnte ich über Amazon sowieso nichts mehr machen. Nach etwas Suchen fand ich eine Kontakt-Email-Adresse und schrieb mein Anliegen.

Eine Antwort kam überraschend schnell und sogar am Wochenende. Mir wurden einige Tips gegeben um eine vermeintlich defekte SD-Karte wieder zum Laufen zu bekommen und wenn das nicht fruchtet, bekäme ich einen Ersatz. Und den wollte ich auch.     
Ich erhielt daraufhin eine RMA-Nummer, schickte meine Karte nach Großbritannien und nach einer Woche wurde die neue Karte verschickt. Und was soll ich sagen? Sie haben mir sogar das neuere (4k-ready) Modell geschickt.

## Fazit

Natürlich dauerte der gesamte Prozess insgesamt 2 Wochen, aber ich war wirklich überrascht wie unkompliziert es am Ende war. Ich bin sehr zufrieden mit den Service von Lexar (Micron).    

Datenträger fallen aus, aber nehmt ruhig die Herstellergarantie in Anspruch. Ein Kaufbeleg ist nicht immer erforderlich und eventuell ist der Ersatz aus der aktuellen Modellreihe.