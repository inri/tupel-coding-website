---
layout: post
title: "Dominikanischer Wegezoll"
date: 2015-02-06
category: reisen
tags: [Reisen, Dominikanische Republik]
published: true
comments: false
share: true
---

Es ist Nachmittag und wir fahren eine mittelgroße Landstraße auf den Weg nach Santiago entlang. Schon von Weitem sehen wir die beiden Polizisten, die Autos anhalten und kontrollieren. Ein freundliches Lächeln aufsetzen und hoffen, dass es nur Routine ist. Wir werden angehalten und ein Polizist kommt zum Fahrerfenster. 


Zum Glück hatten wir einen Muttersprachler dabei und das folgende Gespräch hat er für uns geführt.

> Hallo die Herren. Wo kommen Sie denn her?

Wir lächeln freundlich und geben die Auskunft. Aus Deutschland kommen wir. 

> Sie fahren doch nicht unter Alkohol- oder Drogeneinluss?

Man sieht sofort, dass wir alle nüchtern sind. Selbstverständlich verneinen wir die Frage. Dann kommt der Paukenschlag.

> Wir wäre es mit einer finanzielles Großzügigkeit?

Ja. Das hat der Polizist uns gerade ins Gesicht gefragt. Wir kramen 500 Pesos (ca. 10 Euro) hervor, er bedankt sich und wünscht eine gute Weiterfahrt.

Für 500m ist Stille im Auto, dann fallen wir aus allen Wolken. Man wusste es zwar schon, aber dass die Polizei, zumindest hier auf dem Land, so dreist ist, erstaunt uns sehr. Er hat sich nicht einmal Mühe gegeben eine halbwegs plausible Begründung zu finden. Eventuell hat er auch sofort die Brieftasche neben der Gangschaltung liegen sehen. 

Ein anderer Bekannter hatte uns erzählt, dass in der Hauptstadt Santo Domingo die Korruption der Polizei sehr zurückgegangen ist. Wenn man früher beim Überfahren einer roten Ampel ertappt wurde, reichten einige tausend Pesos und man durfte weiterfahren. Heute muss man einen Lehrgang besuchen (ähnlich unserer MPU) und wird mit Bestechung nicht weit kommen. Auf dem Land scheint nicht nur das noch sehr gut zu funktionieren, sondern auch das Einziehen des Wegezolls.

Andere Länder, andere Sitten.