---
layout: post
title: "Spoilerfreies Lesen"
date: 2015-06-18
category: blog
tags: [EBooks, iPhone]
published: true
comments: false
share: true
---

Ich hörte den Begriff [*Spoiler*](https://de.wikipedia.org/wiki/Spoiler_(Medien)) als erstes in Verbindung mit Fernsehserien. Zu jeder guten Serienkritik gehört mindestens eine *Spoiler-Warnung* an einer Stelle, die bestimmte Handlungen detaillierter diskutiert. Mein Titel ist etwas überspitzt formuliert, geht aber in die gleiche Richtung.   

Ich las vor einigen Tagen den Roman *Der Marsianer* von [Andy Weir](https://de.wikipedia.org/wiki/Andy_Weir). Das Buch ist genial, auch wenn ihr nicht so sehr auf SciFi steht, unbedingt lesen! Die Handlung ist simpel wie mitreißend: Ein Astronaut wird von seiner Crew auf dem Mars zurückgelassen und muss ums Überleben kämpfen. Das macht er mit viel Einfallsreichtum und Humor. Von Anfang an schwebt aber die Frage im Raum: Kann er überleben? Wird er jemals zurück zur Erde reisen können?      
Und wie das in einem spannenden Roman so sein muss, gibt es Fortschritte und Rückschläge für die Hauptperson. 

Das Buch las ich komplett auf meinem iPhone 5s in der iBooks App. Meist tat ich das nachts und das sah dann ungefähr so aus:

<figure>
	<a href="/images/2015-ibook-marsianer.jpg">
	<img src="{{ site.url }}/images/2015-ibook-marsianer.jpg" alt="iBooks Der Marsianer"></a>
	<figcaption>iBooks bei Nacht</figcaption>
</figure>

Was fällt euch auf und worauf will ich mit meinem Titel (*Spoilerfreies Lesen*) hinaus?     
Man sieht zwar ganz leicht eine (berechnete) Seitenanzahl, aber man weiß nicht an welcher Stelle des Buchs man sich befindet. Würde ich ein Buch aus Papier lesen, wüsste ich ziemlich genau wann das Ende naht. Wenn der Hauptperson ein Unglück geschieht und ich nur noch 10 Seiten zwischen meinen Fingern habe, kann ich ziemlich sicher davon ausgehen, dass es das gleich war. Umgekehrt weiß ich bei der Mitte des Buches, dass kann nicht so tragisch sein, wenn seine Luftschleuse explodiert. 

Mich traf diese Erkenntnis im letzten Viertel des Romans und ich  freute mich ganz einfach darüber. Ich wusste nicht wie viele Seiten noch kommen und es war einfach ein tolles Leseerlebnis. 

Natürlich sieht man beim Öffnen der App für einige Sekunden den seitlichen Balken mit dem Fortschritt und auch sobald man den Fokus wegnimmt. Aber das ist relativ ungenau und lässt sich gut ignorieren.      
Nun bräuchte ich nur noch einen etwas größeren Bildschirm…