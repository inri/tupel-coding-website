---
layout: post
title: "On Device Testing für alle!"
date: 2015-06-08
category: it
tags: [XCode]
published: true
comments: false
share: true
---

Mich haben die vorgestellten Neuerungen auf der WWDC-Keynote nicht vom Hocker gehauen. Das mit Abstand beste kam danach und zwar ist das die kommende Version 7 von XCode. 

Die könnt ihr euch jetzt schon schon als Beta anschauen und ausprobieren: [XCode 7](https://developer.apple.com/xcode/)

Warum ist die so toll? Endlich kann man selbst programmierte Apps auch auf den eigenen Geräten testen! Das konnte man bis gestern nur im Verbindung eines 99$-Developer Account, heute benötigt man nur noch eine Apple ID. Das ist großartig. 

Kleiner Tip: Wer wie ich mal einen solchen Developer Account hatte, dieser aber mittlerweile ausgelaufen ist, sollte die damit verbundene Apple ID nicht in XCode verwenden. Am besten eine neue Apple ID anlegen und dieser kann XCode dann das *Free Provisioning* zuordnen. 

Viel Spaß und happy Coding!