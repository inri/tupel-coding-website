---
layout: post
title: "Die gute S-Linie"
date: 2015-09-25
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Ich bin seit dem iPhone 4S dabei und wechselte alle zwei Jahre auf das neuste Modell. Einige wenige Worte als Begründung.

[Gruber in seinem iPhone 6S Review über die S-Linie:](http://daringfireball.net/2015/09/the_iphones_6s)

> For a typical iPhone user on a two-year upgrade cycle, I think the S years are the better phones, historically.

Genau so ist es! Ich bin zwar erst seit dem iPhone 4S dabei, aber mir wurde schnell klar, dass die S-Linie die bessere von beiden ist.

Natürlich ist ein neues Design spannend, aber oft gibt es Kinderkrankheiten, die eben erst nach einer gewissen Zeit gelöst werden. Ob berechtigt oder nicht, ich verweise auf *Antennagate*  (iPhone 4) und *Bendgate* (iPhone 6).     
Da die S-Linie nicht mit einem neuen Design aufwarten kann, müssen andere und oft sinnvollere Features her. Die grandiose Kamera des 4S oder das TouchID im 5s lassen die Vorgänger sofort älter als ein Jahr aussehen. Dieses Jahr gibt es neben einem großen Kamera-Update eine neue Bildschirmtechnologie und einen enorm großen Performance-Sprung. 

Lange Rede kurzer Sinn: Die S-Linie des iPhones ist die bessere. So.