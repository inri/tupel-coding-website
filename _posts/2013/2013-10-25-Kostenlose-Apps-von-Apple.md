---
layout: post
title: "Kostenlose Apps von Apple"
date: 2013-10-25 00:00:01
category: it
tags: [Apple, App]
published: true
comments: false
share: true
---

Als auf der Apple Keynote am Dienstag verkündet wurde, dass OS X 10.9 aka *Mavericks* kostenlos sein wird, konnte ich mir ein Grinsen nicht verkneifen. Ebenfalls kostenlos ist nun auch Apples iLife und iWork Suite, für OS X und iOS. 

In der Softwareentwicklergemeinde wurde diese Entscheidung mit gemischten Gefühlen aufgenommen. Ich glaube es ist eine Art Angst, dass die potentiellen Kunden von den Gratis-Apps so verwöhnt sein könnten, dass sie nicht mehr bereit sind überhaupt Geld für Apps auszugeben.    
Ich denke das ist eine unbegründete Angst.

### Gratis Betriebssystem

[Kevin Hoctor gab beispielsweise zwei Gründe an]("https://alpha.app.net/kevinhoctor"), warum Apple den Entwicklern mit dieser Entscheidung geholfen hat. 

>Mavericks being free will speed adoption and let us target it earlier. 

Zum Betriebssystem ist folgendes zu fragen. Wie viele normale Nutzer haben sich jemals ein Betriebssystem direkt gekauft? Sowohl Windows als auch OS X waren und sind stets auf den Computern vorinstalliert. Noch vor einigen Jahren erschienen Betriebssysteme außerdem nicht jährlich, sondern in größeren Zeitabständen, meist alle 3 Jahre.

* 1998: **Windows 98**
* 2001: **Windows XP** 
* 2006: **Windows Vista**
* 2009: **Windows 7**
* 2012: **Windows 8**

Benutzer, die großen Wert auf ihre Computer legen oder sie beruflich benötigen, kaufen sich sowieso alle 2 bis 3 Jahre einen neuen inklusive neuster System und der Familien-PC kann gerne mal eine Version überspringen. Jetzt plötzlich jedes Jahr für ein Update Geld zu bezahlen, ist nicht sehr verständlich und damit steigt indirekt der Aufwand für Softwareentwickler.    

Wenn Nutzer ihre Systeme nicht auf den neusten Stand halten, muss man die eigene Software für eine Vielzahl an Systemversionen bereitstellen und auf coole Features verzichten.
In weniger als 24 Stunden haben mehr Nutzer auf *Mavericks* geupdatet als vor einem Jahr nach einer Woche auf *Mountain Lion*. Das ist eine gute Nachricht.

### Gratis Apps

Apple Hardware ist gut, aber sie hatte schon immer ihren Preis. Wer kann mir versprechen, dass das neue iPhone nicht hätte auch 20€ billiger sein können, wenn Apple die iOS-iWork Apps nicht gratis gemacht hätte? Ich denke Apple wird deshalb nicht untergehen. Für sie ist aber der Nutzen größer, denn als Käufer bekomme ich auf diese Weise mehr und das macht mich zufrieden. Punkt.

Der zweite Grund von Kevin Hoctor deutet auf einen anderen Umstand hin:
    
>Free Apple apps are easier to dismiss as hardware discounts than $10 apps, which made our apps look over-priced.

Ein komplettes Word Programm oder eine Fotoverwaltung für unter 10€ anzubieten ist doch der eigentlich Hohn gegenüber normalen Softwareentwicklern.    

Ich denke die Käufer sehen diese kostenlosen Apps als Gesamtpaket mit der Hardware und dem Betriebssystem. Für meine Email, Kalender und Browser muss ich schließlich auch nichts bezahlen. Zumindest nicht direkt.    
Man kann sich darüber streiten welche Art von Programmen im Umfang dabei sein sollten, aber Apple hatte eben diese im Portfolio.

### Fazit

Ich habe mich darüber gefreut und ich weiß dass auch einige meiner IT-Kollegen sich darüber freuen oder zumindest Genugtuung empfinden. Dass Apple plötzlich jedes Jahr 20€ für einige Systemupdates haben wollte, stieß auf wenig Begeisterung. 

Vielen anderen Kunden wird diese Neuigkeit gar nicht bewusst sein. Sie werden irgendwann den App Store ansteuern und entweder Geld für Apps ausgeben, oder nicht. Ob Apples Apps nun Geld kosten oder nicht, wird an dieser Entscheidung nichts ändern.
