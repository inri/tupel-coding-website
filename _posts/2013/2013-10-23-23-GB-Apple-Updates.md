---
layout: post
title: "Gigabyteweise Apple Updates"
date: 2013-10-23 08:00:00
category: it
tags: [macOS, OS X, iOS]
published: true
comments: false
share: true
---

Was für eine Update-Flut! Da kommen ganz schön viele Daten zusammen. Ich habe fünf Apple Geräten die Updates verpasst (MBPr, MBA, iPhone 4S, iPhone 5S, iPad mini) und insgesamt belief sich das auf ca. 23 GB. Wow.

#### Updates für OS X

* 5,30 GB Mavericks
* 1,94 GB iMovie (2x)
* 1,15 GB iPhoto (2x)
* 749 MB Garageband (2x)
* 90 MB iTunes (2x)
* 237 MB iBooks Author
* 1,24 GB XCode 
* 290 MB Pages
* 444 MB Keynote
* 190 MB Numbers

**= 15,56 GB** 

#### Updates für iOS

* 266 MB Pages für iOS (2x)
* 234 MB Numbers für iOS
* 635 MB iMovie für iOS (2x)
* 463 MB Keynote für iOS (2x) 
* 287 MB iPhoto für iOS (2x)
* 610 Mb Garageband für iOS (2x)
* 1,40 GB iOS 7.0.3 für iPhone 5s
* 1,10 GB iOS 7.0.3 für iPad mini

**= 7,26 GB**


#### Anmerkung

Garageband lädt nach der Installation weitere Daten in unbekannter Höhe nach.    
Auf dem iPad hatte ich iMovie, Pages, Keynote und iPhoto gar nicht mehr installiert, da hat er zuerst die alte Version und danach das Update geladen. Ich habe aber nur das Update gezählt.   
Das 7.0.3er Update für das 4S war seltsamerweise sehr klein.
