---
layout: post
title: "Kurze Zusammenfassung zur iPhone Keynote"
date: 2013-09-11 09:00:00
category: it
tags: [Apple, iOS, iPhone]
published: true
comments: false
share: true
---

Am gestrigen Dienstag habe ich zusammen mit meiner Freundin unsere neue Wohnung gestrichen und war erst spät am Abend zurück. Glücklicherweise war das Video zur Keynote schon online und da ich Twitter und Co gemieden habe, konnte ich es spoilerfrei anschauen.   



## Warm Up

Tim Cook kommt auf die Bühne und sagt uns mit leicht zittriger Stimme, dass sie [Apple] sich darauf freuen uns an diesem Morgen einige Dinge zu zeigen, auf die sie stolz sind. So beginnt doch jede Keynote, oder? Tim, nicht weniger haben wir sowieso von euch erwartet.

Leider müssen wir vor der Vorstellung eines neuen iPhones einige allgemeine Apple-Informationen über uns ergehen lassen. Vielleicht ist das ein ungeschriebenes Gesetz um den Apple-Gott wohlzustimmen?

* Zuerst das wirklich tolle iTunes Festival, welches natürlich wieder gestreamt wird. Ist zwar auch nichts anderes als Werbung für Apple, aber auf diese Art und Weise sehr erträglich.
* Eine Apple Store Vorstellung darf natürlich auch nicht fehlen. Yeah, Retail News! 
* Zahlen sind ebenfalls Pflicht: Nächsten Monat wird das 700. Millionste iOS-Device verkauft.

Schwupps sind wir schon nach nur 10 Minuten bei iOS 7. Das ging schnell.

## iOS 7

Federighi stellt einige Features von iOS 7 vor. Das meiste (oder sogar alles?) ist bereits aus den Betas bekannt. Design, Controlls, Siri, Töne, Kamera, Musik/iTunes Radio,… über 200 Features. Die Beispiele gab es nur im iPhone-Format zu sehen. Es wird zwar zwischen iPhone und iPad keine großen Unterschiede geben, trotzdem etwas verwunderlich.   
iOS 7 in 6 Minuten abgehandelt, bleiben noch knapp 70 Minuten übrig.

## Apps von Apple

Es geht weiter mit dem 3er-Gespann von iWork, iMovie und iPhoto. Sie werden kostenlos! Schöne Neuigkeiten und noch bessere wäre es, wenn ich bei den anderen Apple Apps auch eine Wahl hätte (ja, ich meine dich Newsstand!). Aber davon ist nicht die Rede, denn es geht mit dem iPhone weiter.

## iPhone 5C

Tim erklärt wieso es dieses Jahr zwei neue iPhones bzw. Designs geben wird und die alten Modelle auslaufen. Wissen wir doch schon. Phil Schiller stellt die neuen Geräte vor.   
Den Anfang macht das iPhone *5C* und obwohl Schiller einräumt, dass es das eine oder andere Bild schon im Web gab, haben wir es bestimmt nicht so wie im Apple Video gesehen. Mh, doch. Die Farben kennen wir. Neu sind allerdings die bunten Hüllen mit Löchern auf der Rückseite.

Die inneren Werte sind ebenfalls wie erwartet. Gleicher Bildschirm, gleiche Kamera und gleicher A6 Prozessor wie beim alten Modell. Der Akku ist wohl etwas größer, hört hört! Ansonsten scheint es wohl viele LTE-Bänder abzudecken und nicht wieder in mehrere iPhone-Versionen pro LTE-Regionen unterteilt zu sein. Und es sieht mit iOS 7 natürlich unglaublich schön aus. 

Aber wieso beginnt die Speichergröße bei 16 GB? Ist das nicht mittlerweile sogar für Normalnutzer zu wenig?

## iPhone 5S

Okay, bleiben noch gute 50 Minuten für die zweite Version. Ist ja heute relativ zügig.
Schiller kündigt es als das &quot;*most forward-thinking Phone ever*&quot; an und das kurze Video zeigt den neuen Homebutton und die neue goldene Farbvariante. Es hat viele Innovationen, aber nur drei werden jetzt vorgestellt:

* Die Performance ist natürlich besser geworden und zu verdanken haben wir das einem A7 Chip. Einem 64-bit A7 Chip! Das ist wirklich cool und ich bin schon sehr auf die ersten Berichte gespannt. Der im A7 integrierte M7 Motion Chip verarbeitet die Informationen der Sensoren und das dürfte auch der Akkulaufzeit gut tun. Diese ist scheinbar nur gleich geblieben. Sehr schade.

* Die Kamera wurde auch überarbeitet und deckt sich teilweise mit den bekannten Informationen: größerer Sensor, 5 Linsen, die Software bearbeitet die Bilder gleich automatisch, Bildstabilisator, Slow-Motion Funktion und ein neuer 2er Flash mit zwei verschieden-farbigen Blitzlichtern, die sich der Fotoumgebung anpassen. Cool, cool. Scheint mir insgesamt ein größeres und besseres Update zu sein als vom 4S zum 5.

* Security! Oh… ein heikles Thema in diesen Tagen. Die Lösung für die faulen Benutzer, die keinen Pin-Code nutzen möchte: der Touch ID Sensor. Und die Überraschung: Er sitzt im Homebutton. Nützlich ist er aber nicht nur beim Anschalten des iPhones, sondern auch beim Kauf von Apps und sicherlich bald noch anderen Dingen. 

Tim ist wieder auf der Bühne und zeigt einen weiteren iPhone 5C Werbeclip. Am Ende gibt es wieder Musik, diesmal von Elvis Castello.   
Und dann war es das auch schon.

## Sidenotes

Tim hat sich manchmal verhaspelt, fast so als ob er seinen Text nicht perfekt auswendig gelernt hat.   
Federighi war wieder amüsant und bekam die meisten Lacher.
Schiller machte auch einen guten Witz als er aufzählte welche Schadstoffe nicht im iPhone enthalten sind (PVC-free,… -free,…) und sich am Ende ein grinsendes &quot;*…and of course Android-free*&quot; nicht verkneifen konnte.   
Auch ein schöner trockener Kommentar von Schiller zum Touch ID Sensor: &quot;*Es wäre keine neue Killer-Technologie, wenn es kein Video gäbe das uns alles darüber erzählt.*&quot;.   

Ohne genau hinzuschauen, sieht der Standard-Homescreen von Weitem schon etwas unharmonisch aus. Nicht dass es wirklich bedeutend ist, aber es fiel während der Keynote immer mal wieder auf.  
Erstmalig werden die iPhones auch in China sofort verfügbar sein. Vielleicht ist das der Grund, warum so viele Informationen geleakt sind - es mussten einfach viele hergestellt werden und das dauert eben seine Zeit. Je mehr Zeit vergeht, desto eher wird etwas öffentlich.    
Kein Wort zu NFC.    


 


