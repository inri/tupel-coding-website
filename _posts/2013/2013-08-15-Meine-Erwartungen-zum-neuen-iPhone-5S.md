---
layout: post
title: "Erwartungen an das neue iPhone"
date: 2013-08-15 12:00:00
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Die Gerüchteküche brodelt seit Wochen und nun gibt es ein erstes Datum an dem Apple das neue iPhone, vermutlich das iPhone **5S**, vorstellen wird. Anfang September scheint mir eine sichere Wette zu sein und zum Glück komme ich einige Tage vorher aus dem Urlaub zurück.   


Es schwirren schon so einige Details zum neuen iPhone herum und deshalb werde ich meine Mutmaßungen und Wünsche auch mal kundtun.

### Fingerabdruckscanner

Apple hat eine Firma gekauft, die sich mit dieser Technologie auskennt. Aber insgesamt soll die Technik noch nicht weit genug sein oder hat heute irgendjemand noch ein Notebook mit Fingerabdruckscanner? Die bisherigen Scanner nutzen sich mit der Zeit ab und das ist natürlich kein erwünschter Effekt. Was auch immer wir von Apple im September oder später erwarten können, es wird anders sein als die Fingerabdrucktechnik, die man bisher gesehen hat.   

Für einige stellt sich die Frage: *Braucht man das?* - ich denke ja, aber so denke ich erst seit kurzem. Ich habe bis vor einigen Wochen meine i-Geräte nicht mit einem Pin/Passwort geschützt. Seit ich das aber mache, nervt ich das ständige Eintippen des Codes. Wenn ein flinker Scanner im Home-Button dafür sorgen würde, dass man sich automatisch autorisiert, dann wäre das ein großer Gewinn für die Usability **und** Sicherheit.

### Akkulaufzeit

Über die meisten Eckdaten der Hardware kann man verschiedener Meinung sein, aber ich habe bisher noch niemanden gehört, der die Akkulaufzeit des iPhone in Ordnung findet. Fakt ist: Die 8 Stunden Internetnutzung mit 3G sind zu wenig. Mit iOS 7 kommt zwar eine simple Methode das 3G schnell zu deaktivieren, aber das wird das Problem nicht vollständig beseitigen. Liebe Ingenieure bei Apple, 10 bis 12 Stunden Akkulaufzeit wären wirklich grandios!

### Kamera

Das ist weniger ein Wunsch als viel mehr eine sichere Erwartung. Der Sprung zu 10 Megapixel wird wohl passieren und einen besseren Blitz kann man auch gut gebrauchen. Ansonsten sind mit vor allem Geschwindigkeit und ein guter Sensor wichtig. Für richtig fantastische Fotos habe ich eine etwas größere Kamera (später mehr dazu).

### iOS 7

Ich bin gespannt wie viel Überraschung im endgültigem iOS 7 stecken wird, aber was man bisher sieht, scheint gut zu werden.

### NFC

Spoiler: Ich habe dank meiner neuen Kamera endlich Kontakt zu diesem NFC und auch wenn das Feature sicherlich kein wirklich wichtiger Grund für NFC ist, kann Apple in diesem Jahr vielleicht nachrüsten. Wie das technisch passieren soll, sei mal dahingestellt.

### Größe &amp; Form bzw. iPhone 5C

Alles bleibt so wie es ist. Beziehungsweise wird es zusätzlich ein iPhone **5C** geben.    
Was haben wir da zu erwarten? Technisch ein iPhone auf dem Stand eines 4S, aber mit der Displaygröße eines iPhone 5, Lightning Connector und natürlich einem billigeren Gehäuse aus Kunststoff. Wegen mir könnte Apple auch dieses Kunststoffgehäuse für den iPod Touch verbauen, der mit 320€ schließlich auch ganz schön teuer für einen MP3-Player ist, überspitzt gesagt.

### Weiteres

Mir fällt nichts weiter ein. Es gibt auch keine tollen Features bei anderen Smartphones, die ich mir wünschen würde. Mit den oben genannten Verbesserungen würde man ein ziemlich gutes Smartphone haben.    

Wir können also gespannt dem September entgegen fiebern.