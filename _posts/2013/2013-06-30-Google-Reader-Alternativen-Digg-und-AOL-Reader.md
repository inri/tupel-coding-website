---
layout: post
title: "Google Reader Alternativen: Digg & AOL Reader"
description: ""
date: 2013-06-30 
tags: [RSS]
category: it
published: true
comments: false
share: true
---

Ich war mir nicht sicher, ob ich vor dem offiziellen Ende des Google Readers noch diese beiden Alternativen in Augenschein nehmen kann. Doch dann purzelten die Einladungen in meinen Email-Eingang und ich konnte mir die neuen Reader endlich anschauen.      
Das wird kein langer Beitrag, nur einige Beobachtungen. Bei Interesse entweder [AOL](http://reader.aol.com/) oder [DIGG](http://digg.com/reader) klicken.

* Beide Reader bestechen durch ein klares Design, angelehnt an den guten alten Google Reader. Also in der linken Seitenleiste die RSS-Listen, rechts die Artikel. 
* Insgesamt wirken sie moderner als Feedbin (vermutlich weil sie heller sind).
* Bei Digg gibt es scheinbar keine Counter für ungelesene Artikel. Das ist unschön.
* Dafür hat Digg meine Abonnements korrekt (sortiert und geordnet) von Google übernommen.
* Digg nutzt serifen - und Aol serifenlose Schrift. Erstere in schwarz, letztere in dunkelgrau. 
* Digg hat iPhone und iPad Apps, wie auch Feedly, welche wie die Website aussehen. 

## Fazit

Ich mache es kurz: Feedbin hat es unter diesen beiden schwer. Sie sind einfach genauso gut und kosten nichts. Zudem hat Digg gleich die Apps am Start, was definitiv ein Nachteil für AOL ist. Feedbin ist zumindest in vielen ReadLater Apps zu finden. Feedly hebt sich von ihnen ab und ist u.a. auch deshalb mein Favorit. Aber in den nächsten Monate kann sich noch was entwickeln und eventuell wechsle ich.