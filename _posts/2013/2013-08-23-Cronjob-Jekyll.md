---
layout: post
title: "Cronjob für Jekyll"
date: 2013-08-23 19:15:00
category: it
tags: [Tupel Coding]
published: true
comments: false
share: true
---

Achtung Achtung! - Das **war** nur eine Testpost. Oder doch nicht?

Für den Blog hier schreibe ich meist mehrere Artikel parallel und habe deshalb zu viele Artikel fertig um sie alle sofort zu veröffentlichen. Bei Blogger konnte ich einen Veröffentlichungsdatum mit Zeitpunkt pro Artikel festlegen. Leider funktioniert das bei Jekyll nicht.

Man kann durchaus beim Attribut *date* ein Datum und eine Uhrzeit festlegen, aber dieses Attribut ist nur bei der Generierung der Website von Bedeutung. Da ich den Git Hook *post-receive* verwende, generiert Jekyll die Seite nur neu, wenn ich eine Änderung hochlade. Möchte ich dass ein Artikel morgen erscheint und lade ihn heute schon hoch, dann wird er nicht generiert und erscheint nicht, denn dafür müsste Jekyll die Website morgen nochmals generieren.

Unter Umständen übersehe ich die richtige Jekyll-Funktion dafür. Falls das der Fall sein sollte, sagt mir bitte Bescheid. Ansonsten wäre die Lösung ein Cronjob, der das Generieren täglich oder alle 3 Stunden ausführt, die Lösung. 

###Nachtrag

Mein Cronjob funktioniert und so sieht er aus:

    * */3 * * * path/to/your/git/hooks/post-receive

Damit wird alle 3 Stunden der *post-receive* Hook ausgeführt. Natürlich kann man ein neues Skript schreiben, aber der Hook tut ja bereits das gewünschte.

Wenn euch ebenfalls die Emails auf die Nerven gehen, die nach jedem Cronjob verschickt werden, dann deaktiviert sie mit:

    MAILTO=""

Diese Zeile muss **vor** dem Cronjob gesetzt werden, auf den sie sich beziehen soll. Also in meinem Fall:

    MAILTO=""
    * */3 * * * path/to/your/git/hooks/post-receive

Weitere Informationen zu Cronjobs natürlich bei [Uberspace](http://uberspace.de/dokuwiki/system:cron).
