---
layout: post
title: "Kastanie im Herbst"
category: blog
tags: [iPhone, Fotografie]
comments: false
share: true
image:
  feature: 2011-kastanie-original-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
thumb: 2011-kastanie-200.jpg
---

Während meines Bachelorprojekts habe ich das HPI meist über den seitlichen Eingang verlassen. An diesem Tag bin ich aber über den Haupteingang nach draußen gegangen und sah die große Kastanie im herbstlichen Licht getaucht. 


Ich glaube ich hatte es eilig, deshalb machte ich nur einen Schnappschuss und ging weiter. Später erst staunte ich über das tolle Bild. 

<figure class="half">
	<a href="/images/2011-kastanie-original-1600.jpg">
		<img 
		  src="/images/2011-kastanie-original-400.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
	<a href="/images/2011-kastanie-1600.jpg">
		<img 
		  src="/images/2011-kastanie-400.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
	<figcaption>Links das Original, rechts mit Filter</figcaption>
</figure>