---
layout: post
title: "Under the Dome. Staffel 1 bis Episode 9"
date: 2013-08-29
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-under-the-dome-lg.jpg
  credit: CBS
  creditlink: http://www.cbs.com/shows/under-the-dome/
---

Eines schönen Tages schießt ein durchsichtiges Kraftfeld halbkreisförmig aus der Erde und schließt die Kleinstadt Chesters Mill in sich ein.

## Um was geht es?

Niemand weiß was dieses Kraftfeld ist, oder woher es kommt oder wer dahinter steckt. Die eingeschlossenen Menschen, nicht nur, aber größtenteils Bewohner der Stadt, müssen irgendwie damit klarkommen. Und wie das so ist, machen zahlreiche Geheimnisse die ganze Sache nicht leichter. 

Man könnte jetzt einige Charaktere aufzählen, aber die Archetypen sind eigentlich bekannt. Ob der Anführer, der Held, die Neugierige oder der Kluge,... sie alle finden sich auch in *Under the Dome* wieder.

Interessanter ist die sicherlich aufkommende Frage *Was passiert dort?*, doch eine Antwort wird es bestimmt nicht so schnell geben. Die Serie fühlt sich nach 9 Episoden so an wie *Lost* und dieser Vergleich ist sicherlich nicht verkehrt, immerhin basiert sie auf einen Stephen King Roman. Abgesehen von dem Kraftfeld, haben einige Kinder seltsame Anfälle bei denen sie zucken und immer die gleichen Sätze wiederholen, irgendwas mit fallenden Sternen, und mit dem Kraftfeld in Verbindung stehen. 

Es gibt viele Fragen und mit jeder Episode kommen neue hinzu. Daneben geht das Leben weiter und führt hier und da zu Komplikationen, schließlich müssen sich die Menschen in ihrer neuen Situation erst zurechtfinden.

## Was gefällt?

SciFi-Stories finde ich immer interessant und genau wie bei Lost möchte ich gerne die Auflösung erfahren. Die große Story schreitet langsam aber stetig voran und die Nebenhandlungen sind auch sehenswert.

Sehr erstaunlich ist die Serie in Bezug auf Todesopfer. Ich hatte den Eindruck, dass der Anteil der Toten bisher relativ hoch war. Im Prinzip genau wie ich es mir im echten Leben vorstellen würde. Da ist *Under the Dome* mit *The Walking Dead* wesentlich enger verwandt als mit *Falling Skies* und das ist definitiv ein Pluspunkt (obwohl Steven Spielberg *Under the Dome* auch mit produziert).

## Fazit 

Eine spannende und unterhaltsame SciFi-Serie. Wer das Genre mag, der wird auf seine Kosten kommen.