---
layout: post
title:  "Breaking Bad Season 2.2 Auftakt"
date: 2013-08-13 18:10:00
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-breaking-bad-lg.jpg
  credit: AMC
  creditlink: http://www.amctv.com/shows/breaking-bad
---

Lange, lange mussten wir warten, aber nun beginnt endlich der finale Abschluss von Breaking Bad. Das Ende des ersten Teils ließ uns mit einem heftigen Cliffhanger zurück. 


Hank dämmerte es, dass sein Schwager Walter der mysteriöse Drogenbaron Heisenberg sein könnte. Die Karten sind neu gemischt und Walter nicht mehr sicher. Oder doch? Wie wird sich diese Erkenntnis auswirken? 

Wie schon im ersten Teil, blicken wir für einige Minuten in die Zukunft und sehen einen einsamen, bärtigen Walter, der in den Ruinen seines einstigen Hauses einbricht um das in der Steckdose verstecke Glasröhrchen mit dem Gift Rizin zu holen. Am Ende dieser Szene sieht er das verzerrte Bild seiner selbst im Spiegel. Was ist mit Walter White passiert?

Der Auftakt spielt nicht versteckt mit den Motivationen der einzelnen Charaktere, sondern zeigt sie klar auf:

### Hank
Hank flüchtet regelrecht nach Hause um seinen Verdacht zu bestätigen, fast bricht er auf dem Weg dorthin zusammen. Am nächsten Tag sind die Akten da und die Suche nach Beweisen beginnt. Später erfahren wir, dass sogar schon Walters Auto verwanzt wurde. 

### Lydia
Lydia stattet Walter einen Besuch ab und möchte von ihm Hilfe für die Meth-Produktion, da die Qualität immer weiter sinkt. Nicht nur er schickt sie weg, sondern Skyler persönlich jagt sie vom Hof der Waschanlage.   

### Jesse

Jesse macht sein moralischer Ballast schwer zu schaffen. Er möchte den Eltern des getöteten Jungen und der Enkelin von Mike sein Geld geben, aber Saul sagt Walter Bescheid und dieser besucht Jesse sofort und redet auf ihn ein. Dabei verrät Jesse ihm, dass er denkt, dass er Mike getötet hat, doch Walter streitet es so lange ab, bis Jesse im glaubt. Nicht. Natürlich glaubt er ihm nicht und vermutlich realisiert er, dass Walter ihn nicht das erste Mal angelogen hat. Wie viel Gefahr droht ihm? Jesse will auf keinen Fall das Geld und verteilt es nachts in der Stadt.  

### Walter

Walter ist wieder in Therapie wegen seiner Krebserkrankung und hat mit den Nebenwirkungen zu kämpfen. Er bemerkt beim Erbrechen, dass ein Buch im Bad fehlt und beginnt zu ahnen, dass zwischen der plötzlichen Übelkeit von Hank und dem Verschwinden des Buches ein Zusammenhang besteht. Er gibt seiner Paranoia nach und sucht mitten in der Nacht sein Auto ab. Und wird fündig.      
In der letzten Szene kommt es überraschend zu einer Aussprache zwischen Hank und Walter. Walter konfrontiert Hank mit dem Fund an seinem Auto und dieser nutzt vielleicht aus Verzweiflung die Situation um Hank niederzuschlagen. Hank möchte Walter ins Gefängnis bringen, aber wie viele Beweise hat er? Walter argumentiert, dass er wegen seines Krebs sowieso nie ins Gefängnis gehen wird und Hank die ganze Ermittlung sein lassen soll.

Die Weichen sind gestellt, die Motivationen klar zu erkennen und Walters Kartenhaus beginnt schon an einer Ecke zusammen zu fallen.     
Wird Walter doch wieder zu Heisenberg oder bleibt er aus dem Geschäft? Wie wird sich Hank verhalten, selbst wenn er keine Beweise findet. Er weiß, dass Walter Heisenberg ist. Wie lange ist Jesse noch zu kontrollieren? Ich würde noch die Frage nach dem Krebs stellen, aber der Ausblick scheint diese schon zu beantworten. 

Es wird eine spannende Staffel.