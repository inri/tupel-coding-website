---
layout: post
title: "Schummeln beim Programmieren"
description: "Wenn ich mir meine Programmierfragen durch das Internet beantworten lasse, fühlt sich das wie Schummeln an."
date: 2013-01-11 
category: it
tags: [Coding]
published: true
comments: false
share: true
---

Zur Zeit arbeite ich an einer einfachen Website mit HTML, CSS und jQuery. Wie das so ist, muss ich in regelmäßigen Abständen Antworten auf meine Fragen suchen und meist fühle ich mich dabei so, als würde ich schummeln.      

Genauso ist es auch beim Programmieren, obwohl es seltener passiert. Ich weiß, man kann unmöglich alle Befehle, Attribute und Geheimnisse einer Programmiersprache kennen und deshalb geht es gar nicht ohne die Suche im Web. Wieso dann aber dieses Gefühl?

Heute fiel mir auf, dass das Googlen im Prinzip nichts anderes ist, als das Nachschlagen in einem Programmierbuch. Ich habe relativ wenig Bücher in nichtdigitaler Form, aber bei Informatikern älteren Jahrgangs sieht man oft in der Nähe der Schreibtische dicke Wälzer wie das *Das C++ Kompendium* oder *Java The Complete Reference* verstauben.     
Auch in dieser Hinsicht bin ich dem Internet sehr dankbar, denn so bequem und einfach konnte man in der Vergangenheit nicht programmieren. Nicht auszudenken, wie viel Zeit ich früher mit dem Wälzen von Bücher vergeudet hätte. Heutzutage finde ich schneller und meist bessere Lösungen, siehe StackOverflow. 

Und jetzt fühle ich mich nicht mehr ganz so sehr wie ein Schummler.