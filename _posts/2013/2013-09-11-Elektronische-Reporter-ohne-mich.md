---
layout: post
title: "Elektronische Reporter ohne mich"
date: 2013-09-11 10:00:00
category: medien
tags: [TV, Persönlich, Google]
published: true
comments: false
share: true
---

[Ende Juni wurde ich von einer Journalistin des Elektronischen Reporters zu dem Thema Datenbrillen interviewt.](http://ingorichter.org/blog/Mein-Interview-mit-dem-Elektronischen-Reporter.html)   
Wie ich nun erfahren musste, hat es mein Interview nicht in den 4:30 Minuten kurzen Beitrag geschafft. Schade.   

Es gab wohl Probleme beim O-Ton der vor mir interviewten Professorin und so mussten sie den Beitrag umkonzipieren und haben ein amerik. Entwicklerteam und einen anderen Informatiker befragt.   
Seinen Aussagen nach zu urteilen hat er die gleichen Fragen gestellt bekommen wie ich. Also wenn ihr einen Eindruck von meinen Antworten bekommen wollt, ich habe in etwas das gleiche gesagt.   

Aber Hand aufs Herz: Sein Hintergrund war einfach besser, schön vor der Volksbühne und mit Kunst im Bild. Das sieht natürlich schicker aus als der triste Fachschaftsrat-Raum der Beut Hochschule. Habe ich gleich geahnt.   
Da hätte man am/um/im HPI bessere Aussichten gehabt. Sehr schade.

[Hier gibt es den 77. Elektronischen Reporter (ohne mich!)](http://www.elektrischer-reporter.de/phase3/video/316/) 
