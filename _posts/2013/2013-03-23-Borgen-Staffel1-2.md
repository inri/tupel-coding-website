---
layout: post
title: "Borgen, Staffel 1 und 2"
description: ""
date: 2013-03-23 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-borgen-lg.jpg
  credit: DR
  creditlink: http://www.dr.dk/DR1/Borgen/
---

Schon seit einigen Monaten steht *Dänische Serie über Politik* auf meiner Wunderlist als *To-Watch* und endlich kam ich dazu die erste Staffel von Borgen anzuschauen. Und gleich danach auch noch die zweite. Meine Meinung mit einem Wort? **Grandios.** 


Borgen ist jetzt schon in meiner persönlichen Top 5 der besten Serien. Gleichzeitig macht mich die Serie wütend und zwar auf unsere Fernsehlandschaft.

## Die dänische Politik

Borgen blickt in zehn Episoden pro Staffel hinter die Kulissen der dänischen Politik im ersten Regierungsjahr der frisch gewählten Premierministerin Birgitte. Im Zentrum einer Episode steht meist ein Problem oder ein politisches Vorhaben, welches zu einem solchen wird. Gnadenlos und ungeschmückt werden die handelnden Personen, meist Politiker, skizziert und der Politikbetrieb zur Schau gestellt. Selbstverständlich nehmen auch die Medien dabei eine wichtige Rolle ein und bleiben von der Kritik nicht verschont.

Diese Serie ist mitreißend, interessant, intelligent, emotionale, kritisch - einfach nur gut. Neben *The Newsroom* ist sie eine der besten Serien der letzten 12 Monate für mich.

## Das deutsche Fernsehen

Jetzt folgen gleich drastische Worte. Das deutsche Fernsehen muss sich viel Kritik gefallen lassen. Oft wird der Vergleich mit amerikanischen Serien herangezogen, welche zwar sehr gut sind, aber andererseits, betonen die Kritiker der Kritiker oft, ein höheres Budget haben und oftmals auch nur rar gesät sind. Denn von vielen Serienpiloten gehen nur wenige in Produktion und von denen sind auch nur wenige herausragend und schwappen nach Deutschland.      
Doch wie erklären sich die Klugscheißer dann eine Serie wie Borgen? Noch einmal: Diese schier grandiose Serie kommt aus Dänemark. Dä-Ne-Mark! Ein kleines Land mit weniger als 6 Millionen Einwohnern!

Das ist keine Kritik an die kreativen Fernsehmacher, die haben bestimmt auch gute Idee. Doch diejenigen, die entscheiden, was produziert wird und was nicht, sollten sich jetzt (genau jetzt!) in eine Ecke verkriechen und schämen.      
Ja, vielleicht ist Borgen zu intelligent für ein großes Publikum, aber ihr habt doch euren Müll bereits im Programm. Ihr habt eure Casting-Shows, euer Reality-TV, euer Musikantenstadl, eure hunderte Talkshow-Formate und den ganzen anderen Müll. Ist es da zu viel verlangt, einige gute Serien zu produzieren?      
Damit spreche ich die öffentlichen-rechtlichen genauso an wie die privaten. Keiner von euch beiden kann sich aus der Verantwortung stehlen.

## Fazit

Besonders Serien sind zur Zeit in Deutschland einfach nicht gut und jeder der mit einem *aber!* antworten möchte, hat entweder Borgen nicht gesehen oder weiß nicht wovon er redet. Nein, über diese Feststellung diskutiere ich nicht! Unsere Serien sind Mist und wir sollten uns alle dafür schämen, dass ein kleines Land wie Dänemark wesentlich besseres zum Vorschein bringt.