---
layout: post
title: "Galaxy S4 Features im Check"
description: ""
date: 2013-03-14 
category: it
tags: [Android, Samsung]
published: true
comments: false
share: true
image: 
  feature: 2013-samsung-galaxy-S4-lg.jpg
  credit: Samsung
  creditlink: http://samsung.com

---

Und alle so: *Samsung hat so viel tolles an der Software gemacht!* und ich so: *Nö.*     
Das meiste von diesem Zeug ist genauso nützlich wie 2 GB RAM in einem Smartphone. Ein Tipp: Gar nicht, wenn das System gut ist. Es ist aber eine große Anzahl an Veränderungen und damit kann man sich schmücken. Klar.

#### Samsung OS

Stolz hält man das neue S4 in die Kamera und zeigt das angepasste UI, welches sich vom normalen Android abheben soll. Dafür haben die *Views* fancy Animationen und die Info-Bar ab oberen Rand ist nicht mehr schwarz. Wow. Natürlich kommen noch tonnenweise Software-Features.

#### Dual Kamera

Beispiel Familie: Die meisten Fotos werden von einem Elternteil gemacht, so dass entweder die Mutti oder der Vati zu 90% auf den Bildern nicht zu sehen ist. Damit ist jetzt Schluss! sagt Samsung und stellt ihre Lösung vor. In ein 13 MP Foto wird das Gesicht des Fotografen hinein kopiert und zwar über die 2 MP-Frontkamera. Was für eine innovative Idee. Im Ernst: Es sieht Scheiße aus und kein normaler Mensch wird das jemals nutzen. 

#### Sound &amp; Shot

In einem Foto wird ein kurzer Sound gespeichert und beim Anschauen abgespielt. Ich freue mich schon auf folgende Szene: Da schaut dich im Foto ein attraktiver Mensch an und plötzlich ertönt ein Furz-Geräusch. *Sound &amp; Fart*.      
Nein, die Frage ist: Von welchem Format reden wir hier? Können diese Fotos dann nur Besitzer von Samsung-Smartphones anschauen oder wie?

#### Drama Shot

Eine Art lange Belichtungsdauer-Effekt. Abgesehen davon, dass ich bisher nicht das Bedürfnis hatte, eine Bewegung auf diese Art abzubilden, frage ich mich wie die Software die Entscheidung trifft. Welche Bewegung wird wann dargestellt? Alle 20% der Aufnahme-Länge? Oder kann ich das aussuchen? Muss man sich in der Realität mal anschauen.

#### Eraser

Vorbeigehende Personen aus Bilder entfernen. Das gab es doch auch als App oder zumindest eine Beta? Wie hieß das noch...

#### Air View

Das wurde auf der Präsentation leider schnell abgewickelt. Der Finger berührt den Bildschirm nicht und trotzdem kann man irgendwie navigieren. Wie funktioniert das und funktioniert es gut? Braucht man das? Funktioniert es nur mit Samsung Apps oder mit allen? Viele Fragen, keine Antworten

#### S Translate

Szene: Er fragt auf Englisch und das S4 spricht es Chinesisch. Der Chinese antwortet auf Chinesisch, der Junge starrt in die Luft und versteht das plötzlich, oder wie? Nein. Vorher hatte er seine Frage auf Englisch bereits getippt (das sah man nicht so richtig) und die Antwort wurde textuell übersetzt. Warum nicht gleich eine Speech-2-Speech-Übersetzung? Das wäre cooler gewesen.      
Aber die Szene überhaupt, Backpacker in Shanghai, na klar. Das mache ich ständig. Ich übersetze nicht so oft Dinge in meinem wirklichen Leben und im Ausland mache ich mir viel mehr Gedanken über die Roaming-Gebühren. Und die werden mit diesem S Translate garantiert hoch sein. Und was ist verkehrt an Googles Übersetzungsdienst?

#### Story Album

Fotoalben zusammen klicken und mit einem Dienst ausdrucken können. Ja... ach. Vielleicht ganz nützlich, aber doch keine Sache, die jeder braucht. Vermutlich gibt es Drittanbieter-Apps die das besser können. 

#### Samsung Smart Switch

Super einfach und super schnell alle Daten vom alten aufs neue Gerät schieben! Wie einfach? Natürlich mit einer Drittsoftware für den Computer... wie toll! Tja Freunde der Sonne, das klappt mit den iGeräten dank iCloud besser und wirklich einfacher. Aber wie will man es auch sonst machen, wenn jeder Android-Hersteller sein eigenes Süppchen kocht.

#### S Voice Drive mit Kartendienst

Ersteres sicherlich sinnvoll, aber letzteres sehr seltsam wenn man bedenkt, dass Google seinen Maps Dienst bereitstellt. Ob das Samsung besser als Google oder Apple kann? Ansonsten klingt alles mit dem S vorne wie Siri. Sogar das Beispiel, dass man beim Fahren auf Nachrichten reagieren kann, kommt sehr bekannt vor. Aber natürlich waren die Menschen die größte Inspiration für Samsung, gleich nach Apple.

#### Samsung Knox

Eher ein Business Feature. Und ein Feature, welches auf der Präsentation in gefühlt unter einer Minute abgespeist wurde, weshalb wir auch nicht sehr viel drüber wissen, außer dass es in ähnlicher Form vom US Government benutzt wird und deshalb sicher sein soll.

#### Group Play

Bis zu 8 Galaxy Geräte können als ein großes Soundsystem verbunden werden. Ich bekomme ja nicht mal vier Leute in meinem Bekanntenkreis mit iPhones zusammen. Sehr optimistisches Features. Aber notwendig? Nein.

#### Dual Video Call

Auch so ein Ding, vielleicht brauchen das fünf Leute mal, aber das ist doch nichts, was man auf einer Keynote vorstellt. Was soll die Schwierigkeit gewesen sein? Man fügt zwei Videostream (von beiden Kameras) zusammen und schickt das weg. Keine Magie, genau wie bei Dual Camera.

#### Air Gesture

Das funktioniert mit der Frontkamera. Auch keine Magie, nur die Frage: nutzt man es so oft bzw. lackiere ich so oft meine Fingernägel und muss genau in diesem Moment mein Smartphone bedienen?

#### Smart Scroll / Pause

Revolutionär. Naja. Abwarten und Tee trinken, denn die Augenbewegung zu interpretieren ist auch kein Hexenwerk, aber ist es notwendig? 

#### S Health

Das S4 hat viele Sensoren. Nehmen wir mal an, dass sie gut funktionieren. Das wäre auf jeden Fall praktisch und würde viele coole Dinge möglich machen. Bei dem ganzen anderen Zeug, das noch an diesem S Health dran hängt, Kalorien zählen,... stellt sich mal wieder die Frage: Kann das Samsung wirklich gut? Oder gibt es nicht Drittanbieter-Dienste, die mehr können und das auch noch besser? Wie sehr bin ich auf Samsung angewiesen, wenn ich meine Daten exportieren will oder auf ein anderes Smartphone wechsle?

## Fazit

Die meisten Software-Features lassen mich kalt, denn sie sind eigentlich keine. Von den 15 hier aufgezählten, sind gerade mal 3 interessant und eventuell nützlich. Der Rest ist App-Schnickschnack und ich bin mir nicht sicher, ob das die Rolle von Samsung sein sollte, fancy Apps zu liefern.      
Mir kommt es eher vor, als möchte sich Samsung den Anstrich eines ach-so-innovativen Unternehmen mit vielen Ideen geben. Masse statt Klasse. Wer nur laut genug schreit, wird auch erhört.        
Das Gerät an sich ist genauso *nur* Modell-Pflege wie auch bei Apple jedes Jahr das iPhone. Das kann man nicht kritisieren. Von daher warte ich gespannt auf die Einschätzungen und Meinung der Presse. Gizmodo ließ das neue S4 auch kalt.

Wenn der Redner am Ende sagt *Remember, the Galaxy S4 is designed to understand the way we live now, the way we work, we socialise. The way we enjoy ourselves.* klingt das fast wie eine Drohung. Wenn ich der Meinung bin, dass die tausenden Features nicht unbedingt sinnvoll sind, lebe ich wohl nicht auf die richtige (Samsung-)Art und Weise. Oder wie?