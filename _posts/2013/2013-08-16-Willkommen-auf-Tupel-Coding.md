---
layout: post
title: "Willkommen auf TupelCoding"
date: 2013-08-16 00:00:00
category: blog
tags: [Tupel Coding]
published: true
comments: false
share: true
---

Hallo und schön dich hier zu sehen! Falls du es noch nicht weiß, ich bin Ingo und das ist meine Website namens *Tupel Coding*. Wer ich bin und was ich genau mache, kannst du auf der [About-Seite](/about/) nachlesen. Oder eventuell bist du über meinen alten Blog (*Studenten am HPI*) hier her gelangt und dann weißt du das alles ja schon.

##Eine eigene Website

Ich habe schon länger mit dem Gedanken gespielt eine eigene Website ins Netz zu stellen und hier ist sie nun. Wie kam es eigentlich dazu? Als ich vor gefühlt hundert Jahren als Austauschschüler in Norwegen war, habe ich einen kleinen Blog geschrieben, auf [blog.de](http://www.blog.de/). Nach dem Jahr im Ausland führte ich den Blog leider nicht weiter, sondern begann erst wieder vor ungefähr 4 Jahren mit dem Schreiben. Vor allem weil ein Studium am Hasso-Plattner-Institut vor mir lag und ich mir dachte *Das könnte vielleicht interessant sein*. 

Der Anfang war etwas holprig, aber seit gut zwei Jahren schreibe ich regelmäßig meine *selbstredend großartigen* Gedanken rund um mobile Technik und mein Informatikstudium auf. Bis vor einigen Tagen passierte das auf einem bei Blogger gehosteten Blog.   
Blogger ist als Einstieg in die Welt des Bloggens großartig und in vielen Aspekten besser als kleinere Portale wie beispielsweise blog.de. Aber es gibt eben auch Nachteile.

Diese Website hoste ich **nicht** auf meinen eigenen Server, was ich ursprünglich mal geplant hatte, sondern auf dem grandiosen [Uberspace](https://uberspace.de/). Weitere Informationen zur Seite werde ich in kommenden Artikeln verarbeiten. 

Im folgenden einige weitere Informationen zur Website:

### Abonnements: RSS ja, Email nein

Meinen alten Blog konnte man über RSS und Email abonnieren. Die Emails fallen leider weg, vorerst. RSS müsste eigentlich funktionieren, aber zumindest bei Feedly ist das aktuell noch etwas seltsam. Ich bleibe dran.

### Kommentare über Diskus

So fern ich die Kommentare freigeschaltet habe, und das habe ich bisher immer gemacht, könnt ihr unter jedem Artikel kommentieren [^1].       
Wer mir trotzdem lieber direkt schreiben möchte, bitte [über Kontakt](/contact/).

### Vorsicht Baustelle

Die wichtigsten Teile der Website laufen, der Blog und meine Fotos. Des weiteren arbeite ich an der *Coding*-Kategorie und natürlich weiteren kleinen Verbesserungen. Diese Website ist zwar irgendwie immer eine Baustelle, aber in den nächsten Wochen und Monaten ganz besonders.

Aktuell überführe ich Stück für Stück alle Artikel vom alten Blog. Zur Zeit bin ich bei 2011 und dieser Prozess wird sich noch eine Weile hinziehen, denn ich lese jeden Artikel und nehme kleine Änderungen vor. Einige davon sind dem Markdown geschuldet, andere eher stilistischer Natur. 

Wie auch immer, ich wünsche euch viel Spaß hier!

[^1]: Die erste Version von TupelCoding hatte noch keine Kommentiermöglichkeit.