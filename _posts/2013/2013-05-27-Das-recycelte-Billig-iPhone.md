---
layout: post
title: "Das recycelte Billig-iPhone"
description: ""
date: 2013-05-27 
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Seit Jahren wird über das Erscheinen eines billigen iPhones gemutmaßt. Grundsätzlich schließe ich mich der Meinung von John Gruber an: Apple bietet längst solche iPhones an und zwar sind das die alten Modelle, zur Zeit das iPhone 4 (399€) und iPhone 4S (580€). Zugegeben, über die Bezeichnung *billig* kann man streiten. 

Apple möchte gute Produkte anbieten und wenn für einen bestimmten Preis kein gutes Produkt herstellbar ist, gibt es das eben nicht. Für die letzten Jahre war das sicherlich zutreffend, aber meines Erachtens sind wir technisch an einem Punkt angekommen, an dem selbst mit billiger Hardware ein solides Smartphone machbar sein kann. 

*Doch Apple hat doch das iPhone 4 noch im Sortiment?! Was hat sich geändert?*

Einiges.        
Ich glaube dass ein iPhone mit Aluminium Hülle nochmals billiger sein kann als das verglaste 4 und 4S. Zudem passt es besser in das aktuelle Design der i-Geräte. Selbst wenn in einem billigen iPhone haargenau die gleiche Hardware wie beispielsweise im iPhone 4 drin ist, kann Apple dieses als *neu* und *innovativ* verkaufen und dadurch sicherlich höhere Absatzzahlen erzielen als mit dem stillen Verkauf der alten Modelle.        
Ein iPad ist für 330€ zu haben, wieso kann es dann kein iPhone für den gleichen Preis geben? Für viele ist der hohe Preis definitiv ein Grund sich nicht für ein iPhone zu entscheiden oder zumindest kein 2 Jahre altes iPhone zum Preis der aktuellen Konkurrenzmodelle. 

Ich kann mir daher sehr gut vorstellen, dass vielleicht schon im Sommer ein billiges iPhone im Design der aktuellen iPods (32 GB für 320€) vorgestellt wird. Genauer gesagt wird es eine 8 GB Version für 360€ und eine 16 GB für 460€ geben. Dadurch werden die alten Modelle obsolet und Apple kann die verglasten Geräte komplett aus dem Sortiment nehmen. 

Der Unterschied zum iPhone 5 wird trotzdem noch groß genug sein. Die *Liebhaber* werden bei dem neusten und tollsten Modell bleiben und andere probieren zum ersten Mal ein iPhone aus. Sicherlich geht es Apple nicht um eine möglichst weite Verbreitung der iPhones. Aber das klein iPad stellt auch einen neuen Kompromiss dar, wieso sollte ein iPhone nicht auch folgen? 

Außerdem bekommt vielleicht sogar das große iPad im Sommer eine Design-Auffrischung, zusammen mit einem neuen iOS. Dann hätte man eine einheitliche Produktlinie sowohl in der Hardware, als auch Software.