---
layout: post
title: "Die Semester-Lotterie"
description: ""
date: 2013-03-12 
category: blog
tags: [Studium]
published: true
comments: false
share: true
---

Als Student muss man für den Abschluss eine festgelegte Anzahl an Vorlesungen und Kursen besuchen, von denen nahezu alle bewertet und zur Berechnung der Endnote herangezogen werden. Ist bei Kernkursen relativ statisch festgelegt, wann man sie zu belegen hat, verwässert sich das bei den Vertiefungsgebieten etwas. Aber wenn man einen Kurs nicht besteht oder ihn aus freien Stücken wiederholen möchte, kann es vorkommen, dass zwischen der Erst- und Zweitbelegung einige Semester liegen. 

## Routine vs. Frische

Bei uns am HPI gibt es kaum traditionelle Kurse in dem Sinne, dass ein Dozent sie seit Jahrzehnten lehrt und das HPI ist ja erst 13 Jahre alt. Das hat den Vorteil, dass Vorlesungen frisch bleiben und von Dozent zu Dozent abwechslungsreicher sind. Natürlich kann man einen genialen Professor haben, der die Vorlesung seit 10 Jahren interessant gestaltet, aber ist dies nicht der Fall, wird sie immer langweiliger. Der Nachteil der Frische ist bei der Vergabe der Noten zu finden. 

Wäre unser System weniger leistungsgeil, könnte man einige Vorlesungen entspannter besuchen. Frisch aus der Schule mit den Abiturprüfungen im Rücken, geht es weiter ans Noten Zählen und das kann oftmals frustrierend sein. Hält ein Dozent einen Kurs zum ersten Mal, kann die Klausur sehr unterschiedlich ausfallen. Manchmal überraschend gut und manchmal eher das Gegenteil. Der Leistungserfassungsprozess (Unisprech) wird natürlich Semester für Semester optimiert, aber davon habe ich kaum was, wenn der Kurs hinter mir liegt.

Da wünscht man sich oftmals die Routine und eine erprobte Klausur. Das bieten traditionelle Kurse übrigens auch nicht immer, denn am Ende kommt es nicht auf den Dozenten und Professor an, sondern denjenigen, der die Klausur entwirft. Und diese Person kann auch wechseln.

## Typische Fälle

In meinen 7 Semestern am HPI sind mir schon einige kuriose Benotungen über den Weg gelaufen. Man erinnert sich meist an die für sich persönlich negativen Vergleiche. Wobei, wenn der eine schlechter abschneidet, profitiert der andere. Es ist vermutlich ausgeglichen.

**Theoretische Informatik:** Als ich TI im Jahre 2010/11 belegte, bekamen wir eine relativ kniffelige Klausur vorgesetzt. Ja, ich war nicht perfekt vorbereitet, aber schwierig war sie eben auch. Ich war nicht sicher, ob ich meinen zweiten Versuch während des ersten Bachelorprojektsemesters (2011/12) machen sollte. Meine Projektpartner taten es, ich nicht und ich bereute es. Denn die Klausur war sehr einfach. Es hagelte gute Noten und die meisten Teilnehmer bestanden, was für TI-Verhältnisse erstaunlich ist.       
Im Zuge meiner Klausurvorbereitung für den zweiten Versuch, bekam ich die Aufgaben der leichten Klausur in die Hand. Wenn ich mir bei den vielen guten Noten noch nicht sicher gewesen wäre, nachdem ich diese Aufgaben gesehen hatte, war ich es. Meine jetzige Klausur war übrigens okay. Sie war definitiv nicht so leicht und besonders zeitlich war es knapp, aber auch nicht so schwer wie meine erste. 

Eine kurze Anekdote zur Wahrnehmung: Ich sprach mit einem Wiederholer, der in der leichten Klausur eine 1,3 bekam und vorher durchgefallen war. Sein gutes Abschneiden führte er nicht auf die leichten Aufgaben zurück, sondern vermutete, dass bei der Korrektur der ersten Klausur eines seiner abgegebenen Blätter verloren gegangen war. Zur Klausureinsicht konnte er nicht gehen und somit auch den Verdacht nicht bestätigen. Vielleicht haben sie damals bei allen Studenten einige Blätter verloren?

**Mathematik 2:** Ein Beispiel für Kontinuität. Der Dozent hält die Vorlesung schon seit einigen Jahren und schreibt auch die Klausuren selbst. Mathe ist nicht einfach, aber die Klausuren bewegen sich immer auf einem ähnlichen Niveau. 

**Mathematik 1:** Der Dozent ist zwar seit Jahren der gleiche, aber die Klausuren werden von stets wechselnden Übungsleitern entworfen. In unserem Jahr (2009/10) sollen die Punkte angepasst worden sein, da sonst die Durchfallrate sehr hoch gewesen wäre. Hoch war sie trotzdem. Ich setzte den zweiten Versuch vier Semester später an und geriet ins Chaos von mitten im Semester wechselnden Übungsleitern. Die Zwischenklausur kam von dem einen, die Endklausur von dem anderen. Die Zwischenklausur fiel sehr gut aus und meiner Meinung nach, wurde die Endklausur deshalb anspruchsvoller gestaltet. Die Ergebnisse waren entsprechend und zogen den Gesamtschnitt wieder erheblich nach unten.

**BWL:** Wir haben zwei kleine Vorlesungen in BWL. In unserem Jahr gab es einen neuen Dozenten und der ging auch danach wieder. Die zwei Klausuren waren eigentlich einfach, denn die Fragen konnten wir vorher einreichen, er wählte eine Anzahl möglicher aus und in der Klausur gab es keine Überraschungen. Leider wurden die Lösungen vorher nicht festgelegt, die musste sich jeder selbst aus den Vorlesung und Folien herausarbeiten. In unserem Forum teilten wir unsere Ergebnisse und wähnten uns in Sicherheit, aber seine Korrektur war aberwitzig.         
Das Spektrum ging deshalb auch bis zur 3,x und ich glaube in der zweiten Klausur gab es nicht mal eine 1,0. Ein Jahr später sah das ganze anders aus, denn bei dem neuen Dozenten hatte kein Studenten eine schlechtere Note als 1,7 und der überwiegende Teil bekam 1,0en und 1,3en. Wahnsinn.
 
**Programmiertechnik 1:** Dieses Jahr neu dabei in der Reihe ist PT 1. Die letzten Jahre führte der Dozent am Ende von PT 1 und 2 mündliche Prüfungen durch. Seit dem diesjährigen Dozentenwechsel steht eine Klausur auf der Tagesordnung und die Ergebnisse fielen gut aus. So war die schlechteste Note eine 3,3, durchaus erstaunlich. Bei den mündlichen Prüfungen war der praktische Anteil sehr hoch und für einige fatal. In jedem Jahrgang gibt es eine gute Hand voll Studenten, die wenig bis gar keine Erfahrung in Programmieren haben und denen fällt diese Prüfung entsprechend schwer. Dieses Jahr haben sie Glück gehabt.

## Überall

Das Leben ist nicht fair, warum sollte es die Benotung auf der Uni sein? Korrekt. Und trotzdem ärgerlich. Die gleichen Geschichten höre ich auch von anderen Studenten. Bei meiner Freundin gab es sogar in der gleichen Vorlesung eines Semesters solche Schwankungen. Sie sollten einen längeren Aufsatz, eine Falldarstellung, schreiben und kontrolliert wurde das nicht von einem, sondern mehreren Dozenten. 
Also das erste Drittel übernahm A, ein Drittel übernahm B und das letzte Drittel übernahm C. Seltsamerweise waren die Noten, die B vergab um einige Punkte schlechter als von A und C. Wurden die Studenten zufälligerweise schon nach zu erwartender Leistung eingeteilt? Eher nicht, denn in der B-Gruppe waren *bekannte Streber* und die bekamen auch schlechte Noten und umgekehrt fanden sich in A und C auch weniger gute Studenten mit sehr guten Noten wieder. 

## Fazit

Natürlich wird die Abschlussnote nicht durch eine Lotterie bestimmt. Dafür sind wir Studenten noch immer selbst zuständig. Und neben Vorlesungen mit Klausuren, gibt es auch immer noch eine Menge an Seminaren, in denen die Leistung sehr individuell benotet wird.       
Trotzdem profitieren einige Studenten enorm durch den Zufall und andere sehen dadurch schlechter aus als sie sind. Kein 1er Kandidat wird am Ende mit einer 3 dastehen und umgekehrt kein 3er mit einer 1. Aber eine 2,7 kann durchaus eine 2,0 sein oder auch eine 3,3.  Ein gewisses Spektrum ist durchaus gegeben. 

Mich würde der statische Aspekt sehr interessieren. Vielleicht haben 95% aller Studenten mal Vor- und mal Nachteile und am Ende ist es ausgeglichen, nur 5% profitieren oder verlieren dabei. Vielleicht sind es statt 5% eher 25%? Wenn wir das weiterdenken kommen wir zur Frage: Wie kann man die Abschlüsse von unterschiedlichen Universitäten überhaupt vergleichen?       
Wenn nicht ständig die Leistung in Form von Zahlen auf irgendwelchen Zeugnisse in unserer Gesellschaft von enormer Wichtigkeit wäre, müsste man sich gar keine Gedanken darüber machen.