---
layout: post
title: "Orange is the New Black"
date: 2013-08-26
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-orange-is-the-new-black-lg.jpg
  credit: Netflix
  creditlink: https://www.netflix.com
---

Was passiert, wenn man in einer lesbischen Phase des Lebens aus Liebe Drogengeld schmuggelt? Erst einmal nichts. Und ein Jahrzehnt später wird man verpfiffen und muss für ein Jahr ins Gefängnis gehen. 

## Um was geht es?
   
*OitNB* erzählt auf charmant humorvoller Weise die Geschichte von Piper und ihren Mitinsassinnen. Der Gefängnisalltag hält viele Überraschungen für sie parat und nicht nur Piper muss damit lernen umzugehen, auch ihr Verlobter Larry muss sich an die neue Situation gewöhnen.

Nach und nach lernt man die anderen Frauen und die Gründe für ihre Inhaftierung kennen. Auch die Wärter spielen selbstverständlich eine wichtige Rolle in dieser kleinen Welt.

## Was gefällt?

Das Storytelling kann auf jeder Ebene überzeugen. Das Grundgerüst ist das Leben von Piper, aber die ganzen kleinen Geschichten der anderen sind ebenfalls hochinteressant.  

Ich kann *OitNB* nicht mit Bestimmtheit einem Genre zuordnen. Die Hintergrundgeschichten sind dramatisch und auch das Leben im Gefängnis für Piper, aber oftmals finden sich viele witzige Elemente wieder und lockern die Stimmung. Besonders im Finale der ersten Staffel überwiegt das Drama dann doch und lässt uns gespannt auf die nächste Staffel warten.

Gelobt müssen auch die Schauspielerinnen werden, denn sie machen einen großartigen Job. Das bekannteste Gesicht ist sicherlich Cpt. Janeway (Kate Mulgrew) aus Star Trek, die man in der Rolle einer russischen Chefköchin bestaunen kann. 

## Ein Wort zu Netflix

Die Serie wurde, wie beispielsweise auch *House of Cards*, von [Netflix](http://de.wikipedia.org/wiki/Netflix) produziert und das ist kein TV-Sender, sondern eine Streaming-Plattform. Das mag nicht weiter wichtig erscheinen, ist aber eine hochinteressante Entwicklung für solche Art von Produktionen. Anfangs war man skeptisch ob dieser Serviceanbieter eine gute Serien entwickeln kann. Aber die guten Kritiken und Emmy-Nominierung für *HoC* geben eine deutliche Antwort. 

## Fazit 

Mir hat die erste Staffel großen Spaß bereitet. Man sollte sich vom Gefängnis-Setting nicht täuschen lassen, denn die ist nur Fassade. Im Mittelpunkt stehen die tollen Geschichten der inhaftierten Frauen.
