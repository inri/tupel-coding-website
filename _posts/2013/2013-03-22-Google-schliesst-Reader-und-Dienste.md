---
layout: post
title: "Google schließt Reader und Dienste"
description: ""
date: 2013-03-22 
category: it
tags: [Google]
published: true
comments: false
share: true
---

Wie viele andere, war auch ich über die Entscheidung von Google, den Google Reader Dienst im Juli einzustellen, enttäuscht. Ein nicht unwesentlicher Teil meines Nachrichten- und Artikelkonsums organisiere ich mit den Google Reader. Nun muss man sich wohl langsam nach Alternativen umschauen und das wird den Feed-Reader Markt sicherlich beleben.

Doch man sollte sich auch einige kritische Gedanken zu Google machen. Ja, jetzt ist es nur Google Reader, der für die meisten Google-Dienste-Nutzer keine immense Bedeutung hat. Welcher Dienst ist als nächstes an der Reihe? Vielleicht Blogger? Ich werde mit dem Blog sowieso demnächst umziehen, aber wenn man schon etliche Jahre dabei ist und die Domain sich etabliert hat, durchaus ärgerlich. Oder wie wäre es überraschenderweise mit Google Drive? Plötzlich hat Google keine Lust mehr Microsoft Konkurrenz zu machen?

Zur Diskussion möchte ich zwei weitere Links bereitstellen. Zum einen einen [Mitarbeiter von Giga](http://www.giga.de/unternehmen/google/news/google-account-deaktiviert-backups-backups-backups/), dem sein Google Account ohne Vorwarnung deaktiviert wurde. Das beinhaltet alle (alle!) Dienste von Emails bis Google Reader. Bisher weiß er nicht was sein Fehler war und konnte noch niemanden bei Google erreichen. Zum anderen eine [Rechtsanwältin](http://www.kerstin-hoffmann.de/pr-doktor/2013/03/19/nochmal-dein-facebook-gehort-dir-nicht/), die uns erklärt dass wir den Bedingungen aller Dienste zugestimmt haben.

Im Prinzip läuft das auf die Frage hinaus: Wie lange dürfen diese *Internet*-Großkonzerne noch willkürlich handeln? Genauso wie Apple für seine mehr oder weniger transparenten App Store Restriktionen kritisiert wird, muss auch die Zensur der anderen Riesen hinterfragt werden. Einerseits sind wir alle nur Nutzer einen kostenlosen Dienstes, andererseits verdienen sie nur mit uns Geld.

Ein dritter, sehr empfehlenswerter Link, geht zu [Marco Arment](http://www.marco.org/2013/03/19/free-works). Er sinniert über die Natur von kostenlosen Diensten und dass sie gerade nicht besonders gut für die Nutzer sind und für unsere Computer-Industrie. Unbedingt lesen.