---
layout: post
title: "Dexter und die schwache finale 8. Season"
date: 2013-08-13 19:00:00
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-dexter-finale-lg.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/dexter/home
---

In Bezug auf Filme und Serien bin ich vorsichtig was zu schnelle negative Urteile angeht. Doch wenn ich etwas gut finde, weiß ich es meist sofort. Bei Filmen kann eventuell die Wirkung nachträglich &quot;reifen&quot;, aber ehrlicherweise muss ich zugeben, dass das nicht oft vorkommt. Bei Serien ist es anders. 


Da gibt es auch mal schwächere Episoden und ich vertrete die Auffassung, dass man tolerant sein und nicht gleich das Ende der Serie herbeireden sollte. 

Vor einigen Wochen begann die finale Season von Dexter und ich fand sie anfangs ganz gut. Leider nur anfangs, denn nach drei Episoden wurde es zwar nicht schlecht, aber auch nicht überragend. Nun bin zu neugierig um wegen einiger weniger guten Episoden gleich komplett das Handtuch zu werfen. Aber nachdem ich die Auftaktepisode zur finalen Season von Breaking Bad sah, konnte ich das offensichtliche nicht länger leugnen. Bis zur Mitte der 8 Season ist Dexter leider schlecht.

##Ein Dexter Spin-Off

Ich möchte näher auf die Gründe dafür eingehen, aber zuerst meine Vermutung bzgl. einer Spin-Off Serie von Dexter mitteilen, denn leider hängt beides zusammen. Es gibt die Gerüchte, dass eine Serie über Debra Morgan geplant sein könnte. Zuerst war ich skeptisch, denn die Serie Dexter drehte sich nur um Dexter. Die Haupthandlung stand immer im Vordergrund und mir fällt keine Nebenhandlung aus früheren Seasons ein, die nicht in irgendeiner Art und Weise mit Dexter zu tun hatte.   

In dieser Staffel gibt es davon aber viele, zu viele. Diese sind mir aufgefallen:

* Bisher völlig ohne Verbindung zu Dexter ist das Auftauchen von Masukas Tochter. Sie nähern sich an, er baut Mist, sie ist böse, sie verzeiht ihrem Vater und so weiter. Alles völlig ohne Belang. Ich sehe leider überhaupt nicht, welche Verbindung zu Dexter hergestellt werden kann. Wobei man fairerweise sagen muss, dass Masukas Rolle sowieso nur eine Art Lückenfüller ist.

* Dann ist da der neue ehrgeizige Quinn, der, angestichelt von Batista und seiner Freundin Jamie, versucht befördert zu werden. Es ist eine Art Liebesdreieck mit dem Bruder der Freundin als Chef des Freunds. Dexter hängt da nur mit drin, weil sie auf seinen Sohn aufpasst und Quinn mit ihm arbeitet. Sonst nichts.

* Debra hat ihren Polizeijob geschmissen und ist in einer privaten Ermittlungsfirma irgendwie das Mädchen für alles. Mal dem hinterherspionieren, mal das Kopfgeld abholen. Dass sie dort gelandet ist, passte gut in die Handlung von Dexter und man konnte gleichzeitig einen neuen Charakter (ihren Chef) einführen. Aber mittlerweile passiert in diesem Umfeld vieles, was nichts mit Dexter zu tun hat. Eigentlich ein perfekter zweiter Schauplatz, neben dem Polizeirevier.

* Als letztes habe ich auf meinem Notizzettel "Dexter als Vater" stehen. Ich glaube nicht dass Dexter das Finale überlebt bzw. so weitermachen kann. In einem Spin-Off kann ich ihn mir nur als Gastdarsteller vorstellen. Deshalb bezieht sich mein letzter Punkt weniger auf ihn, als viel mehr auf seinen Sohn, der nun etwas Persönlichkeit verpasst bekommt. Wird Debra seine Ersatzmutti?

Hand aufs Herz, bestimmt ein Drittel jeder Episode ist mit unwichtigen Nebenhandlungen gefüllt. Vielleicht gab es deshalb auch keinen Serienkiller in dieser Season? Denn mit einem solchen hätte man nicht so viel belangloses einstreuen können.

##Ungereimtheiten

Klammern wir die Nebenhandlungen mal aus. Ist der hauptsächliche Erzählstrang spannend und interessant? Ich weiß nicht mal genau worum es geht. Da ist die Psychiaterin Dr. Vogel, die anfangs durchaus ein interessanter Charakter war. Irgendwie denkt man immer noch, dass sie irgendwelche Pläne mit Dexter hat, aber dafür haben die Autoren sie in den letzten Episoden ziemlich selten auftreten lassen. Und konnte sie mit dem &quot;Brain Surgeon&quot; überhaupt unter einer Decke stecken? Auf mich wirkte er zumindest nicht so.    
Nun verteidigt sie den &quot;jungen Dexter&quot; Zach und möchte dass Dexter sein Ziehvater wird. Aber wieso? Was ist die Motivation von Dr. Vogel? Oder möchte man einen neuen Dexter für das Spin-Off erschaffen? Wenn ja, dann passierte das ziemlich unspektakulär.

Dann ist da noch die Geschwister-Beziehung. Was soll man davon halten? Ich kann es leider nicht näher beschreiben, aber irgendwie passt da etwas nicht zusammen.   
Dexter als Ziehvater für Zach kam mir als Handlung auch etwas überstürzt. Vielleicht ist die Idee gut, aber dass Dexter versagt bevor diese Beziehung überhaupt aufgebaut werden konnte, ist ein großes Versäumnis von Seiten der Autoren.

Ich hatte mich auf Hannah gefreut, aber ihr Auftauchen ist auch misslungen. Was steckt dahinter? Soll sie Dexter ablenken und ihn durcheinander bringen? Soll sie der Keil zwischen ihm und seiner Schwester sein? Wenn ja, wohin führt uns das? Debra hat schon einmal versucht ihn umzubringen und dass sie gegen Hannah bedenken hat ist nach dem Mordanschlag gegen sie auch verständlich. Läuft es auf Dexter vs. Debra hinaus? 

##Fazit

Nachdem ich Breaking Bad gesehen hatte, musste ich diese Zusammenfassung schreiben. Ich sehe bei Dexter einfach nicht das &quot;große Ganze&quot;. Der Zuschauer muss nicht genau wissen in welche Richtung die Handlung steuert und Überraschungen sind schließlich das beste an den Geschichten, aber Dexter ist für mich völlig undurchsichtig. Quasi genau im krassen Gegensatz zu Breaking Bad und das ist sehr schade für Dexter.

Auf welche Aussage läuft die Serie hinaus? Kann ein so spezieller Serienmörder wie Dexter keine Eingeweihten haben oder Menschen in seiner Nähe? Gibt es für ihn keine Absolution, weil er doch nur ein schlechter Mensch ist?

Die Chancen für ein Spin-Off scheinen mir sehr groß zu sein. Die Autoren wollen sich diese Möglichkeit offen halten und darunter leider die gesamte Serie. Das ist sehr schade.