---
layout: post
title: "Breaking Bad, Finale"
date: 2013-10-02 18:00:00
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-breaking-bad-finale-lg.jpg
  credit: AMC
  creditlink: http://www.amctv.com/shows/breaking-bad
---

Wenn uns jemand eine Geschichte erzählt, können wir nur lauschen, aber nicht den Verlauf der Geschichte bestimmen. Egal ob als Roman, Film oder Serie, diese Regel gilt für alle Arten von Geschichten. Am Montag durften wir Zeugen des Abschluss einer grandiosen Geschichte werden. 


Gemeint ist natürlich das Staffelfinale von Breaking Bad, die Geschichte um den Chemielehrer Walter White der ein Meth-Imperium aufbaut. Grandios ist diese Geschichte weil sie unglaublich gut erzählt ist. Dabei folgt das Drama eben nicht einfachen Erzählstrukturen, wie das beispielsweise bei Hollywood-Komödien üblich ist, sondern entwickelt eine spannende und mitreißende Dynamik.

Ich möchte einige erzählerische Entscheidungen des Finales diskutieren und natürlich wird es Spoiler geben.   
Nach der vorletzten Episode gab es alle möglichen Spekulationen über das Ende. Die zentrale Frage zu jeder Zeit war aber: Wie beendet man überhaupt die Geschichte um eine solch außergewöhnliche Person wie Walter White? Was wird er seiner Familie und der Welt hinterlassen? Welches Schicksal ereilt ihn letztendlich und welches Schicksal wäre vielleicht eher angemessen?

### Geld für die Familie

Immer wieder betonte Walter White, dass er alles nur für seine Familie getan hat. Er wollte vor allem finanzielle Sicherheit für sie, wenn der Krebs ihn eines Tages besiegt hat. Wir wissen es mittlerweile besser, denn sein persönlicher Ehrgeiz und gekränkter Stolz wegen der später erfolgreichen Firma, die er kurz nach der Gründung verlassen hat, waren der eigentliche Antrieb für ihn. Die Sorge um seine Familie war nur der Anstoß.

Walter packte in der vorletzten Episode der Ehrgeiz ein letztes Mal nachdem er seine ehemaligen Partner in einem TV-Interview sah und sie seine Rolle bei der Gründung der Firma herunterspielten (um sich von dem Drogenkönig zu distanzieren). Er verlässt sein Exil und kehrt zurück, aber wieso?    
Der erzählerische Kniff hierbei ist in der Tat grandios. Sein Sohn wollte das Geld nicht und auch nichts mehr mit ihm zu tun haben. Nach dem Telefonat war Walter am Ende, denn er konnte nichts mehr für seine Familie tun. Aber seine ehemaligen Partner können und sind vielleicht sogar moralisch verpflichtet, nachdem sie ihn öffentlich verunglimpft haben. Grandios. 

### Nebendarsteller angemessen verabschieden

Ein wenig Drohung ist doch notwendig. Walter holt sich für das &quot;Drohschauspiel&quot; die alten Kumpels von Jesse. Und genau **so** lässt man Nebencharaktere auf eine angemessene Art und Weise ein letztes mal auftauchen.    
Der Gegensatz dazu, wie man es nicht macht, war bei Dexter zu sehen, über dessen Finale ich nicht viele Worte verlieren werde. In der letzten Staffel bekam Dexters Laborkollege Masuka Besuch von seiner Samenspendertochter und wie sich leider herausstellte, war das ein völlig irrelevanter Erzählstrang. Pro Episode hatten die beiden ein bis zwei Minuten und man wunderte sich ständig, was die Bedeutung sein mag oder ob es überhaupt eine gibt.

### Das letzte Rizin

Vom Beginn der Season wussten wir ja schon, dass Walter das in seinem Haus versteckte Rizin abholen wird. Aber für wen war es bestimmt? Spekulationen schielten auf Walters ehemalige Partner, doch das hätte keinen Sinn ergeben. Wieso sollte Walter sie umbringen wollen? Sie haben zwar seinen Stolz verletzt, mehr aber auch nicht. Das würde nicht in die Heisenbergsche Moral passen.

Ob es Lydia verdient hat, kann man sicherlich diskutieren. Walter weiß, dass sie eine gierige Frau ist und ihr allein die Gewinne aus dem Meth-Business wichtig sind. Aus dieser Perspektive trifft es sicherlich nicht die Falsche.

### Vermächtnis hinterlassen

Wundervoll war die letzte Szene zwischen Skyler und Walter. Es gab darin einen Moment, in welchem ich für den Bruchteil einer Sekunde Enttäuschung spürte. Walt setzt an um sein ewiges Mantra (&quot;Alles nur für die Familie…&quot;) aufzusagen, Skyler unterbricht ihn (genauso genervt wie ich), doch er fährt mit der Offenbarung fort, dass er alles für sich getan hat und er es mochte weil er gut darin war. Wow!   

Das ändert zwar nichts daran, dass sein Sohn diese Wahrheit nie erfahren wird, aber der Zuschauer ist versöhnt. Walt bekommt einen angemessenen Abschied von seiner Familie und gibt die letzte Ruhestätte von Hank preis. Nicht ohne anzukündigen, dass seine Mörder einen hohen Preis zahlen werden. Hierbei wird schon deutlich, dass es Walt nicht mehr um das Geld geht, welches die Nazis ihm genommen haben. Es geht um seine Familie und wenn er bis jetzt auch viele Fehler gemacht hat, diese eine Sache beendet er richtig.

### Einer gegen Alle

Walt ist schwerkrank und in seiner jetzigen physischen Verfassung noch viel weniger ein ernstzunehmender Gegner als sonst. Was liegt also näher als die Nazibande mit einem typischen Walter-White-Trick den Garaus zu machen? Endlich schließt sich auch der Kreis zur ersten Episode der 5. Staffel vom letzten Jahr, als Walter mit Vollbart seinen 52. Geburtstag in einem Diner feiert und wir einen Blick in seinem Kofferraum voller Waffen werfen konnten. Der Showdown steht kurz bevor.

Noch ist unklar was Walter mit Jesse plant. Er weiß, dass Jesse nicht wie abgesprochen von der Nazibande umgebracht wurde und vermutlich denkt er, dass Jesse ihr Partner ist. Doch nachdem Walt seinen Wagen auf seine Weise vor dem Clubhaus geparkt hat, ist schnell klar ist dass die Bande nun ihn umbringen möchte, womit er gerechnet hat. Als er dann Jesse als gepeinigten Gefangenen erblickt, tut er eine letzte richtige Sache und reißt ihn zu Boden, so dass Jesse im anschließenden Kugelhagel nicht umkommt. 

Vom Plan bis zur Ausführung, selbst die spontane Entscheidung und Jesses Rache an Todd - einfach nur gutes Storytelling. Es ist nachvollziehbar und fühlt sich richtig an, etwas das ich bei Dexter am Ende vermisst habe. Auch dass Jesse Walt nicht erschießt ist logisch, das hätte er auch ohne Walts Wunde zu sehen nicht getan.    

### Verdientes Ende

So bleibt als einzig fahler Nachgeschmack dieser letzten Episode, dass es eben die letzte ist und sich einige Enden vielleicht etwas zu rund und abgeschlossen anfühlen. Weitere Cliffhanger oder all zu viele offene Fragen hätten das Ende jedoch vermiest, z.b. wie bei Lost. Die Geschichte ist erzählt.

Dass Walt seine letzte Ruhe zwischen all den Gerätschaften findet bei denen er sich wohl gefühlt hat, ist ein wunderschöner versöhnlicher Abschluss. Auch ein gnädiger sicherlich, aber ein verdienter.