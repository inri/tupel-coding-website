---
layout: post
title: "Ein Recht auf Internet"
description: "Ich fordere ein Recht auf Internet!"
date: 2013-01-13 
category: blog
tags: [Technologie]
published: true
comments: false
share: true
---

Pünktlich zum 1. Januar wurde mein Internet von Alice abgedreht, wie gewünscht, denn ich hatte meinen Vertrag gekündigt. Leider war das Wohnheim Internet noch nicht aktiviert, so dass ich zuerst über mein iPhone ins Netz ging und dann das Wohnheim Internet meiner Freundin via Kabel nutzte. 

So richtig praktisch war das aber nicht, denn wir hatten für sie die Mac-Adresse eines Ethernet-Adapters von Apple angegeben, so dass ich ihn nur für mein MacBook verwenden konnte. PC, iPhone und iPad blieben außen vor, wobei ich über Bluetooth mein MacBook-Internet für die mobilen Geräte teilen konnte.     
Es war einfach unpraktisch und mit etwas Pech hätte ich auch gar kein Internet in der Nähe haben können. Da wurde mir bewusst, dass es **ein Recht auf Internet** geben müsste. 

Egal in welcher Wohnung oder welches Haus ich ziehe, ob mit oder ohne Provider-Vertrag, es müsste eine Dose geben, aus welcher ein dünnes Rinnsal Internet tropft. Das muss nicht viel sein, einfach nur genug um die wichtigsten Dinge zu erledigen. Wegen mir auch mit monatlicher Traffic-Beschränkung von z.b. 1 GB. Und auf den Mietpreis kann ja auch eine Gebühr dafür draufgeschlagen werden, ein symbolischer Euro.

Das wäre geradezu fortschrittlich und vorbildlich für ein modernes Land. Aber was rede ich...