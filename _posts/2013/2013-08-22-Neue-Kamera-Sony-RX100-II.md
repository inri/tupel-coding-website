---
layout: post
title: "Neue Kamera: Sony RX100 II"
date: 2013-08-23 15:00:00
category: blog
tags: [Fotografie, Sony RX100 II]
published: true
comments: false
share: true
image: 
  feature: 2013-sony-rx100m2-lg.jpg
  credit: Sony
  creditlink: http://sony.com
---

Seit gut zwei Wochen fotografiere ich mit meiner neuen Kamera, einer Sony RX100 II. Ich diesem Artikel möchte ich erzählen wie ich zu dieser Kamera gekommen bin.

##Die alte Kamera

Meine erste richtige, gute Digitalkamera habe ich mir vor einigen Jahren gekauft. Es war eine sogenannte Bridge-Kamera, die Panasonic FZ28. Dieser Kameratyp sieht einer Spiegelreflexkamera ähnlich, ist aber meist etwas kleiner und leichter, und hat statt eines Wechselobjektivs ein fest verbautes Zoomobjektiv. Die FZ28 hatte beispielsweise 18x Zoom, was schon ganz schön beachtlich war. 

Damals hatte man drei Wahlmöglichkeiten: Entweder eine Kompaktkamera, eine Spiegelreflex oder eben eine Bridge-Kamera. Eine Spiegelreflex war noch relativ teuer und da mein Papa eine Bridge hatte (den Vorgänger der FZ28) entschied ich mich für diese. Sie war toll und ich fotografierte gerne mit ihr. In den letzten beiden Jahren nahm ich sie aber immer seltener mit, denn ich hatte ein iPhone. 

Nein, das iPhone schoss keine besseren Fotos als die FZ28, aber es war sowieso immer bei mir und die Fotos waren okay. Ich nutzte die große Kamera nur noch bei besondere Anlässen (Urlaub etc.). Dass das sehr schade war, wurde mir vor einigen Wochen bewusst als ich begann meine Fotosammlung zu sichten und zu sortieren. Die iPhone Fotos waren zwar okay, aber insgesamt merkte man den Qualitätsunterschied deutlich. 

##Neue Anforderungen

Die FZ28 schießt gute Fotos, aber sie bringt auch einige Nachteile mit sich. Sie ist groß und schwer und man kann sie nur in einer zusätzlichen Kameratasche transportieren (Hosentasche ist eher ungünstig). Ich wollte also auf jeden Fall eine **kleinere und leichtere** Kamera.

Die FZ28 hat noch einen digitalen Sucher, aber ich nutzte diesen nie, denn das Bild im Sucher war viel zu klein. Die großen LCD-Screens sind schon ziemlich genial und weil ich gerne mal interessante Winkel zum Fotografieren ausprobiere, wollte ich eines von diesen **schwenk- und kippbaren** Displays. 

Als letzten und wichtigsten Punkt ist natürlich die **Bildqualität** zu nennen. Der entscheidende Faktor befindet sich immer hinter der Kamera und ist natürlich der Fotograf. Ein guter Fotograf kann auch mit schlechter Ausrüstung gute Fotos machen. Da ich aber kein Fotograf bin, sollte meine Kamera von alleine möglichst gute Fotos zaubern. 

Ein Zoomobjektiv war mir nicht mehr so wichtig. Ich mochte es an meiner FZ28 gerne, doch je mehr man zoomt, desto wackeliger wird das Bild und es dauert natürlich auch immer seine Zeit das Objektiv auszufahren. Außerdem wird es niemals eine leichte, kompakte Kamera mit Zoomobjektiv geben.

Der Preis sollte auch im mittleren 3-stelligen Bereich liegen.

##Micro Four Thirds

Ich schaute mich in zwei Kategorien um, wobei eine neue hinzugekommen war: Micro Four Thirds (MFT). MFT Kameras sind quasi die kleinen Geschwister von Spiegelreflexkameras. Sie sind kleiner und kompakter als diese, haben aber auch kleinere Sensoren, welche allerdings wesentlich größer als Sensoren herkömmlicher Kompaktkameras  sind (die meisten Sensoren haben eine Diagonale von 1/2.3&quot; und 1/1.7&quot;, MFTs liegen bei 4/3&quot;! siehe [hier](http://en.wikipedia.org/wiki/File:Sensor_sizes_overlaid_inside_-_updated.svg) und [hier](http://www.gizmag.com/camera-sensor-size-guide/26684/pictures#1)). Und MFTs haben Wechselobjekte. 

Erfüllten MFTs also meine Anforderungen? Leider nicht so richtig. Sie sind kompakt und man kann tolle Fotos mit ihnen schießen (dank des großen Sensors), aber trotzdem hat man Wechselobjektive und benötigt entsprechend Platz dafür. Und Kleingeld, denn oftmals sind die mitgelieferten Standard-Objektive murks und um das Potential der Kamera auszuschöpfen, benötigt man auch ein gutes Objektiv, so dass man wieder im höherpreisigen Segment landet. 

MFT Kameras sind optimal, wenn man maximal 500g an Gerätschaft transportieren, sehr gute Fotos machen und Objektive wechseln möchte. 

##Eine gute Kompaktkamera

Ich suchte also weiter und wurde bei der RX100 fündig (Vorgänger der RX100 II). Sie erschien vor einem Jahr und deklassierte damals alle Kompaktkameras in ihrer Kategorie. Der Grund dafür war ihr relativ großer 1&quot; Sensor und die fantastischen Bilder, die man damit machen konnte. An einigen Stellen merkte man aber, dass es die erste Kamera ihrer Reihe war. 

Vor einigen Wochen stelle Sony ihre Nachfolgerin, die RX100 II, vor und die hat es in sich. Unter anderen wurde der bisherige Exmor Sensor überarbeitet (nennt sich nun [Exmor R](http://www.sony.net/Products/di/de/products/g681/index.html)) und ist jetzt noch lichtempfindlicher, weil der Sensor rückseitig belichtet wird.   
Daneben wurde das Display klappbar, die Kamera bekam einen Zubehörschuh und WLAN/NFC Anbindungsmöglichkeiten. All die anderen gute Eigenschaften ihrer Vorgängerin, wie das Carl Zeiss Objektiv mit einer Blende von F1,8 oder der Objektivring, wurden beibehalten. 

Insgesamt eine sehr gute Weiterentwicklung und genau das was ich gesucht hatte. Diverse Reviews in Textform ([Ralfs Fotobude](http://www.ralfs-foto-bude.de/kameratest/kamerahersteller/sony/sony-cybershot-dsc-rx100ii?showall=&start=1)) oder als Video bei Youtube bekräftigten meine Entscheidung.

Im nächsten Artikel über die RX100 steht die Kamera im Vordergrund und welche Erfahrungen ich bisher mit ihr gemacht habe. Außerdem erscheint noch ein Artikel über das tolle Zubehör für die RX100.