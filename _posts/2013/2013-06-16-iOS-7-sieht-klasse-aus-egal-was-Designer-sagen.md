---
layout: post
title: "iOS 7 sieht klasse aus, egal was Designer sagen"
description: ""
date: 2013-06-16 
tags: [iOS, Design] 
category: it
published: true
comments: false
share: true
---

Das Geschrei der Design-Community war kurz nach der WWDC-Keynote groß. Ich fasse es mal zusammen: *Wieso hat Apple ein solches iOS gemacht? Wie sieht das denn aus! Diese Icons! Diese Farben! Diese dünne Schrift! Das ist doch alles von Android geklaut! Das ist doch alles von Windows Phone geklaut!*

## Hohe Erwartungen

Seit bekannt wurde, dass in Zukunft John Ive bei Apple die Hoheit auch über das Softwaredesign haben wird, hörte ich viel Lob, Freude und erste Erwartungen. Das war vor einem halben Jahr. Es war nicht ganz klar, ob es zur WWDC schon Änderungen geben würde, aber in den letzten Wochen tauchten erste Gerüchte auf und Designer begannen nicht nur über ihre Erwartungen zu reden, sondern sie auch zu entwerfen. Nach dem zehnten Entwurf war mir klar, dass Apple auf irgendeine Art und Weise enttäuschen wird.

Die ersten ziemlichen harten Kommentare haben mich erstaunt. Zuerst war ich mir nicht sicher, ob sie tatsächlich das sind, was ich ahnte. Doch man kann sie in die gleiche Kategorie von nerdiger Kritik stecken, der man als Informatik stets begegnet. Oder anders gesagt: Wenn eine hochprofessionelle Firma etwas vorstellt, auf dessen Gebiet sie zweifelsohne gut ist, dann ist es mit hoher Wahrscheinlichkeit in der Tat richtig gut.

Bei dieser Aussage muss man vorsichtig ein, denn natürlich bauen auch Profis Mist. Aber es kommt nicht so unglaublich oft vor. Was die Designfrage bei Apple angeht, kommen zwei Faktoren hinzu, die man für eine Bewertung ebenfalls im Kopf haben muss: **a)** iOS 7 ist eine Beta-Version und **b)** das Design wurde in knapp 8 Monaten entwickelt.     
Die ersten Kommentare waren argumentativ nicht unterlegt, da reden wir nicht drüber. Nach und nach gab es längere Beiträge und Gedanken, aber auch davon hat mich noch keiner überzeugt. Meistens werden kleine Beta-typische Fehler bemängelt. Geschenkt. Oft lese ich auch davon, dass irgendwelche Regeln von Apple missachtet wurden. Diesen Farbverlauf könne man gar nicht benutzen und jenes Symbol passt nicht zu den anderen.

Für mich klingen diese Kritikpunkte wie das Jammern eines Musterschülers, der zwar brav alles so gemacht hat wie ihm beigebracht wurde, am Ende aber doch eine schlechtere Note bekam als der Punk, der sich nicht an jede Regeln gehalten hat.

## Ich mag das neue iOS 7

Mir fiel genau eine negative Sache beim neuen Homescreen auf und das waren die hellgrünen Buttons. Auf meinem Desktop-Monitor wirkten sie fast neon, aber als ich ein Vollbildscreenshot auf dem iPhone sah, war es okay.      
Mir gefällt die neue Leichtigkeit sehr. Und zu flach und von Windows Phone kopiert empfinde ich es auch nicht. Da schwirrte so ein Youtube-Vergleichsvideo rum, was im Prinzip nur zeigte, dass beide UIs noch genug unterschiedlich sind. Mal sehen was der Herbst bringt.

Ich denke einige Ecken und Kanten werden noch ausgebessert. Die Überarbeitung ist gelungen und ich bin gespannt wie schnell andere Apps die Designsprache übernehmen.     
Wer der Meinung ist, er könne dann ja gleich ein Android oder Windows Phone benutzen, solle es bitte tun. Niemand hält euch auf.