---
layout: post
title: "The Walking Dead, Ausblick 3.12"
description: ""
date: 2013-02-27 
category: medien
tags: [The Walking Dead]
published: true
comments: false
share: true
---

Der Fokus lag in erster Linie auf den Besuch von Andrea im Gefängnis. Ihre Motivation ist das Zusammenbringen oder zumindest Frieden schaffen zwischen Woodbury und der Gruppe im Gefängnis.


## Rückschau 3.11

Natürlich weiß sie nicht, was tatsächlich passiert ist und entweder will es ihr niemand ausführlich erklären oder sie will es nicht verstehen. Wir sehen die Geschehnisse aus der Vogelperspektive und wissen, dass mit dem Governor ein solcher Frieden nicht zu erreichen ist. Aber Andrea sieht ihn in einem anderen Licht und kann sich selbstverständlich nicht dazu durchringen seinem Treiben ein Ende zu bereiten.

Im Gefängnis herrscht immer noch die Rickdatur, wobei man ihm aber auch deutlich zu verstehen gibt, dass er sich zusammenreißen und der Führer der Gruppe sein soll. Sein eigener Sohn ist davon aber nicht mehr überzeugt und möchte lieber, dass sein Vater sich ausruht. Glenn hält sich einigermaßen zurück, obwohl er am liebsten einen Gegenangriff fahren würde. Merle entschuldigt sich halbherzig bei Michonne, aber kann man ihm trauen?

## Woodbury

Dass Andrea die Neuigkeiten vom Gefängnis erst verdauen muss, kann ich akzeptieren. Doch sie sollte mal über ihre persönlichen Einschätzungen nachdenken. Sie irrte sich in Shanes Verhalten und dass der Governor seltsam ist, weiß sie auch. Sie kennt die Show-Kämpfe und sie war beim Daryl-vs.-Merle Todeskampf dabei. 

Sie hat die Köpfe-Sammlung und die Tochter des Governor gesehen, und weiß nun auch dass er Maggie und Glenn folterte. 
Auch dass der Angriff auf das Gefängnis von ihm kam, kann sie zumindest vermuten. Wozu lässt er sogar die Kinder zu Soldaten ausbilden? Nur Verteidigung? Und sie bekam eine deftige Zurechtweisung von Michonne, in welcher sie ihr sagte, dass er Merle nach ihr schickte und letztlich warnte sie Andrea schon sehr zeitig vor dem Governor.

Was ich sagen will: Sehr bald sollte es bei Andrea klick machen und entsprechende Taten folgen. Aber ich vermute stark, dass der Governor schon mit ihr abgeschlossen hat und sie bald selbst ins Kreuzfeuer gerät. In der nächsten Episode wird noch kein Krieg ausbrechen.

Ich habe mich gefreut, dass Tyreese und seine Gruppe wieder aufgetaucht sind. Dummerweise sind sie nach Woodbury gekommen und dort sind sie natürlich willkommen. Die *weiße* Hälfte der Gruppe würde sofort gegen Rick ziehen. Ich denke Tyreese wird sich dem Anschließen, aber im richtigen Moment die richtige Entscheidung treffen. 

## Gefängnis

Wir wissen bereits, dass Rick mit Carl und Michonne einen Ausflug machen wird. Vermutlich will er zum einen ein Auge auf Michonne werfen (Kann man ihr vertrauen?) und zum anderen Carl trainieren oder ihm beweisen, dass es ihm gut geht und er keine Pause als Anführer braucht. Wird dies gelingen? Ich denke ja. Michonne ist wieder bei Gesundheit und wird Rick irgendwie aus der Patsche helfen können, wenn er mal wieder Lori sieht. 

Wird es im Gefängnis zu Reibereien kommen? Ich denke ja, aber noch nicht in der nächsten Episode. 