---
layout: post
title: "SEO Seminar, Auswertung"
description: ""
date: 2013-02-22 
category: it
tags: [HPI, Studium, SEO]
published: true
comments: false
share: true
---

Das Semester bzw. die Vorlesungszeit ist vorbei und demnach auch die Seminare. Ich möchte gerne einige abschließende Worte zum Suchmaschinenoptimierung-Seminar schreiben. 

## Was haben wir gemacht?

Zwei Dinge. Jeder Teilnehmer hat eine Website erstellt und sollte diese für drei bestimmte Keywords optimieren (*hochsommerliche paranormale Insuffizienz*). Wir entschieden uns gegen die *Welche-Methode-ist-die-beste?*-Variante und wählten den Wettbewerb. Also so gut wie möglich mit allen Mitteln optimieren und am Ende gibt es einen Gewinner und das war übrigens ich). Daneben bekam jeder ein SEO-Thema für einen Vortrag.      
Insgesamt mussten wir drei Protokolle anfertigen und abgeben. Welche Optimierung haben wir wann gemacht und was haben wir uns davon versprochen oder welche Erkenntnisse haben wir gewonnen. 

## Was sind die Ergebnisse?

Am Ende wurden jeweils die Top 3 Plätze bei Google und Bing betrachtet und das waren interessanterweise fünf verschiedene Personen. Ich war der einzige, der bei beiden Suchmaschinen in den Top 3 war, genauer gesagt je auf Platz 2. Das brachte mir den Gesamtsieg und ein coolen USB-Kugelschreiber als Siegergeschenk.

Die Protokolle wurden auch ausgewertet und eine Liste erstellt, wann wer welche Optimierung vorgenommen hat. Zusätzlich haben alle Seminarteilnehmer über die ihrer Meinung nach besten Optimierungen je Suchmaschine abgestimmt. Vorneweg, jeder der Top 3 hat alle guten Optimierungen irgendwann durchgeführt.

Insgesamt ist eine Top-Level-Domain, Meta-Tags, Social Networking, Backlinks und unique Content ganz nützlich um ein gutes Ranking zu erreichen.

## Was sind die wirklichen Ergebnisse?

Dieses ganze SEO ist nicht gerade einfach. Oder gerade doch. Ich bin der Meinung dass Links sehr wichtig sind. Unser Seminar war zwar einerseits hochinteressant, aber andererseits auch etwas chaotisch gewesen. Jeder hat zu verschiedenen Zeiten Optimierungen durchgeführt und teilweise auch bis kurz vor dem Ende mit einigen Dingen gewartet. Es war kaum möglich zu wissen, ob meine Optimierung für meine Rangänderung verantwortlich war oder die eines anderen.

1.	Eine Domain mit Keywords ein guter Anfang       
2.	Meta-Tags setzen, eine XML-Sitemap und sinnvoller Content      
3.	Seite bei den Suchmaschinen anmelden > Webmaster Tools      
4.	Links, Links und nochmals Links!  

Daneben gibt es viele weitere kleine Dinge, die man beachten sollte. Am wichtigsten sind aber Links und diese erhält man am besten mit guten Content, weil andere auf deine Seite verlinken.