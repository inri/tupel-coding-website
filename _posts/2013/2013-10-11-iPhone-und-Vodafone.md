---
layout: post
title: "iPhone und Vodafone, Teil 1"
date: 2013-10-11 16:00:00
category: blog
tags: [iPhone, Persönlich]
published: true
comments: false
share: true
---

#### oder: Schlechter Service auch im Jahr 2013

Die Vorgeschichte ist kurz. Nach zwei Jahren möchte ich mein iPhone 4S an die Freundin weiterreichen und mir selbst das neue 5s gönnen. Erste Frage: Kauf oder Vertrag? Gegen Vertrag sprechen höhere Kosten insgesamt, gegen Sofortkauf sprechen höhere Kosten einmalig und die Tarifbegrenzung durch die Prepaid-Karte [^1]. Für den Vertrag sprechen neben den Flats auch LTE. Ausschlaggebend war für mich aber die monatliche Finanzierung, denn besonders nach den ganzen Umzugskosten und Möbeleinkauf muss man nicht unbedingt eine weitere große Summe ausgeben.   
Zweite Frage: T-Mobile, Vodafone oder o2? Wie der Titel es schon verrät, ist es Vodafone geworden. Die hatten das beste Preis-Leistungs-Verhältnis, gefolgt von T-Mobile. o2 hat sich schon allein durch die grottenschlechte mobile Website disqualifiziert. Interessanterweise spielte die Verfügbarkeit des iPhone keine Rolle, denn es **war** bei beiden sofort lieferbar. Ich bestellte erst eine Woche nach offiziellen Verkaufsstart des iPhones, also am 27.09., vorher war einfach keine Zeit. 

### Es kommt bestimmt morgen...

Sofort lieferbar am Freitag? Dann wird es bestimmt am Montag bis Mittwoch kommen. Nö. Vier Tage später (Montag) erhöhte sich die Lieferzeit auf die nächste Woche, also sollte es nun zwischen dem 7. und 12.10. kommen. Okay, kein Problem, ich bin ja geduldig.   
Wiederum eine Woche später am Montag den 07.10. änderte sich die Lieferzeit dann auf den 04. bis 09.10. Huch? Ein seltsamer Sprung, aber okay, dann kommt es ja bald! Gleichzeitig fand ich ein Forumsbeitrag von Vodafone mit ständig aktualisierten Lieferzeiten und dort stand tatsächlich für das iPhone 5s in spacegrau mit 32 GB *im Versand*. Toll! Dann kann es ja nur noch einige Tage dauern. Nö.

Der 09. verstrich und ich wurde stutzig. Wie kann das sein? Gilt für meine Bestellung vielleicht nicht der 27.09. als Datum sondern erst der 02.10. mit der Herzlich-Willkommen-bei-Vodafone Email? Anrufen und fragen konnte ich nicht, denn die tolle Hotline verlangt stets eine Vodafone-Nummer und die hatte ich noch nicht. 

Ich schaute mich auf der Vodafone-Seite um und fand ein Forum mit anderen Wartenden. Dann schaute ich mir meinen Bestellstatus an, sehr gründlich. Vielleicht 5 Minuten lang ohne zu Blinzeln. Da sah ich plötzlich meinen Fehler! Das aktualisierte Datum war der 04.**11.** bis zum 09.**11.** - och nö! Zwei Wochen waren um und nun standen mir noch vier weitere bevor?! 

### Apple verknappt den Bestand

Natürlich kann Vodafone selbst nichts für die Verzögerung. Sie haben x iPhones bestellt, aber weniger wurden geliefert. So ist das nun einmal. Wieso die Deppen aber *Sofort lieferbar* auf ihrer Website deklarieren, ist unverständlich. Ich kann doch etwas nur sofort liefern, wenn ich es in meinem Lager habe. Vodafone hat scheinbar einfach nur gehofft, dass sie ihre Bestellung bekommen und damit wären die iPhone ja quasi sofort lieferbar. Mittelgroße Kundenverarsche.

Mein Lieblingskommentar von einem Vodafone-Kunden war die Mutmaßung, dass Apple den iPhone Bestand mit Absicht gering hält, damit es etwas exklusives bleibt. Ja genau! Die Kunden 6 Wochen warten zu lassen ist fantastisches Marketing. 

### Kein iPhone, aber eine Rechnung

Ich war geduldig, bis heute früh. Da erhielt ich die erste Rechnung von Vodafone und natürlich haben sie schon einige Tage abgerechnet und eine Vorauszahlung für den kommenden Monat veranschlagt. Klar, die Bestellung ist 14 Tage her, Zeit für die erste Abbuchung. Es ist ja auch egal, ob der Kunde den Vertrag überhaupt nutzen kann. Ich bekam bisher weder iPhone noch sonst irgendwelche Unterlagen (SIM o.ä.). 

Das einzig gute an der Rechnung war die Tatsache, dass meine zukünftige Mobilfunknummer drauf stand. Nun konnte ich also endlich die Hotline anrufen und schilderte mein Unbehagen. Der Mitarbeiter fragte tausend Sachen und es dauerte eine kleine Ewigkeit, doch am Ende hatte ich ein Gutschrift von 50€. Etwas zufriedener legte ich auf und dachte nach.

### Upgrade oder Gutschrift

Im Prinzip ist mit der Gutschrift der laufende Monat bezahlt. Doch so oder so muss ich noch 4 Wochen auf mein iPhone warten und evtl. verstreichen am Ende 1,5 Monate bis ich den Vertrag überhaupt nutzen kann (und die Gutschrift verrechnet ja nur einen Monat). Dann müsste ich nochmals anrufen und mich mit denen rumstreiten...      
Bessere Idee: Noch einmal anrufen und nach einem Upgrade fragen. Ich wollte zwar keine 64GB im iPhone, weil ich nicht so viel nutze, aber wenn die mir ein Upgrade geben, kann ich die Gutschrift für mich persönlich als verringerte Upgrade-Gebühr verrechnen. 

Gedacht, getan und nun kommt mein iPhone wohl nächste Woche. Wenn es das doch nicht tun sollte, werde ich es doch wieder auf die 32GB downgraden und mich mit denen im November rumstreiten. 

### Fazit

Puh… was will man sagen? Laut Telekom-Seite würde ich mein iPhone in dieser Woche bekommen, wenn ich es vor zwei Wochen dort bestellt hätte. Andererseits vertraut man doch darauf, dass die Anbieter mittlerweile wissen, wie der Hase bei neuen iPhones läuft. Letztes und vorletztes Jahr gab es doch auch Wartezeiten und Lieferverzögerungen. Trotzdem bläst man mutig ins Werbehorn und verspricht den Kunden unmögliches.    

Es ist traurig, aber der Service ist immer noch schlecht. Ob dass die Kundenverarsche mit den Lieferterminen ist, das Verschicken der Rechnung obwohl kein Dienst in Anspruch genommen wurde oder die tausenden Nachfragen in der Hotline. Diese Zentralen müssen Irrenhäuser sein.

[^1]: Billig-Anbieter wie blau.de sind super, wenn man nur eine Option möchte wie beispielsweise Mobiles Internet bei mir. Soll eine Telefonflat oder SMS-Flat dazu wird es schnell teurer und dann lohnt sich das nicht mehr.