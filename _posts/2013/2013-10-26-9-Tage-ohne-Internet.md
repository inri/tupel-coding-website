---
layout: post
title: "9 Tage ohne Internet"
date: 2013-10-26 10:00:00
category: blog
tags: [Persönlich, Web]
published: true
comments: false
share: true
---

In den letzten Monaten war ich vermutlich nie mehr als 12 Stunden am Stück komplett ohne Internet. Dabei konsumiere ich nicht in jeder einzelnen Minuten Nachrichten und Informationen. Es sind wohl eher alle 10 Minuten. Spaß beiseite. Twitter, RSS / Feedly, App.net, Facebook, Instagram und Emails rufe ich quasi jederzeit ab.

Ende August war ich dann auf den Weg in eine norwegische Hütte am Fjord und Internet gab es dort nicht. Eventuell hätte ich mich vorher über norwegische Mobilfunkanbieter und deren passende Internet-Prepaid-Tarife schlau machen können, aber warum? Urlaub ist schließlich Entspannung und Ruhe.   
So verbrachte ich 9 Tage komplett ohne Internet. Auf zwei Ausflügen in norwegische Städte fand ich übrigens kein kostenloses WLAN. Ja, ich habe danach gesucht.

## Unerwartete Grenzen

Mir war vorher klar, dass mir die geliebten Dienste nicht zur Verfügung stehen werden und so passierte es mir dann auch sehr selten aus Gewohnheit eine App zu öffnen, die dank fehlender Datenverbindung wertlos geworden war. Viel öfter stutzte ich über eine nicht funktionierende App und ärgerte mich. Beispielsweise als ich die zweite Welt von *Plants vs. Zombies 2* betreten wollte, diese aber erst hätte geladen werden müsste. Oder wenn die Karten App von Apple nur ein leeres Raster anzeigte, obwohl ich die Orte im Vorfeld schon studiert hatte, offline Benutzung war doch ein Feature, oder?. Google Maps konnte das besser. 

Habe ich das ziellose Scrollen durch meinen Twitterstream vermisst? Natürlich! Ich ahnte schnell, dass ich garantiert einige Neuigkeiten verpassen werde und siehe da: Microsoft kauft Nokia. Trotzdem, es war schön und besonders ruhig. Und man denkt gar nicht so oft an diesen Mangel.     
Erst in den letzten zwei Tagen wurde ich etwas zittrig. Einerseits vor Freude der Berge an Neuigkeiten und Nachrichten, die auf mich warten und andererseits aus Angst vor den Bergen an Neuigkeiten und Nachrichten die auf mich warten und die es durchzuarbeiten galt. 

## Nachrichtenflut

Einige Zahlen: Tweetbot zeigte mir nur 300 ungelesene Tweets an. Allerdings änderte sich dieser Wert lange Zeit nicht, obwohl ich begann mich von unten nach oben durch meinen Stream zu arbeiten. Bei Netbot das gleiche. Feedly erwartete mich bei fast jeder Kategorie mit +499 neue Nachrichten, also den maximal angezeigten Wert. Mit denen bin ich fast durch und habe knapp 50 Artikel zum Lesen an Readability gesendet. Emails gab es mit 70 relativ wenig, wovon aber auch nur 10% wirklich sinnvoll waren (wie nicht anders zu erwarten war).

Ich kann mir kaum vorstellen eine richtig lange Zeit, z.b. 6 Monate, offline zu sein. Die Masse an Nachrichten, die dabei zusammenkommt, ist doch gar nicht zu bewältigen.  
Ärgerlich ist aber selbst nach 9 Tagen die Redundanz. Wie gerne hätte ich ein intelligentes System, welches mir eine Neuigkeit nur einmal anzeigt und entweder den &quot;besten&quot; Überbringer aussucht oder aus allen die wichtigsten Aussagen zusammen sucht. 

Eine kleine Auszeit vom Internet würde ich euch trotzdem ab und zu empfehlen.    
Die Ruhe tut gut.