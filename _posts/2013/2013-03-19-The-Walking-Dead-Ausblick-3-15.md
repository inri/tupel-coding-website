---
layout: post
title: "The Walking Dead, Ausblick 3.15"
description: ""
date: 2013-03-19 
category: medien
tags: [Serie, The Walking Dead]
published: true
comments: false
share: true
---

Fassen wir mal meine einigermaßen richtigen und weniger richtigen Vermutungen zusammen. 


Ja, der Governor bereitet eine Gefahr vor. Leider kein Panzer, sondern einen Pferdehänger voller Zombies, kennen wir ja schon. Ja, Andrea flieht. Leider nicht mit Tyreese.       
Ich war sehr dankbar, dass Andrea endlich den persönlichen Durchbruch hatte und ihr klar wurde, wer der böse Mann ist. Leider bewahrheitete sich auch meine Vermutung, dass der Governor mit ihr abgeschlossen hat. Das war zu Beginn der 14. Episode eventuell nicht der Fall, kristallisierte sich aber bei der Verfolgung und spätestens ihrem Trick gegen ihn deutlich heraus. Dass der Governor auf sie sauer ist, kann man nachvollziehen.      
Tyreese wurde noch nicht aktiv in Stellung gebracht, aber ich bin mir sicher, dass er den Braten riecht.

## Woodbury

Ich glaube in der nächsten Episode wird nicht sehr viel Woodbury auftauchen, aber was passiert dort noch, zumindest irgendwann? Klar, Tyreese konnte einerseits die Stadt nicht gefahrlos verlassen und andererseits weiß er ja nun, dass der Governor plant, die Gefängnisbewohner dahinzumetzeln. Wenn er ihnen helfen möchte, kann er das am besten hinter den feindlichen Linien tun.     
 
Noch eine Vermutung bzgl. des Todes des Governor. Schon jetzt wird ja gerätselt, wer ihn umbringen wird, wenn er denn tatsächlich am Ende der Staffel sterben sollte. Die höchsten Wetten gab es bei den Frauen, also Andrea, Michonne und Maggie. Aber warum kein Kerl und warum nicht Tyreese? Im Comic wird er zwar vom Governor getötet, allerdings ist da die Handlung eine völlig andere und ich könnte mir gut vorstellen, dass Kirkman für die Serie den Spieß umdreht. Oder man belässt es bei der Comic-Version, da wird er beim Sturm auf das Gefängnis von einer Frau aus den eigenen Reihen getötet, was wiederum auf Andrea deutet.

Milton wird es auch nicht gut ergehen, er ist schon im Visier des Governors.

## Gefängnis

Im Gefängnis muss noch die Frage um Michonne geklärt werden. Die Ausschnitte geben einige interessante Hinweise. Rick teilt sich Daryl und Merle mit und besonders letzterer sagt ihm klipp und klar welche Konsequenzen daraus folgen. Geradezu anerkennend bemerkt er Ricks Kaltblütigkeit, Michonne für die vage Chance zu opfern. Aber scheinbar wird auch ein Verlassen des Gefängnisses diskutiert.

Spätestens wenn Merle seine Anerkennung Rick gegenüber zeigt, sollte ihm klar werden, dass die Auslieferung Michonnes nicht der richtige Weg sein kann. Ich bezweifle auch, dass sie das mit sich machen lassen würde.     
Wie denken die anderen darüber? Keiner kenn sie so richtig und besonders vertrauenswürdig hat sie sich auch noch nicht gemacht. Aber den meisten wird, genauso wie Merle, klar sein, dass ihre Auslieferung bedeutet.

Ich wünsche mir ja eine Falle. Eine gespielte Auslieferung. Doch sind sie schlau und fit genug dafür?

## Zombies sind die Bedrohung

Im letzten Ausblick wollte ich das schon ansprechen. In dieser Staffel liegt der Fokus ganz klar auf die menschlichen Monster. Die Zombies werden ganz gut in Schach gehalten. Oder wann haben sie das letzte Mal wirklich Probleme gemacht? Im Gefängnis gab es einige kniffelige Situationen, aber die waren auch schon etwas besonderes. Die alltägliche Gefahr ist nicht mehr so präsent.       
Das wurde besonders schön bei Andreas Flucht zum Gefängnis deutlich. Nur mit einem Messer bewaffnet, meistert sie brenzlige Situationen, bei denen sie in den ersten beiden Staffel gnadenlos gefressen worden wäre. 

Ich wünsche mir zum Finale eine Überraschung von Seiten der Zombies. Eine Zombieherde ist leider nicht mehr zu erwarten. Vielleicht lassen sich die Autoren etwas tolles einfallen?