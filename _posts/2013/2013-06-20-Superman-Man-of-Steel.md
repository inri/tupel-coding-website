---
layout: post
title: "Superman - Man of Steel"
description: ""
date: 2013-06-20 
tag: [Film]
category: medien
published: true
comments: false
share: true
image: 
  feature: 2013-man-of-steel-lg.jpg 
  credit: WarnerBros
  creditlink: http://manofsteel.warnerbros.com/index.html
---

Superhelden sind zu einem der wichtigsten Themen im modernen Film geworden. Die zahlreichen und mannigfaltigen Comic-Vorlagen bieten eine breite Palette an Geschichten und so verwundert es ist nicht, dass sich als Genre neben Action und Humor auch Drama etabliert hat.      


Genau in der Mischung dieser Elemente unterscheiden sich die beiden großen Comic-Universen: DC und Marvel. Bei letzteren überwiegt das Schmunzeln, wohingegen bei DC die Welt ein wenig ernster und dunkler ist. In der Tradition von *Watchmen* und *The Dark Knight* reiht sich nun auch *Man of Steel* ein. Ein verdienter Platz.

Ich fasse mich in der Beschreibung der Filmhandlung kurz: Der Beginn des Films führt uns nach Krypton und in die Vorgeschichte zu Superman. Mit der Landung von Clarks Raumschiff springen wir in die Zukunft und erleben ihn als erwachsenen Mann in einer Art Findungsphase. Dieses erste Drittel des Films ist unterbrochen von Rückblenden zu seiner Kindheit und beschreiben mit wenigen Gesten und Worten wieso Superman so ist, wie er eben ist. 

Kurz nach der ersten Begegnung mit Lois finden wir uns im Hauptplot wieder und erleben die ersten *richtigen* Actionszenen. Es ist nicht zu viel gespoilert, wenn ich verrate, dass General Zod ein fieser Bastard ist. Das wird sofort in den ersten Minuten klar und dahin steuert letztlich auch der Film.       
Das Finale ist bombastisch und muss sich auf keinen Fall hinter einen *Avengers* verstecken. Mehr möchte ich zur Handlung nicht sagen. Der Film legt den Grundstein für Superman, so wie es einst *Batman Begins* getan hat. Der Fokus liegt auf der wichtigen Figur des Clark Kent und auch wenn es keine großen Geheimnisse gibt, sollte die Geschichte möglichst ungespoilert wirken.

#### Kraft und Macht

Superman wirkte überaus mächtig auf mich. Damit sind vor allem die Kampfszenen gemeint. Mit früheren Superhelden-Filmen braucht man das vermutlich nicht vergleichen, denn ich glaube kein Superman sah jemals stark aus. Dabei ist er der *Mann aus Stahl* und genau so sollten seine Kämpfe auch aussehen. In diesem Film sind sie hart, sind sie schnell und dynamisch, und es wird verdammt viel zerstört.       
Wenn man mich so fragen würde, ich denke Superman könnte locker alle Avengers vermöbeln. Der Hulk und Ironman wirken auch mächtig, aber durch die Präsenz von *Pseudosuperhelden* wie Black Widow, Hawkeye und Captain America wird dieser Effekt verringert. Auch Batman empfand ich niemals als körperlich übermächtigen Helden. Es wird interessant sein zu sehen, welchen DC-Helden Filme bekommen. 

#### Charakterentwicklung

Zugegeben, ich war etwas skeptisch wie die Autoren die Entwicklung von Supermans Charakter im Film verpacken. Einerseits kennt man ihn und seinen Kodex, andererseits ist es für einen Neuanfang immens wichtig den Grundstein zu legen. Das gelingt dem Film in den Rückblenden außerordentlich gut, wird aber auch konsequent bis zum Ende gehalten. Hier könnte ich eine Szene Spoilern, die besonders unter Fans zu Diskussionen führen wird. Meiner Meinung nach ist die Szene perfekt und gibt dem Film genau den richtigen Anteil an Drama.     
Am ehesten ist sie mit der Schlussszene aus *The Dark Knight* vergleichbar. Batman flieht vor der Polizei und Jim Gordons Sohn fragt wieso er verfolgt wird und sein Vater sagt: *Weil er der Held ist den Gotham verdient, aber nicht der den es gerade braucht, also jagen wir ihn, weil er es ertragen kann, denn er ist kein Held, er ist ein stiller Wächter, ein wachsamer Beschützer, ein dunkler Ritter.*

Ich liebe dieses Ende.

#### Apropos Liebe...

... ist zum Glück sehr dezent gehalten. Alles andere wäre auch unpassend gewesen, denn dieser Aspekt in Clarks Leben wird sich erst später entfalten. Humor übrigens auch, ganz dezent.

#### Verweise zum Universum

Bei einem Kampf streift Superman zuerst einen Satelliten mit dem Wayne-Enterprise Logo und später demoliert er einen Luthercorp LKW und ein sich im Bau befindendes Luthercorp Gebäude, dessen Eröffnung in 106 Tagen wohl etwas verschobene werden muss. Oder anders gesagt: Es wurden schon einige Brotkrumen für spätere Filme ausgelegt. 

#### Tagesaktuelles Geschehen

Witzig ist eine Szene am Ende, in der Superman einem General einen Überwachungssatelliten vor die Füße knallt und meint, dass die Regierung ihm eben Vertrauen muss. Ein Wink mit den Zaunpfahl, Mr. Obama.       
Den Grund, warum Krypton untergegangen ist, kann man sicherlich auch als Hinweis an die Menschen interpretieren, Stichwort: Ressourcen. Daneben findet sich eine deutliche Kritik an der kryptonischen Gesellschaft. Ich verrate nur so viel, dass ich mir gut vorstellen kann, dass wir Menschen ebenfalls in diese Richtung driften. Insgesamt sind es aber keine erhobenen Zeigefinger, sondern einfach logische Gründe.

## Fazit

Anschauen! Ja, er ist auf jeden Fall düsterer als *Avengers*, Spiderman und Thor. Etwas Lachen musste ich beispielsweise nur bei der Dummheit der Soldaten und am Ende gibt es einen guten Witz. Ansonsten ist es eine tragische Geschichte mit vielen Gänsehaut-Momenten.       
Kurz gesagt: Superman ist wieder cool, auf seine Art und Weise, und definitiv im *Club der Superhelden des 21. Jahrhunderts* angekommen. Ich freue mich sehr auf die Fortsetzung und muss nicht mal unbedingt andere Helden darin haben. *Man of Steel* hat in der relativ kurzen Filmzeit viel von Superman erzählt, aber dieser Charakter und Personen aus seiner Welt haben noch viel mehr zu bieten. Wir dürfen uns freuen.

#### P.S. Soll ich negative Kritikpunkte erwähnen? 

Auch wenn die Rückblenden kurz sind, unterbrechen sie die Handlung und so wirkt das erste Drittel des Films nicht *wie aus einem Guss*. Das ist nicht sehr tragisch, aber fällt auf. Mir missfiel die Präsenz der schiesswütigen Soldaten nicht so gut. Den Autoren war es wichtig, dass die Menschen mithelfen, aber es scheint sich nicht rumzusprechen, dass diese Außerirdischen kaum verwundbar sind. Ich hätte gerne mehr Interaktion zwischen Lois und Clark gesehen, aber das folgt sicher noch.