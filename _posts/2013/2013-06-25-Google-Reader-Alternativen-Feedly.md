---
layout: post
title: "Google Reader Alternativen: Feedly"
description: ""
date: 2013-06-25 
tags: [RSS]
category: it
published: true
comments: false
share: true
---

In exakt einer Woche befindet sich Googles RSS Reader in Rente und wir müssen uns bis dahin einen neuen suchen. Ich habe aktuell vier potentielle Reader auf dem Schirm, von denen aber erst zwei online sind. Heute teile ich mit euch meine Meinung zu *Feedly*.

Man meldet sich bei Feedly über den eigenen Google Account an und dann werden alle RSS Feeds importiert. Ich habe meine Feeds bisher eher simpel konsumiert und ob Feedly eure Ordner und Markierungen übernimmt, weiß ich nicht. Ich glaube favorisierte Artikel übernimmt Feedly als Read Later.

## Usability Web

Lesen lassen sich Artikel sehr gut, wenn nicht sogar besser als beim Google Reader. Man hat diverse Möglichkeiten [^1] einen Artikel zu verbreiten: Google+, Twitter, Facebook, LinkedIn, Buffer, Email. Man kann ihn auch direkt zu Instapaper, Pocket oder Evernote schicken. Ich habe bei Feedly meine Abonnements erst einmal sortiert. Unsortierte Feeds gibt es nicht, alle befinden sich in Ordnern, auch wenn es nur ein default-Ordner ist. Man kann zwischen 16 Design-Themen wählen. Nett.

Aber die wichtigste Frage: Wie fühlt sich die Benutzung an? Ich habe mir noch keine abschließende Meinung gebildet, das fiel mir bei einem anderen Reader leichter, aber sie ist erst einmal positiv. Makel gibt es trotzdem.       
Wenn ich die Feeds in einem Ordner durch klicke, wird manchmal der Counter des Ordners in der Seitenleiste nicht aktualisiert. Dann ist man fertig und denkt, es kamen gerade neue hinzu. Klickt also noch mal auf den Ordner und dann erscheint die Bitte zu Aktualisieren. Das Aktualisieren ist aber ein *refresh*-Button, nicht das Aktualisieren der gesamten Seite, denn das bringt nichts.      
Auch wenn man schnell mal paar Feeds wegklickt und dann die Seite schließt, kann es vorkommen, dass Feedly sie noch gar nicht als gelesen markiert hat.      
Man kann definitiv die verwendeten Technologien verbessern, Stichwort Websocket.

Ansonsten gibt es überall kleine Nettigkeiten, an die man sich schnell gewöhnt (Markieren einer kompletten Liste, Index-Ansicht, SavedForLater direkt in Feedly). Ob ich bei Instapaper bleibe oder Feedly als Speicher für Artikel nutzen werde, weiß ich noch nicht. Bei Instapaper kann man sortieren, mal sehen ob Feedlys Tags einen ähnlichen Nutzen haben.

## Usability iPhone

Für Feedly habe ich mich vor allem interessiert, da es eine eigene iPhone App mitbringt. Diese hat erst einmal ein okayes Design. Die Seitenleiste mit dem Ordnern kann man von links rein- und rausschubsen (Navigation Drawer). Wenn eine Kategorie leer ist, werden die letzten schon gelesenen Artikel angezeigt, was etwas verwirrend ist.

Das Laden ist relativ fix und im Vergleich zu meiner in den letzten Monaten erlahmte Reeder-App voll in Ordnung. Das Lesen ist auch in der App angenehm. Das Markieren als gelesen passiert durch einen Swipe. Ein kurzer Swipe für einen Artikel und ein langer für alle aktuell sichtbaren Artikel (4 - 5 auf dem iPhone 4).     
Das Lesen macht auch auf der App Spaß und zum Nacht-Modus lässt sich auch relativ leicht mit einem Klick wechseln.

### Fazit

Mit Feedly kann ich mich in der Tat anfreunden. Die Benutzung ist gut, das Design überzeugt und ich stelle mir nur noch eine Frage: Wie finanziert sich Feedly [^2]? Auf dem iPhone ist keine Werbung zu finden. Im Web auch nicht.

[^1]: Aktuell (August 2014) wirbt Feedly mit 100+ Dienste.

[^2]: Feedly hat ein Abomodell für Pro-Nutzer eingeführt. Kostenpunkt 5$ pro Monat.