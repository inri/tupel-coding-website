---
layout: post
title: "The Walking Dead, Finale der 3. Staffel"
description: ""
date: 2013-04-01 
category: medien
tags: [Serie, The Walking Dead]
published: true
comments: false
share: true
image: 
  feature: 2013-walking-dead-season-3-finale-lg.jpg
  credit: AMC
  creditlink: http://amc.com
---

Meine Vermutung bezüglich der Konfrontation war relativ richtig…

## Die Vermutungen

Tatsächlich gab es eine Spaltung der Gruppe und die Starken blieben im Gefängnis versteckt um die Angreifer zu vertreiben. Ein Unterschied zu meiner Vermutung war nur, dass die Schwachen nicht das Weite suchten, sondern sich im Wald versteckten, was ja auch pfiffig ist. Dass ich mit Tyreese und seiner Schwester auch noch einigermaßen richtig lag, freut mich nur halb, denn die beiden fanden Andrea leider nicht. 

Auch meine *Todesliste* war murks. Also kann ich nur nachträglich ein Gefühl schildern: Mir kam es nicht in den Sinn, dass es auch eine Alternative zu dem Tod des Governors geben kann, obwohl ich nicht das Gefühl hatte, dass seine Geschichte und sein Charakter zu Ende erzählt wurden. Wieso ich das nicht aufgeschrieben habe? Keine Ahnung.

## Meinung zum Kampf

Am Ende der letzten Episode stand für die Gefängnisbewohner die Frage im Raum: Kämpfen oder Fliehen? Ich war mir nicht sicher, ob das Gefängnis ein guter Ort zum Kämpfen sei, im Comic war es das nicht. Aber andererseits hat der Governor auch keine Armee, sondern größtenteils unbefleckte Stadtmenschen bei sich. Mit etwas Glück und Taktik, konnten sie schnell wieder vertrieben werden. Okay. Es war kein großes Bumm- und Krach-Finale auf Leben und Tod, aber durch und durch nachvollziehbar.

Als der Governor schlussendlich völlig den Verstand verliert, bestätigte sich mein Gefühl. Denn der TV-Governor war noch lange nicht der Governor aus den Comics. Nun ist er es. Ich hätte eventuell doch noch mit einem Auftauchen in Woodbury oder am Gefängnis gerechnet, aber man ließ ihn mit seinen Knechten davonfahren. Die nächste Staffel wird spannend. 

## Meinung zu Andrea

Dass es Milton nicht gut ergehen wird, vermutete ich schon nach der 14. Episode. Nachdem der Governor es scheinbar richtig toll findet die Personen zu töten und als Zombies durch die Welt laufen zu lassen, lag diese Art der *Bestrafung* nahe. Die Verbindung mit Andrea war eine gute Idee. Es zeigt noch einmal ihre Beweggründe und auch Miltons Wandlung.      
Doch dass sie der Gefahr tatsächlich nicht entgehen kann, war auch für mich überraschend. Man munkelt dass es wohl ein wenig die Entscheidung der Schauspielerin war und außerdem war der Tod von Andrea für viele Personen von Bedeutung und daher nicht unwichtig. Ich kann damit leben, obwohl man mit ihrer Figur noch viel hätte machen können, siehe Comic.   

## Meinung zu Carl

Rückte Carl in den letzten Episoden in den Hintergrund, macht er nun wieder von sich reden. Und wie! Eine durchaus diskussionswürdige Szene. Rick stellt sich die Frage, ob Carl angemessen reagiert hat oder nicht. Zu Beginn meint er, dass Carl ein Kind sei und diese vergessen schnell, aber später beweist ihm sein Sohn genau das Gegenteil. Doch war das trotzdem notwendig?      
Wir Zuschauer wissen, dass Allen und sein Sohn Jody es schon einmal auf Carl abgesehen hatten und ihn als leichtes Ziel beurteilten. Hätte sich Jody wirklich ergeben? Carl konnte es nicht wissen und traf eine brutale Entscheidung, obwohl er nur ein Junge ist.        
Rick erkennt, was seinem Sohn fehlt: Zivilisation. Als sich die Chance ergibt die Gruppe zu vergrößern, ergreift er sie. Carl scheint nicht sehr begeistert zu sein, aber das wird der Gruppe gut tun. Außerdem hatten sie bisher noch nie so viele Mitglieder, was auch was neues für die Serie und Handlung ist.

## Fazit

Jeder hat mit dem Tod des Governors gerechnet und genau das Gegenteil zu tun, ist schlichtweg grandios. Niemand ist sicher, auch nicht todsicher. Doch wohin wird die Reise führen?      
Rick ist nun der Anführer einer größeren Gruppe, aber wird man im Gefängnis bleiben oder nicht? An und für sich ist es ideal. Im Comic mussten sie gehen, weil die Zäune komplett zerstört und ihre Gruppe zerrieben wurde. Mit einer größeren Gruppe umherziehen dürfte auch schwierig sein. Ansonsten braucht man Vorräte für die vielen Menschen. So wird es weitere Außeneinsätze geben und eventuell auch Dinge wie Anbau von Nahrung. Oder wird wieder einen Zeitsprung durchgeführt? Und wo ist der Governor? Er wird wiederkommen, das ist sicher. Er hasst Rick und will Rache, aber noch fehlen ihm die Mittel. Wo findet man am besten eine Art Armee? 

Das könnten weitere Fragen sein: Wie wird sich Carls Charakter entwickeln? Bahnt sich eine Romanze zwischen Rick und Michonne an? Oder Michonne und Tyreese? Hat die Romanze zwischen Daryl und Carol nun wieder Chancen, da er eingesehen hat, dass er in dieser Welt nicht allein sein kann? Werden wir von den neuen Gruppenmitglieder mehr sehen oder bleiben sie im Hintergrund? 

## Themen der Staffeln

Kirkman möchte aus The Walking Dead keine Endlosserie machen. Er möchte gute Geschichten erzählen und sich dabei nicht wiederholen. Also was haben wir bisher an Themen gehabt?      

**Staffel 1** war eine Einführung in das TWD-Universum. Es wurde beschrieben, wie schwierig Entscheidungen sein können und wie fremde Menschen zusammenfinden.      
Danach führte der Weg zu Beginn der **2. Staffel** zur Farm, einem vorerst sicheren Rückzugsort. Hier ging es um die Frage der Gastfreundschaft, also wer hat welche Rechte in einer Zombie-Apokalypse. Daneben gab es den mehr oder weniger offenen Kampf um die Anführer-Position. Die Zombies gerieten in den Hintergrund und das rächte sich am Ende der Staffel.     
Das Gefängnis war erstmals seit Beginn ein Zombie-sicherer Ort und die Zombies waren keine Bedrohung mehr in der **3. Staffel**. Viel mehr stellten sich Menschen als Bedrohung heraus, natürlich mit Ausnahmen. Es scheint die Hohe Kunst zu sein, den richtigen Menschen zu vertrauen. Denn ohne Vertrauen geht es nicht, weil man dann allein oder fast allein da steht. 

Was kann die große Frage der kommenden Staffel sein? Scheinbar müssen sich die Menschen zusammenschließen um stark zu sein. Doch wie organisiert man ein solches Zusammenleben und wieso hat Woodbury nicht funktioniert, mal abgesehen vom geisteskranken Governor? Große Gruppen müssen sich auch gegen große Gruppen von Menschen verteidigen können und gleichzeitig alles notwendige zum Leben erwirtschaften. Oder vielleicht steht man auch einem übergroßen Gegner gegenüber und muss die bittere Pille der Versklavung schlucken, so ähnlich wie im aktuellen Stand der Comics? 

Viele Fragen und ein langer Sommer warten auf uns. 