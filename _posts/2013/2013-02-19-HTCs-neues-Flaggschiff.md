---
layout: post
title: "HTCs neues Flaggschiff"
description: ""
date: 2013-02-19 
category: it
tags: [Technologie]
published: true
comments: false
share: true
image: 
  feature: 2013-htc-one-lg.jpg 
  credit: HTC
  creditlink: http://htc.com
---

Vor wenigen Stunden stellte HTC sein neuen Smartphone mit dem altbekannten Namen HTC One vor. Ich gebe mal meinen Senf dazu, gehe aber nicht zu sehr in die Details. Für ausführliche Reviews gibt es genug andere Seiten.

## Ultrapixel

Drei Dinge stechen bei dem neuen HTC besonders ins Auge: Das Gehäuse aus Aluminium, die enorm hohe Pixeldichte des 4,7 Zoll Bildschirms und die wenigen, aber dafür umso größeren Pixel der Kamera.      
Letzteres ist auf jeden Fall ein interessantes Experiment. Der Bildsensor fasst zwar nur 4 MP, ist dafür aber so groß wie 13 MP-Sensoren und soll deshalb mehr Licht aufnehmen. Das ist besonders in Situationen mit weniger Licht hilfreich und führt zu weniger Bildrauschen. Zwei Dinge dazu: Die Fotos sind trotzdem nur 4 MP groß, zumindest habe ich bisher nichts anderes gehört. Weniger Rauschen ist ja gut und schön, aber nun stellt sich die Frage ob ich öfter bei gutem Licht fotografiere und lieber größere Bilder haben mag oder eher in dunkleren Gefilden knipse und die kleinere Bildgröße akzeptieren kann? Zweitens gab es noch keine Tests mit dem Gerät. Kann HTC das Versprechen einhalten? Wenn die Verbesserung für normale Bilder nicht sichtbar und die Rauschminderung nur minimal ist, wäre das natürlich ärgerlich. 

## Wer hat die meisten?

Der große 4,7 Zoll Bildschirm des One hat sagenhafte 1080p und nicht weniger als 468 ppi, zum Vergleich: das iPhone hat 326 ppi. Da freut sich natürlich der Android-Entwickler, denn so kann er mal wieder seine UIs anpassen. Der Bildschirm ist super. Aber sollte er bei der Entscheidung zwischen zwei Geräten der ausschlaggebende Punkt sein? Nein.      
Ich bin mir sicher dass beispielsweise Apple in den nächsten fünf Jahren die Pixeldichte bei den i-Geräten erhöhen wird. Aber aktuell ist das nicht notwendig und der Unterschied zwischen 4xx und 3xx ppi ist nicht riesig groß. Apple hat ja nicht umsonst das Wort Retina eingeführt. Es sagt ganz einfach aus, dass die Pixel des Gerätes so klein sind, dass man sie mit bloßem Auge nicht erkennen kann. Und so ist.       
Mich würde interessieren, wie groß der Preisunterschied zwischen den Pixeldichten ist und ob sich HTC damit einen Gefallen tut. 

## Aluminium

Das Gehäuse erinnert ebenfalls an das iPhone 5, aber da möchte ich ungern mutmaßen, ob man sich inspirieren ließ oder nicht. Das muss man sich mal genauer anschauen. Immerhin ist die Rückseite gewölbt.      
Ein weiterer interessanter Punkt ist die Dicke des One. Ich vermute mal, dass HTC das Gerät sehr dünn gestalten wollte und das ist ihnen auch gelungen. Es ist 0,3 mm dünner als das iPhone 4S. Dagegen das iPhone 5 kann es noch um 1,3 mm unterbieten. Mal sehen was Samsung demnächst präsentieren wird und vielleicht kann man daraus schließen, dass ein enorm dünnes Smartphone nicht so einfach zu bauen ist.     
Pluspunkt für das HTC: Das NFC ist auch im Aluminiumgehäuse integriert, neben der Antenne und LTE. Das bedeutet, dass ein iPhone aus Aluminium vielleicht doch noch mit NFC ausgestattet wird.

## Software

Ach HTC, was soll der Mist? Stolz wie Bolle stellte man das neue Sense vor und Spielereien wie *BlinkFeed* und *Zoe*. Ersteres eine Art [Flipboard](http://flipboard.com/) und letzteres ein eigenes Format für Kurzvideos mit Bildern. Wer bitte braucht solche Hersteller-spezifischen Formate? 
Natürlich konnte auch nicht das Versprechen fehlen, dass man so schnell wie möglich auf die neue Android Version updaten wird. 

Zur Software und insbesondere Buttons sei auf [Gruber](http://daringfireball.net/linked/2013/02/19/htc-one) verwiesen, der eine Ähnlichkeit zu einem anderen Smartphone und dessen Button-Funktionalität entdeckt hat.

## Fazit

Ich weiß es doch auch nicht. Äußerlich mag es zu gefallen. Es sieht dünn und leicht aus und der Bildschirm macht sicherlich auch Spaß. Natürlich ist es zu groß, aber hat man noch eine Wahl im Android-Universum? Ob die Kamera wirklich so toll ist, bleibt abzuwarten. Ich finde das HTC One nicht uninteressant, aber Android bereitet mir Bauchschmerzen. Wird HTC schnelle Updates liefern oder mit der Anpassung vom neuen Sense überfordert sein. Immerhin scheint HTC alle Karten auf ein Smartphone zu setzen.      
Der Spiegel bemängelt, dass das One zwar hübsch sei, aber nicht neu. Doch was ist neu? 3D-Bildschirme scheinen wohl erstmal vom Tisch, ansonsten sind 468 ppi doch nicht gerade wenig. LTE und NFC ist auch drin, dazu ein Experiment mit den Kamerapixeln.  

Was kann man mit Smartphones noch machen? Ich schrieb das vor Monaten schon und ich sage es gerne noch einmal: Die Hardware läuft der Software davon! Aber gerade die Software sollte nicht von den Geräteherstellern kommen, denn das behindert nur den Android Kern. Und was HTC da gezeigt hat, ist auch nicht innovativ. Ich weiß auch nicht, was man erwarten kann.