---
layout: post
title: "Mio Mio Mate"
description: ""
date: 2013-04-19 
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
image: 
  feature: 2013-mio-mio-mate-lg.jpg 
  credit: Vivaris
  creditlink: http://www.vivaris.net
---

Als ich vor einigen Tagen im Edeka meines Vertrauens unterwegs war, entdeckte ich in der Getränkeabteilung auf einem Kasten Club Mate eine mir unbekannte Flasche: Die *Mio Mio Mate*. Weit und breit stand nur eine da und neugierig wie ich bin schnappte ich sie mir. Im gleichen Augenblick tauchte neben mir mir ein Edeka-Mitarbeiter auf und erzählte, dass er mal eine bestellt hat, aber womöglich keine weiteren bestellen wird. Außer ich frage drei Mal in der Woche danach.      
Eigentlich müsste ich meine Homies bei Edeka mal zu einem Umtrunk einladen. Man kennt sich, man grüßt sich, man bestellt sich neue Getränke und legt das Schicksal eben dieser in die Hände des Kunden. Wow. Ich mag Edeka. 

Ich schmunzelte und versprach ihm zu berichten wie sie mir gemundet hat. Ihr bekommt das aber sofort zu lesen! 

Ich mag das Flaschenformat von Club Mate, aber die dickere Flasche der Mio Mio Mate gefällt mir auch sehr gut. Die Farbgebung wirkt schön Retro. Das Icon der Wahl scheint eine Art mexikanisch/inkaischer(?) Vogel zu sein. Damit kann man leben.

Sie schmeckt wie Mate. Leider hatte ich gerade keine Club Mate zum Vergleich bei der Hand, aber meinem Geschmacksgedächtnis nach würde ich behaupten, dass sie weniger süß ist als Club Mate. Das finde ich richtig gut. Die harten Zahlen sagen etwas anderes. Sie hat auf 100 ml immerhin 0,7 g mehr Zucker (5 g vs. 5,7 g) und 4 kcal mehr (20 kcal vs. 24 kcal). Vielleicht hätte ich doch einen direkte Vergleich machen sollen.

Anyway. Sie ist okay und ich würde sie gerne noch einmal probieren. Das werde ich meinen Edeka-Connections auch ausrichten.

Übrigens, der Koffeingehalt ist bei beiden gleich (20 mg/100 ml). Hergestellt wird die Mio Mio Mate von Vivaris Getränke GmbH. Aha. Weitere Meinungen gibt es unter Google, Stichwort *Mio Mio Mate*.