---
layout: post
title: "Zugvögel"
description: "Zugvögel auf den Weg in den Süden."
category: blog
tags: [iPhone, Fotografie]
comments: false
share: true
image:
  feature: 2011-birds-november-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
thumb: 2011-birds-november-200.jpg
---

Von diesem Bild bzw. Motiv habe ich zwei Versionen. Das erste fotografierte ich vor zwei Jahren und das zweite (unten) vor einem Jahr. Damals im Herbst 2011 war ich gerade frisch gebackener iPhone-Besitzer und experimentierte begeistert mit dessen Kamera. Das Foto entstand sehr spontan. 

Ich war auf dem Weg zur S-Bahn und sah durch die Bäume einen Schwarm Zugvögel. Sofort zückte ich mein iPhone und benutzte die Schnellfunktion um die Kamera zu öffnen (über den Lockscreen). Erstaunlicherweise funktionierte das ausgesprochen gut und ich bekam damals meinen Schnappschuss. Einige Sekunden später waren die Vögel hinter einem Gebäude verschwunden, was man auf dem Originalbild auch sehen kann.

Der Filter gibt dem Bild ein Aquarell-artiges aussehen.

<figure class="half">
	<a href="/images/2011-birds-original-november-1600.jpg">
		<img 
		  src="/images/2011-birds-original-november-400.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
	<a href="/images/2011-birds-november-1600.jpg">
		<img 
		  src="/images/2011-birds-november-400.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
	<figcaption>Original und Bearbeitetes</figcaption>
</figure>

Das gleiche Motiv habe ich letztes Jahr noch einmal geschossen, diesmal in Finsterwalde. Ich hatte zwar einige Sekunden länger Zeit, denn ein störendes Gebäude war nicht in der Nähe, aber der Fokus liegt leider trotzdem links auf den Bäumen.

<figure>
	<a href="/images/2011-birds-october-1600.jpg"><img src="/images/2011-birds-october-800.jpg" alt="Zweite Version des Zugvögel-Motivs."></a>
	<figcaption>Nicht so gelungen wie das erste Motiv, hat aber auch seinen Charme.</figcaption>
</figure>