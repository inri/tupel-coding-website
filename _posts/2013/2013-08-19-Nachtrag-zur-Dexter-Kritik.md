---
layout: post
title: "Nachtrag zur Dexter Kritik"
date: 2013-08-19 14:00:00
category: medien
tags: [Serie]
published: true
comments: false
share: true
---

Uns erwartet noch 4 Episoden Dexter, dann ist die Geschichte des &quot;*beliebtesten Serienkillers*&quot; zu Ende erzählt. Puh. [Vor einer Woche habe ich beschrieben]({{ site.url }}/blog/Dexter-und-die-schwache-finale-8-Season.html) wieso mir die letzte Staffel von Dexter nicht gut gefällt. Zu einigen Spekulationen möchte ich jetzt, nach dem Anschauen der 8. Episode, gerne etwas ergänzen.   
Spoiler ahead!

## Kein Season-Killer

Das Katz-und-Maus-Spiel zwischen Dexter und dem Hauptbösewicht bzw. Season-Killer war stets eine der Stärken der Serie. Dabei mussten wir als Publikum nicht sofort die Identität desjenigen kennen, aber es gab immer eine direkte Verbindung zu Dexter. 

**Season 1**     
Der Kühllaster-Killer und gleichzeitig Dexters Halbbruder schickte ihm die Barbiepuppen-Körperteile und stand sowieso im Mittelpunkt der ersten Season.

**Season 2**     
Dexter wird selbst zum Gejagten durch Sgt. Doakes.

**Season 3**     
Der *Häuter* treibt sein Unwesen und entführt am Ende sogar Dexter selbst.

**Season 4**     
Wer erinnert sich nicht gerne an den *Trinity-Killer* zurück?

**Season 5**     
Die Season mit Lumen war auch eine tolle Zeit und im Zentrum stand die Gruppe aus Vergewaltigern.

**Season 6**     
Der Season-Killer Travis mit seinen religiös-rituellen Morden war vielleicht nicht der spannendste, aber diese Morde waren der rote Faden der Season.

**Season 7**     
Auch wenn es in der vorletzten Season eher um Hannah und Debra ging, war Isaak Sirko als russischer Bösewicht sehr spannend.

**Season 8**     
Ja, um wen geht es in dieser Season? Der Vorschau auf die 9. Episode nach zu urteilen ist der wahre *Brain Surgeon* die Psychiaterin Dr. Vogel. Überraschung. Wie bereits schon gesagt, man hatte immer so ein seltsames Gefühl bei ihr, eben auch weil der mutmaßliche Killer selbst überrascht war sie zu sehen und der richtige Killer ihr ja die Päckchen mit den entnommenen Hirnteilen hinterließ. Trotzdem ist dieser Handlungsstrang in den anderen Irrungen und Wirrungen rund um Dexter und sein Geheimnis untergegangen bzw. hätten die Autoren schon viel eher die Katze aus dem Sack lassen sollen, wenn nicht für Dexter, dann zumindest für uns.

## Motivation

Wieso tauchte Dr. Vogel jetzt auf und welche Art Spiel spielt sie mit Dexter? Will sie sich selbst beweisen, dass sie einen Serienkiller manipulieren kann? Oder ist es eine Art Rache, für der wir aber nicht den geringsten Hinweis haben? Oder soll es uns zeigen, dass Dexter am besten niemanden vertrauen sollte?

Es kann nicht mehr lange dauern bis Dexter die richtigen Schlüsse zieht. Was passiert dann? Schließlich befinden wir uns dann in der 9. Episode und ich bezweifle, dass die Jagd auf Dr. Vogel 4 Episoden lang dauert.

Wie bereits gesagt, ich kann mir keinen Reim auf die Handlung bilden. Debra geht wieder zurück zur Polizei und Hannah bleibt in Miami und wird von einem Marshall gesucht. Hört sich wieder nach Kleinigkeiten an und in wie fern diese Handlungen dem roten Faden dienen, bleibt abzuwarten.