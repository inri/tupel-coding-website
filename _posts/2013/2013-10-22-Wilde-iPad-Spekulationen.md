---
layout: post
title: "Wilde iPad Spekulation"
date: 2013-10-22 14:00:00
category: it
tags: [iPhone, iPad, Apple]
published: true
comments: false
share: true
---

Ich möchte auch mitraten! 

### iPad mini 2

Der Preis von 329€ wird bleiben bzw. nur marginal erhöht, vielleicht auf 349€? Daher glaube ich auch nicht, dass es ein TouchID-Upgrade geben wird. Wenn der Preis um 300€ höher wäre, könnte man schon eher damit rechnen.  
  
Eine Vermutung ist auch, dass das neue iPad mini das iPad 2 ersetzen, also 399€ kosten, und das alte iPad mini für ca. 250€ angeboten wird. Aber ob Apple damit noch genug Gewinn einfährt?

Retina halte ich für ziemlich sicher. Die Bildschirme gibt es und die Konkurrenz verbaut sie. Außerdem bezweifle ich dass Apple seine non-Retina Geräte in der Produktlinie behalten möchte. 

Den Prozessor für ein Retina Bildschirm gibt es auch und der ist im iPad 4 als A6X drin. Ein Update auf den neuen A7 halte ich für zweifelhaft, das iPhone 5c hat ja auch noch den alten A6er und kostet 600€.

### iPad 5

Dem großen Bruder würde sicherlich ein stärkerer & stromsparender Prozessor guttun, und das 64-Bit-Upgrade. 

Retina ist das iPad 4 ja schon gewesen, aber das Design war noch nicht überarbeitet. Das neue iPad wird schlanker und schicker.

TouchID ist eine interessante Frage. Ich glaube nicht. Ich glaube es macht beim iPad noch nicht so viel Sinn, denn es ist eine andere Gerätekategorie. Nächstes Jahr wird es bestimmt kommen (geringer Kosten für den Sensor), aber dieses Jahr bleibt es dem iPhone vorbehalten.

### iPad sonst

Die Farben sind mir eher egal. Naheliegend wäre eine einheitliche Farbgebung zusammen mit den iPhones.    
Natürlich wird es einige kleine Updates geben (Kamera, Akku).   

Das iPad 2 wird eingestellt und mit dem Retina-Update des minis wird die komplette iPhone-iPad-Linie *retinasiert*.    
Nächstes Jahr ist dann die 64-Bitisierung an der Reihe.

### Sonst

* Endlich OS X Maverick und hoffentlich schon Ende der Woche verfügbar.    
* Mavericks und seine Features sind größtenteils bekannt, also kann man auf eine Überarbeitung der iWork Suite hoffen.
* Es wird mehr Infos zum Mac Pro, aber das dürfte eher die Profis interessieren.    
* Einige hoffen auf neue Bildschirme und letztlich hieß es in Apple Einladung &quot;*We still have a lot to cover*&quot;.
* Die iMacs bekamen vor einigen Wochen schon ihr stilles Update.   
* Die MacBooks sind mit Sicherheit an der Reihe, aber ein Retina MacBook Air wird es leider noch nicht geben.



