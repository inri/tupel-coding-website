---
layout: post
title: "Die Wahrheit von Bildern"
description: ""
date: 2013-05-17 
category: blog
tags: [Technologie]
published: true
comments: false
share: true
---

In den letzten Tagen gab es diverse Berichte zu einem Foto, welches zum *World Press Photo 2012* gekürt wurde, aber anscheinend etwas mehr oder weniger nachbearbeitet wurde. Die Organisation meldete sich auch zu Wort und [bekräftigt ihre Entscheidung](http://www.worldpressphoto.org/news/digital-photography-experts-confirm-integrity-paul-hansen-image-files). Was ist davon zu halten?

Auf [hackerfactor.com](http://www.hackerfactor.com/blog/index.php?/archives/550-Angry-Mob.html) kann man eine schöne vorher-nachher-Version des Bilder finden und sich vielleicht schon eine Meinung bilden. Vor allem der Farbton des Bildes wurde bearbeitet. Es wurden also keine Personen oder anderes hineingeschnitten. Aber nach Meinung einiger ist dies schon zu viel Manipulation des originalen Bildes oder zumindest zu viel um mit einem Preis ausgezeichnet zu werden. Das führt mich zu der Frage: 

**Was ist das originale Bild?**

Fotografieren ist in technischer Hinsicht mittlerweile ziemlich komplex geworden. Beim Drücken auf den Auslöser wird das Bild, genauer das Licht, auf den Fotochip gejagt und da passiert schon die erste Interpretation des Bildmaterials. Man hat also das Bild wie es der Chip wahrnimmt. Das bekommt man aber nicht direkt, sondern es wird ein weiteres mal verändert in dem die Kamera es in das RAW-Format umwandelt.      
Mit diesen RAW-Daten arbeiten die meisten Profifotografen, denn sie bieten wesentlich mehr Informationen über das Bild. Doch wie gesagt, sie sind nicht das Bild an sich. Bei den meisten Consumer-Kameras bekommt der Fotograf sogar nur eine JPEG-Datei zurück, welche wiederum eine weitere Interpretation der Daten des RAW-Bildes darstellt. Mit jeder Stufe sinkt die Anzahl der Information und das Bild ändert sich geringfügig. Was ist das Original?  

Die Umwandlung eines Bildes von RAW zu JPEG ist von Gerät zu Gerät unterschiedlich. Knipse ich ein Foto und lasse meine Kamera ein RAW und ein JPEG ausgeben, kann ich später am Computer das RAW-Bild selbst in JPEG umwandeln und werde feststellen, dass die beiden JPEGs sich leicht unterscheiden werden.       
Worauf will ich hinaus? Es gibt kein richtiges Original. Das einzige Original befindet sich rechts und links neben unserer Nase. 

## Situation des Fotos

In dem beschriebenen Fall sehe ich das Bild folgendermaßen. Da stehst du als Fotograf am Ende einer engen Straße und da kommt dir eine riesige Menschentraube entgegen. Sie jammern, klagen und schreien weil sie ihre toten Kinder zu Grabe tragen. Wie viel Zeit bleibt dem Fotograf um die Kamera so einzustellen, dass man die Stimmung und Gefühle, die man selbst spürt und vermitteln möchte, korrekt einfängt? Lässt man zu viele Sekunden verstreichen, gelangt die Masse auf die offene Straße und die Umgebung passt einfach nicht mehr oder lässt kein Foto mehr zu.       
Ich würde das Foto sofort machen. Und ich würde es auch geringfügig bearbeiten. Das Foto ist nämlich gut. Man kann übrigens auch nur einigermaßen gute Fotos vernünftig bearbeiten. 

Sicherlich kann man über Fotos und ihre wahre Natur munter streiten. Aber mir ist es lieber, dass Fotografen solche Momente einfangen und bearbeiten, als dass sie die Chance verstreichen lassen oder die Fotos nicht veröffentlichen. 