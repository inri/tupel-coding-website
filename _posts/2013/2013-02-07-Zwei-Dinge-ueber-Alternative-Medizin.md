---
layout: post
title: "2 Gedanken über Alternative Medizin"
description: "2 Gedanken über Alternative Medizin"
date: 2013-02-07
category: blog
tags: [Medizin]
published: true
comments: false
share: true
---

Ich übersetze hier mal zwei sehr gute Gedanken über *alternative-medicine*

*Eines der bezeichnendsten Kriterien der Medizin ist, dass ein vor 30 Jahren verstorbener Arzt, der heutzutage arbeiten möchte, eine neue Ausbildung durchlaufen muss. In einigen Disziplinen kann man diese 30 Jahre sogar auf nur 10 Jahre verkürzen. Und genau das steht im Kontrast zu Pseudowissenschaften wie Homöopathie. Wenn der Begründer von Homöopathie, Samuel Hahnemann (geb. 1755) heute leben würde, könnte er seine Praxis wieder eröffnen und seine Patienten ohne Änderung seiner ursprünglichen Theorien behandeln.*

*Es gibt so etwas wie Alternative Medizin nicht. All das was tatsächlich wirkt und den Placebo-Test besteht, sollte Medizin genannt werden, miteingeschlossen Pflanzen und andere Substanzen, die manchmal als alternative Medizin bezeichnet werden. Im Gegenteil dazu verdient all das, was den Placebo-Test nicht besteht, den Term Medizin nicht und sollte anders genannt werden.*

