---
layout: post
title: "Selbstexperiment: 30 Stunden wach"
description: ""
date: 2013-05-31 
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
---

Mir ist heute bewusst geworden, dass ich noch nie eine Nacht im komplett nüchternen Zustand durchgemacht habe. Also *durchmachen* im Sinne von nicht schlafen, also auch nicht erst Früh und dann bis Nachmittag schlafen, sondern einfach gar nicht schlafen und zwei Tage lang wach sein. 


Zu Festveranstaltungen wie Silvester trinkt man ja meistens ein Schlückchen mehr oder weniger und außerdem geht es dann trotzdem noch ins Bett, auch wenn es schon Morgen ist. Bei den meisten Partyabenden ist es ebenfalls so. Aber einfach wach sein und konzentriert einer Tätigkeit nachgehen? Eine Premiere für mich.

Doch zuerst zur Vorgeschichte. Meine liebe Freundin ist zur Zeit im Urlaub und ich bin allein in Potsdam. Neben vielen Nachteilen, hat es aber einen Vorteil: Ich kann meine Zeit komplett frei einteilen. Ich arbeite gerne nachts und gehe auch relativ spät schlafen. Die letzten Tage war es nie vor 3 Uhr früh. Aber gestern, also Donnerstag, konnte ich richtig lange, richtig gut arbeiten.       
Tagsüber lief ich so auf 70 bis 80%, habe einige Übungen bearbeitet und die eine oder andere Serie nebenher geschaut. Abends habe ich mich an mein Android Projekt gesetzt und zahlreiche Bugs behoben. Mir ging es gut, ich war konzentriert und ich kam vor allem gut voran. Dann wurde es 2 Uhr, 3 Uhr und 4 Uhr, aber ich nicht müde.

Neben der eigentlichen Müdigkeit geben sich meist die Augen zuerst geschlagen. Meine Lider wurden zwar eine zeitlang etwas schwerer, aber da war es auch schon nach 6 Uhr früh. Zwei Stunden später frühstückte ich und ging wiederum zwei Stunden später, also gegen 10 Uhr, duschen. Nach der Dusche fühlte ich mich genauso wach wie sonst auch nach 6 Stunden Schlafen. Ich programmierte noch bis 11 Uhr und ging dann in die Stadt.

## Seltsame Realität

Was mich am meisten verwundert hat, ist meine verschobene Realitätswahrnehmung, besonders in Bezug auf die vergangenen Stunden. Mein Gedächtnis ist da irgendwie seltsam, vermutlich übermüdet. Ich könnte jetzt nicht sofort wiedergeben wann genau und was genau ich in den letzten 30 Stunden gemacht habe. Jetzt da sich der Abend nähert, ertappe ich mich ab und zu dabei, wie ich sekundenlang auf den Bildschirm des Macbook starre und angestrengt nachdenke, was ich gerade machen wollte. Oder beim Korrekturlesen dieser Zeile langsam beginne zu tagträumen.       
Auf jeden Fall ein seltsames, aber interessantes Gefühl. Nichtsdestotrotz gehe ich heute etwas zeitiger ins Bett als sonst und versuche bis dahin nicht einzuschlafen.
