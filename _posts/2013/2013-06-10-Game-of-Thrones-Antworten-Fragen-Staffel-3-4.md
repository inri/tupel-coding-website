---
layout: post
title: "Game of Thrones, Antworten und Fragen zu Staffel 3 und 4"
description: ""
date: 2013-06-10 
category: medien
tags: [Serie]
published: true
comments: false
share: true
---
 
Was war das nur für eine grandiose Staffel mit einem würdigen Abschluss! Vor gut 2 Monaten habe ich einige Fragen an diese Staffel gestellt und lasse die Antworten noch einmal Revue passieren. Außerdem ergeben sich selbstverständlich neue Fragen.

### Arya und Gendry

Jaqen haben wir leider nicht wiedergesehen, aber er wurde von Arrya (indirekt?) erwähnt. Immerhin.       
Arya und Gendry trafen auf die Rebellen und machten mal wieder Bekanntschaft mit Magie (Wiederbelebung durch den Gott des Feuers). Doch dann wurde Gendry von der roten Priesterin abgeholt, verführt um sein Blut zu gewinnen und sollte anschließend geopfert werden. Davos begann an seinem König Stannis ein weiteres Mal Verrat und setze Gendry in ein Boot Richtung Kings Landing.
Arya fiel dem Hound in die Hände und war damit nicht ganz glücklich. Aber der ist meiner Meinung nach einer von den Guten. Er wollte sie *nur* zu ihrer Mutter bringen. Leider oder zum Glück kamen sie zu spät zur Red Wedding und sind nun wieder unterwegs, wohin auch immer.       
Wohin wird es Arya und Gendry als nächstes verschlagen oder besser gefragt: Wo sind sie sicher? Gendry soll nach Kings Landing, dort kennt er sich zumindest aus. Doch wem vertraut der Hound? Eventuell geht es Richtung Nights Watch oder vielleicht treffen sie in der Gegend ihren kleinen Bruder und Osha?

### Kings Landing

Sansa und Tyrion sind verheiratet. Es ist zwar nur eine politische Verbindung, aber dennoch nicht ganz unwichtig. 
Ob es von Tywin eine gute Idee war, seinen Sohn zur Hochzeit zu zwingen? Oder ihm zu sagen, dass er Tyrion als Baby nur nicht umgebracht hat, weil es zum Wohle der Familie war und obwohl er es persönlich gerne getan hätte?       
Littlefinger hatte seine gierigen Hände auch schon nach Sansa ausgestreckt und dürfte enttäuscht sein. Außerdem ist Tyrion wieder bei den Sitzungen dabei und macht ordentlich Stunk gegen Joffrey. Wobei Joffrey irgendwie jeden ans Bein pisst, inklusive seines Opas. Ich kann mir gut vorstellen, dass sein Verhalten keine weitere Staffel toleriert wird, wer auch immer ihn *zur Vernunft* bringen wird.        
Dass Jamie und seine Begleiterin nicht ohne Probleme nach Kings Landing gelangen, konnte man auch ahnen. Wie enorm wichtig die Geschehnisse der beiden allerdings für die Charakterdarstellung sein würde, war doch überraschend. Von Jamie kann man noch einiges erwarten, da bin ich mir sicher. 

Nun stellt sich die alte Frage aber ein weiteres mal. Seine Schwester / Geliebte hat er bereits begrüßt, aber wie wird es weitergehen? Sie soll den Bruder der Königin in spe heiraten, doch seinem Vater kann Jaimie wohl kaum widersprechen. Wie wird er auf Jaimies fehlender Hand reagieren, denn schließlich war sein Sohn ein Ritter. Was ist er jetzt noch?      
Joffrey ist ein kleiner Widerling und obwohl Margaery Tyrell eine ganz pfiffige Frau ist, bin ich mir trotzdem nicht sicher, ob sie sich immer gegen ihn behaupten kann.

### Der Norden

Die versprochene Hochzeit mit einer Frey-Tochter nicht einzulösen, war ein großer Fehler von Robb. Die Konsequenzen waren deutlich. Ich kann jedoch nicht behaupten, dass es mich völlig überrascht hat. Mit dieser Heftigkeit hatte ich aber nicht gerechnet... puh. Immerhin gibt es nun einen Erzählstrang weniger, wie ein Bekannter schon makaber feststellte.       

Die Eiszombies (White Walkers) haben wir leider nur kurz gesehen und wissen jetzt zumindest, dass sie nicht unbesiegbar sind. Allerdings marschiert trotzdem eine große Armee von Untoten auf. Diese Kunde wird in Westeros verbreitet und vermutlich eine zentrale Handlung der nächsten Staffel. Supi. 
Jon Snows Gastspiel bei den Wildlings ist also beendet. Diese planen vermutlich immer noch ihren Kampf gegen die Nights Watch, aber da werden wohl ein paar White Walkers für etwas Ablenkung sorgen. Sehen wir eventuell eine brüderliche Allianz gegen die Zombies? 

Den Verlauf um die Geschichte von Theon Greyjoy haben die Autoren ungeschickt umgesetzt. Die 2. Staffel endete mit der *Meuterei* seiner Schiffscrew und die Zuschauer wussten nicht, wie es weitergeht. Plötzlich war er in einem Verlies und wurde die gesamte 3. Staffel über von einem Unbekannten gefoltert und das scheinbar nicht zu Hause auf den Iron Islands. Erst in der letzten Episode erfahren wir von wem und wieso. Sein Vater wird erpresst, er soll sich aus dem Norden zurückziehen, aber sein Sohn ist ihm egal. Doch seiner Schwester nicht und so segelt sie ihm mit einem Schiff und guten Männern zur Rettung.      
Warum Haus Bolton, welches nun den Norden regieren soll, gegen Haus Greyjoy taktiert, versteh ich als Zuschauer leider nicht. Vermutlich gibt es eine Art Rivalität, aber ich hätte von Tywin Lannister erwartet, dass er sich die Feinde seiner Feinde zu Freunden macht. Andererseits wäre Lord Bolton ohne Zusicherung auf den Norden wohl nicht beim Verrat an Robb dabei gewesen.      
Ich denke dass Asha ihren Bruder Theon befreien wird und wir es dann mit einem *neuen* Theon zu tun haben. Denkbar wäre ein Krieg zwischen den Boltons und Greyjoys, aber die White Walkers kommen ja auch noch. 

Die letzten verbliebenden männlichen Starks trennen sich. Dass Bran scheinbar ein ziemlich mächtiger Warg ist, haben wir relativ zeitig in der 3. Staffel erfahren. Nun will er in den Norden um die Armee der White Walkers aufzuhalten, wie auch immer er das machen will.      
Deshalb die Frage: Wie will er das vollbringen? 

### Drachen und andere

Wann wird Daenis Targaryen übers Meer nach Westeros segeln? Die Frage bleibt bestehen.       
Ich sehe die Drachenkönigin nicht ganz so perfekt und gut wie viele andere sie sehen. Sie war in der 3. Staffel die große Befreierin und dass sich die Sklaven ihr freiwillig anschließen ist gut und schön. Aber werden sie noch für sie kämpfen, wenn es Gegenwehr und Verluste gibt?  Außerdem sieht sie sich immer noch als rechtmäßige Herrscherin über Westeros, müsste den Bewohner dort konsequenterweise die gleiche Wahl wie den Sklaven lassen.nnnnnn
Eventuell stellt sich die Frage, wann sie ankommen wird. Wird Westeros von White Walkers überlaufen sein oder ist der Kampf gegen jene gerade vorbei und die Kräfte der anderen geschwächt?

Über die Magie der roten Priesterin haben wir leider nichts genaues erfahren. Das Schattenwesen war scheinbar auch eine einmalige Geschichte. Überraschend ist tatsächlich ihre Zukunftsvision, die ganz klar die Gefahr im Norden sieht.
Mir wurde bewusst, dass die vielen Prisen Magie und Zauberei nicht rational erklärt werden. Magie ist da, aber nicht im Überfluss.

### Fazit

Das war eine gute Staffel, auch wenn viele Fragen noch nicht abschließend beantwortet sind und bestimmt nicht schnell beantwortet werden.      
Die Red Wedding wird vielen im Gedächtnis bleiben. Mir persönlich gefällt das Ungleichgewicht auf Seiten der Lannisters nicht so sehr, wobei man andererseits auch zugeben muss, dass nicht mehr jeder Lannister von der Sache überzeugt ist, siehe Tyrion und Jamie.      
Einige Charaktere haben interessante Veränderungen durchgemacht und das zeigt die hohe erzählerische Qualität dieser Serie, auch wenn mal jemand sterben muss.      
Ich lese parallel die Bücher und bin gerade am Ende der 1. Staffel. Bis die 4. Staffel beginnt, werde ich hoffentlich aufgeholt haben und kann vielleicht einen detaillierteren Vergleich bieten.