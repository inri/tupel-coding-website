---
layout: post
title: "Warum kein Flaggschiff Android mit 3,x"
description: ""
date: 2013-03-15 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Samsung setzt der Jagd nach Zollgrößen einen drauf und präsentierte das neue S4 mit einem 5 Zoll Bildschirm.

Auch das neue HTC One ist knapp an der 5 Zoll Grenze. Ich frage mich: Wieso erscheinen in letzter Zeit keine leistungsstarken Android Telefone mit 3,6 Zoll oder so ähnlich? Samsung soll das S4 nicht einstampfen, aber wieso gibt es keine Alternative? Sie werfen doch sonst so viele Smartphones auf dem Markt. Doch von diesen Schrott-Smartphones hört man irgendwie nichts.

**Theorie 1: Man glaubt dass alles über 4,5 Zoll die bessere Bildschirmgröße für ein Smartphone sei.**       
Ich halte die Größe für Quatsch, aber es gibt durchaus Menschen, die das mögen. Okay. Aber das ist nicht die dominierende Größe, sonst würde sich ein einzelnes Smartphone (iPhone) über die Jahre nicht so gut verkaufen. Und ja, das iPhone stellt in Sachen Verkäufe jedes andere in den Schatten.

**Theorie 2: Die Technik passt nicht in kleinere Gehäuse (zu akzeptablen Preisen).**         
Was Apple mit dem iPhone 5 in Sachen Maße und Gewicht gemacht hat, ist erstaunlich und sucht noch seinesgleichen. Es gibt auch kein leistungsstärkeres Android Smartphone. Dabei stopfen die Android-Hersteller stets dicke Prozessoren und viel RAM in ihre Geräte. Vielleicht würde diese Technik gar nicht in einem kleineren 3,x Zoll Smartphone passen? Oder zumindest nicht mit einem ausreichend großen Akku. Oder am Ende viel teurer sein?

**Theorie 3: Größere Bildschirme sind ein gutes Verkaufsargument.**     
Wir kennen es von Digitalkameras, die Megapixel. Je mehr, desto besser. Ab einem gewissen Punkt, passte das aber nicht mehr und wo ist jetzt dieser Punkt bei Bildschirmen von Smartphones? Vielleicht hoffen die Hersteller, dass ein großer Bildschirm mit möglichst vielen Pixeln die Kunden anlockt? Wenn ich 100 Menschen das Smartphone in die Hand gebe und es für x von ihnen zu groß ist, wäre der Punkt erreicht. Oder wo ist der Unterschied zwischen 330 ppi und 440 ppi? Wenn ich keine Pixel sehe, sehe ich keine Pixel.

**Theorie 4: Das iPhone beherrscht den Markt der Highend- und 3,x-Zoll-Smartphones.**
Wie bei Theorie 1 schon erwähnt, verkauft sich das iPhone als einzelnes Gerät mit dieser Leistung und Bildschirmgröße mit Abstand am besten. Vielleicht lohnt es sich für die Hersteller nicht mehr, in diesen Markt zu gehen?

Liegt es an der Technik, Werbung, Marktanteile oder Idiotie? Oder aus einer Mischung aus allem? Ich glaube trotzdem, dass ein 3,x Zoll Android Phone eine realistische Chance hätte, wenn es die gleiche Leistung wie seine riesig-großen Flaggschiff-Brüder mitbringen würde.