---
layout: post
title: "Anmerkungen zur WWDC Keynote 2013"
description: ""
date: 2013-06-11 
category: it
tags: [Apple]
published: true
comments: false
share: true
image: 
  feature: 2013-wwdc-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Während der gestrigen WWDC habe ich mir einige Stichpunkte gemacht.

*	Willkommensvideo in schwarzweiß &gt; deutet neue Designrichtung an
*	Cook natürlich wieder über Apple Stores, als Beispiel diesmal Berlin! (mit Video)
*	Zahlen, Zahlen, Zahlen, z.b. 10 Bil. $ an Dev (3x mehr als bei allen anderen Plattformen!)
*	Live Company Launch von anki ist auch nur eine coole Demo von AI-gesteuerten Autos
*	Zwischenfrage: Erscheinen alle auf einer Keynote vorgestellten Produkte und Apps?
*	Zahlen zu Macs sogar mit Windows 8 Vergleich

*	Craig Federighi über OS X
*	Ein Witz auf einer Apple Keynote und sogar als Folienbild? 	Wow. OS X Sea Lion!
*	OS X Mavericks (California-based Versionsnamen sind okay)
* Tags für jede Art von Datei &gt; evtl. ganz nützlich
* Multiple Display wird wohl viel, viel besser! Begeisterung bei den Devs
* die Implementierung hat anscheinend doch ein paar Jahre gedauert
* diverse Optimierungen sollen 72% less CPU activity bringen &gt; wäre schön
* Safari mit vielen Verbesserungen &gt; kann ich nicht beurteilen
* TODO: Post über die Gründe, warum ich viele Google Produkte nutze schreiben
* Maps und iBooks für Mac &gt; Endlich!
* Sonst &gt; sehr humorvoll und witzig, einige Seitenhiebe gegen altes Design
* Fazit Federighi sollte öfter Keynotes halten

* Phil Schiller über Mac
* MacBook Air &gt; neues Line-Up mit neuen Haswell CPU bringt wesentlich längere Akkulaufzeit (13 Hours für 13 Zoll) 
* Freundin sagt, sie ist mir ihrem Vorjahresmodell trotzdem sehr zufrieden
* Sneak Peak auf neuen Mac Pro &gt; "ganz toll, revolutionär,..."
* Meine Meinung &gt; Schön und gut, aber für die meisten Apple-Menschen völlig unwichtig
* iWork for iCloud &gt; Konkurrent zu Office 365 und Google Docs
* Muss ich mir näher anschauen, aber finde den Ansatz ziemlich gut (vor allem weil man sich dann von Google Docs lösen könnte)

* Weiter mit iOS! Großer Applaus vom Publikum
* Zahlen blabla und völlig zurecht Kritik an Android (Fragmentierung, Nutzungsstatistiken)
* iOS 7 biggest change since iPhone mit neuem Design (hört, hört)
* Video mit John Ive &gt; sogar Applaus aus dem Publikum ist hörbar durch das Video!
* Standing Ovations nach dem Video... (okay?) 
* Wieder Craig Federighi über iOS 7
* neues Design &gt; erster Eindruck positiv
* Multitasking für alle Apps &gt; vermutlich ganz gut
* neues Control Center &gt; sehr gut
* AirDrop &gt; cool, aber leider nicht für alte iPhones :-(
* neue Camera App &gt; nice
* neue Photos App &gt; nice
* Fazit: Gespannt auf mehr

* Eddi Curr über Dienste
* Siri mit neuer Stimme und neuen Features &gt; äh... toll
* iOS in the Car &gt; praktisch, wenn man ein solches Auto hat
* Updates von Apps automatisch! &gt; Yeah!
* nicht iRadio, sondern iTunes Radio &gt; kostenlos, aber mit Werbung und only US :-(
* Craig wieder über weitere kleinere Features (Activation Lock &gt; guter Diebstahlschutz?)
* Tim Cook macht Abschluss


## Erster Eindruck

Es wurden viele kleine interessanten Dinge vorgestellt. Einige sind sinnvolle Neuerungen, andere längst überfällige. Das große Wow war für mich nicht dabei. Aber ich hatte auch keines erwartet.
Von Designern habe ich einige negative Kommentare zum neuen iOS Design gelesen. Da bin ich doch ganz froh kein Designer zu sein. 

Etwas enttäuscht bin ich von den wenigen Verbesserungen an der iCloud, allerdings könnte es auch sein, dass diese bei der Keynote nicht weiter erwähnt wurden. Hoffnung besteht also.
Ansonsten war Craig Federighi ein sehr guter Redner und wir können nur hoffen, dass er in Zukunft mehr zeigt.