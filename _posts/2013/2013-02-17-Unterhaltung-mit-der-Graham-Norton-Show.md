---
layout: post
title: "Unterhaltung mit der Graham Norton Show"
description: ""
date: 2013-02-17 
category: medien
tags: [Fernsehen]
published: true
comments: false
share: true
image: 
  feature: 2013-graham-norton-show-lg.jpg 
  credit: BBC
  creditlink: http://www.bbc.co.uk/programmes/b006xnzc
---

Ich bin über ein YouTube Video der britischen [Graham Norton Show](http://www.youtube.com/watch?v=3d-X_11jqTo) gestolpert. Ich glaube das ist ein Late Night Format, also das, was Stefan Raab versucht und Harald Schmidt mal gemacht hat. Zu Gast waren Will Smith, ein Sänger von Take That und Tom Jones. Schaut euch den Spaß mal an, es ist großartig! 

Sie führen klasse Gespräche, erzählen grandiose Anekdoten, sind ausgelassen und es wird verdammt viel gelacht. Da wurde mir bewusst, dass es nichts vergleichbares im deutschen Fernsehen gibt. Mit ausländischen *Stars* sowieso nicht. Die Hürde ist meist die Übersetzung, die den *Stars* ins Ohr gesäuselt wird. Die Moderatoren reden und fragen meist auf Deutsch oder im mittelmäßigen Englisch. Die Lösung wäre ein Gespräch auf Englisch und zwar nur auf Englisch, oder entsprechend die Muttersprache des Gasts. Viele Sendungen werden vorher aufgenommen, die kann man bis zur Ausstrahlung untertiteln. Fertig.

Aber auch Sendungen mit deutschen Gästen fühlen sich oft verkrampft an. Es gibt zwar einige Ausnahmen, doch die findet man eher auf Spartenkanälen. Und wo wird sich überhaupt 45 Minuten lang mit den Gästen unterhalten? Damit meine ich ausdrücklich nicht diese Polit-Talks, von denen wir gefühlt tausend verschiedene haben.

**Nachtrag:**      
Das beste aus Deutschland in letzter Zeit wäre wohl *Roché &amp; Böhmermann*. Eine Stunde gute Unterhaltung. *neoParadise* ist als Sendung ebenfalls grandios, aber natürlich weniger Talk als viel mehr Entertainment. 