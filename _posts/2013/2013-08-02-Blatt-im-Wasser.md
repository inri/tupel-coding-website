---
layout: post
title: "Blatt im Wasser"
description: "Ein schnelles Foto von einem Blatt im Wasser"
category: blog
tags: [iPhone, Fotografie]
comments: false
share: true
image:
  feature: 2011-herbst-blatt-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
thumb: 2011-herbst-blatt-150.jpg
---

Eines meiner absoluten Lieblingsbilder, die ich mit dem iPhone 4S geschossen habe. Am See waren die Bäume schon kahl und ihre Blätter lagen auf dem Grund des Sees. Aber nicht alle. 


Einige schwammen an der Oberfläche und eines habe ich fotografiert. Ich ahnte nicht, dass ich eine solche Tiefenunschärfe ins Bild zaubern konnte, aber es gelang. Der Filter veränderte das Bild kaum, ich mochte den Retro-Rahmen.

Leider kann ich kein Original präsentieren, da iPhoto einige Bilder aus diesem Zeitraum *vergessen* hat. Dieses hier ist die Kopie von Instagram.

<figure>
	<a href="/images/2011-herbst-blatt-lg.jpg"><img src="/images/2011-herbst-blatt-lg.jpg" alt="Blatt im Wasser"></a>
</figure>