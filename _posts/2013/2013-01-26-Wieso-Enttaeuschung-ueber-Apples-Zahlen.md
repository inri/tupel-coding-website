---
layout: post
title: "Wieso Enttäuschung über Apples zahlen?"
description: "Wieso die Enttäuschung der Anleger über Apples Quartalszahlen nicht nachvollziehbar ist."
date: 2013-01-26 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

In allen Medien finden sich nun Berichte über die einerseits rekordbrechenden Quartalszahlen von Apple und andererseits der Enttäuschung der Anleger und der Abfall des Aktienkurses um 10%. Für mich stellt sich die Frage: Hat die Anlegerreaktion noch was mit gesundem Menschenverstand zu tun, oder begreifen einfache Leute, wie ich und du, die Vorgänge nicht?

## 14 Wochen vs. 13 Wochen

[MG Siegler](http://techcrunch.com/2013/01/23/long-aapl/) hat einige Punkte dazu näher beschrieben und demnach scheinen eher die Analysten und Marktforscher ihren Job nicht besonders gut gemacht zu haben. Der Knackpunkt ist das Vorjahresquartal, welches mit 14 Wochen aus einer zusätzlichen Woche bestand, als den normalerweise üblichen 13. Dadurch waren die Vorjahreszahlen schon sehr hoch. Rechnet man eine Woche heraus, sieht das jetzige Ergebnis noch beeindruckender aus. Oder aber man addiert eine Woche in diesem Quartal hinzu und dann trifft das Ergebnis die Erwartungen der Analysten ziemlich genau.

Beachten sollte man auch die Aussagen, dass Apple Probleme beim Fertigen der iMacs hatte und sie mit Verzögerung verkauft wurden und auch gar nicht genug iPad Minis herstellen konnte.      
Florian bekam sein vor Weihnachten bestelltes Mini erst gestern. Immer noch muss man 2 Wochen Wartezeit in Kauf nehmen.

## Erwartungen und Wachstum

Einige Kommentatoren meinen auch, dass die Reaktion der Börse nicht sehr verwunderlich ist. Apple ist ein gesundes Unternehmen mit großen Umsatz und Gewinn, aber das ist für Anleger uninteressant. Eine Investition war noch lukrativer als Apple vor einigen Jahren begann rasant zu wachsen. Nun ist der Kurs sehr hoch und dem Unternehmen geht es gut, aber es gibt keine Anzeichen für eine *Zeitmaschine* von Apple, die erneut eine iPhone-ähnliche Popularität erreichen wird. Ich denke, einige Jahre wird Apple noch steigende Verkaufszahlen haben, aber der utopisch raketenhafte Wachstum ist wohl eher nicht zu erwarten. Und das finden Anleger doof.

Wie krank diese ganze Erwartungshaltung mit dem Wachstum ist, kann man sich sehr einfach vergegenwärtigen:

Wenn ich meine Einnahmen von 150€ auf 165€ steigern kann, ist das ein Wachstum von 10%. Steigere ich mich daraufhin nochmals um 15€, also auf 180€, sind das keine 10% mehr, sondern nur noch 9,09% Wachstum. Erneute 10% wären dann nämlich 16,50€ mehr, also insgesamt 181,50€. Und je größer die Summen, desto wahnsinniger werden diese Zahlen.

Aber genau auf diese irre Vorstellung von einem Unternehmen basiert unsere Marktwirtschaft. Wenn man in seinem Markt den Gewinn nicht mehr erhöhen kann, tendieren viele Firmen zur Kostensenkung, Mitarbeiterentlassung beispielsweise. Das schadet den Unternehmen, aber steigert das Wachstum, denn man hat weniger Kosten bei gleichem Gewinn. Die Anleger sind glücklich.