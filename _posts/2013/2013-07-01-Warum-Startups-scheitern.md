---
layout: post
title: "Warum Startups scheitern"
description: ""
date: 2013-07-01 
tags: [Startup]
category: it
published: true
comments: false
share: true
---

[Business Insider hat 33 Stories](http://www.businessinsider.com/33-startups-that-died-reveal-why-they-failed-2013-6?op=1) von gescheiterten Startups verlinkt und ich habe begonnen die Geschichten zu lesen. Einige Links sind schon tot, doch insgesamt habe ich die Hälfte der Text bereits *verarbeitet* und wollte damit beginnen die einzelnen Gründe für das Scheitern festzuhalten.

Einige Randinfos vorne weg: Die meisten Startups haben etwas mit Software zu tun. Das Scheitern ist meist schon einige Jahre her, es sind keine brandaktuellen Beispiele. Die Gründer sind sehr unterschiedlich. Viele haben etliche Jahre Erfahrung auf den Buckel, es sind aber auch relativ junge dabei, die direkt von der Uni kamen.

## Fazit I

Heute stelle ich das Fazit mal an den Anfang. Mich hat vor allem überrascht, dass die Ideen meist relativ gut waren und die Gründer ebenfalls fachlich überzeugen konnten. Und Geld war eher selten ein Problem. Fast alle haben verschiedene Summen von Investoren erhalten und konnten das Produkt entwickeln. Trotzdem sind sie gescheitert.       
Oder um es anders zu sagen: Konnte ich mir letzte Woche durchaus noch vorstellen, ein Startup zu gründen, so hat sich mein Selbstbewusstsein in dieser Hinsicht pulverisiert. Einige Fehler sind ziemlich offensichtlich und manchmal ahnten die Gründer schon, dass dieses und jenes nicht optimal ist. Aber dann arrangiert man sich und hofft, dass es gut gehen wird. Und genau das dachte ich bei einigen Geschichten auch.

Im folgenden die Quintessenz. Einige Fehler wiederholen sich natürlich oder tauchen in verschiedener Variation auf.

### Kein gutes Team gefunden

Die meisten Ideen kann man alleine kaum verwirklichen. Ob nun in technischer Hinsicht oder bezogen auf das Business, man braucht Menschen, die die Vision teilen und ebenfalls bereit sind ihr Herzblut in ein Projekt zu stecken. Geld ist dabei nicht immer das Problem. Klar, wer bei einer größeren Firma angestellt war, wird bei einem Startup nicht das gleiche Gehalt bekommen, aber Hungerlöhne sind es dank der Investitionen auch nicht.      
Es ist scheinbar wesentlich schwieriger Menschen zu finden, mit denen man harmoniert und mit denen man sich zutraut ein Startup zu stemmen.

Über den Job-Verteiler vom HPI kommen öfter auch Anfragen von Startups rein. Ein Projekt suchte nach einem Android Entwickler für einen Prototypen und so meldete ich mich bei den Jungs. Die Idee fand ich überhaupt nicht spannend, mich interessierte einfach die Arbeit an einem richtigen Projekt.       Zum Glück interessierten sich die Jungs auch nicht für mich. Der Grund war vermutlich, dass sie schon im Mai beginnen wollten und Geld sammelten, ich aber erst ab September verfügbar bin. Ich bin ganz froh, dass das nichts wurde, denn in ein Projekt, welches dir nichts bedeutet, wirst du nicht deine ganze Energie stecken.

### Zu viele nahe Beziehungen im Team / Projekt

Ich bleibe gleich beim Team. Man sollte möglichst wenig nahe Beziehungen im Team haben. Man kann mit seinem besten Freund ein Startup gründen oder mit der Lebensgefährtin, aber man sollte kein Startup mit allen zusammen gründen. In dem besagten Fall bestand das Team aus dem besten Freund, Mitbewohner und Freundin des Gründers. Er resümierte nach dem Scheitern, dass er 2 Monate lang nicht mit seinem Mitbewohner gesprochen hat, fast seinen besten Freund verlor und kurz vor der Trennung mit seiner Freundin stand.       
Generell sollten nahe Beziehungen keine zu große Rolle im Projekt darstellen. In einem anderen Fall waren Familie und Freunde Beta-Tester und das führte dazu, dass die Kritik nicht objektiv angenommen wurde. Die Gründer nahmen vieles persönlich und das Produkt litt am Ende darunter.

Familie und Freunde sind wichtig und es spricht nichts dagegen sie an dem Projekt teilhaben zu lassen, aber sie sollten eben nicht die einzigen Faktoren sein.

### Zielgruppe nicht einbeziehen / falsch wählen / überschätzen

Das ist vermutlich einer der häufigsten Gründe für das Scheitern und gleichzeitig ein gefährlicher, denn man ist von seiner Idee überzeugt und überträgt dieses Gefühl fälschlicherweise auch auf andere.      
Man sollte die Zielgruppe in die Entwicklung miteinbeziehen, auch wenn man denkt, dass man sie gut kennt oder selbst dazugehört. Eventuell ist die Idee tatsächlich gut, doch nur für einen selber. Erst wenn Außenstehende ebenfalls positives Feedback geben, ist die Idee womöglich nicht schlecht.
Man sollte sich generell nicht zu wenig Gedanken über die Zielgruppe machen.

In meinem Fall kämen drei verschiedene Zielgruppen zustande: Eltern, Therapeuten und Krankenkassen. Alle drei haben verschiedene Motivation und finanzielle Möglichkeiten. Im besten Fall ist das Produkt für alle gleich, aber wenn ich dann beginnen würde, um die Gunst der Therapeuten zu werben, falle ich vielleicht schnell auf die Nase. Dann hat das Marketing einen Fehlstart hingelegt und das kann finanziell schon ungemütlich werden.       

Und selbst wenn die Zielgruppe passt, könnte sie überraschenderweise kleiner ausfallen als erhofft oder benötigt. An all diese Möglichkeiten muss man bei der Planung denken.

### Kein oder ein falsches Businessmodell

Ein Unternehmen muss über kurz oder lang Geld einnehmen. Man möchte als Gründer schließlich Geld verdienen und nicht ewig von Investoren abhängig sein, welche natürlich auch nicht aus Spaß in das Unternehmen investieren.       
Eine fehlerhafte Annahme ist zum Beispiel der Glauben, dass das Geld schon kommen wird, wenn die Nutzer kommen. Eher nicht. Denn dann kann es meist schon zu spät sein. Je nach der Art der Idee kann eine schnell wachsende Anzahl Nutzer schnell zu hohen Kosten führen (Backend-Server). Plötzlich schmelzen die Investoren-Gelder, Nachschub ist nicht in Sicht und von den Nutzern kann auch kein Geld kommen.

Selbst wenn man einen Businessplan hat: Geld kann man so und so verlangen. Hilfreich und sehr empfehlenswert ist die genaue Betrachtung und Analyse des Marktes und der Konkurrenz.

### Zu viel Zeit benötigen &amp; Zu viele halbfertige Features implementieren

Diese beiden Fehler tauchen meist zusammen auf. Man muss mit dem Produkt nicht der erste auf dem Markt sein um Erfolg zu haben, doch zu viel Zeit sollte man sich auch nicht lassen. Zu hoch ist die Gefahr, dass man sich in Features verläuft und dann kommt der zweite Fehler zum Tragen. Das Produkt ist endlich fertig, hinkt der Konkurrenz aber hinterher und hat statt weniger richtig guten Features zahlreiche halb gare.

Alternativ kann man eine Beta Version veröffentlichen. So zeigt man der Welt was man hat und wohin sich das Produkt entwickelt. Als Bonus gibt es sogar Rückmeldungen von Testern. Bei dieser Art von Alternative sollte man natürlich schon Funktionen bereitstellen, also Fokussieren auf wenige und vor allem wichtige Features.

### Externer CEO / Schlechtes Board

Hat man irgendwann ein erfolgreiches Unternehmen und dieses wächst auch, wird man sich mit den nächsten Schritten der Firmenorganisation beschäftigen müssen. Ein externer CEO, der sich mit Technik nicht auskennt, ist keine besonders gute Idee. Einerseits leuchtet das ein, andererseits möchte man im Board auch externe Leute sitzen haben.      
Grundsätzlich sollte man sich jemanden suchen, der die Vision des Unternehmen versteht und unterstützt und der im Zweifelsfall auf der Seite der Gründer ist.

## Fazit II

Ein weiterer Beitrag wird folgen falls in der anderen Hälfte der Berichte weitere Fehler beschrieben werden.

Wie bereits gesagt, sind einige Fehler sicherlich sofort als solche erkennbar. Aber wenn man selbst in einer entsprechenden Situation ist, hofft man vielleicht dass es schon irgendwie funktionieren wird.      
Etwas mehr verwundern mich Fehler hinsichtlich des Business Plans. Bei meiner Beratung wurde mir sofort und deutlich erklärt, dass der Business Plan sehr wichtig ist und es ohne quasi kein Geld gibt. Trotzdem haben viele von den beschriebenen Startups kaum Probleme mit Geld gehabt. Vielleicht waren ihre Präsentationen nur sehr gut? Oder in Amerika ist man lockerer und gibt schnell mal tausende Dollar, wenn nicht sogar Millionen, für ein Startup aus?

Vor Unternehmensgründungen sollte man auf jeden Fall Respekt haben. Zumindest für die, die wissen was sie tun. Vermutlich gibt es viel zu viele, denen niemand konkret erklärt, was man alles braucht und benötigt. Ich kann es ja verraten: **Eine gute Idee alleine nützt noch gar nichts.**