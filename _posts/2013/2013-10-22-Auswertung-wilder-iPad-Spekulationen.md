---
layout: post
title: "Auswertung wilder iPad Spekulationen"
date: 2013-10-22 19:00:00
category: it
tags: [iPhone, iPad, Apple, iOS]
published: true
comments: false
share: true
---

Ich habe mitgeraten und das gar nicht mal so schlecht. 

### iPad mini 2

Kein TouchID, aber Retina und A7 Chip - 2 von 3. Beim Preis ging Apple tatsächlich auf 389€ hoch und reduziert das alte mini um großzügige 40€.    
Ich denke die Gewinne aus dem letztjährigen iPad mini waren nicht so prall, aber dieses Jahr wird die Marge bestimmt wieder stimmen. Der Kunde spart 40€ und Apple bestimmt mehr als 80€. Retina und A7 sind zwar ein tolles Update aber sicherlich keine 60€ Aufpreis (20%) in der Realität wert. Aber darum geht es ja auch nicht immer. 

### iPad Air

Wohoo! Endlich gibt es das Namensupdate! Es gab letztes Jahr schon Spekulationen, dass das iPad mini *Air* getauft wird. Nun ist es endlich so weit, zumindest beim großen.
Ansonsten war beim iPad Air alles wie erwartet. Kein TouchID und viele kleinere Upgrades - 3 von 3.
  
### iPad sonst

Kein Gold, aber das Silber und Spacegrey vom iPhone gibt es nun auch für die neuen iPads. Die 64 Bit sind also doch schon dieses Jahr dabei und die letzten nonRetina iPads werden mit Sicherheit nächstes Jahr rausgeschmissen.

Dass Apple das iPad 2 weiter im Programm hat **und** vor allem für fast den gleichen Preis wie letztes Jahr verkauft (379€ und für 10€ mehr gibt es ein technisch weit überlegend iPad mini 2!), ist schon seltsam. Es mit den iPad 4 zu ersetzen war vermutlich nicht lukrativ genug, siehe auch mein Kommentar von oben. 

### Sonst

* Kein neuen Monitore. Schade.
* MacBook Pro Updates, die komischerweise um 200$ im Preis gesunken sind. 
* Der neue Mac Pro für immerhin stolze 3000$.
* Neue Apps wohin man schaut! iLife und iWork bekamen eine komplette Überarbeitung, für iOS und Mac. Und sind kostenlos, zumindest beim Kauf neuer Geräte.
* Was Apple zukünftig bei den iPads als höhere Margen erzielt, geben sie indirekt an die Kunden weiter oder so - OS X Mavericks ist kostenlos! 

### Fazit

Die größte Überraschung gab es sicherlich in Sachen Software, vor allem auch wegen der Preispolitik.   
Am Kopf kratze ich mich bei der Preiserhöhung des iPad minis. Insgesamt sind es aber durchweg gute Updates der bestehenden Produkte.  




