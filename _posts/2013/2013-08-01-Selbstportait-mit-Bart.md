---
layout: post
title: "Selbstportrait mit Bart"
description: "Ein einfaches Foto im Flur geschossen."
category: blog
tags: [iPhone, Persönlich, Fotografie]
comments: false
share: true
image:
  feature: 2012-selfportrait-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
thumb: 2012-selfportrait-200.jpg
---

Vor etwas über einem Jahr beendete ich mein Bachelorprojekt und daher stand auch das Bachelorpodium an. In dessen Vorbereitung probierte ich meinen Anzug und schoss bei der Gelegenheit gleich ein paar Fotos von mir selbst.  


Wir haben im Flur einen Spiegel mit metallenem Rahmen hängen und an diesem befestigte ich ein magnetischen Gorillapod mit dem iPhone 4S drauf. 

<figure>
	<a href="/images/2012-selfportrait-1200.jpg">
	<img 
		src="/images/2012-selfportrait-800.jpg"
		alt="Selbstportrait mit Bart"></a>
</figure>

Ich freute mich so sehr über das Ergebnis, dass ich dieses Bild ein Jahr lang als Profilbild verwendete. Unten das Original, ungeschnitten und ohne Filter.

<figure>
	<a href="/images/2012-selfportrait-original-1600.jpg">
	<img 
		src="/images/2012-selfportrait-original-800.jpg"
		alt="Originalfoto ohne Beschneidung und Filter"></a>
	<figcaption>Beim Original ist auch die weiße Wand besser zu erkennen.</figcaption>
</figure>

<figure>
	<a href="/images/2012-selfportrait-smiling-1600.jpg"><img src="/images/2012-selfportrait-smiling-800.jpg" alt="Eine freundlichere Version des Bildes mit Lächeln"></a>
	<figcaption>Quasi das Gegenstück mit Lächeln, aber auch geschnitten und etwas anders gefiltert.</figcaption>
</figure>