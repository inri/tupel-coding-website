---
layout: post
title: "The Walking Dead, Staffel 4 bis Episode 3"
date: 2013-10-28 20:00:00
category: medien
tags: [Serie, The Walking Dead]
published: true
comments: false
share: true
---

Die Untoten und zurück! Bevor ich meine Gedanken zum Staffelauftakt kundtue, sind zuerst einige Dinge klarzustellen.    
Ich mag die Serie *The Walking Dead* sehr. Ist sie meine Lieblingsserie? Nein. Sie ist aber auf jeden Fall in meiner persönlichen Top 5. 

Allgemein gesagt ist die Geschichte um TWD einfach sehr gut erzählt. Egal ob das Zombie-Setting, die Charaktere, die Schicksale, die Tragödien,... alles passt zusammen. Das trifft selbstverständlich auch auf die Serie zu und noch dazu schafft sie eine unglaublich hohe Qualität. Die cineastischen Auswüchse des Zombie-Themas können auch sehr trashig und billig wirken. Das ist bei TWD definitiv nicht der Fall [^1].

### Das Gefängnis als Heimat

Hat man sich am Ende der dritten Staffel noch gefragt, ob die Gruppe das Gefängnis verlassen wird, war mit der Ankunft der Woodbury-Bewohner die Stoßrichtung klar. Nicht zuletzt ist das Gefängnis vermutlich auch ein viel zu schönes Set um es nur für eine Staffel zu nutzen. Mittlerweile ist die Gemeinde sogar weiter gewachsen und es werden Überlebende aufgenommen.

Ricks mentaler Zustand hat sich beruhigt, aber schockiert von seinen und vor allem Carls Taten, degradierte er sich selbst und Sohnemann zu Gefängnisfarmern. Die Ricktatorship ist vorbei und nun berät sich ein Council bestehend u.a. aus Hershel, Daryl und Carol. Alle wollen Rick zurück, aber er möchte sich abgrenzen und keine Verantwortung tragen.

### Die neue Gefahr

Die Runs nach Proviant und anderen nützliche Dingen, laufen mittlerweile ziemlich professionell ab, auch wenn es durch besondere Umstände mal etwas knapp wird (Stichwort: durchs Dach fallende Zombies im Supermarkt). Zombies sind an und für sich keine große Gefahr mehr, die Gruppe kann sehr gut mit ihnen umgehen.

Der Governor ist noch irgendwo da draußen und Michonne ist auch auf der Suche nach ihm, aber zumindest in den ersten Episoden scheint der Fokus auf eine Gefahr im Inneren des Gefängnisses zu liegen. Die Bewohner befällt eine Art Grippe, die für sich genommen nicht tödlich ist, aber deren Krankheitsverlauf die Personen so schwächen kann, dass der Zombie-Virus (mit dem alle Menschen infiziert sind [^2]) ausbricht. Kurz gesagt: Eine neue Gefahr, die die Ordnung im Gefängnis durcheinander bringt.

### Fazit

Und genau das erwartet uns in dieser Staffel. Wie gehen unsere geliebte Charaktere mit der neuen Art von Gefahr um? Es müssen schwierige Entscheidungen getroffen werden und die daraus resultierende Dynamik ist packend. Drei Episoden sind schon über die Bildschirme geflimmert und jede Minute war spannend. 
   
Neben Fragen der charakterlichen Entwicklung gibt es auch handfeste Probleme, wie zum Beispiel jemanden, der die Zombies außerhalb des Zaunes mit Ratten füttert und dadurch anlockt. Ein Maulwurf?  

*The Walking Dead* ist noch immer *The Walking Dead*. Gute Geschichte und viel Spannung. Bitte weiterschauen.

[^1]: Als Negativbeispiel sei *Once Upon A Time* genannt. Das Setting ist in der Märchenwelt angesiedelt. Daher werden gerne digitale Hilfsmittel genutzt und das Ergebnis sieht einfach nur schlecht aus. So richtig schlecht. 

[^2]: Siehe Staffel 1, letzte Episode. Rick erfährt vom Forscher dass alle Menschen infiziert sind, aber man sich eben erst nach dem Ableben verwandelt.



