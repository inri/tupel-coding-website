---
layout: post
title: "MDSE, DSL &amp; Checks"
description: ""
date: 2013-02-12
category: it
tags: [HPI, Studium]
published: true
comments: false
share: true
---

Im ersten Teil habe ich unser Metamodell vorgestellt und nun zeige ich euch, wie unsere domainspezifische Sprache aussieht und die Checks.

## Wofür ist die DSL eigentlich gut?

Eine DSL erstellt man, einfach gesagt, um das Programmieren zu erleichtern. Im Fokus einer DSL steht meist ein Benutzer, der in der Domain irgendwas machen muss und dafür die domainspezifische Sprache nutzt. In unserem Beispiel haben wir ein automatisches Haussystem und der Nutzer kann beispielsweise ein Hausbesitzer sein. Dieser trägt zum einen die Sensoren und Aktuatoren in seinem Haus ein und beschreibt Regeln, wie diese Elemente aufeinander reagieren sollen. Da der Kunde für das System nicht erst Programmieren lernen soll, geben wir ihm eine Sprache zur Hilfe, mit der er diese Dinge möglichst einfach beschreiben kann.

**Beispiel Sensoren und Aktuatoren:**

	Rooms ( 
	  Roomname: Room1 ( 
	    Actuator-List ( 
	      Lamp: Lamp1 )
	    Sensor-List ( 
	      SimpleSwitch: Switch1,
	      Clock: WallClock,
	      TwilightSwitch: Twilight1 )
	  )
	)

und eine Regel: 

	Rule: LampOff ( 
	  If ( 
	    And-Part: ( 
	      Switch1Off and ClockLate )
	  )
	  Then ( 
	    Lamp1Off )
	)

Um die DSL zu definieren haben wir *Xtext* benutzt.

Die blauen Wörter und Klammern sind quasi die einleitenden Wörter und je nachdem was danach verlangt ist, beschreibt der Benutzer dann z.b. ein Sensor mit *TempSensor* gefolgt von einem Namen, der als String aufgenommen wird.

## Wenn der Benutzer Fehler macht

Genau dann kommen die Checks ins Spiel. Sie überprüfen meist schon bei der Eingabe, ob sie funktioniert oder nicht. Die DSL ist schon relativ automatisch und die Autovervollständigung gibt die nächsten einleitenden Wörter vor und so weiß der Benutzer was er schreiben muss. Auch wenn eine Klammer vergessen wird, erscheint sofort ein aufforderndes Rot. 

Bei uns gibt es z.b. die leider umständliche Wahl zwischen *=* und *==* je nachdem ob wir eine Zuweisung haben oder einen Vergleich. Doch genau das testen die Checks und stellen sicher, dass keine oder zumindest so wenig Fehler wie möglich entstehen können.