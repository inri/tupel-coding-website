---
layout: post
title: "Entscheidung in TWD"
date: 2013-11-05 23:30:00
category: medien
tags: [Serie, The Walking Dead]
published: true
comments: false
share: true
---

Hier folgt eine Diskussion über eine Entscheidung in der 4. Episode der 4. Staffel *The Walking Dead*. Wer sie noch nicht gesehen hat, **Spoiler voraus!**

Was möchte ich diskutieren? Die beiden infizierten, aber noch lebenden Personen wurden von Carol in ihren Quarantänezellen getötet und danach verbrannt (Ende Episode 2). In der darauffolgenden Episode hat sie ihre Tat gegenüber Rick zugegeben, er reagierte aber zunächst nicht.    

Tyreese hat den brutalen Tod seiner Freundin noch nicht verarbeitet und trägt eine große Portion Wut mit sich herum. Aktuell ist er noch unterwegs, doch wenn er ins Gefängnis zurückkehrt, wird er sich mit Sicherheit den Täter vorknöpfen.    Und wenn bis dahin keiner gefunden ist, weiß Gott was passiert.    
Lange Rede kurzer Sinn: Die Konfrontation zwischen Rick und Carol war unausweichlich.

### Carol White

Beide begeben sich auf Nahrungssuche in einer verlassenen Ortschaft. Das war natürlich Ricks Idee, denn er wollte sie wieder verstehen können. Carol ist mittlerweile ein anderer Mensch, nicht mehr die eingeschüchterte Frau unter der Knute eines prügelnden Mannes. Sie hat eine Walter-White-ähnliche Entwicklung durchgemacht, ist emotional immer kühler geworden.    
Zuerst verlor sie ihren Mann, danach ihre Tochter und später immer mal wieder liebgewonnene Gruppenmitglieder. 

Sie hat einen starken Überlebenswillen und will um jeden Preis die Gruppe beschützen. Dafür trifft sie auch schwere Entscheidungen, wie eben zwei Menschen zu töten ohne sich mit irgendjemanden aus der Gruppe abzusprechen.

### Verbannung

Beim Durchsuchen eines Hauses reden Rick und Carol mehr oder weniger über die Tat. Sie erwartet von Rick, dass er es akzeptiert, verstehen muss er es nicht. Ihrer Ansicht nach ist Rick schwach geworden, geht schwierigen Entscheidungen aus dem Weg.    
Sie treffen ein junges Paar und beschließen sie mit zum Gefängnis zu nehmen. Die beiden drängen darauf beim Sammeln von Lebensmitteln in der Umgebung zu helfen und so gibt Rick dem jungen Mann seine Uhr und sie verabreden sich in 2 Stunden am Auto. 

Doch dazu kommt es nicht, denn Carol und Rick finden später die Überreste der jungen Frau und vom Mann keine Spur. Trotzdem warten sie, vor allem weil Rick dazu drängt. Carol wiederum wird ungeduldig und meint zu Rick, dass es wirklich eine schöne Uhr war, er sie aber nicht wiederbekommen wird.    
Ich denke diese emotionslose Sicht der Situation hat bei Rick den Ausschlag gegeben. 

Rick verbannt sie. Carol soll nicht zurück zum Gefängnis mitkommen. Sie wehrt sich nicht, akzeptiert seine Entscheidung. Er vertraut ihr nicht mehr, hat vielleicht sogar Angst.    
Zusammen packen sie ihr ein Auto voll und dann begleitet die Kamera Rick auf der Rückfahrt. Immer wieder dreht er sich im Rückspiegel um, aber es folgt niemand.

### Kontra

Was spricht gegen Ricks Entscheidung?

* Er ist nicht mehr der Anführer der Gruppe, dürfte solche Entscheidungen deshalb gar nicht alleine treffen.
* Carol ist ein wichtiges Mitglied der Gruppe. Sie ist erfahren, bringt den Kindern das Kämpfen bei und hat vermutlich viele andere Fähigkeiten.
* Ihre Tat war richtig und notwendig. Die beiden waren schließlich infiziert und hätte man sie eher getötet wäre jetzt vielleicht nicht das ganze Gefängnis infiziert.

### Pro

Was spricht für seine Entscheidung?

* Die Situation mit Tyreese ist völlig offen. Selbst wenn Rick es ihm nicht sagt, wird Tyreese nicht ruhen bis der Täter gefunden wird. Und wenn er es herausfindet oder sie es ihm sagt, wird er es nicht akzeptieren können.
* Carol kann man nicht mehr vertrauen. Ob es eventuell richtig war diese Bedrohung für die Gruppe zu eliminieren, sei mal dahingestellt. Aber ohne jegliche Absprache mit den anderen (aus dem *Gefängnis-Rat*) diese Entscheidung zu treffen, war ein großer Fehler.
* Eventuell hat Carol selbst geahnt, dass ihre Tat nicht verzeihlich ist. Beim Einpacken des Auto hat Rick ein mehrteiliges Messerset in ihren Sachen entdeckt. So etwas nimmt man nicht auf einer Tour mit.

### Fazit

Obwohl Tyreese eine ganz gute Begründung wäre, war das sicherlich nicht Ricks Hauptgrund. Er erkennt Carol einfach nicht mehr und kann ihr nicht mehr vertrauen. 
  
Sie meint zu ihm, dass ihre Tat nicht anders war als Shane zu töten. Oder beispielsweise auch wie Carl den Jungen erschießt. Aber beides sind in meinen Augen andere Situationen. Shane wollte Rick töten, es war nichts anderes als Notwehr. Ähnlich war es auch bei Carl. Der Junge war vielleicht in dieser Sekunde keine Bedrohung, aber es fand ein Angriff der Governor-Leute statt. Wer weiß was der Junge getan oder gerufen hätte? Und nicht zuletzt ist Carl selbst noch ein Junge. 

Ich war mir zu Beginn der Episode nicht mal sicher, ob Rick sie nicht sogar töten wird. Sie nur zu verbannen war zumindest für die Serie die bessere Idee, denn wer weiß wann man sie wieder trifft. Sie wird sicherlich außerhalb des Gefängnis überleben. 

An dieser Stelle möchte ich betonen wie interessant diese Episode war. Insgesamt auch die Heranführung zu dieser Situation mit Carol wurde über die letzten Episoden richtig gut inszeniert.







 


