---
layout: post
title: "6 Wahrheiten die dich besser machen"
date: 2013-08-17 09:00:00
category: blog
tags: [Tips, Wirtschaft, Philosophie]
published: true
comments: false
share: true
---

Mir ist ein äußerst interessanter Artikel unter die Finger gekommen. Die lange Version des Titels lautet &quot;*6 harte Wahrheiten, die dich zu einem besseren Menschen machen*&quot; und ja, er hört sich etwas provozierend an. 


Anfangs wollte ich nur einige Gedanken zum Inhalt formulieren, doch nun gibt es den gesamten Artikel von mir übersetzt. Natürlich ist er im Original besser, aber nicht alle meine Leser lesen gerne lange englische Texte.  

Trotzdem hier der Link zum Original: <a href="http://www.cracked.com/blog/6-harsh-truths-that-will-make-you-better-person/" target="_blank">6 Harsh Truths That Will Make You a Better Person</a>
 
Ich gebe mir Mühe den Artikel so gut wie möglich zu übersetzen. Kleine Ungenauigkeiten mögen mir trotzdem verziehen sein. Ich schreibe es noch einmal deutlich: **Der folgende Text ist eine Übersetzung und nicht meine Schöpfung!**

In einigen Tagen gibt es noch einige Worte von mir über den Text, aber nun viel Spaß beim Lesen.


#### 2013, Motherfuckers. Yeah! Los geht's.

&quot;*Was geht los?*&quot; fragst du. Ich weiß es nicht. Kommt, lasst es uns gemeinsam herausfinden.

Du musst das folgende nicht lesen, wenn deine Karriere gut läuft, du mit deinem Leben zufrieden und einfach glücklich mit deinen Beziehen bist. Mach dir einen schönen Tag mein Freund, dieser Artikel ist nicht für dich. Du bist super, wir sind alle stolz auf dich.  

Für den Rest von euch habe ich folgende Aufgabe: Nennt 5 eindrucksvolle Dinge über euch selbst. Schreibt sie auf oder schreit sie einfach heraus. Aber hier ist die Bedingung: Du darfst nichts darüber auflisten wie du bist (z.b. Ich bin nett, ich bin ehrlich). Stattdessen sollst du nur Dinge nennen die du tust (z.b. Ich habe eine Schachmeisterschaft gewonnen, Ich koche das beste Chili). Wenn du diese Aufgabe schwierig findest, ist der folgende Text was für dich und du wirst es nicht gerne hören. Zu meiner Verteidigung kann ich nur sagen dass es Wahrheiten sind, die **mir** vor 10 Jahren jemand hätte sagen sollen. 

### 6. Die Welt interessiert sich nur dafür was sie von dir bekommen kann.

Stellt euch folgendes vor: Der Mensch den ihr am meisten liebt wurde soeben angeschossen. Er oder sie liegt blutend und schreiend auf der Straße und ihr steht daneben. Da kommt ein Kerl angelaufen und sagt &quot;*Geht zu Seite*&quot;. Er schaut sich die Wunde an und holt ein Taschenmesser heraus - er möchte gleich hier auf der Straße eine Operation durchführen. &quot;*Okay, wer ist hier verletzt?*&quot; fragt er.

Und du fragst zurück &quot;*Sind Sie ein Arzt?*&quot; worauf er antwortet &quot;*Nein*&quot;. Du erwiderst irritiert &quot;*Aber Sie wissen schon was Sie hier machen? Sie waren Sanitäter bei der Armee, oder…*&quot; 

Nun ist der Kerl genervt und sagt dir, dass er ein netter Typ ist, ehrlich und immer pünktlich. Er erzählt dir, dass er ein klasse Sohn ist, viel tolle Hobbys hat und fast nie flucht.
Du bist noch verwirrter &quot;*Was zur Hölle hat das für eine Bedeutung wenn mein geliebter Mensch hier liegt und verblutet? Ich brauche jemanden der weiß wie man Schusswunden behandelt! Kannst du das oder nicht?!*&quot;

Nun regt sich der Kerl langsam auf. Wieso bist du auch so oberflächlich und selbstsüchtig? Interessieren dich die guten Qualitäten der anderen überhaupt nicht? Hast du ihn nicht gerade sagen hören, dass er nie den Geburtstag seiner Freundin vergisst? In Anbetracht all der tollen Dinge die er tut, ist es dann wirklich noch wichtig, ob er weiß wie man eine Operation durchführt? 

In diesem Moment packt dich die nackte Angst, du fasst ihn an den Schultern und brüllst &quot;*Ja, ich sage dass dieser ganze andere Scheiß mir völlig egal ist, denn in dieser Situation hier brauche ich nur jemand der weiß wie man eine Blutung stoppt, du verrücktes Arschloch!*&quot;

Hier ist meine brutale Wahrheit über die Erwachsenen-Welt: Du bist an jeden Tag in dieser Situation. Nur bist du der verwirrte Typ mit dem Taschenmesser und die gesamte Gesellschaft ist das blutende Schussopfer.  
Wenn du dich fragst wieso die Gesellschaft dich meidet oder dich nicht zu respektieren scheint, es ist weil die Gesellschaft voll mit Menschen ist, die Dinge brauchen. Sie brauchen Essen, sie brauchen Häuser, sie brauchen Unterhaltung und sie brauchen erfüllende sexuelle Beziehungen. Du kommst zum Unfallgeschehen mit deinem Messer in der Hand, weil du geboren bist - genau ab diesem Moment bist du ein Teil des Systems geworden, welches nur dafür geschaffen wurde die Bedürfnisse der anderen zu sehen.

&quot;*Hier ist der Scheiß den du wolltest, nun hau' endlich ab!*&quot; Entweder nimmst du die Aufgabe an, siehst was gebraucht wird und lernst nützliche Fähigkeiten oder die Welt wird dich verstoßen, völlig egal wie nett, großzügig oder höflich du bist. Du wirst arm und allein sein, da draußen in der Kälte.
Hört sich das gemein oder krass oder materialistisch an? Was ist mit Liebe und Freundlichkeit, bedeuten sie gar nichts? Doch natürlich. So lange sie dazu führen dass du Dinge für Leute machst, die sie nicht von woanders bekommen können. 

### 5. Die Hippies lagen falsch.

<a href="http://www.youtube.com/watch?v=8kZg_ALxEz0" target="_blank">Das ist die großartigste Szene in der Geschichte des Films. (Youtube)</a>

Wer das Video nicht anschauen kann, das ist die berühmte Rede von Alec Baldwin im filmischen Meisterwerk *Glengarry Glenn Ross*. Der Charakter von Baldwin, der auch der Bösewicht ist, hält vor einigen Kerlen eine Ansprache und reißt ihnen förmlich den Hintern auf, in dem er ihnen sagt, sie sind gefeuert wenn sie die Aufträge nicht zum Abschluss bringen. Das klingt dann so &quot;*Ein netter Kerl? Ist mir scheißegal. Ein guter Vater? Fick dich! Geh nach Hause und spiele mit deinen Kindern. Wenn du hier arbeiten willst, verkaufe.*&quot;

Es ist brutal, unhöflich und Borderline-soziopathisch, und es ist ein ehrlicher und exakter Ausdruck von dem was die Welt von dir erwartet. Der Unterschied ist nur: In der echten Welt denken die Menschen dass es falsch ist auf diese Weise mit dir zu reden und deshalb haben sie sich dafür entschieden, dich einfach fallen zu lassen. 

Diese eine Szene hat mein Leben verändert. Ich würde meinen Wecker so programmieren, dass er mir jeden morgen diese Szene vorspielt, wenn ich wüsste wie das geht. Alec Baldwin wurde für diesen Film mit einem Oskar nominiert und dabei ist das die einzige Szene, in der er auftaucht. Klügere Menschen haben herausgefunden, dass das geniale an dieser Szene die Reaktion der Menschen ist. Die eine Hälfte denkt nur darüber nach was für ein Arschloch er als Chef ist und die andere Hälfte würde am liebsten sofort losgehen und verkaufen. Der Punkt ist, dass der Unterschied der beiden Einstellungen - verbittert vs. motiviert - größtenteils entscheidend dafür ist, ob du erfolgreich bist. 

Zum Beispiel möchten einige Leute gerne mit der Rede von Tyler Durden aus Fight Club antworten: &quot;*Du bist nicht dein Job.*&quot;   
Allerdings bist du genau das. Einverstanden, dein *Job* und deine Vorstellung von Anstellung mag nicht das gleiche sein, aber in beiden Fällen bist du nichts weiter als die Summe deiner nützlichen Fähigkeiten. Eine gute Mutter zu sein ist ein Job der Fähigkeiten benötigt. Es ist immer etwas nützliches, dass eine Person für die Gesellschaft tun kann. Aber gehe nicht von der falschen Annahme aus: Denn dein *Job* - alle nützlichen Dinge die du für andere tun kannst - ist genau alles was du bist.

Es gibt einen Grund dafür warum Chirurgen mehr respektiert werden als Comedians. Es gibt einen Grund dafür warum Mechaniker mehr respektiert werden als arbeitslose Hipster. Es gibt einen Grund dafür wieso dein Job zu deinem Label wird wenn dein Tod es in die Nachrichten schafft (&quot;*Der Bundesliga Torschütze starb in einem Unfall...*&quot;).  
Tyler Durden sagte &quot;*Du bist nicht dein Job*&quot; aber er gründete auch ein äußerst erfolgreiches Unternehmen für Seife und wurde der Kopf einer internationalen, sozial-politischen Bewegung. Er war vollständig sein Job. Viele Menschen haben die Ironie dieses Films nicht verstanden.  

Oder denkt über dieses Beispiel nach: Als die Sandwich-Kette Chick-fil-A gegen die Ehe zwischen Homosexuellen mobilisierte, gab es zwar Proteste, aber sie haben trotzdem weiterhin Millionen von Sandwiches verkauft. Nicht weil die Leute ihrer Meinung waren, sondern weil sie gut in ihrem Job, leckere Sandwiches zu machen, waren. Und nur das zählt.

Du musst das alles nicht mögen. Ich mag es auch nicht wenn es an meinem Geburtstag regnet. Es regnet trotzdem. Menschen haben Bedürfnisse und belohnen diejenigen, die diese erfüllen können. Das sind einfachste Mechanismen des Universums und sie sind unabhängig von unseren Wünschen.

&quot;*Das ist doch totaler Quatsch. Ich habe ein sauberes polizeiliches Führungszeugnis und das ist alles was ich bekomme?*&quot; Wenn du protestierst, weil du kein gieriger Kapitalist bist und Geld nicht alles sei, kann ich dir nur erwidern: Wer hat was von Geld gesagt? Du begreifst das große Ganze nicht. 

### 4. Was auch immer du produzierst, es muss kein Geld erwirtschaften, aber es muss einen Nutzen für Menschen haben.

Versuchen wir es mit einem nicht-Geld Beispiel, damit du nicht zu sehr daran aufhängst. In meiner Arbeit als Redakteur lese ich viele Leserbriefe von alleinstehenden Männern Mitte 20. Sie jammern meistens darüber, dass sich keine Mädchen für sie interessieren, obwohl sie doch die nettesten Typen der Welt sind. Ich kann dir sagen was an diesen Gedanken fehlerhaft ist, aber es wäre wohl besser, wenn ich es Alec Baldwin erklären lasse.

In diesem Fall spielt Baldwin die Rolle der attraktiven Frau in deinem Leben. Sie würde es zwar nicht so direkt sagen, aber es würde wieder ungefähr lauten: &quot;*Netter Typ? Wen interessiert das? Wenn du hier arbeiten willst, verkaufe.*&quot; Also was bringst du mit? Weil das süße Mädchen von dem du träumst, die aus dem Buchladen, die aussieht wie Zooey Deschanel, sie braucht jeden Abend eine Stunde um sich schick zu machen und fühlt sich schlecht, wenn sie mehr als einen Salat zum Mittag ist. Sie wird in 10 Jahren Chirurgin sein. Was machst du?

&quot;*Was? Du meinst ich kann solche Frauen nicht bekommen so lange ich keinen guten Job habe und viel Geld verdiene?*&quot; Nein, dein Gehirn zieht diesen Schluss damit du stets eine Entschuldigung hast, wenn dich jemand abweist (in dem du denkst die anderen sind oberflächlich und selbstsüchtig).  
Ich frage dich: Was kannst du bieten? Bist du klug? Humorvoll? Interessant? Talentiert? Ambitioniert? Kreativ? Ok, also was machst du nun konkret um der Welt diese Eigenschaften zu demonstrieren? Sag nicht dass du ein netter Typ ist, das ist das Minimum. Hübsche Mädchen treffen 36 mal am Tag Kerle, die nett zu ihnen sind. Denk daran: Der Patient liegt auf der Straße und verblutet. Weißt du wie man operiert oder nicht?

&quot;*Naja, ich bin nicht sexistisch, rassistisch, gierig, oberflächlich oder beleidigend! Ich bin nicht wie die anderen Deppen!*&quot;  
Es tut mir Leid, ich weiß dass es hart ist folgendes zu hören, aber wenn du nur Dinge auflisten kannst, die nichts weiter sind als Fehler, die du nicht hast, dann geh weg vom Patienten. Es wird einen charmanten und witzigen Typen mit einer erfolgreichen Karriere geben, der bereit ist deinen Platz einzunehmen und die Operation durchzuführen. 

Bricht dir das dein Herz? Ok, also was nun? Wirst du Trübsal blasen oder wirst du lernen wie man operiert? Es liegt allein an dir, aber beschwere dich nicht dass Frauen auf Idioten stehen. Sie stehen auf diese Idioten weil diese Idioten andere Dinge haben, die sie anbieten können.  
Du sagst, dass du ein guter Zuhörer bist? Weißt du was, es gibt mit Sicherheit einen Typen im Leben jeder Frau, der gut zuhören kann und der noch dazu Gitarre spielt. Zu sagen, man sei ein netter Kerl, ist ungefähr so wie ein Restaurant mit dem Werbespruch &quot;*Wir bieten Speisen, die Sie nicht krank machen&quot;*. Oder ein Film dessen Titel &quot;*Dieser Film ist auf Deutsch*&quot; lautet und den Untertitel &quot;*Die Schauspieler sind gut zu erkennen*&quot; hat. 

Ich denke das ist der Grund warum du ein netter Kerl bist und trotzdem unzufrieden mit dir selbst. Genauer gesagt…

### 3. Du hasst dich selbst weil du nichts tust.

&quot;*Also soll ich ein Buch lesen das mir erklärt wie man Frauen aufreißt?*&quot;  
Nur wenn der erste Schritt sagt &quot;*Beginne damit der Typ zu werden in dessen Nähe Frauen sein wollen&quot;*. Das ist immer der Schritt der übersprungen wird. Ständig heißt es &quot;*Wie bekomme ich einen Job?*&quot; und nicht &quot;*Wie werde ich der Typ nach dem ein Unternehmen sucht?&quot;*. Oder eben &quot;*Wie bekomme ich hübsche Mädchen dazu mich zu mögen?*&quot; anstatt &quot;*Wie werde ich der Typ auf den hübsche Mädchen stehen?*&quot;. Die zweite der beiden Fragen kann sehr gut bedeuten, dass du einige deiner Hobbys aufgeben musst, mehr auf dein Äußeres achten und Gott was weiß ich alles. Vielleicht musst du sogar deine Persönlichkeit ändern.

Du fragst &quot;*Wieso kann ich niemanden finden der mich so mag wie ich bin?*&quot; - Die Antwort lautet: Weil Menschen Dinge brauchen. Das Opfer verblutet und du hast nichts besseres zu tun als dich zu beschweren, dass es nicht mehr Schusswunden gibt die sich von selbst heilen? <a href="http://www.youtube.com/watch?v=mSnRq6iyHKg" target="_blank">Hier ist ein weiteres Video</a>

Jeder der dieses Video anschaut, wird sofort etwas glücklicher, wenn auch nicht alle aus demselben Grund. Kannst du so etwas für Menschen machen? Warum nicht? Was hält dich davon ab dir einen Tanga und Cape anzuziehen, dich auf eine Bühne zu stellen und mit deinen Penis hin und her zu wedeln? Dieser Kerl kennt das Geheimnis wie man Menschen für sich gewinnt: Einfach etwas machen… was auch immer man darunter versteht. Etwas zu machen ist besser als etwas nicht zu machen. 

&quot;*Aber ich bin in nichts besonders talentiert.*&quot; Okay, ich habe gute Nachrichten für dich - investiere viele Stunden in etwas und du kannst praktisch gut in allem werden. Ich war damals der schlechteste Autor auf der ganzen Welt und war Jahre später, mit 25 Jahren, nur ein Stückchen besser. Aber während ich jämmerlich mit meiner Karriere scheiterte, schrieb ich in meiner Freizeit. Ein Artikel pro Woche. Es dauerte acht Jahre bis ich damit Geld verdiente und 13 Jahre um gut genug zu werden um es in die New York Times Bestseller Liste zu schaffen. Es hat mich wahrscheinlich 20.0000 Stunden an Üben gekostet.

Du magst es nicht, deine gesamte Zeit in eine Fähigkeit zu stecken? Ich habe gute und schlechte Neuigkeiten für dich. Die guten sind, dass das bloße Ausüben des Training dir dabei helfen wird dein Schneckenhaus zu verlassen. Die meisten hören schnell auf weil es zu lange dauert bis Resultate zu sehen sind. Sie verstehen nicht, dass der Prozess das Resultat ist.  
Die schlechte Neuigkeit ist: Du hast keine Wahl. Wenn du hier arbeiten willst, verkaufe. Ich denke du hasst dich selbst, nicht weil du ein geringes Selbstwertgefühl hast oder weil andere gemein zu dir sind. Du hasst dich selbst, weil du nichts tust. Deshalb kannst du dich nicht mal selbst lieben - und schickst **mir** private Nachrichten und fragst was du mit deinen Leben tun sollst. 

Schritt 1: Steh auf.  
Rechne ein wenig: Wie viel Zeit verbringst du damit Dinge zu konsumieren, die andere gemacht haben (TV, Musik, Games, Websites) und wie viel Zeit steckst du in eigens? Nur eines von beiden erhöht deinen Wert als Mensch. Und wenn du nicht magst was du liest und mir mit einer Weisheit deiner Kindertage kontern willst, wie z.b. &quot;*Die inneren Werte zählen!*&quot;, dann kann ich nur sagen…

### 2. Die inneren Werte sind nur wichtig weil sie dich etwas tun lassen.

In meiner Branche kenne ich dutzende ehrgeizige Autoren. Sie halten sich selbst für Schriftsteller, sie stellen sich auf Parties als Schriftsteller vor und sie wissen, tief in ihrem Inneren haben sie das Herz eines Schriftstellers. Sie vergessen nur den letzten kleinen Schritt: Überhaupt erst einmal **etwas zu schreiben**. Aber ist das wirklich wichtig? Ist das Schreiben von Dingen so wichtig wenn man entscheiden soll, wer ein wahrer Schriftsteller ist und wer nicht? 

Na selbstverständlich!

Ich kenne *Schriftsteller* die weniger Wörter geschrieben haben als sich auf einer Einkaufsliste finden lassen. Aber es gibt eine allgemeine Abwehr gegen alles, was ich bisher gesagt habe und gegen jede kritische Stimme in deinem Leben. Es ist das, was dein Ego zu dir sagt um dich davor zu bewahren zu viel Energie in deine eigene Verbesserung zu investieren: &quot;*Ich weiß dass ich im Inneren ein guter Mensch bin.*&quot; Es könnte auch &quot;*Ich weiß wer ich bin*&quot; oder &quot;*Ich muss nur ich selbst sein*&quot; lauten.  
Verstehe mich nicht falsch, du bist all das was du auch im Inneren bist - der Typ der ein Haus für seine Familie gebaut hat, konnte das, weil er das im Inneren ist. Alles schlechte was du getan hast, begann mit einem Impuls in deinem Kopf. Und alles gute ebenso. &quot;*Wer du im Inneren bist*&quot; ist die metaphorische Erde aus der deine Früchte wachsen. 

Hast du bemerkt, dass die Kamera immer nach oben zeigt und nie auf die Wurzeln des Baums? Aber genau das muss jeder wissen und viele können es nicht akzeptieren: 

**Du** bist nichts weiter als die Frucht.    

Niemanden interessiert die Erde. &quot;*Was du im Inneren bist*&quot; ist bedeutungslos abgesehen davon was es für andere Menschen schafft.  
Du hast Mitleid mit den Armen in der Gesellschaft? Großartig. Resultiert das in Taten für sie? Wenn du von einer furchtbaren Tragödie in der Nachbarschaft hörst, denkst du dann auch &quot;*Diese armen Kinder, ich lasse sie wissen dass ich an sie denke.*&quot;? Denn wenn ja, fick dich! Finde stattdessen heraus was sie brauchen und gib es ihnen. Hundert Millionen Menschen haben das Kony Video geschaut und quasi alle haben an die armen afrikanischen Kinder gedacht. Was haben diese kollektiven guten Gedanken gebracht? Nichts. Jeden Tag sterben Kinder weil Millionen von uns sich sagen, dass Mitgefühl genauso gut ist wie richtige Taten. Es ist ein innerer Mechanismus der vom trägen Teil unseres Gehirns kontrolliert wird und uns davon abhält etwas zu tun.

Wie viele von euch laufen herum und denken sich &quot;*Er/Sie würde mich lieben, wenn er/sie wüsste was für eine interessante Person ich bin!*&quot; Wirklich? Wie manifestieren sich deine interessanten Gedanken und Ideen in der Welt? Was lassen sie dich tatsächlich machen? Wenn deine Traumfrau oder Typ eine versteckte Kamera hätte, die dir einen Monat lang überall hin folgt, würden sie beeindruckt sein über das was sie sehen? Denk daran, sie können deine Gedanken nicht lesen, nur beobachten. Würden sie ein Teil dieses Lebens sein wollen?

Alles worum ich dich bitte ist die gleichen Standards, die du an jeden anderen Menschen legst, auch an dir selbst anzulegen. Bestimmt hast du auch einen nervigen christlichen Freund, der anderen nur hilft in dem er ihnen anbietet für sie zu beten. Macht dich das auch verrückt? Ich möchte hier gar nicht diskutieren ob Gebete helfen oder nicht. Das ändert nichts an der Tatsache, dass diese Leute genau die Art von Hilfe ausgewählt haben, für die sie nicht vom Sofa aufstehen müssen. Sie sind frei von jeglichen Fehlern, sie haben reine Gedanken und ihre innere Erde ist so rein wie sie nur sein kann, aber welche Früchte erwachsen daraus?    
Und sie sollten das besser kennen als sonst jemand, denn ich habe die Frucht-Metapher aus der Bibel geklaut. Jesus hat etwas in der Art von &quot;*ein Baum wird an seinen Früchten gemessen*&quot; immer und immer wieder gepredigt. Okay, Jesus hat nie gesagt &quot;*Wenn du hier arbeiten willst, verkaufe.*&quot;.   
Nein, er hat gesagt &quot;*Jeder Baum der keine guten Früchte trägt, soll abgesägt und verbrannt werden*&quot;.

Die Menschen reagieren nicht besonders gut, wenn man ihnen das sagt. Genau wie die Verkäufer nicht gut reagierten als Alec Baldwin ihnen sagte, dass sie sich paar Eier wachsen lassen oder  abhauen sollen. Was mich zum finalen Punkt bringt…

### 1. Alles in dir wird gegen Verbesserungen kämpfen.

Der menschliche Verstand ist ein Wunder und du wirst es niemals schöner in Aktion erleben, als wenn er sich gegen die Beweise wehrt, dass er sich ändern muss. Deine Psyche ist mit zahlreichen Abwehrschichten ausgestattet, die alles abschmettern was die Dinge auch nur ein klein wenig verändern könnten - frag einen Süchtigen.  
Sogar jetzt in diesem Moment, wenn einige von euch diese Zeilen lesen, bombardiert euer Gehirn euch mit reflexhaften Gründen sie abzulehnen. Hier eine Auswahl:

#### Jede Kritik als Beleidigung interpretieren.

&quot;*Wer ist er denn, mich faul und wertlos zu nennen? Eine netter Mensch würde niemals so mit mir reden. Er hat das ganze Zeug doch nur geschrieben um sich mir überlegen zu fühlen. Er will dass ich mein Leben bedauere. Ich werde mir eine eigene Beleidigung ausdenken um mich zu revanchieren.*&quot;

#### Statt sich auf die Nachricht zu konzentrieren, den Fokus auf den Nachrichtenübermittler legen.

&quot;*Wer ist DIESER Typ, MIR zu sagen wie ich zu leben habe? Oh, als wäre er ein Heiliger! Er ist nur ein dämlicher Autor im Internet. Ich finde etwas über ihn heraus dass mir beweisen wird, dass er und alles was er sagt dumm ist! Ich habe seine alten Rap Videos auf Youtube gesehen und seine Reime sind mies.*&quot;

#### Statt sich auf die Nachricht zu konzentrieren, den Fokus auf den Ausdruck der Nachricht legen.

&quot;*Ich werde einen seiner schmutzigen Witz aus dem Kontext reißen und dann nur noch über diesen denken und reden!*&quot;

#### Die eigene Erfahrung revidieren.

&quot;*So schlimm ist es doch gar nicht. Ich habe zwar letzten Monat noch an einen Suizid gedacht, aber jetzt fühle ich mich besser! Es ist durchaus möglich, dass alles gut wird, wenn ich nur so weiter mache wie bisher. Ich werde bald durchatmen können und wenn ich dem hübschen Mädchen weiterhin Gefallen tue, wird sie vielleicht mal vorbeikommen.*&quot;

#### So tun als würde man sich mit jeder Verbesserung irgendwie selbst verraten.

&quot;*Oh, also nehme ich an, ich soll meine ganzen Mangas los werden und stattdessen 6 Stunden am Tag trainieren? Weil das die einzige Option ist!*&quot;

Und so weiter. Denk daran, Jammern ist bequem. Das ist der Grund warum es so viele Menschen machen. Glücklich sein erfordert Anstrengungen. Es ist so viel leichter sich zurückzulehnen und die Arbeit von anderen zu kritisieren. Der Film ist blöd. Die Kinder von diesem Paar sind Plagen. Die Beziehung der anderen ist ein Fiasko. Der reiche Typ ist selbstsüchtig. Das Restaurant ist Scheiße. Dieser Internet Autor ist ein Arschloch.   
&quot;*Ich hinterlasse lieber einen gemeinen Kommentar und empfehle der Website ihn zu feuern. Seht ihr, ich habe etwas geschaffen.*&quot;

Oh warte, habe ich vergessen diesen Teil zu erwähnen? Egal was du baust oder erschaffst, sei es ein Gedicht, eine neue Fähigkeit oder eine Beziehung - du wirst sofort von Leuten, die selbst nichts machen, kritisiert und mit Schmutz beworfen. Vielleicht nicht in dein Gesicht, aber sie werden es tun. Dein betrunkener Freund möchte nicht dass du nüchtern wirst. Dein dicker Freund möchte nicht dass du trainieren gehst. Deine arbeitslosen Freunde möchten nicht dass du Karriere machst. 
Denk immer daran, dass sie nur ihrer eigenen Angst Ausdruck verleihen. Die Arbeit anderer schlecht zu machen ist nur eine weitere Entschuldigung nichts zu tun. 

&quot;*Warum soll ich selbst etwas tun, wenn das Zeug der anderen Scheiße ist? Ich hätte schon längst einen Roman schreiben können, aber ich warte auf was besseres. Ich möchte schließlich nicht das nächste Twilight schreiben.*&quot;    
Solange sie selber nichts produzieren, wird es für sie immer perfekt sein und sie müssen sich keine Vorwürfe machen. Oder selbst wenn sie doch mal was machen, gehen sie sicher dass es ironisch ist. Sie werden es mit Absicht schlecht machen, nur damit jeder weiß, dass es nicht ihre wahren Bemühungen widerspiegelt. Mit richtigem Aufwand wäre es fantastisch geworden, nicht so wie der Scheiß den du gemacht hast. 

Lest mal unsere Kommentare unter den Artikeln. Immer wenn sie gemein werden, passiert das aus dem gleichen Winkel heraus. Dieser Autor sollte gefeuert werden. Das Arschloch sollte nichts mehr schreiben. Macht keine Videos mehr. Es lässt sich immer auf &quot;*Hört auf etwas zu schaffen. Das ist anders als wenn ich es gemacht hätte und die Aufmerksamkeit die ihr bekommt lässt mich schlecht fühlen.*&quot; herunterbrechen.  
Sei nicht diese Art Mensch. Wenn du so ein Mensch bist, hör auf einer zu sein. Das ist genau das warum andere Leute dich hassen und du dich selbst auch. 

Wie wäre es damit: Ein Jahr. In den nächsten 12 Monaten machst du irgendwas, frag mich nicht was. Irgendwas und du wirst so gut darin, dass andere Leute beeindruckt sind. Lerne eine neue Fähigkeit, mach Karate, einen Tanzkurs, lerne Backen, baue ein Vogelhaus, lerne eine Programmiersprache oder Massieren. Dreh ein Pornofilm.  
Mach was du willst, aber es gibt eine Bedingung: Konzentriere dich nicht auf dich selbst (nicht: &quot;*Ich finde eine Freundin, ich verdiene viel Geld…*&quot;) sondern nur auf die Tatsache, dass du eine neue Fähigkeit lernst die dich interessanter und wertvoller für andere Menschen macht.