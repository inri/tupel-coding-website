---
layout: post
title: "Langweilige Videos mit Google Glass"
description: ""
date: 2013-02-23 
category: it
tags: [Google, Glass]
published: true
comments: false
share: true
---

Google hat mal wieder ein [Video über (bzw. gefilmt mit) Google Glass](http://www.youtube.com/watch?feature=player_embedded&v=v1uyQZNg2vE) veröffentlicht. Das Video sieht gut aus und zeigt die zahlreichen Möglichkeiten eine solche Brille einzusetzen. Oder?     

Im Fokus stehen verschiedene Personen und deren Beruf oder Hobbys. Ob Laufstegmodel, Eisläuferin, Ballon und Achterbahn fahren, Hund oder Kinder. Alle Personen filmen beeindruckende und besondere Situationen. Mir kamen dabei folgende Gedanken.

Als die Videoaufnahme in jedes Mobiltelefon und jeder Kompaktkamera Einzug hielt, hatte dadurch auch jeder Mensch die Möglichkeit eigene Filme zu drehen. In wie fern unterscheidet sich nun die Google Glass davon? Es ist zum einen bequemer und erlaubt einen persönlichen Blickwinkel. Okay.     
In Sachen Videotechnik war es das dann aber schon und die zahlreichen Beispiele aus dem Video lassen mich nur müde lächeln. Sicherlich ist die Sicht einer Eiskunstläuferin cool, aber nach dem fünfzigsten Video will das auch keiner mehr sehen. Das gleiche trifft für spielende Kinder und Vergnügungsparks zu.      
Genau wie ich und die meisten Menschen nicht jede Banalität aus dem Alltag filmen, wird das bei einer Videokamera auf der Nase auch nicht großartig anders sein. So spannend ist ein durchschnittliches Leben leider nicht.

Deshalb warte ich auf weitere Videos von Googles Glass, die hoffentlich richtige Innovationen und Idee zeigen.