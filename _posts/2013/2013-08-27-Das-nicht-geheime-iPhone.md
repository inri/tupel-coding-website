---
layout: post
title: "Das nicht geheime iPhone"
date: 2013-08-27 10:00:00
category: it
tags: [iPhone, Apple]
published: true
comments: false
share: true
---

Apple ist für seine strikte Produktpolitik bekannt. Über kommende Produkte gibt es keine offiziellen Informationen bis Apple sie selbst vorstellt. 


Ich finde diesen Ansatz ganz sinnvoll, denn es gibt nicht nur ab diesem Zeitpunkt alle Informationen, sondern auch ein konkretes Verkaufsdatum. Schlimm sind dagegen die halb garen Produktvorstellungen anderer Unternehmen, bei denen weder die Produkte 100%ig funktionieren und ausprobiert werden können, noch alle Informationen vorhanden sind. So weiß man nicht welcher Kontinent welchen Prozessor bekommt oder ob das Gerät überhaupt hierzulande erscheint.

Zurück zu Apple: Einige Wochen vor der Vorstellung eines neuen iPhone/iPad/... schwirren meist unscharfe Bilder und Informationen durch das Gerüchtenetz. Die zahlreichen Leaks zum kommenden iPhone 5S/5C scheinen mir aber wesentlich mehr und essentieller zu sein als bei früheren Versionen.    

Es ist noch nicht bestätigt, aber das Apple Event soll am **10.09.13** stattfinden. Schon knapp zwei Wochen vorher wird folgendes ziemlich konkret vermutet: 

###5S &amp; 5C wie Color

Die Weiterentwicklung des iPhone 5 wird sehr wahrscheinlich das Namenschema der Vorgänger beibehalten: **iPhone 5S**.   
Außerdem wird sich zum weißen und schwarzen/grauen Modell ein **goldes** bzw. **Champagner**-farbenes Modell dazugesellen (besonders in der asiatischen Welt ist diese Farbe sehr beliebt).   
Und es wird ein &quot;Billig&quot;-iPhone vorgestellt: **iPhone 5C** mit C wie **C**olor, also mit anderen Farbvarianten neben oder statt schwarz/weiß.

Zu allen Modellen erscheinen seit Wochen verschiedene Fotos und Videos, scheinbar auch aus unterschiedlichen Quellen.

###A7

Wie jedes Jahr wird es einen neuen A-Chip geben, dieses Jahr den A7. Angeblich könnte der A7 sogar 64-Bit sein, zumindest wird das von Apple getestet. 

###Fingerabdruckscanner

In den Homebutton wird sich ein Fingerabdruckscanner befinden. Wie der genau funktionieren wird, ist noch nicht ganz klar, aber der Kauf von *AuthenTec* hat mit Sicherheit etwas damit zu tun.

###Kamera

Die Kamera bekommt ein *dual LED flash* und wird sich natürlich auch verbessern, soll heißen: eine größere Blende und mehr Megapixel im Sensor.

###Keine Überraschung

Wenn ich die Liste überfliege und dann Stichworte wie 5C, 64Bit und Fingerabdruckscanner lese, bezweifle ich dass uns Apple im September noch überraschen kann.

Ich fahre heute für knapp zwei Wochen in den Urlaub (Norwegen!). Mal sehen ob das Event tatsächlich danach stattfindet und sich noch weitere Informationen ansammeln.   
Einige Artikel sollten in meiner Abwesenheit automatisch erscheinen.