---
layout: post
title: "Schlechter Service bei Designerin"
description: "Wir wollten eine simple Tasche für ein MacBook Air, aber das bekam die Designerin nicht hin."
date: 2013-01-04 
category: blog
tags: [Design]
published: true
comments: false
share: true
---

Dies ist die Geschichte einer MacBook Air Tasche. Es ist eine für uns eher frustrierende Geschichte, wie das eben so ist, wenn man es mit unfähigen Menschen zu tun hat. Die Anfangssituation ist simpel: Wir suchten eine einfache Stofftasche für ein 13 Zoll MacBook Air. Und zwar für Weihnachten 2012. 

In den Weiten des Internets gibt es natürlich viele Angebote, aber sie erfüllten die wichtigsten Kriterien nicht: Die Taschen waren entweder ausgefallen bunt und/oder teuer. Wir wollten eine ganz schlichte, dunkle Tasche und auch nicht mehr als 50€ dafür bezahlen.       
Meine Freundin war schon öfter in einem kleinen *Design-Laden* in Potsdam. Die Inhaberin bietet neben verschiedenen Dingen wie beispielsweise Kulturbeutel, Smartphonetaschen und schicke Kissen, auch Auftragsarbeiten an. So bekam ich letztes Jahr einen kleinen Beutel für meine ganzen Kabel und Ladegeräte von ihr.      
Wir dachten uns also, vielleicht würde sie so eine Tasche machen. Wir waren im November dort, haben das mit ihr besprochen, sie nahm die Maße und wir suchten den Stoff aus. Schlicht, dunkel und Kosten von nur 35€.

## Fehler 1: Kein Termindruck

Wir machten den ersten großen Fehler und sagten ihr, dass es bis Weihnachten fertig sein muss. Sie versprach es für Anfang Dezember und natürlich konnte sie den Termin nicht einhalten. Obwohl meine Freundin die letzten beiden Wochen vor Weihnachten Druck machte und anrief, konnte sie die Tasche erst Donnerstag vor Weihnachten abholen, wir begaben uns schon Samstag auf Reisen.  
   
Nur aus Spaß an der Freude wollten wir die Tasche nicht einige Wochen vor Weihnachten in den Händen halten. Als meine Freundin mal zwischendurch im Laden war, bot sie (meine Freundin) ihr (der Designerin) auch an das MacBook erneut zu vermessen. Das wollte sie aber nicht und das führte zu...

## Fehler 2: Keine Kontrolle durch den Kunden

So ein MacBook ist kein Mysterium. Es dauert keine zwei Minuten bis man alle relevanten Eigenschaften, vor allem die Maße, im Internet gefunden hat. Ja man kann sogar ohne Probleme die Maße von anderen Taschen in Erfahrung bringen. Wenn ich als Designer merke, dass meine Skizzen um einige Zentimeter abweichen, dann denke ich noch mal drüber nach. Oder?      
Die Designerin wollte nicht erneut vermessen, sie vertraute scheinbar auf ihre Notizen. Und wir vertrauten darauf, dass sie schon wissen wird, was sie tut. Das war aber nicht der Fall. Wir hätten sie bzw. ihre Arbeiten kontrollieren sollen.

Da kam also meine Freundin mit der Tasche nach Hause und war einfach nur glücklich, dass sie vor Weihnachten fertig geworden war. Sie hatte zwar im Laden ihr eigenes MacBook Air mal reingesteckt und es passte auch hinein, aber erst zu Hause wurde uns bewusst, dass die Tasche viel zu groß war. Der Mac rutschte hin und her.       
Ich nahm die Maße der Tasche und schaute mir an, wie der Mac optimal hineinpassen würde. Ich glaube an der kurzen Seite waren es 3 cm und an der lange 2 cm zu viel. Dann suchte ich im Netz schnell nach anderen Taschen für das Air und stellte fest, dass diese genau die Maße hatten, die ich nach dem erneutem Ausmessen auch gewählt hätte. 

## Auf ein Neues

Vor Weihnachten konnte die Designerin natürlich nichts mehr machen. Wir verschenkten die Tasche und nahmen sie dem Beschenkten wieder weg. Alles nicht so schön. Nun war meine Freundin im Laden und die Designerin war sichtlich unglücklich über die Nachbesserungsforderung.       
Sie sagte, dass sie den Auftrag gar nicht hätte annehmen sollen und das glaube ich ihr sogar. Insgesamt saß sie bestimmt mehr als 2 Stunden an der Tasche und nach Abzug der Materialkosten, stimmte das Aufwand-Nutzen-Verhältnis nicht mehr.
    
Nun will sie die Tasche längs etwas kürzen und an den Seiten eine doppelte Naht machen, damit sie enger wird. Aber die Maße hatte sie nicht noch einmal genommen. Wir hoffen auf ein Ende dieser Odyssee und überlegen uns das nächste Mal, ob man lokale Handwerker für so etwas beauftragen sollte.

## Epilog: Business und Selbstständigkeit

Nicht jeder kann ein Business aufbauen und leiten, das trifft für kleinere und erst recht für größere Unternehmen zu. Auch die Selbstständigkeit wie sie von der besagten Designerin *praktiziert* wird, ist scheinbar eine Herausforderung. Das ist für sie eigentlich schade, denn ihr Geschäft hätte durchaus Potential auf etwas Wachstum. Ich weiß nicht wie gut sie an den *kleinen* Taschen verdient, aber ich würde an ihrer Stelle definitiv auf den Tablet- und Notebook-Markt schielen. 
Man muss auch nicht nur in Potsdam sein Handwerk anbieten, es gibt auch Portale wie Dawanda, auf denen man seine Produkte online verkaufen kann.

Fachlich mag die Designerin gut sein. Über ihre Taschen kann man sich nicht beklagen, die Verarbeitung ist so wie sie sein soll. Aber sie scheint nicht in der Lage zu sein Termine einzuhalten, ein Projekt vernünftig zu organisieren und eine einigermaßen nette Verkäuferin zu sein.     
Meine Freundin traute sich beim letzten Mal kaum in den Laden zu gehen. Wir sind keine *Faust-auf-den-Tisch-hauen-Kunden* [^1], die die Geduld eines Verkäufers ausreizen.  Aber die Designerin gab uns das Gefühl, dass wir eine Belastung wären und fast unerhörtes Verlangen. So sollte es einfach nicht sein, aber dann braucht sie sich später auch nicht wundern, wenn es nur ein kleines Geschäft bleibt mit einigen unzufriedenen Kunden. 

Das war auf jeden Fall unser letzter Einkauf bei ihr. 

[^1]: Mit kleinen Unannehmlichkeiten kann man auch als Kunde leben, z.B. zwei Stück Zwiebel im Döner, obwohl man keine wollte.