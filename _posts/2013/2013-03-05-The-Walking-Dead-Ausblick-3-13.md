---
layout: post
title: "The Walking Dead, Ausblick 3.13"
description: ""
date: 2013-03-05 
category: medien
tags: [The Walking Dead]
published: true
comments: false
share: true
---

Da Woodbury in der vergangenen 12. Episode keine Rolle spielte, ändere ich meine Vermutungen über Andrea und den Governor nicht. Im Mittelpunkt standen nur die drei wichtigen Charaktere: Rick, Carl und Michonne. 


Man konnte schon ahnen, dass vor einer erneuten Konfrontation mit dem Governor, die Gruppe um Rick und ganz besonders er selbst, eine Entwicklung durchleben müssen. Und genau dies tat die letzte Episode oder zumindest wurde der Anfang gemacht. Rick blickte in den Spiegel uns sah sein zukünftiges Ich. Carl half sich selber dabei den Tod seiner Mutter zu verarbeiten und Michonne konnte bei den beiden Vertrauen gewinnen.

## Ein Sturm zieht auf

Hätte ich der Gefängnis-Gruppe vorher eher mittelmäßige Chancen bei einem Aufeinandertreffen mit der Woodbury-Crew gegeben, hat sich das Blatt mittlerweile gewendet. Ich denke es war elementar wichtig, dass Rick wieder einigermaßen zu Verstand gekommen ist. Nicht nur wird er Michonne Vertrauen und ihre Hilfe dankbar annehmen, sondern vielleicht auch auf Tyreese zukommen. Das wäre vor einer Episode nicht vorstellbar gewesen. Man denke nur an den Rucksacktouristen, den sie auf dem Hinweg ignoriert haben.

Merle ist der einzige in der Gruppe, dem ich längerfristig kein Vertrauen entgegenbringen würde.