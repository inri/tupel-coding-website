---
layout: post
title: "Game of Thrones, Fragen an Staffel 3"
description: ""
date: 2013-04-05 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-game-of-thrones-season-2-lg.jpg
  credit: HBO
  creditlink: http://hbo.com
---

Ich habe begonnen die Romane zu lesen und nebenbei die letzten beiden Staffeln angeschaut. Ich freue mich auf die nächsten zehn Episoden und habe mir die wichtigsten Fragen zusammengeschrieben. Vielleicht interessiert es jemanden:

*	Wohin wird es Arrya Stark und Roberts Bastard Gendry verschlagen? Und werden wir Jaqen wiedersehen (Valar Morghulis)?
*	Wie wird es Sansa, der Ex-Königin, am Hof ergehen?
*	Steht die Hochzeit und Ehe zwischen König Joffrey und Margaery Tyrell unter einem guten Stern? 
*	Welche Aufgabe wird Tyrion Lannister zukommen, da er nicht sehr dankbar aus den Diensten der Hand des Königs entlassen wurde?
*	Welche Auswirkungen hat die Hochzeit zwischen Robb Stark und der Heilerin Talisa, da er schließlich einer Tochter vom Hause Frey versprochen wurde?
*	Was für ein Schattenwesen ist beim (mehr oder weniger rechtmäßigen König) Stannis Baratheon und seiner Priesterin? Und welche Rolle wird es noch spielen?
*	Werden die weißen Wanderer schon bald eine ernsthafte Gefahr oder können die Wilden und Nightswatch sie aufhalten? 
*	Welche Rolle wird Jon Snow bei den Wilden einnehmen?
*	Was passiert wenn Jaime Lannister wieder in Kings Landing ist?
*	Wird Drachenkönigin Daenis Targaryen bald nach Westeros kommen?
*	Was passiert mit mit Theon Greyjoy, wenn er wieder zu Hause ist? Und welche Auswirkung hat der Verrat des Hauses Greyjoy? 
Wohin gehen die beiden Jungen der Starks mit Hodor und Osha?

Drücke ich einer Partie die Daumen? Naja. Sympathieträger sind am ehesten die Starks und Tyrion Lannister. Daenis entwickelt sich mit ihren Drachen auch zu einer immer härteren Anführerin und ist auch von diesem *Das ist mein Königreich*-Glauben getrieben. Schade. Die Wildlinge kann man noch nicht so ganz einschätzen. Sie sind vermutlich wichtiger als man denkt.
Den Lannisters, allen voran Cersei und Joffrey, wünscht man nichts gutes. Auch der Verrat der Greyjoys wird sicherlich noch eine Rolle spielen. Und Stannis mit seinem Schattenwesen.
Es bleibt spannend!