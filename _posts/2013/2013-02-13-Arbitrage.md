---
layout: post
title: "Arbitrage"
description: "Ein spannender Thriller mit zu wenig Tim Roth."
date: 2013-02-13 
category: medien
tags: [Film]
published: true
comments: false
share: true
image: 
  feature: 2013-arbitrage-lg.jpg 
  credit: Lionsgate
  creditlink: http://www.lionsgate.com
---

Ich mag Tim Roth als Schauspieler sehr gerne. Nun stolperte ich über den Film Arbitrage und der Trailer versprach einen spannenden Thriller. Ich wurde nicht enttäuscht. Im Fokus steht aber erwartungsgemäß Richard Gere und Tim Roth ist viel zu wenig zu sehen. Schade.

## Worum gehts?

Die Situation ist relativ schnell klar, sogar wenn man sich nur den Trailer anschaut. Der wohlhabende Geschäftsmann Robert Miller will seine Firma verkaufen und das muss so schnell wie möglich passieren, weil deren Bücher gefälscht sind und die Kacke am Dampfen ist. Zu dieser Sorge gesellt sich ein Autounfall, bei dem seine Affäre ums Leben kommt und er als Fahrer vom Tatort flüchtet. Ab jetzt ist ihm der Detektive Michael Bryer auf der Spur und versucht Beweise gegen ihn zu finden, denn für Bryer ist klar, dass er der Fahrer des Unfallwagens war.

Besonders viele Ruhepausen gibt es nicht. An allen Stellen flammen gefährliche Brandherde auf und Miller kommt mit dem Löschen nicht hinterher. Seine Tochter, Finanzchefin des Unternehmen und bisher unwissend, beginnt etwas von dem Betrug zu ahnen. Der Sohn von Millers ehemaligen Fahrer holt ihm vom Tatort ab und soll ihn decken, gerät dabei aber selbst ins Visier von Bryer. Besonders interessant wird die Jagd als klar wird, dass scheinbar die Polizei auch nicht mit fairen Mitteln spielt. 

## Fazit

Ein gutes Porträt eines kalten Geschäftsmann, von dem nach und nach klar wird, dass Geld für ihn das Maß aller Dinge ist. Auf der anderen Seite aber auch eine kritische Betrachtung der Strafverfolgung. Es wird deutlich wie viel Macht und Möglichkeiten die Eliten haben und trotzdem muss man sich die Frage stellen, wie weit Behörden gehen dürfen. Kein perfekter Film, aber durchaus sehenswert.

Etwas enttäuschend fällt das Ende aus bzw. ist gar keines vorhanden. An einigen Stellen sind die Fronten meiner Meinung nach nicht geklärt, doch trotzdem ist plötzlich Schluss.      
Meine Vermutung für ein theatralisches Ende war folgende: Von dem Unfall hat Miller einige Blessuren davongetragen und scheinbar auch eine Verletzung am Bauch, die er nicht verarzten lässt. Als er sich zum fünften Mal aufgrund von Schmerzen an die Stelle fast, war ich mir sicher, dass er am Ende aus allem heil herauskommt und überraschend seiner Verletzung erliegt. Das Schicksal hätte ihn doch eingeholt.