---
layout: post
title: "Dilemma beim Schreiben wissenschaftl. Arbeiten"
description: ""
date: 2013-04-13 
category: blog
tags: [Studium]
published: true
comments: false
share: true
---

Es scheint so, als müsse Wissenschaft in Deutschland kompliziert sein. Einige Meinungen, die ich gehört habe und die sich auch mit meiner Erfahrung decken, sagen, dass wissenschaftliche englische Texte wesentlich besser zu verstehen sind als deutsche. 


In Deutschland sollen wissenschaftliche Texte wissenschaftlich-deutsch sein, also für den Laien nicht verständlich. Aber genau das ist der falsche Ansatz. Eine Bachelor-, Master-, oder sogar Doktorarbeit sollte so geschrieben sein, dass sie auch Menschen verstehen, die nicht im gleichen Fachgebiet tätig sind. 

Geschriebene Texte kann man nicht mehr ändern, aber wir Studenten schreiben neue Texte. Doch genau an dieser Stelle tut sich ein Dilemma auf. Unsere Texte werden selbstverständlich von einer Instanz begutachtet. Doch welche Meinung vertritt diese Instanz?        
Meine Bachelorarbeit sei angeblich zu wenig wissenschaftlich gewesen. Dass eine Arbeit über den Prozess der Anforderungserhebung anders ist als eine über eine bestimmte Softwaretechnologie, sollte klar sein. Aber sei es drum.     
Meine Freundin hatte ihrem Betreuer den ersten Entwurf ihrer Arbeit geschickt und der gab ihr nun das Feedback, dass sie einiges einfacher formulieren kann, denn *es solle auch jemand verstehen, der sich nicht mit dem Gebiet auskennt*. Ihr Betreuer ist relativ jung, die Zweitkorrektorin nicht. Wie schreibt man nun eine solche Arbeit, wenn man nicht sicher sein kann, wie die Instanzen sie bewerten?

Was ich sagen will ist folgendes: Als Student, wenn man seine ersten wissenschaftliche Texte schreibt, wird auf die Art und Weise des Schreibstils geachtet, wobei es dafür keine gängigen Definitionen gibt. Wie man fremde Texte zitiert ist klar definiert. Den Schreibstil bewerten aber die Instanzen und das ist von Person zu Person unterschiedlich. Eventuell gewöhnen sich Studenten einen anderen Schreibstil an oder schlimmstenfalls kehren sie der Wissenschaft den Rücken.

Klare Regeln kann es für Stil nicht geben, aber ein Leitfaden wäre eine große Hilfe.