---
layout: post
title: "Der Apple-Check zum Schämen"
description: "Eine unterirdische Dokumentation über Apple in der ARD."
date: 2013-02-05 
category: medien
tags: [Apple]
published: true
comments: false
share: true
---

Am Montagabend lief in der ARD der Apple-Check und erwartungsgemäß war das eine Aneinanderreihung von Blödsinn. Man ging verschiedenen Grundthesen nach und vergab dann zwischen 0 und 3 Äpfeln, wie sehr die These angeblich zutrifft. Mit gesundem Menschenverstand oder journalistischer Arbeit, hatte die ganze Sendung nichts mehr zu tun.

Ich gehe die einzelnen Punkte mal durch und kommentiere sie.

## Apple infiziert? - 3/3 Äpfeln

Beweis für diese These war zum einen eine verblendete Designstudentin, die einfach nicht wusste, wieso sie einen Mac wollte und natürlich ein Gehirnscan von *Apple-Fans*. Für einen Mac gibt es viele gute Gründe, die man nur schwer widerlegen kann. Sei es das Gewicht, Akkulaufzeit, Trackpad oder Bildschirm, zeig mir einen Windows-Notebook mit diesen Stärken. 

Der Gehirnscan wurde auch erst entsprechend für die These interpretiert. Bei Samsung-Geräten, also bei Samsung-Liebhabern, wird ein anderes Hirnareal aktiviert als bei Apple-Geräten, bei Apple-Liebhabern. Der Professor/Doktor sagte *Man kann die Ergebnisse so interpretieren, dass bei Samsung eher der Kopf entscheidet und bei Apple eher der Bauch.* Apple macht süchtig und die Kunden irrational! Meinte der Apple-Check.       
Wie sähe der Scan wohl bei anderen Geräten und Marken aus? Wieso löst die Apple Technik eine solche Reaktion aus? Vielleicht sind die Kunden einfach nur überaus zufrieden und dieses Produkt ist eine Bereicherung für ihr Leben? Wann hätte das ein normaler Mensch mal über einen Windows 98 Computer behauptet? Und welche anderen Produkte lösen eine solche Reaktion aus? Interessante Fragen, keine Antworten.

Ein Grund für die Infizierung durch Apple ist die Tatsache, dass die Menschen zufrieden mit den Produkten sind. Das ist eigentlich ein gutes Merkmal, aber das will der Beitrag nicht zugeben. Jemand der Apple gut findet, wird als bisschen blöd dargestellt.

## Apple ist einfach? - 2/3 Äpfeln

Die Profis konnten fast nichts Negatives am iPad finden, im Gegensatz zum Surface und Galaxy Note und so nahm man den älteren Herren als Beispiel, dass einiges doch zu kompliziert sei. Naja. Ich fand das Beispiel zu speziell und im vergleich zu anderen Geräten war es sicherlich nicht komplizierter, bspw. die Foto-Übertragung von iPod zu iPad. 

## Apple ist seinen Preis wert? - 1/3 Äpfeln

Dass ein iPhone 3GS von 2009 nach einem Bierbad nur widerspenstig seinen Dienst tut, kann man ahnen. Warum man es dann aber mit einem Samsung Galaxy S2 von 2011 vergleicht, ist mir rätselhaft. Ging das gleichaltrige iPhone 4S in den Proben vielleicht nicht kaputt?      
Auch für die Akku-Thematik gibt es eine weitere interessante Begründungen, die man ruhig hätte erwähnen können. Design hin oder her, kann der Nutzer den Akku selbst austauschen, kann er auch minderwertige evtl. sogar gefährliche Akkus einbauen. Apple hat entschieden, dass das nicht sein muss und vermutlich für mehr Nutzer eine Gefahr birgt, als ein wirklicher Nutzen für die Deppen, die ihre iPhones ins Wasser fallen lassen. Apple aber vorzuwerfen, sie wollen die Nutzer mit dieser Restriktion abzocken [^1] ist geradezu albern wegen ihres nächsten Arguments.

Die Material- und Herstellungskosten des iPhones sind viel geringer als sein Verkaufswert und das ist die blanke Abzocke. Wie man das Verhältnis realistisch einschätzt, sagen uns die Redakteure nicht. Wie sieht denn das bei Autos und Kühlschränken aus?      
Und niemals wurde der Wert, den ein iPhone für seinen Nutzer haben kann, mit seinem Preis verglichen oder wie das bei anderen Herstellern wohl wäre. Ich persönlich nutze mein iPhone viel mehr als damals mein HTC Desire.     
Und das Forschung und Entwicklung für ein Gerät pro Jahr mehr kosten dürfte als bei 20 Geräten anderer Hersteller, sollte auch klar sein.

## Apple ist fair? - 0/3 Äpfeln

Das Unverständnis dieses Teils liegt wohl darin begründet, dass man von Apple annahm, dass sie im Gegensatz zu allen anderen westlichen Großfirmen ihre Produkte *fair* herstellen, wie auch immer dieses *fair* genau definiert ist. Kapitalistische Realität ist aber nun einmal: Die westliche Welt beutet andere Länder aus. Teils ist das nicht per se schlecht, denn es schafft u.a. Arbeitsplätze dort, aber natürlich gibt es auch die negativen Seiten. Für uns ist das übrigens sehr gut und das ist es schon seit Jahrzehnten.      
Dass der Beitrag die Überstunden, den geringen Lohn und die Selbstmorde benennt, aber keine Silbe von Apples Initiativen verlauten lässt, ist beschämend für den öffentlich-rechtlichen Rundfunk. Denn die Realität sieht so aus, dass von allen großen Technikunternehmen, Apple das fortschrittlichste in diesem Bereich ist. Darüber schrieb ich auch schon einmal

Keine Frage, aktuell ist das alles noch lange nicht zufriedenstellend, aber zumindest arbeitet man bei Apple daran.

## Mein Fazit

Mein Problem bei solch platten Pauschalisierungen gegen eine spezielle Firma ist das, was unausgesprochen bleibt: *Die anderen Firmen wie Samsung müssen wohl besser sein.* Der unbedarfte Zuschauer konsumiert die Kritik an Apple und auch wenn nichts darüber gesagt wird, denkt er, dass die anderen Firmen anscheinend besser sein müssen. Sie wurden in der Kritik schließlich nicht genannt.      
Hat der öffentlich-rechtliche Rundfunk das wirklich nötig? Für solchen Mist bezahle ich also GEZ bzw. Rundfunkgebühren? Dann kann ich auch einfach im Heise-Forum lesen...

### Epilog

Im Anschluss konnte man bei Hart aber Fair weiter der Facepalmierung frönen. Thema war der Umgang von Kindern mit modernen Bla. Als ich der Medienpädagogin beim ersten Satz lauschte, wie sie von Studien redete und dabei die Augen weit aufriss, damit man ja auch sieht wie wichtig ihr das Thema ist, war mir klar dass keine neuen Erkenntnisse zu holen waren.       
Aber der Lauer war großartig, also hatte es sich doch gelohnt. Sehr gut war eine Szene, in der er einen bayerischen Schuldirektor so richtig gegen die Wand fahren lässt.      
Völlig zurecht.

[^1]: Ein Wechsel des Akkus geht nur vom Servicetechniker und das kostet mehr als ein Billig-Akku aus China.
