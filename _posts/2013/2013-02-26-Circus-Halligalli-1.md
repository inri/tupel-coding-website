---
layout: post
title: "Circus Halligalli, Folge 1"
description: ""
date: 2013-02-26 
category: medien
tags: [Fernsehen]
published: true
comments: false
share: true
image: 
  feature: 2013-circus-halligalli-lg.jpg 
  credit: Pro7
  creditlink: http://www.prosieben.de/tv/circus-halligalli
---

*neoParadise* adé, willkommen in der Manege des Circus Halligalli! Aber kann die neue Sendung von Joko &amp; Klaas den Erwartungen gerecht werden? Teil-teils. Eine Meinung. 

Der Aufbau der neuen Sendung der beiden Chaoten ist größtenteils identisch mit *neoParadise*: Beiträge, Gäste, Telefonzellen-Band statt Schrank-Band und ein musikalischer Auftritt am Ende. So weit, so gut. Sicherlich wollte Pro 7 kein völlig neues Konzept und das alte war und ist ja auch gut. Etwas enttäuschend war das leider, weil die beiden im Vorfeld sehr mysteriös taten und ich irgendwie mehr erwartet habe.

Einige Worte zu den einzelnen Elementen der Sendung.

#### Blödeleien - Die Ja-Sager

Joko und Klaas in Bestform. Eine doofe Idee und ihre Ausführung. Ich fand es schade, dass der Beitrag geteilt wurde und man nun eine Woche auf den zweiten Teil warten muss. Aber ich warte gespannt.

#### Charles Schulzkowski

Für mich bleibt der erste Auftritt dieser Kunstfigur, verkörpert vom herrlichen Olli Schulz, mit Abstand der beste. Bei dem neusten Abenteuer wird er auf die Berlinale geschickt, doch sein Aufenthalt dort ist für mich nicht das Highlight, sondern die erste Szene in der Bar. Richtig guter Text und sehr stimmige Atmosphäre. 

Sein Herumtorkeln zwischen den Stars und Sternchen löste bei mir Mitleid für die Menschen dort aus. Ich glaube dass die Grenze in Form eines kleinen Gitters beim roten Teppich enorm wichtig für den ersten Auftritt war. Olli stand zwar direkt neben der Security und anderen *Journalisten*, hatte aber einen gesunden Abstand zu den Schauspielern. Kam einer zu ihm und trank einen Lütten, entschied er sich freiwillig dazu.
Bei der Berlinale waren sie dem sehr gut angetrunkenem Schulzkowski ausgeliefert. Besonders die zweite Konfrontation mit dem unglaublich höflichen und geduldigen Jörg Thadeusz war eher unnötig. Wenn ich beginne mit den *Opfern* Mitleid zu haben, schlägt das auf die Laune.
 
Es gab schon bessere Beiträge mit Olli.

#### Helge Schneider

Da haben sich die beiden aber einen ziemlich schwierigen Gast ausgesucht. Helge ist mal so und mal so und dieses mal war er in der Rolle des Sonderlings. Wer das mag, hatte Spaß beim Zuschauen so wie Joko und Klaas anscheinend. Aber als Interview ging das kaum durch. 

#### WTF-Momente

Da gäbe es insgesamt vier Stück, wobei einer noch zu Helge Schneider gehört. Mitten in dem Gespräch, welches schon durch den Schulzkowski-Beitrag unterbrochen wurde, kam ein Kleinwüchsiger mit einem Mini-Auto hereingefahren und brachte Klöße in einem Topf. Angekündigt wurde dies als nicht abgesprochene Einlage. Warum? Vielleicht wollte man dem Circus-Charakter gerecht werden.  Joko &amp; Klaas freuten sich und das Gespräch mit Helge wurde ein weiteres Mal unterbrochen. Schade.

Oma Violetta kündigte die Show kurz an und beschwerte sich über ihr Kostüm. Dann war sie weg. Keine Kommentare von ihr, keine wenn auch nur kurze Interaktion mit den Gästen oder so. Auch schade.      
Gaststar Oliver Pocher stand draußen und wurde einmal von Joko und einmal von Klaas abgewiesen. Er käme niemals in die Sendung. Typischer Moment bei den beiden.       
Und am Ende die durchaus kreative Ankündigung von Sido durch Palina.

#### Fazit
Joko und Klaas gehen ihren Weg und machen das, was sie gut können. Der Circus Halligalli ist kein Feuerwerk der großen Innovationen, aber das muss er auch gar nicht sein. Die Sendungen mit den beiden Chaoten bieten stets Abwechslung.

Ich empfand die Auswahl der Gäste und der Beiträge nicht optimal. Schulzkowski hätte man sich vielleicht für später aufheben können. Helge Schneider ist nicht unbedingt leicht, aber es gab auch zu viele Unterbrechungen um das Gespräch in Fahrt zu bringen. Da war *neoParadise* geradliniger und weniger chaotisch.

Trotzdem ist das noch immer besseres Fernsehen als die hundertste Talkshow bei ARD und ZDF, so viel steht fest. Ich freue mich auf die nächste Woche und das sogar aus einem weiteren Grund. Dank meines grandiosen Bruders, sind wir nächste Woche bei der Aufzeichnung einer Sendung mit dabei.