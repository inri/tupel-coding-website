---
layout: post
title: "Schriftgröße auf Websites"
description: ""
date: 2013-03-24 
category: blog
tags: [Design]
published: true
comments: false
share: true
---

Wieso ist die Schrift auf Webseiten so klein? Ich surfe fast nur mit meinem 15 Zoll Retina MacBook und dem stationären Windows-PC (zwei 24 Zoll Monitore) durchs Netz. Zu den beiden Monitoren versuche ich einen Abstand zwischen 70 und 100 cm einzuhalten. Beim Macbook ist es weniger, eher so 50 cm bis 80 cm. Entweder sind meine Augen trotz Brille so schlecht, oder kleine Schriften sind in Mode. 

Auf den meisten Seiten muss ich die Schrift vergrößern (cmd/Strg + / -), sonst macht das Lesen keinen Spaß. Ich bin mir noch nicht ganz sicher, was der Grund für das Problem ist. Sitze ich einfach zu weit entfernt? Eher unwahrscheinlich. Sind die Bildschirme zu groß bzw. zu hochauflösend? Noch unwahrscheinlicher. Oder sind die Webseiten eher für kleinere Bildschirme (17 bis 20 Zoll) konzipiert? Das führt dann zu der Frage, wie es besser sein könnte oder welche Geräte das Hauptziel sein sollten. Oder gehört das auch zum Responsive Design, obwohl man zuerst immer an Mobiltelefone und Tablets denkt, auf die sich die Webseiten anpassen müssen? 

Ich denke mal darüber nach. Vielleicht hat jemand schon einige Lösungsansätze.       
Mit dem Tastenkürzel lässt es sich auch ganz gut leben. Außer man stolpert über Seiten, die dagegen immun sind. Aber solche Seiten sind selten.