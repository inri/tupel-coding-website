---
layout: post
title: "Kalte Wohnung"
date: 2013-11-30 10:00:00
category: blog
tags: [Persönlich, Halle]
published: true
comments: false
share: true
---

Vor zwei Monaten sind meine Freundin und ich nach Halle gezogen. Unsere Wohnung befindet sich im 9. Stock eines typischen Blocks im Stadtteil Halle-Neustadt und ist ein Kompromiss. 


Die Wohnung ist ein Kompromiss, weil die Lage und das Viertel nicht besonders toll sind. Aber die Miete ist sehr günstig und die Wohnung groß. Und als Bonus gab es den Erlass von zwei Kaltmieten.

<figure>
    <a href="/images/2013-halle-lg-1600.jpg">
	   <img 
		  src="/images/2013-halle-lg-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Unser Block, wir wohnten ganz oben rechts (Vorderseite mit Balkon)</figcaption>
</figure>

Wenn man sich dieses Bild anschaut, stellt man zweifelsohne fest dass unser Block kein Schönheitswettbewerb gewinnt. Wir dachten uns dass sei akzeptabel, denn schließlich ist die Wohnung eher als temporäre Lösung für die nächsten 2 Jahre gedacht, nicht für die nächsten Jahrzehnte.

Aber um das Fazit vorweg zugreifen: **Wir haben falsch gedacht!** Wenn ein Haus so scheiße aussieht, folgen mit großer Wahrscheinlichkeit Probleme die nicht nur ästhetischer Natur sind.  Unsere Vermieter geizen nicht nur mit einem neuen Anstrich.

### 18&deg; Raumtemperatur

Der Spaß begann Mitte Oktober. Draußen wurde es kühler und die Heizung heizte nur sporadisch. Der Hausmeister erklärte uns dass die Heizung erst ab unter 18&deg; heizt. In den Oktoberwochen war es wechselhaft, mal waren die Temperaturen über 18 Grad, mal darunter.    
Als die Temperaturen zum Novemberübergang deutlich unter der Grenze blieben aber insbesondere die Wohnzimmerheizung deutlich zu kühl war (direkt über dem Heizkörper nur 35&deg;), wurde die Vorlauftemperatur endlich erhöht und ab der ersten Novemberwoche waren die Heizungen heiß.

Das Problem war nur, die Räume wurden nicht wärmer. Noch immer fiel nachts die Temperatur auf 17&deg; und tagsüber schafften wir zwischen 18&deg; und 19&deg;. Falls das jemand nicht weiß: 20&deg; bis 22&deg; sind normal und stehen sogar in unserem Mietvertrag drin.    
Meine Freundin sitzt im Wohnzimmer nur noch in einer Decke eingewickelt. Beim Fernsehen haben wir kalte Nasenspitzen und kalte Füße. Es ist keine unmenschliche Temperatur, aber auch keine akzeptable. 

### Schlitze in den Fenstern

Was kann der Grund für den Wärmeverlust sein? Die Heizungen sind okay, sie sind nicht der Grund.    
Ich schaute mir unsere Wohnung ganz genau an und entdeckte an den Stellen wo die Fenster eingebaut sind kleine Schlitze. Nicht durchgängig sondern hier und da einige Zentimeter lang und aus diesen Schlitzen kamen kalte Luftströme. Ein möglicher Grund, dachte ich.    
Es war Montag und ich rief den Hausmeister an. Er meinte, dass könne sein und ich solle der Verwaltung mitteilen die Firma für die Fenster zu beauftragen eine Verfugung durchzuführen.    

<figure>
    <a href="/images/2013-halle-wand-1-1600.jpg">
	   <img 
		  src="/images/2013-halle-wand-1-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Die Schlitze an den Fenster sind deutlich zu erkennen und auch der Versuch sie zu verputzen.</figcaption>
</figure>

Dann passierte einige Tage lang nichts. Am Donnerstag riefen wir stundenlang bei der Verwaltung an, doch erreichten niemanden. Erst am Freitag gab es einen Anruf von der Fenster-Firma und die Suche nach einem Termin. Dieser sollte in 10 Tagen sein und außerdem erfuhren wir, dass der Auftrag erst am Donnerstag gestellt wurde. Oho.

<figure>
    <a href="/images/2013-halle-wand-2-1600.jpg">
	   <img 
		  src="/images/2013-halle-wand-2-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Auch beim Balkoneinbau sind große Schlitze und wirklich gut verfugt ist da nichts mehr.</figcaption>
</figure>

Die kalten Tage vergingen und endlich kam der Monteur und ich erklärte ihm die Situation. Er schaute drei Sekunden lang die Fenster an und sagte &quot;*Das liegt nicht an den Fenstern.*&quot;

### Risse in der Fassade

Das große Problem ist die Fassade. Er kennt das von unserem Block und hat das der Verwaltung schon oft mitgeteilt. Passieren tut nichts.   
Besonders auf der Wohnzimmerseite sieht die Fassade schlimm aus. Außen um die Fenster herum müssen die Wände richtig verputzt und abgedichtet werden, teils ist durch die Schlitze die Dämmung zu erkennen.

Die kalte Luft dringt ins Gemäuer und entweder direkt durch die Schlitze in die Wohnung oder macht die Wände einfach sehr kühl. Der halbe Meter zwischen Balkon und Fenster ist beispielsweise richtig kalt. Ich kenne das von Wänden nicht so. Die verbauten Fenster sind übrigens zu klein und deshalb ist der Abstand zwischen Fensterrahmen und Wand breiter als gewöhnlich. Also eine vergrößerte Kältebrücke. 

Bevor die kleinen inneren Schlitze, wenn überhaupt, repariert werden können, muss die Fassade von außen saniert werden.     
Auch ein größerer Heizkörper würde nicht viel bringen, weil einfach sehr viel Wärme durch die kalten Wände entzogen wird.

### Keine Druckmittel

Das ist alles eine Schweinerei, aber es kommt noch besser. Wir können fast nichts dagegen machen. Die einzige Möglichkeit ist eine Mietminderung und in unserem Fall bei den besagten Raumtemperaturen geben die Gerichte zwischen 20 und 30 Prozent auf Warmmiete.    

Doch wenn der Vermieter das ignoriert, kann der Mieter nichts weiter machen, außer Ausziehen. Wir haben bei Nachbarn geklingelt und die haben genau die gleichen Probleme. Bei ihnen regnet es sogar hinein! Die sind aber noch schlimmer dran als wir, weil das Amt deren Wohnung bezahlt und sie nicht mal eine Mietminderung machen können. 

Falls ich mich irre, sagt mir das bitte! Die Informationen habe ich mir auch nur mühsam zusammen gesucht. Ein Anwalt oder der Mieterverein wüssten bestimmt mehr, kosten aber auch Geld. Und bisher sehen alle Informationen so pessimistisch aus, dass wir uns das Geld dann lieber sparen würden.

### Fazit: Umziehen?

Die Wohnung war von Anfang an kalt und den Grund kannte die Verwaltung inklusive Hauswart auch. Als ich ihn auf die rissigen Wände und Schlitze ansprach, reagierte er gereizt. Jetzt weiß ich wieso. Die Vermieter-Seite spielt auf Zeit.   

Aber was sollen wir weiter tun? Die Frist ließ man ohne Kontaktaufnahme verstreichen und deshalb mindern wir für Dezember die Miete. Doch was dann? Es wird draußen kälter und in den letzten Tagen schafften wir nur noch 17,5&deg;, nachts sinkt die Temperatur sogar auf 16,5&deg;. Wie wird es dann erst mit dauerhaften Minusgraden?

Ignoriert die Verwaltung unsere Mietminderung? Wird man uns eine Mahnung schicken? Wird man weiter auf Zeit spielen?    

Zwar haben wir keine Lust auf einen erneuten Umzug, aber vorsichtshalber suchen wir schon eine neue Wohnung.