---
layout: post
title: "Das neue iPhone und fragwürdige Sicherheit"
date: 2013-09-28 00:01:00
category: it
tags: [Apple, iPhone, Usability]
published: true
comments: false
share: true
---

Am Ende ist es nur eine einfache Gegenüberstellung:   
**Sicherheit vs. Usability**.


Die Medaille namens Fingerabdrucksensor bzw. *Touch ID* mit samt allen Technologien und Nutzungsszenarien hat zwei Seiten. Ich persönlich positioniere mich auf der Seite der Usability, diskutiere jedoch zuerst die berechtigten Bedenken zu Apples Technologie. 

#### Daten sind niemals sicher

Machen wir es kurz: Keine Daten sind sicher. Es gibt gute Verschlüsselung und gute Konzepte. Aber an irgendeiner Stelle in der Sicherheitskette gibt es immer eine Schwachstelle. Und wenn es nur ein Passwort ist.

Apple kann die Daten noch so gut sichern, eine 100%ige Garantie kann einfach niemand geben. Das trifft übrigens auf so ziemlich alle Risiken des Lebens zu.    
Gehen wir also von der Annahme aus, dass es möglich ist auf alle Daten zuzugreifen, auch auf die persönlichen Daten die Fingerabdrücke darstellen. Angewendet auf die Technologie rund um das iPhone 5s heißt das nichts anderes, als dass Daten über meinen Fingerabdruck entstehen, irgendwo gespeichert und potentiell verwendet werden können.     
Das gleiche gilt übrigens auch für ähnliche Technologien wie der Biometrische Personalausweis.

## Was genau ist Touch ID?
  
Zur Keynote erhielten wir von Apple die wenigen Informationen, dass die Daten über den Fingerabdruck nur auf dem iPhone gespeichert und nicht an Apples Server übertragen werden. Genauerer gesagt sind sie sogar in einem extra Bereich des Chips verschlüsselt und können nur vom Sensor abgerufen werden. Apple nennt es die *Secure Enclave* und hat das zusammen mit ARM entwickelt, eben mit genau diesem Ziel (sichere Speicherung von Daten) im Blick. &quot;Wie können wir verhindern, dass jemand genau diese Daten nicht erlangen kann?&quot;
  
Okay. Und nun? Siehe oben, sicher sind die Daten doch nicht und früher oder später wird Apple bestimmt eine Datenbank mit diesen Daten anlegen. Oder?

Wie funktioniert der Touch ID Sensor überhaupt? Die gute Nachricht: Finger abschneiden und ans iPhone halten wird **nicht** (sofort) funktionieren. Wieso?    
Der Sensor sitzt unter dem Homebutton und wenn ein Finger den neuen Metallkreis um den Homebutton herum berührt, wird der Sensor aktiviert. Dann scannt ein Raster aus kapazitiven  Sensoren den Fingerabdruck in einer Auflösung von 500dpi und aus diesen Informationen erkennt der Sensor charakteristische Pattern des Abdrucks (anhand der Linien bzw. Berge und Täler des Fingerabdrucks).    
Allerdings scannt der Sensor **nicht nur** die Schicht toter Haut, die man überall als Fingerabdruck hinterlässt, sondern auch die darunter liegende &quot;lebende&quot; Schicht mit Hilfe eines RF-Signals (Infrarot-Technik). Und genau das funktioniert eben nur mit einem &quot;lebenden Finger&quot;. 

#### Ein Hack?

Der CCC hat gezeigt, dass man einen Fingerabdruck abfotografieren, ausdrucken und über den eigenen Finger legen kann. Das akzeptiert der Touch ID Sensor auch. Aber ist das ein Hack und zerfällt damit Apples Sicherheitsversprechen?    
Nein. Wie beim iPhoneBlog schon gesagt, ist das keine neue Technik und kein Geheimnis. Aber es ist vor allem aufwendig und daher sicher nichts, wovor man sich tagtäglich fürchten muss. 

Kritisch wird es für Apple, wenn es jemand schafft in die oben beschriebene *Secure Enclave* einzudringen und die Daten der Fingerabdrücke abzugreifen.    
Mich würde übrigens sehr interessieren welche Ideen die Klugscheißer vom CCC als Touch ID Alternative haben. Und kommt mir jetzt nicht mit &quot;langes Passwort&quot;. Die Anforderung sind möglichst große Sicherheit und möglichst große Usability. Her mit den Ideen!

## Fingerabdruck als Passwort

Die Metadiskussion beschäftigt sich wie bereits gesagt nicht um die Sicherheitskonzepte von Apple sondern um das kommende Szenario: Die Sammlung biometrischer Daten von enorm vielen Menschen. Welche Möglichkeiten liegen in solchen Daten?   
Es gibt kluge Menschen, die sich darüber schon Gedanken gemacht haben. Ich bleibe bei der kleineren Frage: Warum sollte man den Fingerabdruck überhaupt als Passwort nutzen?

Wie viel sicherer im Vergleich zu einem Passwort ist ein Fingerabdruck tatsächlich? Er ist ein offenes Geheimnis, baumelt mit unseren Fingern am Ende des Arm und kann kopiert werden. Sogar wenn wir ihn nur irgendwo hinterlassen.    
Oder man kann beispielsweise mit Gewalt gezwungen werden seinen Finger herzugeben, dafür ist eine Kopie gar nicht notwendig. Drohungen und abgetrennte Finger haben wir schon unzählige Male im Agentenfilm gesehen. Nur eine Zukunftsfiktion oder -vision?

Aber seien wir mal ansatzweise realistisch: Die Wahrscheinlichkeit dass eine der besagten Möglichkeiten eintritt oder das Ziel sein könnte (Zugriff auf die Daten unserer iPhones) ist lächerlich gering! Oder anders: **Dass NSA und Co im Besitz unserer Passwörter sind, ist viel wahrscheinlicher als Fingerabdruckkopierende iPhone-Diebe.**    
Ich bin davon überzeugt dass meine Daten mit Touch ID sicherer sein werden. Endlich kann ich ein ernsthaftes Passwort für meine Apple ID verwenden und hoffentlich bald auch für andere Dienste. 

Noch einmal: Ich bin mir sicher, dass viel zu viele Menschen aus Faulheit und dank schlechter Usability bestehender Sicherheitsysteme viel zu schlechte Passwörter verwenden. Das stellt insgesamt eine viel größere Gefahr für diese Menschen dar, als ein aufwendiger &quot;Hack&quot; eines Fingerabdrucksensors.

## Usability verbessern

Natürlich kann man glauben, dass andere Absichten hinter Apple Vorstoß stecken, doch ich hingegen bin davon überzeugt, dass es in erster Linie um eine Verbesserung der Benutzbarkeit geht. Und so wie es aussieht, werden in den nächsten Monaten viele Hersteller ebenfalls solche Fingerabdrucksensoren in ihre Geräte (Notebooks, Tablets) einbauen. 

Warum also mag ich Touch ID?      
Ich hatte es schon einmal kurz beschrieben: Ich nutze erst seit einigen Monaten die Sperre meines iPhones durch einen 4-stelligen Pin. Ich war davor schlichtweg zu faul und scheinbar bin ich nicht der einzige faule Benutzer. Tim Cook verriet uns, dass 50% der iPhone-Nutzer keine Sicherung nutzen. Ich hätte ja gedacht es sind mehr.   
Es ist trotzdem eine unschöne Zahl und ich bin mir sicher, dass man in Cupertino schon seit einiger Zeit nach besseren Lösungen gesucht hat.

Nicht erst seit den ersten Gerüchten bzgl. des Fingerabdrucksensors habe ich mir gewünscht, dass mein iPhone mich, seinen Besitzer, sofort beim Anschalten erkennt. Das ständige Eintippen des Codes ist einfach nur zeitraubend, nervenraubend und schlechte Usability.   
&quot;*Doch es gibt Passcode-Alternativen!*&quot; höre ich jemanden rufen. Ja, gibt es diese?

#### Gleich schlechte Alternativen

Bei Android kann man einen grafischen Code angeben, aber genau diesen muss mann auch ständig &quot;aufzeichnen&quot; und er ist auch nicht besonders sicher. Damit meine ich gar nicht die Frage nach der Sicherung der Code-Daten, sondern die Fehlerquelle Touchscreen. Wessen Bildschirm ist nicht verschmutzt/schmierig? Mit etwas Probieren kann man die Abfolge der Zeichnung herausbekommen, dank der Dreckschicht auf dem Touchscreen.

Samsung hatte mal ein Gesichtserkennung eingebaut und wenn ich mich nicht irre, konnte man diese sogar mit simplen Fotos umgehen. Wenn ich einen Link finde, reiche ich ihn nach.   
Ansonsten bleiben nur Passwörter und das Thema hatten wir schon. 

So oder so, meinen Wurstfinger als Passwort zu nutzen ist **schnell**, **sicher** und **bequem**. Daher ist **Touch ID** eine gute Entwicklung und ich freue mich darauf. Apple hat die Usability verbessert, aber wir sollten trotzdem ein wachsames Augen auf die Daten haben.