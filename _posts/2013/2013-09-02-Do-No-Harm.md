---
layout: post
title: "Do No Harm"
date: 2013-09-02
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-do-no-harm-lg.jpg
  credit: NBC
  creditlink: http://www.nbc.com/classic-tv/do-no-harm
---

Über *Do No Harm* hätte ich beinahe nichts schreiben können, denn aufgrund der schlechten Zuschauerzahlen wurde die Serie nach nur zwei Episoden abgesetzt. Doch dann entschied man sich bei NBC doch noch dazu die restlichen 12 Episoden auszustrahlen. Eine gute Entscheidung.

## Um was geht es?

Der erstklassige Arzt Jason Cole, verkörpert durch Steven Pasquale, hat eine gespaltene Persönlichkeit. Alle 12 Stunden exakt um 8:25 erscheint sein zweites, böses Ich Ian Pierce und sorgt für Ärger. In den vergangenen Jahren nahm Jason für die nächtliche *Ian-Phase* ein Betäubungsmedikament, doch gegen den Wirkstoff ist sein Körper mittlerweile immun und Ian zurück.   

Jason möchte mit einem neuen Mittel, welches ein Freund und Kollege aus dem Krankenhaus für ihn entwickelt, seine Ian-Persönlichkeit ein für alle mal *töten*. Aber bis das Mittel einsatzbereit ist, beginnt Ian damit Stück für Stück Jasons Leben zu zerstören.

Ein 12-Stunden-Tag ist eine komplizierte Angelegenheit, vor allem wenn man sein Geheimnis nicht preisgeben möchte. Im Krankenhaus hat Dr. Cole gewisse Privilegien, die ihn von nächtlicher Arbeit entbinden. Doch beispielsweise der neidische Arzt Dr. Kenneth fragt sich was Jason des Nachts so tut.    
Nachdem eine erste lange Beziehung gescheitert war, geht Jason Frauen aus dem Weg, zumindest bis er seine Kollegin Lena besser kennenlernt. Doch ohne Komplikationen kann sich diese Beziehung natürlich nicht entwickeln.

Kurz: *Do No Harm* ist ein modernes Drama basierend auf der [Geschichte von Dr. Jekyll und Mr. Hyde](http://de.wikipedia.org/wiki/Der_seltsame_Fall_des_Dr._Jekyll_und_Mr._Hyde).

## Was gefällt?

Mir hat in den ersten Episoden sehr gefallen, dass die Serie spannend und *schnell* ist. Es gibt für Jason beispielsweise im Krankenhaus immer wieder Patientenfälle, aber sie stehen im Gegensatz zu anderen Arzt-Serien nicht im Mittelpunkt, denn die Geschichte von Jason/Ian wird in jeder Episode weiter vorangetrieben. Relativ zeitig wird dem Zuschauer bewusst, dass je länger Ian ein Teil von Jasons Leben ist, er desto mehr zerstören wird und so fiebert man immer wieder mit wenn die 12 Stunden für Jason ablaufen.

Aber selbst Ian ist nicht das reine Böse. Die oben erwähnte frühere Beziehung ist nicht das was man erwartet. Jason und Ian sind nicht unbedingt die komplexesten Figuren der Filmgeschichte, aber es ist unterhaltend ihnen bei ihrem Kampf zuzuschauen, was besonders an der guten Leistung von Steven Pasquale liegt.    
Als ich ihn zuletzt gesehen habe, spielte er die Rolle eines etwas dümmlichen Feuerwehrmanns in *Rescue Me*. In *Do No Harm* kann er zeigen was er kann.

Auch den Wechsel der Persönlichkeiten an einem bestimmten Zeitpunkt zu binden, ist eine tolle Idee. Dass der Mensch Jason/Ian praktisch nie zum Schlafen kommt, denn jede Persönlichkeit möchte ihre Zeit voll ausnutzen, ist eher eine Randnotiz. 

## Fazit 

Warum die Einschaltquoten nicht gut waren, kann ich mir nicht erklären. *Do No Harm* ist auf jeden Fall unterhaltsam und dank des Tempos auch nicht langweilig. Leider wird wohl nur die eine Staffel ausgestrahlt und das war es dann. Wer sich ein schönes Serien-Wochenende machen möchte, den kann ich auf *Do No Harm* verweisen. Eine unterhaltsame Drama-Serie.