---
layout: post
title: "The Walking Dead, Ausblick 3.16"
description: ""
date: 2013-03-25
category: medien
tags: [The Walking Dead]
published: true
comments: false
share: true
---

Ich fand die Vorstellung, dass der gute Bruder den bösen stoppen muss, sehr dramatisch und so wie es tatsächlich passiert ist, war es das auch. Allerdings anders als erwartet. 

Rick kam wie erwartet wieder zu Verstand und das Gespräch mit Merle hat einiges dazu beigetragen. Nicht nur Merles Analyse von Rick sondern die simple Tatsachenbeschreibung, dass jeder im Gefängnis in ihm den Teufel sieht und sie nun trotzdem ein grausames Vorhaben umsetzen wollen. Dass macht die Gruppe genauso schlecht wie ihn. Und gegenüber seines Bruders bemerkt er, dass diese Grausamkeit notwendig ist und jemand es tun muss.

Das ist seine Aufgabe und entsprechend handelt er. Zum Glück kann Michonne ihn überzeugen und er tut einmal im Leben etwas Gutes. Ich mag es, dass keiner aus dem Gefängnis weiß, was er getan hat. Sein Tod ist für Daryl wichtig (Rache), die anderen werden mit Koffer packen beschäftigt sein.

## Gefängnis

Wir wissen aus der Vorschau schon, dass man in Aufbruchsstimmung ist. Aber wer, was und wie genau, können wir nur ahnen. Eine Möglichkeit: Alle gehen weg. Unwahrscheinlich, denn ich erwarte eine Konfrontation und diese müsste dann irgendwo auf der Straße stattfinden. Dabei hat man doch das tolle Gefängnis als Kulisse.      
Zweite Möglichkeit: Hälfte-Hälfte. Die Schwachen (Asskicker, Einbeiniger Hershel) machen sich schon auf dem Weg und bekommen z.b. Michonne als Beschützerin. Dann könnten wir interessante Zombie-Szenen bekommen und wie ich schon forderte: Die Zombies müssen wieder gefährlich werden. Die Starken machen sich auf den Weg nach Woodbury. Aber warum? Angriff auf den Governor? In der Vorschau ist genau dieser aber am Gefängnis. Also entweder über Schleichwege nach Woodbury und z.b. Andrea retten, wobei sie natürlich nicht wissen, dass sie in Gefahr ist oder sie bleiben im Gefängnis (dritte Möglichkeit). Da haben sie eindeutig Heimvorteil und können Fallen und so was vorbereiten.

So oder so, schwierig. Doch da kommen wir zu Woodbury.

Wir haben auch noch eine Andrea, die vermutlich vom Governor gefoltert wird. Dass er sie tötet, denke ich nicht. Was würde es auch bringen, wenn es ihre Gruppe nicht mitbekommt?     *Besser* wäre es, wenn sie gefoltert und in diesem Zustand von Tyreese gefunden wird. In der Vorschau haben wir ihn nicht beim Gefängnis mit der Governor-Gruppe gesehen. Also wo ist er?

In Woodbury werden die Waffen verladen und mit dicken Gefährt zum Gefängnis geritten. Die Stadt ist ungeschützt? Oder wird Tyreese als Wache zurückgelassen?      
Findet eine Konfrontation im Gefängnis statt, muss Ricks Plan gut sein, denn ich halte es für ausgesprochen schwierig die Gruppe um den Governor komplett abzuwehren. Oder vielleicht will man auch nur der Schlange den Kopf abschlagen?

Mir fallen die Mutmaßungen schwer. Vielleicht schickt man nur eine Person *on the Road* (Michonne?) und der Rest der Gruppe versteckt sich im Gefängnis? Im Comic findet der Governor beim Angriff auf das Gefängnis sein Ende, nachdem er dort für heftige Verluste verantwortlich war. Wer könnte diese Verluste sein?

## Menschliche Verluste

Baby Asskicker würde ich nicht gänzlich ausschließen, aber ich glaube Robert Kirkman möchte diese Möglichkeit noch eine Weile verfolgen. Eventuell auch, weil er es im Comic nicht getan hat?      
Hershels Geschichte ist zu Ende erzählt. Er hat eine interessante Entwicklung durchlebt und wirkte auf Rick und seine Mitmenschen. Ein Kandidat, vor allem auch wegen seiner Behinderung.      
Glenn &amp; Maggie? Eher nicht. Ihre Beziehung hat sich zwar konsequent weiterentwickelt, aber an einem Endpunkt sind wir noch nicht angekommen.Vor allem Glenn lässt einige Führungsqualitäten durchblitzen, davon will ich mehr sehen.       
Rick, Carl, Michonne, Daryl? Nein. Die sind alle viel zu wichtig für die Serie und Fans. Ich könnte mir eine schwere Verletzung vorstellen um danach sagen zu können *Das war knapp*. Aber keinen Tod.      
Carol &amp; Beth. Maybe. Carol hatte schon sehr oft Glück gehabt und die angedeutete Liebelei zwischen Daryl wurde auch nicht weiter vertieft. Beth blieb eher farblos. Da war mal was mit Carl, aber das wurde auch nicht weitergeführt.
Andrea, siehe oben.
