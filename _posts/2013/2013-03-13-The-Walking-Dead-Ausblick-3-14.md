---
layout: post
title: "The Walking Dead, Ausblick 3.14"
description: ""
date: 2013-03-13 
category: medien
tags: [The Walking Dead]
published: true
comments: false
share: true
---

In der Episode 3.13 wurden alle Weichen auf die nahende Konfrontation gestellt. Ein blutiges Aufeinandertreffen zwischen Team Woodbury und Team Prison ist wohl unausweichlich. Daneben konnten sich die beiden Alphatiere endlich beschnuppern und das fiel erwartungsgemäß nicht besonders positiv aus.      

Der Governor möchte Rache an Michonne üben und fordert ihre Herausgabe von Rick. Als Gegenleistung verspricht er die Gefängnisbewohner in Ruhe zu lassen, plant aber genau das Gegenteil. Rick ist hin- und hergerissen. Einerseits ahnt er, dass man diesen Irren nicht so einfach los wird, andererseits möchte er die einzige Chance auf Frieden nicht einfach undurchdacht aufgeben. Seinen Leute belügt er und schürt ihre Angst.

Dankbar bin ich besonders für Andreas Aufwachen. Auch wenn die Zweifel seit dem Besuch im Gefängnis an ihr nagten, hatte sie noch Hoffnung auf eine friedvolle Lösung des Konflikts. Aber schon kurz nach Beginn der Verhandlungen realisierte sie, dass der Governor nicht zu kontrollieren ist.

## Aufregung in Woodbury

Die nächste Episode spielt vor allem in Woodbury und wird Andrea dabei zeigen den Governor aufzuhalten. Irgendwie. Was wissen wir noch aus den kurzen Vorschauclips? Milton verrät seinen Boss, dieser wird sauer sein und Andrea wendet sich hilfesuchend an Tyreese und seine Schwester.      
Wie ich es schon erwartet habe, wird Tyreese ein offenes Ohr für ihre Bedenken haben und sicherlich in Konflikt mit dem Governor geraten. Bringen wird es sicherlich nicht so viel. Ich rechne sogar damit, dass Andrea und Tyreese + Schwester aus Woodbury verbannt werden oder fliehen müssen. Eventuell bereitet der Governor eine große Gefahr vor und das Gefängnis muss gewarnt werden (ja, ich denke da an einen Panzer). 

## Vorbereitungen im Gefängnis

Werden wir jemals erfahren wie sich Rick entscheidet? Bevor die Gefängnisgruppe über Michonnes Schicksal diskutieren wird, kommt Besuch aus Woodbury in Form von Freunden mit wertvollen Informationen. Wie man mit den Ausgestoßenen umgehen wird, werden wir in der vorletzten Episode erfahren.      
Merles kleiner Aufstand wird hoffentlich auch noch thematisiert oder zumindest glaube ich nicht, dass sie ihn bis zum Finale ruhigstellen. Eventuell werden die Nachrichten aus Woodbury ihn dazu bringen das Gefängnis zu verlassen. Als Verräter oder Flüchtiger? 
