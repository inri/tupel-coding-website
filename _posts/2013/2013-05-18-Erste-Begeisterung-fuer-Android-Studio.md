---
layout: post
title: "Erste Begeisterung für Android Studio v0.1"
description: ""
date: 2013-05-18
category: it
tags: [Android]
published: true
comments: false
share: true
---

Ich fand die vorgestellten Features und die Geschwindigkeit der neuen *Android IDE* schon bei der Keynote beeindruckend, aber die Realität ist sogar noch beeindruckender. Ich habe nun einige Stunden mit dem Android Studio gearbeitet. Und es macht verdammt viel Spaß. Wieso? Bitte weiterlesen.

## Geschwindigkeit

Ich arbeite mit einem Retina MacBook, genauer gesagt schuftet ein i7 mit 2,6 GHz in dem Gehäuse unter meinen Fingern. Hinzu kommen 8 GB RAM, eine GeForce GT 650M und eine verdammt flinke SSD von Samsung. Nicht alles ist für das Programmieren relevant, aber die Ausstattung ist einfach gut. Trotzdem ruckelt Eclipse an vielen Ecken und Enden. Möchte ich beispielsweise ein *Rename Refactoring* durchführen, dauert es zwischen 2 und 5 Sekunden bis die Änderungen übernommen wurden. Oder bei etwas größeren XML-Layouts scheint die Interaktion mit Maus und Tastatur nur noch einmal in der Sekunde registriert zu werden. Es. Ist. Schlimm.     
Mit dem Android Studio flutscht es einfach wie von selbst. Ich habe bisher vor allem viel Refactoring gemacht und das meiste waren Dinge, die ich schon auf meiner ToDo-Liste stehen hatte, aber noch vor mir herschob. Nun sind sie erledigt und vor allem viel schneller als in Eclipse.

## Kleine Helfer          

Wenn ich im Code XML Resourcen nutze, bekomme ich am linken Rand eine kleine Vorschau der Bilder, Farbe oder was auch immer. Noch cooler wäre diese Funktion, wenn sie auch mit *Drawables* funktionieren würde, zum Beispiel ein *Drawable* für ein Button, welches definiert wie der Button bei welcher Aktion aussieht.

Ebenfalls eine große Hilfe ist die Integration des 9Patch Editors. Bisher musste man zuerst die PNG erstellen, dann für jede einzelne Datei relativ umständlich den Editor öffnen, alles bearbeiten und konnte dann erst die 9PNGs importieren. Nun importiere ich die PNGs direkt in mein Projekt und bearbeite sie mit dem 9Patch Editor. Das geht dank des verbesserten Setzen der Patches viel schneller.

## Intelligentes Löschen

Wenn ich eine Datei (z.b. Resource) löschen möchte, checkt Android Studio das gesamte Projekt und zeigt mir eine Liste mit allen Verlinkungen zur Datei. Ich kann die Dateien in der Liste dann bequem durchgehen und die Konflikte bearbeiten. Es ist geradezu grandios.       
Als ich vor zwei Jahren mit der Android Programmierung begonnen habe, lernte ich viel anhand des Nachprogrammieren von Tutorials. Manchmal sind die Vorgaben aber nicht vollständig, besonders oft beim Wechsel der Kapitel, und dann tauchen überall Fehler auf und man weiß als Neuling nicht wo man beginnen soll. Wenn in den XML-Dateien Fehler sind, kann Android R nicht erzeugen und das führt zu eben diesen Fehlern. In Eclipse muss man dann mit der unschönen Problems-Ansicht zurechtkommen.

Das sind die ersten Dinge, die mir über den Weg gelaufen sind und mich beeindruckten. Die Layout-Vorschau ist auch sehr schick und irre schnell. Ich bin gespannt was sich noch entdecken lässt.