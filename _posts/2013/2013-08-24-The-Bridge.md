---
layout: post
title: "The Bridge"
date: 2013-08-24
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-the-bridge-lg.jpg
  credit: Fox
  creditlink: http://www.foxchannel.de/the-bridge-america
---

Im Sommer laufen in den USA neue Serien an und da ich bis vor kurzem mit meinen letzten Prüfungen zu tun hatte, kam ich erst in den letzten Wochen dazu einige neue Serien zu begutachten (= *binge watching*). Den Anfang macht der amerikanisch-mexikanische Krimi [The Bridge](http://de.wikipedia.org/wiki/The_Bridge_%E2%80%93_America).

## Um was geht es?

Exakt auf der Grenze zwischen den USA und Mexiko wird eine horizontal zerteilte Leiche gefunden. Die beiden Körperhälften gehören zu zwei Frauen, die obere Hälfte einer amerikanischen Richterin mit ablehnender Haltung gegenüber Einwanderern und die untere Hälfte einer von vielen jungen Frauen, die in Mexiko ermordet werden.    
Da es sich um zwei Morde aus zwei Ländern handelt, ermitteln die polizeilichen Behörden beider Länder gemeinsam und das geschieht durch die Amerikanerin Sonya North (Diane Kruger) und den Mexikaner Marcelo Ruiz (Demian Bichir). 

Die Serie begleitet die beiden ungleichen Partner bei den Ermittlungen rund um diesen und weiteren kommenden Morden, die auch zusammenhängen. Der Täter möchte Aufmerksamkeit auf die Einwanderungspolitik und generell den Umgang mit Verbrechen beider Ländern lenken. 

## Was gefällt?

*The Bridge* spielt auf mehreren Ebenen. Weder die beiden Ermittler, noch die Mordfälle stehen allein im Mittelpunkt. Die Serie erzählt die Geschichten vieler Personen, die alle in irgendeinem Zusammenhang mit den Morden stehen. Da ist beispielsweise die Witwe eines wohlhabenden Mannes, die einen unterirdischen Tunnel über die mexikanische Grenze in ihrem Keller entdeckt. Oder das junge Mädchen mit dem Workaholic-Vater und Drogen-Mutter, welches einfach so in die touristenunfreundliche Viertel einer Grenzstadt geht und beinahe gekidnappt wird.

Die Hauptdarsteller stechen dabei besonders heraus. Diane Kruger spielt fantastisch eine autistische Polizistin, die kein Fettnäpfchen auslässt. Man kann sie sich wie einen ernsten Sheldon Cooper vorstellen. Ihr mexikanischer Kollege ist dabei fast das glatte Gegenteil und versucht die fehlende Empathie im Team wieder auszugleichen. Die beiden harmonieren und das kann der Zuschauer schmunzelnd genießen.

## Fazit 

Nach dem Anschauen der ersten 7 Episoden von insgesamt 12 der ersten Staffel, kann ich *The Bridge* nur empfehlen. Es ist ein spannender Krimi, am ehesten mit *The Killing* vergleichbar. Die Serie schafft eine äußerst gute Balance zwischen dem stetigen Voranschreiten der Ermittlungen und der Charakterzeichnung der Hauptpersonen. Unter Umständen sind die vielen Nebenhandlungen stellenweise verwirrend, aber trotzdem stets sehenswert.

**Wer das Krimi-Genre und interessante Charaktere mag, sollte *The Bridge* anschauen**