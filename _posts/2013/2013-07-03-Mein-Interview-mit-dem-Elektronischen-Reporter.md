---
layout: post
title: "Mein Interview mit dem Elektronischen Reporter"
description: ""
date: 2013-07-03
tags: [Google, Glass]
category: medien
published: true
comments: false
share: true
---

In einem Beitrag für den [Elektronischen Reporter](http://www.elektrischer-reporter.de/ "Elektronischer Reporter") soll es über Datenbrillen gehen. Und man interessiert sich für meine Meinung dazu.


### Vorgeschichte

Vor einigen Wochen habe ich meine Emails sortiert und stolperte über den Spam-Ordner von Google. Ich bekommen relativ wenig Spam und schaue deshalb nur alle paar Monate dort rein. Meist finde ich dann auch nur *gute* Werbung und deshalb achte ich nicht sehr auf den Inhalt des Ordners.      Diesmal fand ich aber eine Email von einer Journalistin und es war sogar die zweite Mail von ihr, die erste hatte ich anscheinend aus Versehen gelöscht. Obwohl seit der Ankunft der Email fast 4 Wochen vergangen waren und ich keine großen Hoffnungen hatte, dass sie noch interessiert sei, antwortete ich.

## Meinung eines Bloggers

Die besagte Journalistin arbeitete an einem Beitrag über Datenbrillen und bei ihrer Recherche stieß sie auf meine kritischen Artikel über die Google Glass und wollte gerne meine Meinung für den Beitrag aufzeichnen. Es ging nicht speziell um die Google Glass sondern um eine Zukunft mit Datenbrillen allgemein.      
Scheinbar gibt es in der deutschen Blogosphäre so wenig Menschen, die sich mit diesem Thema auseinandersetzen, dass sie in der hintersten Ecke meinen Blog gefunden hat. Ich fühlte mich geehrt und hoffte, dass es noch klappt und das tat es dann auch.

Letzte Woche Freitag fuhr ich nach Berlin und traf mich mit ihr. Wir machten ungefähr 15 Minuten Interview von denen aber nur sehr wenig zu sehen sein wird [^1], denn eine Folge Elektronischer Report ist &lt; 20 Minuten lang. Meine *Rolle* war die eines neugierig-interessierten Nerds / Informatikstudent und das bin ich ja auch. Was habe ich so erzählt?

## Eine Zukunft mit Datenbrillen?

Datenbrillen haben auf jeden Fall Potential. Ich erinnere gerne an Mobiltelefone vor 15 Jahren, damals ahnten auch nur die wenigsten welches Potential in diesen Geräten steckt. Stellt euch nicht unbedingt die Google Glass vor, sondern einfach die Technologie die Welt durch einen virtuellen Schleier zu sehen. Retina Datenbrillen erscheinen bestimmt in nicht all zu ferner Zukunft und wie lange es wohl dauert, bis Kontaktlinsen entsprechende Technik mitbringen?      
Wenn wir die vielen Aktivitäten, für die wir heute noch unser Mobiltelefon aus der Tasche kramen müssen, direkt mit einer Datenbrille machen könnten, wäre allein das Freihaben beider Hände schon ein enormer Gewinn für die Menschen.

Bleiben wir aber bei Brillen, denn in den nächsten Jahren wird es noch keine Hightech Kontaktlinsen geben. Bei den Google Glass stellt sich die zentrale Frage: Wie empfinden die Menschen diese neue Art von Technik? Nicht jeder wird eine besitzen, aber man wird über kurz oder lang nicht mehr an ihr vorbeikommen. Ich bin mir sicher, dass sich neue soziale Regeln ausbilden werden. So wird man an Orten mit besonders hohem Menschenaufkommen (Restaurants, Bars) sensibler mit diesen potentiellen Überwachungskameras umgehen.

Hinzu kommt der Aspekt, dass die anfallenden Daten irgendwo gesammelt werden. Es wird mit Sicherheit keine Datenbrille ohne Internet geben, selbst wenn Speicherchips immer kleiner werden und man nicht zwingend die Daten in der Cloud sichern müsste. Wer hat die Macht über diese Daten und wie schützt man sie? Theoretisch muss man auch die Datenbrillen an sich schützen, denn die Möglichkeit überall und jederzeit auf fremde Augen und Ohren zugreifen zu können, ist für gewisse staatliche Institutionen sehr verlockend.      
Wobei sich einige dieser Fragen auch in Hinsicht auf Mobiltelefone stellen, das darf man nicht vergessen.

## Fazit

Man muss damit beginnen sich Gedanken über dieses Thema zu machen. Selbst wenn die Google Glass keinen reißenden Absatz findet, aus welchen Gründen auch immer, wird die Entwicklung rund um diese Art Technologie nicht stoppen. Neben neuer Möglichkeiten und viel Potential ist es letztlich auch ein neuer Markt und dieser möchte erobert werden.

[^1]: Die Aufnahmen von mir und der Dozentin, die auch an diesem Tag interviewt wurde, fanden leider keine Verwendung.