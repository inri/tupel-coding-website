---
layout: post
title: "Langsame Android Updates"
description: ""
date: 2013-03-21 
category: it
tags: [Android]
published: true
comments: false
share: true
---

Marco Arment verlinkte diesen [Gizmodo-Artikel](http://gizmodo.com/5987508/why-android-updates-are-so-slow), der ausführlich erklärt, wieso es monate- und teilweise jahrelang dauert bis Android-Geräte ein Update erhalten. 


[Dabei kritisierte er](http://www.marco.org/2013/03/19/why-android-updates-are-so-slow) die Naivität des Artikels, der den verschiedenen am Update-Prozess beteiligten Parteien zu viel unkritischen Spielraum für Erklärungen gab. Und er schlussfolgert, dass Apple sich eben um alles kümmert und Google nur die halbe Arbeit macht, so dass der Mist an anderen hängen bleibt.

Mir fiel ein anderer Aspekt auf. Und zwar berichtet der Gizmodo-Autor fast schon triumphierend, dass Apple genauso lange für ein Update braucht, nur dass Apple eben das Update erst ankündigt, wenn es fast fertig ist.        
Für mich bedeutet das aber, Apple hat in puncto OS-Entwicklung die Nase vorn! Sagen wir großzügig, ein Update ist erst offiziell veröffentlicht, wenn es auf 60% aller Smartphone-Modelle der letzten 2 Jahre zu haben ist. Mit welchem Android müsste sich dann iOS 6 messen?      
Die letzte Android Version heranzuziehen und als heißen Scheiß von Google zu handeln, ist abseits der Realität. Das macht sämtliche Vergleiche einer neuen Android-Version nach der im Sommer stattfindenden Google IO obsolet. 

Ein Problem an dem Google arbeiten muss. Meint auch Marco Arment.