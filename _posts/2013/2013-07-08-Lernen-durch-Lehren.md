---
layout: post
title: "Lernen durch Lehren"
description: ""
date: 2013-07-08 23:50:00
category: it
tags: [Coding]
published: true
comments: false
share: true
---

In den letzten Wochen habe ich an meinem Lebenslauf und Vita gefeilt. Ein interessanter Punkt sind dabei die Fähigkeiten/Kenntnisse, die man als Informatiker bzw. Softwareentwickler besitzt. Es ist nicht genug Platz um für jede Programmiersprache den Grad an Erfahrung anzugeben, statt dessen muss man sich aufs Aufzählen beschränken. Aber was kann ich aufzählen und was sollte ich besser nicht aufzählen?

Ich bin beispielsweise über die Angaben eines Kommilitonen gestolpert und war über die Anzahl erstaunt. Ich kenne ihn einigermaßen gut und weiß, dass er nicht zu den &quot;Programmiergöttern&quot; unseres Jahrgangs gehört. Aber das stand ja auch nicht drin, sondern nur, dass er eine große Anzahl an Technologien und Programmiersprachen kennt. Wie gut? - das ist die Frage.
Es gibt z.b. Programmiersprachen, mit denen man sich innerhalb eines Semesters für einige Wochen beschäftigt hat und auch nicht auf einem hohen Niveau. Aufzählen oder nicht? Einerseits ist die Information, dass man diese Sprache kennt, von gewisser Bedeutung, andererseits sollte jeder Programmierer, der drei Programmiersprachen beherrscht sich problemlos in eine neue Einarbeiten können.

Ich tendiere deshalb dazu, nur Fähigkeiten anzugeben, die ich auch sofort abrufen könnte. Beispielsweise mit dem Android Framework kenne ich mich mittlerweile gut aus und könnte mein Wissen sofort anwenden. Ich habe zwar vor einigen Semestern C gehabt und die Referenz von Ritchie und Kernighan  durchgearbeitet, wäre aber sicherlich ein lausiger C Programmierer.

## &quot;Lehren&quot;

Mir fiel also auf, dass ich mein Wissen über Programmiersprachen verbessern sollte. Dazu gibt es schon ganz gute Portale wie die Code School oder Codecademy, aber vielleicht findet sich ein motivierender Weg.
In einem längeren Artikel, [How to Launch Anything](http://www.smashingmagazine.com/2013/06/28/how-to-launch-anything/), beschreibt Nathan Barry wie man ein Produkt veröffentlicht. Unter anderem verweist er dabei auf Chris Coyier, der die Seite [CSS-Tricks](http://css-tricks.com/) betreibt. Auf dieser hat er sein Wissen rund um CSS geteilt und während die Anfänge für Kenner von CSS nicht weiter spannend waren, wurden die Themen anspruchsvoller. Durch den &quot;Druck&quot; seinen Lesern neues Wissen zu vermitteln, wurde auch Chris immer besser.
Das kling doch interessant, dachte ich mir.

Ich mache mir da keine Hoffnungen, die fünf Leser dieses Blogs werden sicherlich nicht auf meine Code-Ergüsse gewartet haben, aber es ist für mich persönlich ein Ansporn und sicherlich rückblickend auch ganz amüsant.
Mein Ziel ist alle 2 Tage etwas in einer von 4 Sprachen zu programmieren. Ausgesucht habe ich mir Objective-C, Python und Ruby. Eine vierte folgt noch und wird so was wie Go, Scala oder Ada. Mal sehen. Den Code werde ich auf GitHub veröffentlichen.  
Das Projekt würde ich aber erst nach meinen Klausuren starten, also ab Mitte August.

Ich nenne das mit Absicht &quot;Lehren&quot; in Anführungszeichen, denn so richtig lehren kann ich gar nicht und möchte dieses Versprechen auch nicht geben. Ich lasse euch an meinen Gedanken und Fortschritten teilhaben und vielleicht wird das sogar interessant.

### Nachtrag Anfang 2016

Pustekuchen! Ich bin so ehrlich und entferne die zweite Hälfte dieses Artikels nicht. Mein Vorhaben führte ich nie aus. Zum einen war Android mein Fokus der letzten Jahre und zum anderen vergaß ich es ganz einfach. Ich schäme mich…