---
layout: post
title: "Entwickler mit magischen Fähigkeiten gesucht"
description: ""
date: 2013-06-21
tags: [Technologie]
category: it
published: true
comments: false
share: true
---

Wenn man sich als angehender Softwareentwickler mit der Jobsuche beschäftigt, stellt man schnell fest, dass besonders große Unternehmen interessante Bewerbungsinterviews führen. Es gibt ganze Blogs mit Sammlungen von mal mehr und selten weniger schwierigen Programmieraufgaben. 


Auch wenn ich mich ab und zu mit solchen beschäftige und nach meinem Studium dieses Vorhaben intensivieren möchte, sehe ich den ganzen Spaß realistisch. Ich begreife eine Algorithmus-Idee irgendwann und kann sie auch umsetzen, aber in viel zu kurzer Zeit sortiert mein Gedächtnis diese wertvolle Information wieder aus.

Am idealsten wäre es für die Interviewer natürlich, wenn man sich nicht im Vorfeld schon mit den Aufgaben beschäftigt hat, sondern spontan auf eine Lösung kommt bzw. einen guten Lösungsweg skizziert, was man letztlich durch das Üben lernt. Doch besonders was das Anwenden von Algorithmen angeht, wird man im (Bachelor-)Studium nicht gerade damit überhäuft. Nach dem ersten Jahr war es das eigentlich auch schon mit dem *komplizierten* Programmieren und danach folgt eher das Erlernen von Konzepten. Umso mehr wundert es mich, welche frischen Informatikstudenten in kniffligen Interview bestehen können. Lassen wir mal die mit 10-jähriger Programmiererfahrung und schneller Auffassungsgabe weg.

## Organisation statt Magie

Die gleiche Frage stellt sich auch [James Hague, der noch weitere gute Aspekte](http://prog21.dadgum.com/177.html "Organizational Skills Beat Algorithmic Wizardry") beschreibt. Zum Beispiel, dass man in der Praxis nur sehr selten komplizierte Algorithmen braucht, aber genau nach solchen in Job-Interviews gerne gefragt wird. Viel wichtiger sind gute Fähigkeiten in der Organisation von Projekten und den ganzen Aufgaben, die sich beim Coden eben so ergeben. Zum gleichen Thema gibt es auch [das höchst interessante Interview](http://www.nytimes.com/2013/06/20/business/in-head-hunting-big-data-may-not-be-such-a-big-deal.html?pagewanted=all&_r=2& "In Head-Hunting, Big Data May Not Be Such a Big Deal") mit dem *Senior Vice President for People Operations* bei Google, der einige mehr oder weniger überraschende Dinge erzählt.

Unter anderem haben sie bei Google herausgefunden, dass typische Kopfnuss-Aufgaben gar nichts bringen und nicht bei der Auswahl helfen. Sie konnten auch bei all ihren Interviewern keine Korrelationen zu den eingestellten Mitarbeitern feststellen, also ob einer ein besonders gutes Händchen bei der Auswahl zukünftiger Mitarbeiter hatte. Auch die Durchschnittsnoten der Bewerber sagen höchstens etwas aus, wenn sie frisch von der Uni kommt. Nach ein paar Jahren haben sich die Menschen so verändert, dass die Noten für eine Entscheidung nicht mehr relevant sind und Google sie angeblich gar nicht mehr verlangt.  
   
Das deckt sich auf jeden Fall auch mit meinem Empfinden.

## Mein bisher größtes Projekt

Ich werde demnächst eine App für Android vorstellen. Sie ist ein Kundenauftrag und ich habe in den letzten Monaten neben dem Studium an ihr gearbeitet. Ihre Kernfunktionalität ist schon seit Mai fertig, aber eine wichtige Funktion war bis gestern ein Problem. Mehr dazu später.      
Die App besteht aus einigen tausend Zeilen Java Code, aber keinem einzigen schwierigen Algorithmus. Es war einfach nicht notwendig. Die Daten liegen in einer SQLite Datenbank und werden über einen Cursor verwaltet. Am schwierigsten war das Verhalten der App, also wo wird zu welcher Zeit welche Information angezeigt, was passiert wenn man mit diesen Vorbedingungen da klickt und so weiter. Natürlich habe ich zahlreiche Stunden über die besten Abläufe gegrübelt, aber das Wissen über Sortierverfahren und Datenstrukturen hat definitiv nicht den Ausschlag gegeben.

Ich kann zwar nur von mir sprechen, aber ich denke den meisten Studenten geht es genauso.

## Fazit

Was nützt diese Erkenntnis? Ich habe vor kurzem erfahren, dass ich selbst in 20 Jahren noch mein Abiturzeugnis einer Bewerbung beilegen sollte. Mein heutiges Ich hat schon nichts mehr mit meinem Ich vor 5 Jahren zu tun. Doch es wird besonders in Deutschland erwartet und man muss sich eben fügen.      
Letztlich sind es ja die Unternehmen, die sich selbst ins Fleisch schneiden, wenn sie das Potential auf diese Weise vorbeiziehen lassen. Wie eine gute Art von Interview aussehen müsste, kann ich euch leider auch nicht verraten. Dass das nicht leicht ist, sollte klar sein, aber zumindest sind die Informationen von Google ein erster Anfang.