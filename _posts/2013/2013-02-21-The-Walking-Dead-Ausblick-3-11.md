---
layout: post
title: "The Walking Dead, Ausblick 3.11"
description: ""
date: 2013-02-21 
category: medien
tags: [ The Walking Dead]
published: true
comments: false
share: true
---

Anstatt eine einzelne Episode zu Reviewen, ist es vielleicht auch mal ganz interessant über die nächste Episode nachzudenken. Was könnte als nächstes passieren oder was wünscht man sich?

## Im Gefängnis

Hatten wir vor der zehnten Episode noch vier Lager (Woodbury, Gefängnis, Daryl &amp; Brother, Tyreese &amp; Co), traf Daryl nun die richtige Entscheidung und ging zurück ins Gefängnis, mit seinem Arschloch-Bruder. Ohne ihr Eingreifen, wäre es für Rick und die anderen vermutlich sehr knapp geworden und das hat ersterer hoffentlich auch endlich begriffen. Wobei man bei ihm vermutlich nicht ganz sicher sein kann, was er zur Zeit begreift. Durch Daryl hat die Gruppe aber wieder einen Anführer, der Ricks Lücke vorerst schließen kann.       
Es macht enorm viel Spaß die Figur von Glenn beim Wachsen zuzuschauen, aber in den letzten beiden Episoden passierte das doch ein wenig zu schnell. Und seine Entscheidung, alleine mit dem Auto wegzufahren, war nicht unbedingt klug. Er wird sicherlich keine großen Töne spucken, aber ob er begreift in welche Gefahr er Maggie gebracht hat?

Mir wäre es auch lieber, wenn Tyreese und seine Gruppe bald wieder auftauchen würden und dann natürlich von Rick akzeptiert werden. Vielleicht hebt man sich das für einen Showdown auf, quasi die Rettung in letzter Sekunde. 
Ansonsten braucht Carl langsam wieder etwas Screentime, er stand in den letzten beiden Episoden nur Schmiere. Das gleiche trifft für Michonne zu.

Merle ist klar, dass der Governor ihn töten will und er allein in der Zombiewelt auch nicht die besten Karten hat. Der Gefängnisgruppe zu helfen, scheint erst einmal logisch. Aber nur vorerst, denn einige aus der Gruppe sind auch wütend auf ihn, wie lange wird er also geduldet? Vor allem kann er vermutlich keine Rückendeckung von seinem Bruder erwarten. Ich könnte mir vorstellen, dass er in der nächsten Zeit hilfreich ist und eine Bereicherung. Doch an einem Punkt wird er etwas Blödes tun (versuchen Rick zu töten?) und genau dann wird Daryl ihn aufhalten (= töten).

## In Woodbury

Im Lager Woodbury ahnt Andrea sicherlich, dass der Governor nicht zum Pilze sammeln draußen war. Wenn ich das richtig gesehen habe, wurden auch zwei seiner Männer bei dem Gefecht getötet. In der Mini-Vorschau haben wir ja schon gesehen, dass sie ihm sagt, dass sie zum Gefängnis möchte und er scheinbar nicht so begeistert davon war. Klar, sobald sie nur drei Worte mit ihren Freunden gewechselt hat, wird ihr Bild vom Governor ein gänzlich anderes sein und dass ist auch ihm klar. Liegt ihm etwas an ihr? Oder hält er sie nur nah bei sich, um sie im richtigen Augenblick gegen ihre Freunde einzusetzen? Psycho bleibt Psycho.

**Zusammengefasst:** Der Governor wird mit Andrea und Vorbereitungen auf einen weiteren Angriff zu tun haben. Im Gefängnis wird man die Lage bewerten und Entscheidungen treffen müssen: Ist es noch sicher oder nicht? Brauchen wir mehr Leute? So sehr ich mir wünsche, dass Tyreese bald wieder auftaucht, glaube ich es fast nicht für die nächste Episode.