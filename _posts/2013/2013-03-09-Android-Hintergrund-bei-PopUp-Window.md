---
layout: post
title: "Android: Hintergrund bei PopUp Window"
description: ""
date: 2013-03-09 
category: it
tags: [Android, Design, Coding]
published: true
comments: false
share: true
---

Möchte man in einer App keinen PopUp-Dialog verwenden, sondern lieber ein eigenes *PopUp Window* designen, muss man an vieles auch selbst denken. Ich wollte gerne meine eigenen PopUps, aber trotzdem ein Dialog-ähnliches Verhalten. Wenn das Fenster erscheint soll der Hintergrund auch tatsächlich in den Hintergrund rücken und trotzdem auf Klicks reagieren.

Was passiert standardmäßig? Der Hintergrund ist nach dem Öffnen von PopUp Fenstern nicht mehr anklickbar. Die darunter liegende Buttons der *Activity* sollen ja auch nicht reagieren bis der Benutzer eine Entscheidung getroffen hat und das PopUp Fenster wieder geschlossen wird. So weit, so richtig. Damit das aufpoppende Fenster sich aber wirklich vom Hintergrund abhebt, wäre es doch schön, wenn der Hintergrund blasser wird oder abgedunkelt, wie beim PopUp Dialog.     
Noch dazu wäre es auch praktisch, wenn ich nicht jedes Mal auf *Abbrechen* klicken müsste, sondern sich das Fenster wieder schließt, wenn man auf irgendeine Stelle auf dem Hintergrund klickt. 

Fallen mir solche Ideen ein, google ich meist, ob sich jemand schon mal Gedanken dazu gemacht hat. In diesem Fall war es so, dass ich erst einmal nach Informationen zum PopUp Window gesucht habe und unter anderen auf eine *How to blur the background when a popup window appears?* - Frage stieß. Gute Idee, aber die Antworten waren wenig hilfreich. Ich dachte also selbst über eine Möglichkeit nach und musste fast lachen, als mir die Lösung einfiel.

<figure>
    <a href="/images/2013-heurigen-popup-1600.jpg">
	   <img 
		  src="/images/2013-heurigen-popup-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Popup mit blassen Hintergrund</figcaption>
</figure>

## Ein Layout in einem Layout in einem Layout...

Wir haben zwei Ziele.     

1.	Der Hintergrund soll blass werden, also soll sich eine Art weißer Schleier auf ihn legen. Das schreit nach einem transparentem Hintergrund und zwar beim PopUp Fenster. Wir schnappen uns einfach die PopUp-XML Datei und umrahmen den bisherigen Code mit einem Eltern-Layout, welchem wir ein transparentes Weiß als Hintergrundfarbe geben (z.B. #8CFFFFFF) und dessen Höhe und Breite wir strecken lassen.      
Danach müssen wir im PopUpCode noch den *PopUpWindow* Konstruktor anpassen (auch *Match_Parent*). Und fertig ist der blasse Hintergrund.
2.	Für das zweite Ziel haben wir schon vorarbeitet geleistet. Wir wollen nichts anderes, als das der Hintergrund (im transparentem Weiß) auf Klicks reagiert. Also geben wir ihm einen *OnClickListener* und lassen ihn bei Klick das PopUp Fenster *dismissen()*.

Ein kleines Problem ist leider weiterhin, dass alle Elemente, die keinen *OnClickListener* haben, auf die Klicks des Eltern-Layouts reagieren. Aber die Buttons sind ausreichend groß, so dass der Benutzer selten daneben klicken dürfte.