---
layout: post
title: "iPhone Math für Apple's Bildungsinitiative"
description: "Satire über ein neues iPhone."
date: 2013-01-21 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

**Breaking News:** Nach Insiderinformationen plant Apple für dieses Jahr ein neues iPhone namens *Math*. Damit engagiert sich der Weltkonzern aus Cupertino weiterhin für die Bildung. Das *iPhone Math* soll einen 4.8 Zoll großen Bildschirm haben und sich besonders im Schulalltag gut einsetzen lassen. Schon im vergangenen Jahr veröffentlichte Apple die kostenlose Software iBooks Author, mit welcher man unter anderem Schulbücher erstellen kann.

Firmennahe Quellen berichten, dass der Name *Math* gewählt wurde, um die Menschen wieder für das gleichnamige Schulfach zu begeistern. Man rechne zu Beginn mit Zurückhaltung von Seiten der Kunden, *doch wird in wenigen Monaten der Begriff Mathematik positiv belegt sein und jeder Kunde sich fragen, wieso er früher schlecht in Mathe war* so eine anonyme Quelle.

Es ist davon auszugehen, dass weitere Sondereditionen folgen werden. Erste Bilder aus Fabriken in Foxconn zeigen die glitzernde Rückseite des neuen iPhones, auf welchem sich eine Abfolge von Zahlen befindet, womöglich Pi [^1]. Auf weiteren Bildern sind Formeln und kleine Skizzen zu erkennen. 

Unterdessen brodelt die Gerüchteküche auch in Südkorea. Ein Pressesprecher von Samsung konnte uns gegenüber die Gerüchte um das neue Flaggschiff-Smartphone *Galaxy Algebra* nicht bestätigen. Dagegen soll das neue Minitablet *Galaxy Note Novel* mit einem integrierten Füllhalter in wenigen Wochen in Produktion gehen. 

Oder so: [Daring Fireball](http://daringfireball.net/linked/2013/01/21/iphone-math).

[^1]: 3.14159265358979323846264338327950...
