---
layout: post
title: "Jahresrückblick 2012"
description: "Mein Rückblick auf ein durchwachsenes Jahr 2012."
date: 2013-01-02 
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
---

Nachdem ich auf einem Blog eine Art Jahresrückblick gelesen hatte, wollte ich auch einen schreiben. Keine Fragen, das vergangene Jahr 2012 war sehr durchwachsen. Insgesamt würde ich es eher negativ bewerten, obwohl es auch großartige Momente gab.

## Beruflich &amp; Privat

In der ersten Hälfte des Jahres stand das Bachelorprojekt im Fokus. Aus verschiedenen Gründen endete der ganze Spaß in einem kleinen Desaster und wird von mir noch ausführlich diskutiert werden. Nach einer sommerlichen Verschnaufpause suchte ich mir meine verbleibenden Kurse zusammen und ärgerte mich etwas über die Bedingungen, 2 Kurse aus diesem Bereich, einer von dort und zwei…. Nachdem zwei Drittel des Semester nun auch schon vorbei sind, bin ich aber ganz zufrieden mit der Auswahl. Es läuft gut und macht Spaß.

Meine Freundin wohnte hier auf dem Campus in einer anderen 2er-WG und deshalb pendelte ich immer hin und her. Abendbrot und Schlafen war bei ihr, danach ging es früh zur Uni und von dort zu mir. Oft aßen wir beide Abendbrot zusammen, danach ging ich zu mir und arbeitete noch ein wenig bevor ich wieder rüberging zum Schlafen. Eine blöde und vor allem unbequeme Situation.      
Wir hatten beide Mitbewohner die okay waren, aber für uns auch nicht die besten Freunde. Als mein Mitbewohner im Sommer seinen Auszug ankündigte, war das die Chance! Wir wohnen zwar nicht mehr sehr lange in Potsdam, 2013 ist das letzte Jahr, aber es würde sich trotzdem lohnen. Ende August zog sie zu mir und das war die beste Entscheidung des gesamten Jahres.  

## Dazwischen

In 2012 begannen einige Projekte, die diese Jahr hoffentlich ihren Abschluss finden bzw. veröffentlicht werden. Dabei gab es aus organisatorischer Sicht einige Stolpersteine, die nicht hätten sein müssen, aber insgesamt sind mir die Projekte wichtig und machen mir vor allem auch Spaß.      
Die Informatik ist ein großes Gebiet und vor einem Jahr war mir noch nicht ganz klar in welche Richtung meine spätere Berufstätigkeit gehen soll. Mit den Projekten kristallisiert sich das langsam heraus. Ich mag UI Design sehr gerne und möchte das am liebsten im zukünftig wichtigen Bereich einsetzen: Mobil und Internet. 

Dann noch einige negative Aspekte. Es gab einen Todesfall in der Familie. Mein Hiwi-Job ist zu Ende, was einerseits mehr Zeit für andere Dinge und andererseits finanzielle Einbußen bedeutet. Neben der katastrophalen Gruppenarbeit im Bachelorprojekt gab es leider keinen Ausgleich in Form eines anderen Gruppenprojekts. Seit vielen Semestern hoffe ich auf ein tolles Projekt, neben dem Studium, aber irgendwie hat niemand Zeit und Lust.       
Ich habe eine ganz gute Idee und wollte schon fast am HPI-Businessplan-Wettbewerb teilnehmen, aber ich habe schlicht und ergreifend niemanden mit dem ich ein Projekt verwirklichen kann. 

## 2013

Was bleibt für 2013 übrig? Zuerst einmal die restlichen Kurse bestehen und den Bachelor beenden. Noch bevor das geschafft ist, muss ich etwas für die Zeit nach dem Studium suchen, egal ob ein Masterstudium, Job oder Duales Studium. Meine Freundin hat konkrete Pläne für 2014 und ich muss nun schauen, was ich im Süden des Landes finde.      
Ich hoffe die Projekte laufen weiter und wie oben schon erwähnt, gehen an den Start. Als ToDo würde ich einfach sagen: Mehr Lesen und mehr Gitarre spielen. Hier warten noch viele Bücher auf mich und Instapaper ist auch vollgestopft. Und die Gitarre muss einfach wieder benutzt werden. Und Sport muss auch mal wieder sein!

Doch am meisten wünsche ich meiner Freundin und mir mehr Ruhe und Energie. Viel zu oft sind wir beide am Wochenende erschöpft von der Woche. Als Student hat man ständig was zu tun und so langsam macht das keinen Spaß mehr. 

Das wird hoffentlich ein gutes Jahr.