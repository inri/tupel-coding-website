---
layout: post
title: "Keine Linksammlung von mir"
description: "Ich werde auf meinem Blog hier keine Linksammlung präsentieren. Tut mir Leid."
date: 2013-02-20 
category: blog
tags: [Tupel Coding]
published: true
comments: false
share: true
---

Hiermit entschuldige ich mich bei den werten Lesern dieses Blogs für eine Sache, von der ihr gar nichts wusstet. Es wird in Zukunft doch keine Linksammlungen bei mir geben.

In den letzten drei Jahren habe ich meine Filterbubble ziemlich gut auf meine Interessen angepasst. Aus diversen Quellen beziehe ich Nachrichten und Neuigkeiten, die mich interessieren. Ab und zu habe ich bei Twitter Links verteilt, aber so richtig viele waren es nie. 

Ein neues Jahr, neue Gedanken. Warum nicht wöchentlich oder monatlich gute Links auf meinem Blog posten? Als Vorbild wären hier fefe und [Felix Schwenzel](http://wirres.net/) zu nennen, die tun das fast jeden Tag. Oder auch der Bildblog. Also begann ich vor einigen Wochen alle interessanten Links auf Twitter zu setzen und nahm mir vor sie hier im Blog noch mal zu verlinken und etwas mehr als 140 Zeichen als Kommentar zu formulieren. 

Wochen und Monate vergingen, doch hier passierte nichts. Zum einen kostet das Unterfangen etwas Zeit und zum anderen bin ich nicht von dem Nutzen überzeugt. Sind alte Links noch interessant? In den letzten Tagen habe ich weitaus weniger Links auf Twitter gesetzt, denn selbst dort kostet es Zeit. 

Vielleicht ist es besser in Zukunft bei meinen normalen Einträgen zu bleiben und nur wirklich interessante Themen zu diskutieren. Gute Links werden euch bestimmt schon von anderer Quelle zugespielt. Oder?

Außer ihr wollt es unbedingt!