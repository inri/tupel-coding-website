---
layout: post
title: "Es ist nicht alles wie man denkt"
description: "Eine kurze Geschichte über die menschliche Natur."
date: 2013-02-06 
category: blog
tags: [Literatur, Persönlich]
published: true
comments: false
share: true
---

**Eine Kurzgeschichte.**

Eines Tages vor einigen Monaten begann es. Ich bog gerade um die letzte Kurve in den Hof, meine Haustür schon in Sichtweite und fühlte mich auf einmal beobachtet. Mir gegenüber im zweiten Stock, stand ein Student an einem Küchenfenster, rauchte gemütlich eine Zigarette und schaute mich an wie ich zu meiner Tür lief. Das war insofern untypisch weil überhaupt sehr selten die Bewohner in ihren Zimmern zu sehen sind. Erst recht beobachtete niemand den Innenhof. Dieses Szenario wiederholte sich in den darauffolgenden Monaten. Nicht gerade selten stand er rauchend bei offenem Fenster und beobachtete die Vorbeigehenden. Es ist ein komisches Gefühl, wenn man von jemanden regelrecht gemustert wird. Meist mehrmals in der Woche ging ich die letzten fünfzig Meter zu meiner Tür unter Beobachtung. 

Der Student war etwas kräftig, hatte etwas längere Haare und kniff seine Augen immer etwas zusammen. Er ließ einen auch selten aus dem Blick, zumindest hatte man das Gefühl. Und es war genauso beim Herausgehen, nur dass man den Blick dann im Rücken spürte. Natürlich kann jeder Mensch andere Menschen von seinem Küchenfenster aus beobachten. Aber ich fragte mich doch manchmal, warum er nicht von seinem Zimmer aus rauchen konnte. Oder warum er so frontal am Fenster stehend, die Kommenden und Gehenden anstarren muss.      
Gegrüßt hat er auch nie. Oder hat er den Gruß von mir erwartet? Manchmal standen seine Freunde oder Mitbewohner bei ihm, dann starrte er entweder nicht so sehr oder sie starrten zusammen. Das war aber selten der Fall. 

Er störte mich nicht in besonderer Weise, dass ich ihm hätte Verwünschungen an den Kopf schmeißen wollen, aber er nervte mich schon.  Die Feststellung *Da steht er wieder rauchend und mustert mich.* löste keine Freude bei mir aus.      
Nun ging ich heute hinaus um mir einen Döner am Bahnhof zu kaufen. Es nieselte draußen und schon vom Fenster im Treppenhaus sah ich ihn. Diesmal stand er nicht am Fenster, sondern mit einem großen Rucksack auf dem Rücken an seiner Haustür und rauchte. Hier unten wirkte er etwas kleiner, aber er starrte mich genauso an wie immer. Ich ging aus der Tür heraus und würdigte ihm keines Blickes. 

Als ich meinen Döner bezahlte und gerade zurückgehen wollte, sah ich ihn kommen. Er lief sehr langsam über die Straße, die anderen Menschen gingen schnell an ihm vorbei. Die runde Kugel am Ende seines weißen Gehstocks tastete sich Zentimeter für Zentimeter vorwärts. Sein Augen waren zusammengekniffen wie immer, aber sie starrten nicht, sie blickten in keine Richtung. 

Er tastete sich zum Zaun und lief ihn vorsichtig entlang, an mir vorbei.

Und ich schämte mich.