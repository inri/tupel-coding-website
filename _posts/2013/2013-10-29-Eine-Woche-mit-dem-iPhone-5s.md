---
layout: post
title: "Eine Woche mit dem iPhone 5s"
date: 2013-10-29 13:00:00
category: it
tags: [Apple, iPhone]
published: true
comments: false
share: true
---

Reviews über das neue iPhone gibt es schon viele und das schon seit einigen Wochen. Ich bin nicht gerade früh dran, das weiß ich. Die folgenden Beobachtungen finden unter dem Aspekt des Wechsels vom iPhone 4S zum iPhone 5s statt. 

### Vom 4S zum 5s

Vor ziemlich genau zwei Jahren habe ich mein ersten iPhone gekauft, das iPhone 4S. Es war und ist immer noch ein tolles Gerät. Schon nach wenigen Wochen merkte ich damals, dass ich es viel öfter und für viel mehr Dinge nutzte als sein Vorgänger, ein HTC Desire. iOS mitsamt den ganzen tollen Apps und nicht zuletzt auch das Ökosystem von Apple gefielen mir richtig gut. Dass mein nächstes Smartphone ein iPhone wird, war beschlossene Sache.

Nun liegt das 5s neben mir. Ein wunderschönes und vor allem leichtes Gerät. Wenn ich es auf zwei Attribute reduzieren müsste, dann wären das seine Geschwindigkeit und sein Gewicht.   

### Gewicht &amp; Größe

Wie auch schon das 5er ist das iPhone 5s wesentlich leichter als das 4 und 4S. Dazu kommt die Vergrößerung bzw. Verlängerung des Bildschirms und daraus entsteht eine seltsame Mischung. Wenn ich jetzt nach einigen Tagen mal wieder mein 4S in die Hand nehme, fühlt es sich *pummelig* an. Also einerseits schwerer und andererseits klein und durch die Glasfronten, die es einige Millimeter breiter machen, auch dicker. Es ist ein kurioses Gefühl.

An das geringe Gewicht muss ich mich erst noch gewöhnen. Mein 4S konnte ich nach einem Jahr blind jonglieren, das 5s fühlt sich noch unbekannt an. Ich nehme es immer ganz vorsichtig in die Hand, erst recht beim Fotografieren.

Ich sehe den 4 Zoll Bildschirm sehr kritisch. Er ist okay. Man erreicht die obere Ecke mit den Daumen gerade so, je nachdem wie man das iPhone in der Hand hält. Ich nutze den kleinen Finger bspw. als Halterung, doch dadurch kann sich der Daumen nicht ewig strecken.    
Falls Apple nächstes Jahr ein noch größeres iPhone herausbringt, wäre das eine große Enttäuschung. Dann braucht uns John Ive nicht erklären, dass es noch was mit Benutzbarkeit zu tun hat. Das wäre Blödsinn [^1]. 

Noch kann man das iPhone aber gut bedienen und der längere Bildschirm ist für die Inhalte klasse. Ich lese sehr viel auf meinem iPhone und bin jedes Mal dankbar.

### Geschwindigkeit

Benutzer die vom 5er zum 5s wechseln, merken sicherlich auch einen Zuwachs in der Geschwindigkeit, aber für mich ist das 5s eine Rakete. Wirklich alles ist unglaublich schnell und vor allem viel schneller als auf dem 4S.    
Als Dienstagnacht iOS 7.0.3 herauskam, habe ich nach dem Update bei meinem iPad mini und alten iPhone die Bewegungsreduzierung aktiviert. Das war schon ganz angenehm. Doch zum Wochenende habe ich es auch beim 5s gemacht und dann spürt man erst einmal richtig den Unterschied.

Die Effekte sind nett, aber ich brauche sie nicht. Ohne sie öffnen sich Apps quasi sofort, ohne Laden. Auch ansonsten findet sich nirgendwo im System auch nur die Spur eines Rucklers. Alles lädt zügig und ist sofort bereit. Wenn es doch mal länger dauert, werden irgendwelche Daten aus dem Netz geladen. Doch sind die Daten erst im iPhone geht es ab.

### Touch ID

Der Fingerabdrucksensor ist ein unglaublich tolles Feature. Und er sichert mein iPhone sehr gut, aber ich bin ja auch kein Spion. Nein ernsthaft, genau so stellt man sich den Nutzen eines solchen Sensors vor. Ich drücke den Homebutton mit meinem Daumen hinunter, lasse ihn kurz auf dem Button und einen Wimpernschlag später sehe ich den Homescreen vor mir.

Der Sensor ist dabei nicht perfekt, es kommt öfter vor, dass ich meinen Finger neu positionieren muss. Den Code nach dem dritten Fehlversuch musste ich bisher aber nur sehr selten eingeben.    
Das hat mir übrigens bewiesen, dass der Fingerabdruck-kopieren-*Hack* theoretisch ein Hack ist, aber praktisch keine Gefahr darstellt. Ich habe den Eindruck, dass der Sensor nur einige Teile meines Daumen gescannt und gespeichert hat. Es nun zu schaffen, mit einer Kopie dieses Abdrucks, in nur drei Versuchen die richtigen Stellen zu finden, dürfte enorm schwierig sein.   
Dann würde ich als Hacker doch lieber den Code herausfinden.

### Weiteres

Das spacegrau der Rückseite hat meiner Meinung nach die gleiche Farbe wie der Metallrahmen.

Die Kamera ist schnell und die SloMo Funktion ziemlich witzig. Noch habe ich es zeitlich nicht geschafft einen kurzen Film aus meinen vielen SloMos zu schneiden, aber das kommt noch.

Letztes Jahr gab es Gejammer über Apples Umstellung auf den Lightning Anschluss. Dieses Jahr? Ich habe exakt nichts gehört. Und warum? Weil er klasse ist. Wenn man ein Jahr lang den alten Connector und den neuen zusammen benutzt (ich, wegen des iPad minis), merkt man erst so richtig wie unschön der alte war.    
Mir tun die armen Würste leid, die demnächst mit diesem irrsinnigen MikroUSB auskommen müssen.

Die Akkulaufzeit konnte ich noch nicht so wirklich testen. Ich lade es in der Nacht, nutze es tagsüber man hierfür, mal dafür und abends sind noch so 30% übrig. Für mein 4S hatte ich noch eine Dockingstation, deshalb war es ständig am Strom.

Die eingebauten Boxen sind angenehm laut. Vielleicht sogar lauter als beim 4S.

### Kleines Fazit

Es ist nicht so, dass ich etwas anderes erwartet hätte. Das iPhone 5s ist ein tolles Smartphone und mit Sicherheit das **beste** zur Zeit. Ja, ich könnte schreiben &quot;Das beste für mich&quot;, aber das werde ich nicht tun. Deal with it. 

Demnächst gibt es weitere Erkenntnisse, wenn ich noch was neues entdecke.

[^1]: Schizophren ist der angebliche Druck der auf Apple lastet, weil alle anderen Smartphone-Hersteller 4,x Zoll Bildschirme in ihre Flaggschiffe verbauen. Nicht alle verkauften 4,x Zoll Smartphones zusammen kommen auf das Volumen von verkauften iPhones. Wieso liest man nirgendwo, dass die ihre Smartphones mal kleiner machen sollten?