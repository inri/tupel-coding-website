---
layout: post
title: "Ein Fazit zum Seriensommer 2013"
date: 2013-11-02
category: medien
tags: [Serie]
published: true
comments: false
share: true
---

Ende August habe ich fünf neue Serien vorgestellt. Ich schrieb die Reviews meist nach dem Anschauen einiger Episoden. Aber wie sieht mein Fazit nach einer Staffel aus? 

### The Bridge

Die Qualität der ersten Episoden konnte *The Bridge* mühelos halten. Ein wirklich fantastischer Krimi. In der Tat folgt eine weitere Staffel und die Storyline ist auch mit Absicht so angelegt. Wer also erwartet, dass alle Handlungsstränge in der ersten abgeschlossen werden, den muss ich enttäuschen. Wobei es nicht enttäuschend ist, denn teilweise gibt es schon Schlusspunkte und der &quot;Hauptfall&quot; wird abgeschlossen. 

Ich freue mich auf die nächste Staffel, besonders auch wegen Diane Kruger und Demian Bichir, zwei hervorragende Schauspieler.

### Orange is the New Black

Springen sie bitte zwei Absätze nach oben und lesen sie den ersten Satz noch einmal. Auch *OitNB* ist ein tolles Drama mit guten Schauspielern. Das Finale fand ich super, auch weil es ein wenig düster war als der Grundtenor der Serie. Ich bin mir sicher dass die nächste Staffel mindestens genauso gut wird. 

### Under the Dome

Zugegeben, sie macht Spaß und ist handwerklich gut. Das soll heißen die einzelnen Episoden sind spannend und die Schauspieler gut. Aber auch wenn die Hauptgeschichte stets Fortschritte macht, kommt ein *Lost*-Gefühl auf. Am Ende wird die Serie eingestampft und dutzende Fragen unbeantwortet bleiben. Bitte belehrt mich eines besseres!

Das ist meines Erachtens der Größe Schwachpunkt US-amerikanischer Serien. Zuerst produzieren sie viele Piloten, danach eine Staffel und je nach Zuschauerinteresse weitere Staffel. Das mag bei weniger storylastigen Serien okay sein (*Greys Anatomy*, *CSI blablabla*, usw.), aber wenn die Geschichte im Mittelpunkt steht, wird eine solche Arbeitsweise gefährlich. 

### Siberia

Ich bleibe bei meinem Hauptkritikpunkt aus dem ersten Review. Die Serie kann sich nicht entscheiden was sie sein möchte.   
In der letzten Hälfte der Staffel gibt es noch einige abgefahrene Ereignisse, aber kurz danach verhalten sich die Spieler wieder als wäre nichts passiert.

Es ist schon eine ganz interessante Serie, aber sie macht es sich selbst schwer. Ich hätte die Kameramänner komplett mitgefilmt, also das Filmen von Filmaufnahmen für die Realityshow. Die Show-Aufnahmen hätte man auch benutzen können, dann aber entsprechend sichtbar. Meist wären aber Spieler und Kameramänner gemeinsam im Bild und erleben verrückte Sachen. Aber eben so, dass auch die Kameramänner mal durchdrehen können und nicht immer perfekte Aufnahmen machen müssen.

Werde ich die nächste Staffel anschauen? Eher nicht.    
Doch der Titelsong (russischer Männerchor) ist große Klasse. [Anhören!](http://www.youtube.com/watch?v=TJ41hEUPYr0)

### Do No Harm

Mir gefiel der Ansatz und auch die Ausführung von *Do No Harm* ganz gut. Auch das Finale war schön und hat die Geschichte abgerundet. Leider bleibt ein sich nie auflösender Cliffhanger, denn die Serie wurde ja schon nach den ersten drei Episoden eingestampft.   
Sehr schade.

