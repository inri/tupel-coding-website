---
layout: post
title: "iPhone und Vodafone, Teil 2"
date: 2013-10-19 00:01:00
category: blog
tags: [iPhone, Persönlich]
published: true
comments: false
share: true
---

Es folgt ein kurzer Nachtrag vom Beitrag der letzten Woche. Mir ist absolut schleierhaft wie diese Mobilfunkunternehmen überhaupt noch funktionieren können. Also dass die nicht längst unter dem Druck ihrer Unfähigkeit zusammenbrechen. Irgendwie stehen die Kartenhäuser immer noch, fast ein Wunder.

Es ist gerade Freitagabend, exakt drei Wochen nach meiner iPhone Bestellung bei Vodafone, und ich liege im Bett und starre an die Decke. Das tue ich nicht freiwillig, nein. Neben mir liegt eine wunderschöne Frau, die ich sehr begehre, doch sie rührt sich nicht und ich kann sie nicht berühren. Ich werfe ihr sehnsüchtige Blicke zu, aber sie reagiert nicht. Sie kann auch gar nicht reagieren und ich kann mich nicht bewegen. Vodafone zwingt uns zur Passivität.   

Die Frau heißt eigentlich iPhone 5s und die Barriere zwischen uns nennt sich *Aktivierung des iPhones durch eine SIM Karte*. 

### Mysteriöse Lieferzeiten

Zur Erinnerung, ich bin letzte Woche auf das 64GB iPhone gewechselt, denn dieses sollte bis zum 18.10. lieferbar sein. Auf das spacegraue 32GB hätte ich bis November warten müssen und bis dahin fleißig meine Gebühren für einen Vertrag bezahlt, den ich ohne SIM und Smartphone gar nicht nutzen kann. Mit 50€ Gutschrift als Rückenwind tätigte ich den besagten Wechsel um hoffentlich bald ein iPhone in den Händen halten zu können.   

Es war also Dienstag (der 15.10.) und mein Bestellstatus unverändert (32 GB iPhone im November). Jedoch die **Lieferzeiten** hatten sich mittlerweile geändert, sowohl bei dem 64GB als auch dem 32GB iPhone. Ich rief bei Vodafone an und fragte nach Informationen.    
Ja, sie könne mir nichts konkretes sagen, aber in ihrer Übersicht soll das 64GB iPhone diese Woche ausgeliefert werden und ich solle mich melden, wenn ich bis Donnerstag nichts von Vodafone höre. Das 32GB iPhone steht angeblich immer noch auf November, obwohl Vodafones Website etwas anderes sagt. Kurios. Oder auch nicht.

### Pakete packen

Mittwochmorgen erhielt ich eine freudige Email von Vodafone: iPhone ist da, Paket wird **voraussichtlich** am gleichen Tag verschickt. Dass es für Vodafone eine so schwierige Aufgabe ist ein iPhone in ein Paket zu stecken, konnte ja niemand ahnen. Der Donnerstag verstrich und ich sah beim Post holen DHL. Doch niemand klingelte. Eine Paketnummer wurde mir von Vodafone nicht mitgeteilt, also rief ich wieder bei Vodafone an und fragte nach wo mein verdammtes iPhone bleibt. Ja, es wurde in die Post gegeben und nun hatte ich auch eine Paketnummer. 

Es kam dann kurz vor 12:00 und ich war gespannt was für tollen Inhalt es haben wird. Immerhin hat es 1,5 Tage gedauert das Ding zu verpacken und zu verschicken. Doch dann die Ernüchterung: Dressierte Affen hätten ein solch teures Gerät nicht schlechter verpacken können. Das iPhone wurde zusammen mit zwei A4 Blättern ins Paket gerotzt, ohne Polsterung. Wow.

<figure>
	<a href="/images/2013-vodafone-paket-800.jpg"><img src="/images/2013-vodafone-paket-800.jpg" alt="Die Große Einpackkunst der Vodafone Logistik"></a>
	<figcaption>Ich habe dem Paket nichts entnommen. Darin war keine Art von Polsterung.</figcaption>
</figure>

### Anschauen ja, berühren nein

Doch damit ist mein Vodafone Abenteuer nicht beendet. Natürlich war keine SIM im Paket (**Herrgott!!!**). Ich rief wieder mal bei Vodafone an und man erklärte mir, die SIM kann erst nach der PostIdent Feststellung verschickt werden. Genau das tat der DHL-Mensch heute. Also muss ich vermutlich bis morgen auf meine SIM warten? Okay, kein Problem. Dachte ich zuerst.

Dann packte ich freudig mein iPhone aus, schaltete es an und wollte schon meine alte MikroSIM aus dem 4S herausfummeln, als mir schlagartig klar wurde: Das wird nix. Seit dem iPhone 5 gibt es NanoSIMs. Die könnte man sich ja noch selbst schneidern, aber Vodafone hat (noch) SIMLock in ihren iPhones. Ohne Vodafone-SIM, keine iPhone Aktivierung. Ohne Aktivierung kann man exakt **nichts** mit dem iPhone machen. Na super.

Ich rief ein letztes Mal bei Vodafone an und wollte eine konkrete Aussage zur Versendung meiner SIM. Ja, sie wurde mittags in die Post gegeben. Warum man bei 3 Wochen Wartezeit nicht hätte das PostIdent Verfahren schon eher durchführen und mir die SIM entsprechend eher zuschicken können? Weil ich das iPhone gewechselt habe. Scheinbar ist das ein solch komplexer Vorgang, dass man nur das Smartphone verschickt und mit dem Rest wartet. Ich habe es nicht verstanden und will es auch gar nicht verstehen.

### Fazit

Halten wir die einzelnen *Fails* mal fest: 

* Angabe falscher Lieferzeiten   
* Rechnungsstellung obwohl keine vertraglichen Leistungen erbracht wurden (weder SIM noch Smartphone!)   
* 1,5 Tage vertrödeln um ein teures Gerät saumäßig zu verpacken   
* SIM erst nach PostIdent Verfahren verschicken und...   
* ... das PostIdent Verfahren erst bei Smartphone Lieferung durchführen   

Ich bin immer vorsichtig, wenn negative Meinungen geäußert werden. Aber mein Abenteuer mit Vodafone wurde mir von verschiedenen Seiten vorausprophezeit.   
Das kann ja noch ein Spaß werden.