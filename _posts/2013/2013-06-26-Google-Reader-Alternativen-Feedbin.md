---
layout: post
title: "Google Reader Alternativen: Feedbin"
description: ""
date: 2013-06-26 
tags: [RSS]
category: it
published: true
comments: false
share: true
---

Feedbin ist eine eher einfach daherkommende Alternative. Also im Gegensatz zu Feedly wohlgemerkt, zum Google Reader finden sich durchaus Parallelen. 


Das interessante an Feedbin sind die Art der Einnahmen: Es werden pro Monat 2$ bzw. 20$ pro Jahr verlangt. Man schließt aber keinen Vertrag ab, sondern kann jederzeit kündigen.       
Ich persönlich finde das wesentlich besser als die ominöse Kostenlos-Kultur bei Google oder Facebook beispielsweise. Ich bezahle lieber direkt für eine Leistung und nicht gerne indirekt über meine Daten. 

## Usability

Wie man die eigenen Abonnements importieren kann, erklärt Feedbin auf einer Hilfe-Seite. Einigen mag das Herunterladen der Google Reader umständlich erscheinen, aber das macht man ja auch nur einmal. Die Feeds werden brav übernommen und auch die Ordnerstruktur und *Starred Items* bleiben bestehen. 

Im Prinzip besteht Feedbin aus 3 Spalten. Links die Feeds, daneben die Artikelliste eines Feeds und das größte Fenster zeigt den Inhalt, also den Text. Und genau da entsteht z.b. ein Problem bei größeren Bildschirmen. Feedly hat erkannt, dass sich Texte auch auf digitalen Bildschirmen besser lesen lassen, wenn die Seite nicht zu breit ist, siehe Tageszeitung. Feedbin dagegen klatscht den gesamten Text in das breite Fenster. Aufs Lesen habe ich gleich viel weniger Lust.

Es gibt auch keine Art Vorschau-Ansicht wie bei Feedly. Wenn ich einen Artikel auswähle, ist er danach als gelesen markiert. Google Reader hat das genauso gemacht und das war genauso unschön. Einfach und überschaubar sind auch die Optionen: *Read / Unread*, Favorisieren (*Starred*) und die Anzeige des Inline des Artikels.

Ein guter Grund auf Feedbin zu setzen ist die Integration in der Reeder App. Da gibt es nicht sehr viel mehr zu sagen, außer dass zumindest bei mir Reeder langsamer geworden ist. Eventuell werden die nächsten Versionen besser oder ein neues iPhone behebt dieses Problem. Ich bin gegenüber einer neuen App durchaus sehr aufgeschlossen. Ach ja, für die iPad-Version der Reeder App gibt es noch keine Integration.

Neben Reeder ist Feedbin in weitere zahlreiche Apps integriert. Und die öffentliche API ist sicherlich ein großer Pluspunkt für Entwickler.

## Fazit

Würde ich direkt von Google Reader kommen und hätte Feedly nicht probiert, wäre ich vermutlich ganz zufrieden mit Feedbin. Es ähnelt dem alten Reader und bietet mit den zahlreichen Integration eine gute Auswahl an Drittanbieter-Apps. Doch leider hat auch die Konkurrenz einige nette Bonbons.        
Ich werde Feedbin im Auge behalten, doch vorerst möchte ich nicht mehr als einen Monat investieren. Ich hätte gedacht, dass bis zum Ende von Google Reader *mehr* passiert. Denn das ist der *Nachteil*, wenn man eine transparente Preisstruktur hat, die Kunden möchten für ihr Geld etwas sehen. Und bei einigen tausend Abonnenten dürfte so einiges zusammenkommen.s