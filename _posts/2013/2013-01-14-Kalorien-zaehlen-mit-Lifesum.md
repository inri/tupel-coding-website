---
layout: post
title: "Kalorien zählen mit Lifesum"
description: "Seit einer Woche zähle ich meine Kalorien mit dem Dienst Lifesum."
date: 2013-01-14 
category: blog
tags: [App, Ernährung]
published: true
comments: false
share: true
image: 
  feature: 2013-lifesum-lg.jpg
  credit: Lifesum
  creditlink: http://lifesum.com
---

In einem Seminar bauen wir aktuell eine Webapp mit der man Kalorien zählen kann. Genauer gesagt soll man damit einfach und schnell die gegessenen Speisen und Getränke in eine Art Essen-Tagebuch aufnehmen können. 
  

Zu Beginn des Projekts dachten wir, dass es einen solchen Dienst noch nicht gäbe. Ich hatte nicht zu intensiv gesucht, aber fand in der Tat nichts. Doch ich hatte nicht gut genug gesucht. Einen solchen Dienst gibt es schon und der ist sogar ziemlich gut. Und eine App hat er auch.

Durch Zufall stieß ich auf [Lifesum](http://lifesum.com) [^1]. Die bieten genau das an, was wir programmieren wollen. Vieles ist genau so oder so ähnlich, wie ich es machen würde, aber definitiv im Rahmen eines Seminars niemals schaffen kann. Wir reden hier auch von UI und gutes UI geht nicht mal eben in wenigen Stunden.      
Ich habe mich bei denen angemeldet und verfolge meine Ernährung nun seit einer Woche sehr genau. Ich hätte es nicht gedacht, aber nach den wenigen Tagen habe ich schon eine Menge gelernt und erfahren. Auch über mich.

## The Hunger Games

Ich muss über meine eigene Überschrift schmunzeln, doch sie trifft den Nagel auf den Kopf. Mein Ziel ist 1 kg Gewichtsverlust pro Woche bzw. nicht mehr als 2000 kcal pro Tag zu mir zu nehmen. Für den Durchschnittsmenschen werden stets 2000 kcal als Tagesbedarf angegeben, aber was bedeutet das genau? Wer von sich denkt, er wüsste ja so ungefähr welche Nahrungsmittel wie sehr gesund und kalorisch sind, der irrt sich vermutlich. Ich habe die bittere Erfahrung gemacht, dass 2000 kcal verdammt wenig sind. Immer öfter knurrt mein Magen. Doch da muss er durch.

Das erste Mal war ich über meine morgendliche Stulle mit Nutella erschrocken (300 kcal). Isst man noch eine zweite Stulle und trinkt ein Glas Milch, hat man mal eben fast 2/3 des täglichen Bedarfs weggefuttert. Müsli kommt demnächst, das kann ich noch nicht so richtig einschätzen. Aber bedeutend weniger wird es nicht, wobei Fett und Zucker niedriger sein sollte.      
Dass Süßigkeiten heftig sind, ahnte man ja. Bei uns stapeln sich noch die Weihnachtsnaschereien und sie werden noch eine Weile existieren. Mehr als drei Merci Riegel sind kaum drin. Dabei trinke ich tagsüber nur Wasser. Würde ich täglich ein Glas Cola wegschlürfen, wäre das schon eine kleine Mahlzeit und ich würde die 2000 kcal gnadenlos sprengen. Obst und Gemüse sind zum Glück Leichtgewichte, aber sie stillen den Hunger für einen wesentlich kürzeren Zeitraum. 

Interessant ist auch die Verteilung der einzelnen Nährwerte. Ich nehme immer noch zu viel Fett zu mir, bei vier von sechs Tagen überschritten, dafür könnten es mehr Eiweiß und Kohlenhydrate sein. 

Die App kann den Barcode auf Nahrungsmitteln scannen und das macht das Tagebuch führen sehr angenehm. Leider klappt das nicht bei allen Lebensmitteln und besonders bei Fertig-Essen wie beispielsweise Mensaessen und Döner, weiß ich gar nicht genau, was da eigentlich drin ist. Da wird man wohl in den nächsten Jahren nichts weiter machen können. In zehn Jahren haben wir vielleicht coole Nahrungsscanner. 

Ob ich pro Woche tatsächlich 1 kg abnehmen werde? Diese Woche geht auch der regelmäßige Sport wieder los und da ist ein Partner von Lifesum App RunKeeper. Das, was ich dann erlaufe, wird automatisch meinem Ernährungs-Tagebuch zugerechnet. Ich bin gespannt und werde weiter berichten.

[^1]: Früher hieß Lifesum noch ShapeUp Club. Eine durchaus sinnvolle Umbennung.
