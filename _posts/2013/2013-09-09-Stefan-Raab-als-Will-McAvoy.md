---
layout: post
title: "Stefan Raab als Will McAvoy"
date: 2013-09-09 10:00:00
category: medien
tags: [Politik, TV]
published: true
comments: false
share: true
---

Stefan Raab ist auf &quot;seinem&quot; Gebiet unglaublich erfolgreich. Seine Musik war erfolgreich, seine Show(s) war(en) erfolgreich, er war indirekt beim Eurovision Song Contest erfolgreich und meistens wird alles, was Raab anfasst, irgendwie erfolgreich.   


Seit einigen Monaten wagt er sich auf ein neues Gebiet: Politik. Mit seiner Show &quot;Absolute Mehrheit&quot; lädt er Politiker zum Diskutieren ein und versucht mit einem neuen Konzept eine interessante Alternative zu den ewig-gleichen Polit-Talkshows zu schaffen.

Will McAvoy ist ein fiktiver Nachrichtensprecher und eine der Hauptfiguren der überaus grandiosen Serie &quot;The Newsroom&quot; von Andy Sorkins. McAvoy ist genau die Art von Nachrichtensprecher, die ich in Deutschland einfach vermisse. Ab und zu fällt mal jemand mit scharfen Fragen und kritischen Nachhaken auf, aber es gibt nicht DIE eine Person. Am ehesten denkt man vielleicht an Oliver Welke und die Heute-Show ist ja auch nicht unkritisch, doch mir ist sie insgesamt zu oberflächlich und da so gut wie nie direkter Kontakt zu Politikern besteht, ein Stück weit beschränkt.

##Raab beim Kanzler-Duell

Vor einer Woche konnte man das sogenannte Kanzler-Duell zwischen Merkel und Steinbrück zu bestaunen. Ich war im Urlaub und holte es nach (deshalb auch der Beitrag erst jetzt).   
Die beiden &quot;Kontrahenten&quot;wurden von vier Moderatoren befragt und neben Anne Will hat Stefan Raab (m.E.) die beste Figur abgegeben. Er wirkte nicht so verkrampft und steif wie die anderen, war etwas frech und stellte relativ kritische Fragen.   
Oder anders: Wenn Raab begann eine Frage zu formulieren, freute man sich darauf.

Wieso können wir das nicht öfter haben? Stefan Raab, wieso machst du das nicht öfter? In der Musik und Comedy hast du alles erreicht und wirst ein Vermächtnis hinterlassen. Warum nicht auf die &quot;alten Tage&quot; den Politikbetrieb umkrempeln?   
Du hast doch nichts zu verlieren.

Such dir ein gutes Recherche-Team, schnapp dir jeden Politiker und nimm sie alle in die Mangel. Mit etwas weniger Klamauk und mehr Schärfe. Die Zuschauer sollen deine Interviews anschauen wollen weil du keine inhaltsleeren Phrasen zulässt, sondern die Politiker zu richtigen Aussagen drängst. Dass sie sich vielleicht nicht wohl und eher bedrängt fühlen, ist dabei völlig egal. 

Ich höre viel zu selten kritische Fragen aus anderen Blickwinkeln.   
Viel zu selten stottern Politiker im Fernsehen weil sie von einer Frage überrumpelt wurden.    
Unsere Medien sind fast nur noch ein Einheitsbrei aus den immer selben Meinungen. Die Menschen müssen wieder informiert werden, mit richtigen Debatten - das hat auch Will McAvoy gefordert. 

[Ich möchte eines Tage eine solche Rede auch bei uns hören und sie soll nicht fiktiv sein.](http://www.youtube.com/watch?v=1zqOYBabXmA)

Los Stefan! Wer sonst wenn nicht du?