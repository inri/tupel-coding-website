---
layout: post
title: "Antithese zwischen Musik und Geschichte"
date: 2013-12-17 10:00:00
category: medien
tags: [Serie, Musik]
published: true
comments: false
share: true
image: 
  feature: 2013-orange-is-the-new-black-finale-lg.jpg
  credit: Netflix
  creditlink: https://www.netflix.com
---

In der Einkaufspassage sang ein kleiner Gospelchor Weihnachtslieder und sofort hatte ich wieder das Lied aus dem *Orange is the new Black" Finale im Kopf. Dabei fiel mir ein, dass ich die Hintergründe über dieses Lied herausfinden wollte.

### I saw the Light

> I saw the light I saw the light  
> no more darkness no more night  
> Now Im so happy no sorrow in sight  
> Praise the lord I saw the light.  

Wer sich ein Musikclip zu dem Lied sucht, findet nicht nur ein Gospel-Lied, sondern sogar ein Country-Gospel-Lied, denn in fast allen Version klimpert eine Gitarre oder Banjo mit. [Die Version von Johnny Cash](https://www.youtube.com/watch?v=iLlw8p1usg4 "Youtube") ist zum Beispiel sehr schön. [I saw the Light](http://de.wikipedia.org/wiki/I_Saw_the_Light "Wikipedia zu I saw the Light") wurde in den 50ern von [Hank Williams](http://de.wikipedia.org/wiki/Hank_Williams "Wikipedia zu Hank Williams") geschrieben, einem Countrysänger (Überraschhung!).   
Doch mich überraschte vor allem die fröhliche Stimmung aller Interpretationen. Das [Publikum dreht bei dem Lied durch](https://www.youtube.com/watch?v=2BDk23Po1KQ "Youtube") und springt freudig tanzend durch die Gegend. [Und manchmal sogar der Chor selbst.](https://www.youtube.com/watch?v=d--HCA5PNs8 "Youtube")   
Ziemlich verrückt und gar nicht das, was ich erwartet hatte.

### I saw the Orange

Das grandiose Finale von *Orange is the new Black*  wird von diesem Lied begleitet. In der Schlüsselszene, aus welcher auch das Titelbild mit der Hauptdarstellerin stammt, führen die Insassinnen ihr Weihnachtsprogramm vor. Als die geplante Sängerin des Liedes plötzlich ein Blackout hat und stumm auf der Bühne steht, singt eine ältere, sonst sehr wortkarge, Insassin das Lied auf wundervolle Art und Weise.    

Die Hauptfigur Piper sitzt im Publikum und wird von diesem Lied ergriffen. Ohne zu viel zu verraten, aber es wenden sich wichtige Personen von ihr ab und gleichzeitig wird sie zur Zielscheibe einer fanatischen Christin.    
Das Lied strahlt Hoffnung und Freude aus, aber Piper verliert in diesem Moment jede Hoffnung und fragt sich womöglich, ob sie jemals das Licht sehen wird. Sie ist das komplette Gegenteil des Liedes, welches besonders im Refrain, wenn alle Insassinnen singen, zum Hohn für sie wird. 

Piper hält es nicht aus und geht nach draußen, wo eine Konfrontation auf sie wartet, in deren Verlauf ihre seelischer Zustand deutlich wird.

### Kunst der Antithese

Nachdem mir nun klar wurde, wie geschickt die Autoren das Lied eingesetzt haben, mag ich das Finale noch mehr.   

Wenn ich für mich das Lied summe, schwingt eine gewisse Schwere darin. Es ist vollständig von der besagten Situation geprägt. Diese Wirkung wird durch die perfekte Gegenüberstellung der gegensätzlichen Stimmungen in dieser Szene hervorgerufen. Auf der einen Seite der fröhliche Chor und auf der anderen die hoffnungslose Piper. 

Und das ist übrigens ein weiter Beleg für die große Güte dieser Serie.