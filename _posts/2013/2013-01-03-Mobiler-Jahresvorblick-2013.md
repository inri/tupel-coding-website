---
layout: post
title: "Technischer Jahresvorblick 2013"
description: "Mit was überraschen uns die Technikfirmen im Jahr 2013?"
date: 2013-01-03 
category: it
tags: [Apple, Android, Technologie]
published: true
comments: false
share: true
---

Was können wir in 2013 von den Technikfirmen erwarten? Neue Smartphones und neue Tablets auf jeden Fall, aber irgendwelche großen Neuerungen? Ein Vorgeschmack wird die CES in Las Vergas sein, auf der zum Beispiel Samsung ganz tolle, innovative Fernseher vorstellen wird. 

## Apple und Produktpflege

Mit was wird uns Apple überraschen? Vor einem Jahr gab es Gemunkel über einen Fernseher von Apple und dieses Jahr wird das gleiche Lied gespielt. Doch es gibt einige handfeste Hinweise, dass mehr dran sein könnte. Angeblich werden schon Prototypen getestet und Tim Cook hat bestätigt dass Fernseher durchaus ein interessanter Markt für Apple darstellen. Es könnten aber gut und gerne noch mehr als 12 Monate vergehen bis ein Produkt vorgestellt wird. Also der iTV erst 2014?

Ansonsten steht Produktpflege auf dem Plan: Sehr wahrscheinlich ein iPad mini 2 mit Retina Auflösung und ein technisch verbessertes iPhone 6 und iPad 5. Ich erwarte in dieser Hinsicht keine Überraschungen, aber eventuell wird iOS 7 eine größere Änderung von Apples mobilen Betriebssystem und hoffentlich sieht man die Handschrift von Ive im Design.      
Auch die MacBook-Reihe wird sich nicht ändern, wieso auch? Retina bleibt den Pro-Modellen vorbehalten, für die Bildschirme würde ansonsten zu viel Akkuleistung und stärkere Prozessoren benötigt, das wird man technisch nächstes Jahr noch nicht erreichen können.      
Dagegen kann sehr wohl die Software verbessert werden und damit ist explizit die iCloud gemeint. Was Dropbox kann, sollte Apple langsam auch hinbekommen.

## Android

Von Google kann man in 2013 interessante Geräte erwarten. Sicherlich bekommt die Tablet-Nexus-Reihe ein Update und man munkelt, dass die Geräte noch billiger werden. Ich würde ja behaupten, dass wir mittlerweile Android-Tablets haben, die technisch locker mit dem iPad konkurrieren können. Wie diverse Zahlen der letzten Wochen aber beweisen, scheinen diese Tablets nicht so richtig durchzustarten. Dabei sieht der Verkauf nicht ganz so schlimm aus wie die Nutzungsstatistiken, wobei das natürlich auch kein Gewinn für die Androiden ist. Hoffentlich erfährt man mehr darüber, ob die Vertriebsstrukturen noch nicht so gut wie bei Apple sind oder sie mit der Herstellung nicht hinterherkommen. Oder warum auch immer.

Des weiteren wird Googles Übernahme von Motorola endlich Früchte tragen. Was Google den anderen Herstellern schon bei den Nexus Phones diktiert, ist auf jeden Fall nicht verkehrt. Wobei ich auch hier sagen würde, dass die großen technischen Überraschungen ausbleiben werden. Fortschritte sollte es beim Android System geben, Ecken und Kanten sind genug vorhanden.
Was passiert aber mit der Nexus Phone Reihe? Werden die Motorolas in Zukunft Nexus heißen oder führt Google den Trend fort, was irgendwie auch eine Unterstützung der Konkurrenz wäre. Generell müssten die Hersteller die Sache mit den Updates auf die Reihe bekommen. 

## Sonstiges

Als weitere Firmen wären noch Samsung und Microsoft zu nennen. Samsung wird seine Vormachtstellung im mobilen Bereich nicht verlieren, auch wenn es nur die reinen Verkaufszahlen sind. Bald erscheint ihr neues S4 und zur Abwechslung vielleicht mal ein gutes Tablet.       
Microsoft sollte sehr bald das Intel-Surface auf den Markt bringen und selbst dann ist es nicht gewiss, dass sie Erfolg haben. Schön wären auch andere Surface-Tablets zu sehen, aber es steht und fällt letztlich mit der Software, Windows 8. Ob wir die Ankündigung für ein Windows 9 lesen werden? Oder bleibt Microsoft stur bei seinem Designkonzept, auch wenn die Tablets sich damit nicht verkaufen?

Und welche neuen Dienste und Firmen werden sich aus der Asche erheben? Auf neue Idee freue ich mich am meisten. Innovate me!