---
layout: post
title: "Zu Gast bei Circus HalliGalli"
description: ""
date: 2013-03-06 
category: medien
tags: [Fernsehen]
published: true
comments: false
share: true
image: 
  feature: 2013-circus_halligalli_olli_schulz-lg.jpg 
  credit: Pro 7
  creditlink: http://www.prosieben.de/tv/circus-halligalli
---

Dank meines grandiosen Bruders waren wir zu dritt am Montagnachmittag im Studio 5 zur Aufzeichnung der zweiten Sendung *Circus HalliGalli*. 

Es war nicht nur für mich das erste Mal, sondern auch mein Bruder und meine Freundin waren bisher noch nie bei einer Fernsehaufzeichnung dabei. Im Vorfeld haben wir wild gemutmaßt, was uns wohl erwartet. Die wievielte Sendung wird aufgenommen? Wer wird zu Gast sein? Wir viele Stunden müssen wir klatschen? Und vor allem: Sieht man uns später im Fernsehen?!

## Das Studio 5

Gute 700 Meter vom Studio 5 entfernt befindet sich die Bahlsen-Fabrik und eigentlich wollten wir vor der Sendung noch einen Abstecher dahin machen. Kartenausgabe war zwar ab 15:45 angekündigt, aber schon 20 Minuten früher verschwanden Menschen, die stark nach Gästen aussahen, hinter dem Eingangstor. So entschlossen wir uns ihnen zu folgen und die zerbrochenen Kekse nächstes Mal zu kaufen. Und diese Entscheidung war nicht verkehrt. Wir waren mit die ersten und konnten in aller Ruhe die Karten abfassen, Jacken, Taschen und Smartphones(!) abgeben und das stille Örtchen besuchen. Dann warteten wir mit den anderen in der Vorhalle, die im gleichen Stil wie die Sendung gehalten war, mit rotem Teppich und überall HalliGalli-Schriftzüge. Sehr schick. 

Langsam füllte sich die Halle und die Schlange aus Wartenden wurde länger. Gegen 16:10 war dann der blockweise Einlass. Wir saßen im Block B und waren nach Block C an der Reihe. Nachdem die ersten im Studio waren, erspähte ich am hinteren Ende der Schlange auch Stefan Niggemeier. Zu unserer Überraschung kam eine kleine Dame mit roten Haaren und Pelzmantel die Tür herein und wurde von einer Assistentin mit *Ah Violetta! Komm... hier lang.* an den Arm genommen und nach hinten geführt. Ständig liefen aus dem Fernsehen bekannte Mitarbeiter-Gesichter an uns vorbei. Wir grinsten, denn wenn sie in der Sendung waren, dann meist betrunken. 

Dann ging es in Richtung Studio. Fast Labyrinth-artig führte man uns zur Manege, wo wir in der zweiten Reihe in Block B, gleich an der Treppe, Platz nahmen. Da saßen wir nun im Circus HalliGalli. Die Kulisse sah wirklich sehr schick aus. Das kommt im Fernsehen leider gar nicht richtig rüber. Rechts hinten gibt es eine gut eingerichtete Bar, mal sehen wann diese das erste Mal benutzt wird.       
Die Szene mit Oliver Pocher aus der ersten Sendung, war tatsächlich kein vorher geschnittener Take. Diese Tür existiert und sie führt in eine schwarz-weiße Kulisse hinter der Kulisse. Auch sehr schick. 

## Kurz vor der Sendung

Langsam wurden die riesigen Kameras vorbereitet eine *Stand*-Kamera, ein Kran und eine Ein-Person-Kamera und die Assistentinnen zählten und rückten uns Gäste hin und her. Der Produktionsleiter lief auch einmal an uns vorbei und verriet den Basecap-Trägern, dass ihre Mützen einen hässlichen Schatten auf ihr Gesicht machen. Aber sie sollten selbst entscheiden, ob sie die abnehmen wollen. Alle taten es. Egal war es trotzdem, denn im Nachhinein fand ich die Zuschauer relativ dunkel beleuchtet.

Wie erwartet gab es einen Publikum-Aufwärmer der mit enorm guter Laune einige Dinge erklärte und unseren Beifall befeuerte. Ich hatte befürchtet, dass dieser Part länger dauern könnte oder irgendwelche *Spielregeln* erklärt werden. Nö. Wir sollten nur lange bei den Gästen und beim Hereinkommen von Joko und Klaas klatschen.        
Es gab auch keine *Klatschen!*-Schilder oder sonst was. Die verließen sich einfach auf uns. Das hat mich sehr beeindruckt. Joko und Klaas kamen auch herein, begrüßten uns, erklärten die Sendung und rissen gute Witze. Die beiden sind in echt gleich noch viel sympathischer und man ahnt schon, wieso nicht jeder Hanswurst ins Fernsehen kommt.

*Denkt dran, wenn ihr etwas lustig findet: Klatschen. Und noch wichtiger, wenn ihr etwas nicht lustig findet, erst recht Klatschen! Wir haben das bitter nötig.*

Zu unserer Überraschung war das die Aufzeichnung der 2. Sendung, also unter anderem mit Anke Engelke zu Gast. Da freute sich besonders meine Liebste. Als weitere Überraschung gesellte sich Olli Schulz ins Publikum. Leider nicht vor uns in die erste Reihe, sondern in Block C. Grundlos war er natürlich nicht da. Und dann ging es auch schon los.

## Die Show

Über uns und teilweise vor uns auf rollbaren Wagen standen Bildschirme und zeigten die Einspieler. Das hatten wir nicht unbedingt erwartet. Es ging mit einer kurzen Rückblende der letzten Sendung los und dann begann die Musik und Violetta tanzte. Dieser Part wurde zwei Mal gedreht. Dabei blieb es aber auch. Der Rest der Sendung war ohne Wiederholungen. Lediglich die Band wurde vorab schon gefilmt und war nicht live im Studio.

Die beiden Chaoten kamen ins Studio und wir klatschten und klatschten und klatschten. Ich dachte noch, dass uns irgendwann ein *Stop*-Zeichen gegeben wird, aber dem war nicht so. Auch bei späteren Situationen klatschen wir so lange, dass wir die Ansagen von Joko und Klaas nicht verstanden. Die begannen irgendwann zu reden und wir klatschten munter weiter. Die Ansagen hörte ich später im Fernsehen auch das erste Mal. Und mir wurde auch dann erst bewusst, dass auch bei den Einspielern der Studioton mitgeschnitten wurde. Den Ton, den ihr hören könnt, auch von den Videos, ist aus dem Studio aufgenommen. Inklusive unserem Lachen.

Sehr genial war Ollis Part in der Sendung. Irgendwann wurde direkt neben uns das Podest aufgestellt und er kam nach einem Beitrag dorthin und forderte Bjarne Mädel zum Rangeln auf. Bei dieser Szene sind sogar wir mal zu sehen (siehe oben, links vom Pult). Ansonsten gab es eher weniger Aufnahmen vom Publikum und meistens war es ziemlich dunkel.

In der Sendung sah man ja das Rangeln, welches durch die Schrankband und der darauffolgenden Werbung unterbrochen wurde. In der Pause machten sich alle zurecht und Olli setzte sich wieder in die erste Reihe. Die Visagistin fummelte an Anke Engelke rum und da rief Bjarne zu Olli *Du hast zu dolle gerangelt, die Anke blutet am Mund!* und Olli erschrak und lief zu Anke. Doch sie blutete nicht, sondern es war ein Trick von Bjarne, der in diesem Moment Olli packte und noch einmal mit ihm auf der Couch rangelte! Einfach so, obwohl die Kameras aus waren! Die verstehen sich alle super und haben Spaß.      
Ach ja, die Werbung sahen wir nicht. Wenn Werbung ist, wird auch eine kurze Pause eingelegt und nachgeschminkt. Das passiert meist auch bei Einspielern, wobei Joko und Klaas diese oft mit anschauen. 

## WTF?

Sehr sehr seltsam war der WTF-Moment. Ein jaulendes Walross mit nacktem Hintern, inklusive menschlicher Hoden. Die Jungs waren ähnlich verstört wie das Publikum. Doch ich ertappte mich vorher dabei, gespannt auf die Überraschung zu sein. Dann war die Sendung auch schon vorbei. Sie gefiel mir besser als die erste und das nicht nur, weil ich live dabei war. Die Ja-Sager Teil 2 war besser und das Interview von Violetta auch. Etwas befremdlich sind die Studiogags. Mal sehen ob sie daran festhalten. 

Im Anschluss wurde noch ein kurzes Video für die Facebook-Seite gedreht und dann war es das. Insgesamt waren wir ungefähr 90 Minuten im Studio. Ich weiß es nicht genau, mein iPhone war in der Garderobe. Deshalb gibt es auch keine weiteren Bilder. Als wir zum Bus gingen, fuhr ein schwarzer Mercedes aus der Studioausfahrt. Hinten drin saß Anke Engelke mit einer Brille auf der Nase und die Augen auf das Smartphone gerichtet.

## Fazit

Ich fand es sehr interessant zu Sehen, wie eine solche Produktion abläuft. Überrascht hat mich wirklich, dass dem Publikum freie Hand gelassen wird. Natürlich sagen sie, dass sie sich über Applaus freuen, aber es gibt keinen Zwang. Auch dass die Sendung hintereinander abgedreht wurde, ist erstaunlich. Ich würde auch sagen, dass kaum etwas rausgeschnitten wurde. Mir fiel im Nachhinein nichts auf.

Joko und Klaas sind hinter der Kamera sehr nett und sympathisch. Das hatte ich aber auch nicht anders erwartet. Natürlich gab es auch von Olli Schulz einige Sprüche.  

Noch mal vielen Dank an meinen Bruder für die Karten!