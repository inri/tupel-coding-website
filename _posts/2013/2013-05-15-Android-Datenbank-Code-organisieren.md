---
layout: post
title: "Android: Datenbank Code organisieren"
description: ""
date: 2013-05-15 
category: it
tags: [Android, Coding]
published: true
comments: false
share: true
---

Keine Frage, kaum eine Anwendung kommt heutzutage ohne Datenbank aus. Im Web gibt es schon unzählige Tutorials über SQLite als Datenbank für Android Apps und dies wird definitiv kein weiteres. Ich habe in den letzten Monaten aber meinen SQLite-relevanten Code angepasst und mittlerweile eine ganz gute Organisation. 

### Redundanz

Das Hauptproblem ist meines Erachtens die große Menge an identischen Codezeilen. Woraus besteht die Beschreibung für die Datenbank?

*	Table Names
*	Column Names
*	Column Numbers

Diese drei Komponenten muss man an verschiedenen Stellen immer wieder nutzen. Im Datenbankadapter/helper und sonst bei allen Arten von Anfragen (*rawQueries*, *deletes*, *updates*,...)

### Konstanten

Auf jeden Fall ist es sinnvoll die *Column Names* in Konstanten zu verpacken. Ich habe eine einzelne *Const.java* Klasse, die nur aus finalen Variablen besteht. Über *Const.Name* kann ich bequem auf die Namen zurückgreifen und weiß mit Sicherheit, dass z.b. kein Tippfehler vorkommt. 

Ich nutze einen [SQLiteHelper](https://github.com/jgilfelt/android-sqlite-asset-helper) und führe die Datenbankabfragen immer über einen Adapter aus. Nun ist es mühsam die *Queries* immer wieder auf Neue in die *insert*, *update*, *delete* und *rawQuery* zu geben und deshalb habe ich diese ebenfalls in einer *QueryHelper*-Klasse ausgelagert. Über ein beispielsweise simples 

	QueryHelper.getAllOrderById(String id) 

bekomme ich für den *rawQuery* den richtigen String mit und muss der Hilfsklasse nur die Id übergeben. Ändere ich etwas an dem Query, muss ich die Änderung nur einmal vornehmen.

Die letzte Erleichterung habe ich auch erst vor einigen Wochen eingebaut. Hat man einen Cursor mit den Daten, kann man über dem *Column Index* auf die Daten zugreifen. Lange Zeit habe ich tatsächlich immer brav die entsprechende Nummer eingetippt, aber wenn man die Tabelle ändert und z.b. eine zusätzliche Spalte einfügt, muss man überall im Code die Nummern anpassen. 
     
Die Lösung ist hier **nicht** die *Column Numbers* mit finalen Integer in der *Constants-Klasse* zu pflegen, sondern die bereits gepflegten *Column Names* Konstanten ein weiteres Mal zu benutzen: 

	 cursor.getString(cursor.getColumnIndex(Const.Column_Name))

Eine Änderung bei den Tabellen der Datenbank, hat keinerlei Auswirkungen auf den Code.

Das sind keine Geheimnisse, aber durchaus eine angenehme Erleichterung bei der Entwicklung.
