---
layout: post
title: "iTunes für Zeitschriften und Magazine"
description: "Wieso gibt keinen iTunes-ähnlichen Dienst um Magazine und Zeitschriften zu verwalten und zu erwerben."
date: 2013-01-07 
category: blog
tags: [Idee, App]
published: true
comments: false
share: true
---

Ich habe unzählige Fachbücher, einige Bücher und viele Zeitschriften in digitaler Form. Die Fachbücher sind nach Themen sortiert und landen auf meinem Kindle und iPad mini je nach Bedarf. Aber wie organisiert man die Zeitschriften am besten?

Wir haben alle keine Zeit. Entweder ich lese Artikel mit aktuellem Bezug oder eben nicht. Eine iPhone 5 Review muss ich mir jetzt, Monate nach Release, nicht mehr antun. Aber die meisten Zeitschriften haben auch zahlreiche mehr oder weniger zeitlose Artikel und Berichte. Technik-Magazine glänzen beispielsweise durch interessante Tutorials. Diese können durchaus auch Monate und Jahre nach ihrem Erscheinen noch interessant und nützlich sein. Aber wie findet man sie?

## Glossar &amp; Sammlung

Im Prinzip braucht man zwei Komponenten. Zum einen eine Art Glossar, in ihm sind alle Magazine und Zeitschriften aufgelistet, inklusive aller Titel der umfangreicheren Artikel, Berichte und Heftinhalte einer Ausgabe. Dabei sind die einzelnen Ausgaben und Heftinhalte noch mit Schlagwörtern, Tags, versehen. Auf diese Weise kann ich nach bestimmten Begriffen suchen, finde alle passenden Inhalte und weiß in welcher Ausgabe welches Magazins sie erschienen sind.
Dieses Glossar kann online organisiert sein und jeder Zeitschriftenverlag pflegt seine Hefte ein.

Auf dem heimischen Computer wird ein Programm für die eigene Sammlung installiert. In diesem werden zuerst die Zeitschriften und Magazine importiert und im zweiten Schritt findet ein Abgleich mit dem Glossar statt. Am Ende kann ich in meiner Sammlung nach bestimmten Themen suchen und finde endlich die passenden Hefte.       
Zusätzlich möchte ich die einzelnen Heftinhalte favorisieren bzw. für späteres Lesen markieren. Ansonsten vergesse ich, was ich mal interessant fand. Und natürlich sollte es früher oder später auch Apps für Smartphones und Tablets geben.

## Finanzierung &amp; Social

Die eigene Sammlung zu pflegen, muss nicht alles sein. Favoriten können im Glossar aufgenommen werden und damit haben wir einen sozialen Faktor (*200 andere Personen fanden diesen Bericht gut*). Warum nicht? Zusammen mit Empfehlungen an Freunde und Facebook-Integration.

Das Glossar dürfte vom Datenumfang her nicht riesig groß werden. Wir speichern keine kompletten Texte oder Medien, sondern nur Titel und Schlagwörter. Finanzieren könnte es sich durch den Verkauf bzw. Vermarktung der Heftinhalte. Finde ich in meiner Sammlung nichts zu einem Thema, kann ich ja auch online suchen und das entsprechende Heft oder den einzelnen Artikel kaufen. Das wiederum wäre eine Einnahmequelle für Verlage.

Ein Online-Glossar haben einige oder vielleicht auch viele Verlage schon. Aber alles ist dezentral, unbequem und teuer. Einige Male habe ich ältere Hefte gesucht und die verlangten Preise waren einfach nur frech. Das muss ja auch nicht sein.

Vielleicht gibt es schon ein Programm für meine Bedürfnisse. Aber solange die Heftinhalte nicht zentral sortiert werden, dürfte ein solches Programm Zukunftsmusik sein. Wenn man bedenkt, auf wie vielen guten Inhalten diese Verlage sitzen, ist das eine riesengroße Schande. Bei Musik klappt es ja auch. 