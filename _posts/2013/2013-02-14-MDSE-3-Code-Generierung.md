---
layout: post
title: "MDSE, Code Generierung"
description: ""
date: 2013-02-14 
category: it
tags: [HPI, Studium]
published: true
comments: false
share: true
---

Nun haben wir ein Metamodell und die DSL mit welcher der Benutzer ein Modell erstellen kann. Aus diesem Modell wollen wir nun Programmcode generieren und quasi ein Programm erzeugen. 

## Xpand

Unser Mittel der Wahl nennt sich *Xpand* und funktioniert eigentlich ganz einfach. Im *Xpand*-Dokument können wir auf das Modell, seine Klassen und deren Attribute zugreifen und mit Kontrollstrukturen wie u.a. *FOR*, *FOREACH* und *IF* beschreiben, welcher Code wann erzeugt werden soll. Im Prinzip funktioniert das genauso wie bei einem Template.

<figure>
    <a href="/images/2013-modeling-codegen-sensors-800.jpg">
	   <img 
		  src="/images/2013-modeling-codegen-sensors-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Sensoren</figcaption>
</figure>

Wir gehen die Liste aller Sensoren durch und schauen nach, vom welchem Typ der Sensor ist. Am Ende soll ein Javaprogramm herauskommen und je nach Sensorart wollen wir ein Textfeld oder eben eine Checkbox erzeugen.

## Unbegrenzte Möglichkeiten

Was das Programm angeht, hat man fast unbegrenzte Möglichkeiten. Ein Team hat z.b. noch eine Simulator programmiert, der einen Tagesablauf mit vergehender Zeit und sich verändernden Temperaturen simuliert. Kann man machen, wenn man zu viel Zeit hat. Ich habe mich auf die simpelste Lösung beschränkt und das ist die grafische Darstellung und ein einfacher *ActionListener*, der reagiert sobald ein Sensor sich verändert. 

<figure>
    <a href="/images/2013-modeling-code-ActionListener-800.jpg">
	   <img 
		  src="/images/2013-modeling-code-ActionListener-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>ActionListener</figcaption>
</figure>


Das bringt einen Nachteil mit sich, denn dadurch werden alle Regeln durchlaufen und nicht differenziert, welcher Sensor gerade seinen Wert verändert hat.

Besonders ungünstig ist das, wenn die eine Regel die Lampe anschaltet weil ein Schalter gedrückt wurde und eine andere Regel danach die Lampe wieder ausschaltet, weil irgendeine andere Bedingung nicht erfüllt ist. Uns ist bewusst, dass man diesen Teil überarbeiten und auch darauf achten müsste, dass der Bewohner einige Dinge außerhalb der Regeln machen möchte.

Aber wie bereits gesagt, die Möglichkeiten sind vielfältig und das ist schließlich ein Projekt zum Experimentieren und Erfahrung sammeln. 