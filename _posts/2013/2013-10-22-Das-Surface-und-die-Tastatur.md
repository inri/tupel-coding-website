---
layout: post
title: "Das Surface und die Tastatur"
date: 2013-10-22 00:01:00
category: it
tags: [Microsoft, Surface]
published: true
comments: false
share: true
---

Heute entdeckte ich [dieses Interview auf Youtube mit dem Design Director der Surface-Abteiltung bei Microsoft, Ralf Groene](https://www.youtube.com/watch?v=ucs3pHwyTok&list=WLStPeLdwjxkcdwCOSa_MT7005g31DQc8A) und schaute es mir zunächst neugierig an. Die Neugier verflog schnell und wandelte sich in Erstaunen und ungläubiges Kopfschütteln.

In der Videobeschreibung ist von einem Interview über die Entstehung des Surface die rede, aber das ist gelogen. Eigentlich redet Herr Groene fast nur über die Entstehung dieser Touch Cover-Tastaturen, die man als teures Zubehör für das Surface erstehen kann. 

### 2,5 Jahre Entwicklungszeit

Doch die erste schockierende Aussage macht Herr Groene gleich am Anfang als er den Beginn der Entwicklung zeitlich einordnet. Seiner Aussage nach erschien kurz nachdem sein Team zusammengestellt wurde das iPad. Also begann Microsoft **Anfang 2010** mit der Entwicklung des Surface und hat dann noch 2,5 Jahre gebraucht um ein Produkt auf den Markt zu bringen. Wow.    

Zum Surface verrät er leider nicht viel Neues, das meiste hat man schon von Microsoft in der einen oder anderen Form gehört. Sie wollten etwas *Eigenes* kreieren, keinen simplen Klon der Tablets auf den Markt. Mich wundert es nur, welche Tablets sie meinen, denn bis Ende 2011 gab es praktisch nur das iPad. Die Android Tablets kamen ewig nicht aus dem Knick.    
Sie haben anscheinend lange mit verschiedenen Prototypen experimentiert, bis sie den Geistesblitz hatten dass eine magnetische Tastatur das Alleinstellungsmerkmal für ein Tablet wäre. 

Als Groene den PR-Satz runterbetete, dass mit den bisherigen Tablets die Nutzer ja eigentlich ein Notebook mit dazu kaufen müssen, war mir klar: *Der hat nichts verstanden.*.

### Tastatur zum Glück

<figure>
	<img src="/images/2013-type-cover-800.jpg" alt="Microsofts Type Cover für das Surface">
	<figcaption>Daran hat Microsoft über zwei Jahre entwickelt</figcaption>
</figure>

Ich bin mir sicher, dass die Technik im Touch und Type Cover sicherlich ganz toll ist und sich die Entwickler viel Mühe gegeben haben. Und vermutlich tue ich Herrn Groene auch Unrecht, denn er war wohl nur für das Gerätedesign verantwortlich. Trotzdem kann man doch nicht ausblenden, dass Microsoft den völlig falschen Fokus gesetzt hat!   

Vermutlich ist das am traurigsten. Die Hardware ist gar nicht mal so schlecht, aber das System totaler murks. Hundert Type Covers können diesen Makel nicht wettmachen. 

### Akkulaufzeit von Tablets / Notebooks mit Windows

Mit in diese Überlegung möchte ich diesen guten Beitrag von [Jeff Atwood über die Akkulaufzeiten von Windows Geräten](http://www.codinghorror.com/blog/2013/10/why-does-windows-have-terrible-battery-life.html) einwerfen. Kurz: Sie ist zu schlecht. Und viel zu schlecht für einen Giganten wie Microsoft. Und vor allem viel zu viel zu viel zu schlecht für ein Gerät aus eigener Herstellung, wie es das Surface ja ist. 

Und warum ein Windows 8 auf der fantastischen Hardware des 2013er MacBook Air nur auf die Hälfte der OS X Laufzeit kommt (**14 Stunden vs. 7 Stunden!**) sollte eine ernsthafte Frage in den Chefetagen von Microsoft sein. 

### Fazit

Microsoft hat immer noch nicht verstanden was die Kunden möchten. Das Surface 2 ist kaum attraktiver als sein Vorgänger. Windows 8.1 ist ebenfalls keine Verbesserung oder Ausbesserung, wie man erhofft hat. Und vermutlich hat man in den letzten Monaten in der Surface-Abteilung größtenteils am Type Cover gefeilt, denn an die Software dürfen die wahrscheinlich nicht. 

Wo wird Microsoft in 5 Jahren sein?