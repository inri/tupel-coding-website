---
layout: post
title: "Förderung für die Gründung eines Unternehmens"
description: ""
date: 2013-06-15
tags: [Startup, Uni] 
category: blog
published: true
comments: false
share: true
---

Wie schreibe ich diesen Beitrag hier um nicht total naiv rüberzukommen? Vielleicht so: Den Gedanken einer Firmengründung hatte ich schon öfters. Allerdings möchte ich kein Unternehmen gründen um ein Unternehmen zu gründen, sondern um eine Idee umzusetzen, wie ich sie mir vorstelle. 

Eine Schwierigkeit war bisher vor allem mein Freundes- und Bekanntenkreis. Ich mag meine Freunde wirklich sehr, aber eine Unternehmensgründung kommt für sie nicht in Frage. Allein kann man aber die wenigsten Ideen umsetzen, deshalb gab es bisher keine konkreten Pläne. Leider.      
Nun beschreibe ich euch die Erkenntnisse einer Woche. Achtung, Achtung, dies ist keine endgültige Analyse und ich habe noch etwas Lesematerial. Das ist nur mein erster Eindruck. Weitere Informationen folgen.

## Die Idee

Als ich mit meiner liebsten vor einigen Tagen spazierte, erzählte sie mir von ihrem Alltag als Sprachtherapeutin und wir hatten eine Idee. Ich möchte nicht zu konkret werden, aber es ist eine Art Dienst / App für Kinder. Vielleicht bin ich hierbei nicht objektiv, doch die Idee ist solide. Sie macht uns nicht zu Millionären, damit kann ich leben. Stattdessen löst sie vermutlich ein Problem, hilft einer bestimmten Gruppe von Menschen und kann auf jeden Fall die Grundlage für weitere ähnliche Ideen sein.

Der nächste Gedanke: Warum keine Firma gründen und die Idee umsetzen? Das rein formale Gründen der Firma ist nicht weiter problematisch, doch es stellt sich die Frage: Wie finanziert man das ganze und woher bekommt man Unterstützung für dieses Vorhaben?

## Förderung

Zur Zeit ist das Unterstützen von Gründungskultur voll im Trend. Die Bundesregierung steckt eine ganze Menge Geld hinein und das kann man ja nutzen, dachte ich mir. Meine blumige Vorstellung sähe folgendermaßen aus: Wir haben ein Jahr Zeit die Idee zu verwirklichen, werden zwischendurch von fachlich geschulten Personen betreut und vermeiden somit möglichst viele Anfängerfehler. Und natürlich erhoffte ich mir auch eine gewissen finanzielle Spritze jeden Monat als Lebensunterhalt.      
Nachdem ich kurz recherchiert und einen Bekannten konsultiert hatte, meldete ich mich bei Menschen von der Uni Potsdam, die Studenten mit Informationen zu Unternehmensgründung versorgen. 

In Brandenburg ist gerade das Förderprogramm Existenzgründung vom BIEM Startup Navigator aktuell. Dazu erhoffte ich mir Informationen. Des weiteren gibt es aber auch das EXIST-Programm vom Bund, was aus vielen Gründen interessant ist. Zum einen unterstützt es Absolventen 12 Monate lang mit 2000€ (abzgl. Steuern), stellt als Bedingung den Kontakt zu einem Mentor von der Uni und fordert die schrittweise Ausarbeitung eines Businessplans. Klingt doch ganz vielversprechend, dachte ich.

## Finanzierung

Aber die Realität sieht eben anders aus. Gleich zu EXIST einige Worte: Auf ihrer Website steht, dass sie ein Ideenpapier erwarten, aber eigentlich wollen die einen fertigen Businessplan. Des weiteren stehen Apps gerade hoch im Kurs und dementsprechend ist es schwierig ein solches Projekt bewilligt zu bekommen. Es müsste *wirklich innovativ* sein und egal was damit gemeint ist, unsere Idee ist es nicht. Außerdem braucht man zwingend einen BWLer im Team.

Was bietet das Förderprogramm vom Startup Navigator, außer Ernüchterung? In der Tat fördert es *nur* die Existenzgründung, wie der Name es schon sagt. Im Zentrum steht Beratung und Coaching bei der Ausarbeitung des Businessplans. Verstehen Sie mich nicht falsch, ein Businessplan ist wichtig und jeder potentielle Gründer setzt sich beim Erstellen eines solchen intensiv mit seiner Idee und deren Umsetzung auseinander. So weit, so gut.      
Weitere Information bzgl. Finanzierung erhielt ich von der Beratung nicht so richtig. Oder zumindest schien es mir, als gehöre das Finden von Geld eben zum Prozess mit dazu. Ich weiß dass eine Firmengründung immer auch ein Risiko ist, aber ich bin nicht bereit das Risiko in Form von hohen Schulden zu tragen. Zumindest nicht, so lange ich nicht eine gewisse Summe erspart habe.

Ich habe hier aber noch einiges an Lesematerial und eventuell findet sich doch noch eine Finanzierungsquelle. Informationen folgen.     

Etwas geärgert habe ich mich über folgenden Aspekt des Förderprogramms Existenzgründung. Man erhält ja Beratung und Coaching, aber die Berater bieten das nicht kostenlos an. Die Gründer erhalten vom Förderprogramm pro Nase 3000€, die sie nur in Coaching investieren dürfen. Ich weiß leider nicht wie umfangreich diese Beratungen sind, aber 3000€ scheint mir eine ganze Menge Geld zu sein.       
Mein Bauchgefühl sagt mir, dass bei diesem Förderprogramm in erster Linie die Berater gefördert werden. Das ist nur eine Vermutung, keine Behauptung! Ich möchte niemanden etwas vorwerfen.

## Fazit

Ich fand das Gespräch enttäuschend und ernüchternd. Die ganze Starthilfe, die Beratung und das Netzwerk ist ja gut und schön, aber mir scheint als ob diese Art Community auch ohne expliziter Förderung entstehen würde oder durch richtige Förderung in Form von Geld erzwungen werden kann.     
Ich werde mich zu diesem Thema noch etwas belesen und vielleicht wollten die netten Menschen von der Uni auch nur ihr Förderprogramm ins Rampenlicht stellen und vergaßen mir einige Informationen mitzuteilen. Hoffentlich.

#### Kommentar von Marcel

Hallo Ingo,

ich kann dir mal kurz ein paar Punkte darlegen was ich aus *IT-Unternehmensgründung* mitgenommen habe.

1. Der App-Hype ist vorbei. Der Markt ist da mittlerweile extrem hart umkämpft und die Gewinnspanne ist sehr klein. Vor allem kommen die meisten *App-Buden* aus Osteuropa und anderen Niedriglohn-Ländern, wodurch es noch schwieriger wird mit denen zu konkurrieren.       
Deine Idee wäre also vermutlich besser als Online-Plattform umzusetzen, falls sich das ganze am Ende irgendwie rentieren soll. Zusätzlich Apps um den Online-Dienst auf mobilen Geräten besser nutzbar zu machen ist eine andere Sache.

2. Gerade bei IT-Unternehmen hat man am Anfang ziemlich niedrige Kosten. Beispiele: den eigenen PC zum entwickeln nutzen, kein extra Büro sondern einfach die Wohnung nutzen, die Gründer machen die ganze Arbeit, keine Mitarbeiter. Das gibt einem in der *Seed*-Phase einen entscheidenden Vorteil. Es ist sehr hilfreich, wenn man es schafft ohne Geld von außen zumindest einen Prototypen fertig zu stellen und auch schon eine kleine Nutzerbasis an sich binden kann, und diese quasi auch als Beta-Tester für sich nutzt.

3. Es ist einfacher einen Investor (Venture Capital, staatliche Förderung usw.) zu überzeugen, wenn man bereits einen Prototypen hat und auch ein paar Nutzer vorweisen kann. Dadurch ist das Produkt / die Idee nicht mehr so abstrakt und man hat quasi schon bewiesen, dass das ganze funktionieren könnte. Auch ein bereits ausgearbeiteter Business-Plan ist wohl sehr wichtig. Wobei das nicht das einzige ist *Elevator-Pitch* / *Rocket-Pitch*, *Business-Canvas* liegen wohl im Trend. Also mal andere Arten eine Geschäftsidee zu beschreiben, mit unterschiedlich viel Informationsgehalt.

4. Die Idee vielen Leuten zeigen. Richtig vielen Leuten!!! Vor allem in Berlin muss es da wohl immer Veranstaltungen geben, wo sich junge Gründer treffen. So kann man oft Investoren finden, die Bekanntheit vergrößern aber auch gut Leute anwerben (z.B. der fehlende BWLer^^)

Hoffe, die Tipps waren zumindest ein bisschen hilfreich. Wünsche dir auf jeden Fall viel Glück, wenn du die Idee weiter verfolgen möchtest.

Grüße

Marcel