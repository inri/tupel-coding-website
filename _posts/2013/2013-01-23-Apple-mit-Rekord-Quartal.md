---
layout: post
title: "Apple mit Rekord Quartal"
description: "Apple aktuelle Quartalszahlen stellen alles in den Schatten."
date: 2013-01-23 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Apple hat im letzten Quartal einen Rekordgewinn und Rekordverkäufe von iPhones und iPads eingefahren. Wow. Nach dem ganzen Quatsch der letzten Wochen, muss man sich das mal auf der Zunge zergehen lassen: 47,8 Millionen verkaufter iPhones. *In Your Face, Samsung.* möchte man fast rufen.

#### Samsung-Vergleich

Vor einigen Tagen hatte sich Samsung über das 100-Millionste Galaxy-S-Phone gefreut. Von der gesamten Galaxy-Reihe, die aus insgesamt drei Geräten besteht (S1, S2, S3) und seit Juni 2010 verkauft wird, also seit 30 Monaten, hat Samsung in 15 Monaten so viele verkaufen können wie Apple in 3 Monaten. Und man bedenke: Pro iPhone macht Apple einen wesentlich höheren Gewinn als Samsung oder ein anderer Player im Markt der Androhend Phones. Für die Branche sind Samsungs Zahlen natürlich hoch, aber Apple spielt in einer gänzlich anderen Liga. 

#### Enttäuschendes iPhone 5?

Es ist klar, dass die 47,8 Millionen verkaufte iPhones nicht nur iPhones 5 sein werden. Aber von einem sich schlecht oder mäßig verkaufenden iPhone 5 kann nicht die Rede sein. Das zeigt noch einmal sehr schön, wie sich die Technik-Presse immer mehr von der Realität entfernt. Statt zu schauen, ob ein Produkt gut und für den Menschen nützlich ist, versteift man sich auf angeblich fehlende Innovationen. Was scheint wichtiger zu sein? Unausgereifter Technikschnickschnack oder ein Gerät, dass die Erwartungen erfüllt?

#### Mini Tablet außer Konkurrenz

Der Tablet-Markt scheint auch nicht einzubrechen und Apple verkaufte mit 22,9 Millionen iPads fast halb so viele Tablets wie iPhones. Wie viele Minis mögen darunter sein? Wenige sind es nicht und John Gruber ahnt schon, dass es mehr sein werden als alle anderen Mini-Tablets zusammen genommen. Bisher haben weder Amazon noch Google irgendwelche Verkaufszahlen veröffentlich, so dass man natürlich nur mutmaßen kann. Doch ein Reinfall ist Apples Preispolitik vermutlich nicht gewesen.

#### Weltweiter Wachstum

Man beachte auch den enormen Wachstum den Apple außerhalb der USA einfährt. Auf dem amerikanischen Markt ist Apple stark, 39% vom Gesamtumsatz, und Europa ist auch ganz gut erschlossen. Aber nun kommen Länder wie China hinzu und das sieht zumindest für mich so aus, als ob noch einiges an Platz zum Wachsen vorhanden ist. 

## Fazit

Ein Ende ist nicht in Sicht. Einige unkten, und werden es weiterhin tun, dass Steve Jobs der Firma fehlt und sie bald nicht mehr erfolgreich sein wird. Bisher hat Apple aber nichts überraschendes gemacht, was man nicht auch unter Steve Jobs erwartet hätte. Ich denke sowieso, er hat in den letzten Jahren vor seinem Tod ein spitzenmäßiges Team zusammengestellt und darauf geachtet, dass sie weiterhin vernünftig entscheiden.        
Mittlerweile steht der Firma so viel Geld und Möglichkeiten zur Verfügung, dass sie eigentlich gar nichts schlechtes herausbringen können. Apple wandelt sich in eine wirklich enorm große Firma. 
Ich habe da eine Vision, nicht direkt von Apple, aber eben von einer großen Firma. Doch das ist eine andere Geschichte...