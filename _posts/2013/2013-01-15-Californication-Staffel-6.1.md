---
layout: post
title: "Californication, Staffel 6 Episode 1"
description: "Die erste Episode der 6. Staffel von Californication führt die Geschichte rund um Hank Moody fort und gibt einen tolles Ausblick auf die restlichen Episoden."
date: 2013-01-15 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2013-californication-season-6.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/californication/home
---

**Hank Moody ist zurück, endlich!**      
Es folgt eine spoilergeschwängerte Zusammenfassung der ersten Episode der 6. Staffel.

## Doch kein Happy End?

Am Ende der 5. Staffel schien das Leben des Hank Moody endlich eine große positive Wendung zu nehmen. Seine große Liebe Karen wartete mit einem Dinner auf ihn und er wollte nur noch einen kurzen Abstecher in seine Wohnung machen. Doch da wartete seine durchgeknallte Ex-Freundin Carrie aus New York, die ihm einen mit Pillen versetzten Drink andrehte. Sie selbst hatte sich ebenfalls eine Überdosis verpasst. Als er langsam das Bewusstsein verlor, legte Carrie ihn auf die Couch und sich auf ihn drauf. Die Lichter gehen aus.

Dass der Titelheld Hank die Nacht überleben wird, war zu erwarten. Aber welchen Einfluss hat der Mordversuch auf die Beziehung zu Karen? Meine Angst war ein erneuter Bruch zwischen beiden, ähnlich der Situation als das Geheimnis mit Mia ans Licht kam. Doch ich freute mich sehr zu sehen, dass Karen zu Hank hält und sich für sie seit der Nacht nichts verändert hat. Sie wartet auf ihn.

## Angst zu verletzen

Welche Wirkung hatte die Nacht auf Hank? Ich an seiner Stelle wäre wütend auf Carrie, denn es war nichts weniger als ein Mordversuch. Doch für Hank stellt sich auch die Frage, in wie weit hat er dazu beigetragen, dass sie keinen anderen Ausweg sah? Die Suche nach der eigenen Schuld wird von der Freundin der Verrückten befeuert. Sie sitzt im Krankenzimmer wo Carrie von Maschinen am Leben gehalten wird und gibt Hank unmissverständlich zu verstehen, dass er allein die Schuld trägt. 

In Traumsequenzen wird noch deutlicher, wie genau sich seine Angst manifestiert. Er möchte mit Karen zusammen sein, aber fürchtet sich davor sie zu verletzen. Wenn er als Ehemann versagt, dann tut sie sich vielleicht auch was an, genau wie Carrie. Um diese Angst zu betäuben betrinkt er sich einen Monat lang in den Bars von L.A., aber im Unterschied zu früher tut er das allein.

Es ist die erste Staffel, die zu Beginn keine Frauengeschichten von Hank zeigt. Das macht besonders deutlich, dass seine Niedergeschlagenheit diesmal tiefgreifender ist als beispielsweise zu Zeiten von Karens Verlobung aus Staffel 1. Für Frauen war bisher immer Platz und Zeit.

Becca und Charlie versuchen erfolglos mit Hank über sein offensichtliches Trinkproblem zu reden. Von dem einen bekommt er das Angebot zusammen mit dem Rockstar Atticus sein ersten erfolgreichen Roman in ein Musical zu verwandeln und von der anderen die neuen Lebenspläne mitgeteilt: Schule schmeißen und Schriftstellerin werden. Sie ist die Tochter ihres Vaters. Aber das Musical interessiert ihn nicht und für seine Tochter kann er auch kein Verständnis aufbringen.

Es kommt was kommen muss: Eine Intervention seiner vier Liebsten. Die Intervention artet unplanmäßig in einem Gejammer von Marcy und Charlie aus. Die Forderung an Hank ist zwar einerseits mit dem Trinken aufzuhören, doch alle möchten eigentlich nur, dass er bessere Laune hat. Getrunken hat er schließlich schon immer. Jetzt ist er aber schlecht gelaunt, unkreativ und suhlt sich in Selbstschuld. Hank ist sichtlich unbeeindruckt. Die Szene ist aber grandios dargestellt, wie eigentlich alle Szenen, in denen alle Hauptdarsteller zusammen an einem Tisch sitzen oder sich wie hier auf einem kleinen Sofa quetschen.

Nach einem letzten Gespräch mit Karen und einer Traumsequenz ohne Albtraum-Carrie, wacht Hank in der Reha auf.

## Fazit

Hank Moody ist endlich zurück und das kann gar nicht schlecht sein. Mir gefällt dabei sehr, dass im Zentrum der Handlung nicht die unglückliche Liebe zwischen Hank und Karen steht und in dieser Hinsicht vorerst alles gut ist. Meiner Meinung nach ist es für Charlie und Marcy langsam Zeit wieder zueinander zu finden. Beide hatten gescheiterte Beziehungen zwischendurch, was kann jetzt noch kommen? Aber ob das noch was wird? Und wird aus Beccas Berufswunsch tatsächlich Realität? Nachdem eigentlich alle losen Handlungsstränge am Ende der letzten Staffel ihren Abschluss fanden, bleibt abzuwarten, welche Seltsamkeiten in den nächsten Episoden folgen werden.
