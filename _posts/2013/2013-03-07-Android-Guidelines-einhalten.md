---
layout: post
title: "Android Guidelines einhalten?"
description: "Was ich an meiner App geändert habe und wieso Guideline mal mehr und mal weniger sinnvoll sind."
date: 2013-03-07 
category: it
tags: [Android, Design]
published: true
comments: false
share: true
---

Ich arbeite seit einiger Zeit an einer App und da sie schick aussehen soll, wollte ich nicht unbedingt die Android Themes benutzen. Sie tun zwar ihren Dienst, aber wenn man etwas bestimmtes haben möchte, muss man meist selbst Hand anlegen. So kam es also, dass ich eigene PNG-Dateien für meine Buttons erstellte und sie in das benötigte PNG9 Format umwandelte.

Es war einfach leichter, deshalb waren meine ersten Buttons eckig. Ein Kommilitone wies mich später darauf hin, dass eckig nicht toll aussieht und eher so 90er Jahre sei. Zu dem Zeitpunkt war von Windows Phone noch nicht so viel zu sehen und von Windows 8 gar nichts.      
Ich änderte die Buttons etwas später und sie wurden rund. Die runden Grafiken habe ich nicht mehr, aber einige Bilder aus der Version der App, die sie noch hatte.

Ich fand es in rund nicht schlecht, mochte aber auch die eckigen. Nun gab es einige Änderungen im Projekt und die App sollte für kleine Tablets angepasst werden. Im Zuge dieser Neuerungen überarbeite ich die App vollständig und habe auch über das Design nachgedacht. 

## Android Guidelines

Ich weiß zwar nicht genau welche, aber ab Android 3.x hat Google relativ umfassende Design Guidelines herausgegeben. Sie legen sehr genau fest, wie groß Icons sein sollten und generell das Design diverser Elemente einer App. Sie stellen nur eine Richtlinie und keinen Zwang dar. Man kann designen was man will, aber es empfiehlt sich die Guidelines zu befolgen.

Google richtet sich nach seinen eigenen Regeln und vor allem die bekannten Apps tun dies auch. Genau wie bei dem Softwareinterface von iOS, erwartet ein Android-Benutzer eine gewisse Linie, die sich durch alle Apps zieht. Und ein relativ auffälliges Merkmal sind eben Buttons. Das Android-System nutzt eckige Buttons und in den Guidelines werden auch genau diese empfohlen. Was also tun?      
Wenn man ein ausgesprochen neuartiges und womöglich einzigartiges Design für die App benutzt, kann das auch gerne gegen die Richtlinien verstoßen. Ich stellte mir selbst die Frage, ob das bei meiner App zutrifft und ich musste nicht lange nachdenken: Nein. Mein Design ist eher simpel und keinesfalls innovativ. Warum sollte ich den Benutzer mit runden Buttons abschrecken? 

Das vorläufige ReDesign sieht wieder eckig aus und ich mag es trotzdem sehr. Bei Gelegenheit werde ich die Guidelines noch einmal unter die Lupe nehmen und Unstimmigkeiten ändern.

## Ausnahmen gibt es auch

Es gibt, wie oben angedeutet, immer wieder sinnvolle Ausnahmen. Ich lasse mir beispielsweise nicht vorschreiben, wie meine Icons auszusehen haben und wie viel Pixel zum Rand sein müssen.      
Was ich in meiner App auch ändere ist die Funktionalität des Back-Buttons. Das System baut intern einen Stapel von Activities auf und bei Klick auf den Back-Button wird dieser Stapel abgearbeitet. Ich möchte aber lieber eine hierarchische Navigation durch meine App. 

Es kommt immer von Fall zu Fall an. Aber man sollte doch versuchen sich an die Guidelines zu halten.