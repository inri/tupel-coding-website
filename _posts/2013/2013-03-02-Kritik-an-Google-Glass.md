---
layout: post
title: "Kritik an Google Glass"
description: ""
date: 2013-03-02 
category: it
tags: [Google, Glass]
published: true
comments: false
share: true
---

Bisher habe ich zwei kurze Kommentare zur Google Glass geschrieben. In den letzten Tagen las ich weitere gute Gedanken und vor allem auch berechtigte Zweifel zu Googles Innovation. 


Ausgelöst hat diese Kommentare womöglich der arrogante Satz von Google-Gründer, Milliardär und Antriebskraft hinter Glass, Sergey Brin, welcher auf einer TED-Konferenz sagte, dass *das Berühren eines Glasbildschirms entmannend ist*. Die Lösung kann natürlich nur die Google Glass sein.

## Barriere sozialer Interaktionen

Ein Kritikpunkt ist die ständige Anwesenheit der digitalen Brille. Wenn ich mit Freunden einen trinken gehe und dann beginne mit meinem Smartphone rumzuspielen, ist das unhöflich. Ich signalisiere dadurch, dass meine Aufmerksamkeit nicht mehr ihnen, sondern meinem Telefon gilt. Lege ich mein Smartphone aus der Hand und stecke es vielleicht sogar in die Tasche, verschiebt sich meine Aufmerksamkeit.       
Aber wie soll das mit einer Brille gehen? Solange ich sie nicht abnehme, weiß mein Gesprächspartner nicht, ob ich ihm zuhöre, oder auf irgendwas anderes achte. In einer ruhigen Umgebung wie einer Bar oder Restaurant kann man sie sicherlich abnehmen oder wie John Gruber hofft, wird sie vielleicht sogar in dieser Art Öffentlichkeit verboten. Aber auf der Straße wird man sie nicht auf- und absetzen wollen.      
Sie wird eine Barriere darstellen. Es stellt sich nur die Frage: Werden wir uns daran gewöhnen? 

## Stets Videoaufnahmen möglich

Ich kann mir vorstellen, dass man sich an den Anblick einer Person mit Glass in einigen Jahren gewöhnen kann. Aber zwei Aspekte habe ich bisher noch gar nicht auf dem Schirm gehabt. [Mark Hurst](http://creativegood.com/blog/the-google-glass-feature-no-one-is-talking-about/) beschreibt diese neue Art von *Gefahr* sehr ausführlich. Eines der wichtigsten Features der Glass ist das Aufnehmen von Fotos, Videos und sicherlich auch Tonaufnahmen. Aber woher weiß ich als Außenstehender, ob der Typ da vorne mit der Google Glass nicht gerade ein Video von mir macht? Man kann es nicht wissen!

Ein Vergleich mit Smartphones ist fehl am Platz, denn auch wenn diese mittlerweile die gleichen Funktionen haben, muss ich das Gerät dabei in der Hand halten. Das fällt auf. Aber eine solche Brille nicht. Wenn das Design noch angepasst wird, erkennt man sie vielleicht nur noch wenn man ganz genau hinschaut.       
Schließt mal eure Augen und stellt euch diese Situation vor. Praktisch jeder kann jederzeit in der Öffentlichkeit gefilmt oder fotografiert werden und weiß es vermutlich nicht einmal. 
Ja, Überwachungskameras gibt es auch schon an vielen Ecken, aber da gibt es den zweiten Aspekt.

## Was passiert mit den Aufnahmen?

Aktuell reden wir von Google. Am liebsten möchte Google alle Arten von Daten in seine Cloud schieben. Ihre Chromebooks sind das beste Beispiel dafür. Auch bei der Google Glass wird das kaum anders sein. Und natürlich tun sie das nicht aus Nächstenliebe, sondern um Informationen zu erlangen. Dieser Konzern lebt schließlich von Informationen, die er entsprechend auswertet und aufbereitet.      
Aber woher weiß der Besitzer der Google Glass, was mit den hochgeladenen Videos, Bildern und Tonaufnahmen passiert? Wie werden sie analysiert? Wird gesprochener Text extrahiert? Werden Gesichter zu Personen zugeordnet? Was passiert wenn der Benutzer eine Datei löscht? Ist sie dann wirklich gelöscht? Haben Regierungsbehörden Zugriff auf diese Daten? 

*Hej, du warst gestern im Umkreis eines Verbrechens, jetzt wollen wir deine aufgenommenen Daten einsehen!* 

Was können die Aufgenommenen machen? Ja, sie wissen meist gar nicht, dass sie aufgenommen wurde, aber was wenn doch? Sollten sie sich an Google wenden? Überwachungskameras speichern ihre Aufnahmen meist nicht besonders lange und deren Qualität ist auch noch eine andere Welt.

In dem Batman-Film *The Dark Knight* gibt es am Ende diese Szene, in der Batman alle Mobiltelefone in Gotham City anzapft und deren Daten auswerten lässt. Guten Morgen, mit Google Glass ist genau das möglich.

## Fazit

Hurst bringt die Zweifel an der Glass mit diesem Satz auf den Punkt:

*Die wichtigste Google Glass Erfahrung ist nicht die Nutzererfahrung, sondern die von allen anderen.*

Es geht gar nicht um die Frage, ob eine solche Datenbrille ein sinnvolles Produkt ist. Es sollte um die Frage gehen, ob die Gesellschaft ein solches Gerät akzeptieren kann. Ein Gerät über dessen eigentliche Macht ein privater Konzern verfügt.
Darüber sollten wir gründlich nachdenken.