---
layout: post
title: "Im Aquarium Berlin"
category: blog
tags: [Sony RX100 MII, Fotografie]
comments: false
share: true
image:
  feature: 2013-aquarium-qualle-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
thumb: 2013-aquarium-qualle-lg-200.jpg
---

Ich war mit meiner Liebsten im [Aquarium Berlin](http://www.aquarium-berlin.de/aquarium.html) und habe mit meiner neuen Kamera herumgespielt. Aquarien sind relativ interessante Orte zum Fotografieren, denn man steht zwei großen Herausforderungen gegenüber: Es ist meist sehr dunkel und die Scheiben spiegeln. 


Bei uns kam eine weitere Schwierigkeit in Gestalt von tausend Familien mit Kleinkindern hinzu. Das führte dazu, dass einem nicht sehr viel Zeit zum Fotografieren blieb weil ständig Menschen vor die Linse gelaufen sind.

Mir ist auch aufgefallen, dass verdammt viel fotografiert wurde und alle Arten von Kameras waren vertreten - von der dicken Spiegelreflex über Bridge-Kameras bis hin zu Mobiltelefonen. Vor zwei Jahren bin ich mit meiner Superzoom-Kamera (eine [Lumix FZ28](http://www.panasonic.de/html/de_DE/Produkte/Produkt-Archiv/Lumix/DMC-FZ28/%C3%9Cbersicht/1265374/index.html)) im *Sealife* gewesen und scheiterte kläglich an den Lichtverhältnissen dort. Kaum ein Foto davon war zu gebrauchen und genau das wird auch bei bestimmt 90% der Besucher der Fall sein. 

Leider habe ich größtenteils nur mit der Automatik der Sony gearbeitet und das war schon ein Aufwand. Durch die längere Belichtungszeit, die in einem solchen Aquarium notwendig ist, sind alle Bewegungen unscharf und die blöden Fische bewegen sich ziemlich viel. Von Unterwasserlebewesen habe ich nur einige brauchbare Fotos. Bei den Reptilien war es schon etwas besser, aber auch da hat man immer noch die Scheiben vor der Linse.

Wenn ich mich nicht irre, dann ist auf dem Titelfoto eine Galeerenqualle zu sehen. Die Quallen waren ziemlich dankbare Motive, denn sie bewegten sich nicht zu schnell und bei ihnen fallen Unschärfen nicht sehr auf. Auf das Titelfoto bin ich ziemlich stolz. Mir gefallen die Blautöne und die Partikel im Wasser wirken fast wie Sterne im Weltall. Ich hätte noch einige gute Quallen-Fotos, aber das ist mein Favorit.

Nun noch einige weitere Motive. Für eine größere Ansicht einfach auf die Bilder klicken. Wenn Interesse besteht, kann ich die Motive gerne auch als Desktophintergründe bereitstellen.

<figure>
	<a href="/images/2013-aquarium-seepferd-1600.jpg"><img src="/images/2013-aquarium-seepferd-800.jpg" alt="Seepferdchen im Aquarium Berlin"></a>
	<figcaption>Auch die niedlichen Seepferdchen haben sich ständig hin und her bewegt. Dieser Kollege hier ist zwar auch nicht scharf, aber scharf genug und außerdem hat das Bild eine interessante Komposition.</figcaption>
</figure>
<br/>

<figure>
	<a href="/images/2013-aquarium-frosch-1600.jpg"><img src="/images/2013-aquarium-frosch-800.jpg" alt="Grüner Frosch im Aquarium Berlin"></a>
	<figcaption>Dieser Frosch saß wie die meisten Frösche im Aquarium still da und ließ sich wunderbar fotografieren. Die Scheibe machte einige Probleme, was man auf dem rechten Seite des Bildes auch erkennen kann.</figcaption>
</figure>
<br/>

<figure>
	<a href="/images/2013-aquarium-spinne-1600.jpg"><img src="/images/2013-aquarium-spinne-800.jpg" alt="Spinne im Aquarium Berlin"></a>
	<figcaption>Bei diesem Bild habe ich mich sehr über das scharfe Netz der Spinne gefreut.</figcaption>
</figure>
<br/>

<figure>
	<a href="/images/2013-aquarium-sophie-1600.jpg"><img src="/images/2013-aquarium-sophie-800.jpg" alt="Sophie im Aquarium"></a>
	<figcaption>Menschen zu fotografieren war wesentlich unproblematischer.</figcaption>
</figure>