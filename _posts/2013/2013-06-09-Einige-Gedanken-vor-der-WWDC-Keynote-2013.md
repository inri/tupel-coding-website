---
layout: post
title: "Einige Gedanken vor der WWDC Keynote 2013"
description: ""
date: 2013-06-09 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Morgen um diese Zeit werden die Live-Ticker gefüllt sein und man erfährt in welche Richtung es dieses Jahr mit Apple geht. Im Fokus der letztwöchigen WWDC Bericht- und Gerüchterstattung lag das neue Design vom kommenden iOS 7 unter Federführung von John Ive und eine Art Radiodienst von Apple.

## Flat Design

Die unzähligen Designstudien zum neuen iOS gingen mir persönlich ziemlich auf die Nerven. Ich habe mir die ersten zwei genauer angeschaut und danach alle weiteren ignoriert.     
Viele denken, es wird ein flaches Design, was genau das auch immer bedeuten mag. Ein iOS in Windows Phone 8 Stil wird es wohl kaum werden. Auf jeden Fall verschwinden die skeuomorphen Elemente und das war in den letzten Monaten schon bei diversen Apps von Apple zu sehen.       
Grundsätzlich glaube ich, dass es keine extreme Veränderung sein wird. Mit Sicherheit können 80% aller iOS-Nutzer noch eine ganze Weile ganz zufrieden mit dem bestehenden UI leben. Das bedeutet natürlich nicht, dass es nicht einige Ecken und Kanten gibt, die in iOS 7 ausgebessert werden sollten. Aber mit Sicherheit werden Kommentatoren enttäuscht sein und mangelnde Innovationsfähigkeit sehen.

Übrigens hoffe ich, dass das Redesign nicht im Fokus der Keynote steht.

## iRadio

Zur Zeit sind die Streaming-Dienste ziemlich erfolgreich, siehe Spotify u.a.. Die Idee ist so simpel wie genial: Man bezahlt nicht mehr pro Musikstück einen Betrag, sondern einen monatlichen Obolus für eine große Auswahl an Musik. Von verschiedenen Endgeräten aus kann man so zu jeder Zeit jedes beliebige Musikstück hören. Es ist nicht verwunderlich, dass Apple in dieses Geschäftsmodell mit einsteigt. Die große Frage ist nur: Wie wird der Dienst von Apple aussehen?
Da gibt es unter anderem zwei ziemlich interessante Gerüchte. Nummer 1: iRadio wird in iOS 7 integriert und kostenlos sein. Und Nummer 2: Apple wird in iRadio Werbung schalten, vermutlich ähnlich wie im Radio.

Doch auch hier hoffe ich, dass iRadio nicht das große Neue an iOS 7 sein wird. Diese Art Dienst zu integrieren, in welcher Form auch immer, ist einfach nur ein logischer Schritt um die Kunden bei Laune zu halten. Wenn ich im Apple-Universum verankert bin und meine Musik bisher in iTunes gekauft habe, möchte ich für die nächste Stufe des Musikkonsums nicht auf einen neuen Anbieter umsteigen müssen. Ganz einfach.      
Ich bin mir nicht sicher, ob dieser Dienst für die meisten Länder sofort verfügbar sein wird.

## Feature-Pflege

Wenn ich vom neuen Design und iRadio nicht viel erwarte, was sonst? Keine Ahnung. Ich sehe keine bestimmte Funktionalität, die in meinen Augen essentiell von Bedeutung wäre. Also auch kein Feature von Android beispielsweise, dass unbedingt auch Teil von iOS werden muss. Außer... vielleicht Widgets. Aber womöglich spricht nur der Nerd in mir diesen Wunsch aus. Ob das aktuelle Homescreen-Konzept aus Reihen von Apps und der komplette Bildschirm für eine App nicht doch Platz für Verbesserungen lässt? Oder sagen wir *Weiterentwicklungen*.

Ansonsten wird Apple weiterhin die eigenen Features pflegen, ob man am Montag darüber redet oder nicht. Viele Entwickler wünschen sich eine bessere iCloud API. Siri hat Konkurrenz und diese schläft nicht. Es gibt weitere Beispiele.

## Fazit

Ich bin gespannt, aber gerade im Moment kann ich nicht besonders viele Ideen mitteilen. Auch zu OS X fällt mir nichts ein. Entweder bin ich besonders zufrieden oder gerade ist nicht der beste Zeitpunkt meine Gedanken zu Papier zu bringen. Vielleicht ist es nach der Keynote besser.      
Ach ja, eventuell bekommen wir einen ersten Vorgeschmack auf ein iTV, aber dazu würden die Poster von Apple nicht passen, denn die beziehen sich auf iOS 7 und OS X.      
Ob wohl stattdessen die iWatch eine Rolle spielt? Tim Cook meinte ja schon, dass diese Art Device keine großen Gewinnmargen verspricht. Würde sich eine alleinige Keynote-Vorstellung lohnen oder zeigt man sie morgen zusammen mit iOS 7? Das wäre immerhin etwas großes Neues.

**Nachtrag:**       
[Gruber](http://daringfireball.net/2013/06/wwdc_2013_expectations) freut sich darüber, dass noch nicht so richtig klar ist, was Apple morgen vorstellen wird und führt dies auf die bessere Geheimhaltung bei Apple zurück. Ich bin mir nicht sicher, ob das der Grund dafür ist oder ob es einfach keine großen Neuerungen und Features gibt, die man sehnlichst erwartet.