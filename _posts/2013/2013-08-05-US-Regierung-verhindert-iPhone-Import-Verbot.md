---
layout: post
title: "US-Regierung verhindert iPhone-Import-Verbot"
date: 2013-08-05 18:00:00
category: it
tags: [Apple, Samsung]
published: true
comments: false
share: true
---

Letzte Woche waren schon einige Stimmen zu hören, die forderten, das drohende US-Import-Verbot für ältere iPhones und iPads zu verhindern. Am Wochenende geschah genau das und natürlich gibt es jetzt von der einen Seite Lob und von der anderen Unverständnis. 


Beispielsweise [fefe drückte seinen Unmut](http://blog.fefe.de/?ts=af034e51) folgendermaßen aus:

> Die TUN nicht mal mehr so, als ginge es bei ihrem Rechtssystem um Gerechtigkeit! Klar, wir haben hier Gerichte und Behörden und so, aber deren Rechtssprüche gelten nur, wenn sie uns gefallen.

Gerechtigkeit ist genau in diesem Fall aber der Knackpunkt.

Die ersten Kommentare am Wochenende begründeten den Eingriff der Regierung in die richterliche Entscheidung mit der Art von Patent, um die es ging: FRAND-Patente. Also Grundlagenpatente, die essentiell für ein Produkt sind und deshalb *zu fairen, vernünftigen und diskriminierungsfreien Bedingungen* lizensiert werden müssen.

Diese Bedingungen sind sicherlich eine Sache der Auslegung und dass es gerade zwischen Apple und Samsung darüber Streit gibt, ist nicht verwunderlich. Nachvollziehbar ist allerdings, dass Samsung in der gleichen Streitsache bei noch keinem europäischen Gericht Erfolg hatte. Ich glaube es wurde nachgewiesen, dass Apple die Patente nutzt, aber ein Import-Verbot eben wegen dieser FRAND-Patente wollte kein Gericht aussprechen.

Dass die US-Regierung ihrer eigenen Wirtschaft nicht schaden möchte, wundert mich nun überhaupt nicht. Das allein wäre schon ein guter Grund für ein Veto. Aber selbst wenn das nicht der Grund gewesen sein soll, ist die Ablehnung des Verbots aufgrund der Grundlagenpatente auch verständlich und sogar begrüßenswert.
Ein Verbot hätte eine fatale Signalwirkung gehabt, besonders da es sich eben um FRAND-Patente handelt.

## Verhandlungen mit Samsung

Nun sieht es so aus, als ob Apple von der eigenen Regierung geschützt wird und Samsung um sein Recht betrogen worden ist. Doch dann stellt sich die Frage, was wollte denn Samsung für diese Patente eigentlich von Apple haben? [Der iPhoneBlog hat einige Antworten parat](http://www.iphoneblog.de/2013/08/05/samsungs-desinteresse-an-fairen-vernuenftigen-und-diskriminierungsfreien-lizenzdeals-fuer-standard-patente/): 2,4% des durchschnittlichen Verkaufspreises eines iPhones, also ungefähr 16$.
Es ist nicht so, dass ich mit meinem Expertenwissen einschätzen könnte, wie angemessen oder nicht dieser Betrag ist, aber mein kleiner Zeh sagt mir, er ist es nicht. Schaut euch dazu mal die [iPhone-Verkaufszahlen](https://de.wikipedia.org/wiki/Apple_iPhone#Verkaufszahlen) an und denkt selbst darüber nach.

Dazu kommen weitere Fragen. Wieso beweist Samsung nicht, dass andere Hersteller einen ähnlichen Prozentsatz bezahlen? Wieso soll Apple für die Patente bezahlen, wenn Qualcomm und Infineon das schon tun (Apple kauft von denen die Chips)? Außerdem soll auch Samsungs Beteiligung an den Chip sehr minimal und fragwürdig sein. Andere Quellen behaupten, dass Samsung die Patente gar nicht lizensieren wollte wenn Apple im Gegenzug nicht seine iPhone-Patente anbietet. Genau diese sind aber eben nicht FRAND.

Was wirklich hinter den Kulissen passiert ist, werden wir wohl nie erfahren. Aber bisher sieht für mich das Eingreifen der US-Regierung nicht wie ein willkürlicher Akt aus.