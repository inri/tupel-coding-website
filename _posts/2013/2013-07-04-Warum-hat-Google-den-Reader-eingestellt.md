---
layout: post
title: "Warum hat Google den Reader eingestellt"
description: ""
date: 2013-07-04
tags: [Google, RSS]
category: it
published: true
comments: false
share: true
---

Das Erstaunen über Googles Ankündigung, ihren RSS-Dienst Reader nach dem 01.07.2013 einzustellen, war nicht gerade klein. 


Viele hat es verwundert, dass Google freiwillig das Potential dieser Menge an Daten verschenkt. Auch über mich würde Google wesentlich mehr erfahren, wenn sie meine RSS-Feeds interpretieren würden. Aber der Google Reader ist jetzt Geschichte und noch immer wird über die Gründe spekuliert.

Kosten: Der Dienst hinter Google Reader ist nicht trivial und die Nutzerzahl war hoch, aber können Kosten tatsächlich ein Grund gewesen sein? Eher nicht. Hinter vorgehaltener Hand sagt man, dass der Dienst seit Jahren relativ stabil lief und kaum gewartet werden musste. Neue Features gab es kaum noch.

Benutzung: Offiziell gibt Google an, dass die Anzahl Benutzer geringer geworden ist. Klar, was niemand benutzt, muss man ja auch nicht weiter unterhalten. Aber ob diese Tatsache wirklich stimmt? Marco Arment kann sich das nur vorstellen, wenn Clients nicht mitgezählt werden. Da ich tatsächlich meistens mit Reeder meine RSS-Feeds konsumiere und sicherlich nicht der einzige damit bin, trifft das vielleicht zu.

## Lockdown

[Marco bezeichnet](http://www.marco.org/2013/07/03/lockdown) den wichtigsten Grund für Googles Entscheidung als Lockdown. Und dieser Meinung würde ich mich voll anschließen. Begonnen hat seiner Meinung nach Facebook mit dieser Art *Krieg*. Plattformen wie Facebook und eben auch Google+ wollen die Nutzer nicht nur auf ihre Seiten locken, sondern sie sollen auch gleich dort bleiben. Inhalte sollen nur da konsumiert werden. Deshalb kann man gleich bequem von dort aus mit Freunden und Bekannten Chatten, seine Bilder und Videos hochladen und sich die Zeit mit Spielen vertreiben.

Der Google Reader ist im Prinzip genau das Gegenteil davon. Der Dienst existiert für sich allein und ist nicht an Google+ angeschlossen. Zudem hat er eine API, die es jedem erlaubt eigene Programme zu schreiben und dadurch verbringen die Nutzer noch weniger Zeit auf der Plattform direkt im Browser. Oder wie es Marco auf den Punkt bringt:

*[...] it’s completely open, decentralized, and owned by nobody, just like the web itself. It allows anyone, large or small, to build something new and disrupt anyone else they’d like because nobody has to fly six salespeople out first to work out a partnership with anyone else’s salespeople.*

Ganz ähnlich [sieht auch der Entwickler des Google Reader die Situation](http://www.forbes.com/sites/alexkantrowitz/2013/07/01/google-reader-founder-i-never-would-have-founded-reader-inside-todays-google/) und würde beim heutigen Google den Reader nicht entwickeln, sondern lieber selbstständig umsetzen. Er sagt sogar wortwörtlich, dass er es hassen würde, seine Idee gegen Google+ zu sehen.

Google ist ein großes Unternehmen und verfolgt seine Interessen, daran ist nichts verwerflich. Oder vielleicht doch? Marco kritisiert die großen Webunternehmen für ihre Haltung, in allem web-nativem eine Gefahr zu sehen, besonders, da sie ohne das freie Web nicht existieren würden. Sie verfolgen nur noch das Interesse größere properitäre Mauern zu bauen. Unrecht hat er damit nicht.

In die gleiche Kerbe stößt [dieser Beitrag über Facebook](http://www.zwentner.com/?p=565). Kurz gefasst: Ihm ging die Werbung auf die Nerven, die schlechten Inhalte (*Facebook ist die Bildzeitung des Web*) und dass alle seine Daten Facebook gehören und er sich nur in einem *walled garden* bewegt.

## Fazit

Das *freie Web* ist nicht unbedingt im Interesse großer Unternehmen, deren Geschäftsmodell von den Benutzern der eigenen Dienste abhängig ist. Ohne Google würde das Internet heute anders und nicht unbedingt besser aussehen. In Zukunft sollte die Webgemeinde trotzdem ein Auge auf die Entwicklung der großen Plattformen haben. Besorgniserregend ist das passende Wort.