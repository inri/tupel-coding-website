---
layout: post
title: "MDSE, das Meta Model"
description: ""
date: 2013-02-08 
category: it
tags: [HPI, Studium]
published: true
comments: false
share: true
---

Ich habe in diesem Semester Modellgetriebe Softwareentwicklung (MDSE) belegt und möchte meine Projektergebnisse hier vorstellen. Den Anfang der Reihe macht eine kurze Einführung zur Vorlesung, dem Projekt und wie der Titel schon verrät: Das Metamodell. 

## Praktisches Projekt

Ursprünglich war diese Veranstaltung den Masterstudenten vorbehalten, aber man wollte sie nun auch den Bachelorstudenten anbieten. Im Gegensatz zu früheren vorlesungsbegleitenden Übungen, gab es bis Mitte Dezember nur Vorlesung und im Anschluss nur das praktische Projekt. Aufgabe war die Entwicklung einer Domain-spezifischen Sprache mit allem drum und dran. Beginnend beim Metamodell, über Checks, der DSL selbstverständlich, einer Code Generierung und zum Abschluss ein Modelltransformation mit ATL.

Vorlage für das Projekt ist die Idee eines automatischen Haussystems. Man hat bestimmte Sensoren (Schalter, Temperatur,...) und Aktuatoren (Lampe, Heizung) für das gesamte Haus und einzelne Räume. Der Nutzer kann Regeln definieren, was bei Zustandsänderung von Sensoren passieren soll. Ja und das ist es auch schon.

Wir erhielten einige vorgegebene Sensoren und Aktuatoren, die jeweils von abstrakten Klassen erben. Des weiteren ist das Konstrukt der Regeln interessant. Sie sind in if- und then-Parts unterteilt. Für beide gibt es gibt u.a. zwei Möglichkeiten wie sie implementiert werden können, generisch oder eben nicht generisch. Die Lösung der anderen Teams war das Definieren von drei verschiedenen Bedingungen: LessThan-, Greater Than- und Equal-Conditions.      
Mit der Klasse an sich war definiert welcher Operand genutzt wird und das konnte man mit unseren Vorgaben auch sehr gut machen. Es gab nämlich nur zwei verschiedene Arten von Werten: Zahlen und Boolsche Werte (true, false). Also hatte die EqualCondition immer ein bool-Wert als Attribut und die anderen beiden entsprechend Int.

## Metamodell

Unser Metamodell ist in diesem Punkt etwas anders. Wir haben jeweils nur eine Art Condition und Action, die aber die Operanden als Attribut haben und die möglichen Werte ebenfalls. Letzteres werde ich noch ändern, denn das ist gerade nicht sehr generisch. Optimal wäre eine abstrakte Value-Klasse, von der man entsprechend die Werte referenzieren kann, die der if/then-Part gerade benötigt. Mit unserer Methode kann man weitere Typen an Attributen einbinden in dem man einfach die Value-Klasse erweitern würde. Bei der ersten beschriebenen Lösung müssten die drei Klassen entweder weitere Attribute bekommen, was zu unserem Problem führt, oder es müssen völlig neue Condition- und Action-Klassen gebildet werden. 

Das if-Konstrukt sieht vielleicht etwas verwirrend aus. Damit soll der Benutzer Bedingungen in And und Or schachteln können. Hier wären auch weitere Möglichkeiten denkbar (Negation).

<figure>
    <a href="/images/2013-modelling_metamodel-1600.jpg">
	   <img 
		  src="/images/2013-modelling_metamodel-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Metamodell</figcaption>
</figure>