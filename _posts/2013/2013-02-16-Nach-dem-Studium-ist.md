---
layout: post
title: "Nach dem Studium"
description: "Was kann man nach dem Studium mit einem Bachelorabschluss machen?"
date: 2013-02-16 
category: blog
tags: [Studium]
published: true
comments: false
share: true
---

Es ist wieder mal Zeit für ungeordnete Gedanken rund um das Studentendasein.

## Und nach dem Bachelor?

Meine Freundin und ich planen so langsam unsere Zeit nach dem Bachelorstudium, also was wir ab Oktober machen werden. Peilen wir einen Master an, den Berufseinstieg oder irgendwas völlig anderes? Wir wissen es noch nicht sicher.

Was mich angeht, ich habe große Lust auf eine praktische Tätigkeit, kann mir aber sehr gut ein weiteres Studium vorstellen. Aber: Was mir am Studieren missfällt, zumindest jetzt im Bachelor, dass man viel zu viele *unwichtige* Kurse zu belegen und bestehen hat. Wieso muss ich mich beispielsweise ein Jahr lang durch Theoretische Informatik quälen bzw. den Klausuren? Nein, Allgemeinwissen oder Basiswissen ist kein guter Grund. Gerade Theoretische Informatik 2 begreifen sowieso die allerwenigsten und retten sich mit einer 3,x. Dann gibt es eben noch die, die es begreifen und denen es Spaß macht und die sollen es wegen mir machen. Für die anderen müsste man sich eine andere Lehrform mit anderen Lehrinhalten ausdenken.

Ich will nur sagen, dass mir schon der Bachelor in einigen Punkten schwerfällt und ich zu wenig in den Gebieten machen kann, die mich interessieren, bspw. in der Softwareentwicklung. Ich hätte wie gesagt nichts gegen ein Masterstudium, aber es müsste viele Bereiche und Gebiete abdecken, in denen ich später tätig sein möchte. Beispielsweise der Informatik-Masterstudiengang in Halle, meine Freundin zieht Halle in Betracht, hat theoretische Informatik auf dem Lehrplan. Nein danke. Überhaupt habe ich bei viel zu vielen Themen dort ein Déjà-vu. 

Wenn ihr ein Studiengang kennt, der eher in Richtung Softwareentwicklung und noch viel lieber auch Design geht, sagt mir Bescheid!

## Bereit für die richtige Welt?

Wenn es kein Masterstudium wird, was dann? Ich habe zwar Lust auf die Praxis, aber fühle mich nicht annähernd bereit dafür. Ich nenne das liebevoll den Bacheloreffekt. So ein Bachelorabschluss ist nichts halbes und nichts ganzes. Ich finde schon, dass er viel Wissen vermittelt, aber das wird in der Gesellschaft noch nicht wertgeschätzt. Oft muss man sich erklären und sieht im Gesicht des Gegenüber die Zweifel, ob das überhaupt ein richtiger Abschluss ist. Und dann frage ich mich, wie das die Personalleiter sehen. Und welchen Wert hat das HPI als Ausbildungsstätte? 

Dazu kommt das Entstehen der Bachelornote. Fast jeder Kurs fließt in diese Note mit ein. Aber welcher Aussagekraft hat eine 3 Jahre alte Note noch? Meine Noten sind von Semester zu Semester besser geworden, aber das schlägt sich nur bedingt in der Endnote nieder.      
Die Alternative wäre wohl das Diplom gewesen. Da buckelt man sich zum Vordiplom, ist im Stoff und sattelfest und beginnt dann erst zwei Jahre lang die Kurse zu besuchen, die für die Abschlussnote relevant sind. Der Master ist leider nicht der Ersatz dafür, denn Masterplätze sind eben nicht für alle Studenten vorgesehen und dieser Abschluss geht ja auch eher in Richtung wissenschaftlichem Arbeiten. An und für sich ist das nicht tragisch und die Trennung sinnvoll, nur leider haben sie zu viel Stoff in den Bachelor gestopft und vergessen Promotion zu betreiben. Zumindest einige Bachelorstudiengänge erfordern viel Fleiß und Zeitaufwand, und belohnen das nicht unbedingt in der Abschlussnote.
