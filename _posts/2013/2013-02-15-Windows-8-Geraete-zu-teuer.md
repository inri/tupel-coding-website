---
layout: post
title: "Windows 8 Geräte zu teuer"
description: ""
date: 2013-02-15 
category: it
tags: [Microsoft]
published: true
comments: false
share: true
---

In einem [interessanten Beitrag diskutiert](http://winsupersite.com/windows-8/explaining-windows-8-pc-sales-over-holidays?page=1) Paul Thurrott eine mögliche Erklärung für die schlechten Verkaufszahlen von Windows 8 Geräten (PCs, Notebooks, Tablets, Touch-Notebooks). Zusammengefasst: Sie sind zu teuer.

Er beschreibt zunächst die Situation mit Windows 7, welches vor einem Jahr noch öfter verkauft wurde als jetzt Windows 8. Demnach kam ein nicht unwesentlicher Anteil der verkauften Lizenzen von enorm billigen Geräten, wie beispielsweise Netbooks. Der Durchschnittspreis für ein Windows 7-Notebook lag bei 420$. Diese billigen Geräte waren einfach nur billiger Mist. Mit Windows 8 verlangt Microsoft bessere Hardware und zum Beispiel Multi-Touch-Fähigkeiten, welche die meisten der billigen Geräte nie hatten.

## Billige Geräte in der Vergangenheit

Der Preiskampf der letzten Jahre hat zu drei Dingen geführt, wovon nur eines positiv ist.     
Windows 7 hat eine große Verbreitung erfahren (gut!).     
Die Käufer haben sich an geringe Preise gewöhnt (schlecht!).      
Das ständige Unterbieten der Preise hat bei den Unternehmen zu geringen Profiten geführt (schlecht!).

Der letzte Punkt ist vielleicht verwirrend. Einige Unternehmen haben von dem Netbook-Boom enorm profitiert, aber denen geht es nun, da die Verkaufszahlen einbrechen, plötzlich schlechter. Riesige Gewinne wurden nie eingefahren, obwohl stets viele billige Notebooks verkauft wurden. Das ist letztlich für Technologie und Forschung nicht so toll und rächt sich genau jetzt, wo ein Windows auf dem Markt ist, welches bessere Computer zu immer noch geringen Preisen verlangt. Wenn nach unten hin aber kein Platz ist, müssen die Geräte teurer werden und das verstehen die Kunden erst einmal nicht.

## Billige Tablets, jetzt

Bei Tablets sieht es dank Apple für die anderen noch ein wenig düsterer aus. Mittlerweile bekommt man ein iPad sogar schon für 330€, das große dann für 500€. Microsoft bot das erste Surface für 500$ an und das neue Pro ist mit 900$ noch erheblich teurer. Wenn man dann vor der Wahl steht, entscheidet man sich allein aufgrund des Preises wohl eher für ein iPad, Kindle oder Nexus.      
Generell tun sich alle Hersteller schwer mit den iPad-Preisen zu konkurrieren. So machen Amazon und Asus mit ihren kleinen Tablets fast keinen Gewinn. Und auf ihnen läuft Android, also ein System mit einer erheblich größeren Ökosystem an Apps und Medien als bei Microsoft.

Thurrott sieht nur eine Möglichkeit für Microsoft: Die Preise müssen gesenkt werden. 