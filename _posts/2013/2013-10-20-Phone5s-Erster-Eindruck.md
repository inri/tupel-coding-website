---
layout: post
title: "iPhone 5s, ein erster Eindruck"
date: 2013-10-20 16:00:00
category: it
tags: [iPhone]
published: true
comments: false
share: true
image: 
  feature: 2013-iphone-5s-volumes-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Meine letzten iGeräte habe ich hastig aus ihren Verpackungen befreit, mich kurz gefreut und sie dann eingeschaltet. Erst nach einiger Zeit fallen deshalb die Finessen des Gerätedesigns auf. Bei meinem iPhone 5s war es dank Vodafone mal anders. 


Ohne SIM ließ sich das iPhone nicht aktivieren respektive überhaupt benutzen. Die Software blieb verschlossen und ich konnte meinend volle Aufmerksamkeit dem wunderschönen Gerät widmen.   

Es folgen einige Erster-Eindruck-Stichpunkte. Ein Hinweis: Ich bin vom iPhone 4S auf das iPhone 5s gewechselt = Einige der folgenden Aspekte sind iPhone 5 Besitzern eventuell gar nicht neu.

* Es ist unglaublich leicht. Wenn man nicht wüsste wie viel Power in diesem leichten Gehäuse steckt, könnte man es auch für ein Dummy-Gehäuse halten. Wenn man es aber einige Minuten in der Hand hält, wird es schwerer und vergleicht man dazu das 4S, möchte man nicht tauschen.

* Entweder ist der Aluminiumrahmen meines alten iPhone 4S schon &quot;vergilbt&quot; oder der des 5s sieht in der Tat silberner aus. Der Rahmen wirkt durch seine Mattheit edler.

* Die beiden Volume-Button haben einen etwas geringeren Durchmesser als beim 4S.

* Der neue Home-Button mit TouchID fühlt sich beim Herunterdrücken anders an und klickt komisch. Der Grund ist TouchID, aber interessant ist es auf jeden Fall.

* Den Metallring als solchen spürt man beim Home-Button nicht.

* Die Verpackung legt den Fokus wieder auf das iPhone. Kein iCloud Icon mehr und kein seltsam schräg-oben Foto aufs Gerät. Besonders letzteres betont den Metallrahmen, welcher aber beim 4S nicht mehr neu war. 

* Die neuen Kopfhörer sitzen bei mir gut und sind angenehm. Der Klang ist in Ordnung, so gut er eben für diese Art Kopfhörer sein kann. Meine Sportkopfhörer von Sennheiser schneiden definitiv schlechter ab.

* Ich finde die Verpackung der EarPods auch ganz hübsch.

<figure>
    <a href="/images/2013-earpods-1600.jpg">
	   <img 
		  src="/images/2013-earpods-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>EarPods und Kabel des iPhone 5s</figcaption>
</figure>

* Spacegrau ist als Farbe ziemlich hell. Ich finde das grau meines iPad Minis viel *abgespaceter*. Im Prinzip hat Apple bei den iPhones nur noch helle Töne. Doch wieso? Bessere Beschichtung? Kratzer sieht man auf dem Mini auch kaum.

<figure>
    <a href="/images/2013-iphone-5s-ipad-1600.jpg">
	   <img 
		  src="/images/2013-iphone-5s-ipad-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>iPad Mini und iphone 5s im Farbvergleich</figcaption>
</figure>

* Die gefrästen Lautsprecherlöcher und der Lightning Anschluss sehen robuster und schicker aus.

### Fazit

Ein durch und durch wunderschönes Smartphone. Oder einfach gutes Design. In meinen Augen reicht zur Zeit kein anderes Smartphone da heran. 

