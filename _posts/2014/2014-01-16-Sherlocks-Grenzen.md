---
layout: post
title: "Sherlocks Grenzen"
date: 2014-01-16
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2014-sherlock-magnussen-lg.jpg
  credit: BBC
  creditlink: http://www.bbc.co.uk/programmes/b018ttws
---

Und da ist die dritte Staffel der britischen Krimiserie *Sherlock* auch schon wieder vorbei. Ich möchte kurz über das Ende der dritten und letzten Episode reden.  
  
Spoiler sind garantiert!

## Die Situation mit Magnussen

Sherlocks Aufgabe bestand darin, alle Akten (=Informationen), die der Medienmogul Magnussen über Watsons Frau besitzt, zu erlangen. Er ging davon aus, das sich unter dem Haus von Magnussen eine riesige Bibliothek mit expliziten Informationen über hunderte wichtige Personen befindet. Und mit genau diesen Informationen erpresst Magnussen die Personen.

Sherlock wollte den Regierungslaptop seines Bruders Mycroft gegen die besagten Akten tauschen. Dafür betäubte er diesen mitsamt den anderen Weihnachtsgästen und danach wurden er und Watson zu Magnussen geflogen. Dieser wiederum durchschaute Sherlocks eigentlichen Plan, die Polizei über das eingebaute GPS zu Magnussen zu locken und konfrontierte ihn mit der Wahrheit über seine Geheimnis-Bibliothek.

Sie existiert gar nicht. Er hat alle Informationen in seinem Kopf gespeichert, seinem *Mind Palace*. Die gleiche Methode verwendet auch Sherlock, doch er war sichtlich erschüttert über diese Wendung.   
Im Finale wird Watson von Magnussen gedemütigt und der freut sich auf die Verhaftung der beiden und kündigt an in seinen Presseblättern eine Kampagne gegen sie zu führen.

Sherlock sieht keinen anderen Ausweg und erschießt ihn mit einer Waffe die Watson in seinem Mantel mitgebracht hat.    
Boom.

## Pressefreiheit und Möglichkeiten

Diese Auflösung führte zu Unverständnis bei einigen Zuschauern. Sherlock weiß nicht mehr weiter und schießt einfach? Kein geniales Ende? Stefan Niggemeier twitterte dazu:

<blockquote class="twitter-tweet" lang="de"><p>Hm. <a href="https://twitter.com/search?q=%23Sherlock&amp;src=hash">#Sherlock</a> hat also nun quasi eine Methode gefunden, mit der man jedem Verbrecher das Handwerk legen kann? Hm. Hm. S03E03</p>&mdash; Stefan Niggemeier (@niggi) <a href="https://twitter.com/niggi/statuses/422803499374874624">13. Januar 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Welche Möglichkeiten hätte Sherlock denn sonst noch gehabt?       
Magnussen kennt die Geheimnisse der Personen und er kennt jedes Detail dieser Informationen, denn er selbst ist die Sammlung all dieser Daten. Die Daten sind in seinem Kopf gespeichert, nicht löschbar.

Die Writer der Episode haben nicht anderes gemacht, als das Problem der Pressefreiheit aufgedeckt. Denn genau dieses Problem schützt Magnussen, welcher ebenfalls ein Problem hat: Denn er besitzt **keine Beweise** für seine Informationen. Das erkennt auch Watson, worauf Magnussen ihm antwortet, dass er die Presse ist und es deshalb unerheblich ist, ob die Informationen bewiesen werden können. 

Selbst wenn am nächsten Tag abscheuliche Dinge in den Zeitungen stehen, die kaum bewiesen werden können, erfassen die Menschen diese Informationen. Ob wahr oder nicht, ist dann egal, denn der Samen sitzt bereits im Kopf und beginnt zu keimen *Vielleicht ist ja was dran...?*

Aber wie das mit Grundrechten so ist, muss die Gesellschaft fehlerhafte Vorgänge tolerieren. 

Ich wiederhole meine Frage: Wie hätte Sherlock anders reagieren können? Ich sehe keine andere Alternative. Das muss nicht heißen dass das nun die ultimative Methode ist, wie es Niggemeier so schön formuliert hat. Sherlock selbst sagt kurz bevor er zur Waffe greift *I am a highly functional sociopath!* und ich deute diese Worte als Eingeständnis, dass ihm bewusst ist, dass er Entscheidungen trifft, die ein normaler Mensch nicht treffen würde.

## Fazit

Ich mochte die dritte Staffel *Sherlock* und ich mochte auch die dritte Episode inklusive Finale. Einige Sekunden lang dachte ich, dass Watson zur Waffe greifen wird, aber das hätte dann eben nicht gepasst. 

Die Serie *Sherlock* betrachte ich eher als Krimi-Fiktion, weniger als britischen Tatort. Wir haben da diesen supergenialen Detektiv mit einem unglaublichen Gedächtnis, wieso sollte dann auch nicht ein passender Überbösewicht existieren? Ein Bösewicht, der dem Helden ebenbürtig und deshalb nicht zu schlagen ist, oder zumindest nicht mit den üblichen Waffen des Helden.    
Deshalb wählte ich auch mein Post-Titel. Diese Episode zeigt dem Zuschauer deutlich, dass der Held auch an Grenzen stoßen kann. 

In der Kritik stand auch der Mangel an episoden-umspannenden Detektivfällen und das ist nicht ganz von der Hand zu weisen. Dieser Preis musste für die Entwicklung der Hauptcharaktere gezahlt werden und den war ich auch gerne bereit zu zahlen. Es ist auch davon auszugehen, dass die Dramatik in den kommenden Staffeln etwas zurückgenommen wird und wieder spannende Fälle im Mittelpunkt stehen.

*Sherlock* ist für mich immer noch sehr gutes Fernsehen und der einzige Wermutstropfen ist die lange Wartezeit bis zur Fortsetzung.

#### (Das Ende der Episode)

Noch ein Wort zum eigentlichen Ende und dem zumindest animierten Auftauchen von Moriarty. Ich freue mich darauf. Ob er es nun wirklich ist oder nicht, sei dahingestellt. Ich traue dieser Serie alles zu. 