---
layout: post
title: "Nutzer bevormunden?"
date: 2014-09-19
category: it
tags: [Web, Social, Facebook]
published: true
comments: false
share: true
---

Twitter und Facebook beschließen Änderungen, die einen erheblichen Einfluss auf die Nutzer der Dienste haben werden. Ersterer beginnt mit dem einstreuen von angeblichen interessanten Tweets für den Nutzer in dessen Twitter-Timeline, auch wenn er den Personen gar nicht folgt.    
Letzterer bevormundet seine Nutzer sowieso schon lang und wird Ende des Jahres u.a. den *Organic Reach* für Facebook-Pages einstellen. 

## Timeline nach Twitter 

Bisher war die eigene Twitter-Timeline, tatsächlich **deine** Timeline. Du hast selbst bestimmt, wessen Tweets du lesen willst, oder eben nicht. Über die offiziellen Twitter-Kanäle (Website, Twitter-App) war zwar auch Werbung zu sehen, aber der konnte man über Apps von Drittanbietern umgehen (z.b. Tweetbot).

Auf der [Hilfe-Seite von Twitter](https://support.twitter.com/articles/495848-was-ist-eine-twitter-timeline) findet sich nun diese Passage, Fettschrift von mir:

> Hinweis: Es können auch Inhalte von Accounts angezeigt werden, denen Du nicht folgst, wie etwa gesponserte Tweets, Retweets von Accounts, denen Du folgst, **oder für Dich relevante Inhalte**.

Ein Algorithmus von Twitter registriert, wenn ein Tweet besonders *erfolgreich*, *beliebt*, *verbreitet* und aus irgendwelchen Gründen für mich interessant ist oder sein könnte. Und dann bekommt man ihn in der eigenen Timeline zu sehen. Gratulation. Wie weit das letztlich geht und ob Drittanbieter-Apps verschont bleiben, ist noch nicht klar, aber wenn wir einmal die Büchse öffnen...

## Timeline nach Facebook

Wenn wir einmal die Büchse öffnen, landen wir zwangsläufig bei Facebook. Facebook zeigt dir schon lange nicht mehr alles an, was deine Freunde verbreiten. Und wenn du meinst, du hast schon lange nichts mehr von der und dem gehört, dann hat Facebook vielleicht für dich entschieden, dass deren Inhalte uninteressant sind.    

Die *Organic Reach* für Facebook-Pages [beschreibt Facebook](https://www.facebook.com/business/news/Organic-Reach-on-Facebook) so: 

> Organic reach refers to how many people you can reach for free on Facebook by posting to your Page. 

Es betrifft in diesem Fall direkt die Besitzer einer Facebook-Page und indirekt die Nutzer. Am Ende war es nur eine Frage der Zeit, bis alles in der *Facebook-Black-Box* verschwindet. Dabei muss das nicht unbedingt immer schlecht sein, wie [Facebooks Änderungen](https://newsroom.fb.com/news/2014/08/news-feed-fyi-click-baiting/) zum Reduzieren von sogenannten *Click-Bating* [^1] Beiträgen zeigen. Die Nutzer wollen das nicht und Facebook kann mit Algorithmen entscheiden, welcher Beitrag in diese Kategorie fällt oder nicht. 

## Informationen reduzieren

Wir sehen hier ganz klar den Trend hin zu Black-Box-Algorithmen, wie sie Google in der Suche schon seit Jahren nutzt. Nun wird sich der Bereich der Suchmaschinenoptimierung (SEO) erweitern und vielleicht Social-Media-Platform-Optimization nennen. Eine Frage bleibt bestehen: Sollte man die Nutzer bevormunden? 

Genauso wie sich tausende Webseiten zu einem trivialen Begriff finden lassen, posten einige hundert Freunde bei Facebook eine ganze Menge an Statusmeldungen, Links zu anderen Beiträgen, etc. und kein Mensch kann das alles lesen. Wie präsentiert man einem Nutzer diese Informationen? Bei Twitter sortierte man selbst aus, bei Facebook kann man Kontakte und Pages ignorieren. 

Bemerkenswert fand ich den Testballon, den Facebook in meine Timeline stiegen ließ. Ein Bekannter, der bisher eher wenig auf Facebook aktiv war, begann vor drei Wochen massenhaft Links, Beiträge und Videos zum Konflikt zwischen Israel und Palästina zu teilen. Knapp zwei Wochen war meine Timeline damit geflutet und ich kurz davon ihn komplett zu ignorieren. Scheinbar hat Facebook bemerkt, dass ich keinen einzigen Beitrag angeklickt habe und plötzlich verschwand er wieder aus meiner Timeline. Zuerst dachte ich, er hätte so plötzlich aufgehört wie er begonnen hatte, aber ein kurzer Blick auf seine Seite bestätigte das Gegenteil. Er teilte fleißig weiter, für mich aber nicht mehr sichtbar.

Ich war besonders in den letzten Monaten kurz davor mein Facebook Account einzustampfen. Was ich präsentiert bekomme, ist enorm langweilig und kaum ein Zugewinn an Information. Dennoch bin ich mit einigen Menschen befreundet, die ich sonst nicht mehr erreichen könnte. Hier stellt sich die Frage, ob man zu Menschen Kontakt halten muss, mit denen man seit 3 oder 5 Jahren kein Wort gewechselt hat. Wenn Facebook für mich nicht funktioniert, muss ich es vielleicht verlassen. Anderen gefällt es und sie bekommen genau das angezeigt, was sie sehen möchten [^2]. 

Vorerst betreibe ich weiterhin eine aggressive *Ignorieren-Politik* [^3] und vielleicht wird mein Feed mit der Zeit besser. Aktuell bemerke ich nur, dass ich seltener Updates erhalte. Fast schon zu selten.

[^1]: Facebook: *Click-baiting* is when a publisher posts a link with a headline that encourages people to click to see more, without telling them much information about what they will see. Posts like these tend to get a lot of clicks, which means that these posts get shown to more people, and get shown higher up in News Feed.

[^2]: Bonus-Beobachtung. Ich bin bei Facebook noch in der Gruppe meines Wohnheims in Potsdam. Da gibt es durchaus interessante Beiträge, aber auch viel zu viel Flohmarkt. Und leider werden mir alle Beiträge aus der Gruppe angezeigt. Ich kann entweder alles sehen oder nichts. Feingranularer lassen sich die Nachrichten nicht filtern. 

[^3]: Alles in meiner Timeline, was mich nicht interessiert, setze ich auf *Ignorieren* und *Das will ich nicht mehr sehen*. 