---
layout: post
title: "Mein 1. Kickstarter Projekt"
date: 2014-08-03
category: blog
tags: [Review, iPhone]
published: true
comments: false
share: true
image: 
  feature: 2014-dock-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Nein, ich möchte **kein** Geld sammeln. Richtigerweise müsste der Titel *&quot;Das 1. von mir unterstütze Kickstarter Projekt&quot;* lauten. Ist aber zu lang.    
Ich habe vor einigen Monaten ein Projekt unterstützt und bekam tatsächlich ein Produkt. 

Gemeint ist das [Cable Weight von Kero](https://www.kickstarter.com/projects/keroproducts/cable-weight-for-iphone-5), ein iPhone Dock. Ich fand bei den üblichen Verdächtigen leider nicht sehr viele iPhone Docks und kaum welche, die mir gefielen. Eigentlich keine. Dieses Kickstarter Projekt schien mir solide und so unterstützte ich es, was am Ende auch nur ein verzögerter Kauf war.    
Für sehr erschwingliche 14$ kaufte ich einen Cable Weight und später noch das [Nomade Cable](http://keroproducts.com/collections/products) zum Vorzugspreis. Insgesamt mit Versand kostete mich das 33€, ein halbes Jahr warten inklusive. Jetzt kosten die Produkte einzeln allein schon 25$ (zzgl. 20$ Versand). 

<figure class="half">
	<a href="/images/2014-dock-close-1600.jpg">
		<img 
			src="/images/2014-dock-close-400.jpg" 
			alt="DESCRIPTION IN HERE"></a>
	<a href="/images/2014-dock-separate-1600.jpg">
		<img 
			src="/images/2014-dock-separate-400.jpg" 
			alt="image"></a>
	<figcaption>Eigentlich nur ein Metallblock</figcaption>
</figure>

Den Projektverlauf fand ich sehr gut. Man wurde stets informiert und auf dem Laufenden gehalten. Auch das Produkt kam pünktlich an und war gut. Es hält was es verspricht und war relativ preisgünstig.      
Später stieß ich auf das Beton-Dock von [hardwrk](http://hardwrk.com/massive-dock-iphone5.html) und finde das eigentlich ziemlich schick und funktionell. Theoretisch kann man ja zwei Docks gebrauchen, aber 60€ für ein Betonklotz?

Kickstarter ist schon eine tolle Sache und vielleicht setzt es sich auch in Deutschland mehr durch. Zu bemerken ist aber auch die Gefahr schwarzen Schafen zu begegnen. Ich habe Geld für ein richtiges Produkt gegeben, aber es gibt auch genug Projekte die Geld sammeln um etwas zu verwirklichen, was die Spender dann nicht in physischer Form erhalten. Ist das Funding für das Projekt erfolgreich, also das Ziel der Einnahmen erreicht, wird das Geld ausgezahlt und als Spender kann man nur noch hoffen.     
Selbst wenn richtige Produkte versprochen wurden, kann die Entwicklung und Produktion länger dauern. Oder eine Supermarktkette bekommt die 5. Lieferung der SmartWatches, bevor alle Unterstützer eine erhalten haben. 

Die Idee von Kickstarter ist toll, aber als Spender immer auch den Kopf einschalten. 