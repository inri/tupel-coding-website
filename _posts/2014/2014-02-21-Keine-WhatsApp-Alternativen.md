---
layout: post
title: "Keine Alternative zu Whatsapp"
date: 2014-02-21
category: it
tags: [Web, App]
published: true
comments: false
share: true
---

Facebook hat für 450 Millionen Nutzer 14 Milliarden Dollar bezahlt, immerhin 35$ pro Nutzer. Die Handvoll Entwickler sind diese Summe sicherlich nicht wert und bahnbrechend innovativ ist ein plattformübergreifender Nachrichtendienst auch nicht. 


Vielleicht steckte auch [eine ganz andere Motivation](http://blog.fefe.de/?ts=adf8d951) dahinter? Wie auch immer, die astronomische Summe möchte ich jetzt gar nicht diskutieren. 

Wie es nicht anders zu erwarten war, wurden, gleich nachdem die Nachricht der Übernahme die Runde machte, ungefragt Whatsapp Alternativen angepriesen. Aber ich kann euch folgendes verraten: Nur verschwindend wenig Menschen werden sich tatsächlich von Whatsapp verabschieden. 

## Facebook, das notwendige Übel

Facebook hat völlig zurecht ein Imageproblem. Zuckerbergs Gefasel &quot;Wir wollen doch nur alle Menschen digital vernetzen&quot; glaubt niemand mehr. Facebook verdient Geld mit den Daten seiner Nutzer. Das ist unsympathisch, wird aber hingenommen. Die alle paar Monate durch das soziale Netzwerk schwappenden Boykott-Aufrufe sind auch nur ein Tropfen auf den heißen Stein des Facebook Nutzers.

Das in Deutschland vor einigen Jahren relativ beliebte studiVZ wurde nicht von Facebook verdrängt weil es schlechter aussah oder weniger Features hatte. Nein, Facebook hatte einfach eine größere (internationale) Nutzerbasis, dagegen konnte das auf Deutschland beschränkte studiVZ auf Dauer nicht ankommen.  

Facebook ist mittlerweile so weit verbreitet, dass es für ein Konkurrenz-Netzwerk äußerst schwierig wird sich durchzusetzen. Und dessen potentielle, innovative Funktionen kann Facebook ebenfalls in kürzester Zeit selbst implementieren. Es gab ein Gebiet, auf welchem sich parallel mit der Verbreitung von Facebook ein Dienst weltweit durchgesetzt hat: Nachrichtendienste.

## Whatsapp ist etabliert, wie Facebook

Ständig ist zu lesen, dass Whatsapp ein Facebook-Konkurrent sei. Wieso das? Für mich sind es zwei unterschiedliche Dienste mit unterschiedlichen Zielen, aber durchaus einigen Gemeinsamkeiten, die eine Fusion rechtfertigen.    
Auf dem Gebiet der Nachrichtendienste ist Whatsapp genauso etabliert und verbreitet wie Facebook. Und genau deshalb werden die meisten Whatsapp-Nutzer Whatsapp-Nutzer bleiben. 

Man kann ihnen sagen, dass Whatsapp unsicher ist und das war es auch schon vorher. Man kann ihnen sagen, dass Facebook alle ihre Daten und Nachrichten auswerten und verarbeiten wird. Das ist alles egal, wenn Whatsapp weiterhin das tut, was es am besten kann: unkompliziert und kostenlos Nachrichten übermitteln. 

Die Zusammenarbeit zwischen Facebook und Whatsapp bzw. die Integration des letzteren wird trotzdem interessant.