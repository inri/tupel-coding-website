---
layout: post
title: "Neue Website für Jeannine"
date: 2014-01-03
category: blog
tags: [Web, Design]
published: true
comments: false
share: true
image: 
  feature: 2014-jeannine-website-new-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Vor etwas über einem Jahr fragte mich eine Freundin aus Berlin ob ich für sie eine Website bauen kann. Sie ist Schauspielerin und wollte eine Visitenkarte im Netz.    

Ich bin kein Webdesigner und das sagte ich ihr auch. Doch eine solche Aufgabe &quot;*zwingt*&quot; mich praktischerweise dazu mich mit diesem Thema auseinanderzusetzen und das war schon lange mein Plan. Ich gab ihr keine Garantie auf ein wunderschönes oder perfektes Ergebnis und sie bezahlte nur die Hostingkosten. 

## Website 1.0

Das erste Ergebnis war eine Fullscreen-Image-Website mit hinein- und herausfliegendem Menü und Inhalt. Natürlich von einer anderen Seite inspiriert, aber immerhin handgemacht. 

<figure>
    <a href="/images/2014-jeannine-website-old-lg-1600.jpg">
	   <img 
		  src="/images/2014-jeannine-website-old-lg-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Große Hintergrundbilder sehen schick aus, lassen aber nur wenig Platz für Informationen.</figcaption>
</figure>

Ein Nachteil der Seite: Sie war nicht responsive, ließ sich also auf einem Tablet nur schwierig und auf einem Handy gar nicht gut benutzen. Mich störte mit der Zeit auch der Herumfliegen-Effekt des Menüs. Er war einfach zu langsam. Man hätte ihn zwar um einige Millisekunden schneller machen können, doch das hätte noch seltsamer ausgesehen. Mit mehr Aufwand kann man es vielleicht richtig gut werden, aber die Expertise habe ich nicht. Und wenn man ihn komplett weglassen würde, sähe das Menü nach gar nichts mehr aus. 

Aber Jeannine war zufrieden und fürs erste erfüllte die Seite ihren Zweck.

## Responsive Design & Zielgruppe

Vor einigen Wochen fragte mich Jeannine, ob ich die Seite nicht responsive machen kann. Die bestehende hatte diesbezüglich einige Knackpunkte wie beispielsweise das Menü oder die Bildergalerie. Also sollte es am besten eine neue Seite werden.

Neben dieser eher technischen Bedingungen, grübelte ich über die optimale Lösung einer Website für Schauspieler nach. Die Zielgruppe sind insbesondere bei noch eher unbekannten Gesichtern die Caster der Casting-Agenturen. Ich stelle mir das so vor: Die sind auf der Suche nach geeigneten Kandidaten. Ihre Zeit ist knapp und die Suche darf nicht zu lange dauern. Sie brauchen neben Bildern bestimmte Informationen und müssen diese, wie gesagt, vor allem schnell finden. 

Die alte Seite war ungünstig. Man sah nur ein Bild, nur eine Information. Für weitere Bilder musste man sich durch das Menü klicken und die nervigen Sekunden der Animation abwarten. Von dort aus ging es wieder zurück zum Hauptmenü und zur nächsten Kategorie, alles verbunden mit unnötigen Sekunden Wartezeit. 

<figure>
    <a href="/images/2014-jeannine-website-1-lg-1600.jpg">
	   <img 
		  src="/images/2014-jeannine-website-1-lg-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Die Anzahl der Bilder pro Zeile und ihre Breite passen sich der Browserbreite an.</figcaption>
</figure>

## Alle Informationen auf einen Blick

Die neue Website bietet alle wichtigen Informationen sofort auf der Startseite. Der Besucher wird mit fünf verschiedenen Bildern von Jeannine begrüßt (s.o.). Per Klick gelangt er direkt zur gewünschten Kategorie oder kann einfach selbst dahin scrollen. Darunter befinden sich ein Willkommenstext für den Besucher, drei weitere Bilder mit Verlinkung zur kompletten Galerie, Jeannines Vita, ihrer Videos und Kontaktdaten der Agentur.

Die Bilder sind eine extra Unterseite, weil es sicherlich nicht notwendig ist dutzende von ihnen auf die Startseite zu klatschen. Sie sind auch nur noch 460px breit und hoch, denn es gibt keine Vergrößerungsansicht. Dafür kann man sich alle Bilder als ZIP laden und anschauen wie man mag.

## Media Queries 

Ganz zu Beginn hatte ich noch überlegt Bootstrap als Framework zu nutzen. Ich wollte es mir erleichtern die Seite responsive zu machen. Doch Bootstrap ist einfach zu mächtig für diesen einfachen Zweck. Also lernte ich bei [Treehouse](http://teamtreehouse.com/library/build-a-responsive-website) und nutzte Media Queries, mit welchen es dann relativ einfach war.   
Mit ihnen habe ich Abstände und Breiten von Elementen, Schriftgrößen und Bildgrößen angepasst. Und auch ob bestimmte Elemente überhaupt sichtbar sind bzw. direkt Fotos ausgetauscht.

<figure>
    <a href="/images/2014-jeannine-website-2-lg-1600.jpg">
	   <img 
		  src="/images/2014-jeannine-website-2-lg-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Kein Menü, nur ein freundliches Willkommensbild. Das Menü folgt noch.</figcaption>
</figure>

Etwas länger musste ich über die optimalen Grenzen der Browserbreite nachdenken. Man kann sich auf 3 beschränken: PC-Bildschirm, iPad und iPhone. Ich habe eine vierte für ältere Monitore mit damals noch nicht so großen Bildschirmbreiten dazu genommen.    
Je nach Breite passen sich die Bilder des Menüs an. Für die Zwischengrößen überlappen sich die Bilder, was aktuell leider noch von rechts nach links passiert und nicht gleichzeitig von beiden Seiten. Ich nutze keinerlei Javascript, aber ohne dem wird es wohl nicht besser gehen.

Für die kleinste Breite wird das Menü komplett rausgeschmissen und es gibt nur noch ein Bild. Aktuell muss man Scrollen, aber es folgt noch ein ausklappbares Menü (wie bei dieser Seite).  

Die dritte Baustelle ist die Breite der Videofenster, ebenfalls nur für Mobiltelefone problematisch. Die Videos sind zu breit und dadurch lässt sich die Seite nicht so gut scrollen. 

## Fazit

Insgesamt war ich überrascht, dass die Arbeit mit Media Queries doch so einfach war. Wobei ich keine überkomplexe Website gebaut habe und auch nicht auf jeden Pups in der Responsive-Landschaft achten musste.   
Ich wäre nicht traurig gewesen die Seite komplett Javascript-frei zu lassen, aber mit den besagten Änderungen wird das nicht der Fall sein.

Zu finden ist Jeannines Seite unter: [schauspielerin-jeannine-rieckhoff.de](http://schauspielerin-jeannine-rieckhoff.de "Schauspielerin Jeannine Rieckhoff")