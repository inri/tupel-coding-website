---
layout: post
title: "Top of the Lake, Episode 1"
date: 2014-08-29
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2014-top-of-the-lake-lg.jpg 
  credit: Sundance Channel
  creditlink: http://www.sundance.tv
---

Wie auch schon bei *Brookly Nine-Nine* wurde ich durch die Golden Globes auf diese Krimi-Miniserie aufmerksam. Die Hauptdarstellerin bekam die Auszeichnung für ihre Rolle als Polizistin. Es gibt zwar nur eine Staffel, aber ich glaube die ist sehenswert. 

Die erste Episode beginnt etwas verworren. Ein kleines Mädchen wird stoisch im tiefen Wasser stehend gefunden und später im Krankenhaus diagnostiziert man eine Schwangerschaft bei ihr, im 5. Monat. Szenenwechsel. Eine Horde Frauen stellt auf einer großen Wiese an einem See einige Container auf, was einem Anwohner und seinen zwei Söhnen nicht so richtig gefällt. Die statten dem Makler einen Besuch ab und ertränken diesen bei der Gelegenheit gleich mal.          
Die Hauptdarstellerin, als versetzte und neue Polizistin in ihrer Heimatstadt, kümmert sich um das Mädchen, denn die örtliche Polizei kann das zum einen nicht besonders gut und steckt mit dem einen Anwohner unter einer Decke, was sie natürlich nicht weiß. Die Frauen am See scharen sich um eine spirituelle Führerin und spielen mit Sicherheit auch noch eine Rolle.   
  
Insgesamt wirkt das Szenario noch undurchsichtig, aber die Serie transportiert eine interessante Stimmung und nicht zuletzt erinnert mich die Gegend, es wurde in Neuseeland gedreht, an Norwegen. Auch dass man von Anfang weiß, dass die Serie abgeschlossen ist, steigert das Interesse.

Die Serie lief auf Arte [^1] und ist auf Deutsch zu haben. Wir haben uns erst einmal nur die Pilotepisode angeschaut, aber sobald sich die Zeit findet, folgt der Rest. Deshalb meine Empfehlung: Ruhig mal reinschauen.

[^1]: Ursprünglich lief sie im Sundance Channel, wo unter anderem auch *Rectify* läuft. Man kann den Sundance Channel für seine kleinen, aber enorm guten Serien im Hinterkopf behalten. 