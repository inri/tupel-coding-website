---
layout: post
title: "6Wunderkinder Programming"
date: 2014-02-16
category: it
tags: [Coding, App]
published: false
comments: false
share: true
---

Im Januar habe ich mich gefragt was andere Programmierer so tun. Dann frage ich mal weiter: Was tun eigentlich die Leute bei den 6Wunderkindern?

## Tschau Wunderlist 2.0

Vor etwas über einem Jahr gab es ein größeres Update für Wunderlist (**Wunderlist 2**). Ich habe die App vorher schon gerne genutzt und tat es danach noch mehr.   
Doch heute ist Wunderlist auf keinem meiner 5 Geräte mehr installiert. Mit meinem Windows System und Android Tablet arbeite ich sehr wenig, das stört mich das Fehlen einer ToDo App nicht. In der Apple Welt kompensiere ich Wunderlist mit Apples Notizen und Erinnerungen Apps. 

Der Grund für den Wechsel besteht aus vier Zeichen: iOS 7. In den über 4 Monaten nach Erscheinen von iOS 7 haben es die Entwickler und Designer der 6Wunderkinder nicht geschafft ein visuelles Update herauszubringen. Wunderlist hat sich für mich nicht mehr gut angefühlt. Das Design wirkte alt und technisch gab es auch immer mal wieder Probleme.

## 30 Entwickler

Laut ihrer [Website](http://www.6wunderkinder.com/de/team) besteht das Team bei den Wunderkindern aus 30 Entwicklern (gezählt im Januar 2014). Entwickler für alle Systeme, für das Backend, Web und generell Design. Was zur Hölle machen die den ganzen Tag?

4 Monate sind nicht besonders viel, aber iOS 7 wurde bereits im Juni vorgestellt und es gab ab diesem Zeitpunkt schon Beta-Versionen. Es zogen zuerst 4 Monate ins Land, dann weitere 4 und Wunderlist veränderte sich nicht. Das ist einfach nur enttäuschend.

Im Vorfeld zur 2.0er Version wurden sicherlich wichtige Ressourcen an das Experiment *Wunderkit* verschwendet. Doch man besann sich auf die eigenen Wurzeln und wollte nun alle Energie in die Wunderlist stecken. Wo ist die Energie hin?

## Get fit in 2014?

Ich wunderte mich ziemlich als über Twitter der [Blog-Eintrag](https://www.wunderlist.com/blog/get-fit-in-2014-with-wunderlist) von Chad Fowler promotet wurde. Nichts gegen Sport und Fitness, aber warum um alles in der Welt schreibt der **Chief Technology Officer** der 6Wunderkinder einen solchen Text für den offiziellen Blog der 6Wunderkinder? 

Ich verstehe es nicht. ToDo Apps sind nicht gerade das Paradebeispiel komplizierter Softwaresysteme. Sogar John Gruber hat mit zwei Freunden eine solche App herausgebracht ([Vesper](http://vesperapp.co)).   
Vielleicht ist Wunderlist 3.0 für alle Systeme fertig, nur ein Team hängt hinterher und man möchte gerne alle auf einmal Updaten? Oder man hat mal wieder Monate in das Entwickeln falscher Features gesteckt? Oder lag die Priorität auf Wunderlist Pro? 

Einige Gründe werden [hier](https://www.wunderlist.com/blog/Preparing-for-a-New-Era-of-Design-Wunderlist-on-iOS-7) vom Chief Marketing Officer erklärt, rufen bei mir aber kein Verständnis hervor.

## Fazit

Natürlich soll ein Produkt nicht zu früh und unfertig veröffentlicht werden. Doch Wunderlist ist am Ende eine ToDo App, nicht mehr und nicht weniger. Wobei man, wenn man den Blog der 6Wunderkinder so überfliegt, allerdings einen anderen Eindruck bekommt.

Ich weiß dass Wunderlist kostenlos ist, was kann man dann schon fordern? Dass kein Geld genommen wird, liegt aber nicht an mir. Ich würde für jede größere Version einige Euro hinblättern. 4,50 Euro pro Monat für eine [Pro Version](https://www.wunderlist.com/de/pro), deren Mehrwert mir nichts bringt, bin ich nicht bereit zu zahlen. 

Mal sehen wie die neue 3.0er Version aussehen wird. Die Versprechungen sind hoch. Ob ich dann wieder zurück wechseln werde, weiß ich noch nicht mit Sicherheit. 