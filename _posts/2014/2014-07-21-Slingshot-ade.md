---
layout: post
title: "Slingshot adé"
date: 2014-07-21
category: blog
tags: [Facebook, App]
published: true
comments: false
share: true
---

Meine Freundin und ich, wir deinstallieren Slingshot. Es war ein nettes Experiment, aber einige Designschwächen sind einfach zu gravierend. Die App macht keinen Spaß.     

Dass die Fotos gelöscht werden, fand ich von Anfang an nicht gut. Ich ließ mich versöhnen, weil man das Speichern immerhin in den Einstellungen aktivieren konnte. Was mir nicht bewusst war: **Nur die eigenen Fotos werden gespeichert!** Das ist Mist und katapultiert Slingshot für mich ins Aus.      
Noch dazu ist es ein Kopie von Snapchat, da können die Entwickler bei Facebook behaupten was sie wollen. 

Die Idee der erzwungenen Antworten um eine Mitteilung freizuschalten, ist nicht schlecht. In der Praxis muss man aber stets darüber nachdenken, wann man Slingshot benutzt und wann nicht.     
Ein Beispiel: Meine Freundin trifft sich mit ihrem Kommilitonninen im Park und ich bleibe zu Hause. Sie slingshotet mir ein Foto von der Gruppe, ich schalte mit einem Foto des Abwaschs, den ich gerade mache, das Foto frei. Nun kommen von ihr im Laufe des Abends noch weitere, aber ich mache einfach nichts spannendes um gut oder gleichwertig zu antworten.        
Oder mit einem Freund habe ich das auch versucht, aber von ihm kamen auch nur uninteressante Bilder zurück und meine fand er auch nicht immer toll.

Mit Slingshot gibt es höhere Erwartungen. Ich werde gezwungen aktiv zu sein und ein Bilder oder Video zu schicken, also will ich auch, dass meine Mühen belohnt werden.      
Warum kann man den Antwort-Zwang nicht optional machen? Das könnte die Erwartungen etwas lockern. Der Absender entscheidet, wann eine Mitteilung einen größeren Wert hat um freigeschaltet zu werden. Oder wann er ein Bild einfach des Bildes wegen teilen möchte.

Übrigens entsteht durch den Antwort-Zwang eine unendliche Kommunikation, Fluch und Segen. Eine Mitteilung bleibt am Ende stets von einer Person ungesehen


    

