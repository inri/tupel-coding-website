---
layout: post
title: "Erwartungen zur WWDC 2014"
date: 2014-06-02
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Die letzte große Veranstaltung von Apple liegt nun schon über 7 Monate zurück und konzentrierte sich auf neue iPad Modelle. Doch deshalb ist trotzdem nicht mit neuer Hardware zu rechnen, wie andere Kommentatoren schon anmerkten. Ob nun iPhone, iPad oder iWatch, sie alle werden ihre eigene Show bekommen.     

Und wenn man sich die [Release Cycles](http://buyersguide.macrumors.com) anschaut, wird schnell klar, dass bestenfalls bei den iGeräten zweiter Klasse[^1] mit einem Update zu rechnen ist.

Doch wird es gar keine neue Hardware geben, weil die Software bei einer Entwicklerkonferenz nun einmal im Vordergrund steht?

## iOS 7.2 mit Healthbook

Selbstredend wird der Nachfolger iOS 8 heißen und Apple wieder dutzende kleine neue Features anpreisen bei denen wir uns Fragen werden, wieso sie nicht schon zeitiger in Form eines Updates erschienen sind. Davon gab es übrigens nur ein größeres, nämlich iOS 7.1 im März.

Viel wurde über die Kirsche auf dem Eis spekuliert: Ein **Healthbook** für Fitness und andere Daten über unsere Gesundheit? Doch genau dafür fehlt die Hardware, oder? Gesundheitsbuch schön und gut, aber mit welchen Daten soll das arbeiten? Perfekt wäre an dieser Stelle eine iWatch, die nicht nur meine Schritte zählt. 	Eine iWatch ist aber Hardware und Hardware muss irgendwo zusammengebaut werden. Egal wie viel Mühe sich Apple gibt, ich bezweifle stark dass bis heute nichts von dieser iWatch geleakt wäre *(, wenn sie denn heute vorgestellt wird)*.  
  
Wenn Apple trotzdem ein Healthbook und neue APIs zeigt, würde ich das als Bestätigung interpretieren, dass wir in naher Zukunft eine iWatch zu sehen bekommen. Doch würde Apple den Überraschungsmoment jetzt schon herausnehmen? Nicht zuletzt denke ich auch, dass Apple eine Healthbook-API aktuell noch nicht für Drittanbieterapps/geräte öffnen wird. Also warum dann vorstellen?

Oder die iWatch wird gezeigt, aber erst im Spätsommer verkauft. Das ist für Apple aber auch nicht üblich.

## OS X Designupdate

Zwischen OS X 10.9.3 und iOS 7 klafft eine mehr oder weniger große Designlücke. Sie ist zwar nicht mit der Lücke zwischen iOS 6 und iOS 7 vergleichbar, aber mit Sicherheit wird Apple diese schließen. Ich bin gespannt.

## Fazit

Meine Erwartungen sind nicht besonders groß. Oder ich habe einfach keine Lust über die vielen kleinen und vor allem notwendigen Änderungen zu schreiben, die ich mir erhoffe. Oder ich bin einfach relativ zufrieden. Die Systeme funktionieren und ein Windows-8-artiges Update ist nicht notwendig.

Viel mehr wundert es mich, dass die Geräte-Updates und Neuerungen für die zweite Jahreshälfte bzw. die zweite Hälfte der zweiten Jahreshälfte geplant sind. Da kommt eine große finanzielle Belastung auf uns zu.

[^1]: Klassifiziert nach finanziellem Erfolg für Apple. Konkret gemeint sind iMac, MacMini und AppleTV.
