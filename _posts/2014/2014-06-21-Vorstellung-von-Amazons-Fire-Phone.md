---
layout: post
title: "Vorstellung von Amazons Fire Phone"
date: 2014-06-21
category: it
tags: [Amazon, Phone]
published: true
comments: false
share: true
image: 
  feature: 2014-fire-phone-lg.jpg
  credit: Amazon
  creditlink: http://amazon.com
---

Ein Amazon Phone gerüchtet schon seit einer ganzen Weile durch die Technik Blogs und nun endlich ist es da, das [Fire Phone](http://www.amazon.com/dp/B00EOE0WKQ/ref=amb_link_1?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=gateway-center-column&pf_rd_r=1RT52AFYN2S7DAXBE0YC&pf_rd_t=101&pf_rd_p=1828037122&pf_rd_i=507846). Auch wenn die Vorstellung durch Jeff Bezoz etwas zu improvisiert wirkte und weniger Kunstpausen besser gewesen wären, wartet das erste Mobiltelefon von Amazon mit einigen interessanten Features auf. 

## Firefly App für iOS, bitte

Mit Abstand am besten hat mir die Gegenstands-Erkennung, genannt Firefly, gefallen. Man hält die Kamera auf irgendwas drauf und sofort wird erkannt was das ist (und wo man es kaufen kann bzw. weitere Funktionen). Das reicht von Gütern wie Bücher, DVDs, CDs, bis hin zu Barcodes, Liedern, Gemälden und Telefonnummern. Amazon kann 100 Millionen Dinge damit erkennen. Selbst wenn man den Screenshot einer Filmszene knipst, wird nicht nur der Film erkannt, sondern die genaue Zeitposition und Schauspieler, die in dieser Szene zu sehen sind. 

Die Technik dahinter wurde auch erklärt. Zum einen nutzt Amazon die mächtigen Server der eigenen Cloud. Aber auch auf dem Gerät selber werden die Daten, die analysiert werden, zurechtgestutzt.     
Ich kann mir dafür auf jeden Fall viele Anwendungsszenarien vorstellen. Für Android bzw. Amazon-Android Entwickler gibt es auch ein SDK um diese Funktion in die eigene App einzubauen. Natürlich ist Firefly am besten, wenn es direkt ins System integriert ist, aber über eine iOS App würde ich mich auch sehr freuen.

## Mayday - Nach Hilfe fragen

Einen Experten direkt und sofort nach Hilfe fragen, wenn es um die Bedienung des Geräts geht, hat Amazon schon bei seinen Tablets eingeführt. Mayday darf natürlich beim Fire Phone nicht fehlen. Ich persönlich würde es sicherlich selten nutzen, aber ich denke der Nutzen für Produktentwicklung ist nicht zu unterschätzen. 

Deine Kunden melden sich bei dir und verraten welche Dinge nicht funktionieren weil zu unintuitiv. Klasse! Wenn diese Informationen nicht in die zukünftige Produktentwicklung fließen, dann weiß ich auch nicht.

## Kein 3D, aber dynamische Perspektive

Vor einigen Jahren waren Smartphones mit 3D-Bildschirmen für einige Zeit in den Medien und verschwanden wieder. Die Technik war Scheiße, aber Amazon hat sich gefragt, ob man die Idee hinter 3D trotzdem irgendwie umsetzen kann.   
Das Ergebnis ist eine Art dynamische Perspektive für sinnvolle Anwendungsgebiete. Ich schreibe &quot;*sinnvoll*&quot; weil mich Amazons Beispiele nicht überzeugt haben.

Auf der Front des Fire Phones sind vier Infrarot-Kameras eingebaut und die helfen dabei herauszufinden, wo die Augen des Nutzers sind. Diese Information kann genutzt werden um das Bild perspektivisch zu verschieben. Als Beispiele wurden Bildschirmschoner, Maps und Games gezeigt.    
Erstaunlich ist, wie viel Zeit Amazon in die Entwicklung gesteckt hat. Man musste erst herausfinden, wie man Köpfe erkennt, vor allem die Köpfe des Nutzers vor dem Fire Phone.

Zusammen mit dem Pseudo-3D agieren auch diverse Kipp-Gesten und Scrolling Effekte. Die Maxime lautet: Ein Kippen ist besser als ein Fingerwisch. Ob das wirklich so ist, muss die Praxis Ers noch zeigen.    
Einige (Lese-) Apps bieten auch das Kippen als Navigationsalternative an, doch damit bin ich nie warm geworden.

## Randnotizen

Die Kamera muss natürlich 13 Megapixel haben, je mehr desto besser. Einen Bildschirmstabilisator erwartet man ja auch im kommenden iPhone. Den Bildvergleich mit dem iPhone und Samsung empfand ich als eher unglücklich. Alle drei Bilder eines nächtlichen Skyline sahen gut aus. Auch der unbegrenzte Foto-Speicher ist nett.    

Technisch ist es selbstverständlich auch fast an der Grenze von dem, was man zur Zeit bei den einzelnen Komponentenherstellern ordern kann (2,2 GHz Quadcore, 2 GB RAM). Der Bildschirm ist superhell, dafür nicht so pervers hochauflösend wie die Samsung Geräte.    
Mal sehen was der Akku zu dieser Leistung und so vielen Kameras zu sagen hat.

## Fazit

Das Fire Phone [^1] ist durchaus ein interessantes Gerät. Leider wird es wohl nicht so schnell bei uns erhältlich sein. Und wenn doch, dann reiht es sich im hochpreisigem Segment bei iPhone und Galaxy Sx ein.    
 
Enttäuschend wird die Software sein, die auch schon auf den Fire-Tablets zu sehr auf Konsumierung ausgelegt ist. Ich bin mir bei den technischen Innovationen zwar nicht sicher, ob sie wirklich sinnvoll sind, aber zumindest hat Amazon nicht irgendwelche Bauteile zusammen geklebt.

[^1]: Wieso hat sich Firefox nicht diese Namensrechte gesichert. Das Fire Phone mit Firefox OS!