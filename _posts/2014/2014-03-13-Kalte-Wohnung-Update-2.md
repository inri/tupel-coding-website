---
layout: post
title: "Kalte Wohnung, Update 2/2"
date: 2014-03-14
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
---

Wir hatten uns im Dezember schon neue Wohnungen angeschaut und mit den Gedanken eines erneuten Umzugs angefreundet. Umso frustrierender war die Ablehnung der Kündigung kurz vor Weihnachten. Wir fuhren zu unseren Familien und wussten, wenn wir Neujahr zurückkommen, wird die Wohnung immer noch kalt sein und keine Lösung in Sicht. 

## Ein Wohnungswechsel

Der Hausmeister hatte uns die Möglichkeit eines Wohnungswechsel innerhalb des Blocks schon angeboten, aber wir hatten bis dahin noch auf die Kündigung gehofft. Vergebens. Die Kündigung war dahin und der Anwalt sagte uns ebenfalls, dass ein Auszug mit Sicherheit Mietforderungen nach sich ziehen wird.    

Wir wussten nicht ob man uns nun glaubt und die Mängel ausbessern oder weiterhin auf Zeit spielen wird. Anfang Januar war es richtig kalt und das milde Wetter noch nicht abzusehen. Wir wollten einfach raus. Der Hausmeister zeigte uns mehrere Wohnungen und darunter eine kleinere in der 3. Etage, mit Balkon. Zu allen Seiten gab es Nachbarn und schon bei der Besichtigung schien sie uns wärmer zu sein als unsere. Wenn, dann sollte es diese sein!    

Wieder mal waren wir die Bittsteller bei der Verwaltung, von alleine kam man nicht auf die Idee uns einen Wohnungswechsel anzubieten. Zumindest erklärten sie sich einverstanden mit der Lösung und gaben sie später als ihre eigene aus (&quot;*Wir haben Ihnen eine neue Wohnung angeboten...*&quot;).

## Neue Vertragsbedingungen  

Um zwei Punkte mussten wir noch mit der Verwaltung verhandeln. Die Wohnung war unrenoviert. Es waren zwar relativ dezente, aber hässliche Pastellfarben. Von den Löchern in den Wänden ganz abzusehen. Der zweite Punkt war der Kündigungsverzicht, der selbstverständlich nicht wieder von vorne beginnen sollte (Verzicht bis September 2014, **nicht** Februar 2015). 

Doch der neue Vertrag hatte immer noch die Klausel drin uns das fanden wir nicht besonders cool. Es gingen einige Schreiben hin und her und der Februar stand vor der Tür. Unsere Sachbearbeiterin druckste ständig herum, dass erst der Eigentümer kontaktiert werden müsse und die Verwaltung das nicht entscheiden kann, blabla. Der Eigentümer hat seinen Sitz aber auf der Isle of Man, einem kleinen Steuerparadies zwischen Irland und Großbritannien. Weitere Schlussfolgerungen kann jeder selbst ziehen. 

Irgendwann hatte ich die Schnauze voll und rechnete ihr vor, wie viel Miete wir bezahlen werden wenn wir bis September in der kalten Wohnung bleiben. Dagegen stand natürlich eine höhere Summe für die neue und billigere Wohnung, weil wir womöglich nicht schon im September ausziehen. Einfache Mathematik zeigte ihre Wirkung und zu unserem Erstaunen verschwand die komplette Klausel aus dem Vertrag. Wir könnten also morgen kündigen und in 3 Monaten regulär ausziehen. Supi.

Für die unrenovierte Wohnung erhielten wir einen Einkaufsgutschein für einen örtlichen Baumarkt. Preislich war der ziemlich okay, so dass wir genug Farben und Zubehör kaufen konnten und dann relativ schnell die Wohnung gestrichen hatten. Der Umzug an sich gestaltete sich, obwohl es nur der Nebenaufgang war, eher mühsam. Mit den Möbeln halfen uns mal wieder Freunde, aber den ganzen Kleinkram buckelten wir zu zweit rüber. 

## Warme Wohnung

Die neue Wohnung ist warm. Einen richtigen 1:1 Vergleich können wir nicht machen, weil der Frühling nun schon vor der Tür steht, aber insgesamt sind wir zufrieden.    
Sie ist uns schöner gelungen als beim ersten Mal. Die Farben passen besser und die Einrichtung auch. Sie hat einen Raum weniger und den vermissen wir auch nicht. Die um 55 Euro höhere Miete der alten Wohnung auch nicht.    
Daneben gibt es viele kleinere und bessere Aspekte. Die Zimmertüren sind moderner, die Küchenschränke höher, der Balkon ist nicht so marode, die Küche hat statt eiskalten Fließen nun angenehmeres Laminat, das Waschbecken is größer und die Lage der Wohnung zwingt uns öfter die Treppen zu nehmen.

Vermissen werde ich die Aussicht von der 9. Etage. Verträumt auf die Lichter der Stadt schaue ich von hier nicht mehr. Etwas blöd ist auch: Die Deckenleuchten bekommen Strom über ein Kabel von der Wand (das Wohnheim grüßt). Das links-gedrehte Bad verlangt einen längeren Waschmaschienenschlauch und warum sich die Badtür nach innen öffnet, ist auch nicht logisch zu erklären. 

## Fazit

Wir hatten Pech und waren zu unerfahren. Bei der nächsten Wohnung werden wir ein besseres Auge für den Zustand haben und uns über die Verwaltung informieren (Stichwort Peloton GmbH). Der Umzug war Mitte Februar uns bis jetzt gab es noch Streit wegen fehlender Mietzahlung (wegen der Mietminderung).    
Dass man uns mehrere Monate lang hingehalten hat, ärgert mich immer noch und der einzige Grund, warum wir noch nicht gekündigt haben, ist der unnötige Umzugsstress für einige letzte Monate in Halle.