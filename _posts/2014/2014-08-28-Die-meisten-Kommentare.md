---
layout: post
title: "Die meisten Kommentare"
date: 2014-08-28
category: blog
tags: [Tupel Coding]
published: true
comments: false
share: true
---

Diesen Blog schreibe ich aus den verschiedensten Gründen. Mir macht das Schreiben Spaß, Schreiben hilft beim Denken und nicht zuletzt möchte auch, dass mein Geschriebenes gelesen wird. 

Wenn man etwas erschafft und es der Öffentlichkeit präsentiert, ist man auch auf Reaktionen neugierig, denn nur den wenigsten unter uns sind die Reaktionen egal. Ignorieren kann man sie sowieso nur selten. 

Auf meine Artikel gab und gibt es nur sehr wenig Reaktionen. Auf dem ersten Blog, drüben bei Googles Blogger, trudelten so wenig Kommentare ein, dass ich bei der ersten Version von *Tupel Coding* Kommentare gar nicht erst zuließ.      
Der meistkommentierte Artikel bei Blogger war nicht etwa eine kontroverse Meinung über Apple oder Android, sondern die Schilderung über das Einrichten des Wohnheim-Internets in Potsdam. Doch selbst diese Kommentare waren weniger *Kommentare* als viel mehr Fragen und Bitten um Hilfe.      

Da ich den alten Blog eine zeitlang online ließ, kamen auch noch Monate nach dem Launch von *Tupel Coding* Kommentare zur Wohnheim-Internet Problematik. Hatte ich anfangs noch nett geantwortet, verlor ich langsam die Geduld.       
*Anonym* schilderte mir ungefragt jede Kleinigkeit eines Problems und erhoffte sich scheinbar eine Antwort, so als ob ich genau der richtige Ansprechpartner sei. Und als ich auf die ZEIK verwies, das sind die Techniker der Uni und richtigen Ansprechpartner, wurde noch nach dem genauen Wortlaut der Frage an die Techniker gefragt. Bitte was? Oder ein Informatikstudent wollte gleich mal wissen, welche Ports er nutzen kann. Dieser Artikel hat mich zum technischen Support gemacht.

Die beste Lösung war das offline schalten des Artikels. Bisher habe ich ihn auch nicht für *Tupel Coding* angepasst und plane es nicht unbedingt. Reaktionen und Kommentare sind toll, aber das von über 200 Artikel genau dieser so viel Aufmerksamkeit erhält, ist schade.