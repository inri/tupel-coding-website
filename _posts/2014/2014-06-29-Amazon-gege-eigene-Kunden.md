---
layout: post
title: "Amazon gegen eigene Kunden"
date: 2014-06-29
category: blog
tags: [Amazon, Usability]
published: true
comments: false
share: true
---

Wir hatten auf Twitter eine Diskussion über Amazon und dabei wurden mir zwei Dinge bewusst. Erstens, Twitter eignet sich nicht gut zum Diskutieren. Und zweitens, ich sollte mir die Zeit nehmen um meinen Standpunkt zu verdeutlichen.

### Verhandlungen mit Verlagen

In Amerika und auch bei uns, steckt Amazon zur Zeit in Verhandlungen mit verschiedenen Buchverlagen. Amazon möchte bessere Konditionen (= niedrige Einkaufspreise bei Büchern) und setzt die Verlage unter Druck. Dazu verzögert es bspw. den Versand von Bücher der Verlage oder führt sie gar nicht mehr im Sortiment. Teils sind sogar schon DVDs und BluRays der Medienhäuser betroffen.    
Die Freude hält sich bei denen in Grenzen und sie beschwerten sich beim Kartellamt über Amazon. Was dabei herauskommt, steht in den Sternen

### Wer wird bestraft?

Ich behaupte, dass das eine untypische Strategie für Amazon ist und noch dazu eine ziemlich dumme. Amazon möchte die Verlage unter Druck setzen, aber geschädigt wird auch der Kunde und dadurch Amazon selbst. Dieser muss entweder auf seine Bücher warten oder findet sie gar nicht erst bei Amazon. Amazons Maxime war stets ein zufriedener Kunde und genau das wird jetzt ignoriert.    

Das Kontra-Argument lautet: Der Kunde kann ja woanders einkaufen. Das stimmt natürlich. Wenn ich etwas bei Amazon nicht finde, kann ich mir einen anderen Onlineshop suchen, der statt Amazon mein Geld bekommt. Trotzdem, ich bin der Meinung, dass ich als Kunde von Amazon bestraft werde und dadurch schadet sich Amazon selbst am meisten. Ich persönlich achte bspw. darauf, nicht kreuz und quer durch die Onlineshop-Welt zu tingeln. Ich möchte nicht unzählige Benutzerkonten und gerne eine zentrale Bestellhistorie. 

### Fallbeispiel Supermarkt

Unsere Diskussion drehte sich um die Frage, ob diese Taktik für Amazon schädlich ist oder nicht. Ein simples Beispiel wäre ein Supermarkt. Wenn es dort ab morgen keine Milch (oder ein anderes Grundnahrungsmittel) mehr zu kaufen gäbe, weil der Supermarkt die Milchbauern unter druckt setzt, schadet das natürlich den Bauern. Aber auch der Kunde des Supermarktes ist geschädigt, denn er muss für seine Milch einen anderen Markt aufzusuchen. Mit ein, zwei Produkten kann man das ja machen, aber wenn sich das häuft, kauft der Kunde irgendwann gar nicht mehr dort ein. Der Supermarkt schadet sich durch diese Taktik selbst. 

### Ökosysteme

Letztlich hängt an einem Amazon-Konto noch eine ganze Menge mehr. Da wären der Kindle und ihr Prime Angebot zu nennen. Ich bin in diesem Ökosystem verankert, genau wie bei Apple. Dadurch bin ich ihnen ausgeliefert, denn sie ändern die Regeln nach eigenem belieben.     
Meine Blindheit darauf zu hoffen, dass die Maxime der Kundenzufriedenheit nicht geopfert wird, nennt man Naivität. Vielleicht ärgern mich Amazons Schritte gegen die Verlage deshalb besonders? 

Jemand der nur alle paar Jahre ein Buch bei Amazon kauft, wird sich daran kaum stören und einfach den nächsten Onlineshop anklicken. Oder in die nächste Buchhandlung spazieren.