---
layout: post
title: "Freundlichkeit in Paris"
date: 2014-09-17
category: reisen
tags: [Reisen, Frankreich]
published: true
comments: false
share: true
image: 
  feature: 2014-paris-faces-lg.jpg 
  credit: Ingo Richter
---

Begeistert treten wir in die Pedalen unserer ausgeliehen Fahrräder und sausen die Bus- und Taxi-Spur der Straße hinunter. Links rauscht der Verkehr an uns vorbei, rechts flanieren die Pariser und von oben scheint eine milde Spätsommersonne. Ein perfekter Tag um diese tolle Stadt mit dem Rad zu erkunden.      

Ich habe den Weg zum nächsten Ziel grob im Kopf, bin mir aber unsicher ob wir nicht schon über das Ziel hinausgeschossen sind und längst hätten abbiegen müssen. Langsam bremse ich ab, bleibe stehen und krame mein iPhone aus dem Rucksack. Sophie kommt hinter mir zu Stehen und steigt ab. Dabei bleibt sie mit ihrem blanken Knöchel an der Spitze des Sattelspanners hängen und die Wunde beginnt sofort zu bluten. 

Schnell ist ein Taschentuch zur Hand und während ich abwechselnd Sophies Knöchel und die Maps App betrachte, mustert uns ein Kellner vom 10 Meter entferntem Café und geht schließlich wieder hinein. Die Wunde hört nicht auf zu bluten, doch den Weg habe ich gefunden. Gerade als ich beginne ein weiteres Taschentuch zu suchen, kommt der Kellner auf uns zu. Er lächelt, sagt etwas auf Französisch, gibt Sophie zwei große Pflaster und geht wieder zurück.

*Merci beaucoup!*

 