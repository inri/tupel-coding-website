---
layout: post
title: "Vorschau auf die Apple Watch"
date: 2014-09-26
category: it
tags: [Apple Watch] 
published: true
comments: false
share: true
image: 
  feature: 2014-apple-watch-lg.jpg 
  credit: Apple
  creditlink: http://www.apple.com

---

Ich habe über die [Apple Watch](http://www.apple.com/watch/) nachgedacht und gelesen, was andere über sie gedacht haben. Monate vor der Veröffentlichung kann man kein vollständiges Urteil abgeben, aber nun wissen wir zumindest, woran Apple die letzten Jahre gearbeitet hat. 


Und in welche Richtung diese Arbeit in den nächsten Monaten noch gehen könnte, lässt sich nun ganz gut abschätzen.

## Design

Mein erster Gedanke beim Anblick der Uhr war ungefähr: *Ui, die ist ja etwas dick, aber schick.* Auch mein zweiter Gedanke urteilte, dass sie gut aussieht und *noch* elegant ist. Was haben wir auch anderes erwartet? Auch Apple muss mit bestehenden Technologien arbeiten und dadurch wird eine Smartwatch von Apple nicht völlig anders aussehen[^1].     
Der Teufel steckt im Detail und das ist bei der Apple Watch die **digitale Krone**. Apple hat erkannt, dass der kleine Bildschirm allein als Input nicht ausreicht[^2] und auf Grund der Größe nicht optimal ist. Mit der traditionellen Uhrenkrone steuert man an der Apple Watch unter anderem Zoom und Auswahlen.      
Das ist übrigens ein Detail, welches man in dieser Form nur bei Apple findet. Andere Hersteller sind mit ihrem Smartwatches auf den Markt, aber bisher nicht erfolgreich und nicht besonders innovativ. Wie lange es wohl dauert, bis die ersten Kopien der Krone an anderen Smartwatches auftauchen?   

#### Personal Device

Heute gibt es das iPhone in vier Ausführungen (5c, 5s, 6, 6+) und in jeweils drei Farben, das iPhone 5c sogar in 5 Farbvarianten. Bis dahin war es ein weiter Weg und lange Zeit gab es nur **ein iPhone**. Diesen Weg beschreitet Apple mit der Watch nicht. Sie sind sich bewusst, dass eine Uhr noch persönlicher ist als ein Smartphone und dass die Käufer Auswahlmöglichkeiten haben möchten und sich nicht mit drei Farbvarianten abspeisen lassen.

Die Auswahl wird letztlich vom Geldbeutel getroffen. Welche Apple Watch soll es sein: *Edition* mit Goldgehäuse, *Sport* mit Aluminium oder die Standard Variante mit Stahl und Saphierglas? Alle Varianten bieten zwei Farbvarianten für das Gehäuse. Richtig individuell geht es bei den austauschbaren Uhrenbändern zu. Die sind nicht nur schick und pfiffig, sondern qualitativ hochwertig. Auf jeden Fall hochwertiger als bei den anderen Smartwatches und sogar als bei *traditionellen* Uhren in diesem Preissegment (beginnend ab 350€). Wobei wir natürlich noch nicht genau wissen, ob alle Bänder gleich viel kosten.

Übrigens zum Thema Display: Ich denke ein rundes Display, wie die [Moto 360](https://moto360.motorola.com) es hat, wird die Apple Watch in absehbarer Zeit nicht bekommen. Eine solche Form ist **nur** für ein Zifferblatt sinnvoll. Die Moto 360 ist der beste Beweis. Die *Android Wear* Erweiterung unterstützt zur Zeit nur viereckige Displays und deshalb werden die Ecken einfach abgeschnitten, so dass der Benutzer nicht alle Informationen sehen kann. Dann stellt sich auch die Frage, wie ein UI für ein rundes Display aussehen soll? Davon sind wir noch einige Jahre entfernt. 

## Funktionen

Die andere große Frage, neben dem Aussehen der Apple Watch, war die Frage nach den Funktionen einer Smartwatch. Im Fitness-Bereich gibt es bereits *rudimentäre* Armbänder und man möchte sicherlich auch sein iPhone nicht immer für jede Kleinigkeit aus der Tasche hervorkramen. Man kann schon mal so viel sagen: Ein iPhone am Arm hat Apple definitiv nicht vorgestellt.  

Die Grundfunktionalitäten lassen sich schnell aufzählen: Nachrichten, Telefonieren, Email, Bezahlen mit Apple Pay, Kalender, Musik, Maps, Apple TV steuern, Fotos, Kamera-Kontrolle, Siri, Wetter und diverse Zeitfunktionen. Das alles wird Apple bereitstellen, natürlich zugeschnitten auf ein kleines Gerät wie die Apple Watch.

#### Zifferblatt  

Neben der Möglichkeit, die Apple Watch durch Armbänder äußerlich anzupassen, kann man die vermutlich wichtigste Funktion einer Uhr ebenfalls frei wählen: Das Zifferblatt. Es gibt eine reiche Auswahl an mehr oder weniger schönen Arten die Zeit anzuzeigen. 

#### Walkie-Talkie deluxe

Nicht immer möchte man sich lange Nachrichten schicken, die man der Apple Watch entweder diktieren oder für die man gleich sein iPhone benutzen muss. Dafür bietet Apple vier verschiedene Möglichkeiten, sich kurze Mitteilungen zu schicken, von Watch zu Watch. Man kann eine kleine **Skizze** zeichnen, einen **Sound/Ton** aufnehmen (wie ein Walkie-Talkie), einen **Rhythmus** tippen oder den eigenen **Herzschlag** schicken.  

#### Aktivitäts-Tracker 

Selbstverständlich kann ich meine Sportlichkeit auch mit der Apple Watch tracken. Genauer gesagt wie viel ich laufe, Sport treibe und sogar wie viel ich stehe. Wie bei allen Apps dieser Art, lassen sich auch Ziele setzen und Erfolge verdienen.

Leider lässt die Apple Watch einen GPS-Chip vermissen und das ist mein größter Kritikpunkt an ihr. Um zu tracken, wie viel ich mich bewege, muss sich die Apple Watch in der Nähe des iPhones befinden und bekommt die Werte von diesem. Ich finde nicht, dass der Zwang, die Watch mit einem iPhone zu verbinden, schlimm ist, aber dass der Tracker danke fehlendem GPS nicht autark funktioniert, ist wirklich sehr schade.

#### Überraschungen inklusive

Beobachter weisen zurecht darauf hin, dass Apple noch nicht alle Katzen aus dem Sack gelassen hat. Bis zum Release ist noch etwas Zeit und der Konkurrenz muss nicht alle Features zeigen. 

#### Upgrade your Watch

Wer weiß was Apple uns im Frühjahr präsentieren wird? Gruber bringt beispielsweise eine Upgrade-Möglichkeit ins Spiel. Eine teure Rolex kauft man für die Ewigkeit. Eine Apple Watch kostet unabhängig von der Auswahl zwar nur einen Bruchteil davon, aber möchte man nicht jedes Jahr eine neue kaufen, egal ob sie ein Stahl-, Aluminium-, oder Gold-Gehäuse hat. Apple selbst wirbt beim S1 Chip der Watch erstmals mit *An entire computer architecture on a single chip.* und spricht ausdrücklich nicht von *System*.      
Ist es so abwegig zu hoffen, dass es möglich sein wird, den eingebauten Chip der Watch zu wechseln? Die zweite Watch wird neben Verbesserungen am Design, Bildschirm und so weiter, sicherlich auch die Leistung des Chips verbessern. Gegen einen Aufpreis könnte man den neuen Chip in die alte Watch einbauen lassen und ist dann zwar nicht up-to-date, hat aber auch kein nach einem Jahr völlig veraltetes Gerät am Handgelenk. 

### Akkulaufzeit 2015 bewerten

In so gut wie allen Meinungen wurde die Akkulaufzeit thematisiert und kritisiert. Viel ist aber nicht bekannt, außer dass die aktuellen Prototypen der Watch nur knapp einen Tag laufen und Apple selbst nicht zufrieden ist. Man kann davon ausgehen, dass die Gesamtlaufzeit der richtigen Apple Watch länger sein wird.     
Falls nicht, ist das wirklich ein großer Kritikpunkt, aber bis wir nichts genaueres wissen, ignorieren wir diesen Aspekt erst einmal.

## Fazit

Apple hat einen guten Grundstein für eine Smartwatch gelegt. Dass sie in den nächsten Jahren dünner und mehr Funktionen erhalten wird, bezweifelt wohl kaum jemand. Edel sieht sie jetzt schon aus.

Werde ich mir in *early 2015* eine Apple Watch kaufen? Ich tendiere sehr stark zu **Ja!** Es gibt zwar noch einiges, was ich über die Watch wissen möchte, aber mir gefällt ihr Aussehen, mich überzeugen die Funktionen und ich bin verdammt neugierig. 

Was die Entwickler bis dahin ermöglichen werden, wird natürlich auch die Apple Watch besser machen.  

[^1]: Kritisieren möchte ich an dieser Stelle die angeblich kreativen Designer, die uns seit Monaten mit ihren Entwürfen einer der erfreuen. Die können zwar ein 3D-Modellierungtool bedienen, sind aber eigentlich keine Designer. Futuristische Designs gehören in SciFi-Filme. Produktdesigns sollten sich an physikalische Gesetze halten und eine 0,5 cm dicke Smartwatch tut das eben nicht. 

[^2]: Und dass selbst ein Sprachassistent wie Siri oder Google Now als Erweiterung auch nicht ausreichend ist.