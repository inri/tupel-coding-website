---
layout: post
title: "Guardians of the Galaxy"
date: 2014-09-18
category: medien
tags: [Film]
published: true
comments: false
share: true
image: 
  feature: 2014-Guardians-of-the-Galaxy-lg.jpg
  credit: Marvel
  creditlink: http://marvel.com/guardians
---

Das beste am ersten Trailer zu dieser weiteren Comic-Verfilmung von Marvel war die Musik. Über die Story verriet er nicht viel und stellte stattdessen einige seltsame Gestalten vor. Zumindest ich ahnte noch nicht, dass *Guardians of the Galaxy* die **beste SciFi-Action-Comic-Komödie in diesem Jahres** wird.

### Awesome Mix Vol. 1

Der Ohrwurm aus dem Trailer, *Hooked On a Feeling* von Blue Swede, ging mir nicht mehr aus dem Kopf und so freute ich mich, als zusammen mit dem US-Start des Films Anfang August auch der Soundtrack herauskam. Und der Soundtrack ist wirklich gut. Wenn der Film nur halb so gut ist, wie seine Musik, lohnt sich ein Besuch, dachte ich naiv.     
Und der Besuch hat sich mehr als gelohnt. Die Musik wird von der ersten Minute an beeindruckend eingesetzt und ist zweifelsohne einer der besten Elemente des Films. Ob im Hintergrund oder als Aufhänger für eine komplette Szene, sie setzt großartige Akzente. Dabei merkt man auch, dass viel Liebe in der Auswahl der Lieder gesteckt wurde. Alle stammen aus den 70er und 80er Jahren und wer weiß wie viele Perlen der zweite Teil noch hervorzaubert. 

Die *Guardians* sind eine Gruppe aus fünf völlig unterschiedlichen Charakteren und das birgt in Filmen eine gewisse Gefahr. Doch hier wird alles richtig gemacht. Jede Figur bekommt eine tolle Geschichte und keine bleibt blass oder im Hintergrund. Selbst der Baum Groot, der nicht der gesprächigste ist, wird zum Publikumsliebling. Dabei gelingt es dem Film, jede Figur interessant und gleichzeitig einzigartig zu machen. Keine ist ein schon hundertmal auf der Leinwand gesehenes Klischee.

Das ist ein Lob an die Comics, das Drehbuch und natürlich auch den Cast. Die Avengers haben teils alleine schon gut funktioniert und waren dem Zuschauer bekannt. Hier nahm man fünf völlig neue Figuren und es funktionierte auch. Das geht auch über die Gruppe hinaus und trifft auf die vielen Nebendarsteller zu. Eine besondere Freude war es *Merle* aus *The Walking Dead* in der Rolle eines Piraten / Schatzjäger / Gauner zu sehen. 

Ich werde an dieser Stelle nichts zur Story verraten, nur so viel sei gesagt: Das Erzähltempo ist hoch und es macht Spaß. Das ist natürlich nicht verwunderlich, schließlich dreht sie sich zu einem großen Teil um die Gruppe. Der Film lässt sich auch in keine Genre-Schublade stecken und bietet für jeden etwas. Der rote Faden ist der Humor und die guten Sprüche.

Technisch muss man auch nicht viel zu den Marvel-Verfilmungen sagen. Klasse. Mit den Schauplätzen und den vielen Alien-Rassen erinnerte es mich an das Star Wars Universum. Es gibt zwar keine Jedis, aber eine ganze Menge Vielfalt und sehenswertes.

### Fazit

Ein wirklich toller Film. Mitreißende Story, fabelhafte Figuren und sehr, sehr lustig. Ich kann ihm nur jeden ans Herz legen. Man muss kein Comic-, SciFi- oder Action-Fan sein um den Film zu mögen. Er deckt viele Genres ab und ist unglaublich gut. Einer der besten Filme in diesem Jahr, definitiv. 



