---
layout: post
title: "Kunst auf der Burg Giebichenstein"
date: 2014-08-21
category: blog
tags: [Kunst, Halle, iPhone Shots]
published: true
comments: false
share: true
image: 
  feature: 2014-giebichenstein-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Man kann ja nicht immer nur vor dem Computer sitzen. Oder dem iPhone. Raus in die Welt und mal unsere Stadt näher anschauen, das war der Plan. Und Mitte Juli stellten Kunststudenten auf der Burg Giebichenstein in Halle ihr Schaffen aus. Wir waren trotz glühender Hitze dort. 

Ich zeige einfach einige Kunstwerke und gebe qualifizierte Kommentare ab.

<figure class="half">
	<a href="/images/2014-giebichenstein-vasen-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-vasen-400.jpg" 
		  alt="Unperfekt Vasen"></a>
	<a href="/images/2014-giebichenstein-pferd-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-pferd-400.jpg" 
		  alt="Kleines Pferd"></a>
	<figcaption>Vasen und hängendes Pferd</figcaption>
</figure>

Die Vasen sind relativ faszinierend. Wenn man bedenkt, dass es früher die hohe Kunst war, möglichst perfekte Formen zu schaffen, scheint heute ein anderer Trend in Mode zu sein. Die Vasen sind regelrecht unperfekt, wirken fast schon quick &amp; dirty zusammengedreht. Der Verkauf ging trotzdem ziemlich gut, obwohl sie im 3-stelligen Preissegment angesiedelt waren.

<figure class="half">
	<a href="/images/2014-giebichenstein-bueste-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-bueste-400.jpg" 
		  alt="Büste eines Wüstennomaden"></a>
	<a href="/images/2014-giebichenstein-hannibal-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-hannibal-400.jpg" 
		  alt="Hannibals Kopf"></a>
	<figcaption>Büsten</figcaption>
</figure>

Hannibal lässt grüßen, zumindest auf dem rechten. Die Büsten waren zwar genauso unperfekt wie die Vasen, aber zu ihnen passt es aus ästhetischer Sicht eher. 

<figure class="half">
	<a href="/images/2014-giebichenstein-kleid-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-kleid-400.jpg" 
		  alt="Mode von morgen"></a>
	<a href="/images/2014-giebichenstein-obst-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-obst-400.jpg" 
		  alt="Vergammelndes Obst"></a>
	<figcaption>Handwerk und Gammelobst</figcaption>
</figure>

Es gab viel und interessante Mode, die Kleider sind nur ein Beispiel. Das Obst war dann schon wieder ein Kunstwerk für sich. In jeden der drei Glaswürfel waren verschiedene Obstsorten und gammelten fröhlich vor sich hin. Auf jeden Fall eine witzige Idee, nur leider waren die Scheiben sehr stark beschlagen, so dass man die Gammele nicht in voller Pracht anschauen konnte.

<figure class="half">
	<a href="/images/2014-giebichenstein-blatt-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-blatt-400.jpg" 
		  alt="Abschlussarbeit in Modellierung"></a>
	<a href="/images/2014-giebichenstein-skulptur-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-skulptur-400.jpg" 
		  alt="Mann mit Schädel"></a>
	<figcaption>Skulpturen</figcaption>
</figure>

In einem Raum waren mehrere von diesen kleinen Skulpturen oder Modellierungen (links) ausgestellt. Das waren aktuelle Arbeiten und neben jedem lag ein Bewertungszettel. Warum das Blatt nur eine 2,5 bekam, war uns leider nicht klar.

<figure class="half">
	<a href="/images/2014-giebichenstein-quichote-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-quichote-400.jpg" 
		  alt="Don Quichote"></a>
	<a href="/images/2014-giebichenstein-skizzen-1600.jpg">
		<img 
		  src="/images/2014-giebichenstein-skizzen-400.jpg" 
		  alt="Skizzen von Menschen"></a>
	<figcaption>Skizzen und Malerei</figcaption>
</figure>

Skizzen und Malerei gab es relativ wenig, doch das wenige war ausgesprochen  interessant. Besonders die Skizzen sind handwerklich toll und das ist auch meine liebste Art von Kunst.

Eine Anmerkung zum Schluss: Die Titel im *alt*-Tag der Bilder sind von mir und nicht die offiziellen Namen der Kunstwerke. 