---
layout: post
title: "Es gibt kein digitales Zeugnis"
date: 2014-06-19
category: it
tags: [Persönlich, Uni]
published: true
comments: false
share: true
---

Als mein Abschlusszeugnis inklusive Urkunde zum Bachelorstudium endlich mit der Post eintrudelte, steckte ich schon bis zum Hals in Arbeit für meinen Job. Die Dokumente landeten im Studium-Ordner. Bis gestern.     

Ich beschließe sie zu scannen, um sie in digitaler Form meinen Bewerbungsunterlagen hinzuzufügen. Dabei fällt mir ein gravierender Fehler auf allen Dokumenten ins Auge. Statt der eigentlich korrekten Note &quot;*Gut*&quot;, prangt neben meiner Notenzahl ein &quot;*Befriedigend*&quot;.

Das Problem ist mit dem Studienreferat schnell geklärt. In schon zwei Wochen bekomme ich die neuen, korrigierten Unterlagen und die fehlerhaften soll ich zurückschicken. Zwei Dinge wundern mich trotzdem.    
Scheinbar trägt jemand die Note und die Notenzahl per Hand oder separat ein (obwohl es eine offizielle Skala für die Zuordnung der Note zur Notenzahl gibt). Sonst wäre der Fehler nicht aufgetreten.   
Und wieso muss ich zwei Wochen auf die neuen Dokumente warten? [^1]

In der IT-Branche generell und vielen Firmen, möchte man digitale Bewerbungen, meist als PDF-Dateien. In zwei Wochen bekomme ich die Dokumente und muss sie erst einmal scannen. Dass sie zu Hause eingescannt wurden, sieht man ihnen im Verbund mit den anderen Dokumenten an. Deshalb hätte ich gerne in weniger als zwei Wochen von der Uni eine digitale Kopie.    
Das geht aber nicht.

&quot;*Dokumente werden erst durch Unterschriften und Siegel zum Zeugnis. Unterschrift und Siegel sind im Original aufzutragen. Damit erklärt sich, dass das Zeugnis nicht digital vorliegt.*&quot;

Das hört sich für mich nach einer bürokratischen Zwickmühle an. Und so werden wohl auch in Zukunft die schicken digitalen Bewerbungen mit mittelmäßig hübschen Zeugnis-Scans durchsetzt sein.     

[^1]: Weil wichtige Menschen von der Uni die Dokumente erst unterschreiben müssen.