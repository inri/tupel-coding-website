---
layout: post
title: "Alle Artikel online"
date: 2014-08-23
tags: [Tupel Coding]
category: blog
published: true
comments: false
share: true
---

Das hier ist nicht mein erster Blog. Seit 2011 habe ich auf Googles Blogger Plattform meine Gedanken veröffentlicht und bis letztes Jahr haben sich dort so einige Artikel angesammelt. 

Nachdem ich auf Jekyll und Markdown umgestiegen bin, konnte ich die *Blogger-Artikel* leider nicht ohne Aufwand transformieren.      

So habe ich in den letzten Monaten **jeden alten Artikel** überarbeitet. Zum einen betraf das die Form: Absätze, Links, Fotos, Tags und Zitate. Und zum anderen auch Ausdruck und Rechtschreibung. Ich habe keinen Artikel komplett umgeschrieben, aber hier und da Formulierungen angepasst. Besonders auch bei Themen, die zukünftige Entwicklungen betrafen, habe ich meine Aussagen nicht geändert, auch wenn sie sich später als falsch herausstellten.      
Auch mein Umgang mit Bindestrichen und Anführungszeichen habe ich angepasst bzw. beide auf ein Minimum reduziert und stattdessen mit kursiver und fetter Schrift gearbeitet.

Hier und da fehlen noch einige wenige Artikel und hier und da fehlen noch einige Fotos. Beides wird nachgereicht, wenn es sinnvoll ist. Bei vielen Artikeln kamen neue Titelfotos hinzu. Viele Links waren erstaunlicherweise noch intakt. 

Intern waren meine Fotos relativ chaotisch was die Benennung angeht. Dafür habe ich mir jetzt sogar einen Guide geschrieben, wie groß die Fotos sein und wie sie benannt werden sollen. Der wird natürlich auch noch veröffentlicht.

Aktuell hat mein Blog 307 Artikel, die Fotos-Artikel miteingeschlossen. Das sind insgesamt nicht einmal 2 MB an Markdown-Dateien. Fotos gibt es dagegen nur 192, aber die nehmen knapp 30 MB ein. 

Die kommenden kleineren Updates werde ich nicht weiter kommentieren. Habt einfach Spaß beim Lesen!