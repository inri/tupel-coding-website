---
layout: post
title: "2 Jahre Retina MacBook Pro"
date: 2014-07-29
category: it
tags: [Apple, MacBook]
published: true
comments: false
share: true
---

CoconutBattery verrät mir nicht nur, dass mein MacBook Pro noch 88% seiner ursprünglichen Akku-Kapazität besitzt, sondern auch sein Alter: 24 Monate. Zwei Jahre arbeite ich nun mit diesem wundervollen Gerät und es ist immer noch eine Freude. 

Wir hatten einen holperigen Start. Die eingebauten Federn, die die empfindliche Hardware im Inneren vor der Unterseite schützen sollen, knarzten bei großer Belastung auf der Oberseite (Handballen). Ein Besuch im Apple Store und ein wenig Reinigung, minimierte das Problem. Verschwunden ist es bis heute nicht gänzlich, tritt aber nur selten auf und ich kann sehr gut damit leben. 

Das war vermutlich auch das negativste aus den zwei Jahren. Sicherlich würde ich gerne nachträglich mein RAM auf 16GB verdoppeln und die 500 GB meiner SSD wurden auch schon mal knapp. Letzteres lässt sich vermeiden, würde ich meine Bilder und Video Galerie extern verwalten und nicht 70 GB VMs mit mir herumschleppen.     
Es gab auch die einen oder anderen Abstürze von OS X, aber nie gingen Daten verloren und meine TimeMachine ist auch fleißig. 

Dagegen ist es jeden Tag ein Genuss auf diesen Retina Bildschirm zu schauen. Am meisten beeindruckt mich die Leistung nach zwei Jahren. Ich habe überhaupt nichts zu meckern! Er ist flink wie am ersten Tag und das Arbeiten mit ihm ist flüssig. Einerseits erwartet man das von 2,6 GHz QuadCore auch, andererseits erinnert man sich an andere Computer, deren Einzelteile nach zwei Jahren Benutzung gewechselt werden mussten.

Der Vorgänger war übrigens ein 13 Zoll MacBook Pro und eigentlich okay. Doch trotz neu eingebauter SSD wuchs nach gut 1,5 Jahren der Wunsch auf einen neuen Mac. Es wurde mit der Zeit langsamer und ein Fan des Gewichts war ich auch nie. Nur leider waren und sie die MacBook Airs noch nicht performant genug, vom fehlenden Retina Bildschirm ganz abgesehen. Nach knapp 2,5 Jahren übergab ich meinen erstes MacBook an meinen Bruder [^2].

Zusammengefasst: Ich bin mit meinem Retina ziemlich glücklich. Der Bildschirm ist toll, die Performance ist toll und das Gewicht ist toll. Ich kann mir vorstellen ihn locker noch zwei Jahre zu nutzen [^3].     
Heute hat Apple die Retina-Reihe mit 16 GB RAM und besseren Prozessoren [aktualisiert](http://store.apple.com/de/buy-mac/macbook-pro). Ganz klare Empfehlung von mir, denn einen solches Notebook bekommt ihr nirgendwo sonst [^4]!

[^1]: Beim Alter muss man ca. 1 Monat abziehen, da ich das MacBook ja nicht sofort nach Launch und auch nicht direkt von der Fabrik abgeholt habe. In meinem Besitz ist es also ca. 23 Monate.

[^2]: Mein Bruder nutzt mein altes 13" MacBook Pro (7,1) und ist noch ganz glücklich denke ich. Es hat schon 51 Monate auf dem Buckel und trotz 1126 Ladezyklen noch 81% der ursprünglichen Akku-Kapazität. Respekt.

[^3]: Mal sehen ob Retina iMacs bald Wirklichkeit werden, so einer wäre interessant.

[^4]: Als Referenz muss ein Kommilitone herhalten. Er wollte Ende letzten Jahres sein MacBook ersetzen und suchte vergeblich nach einem Windows Notebook mit viel Performance, Retina Bildschirm und akzeptablem Preis. Nichts. Das Retina war ihm zu teuer, doch jetzt ist er damit auch sehr glücklich. Wenn er nicht gerade Kaffe drüber kippt...