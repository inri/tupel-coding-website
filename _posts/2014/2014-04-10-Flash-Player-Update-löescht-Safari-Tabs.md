---
layout: post
title: "Flash Player Update löscht Safari Tabs"
date: 2014-04-10
category: it
tags: [Web]
published: true
comments: false
share: true
image: 
  feature: 2014-flash-player-lg.jpg
---
 
Die Feststellung, dass der Adobe Flash Player ziemlicher Mist ist, wundert sicherlich nur die wenigsten. Ich möchte aber gar nicht über katastrophale Webseiten schreiben, sondern man fasst sich schon bei der Installation eines Flash Player Updates an den Kopf.

Ab und zu ploppt eine Updatemeldung für den Flash Player auf und wenn man die Installation startet, erscheint der obere Hinweis. Okay, kennt man auch von anderen Installationen mit anderen Browsern. Andere Installationen bieten aber meist ein *Wir schließen Safari für dich!* an. Der Flash Player nicht.

Wenn ich mein Safari schließe, werden in der Regel alle offenen Tabs abgespeichert und öffnen sich beim nächsten Start wieder, ganz von alleine. Seltsamerweise passiert das nach der Flash Player Installation nicht. Safari öffnet sich und zeigt mir eine leere Adobe Seite, aber von meinen alten Tabs keine Spur.

<figure>
    <a href="/images/2014-flash-player-1600.jpg">
	   <img 
		  src="/images/2014-flash-player-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Eines hat Adobe schon erkannt, mich interessieren keine weiteren Produkte</figcaption>
</figure>

Ich nutze Safari und vielleicht ist auch Safari der Grund für den Fehler, doch Adobe ist für die Usability verantwortlich. Ich habe mich schon beim letzten Flash Player Update darüber geärgert. Wieso muss mir Adobe seine ranzige Seite unter die Nase reiben? Noch dazu, wenn die Seite quasi leer ist?