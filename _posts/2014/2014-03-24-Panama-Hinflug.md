---
layout: post
title: "Panama 1: Hinflug"
date: 2014-03-24
category: reisen
tags: [Reisen, Panama]
published: true
comments: false
share: true
image: 
  feature: 2014-panama-title-hinflug-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Viel zu spät bin ich am Vorabend ins Bett gegangen, so dass meine Netto-Schlafzeit bestenfalls 2,5 Stunden beträgt. Andererseits fühlt sich das mehr wie ein langes Mittagsschläfchen an und nach 4 Stunden Schlaf hätte ich mich sicherlich müder gefühlt.   

Das ist der erste Teil einer Reihe über meine Reise nach Panama. Dieser Teil hat ausnahmsweise einen tagebuchartigen Stil, die kommenden Artikel werden kürzere Berichte sein. [Teil 2](http://ingorichter.org/article/Panama-City/), [Teil 3](http://ingorichter.org/article/Panama-Bijao-Beach/), [Teil 4](http://ingorichter.org/article/Panama-Panamakanal/), [Teil 5](http://ingorichter.org/article/Panama-Abschluss/) 

## 01:35 Aufstehen
  
In einer gute Stunde holt uns der Chef ab und wir fahren nach Tegel. Das Ziel ist nicht sofort der Flughafen, sondern eine Parkplatzvermietung. Dort stellen wir das Auto ab und werden mit einem Shuttle zu unserem Gate gefahren. Inklusive Abholservice und Parkplatz für 5 Tage kostet der Spaß nur 40€. Klare Empfehlung.

## 05:00 Boarding I

Pünktlich 90 Minuten vor Abflug rollen unsere Koffer das Band hinunter. Bei der Sicherheitskontrolle fördert Papa ein altes Schweizer Taschenmesser zu Tage und darf es zu unserer Überraschung sogar mitnehmen. So lange die Klinge nicht länger als eine Kreditkarte ist, sei das wohl okay. Doch in  Anbetracht der noch kommenden Sicherheitskontrollen, besonders in den USA, wird es vorsorglich entsorgt. Und ich habe eine Idee für ein Geburtstagsgeschenk.

Der erste Flug, nach Frankfurt, läuft gut und ist ja auch eigentlich nur ein 75-minütiger Kurztrip. In Frankfurt gelandet, wird unsere Ahnung zur Gewissheit: Der Flug nach Dallas geht tatsächlich zwei Stunden später und dadurch schaffen wir mit Sicherheit nicht den Anschluss nach Panama-Stadt. Die Reise geht ja gut los.

## 09:00 Umbuchen

Unsere planmäßig letzten beiden Flüge sollten wir eigentlich mit American Airlines machen. Am Gate 4 lassen wir die debilen Fragen des Sicherheitsinterviews über uns ergehen und bekommen unseren Flug von Dallas nach Panama-Stadt umgebucht. Bei der Auswahl zwischen Dallas-Miami-Panama-Stadt (Ankunft morgen 11:00) oder Dallas-San Salvador-Panama-Stadt (Ankunft heute 23:30) entscheiden wir uns für die kürzere Variante. Ursprünglich sollten wir 19:40 in Panama ankommen.

Es ist noch Zeit bis zum Boarding. Ich kaufe mir ein lächerlich bequemes Nackenkissen für einen lächerlich hohen Preis. Im Wartebereich finden sich einige Steckdosen und mein iPhone kann sich [vom GPS-Tracking erholen](http://de.fogofworld.com).    
Unsere Koffer müssen auch umgebucht werden und es taucht ein Problem auf. Einer unserer Koffer ist nicht in Frankfurt angekommen, aber welcher, das wissen wir nicht. Was ist denn heute los?

## 10:40 Boarding II

<figure>
	<img src="{{ site.url }}/images/2014-panama-1-cabin.jpg" alt="Flugzeugkabine">
	<figcaption>Point of Ingo für 10 Stunden</figcaption>
</figure>

Die Maschine ist riesig, zumindest für mich. 9 Personen in einer Reihe hatte ich bisher noch nicht. Ich sitze am rechten inneren Gangplatz und habe auf den drei Plätzen neben mir niemanden. Kurz vor Abflug kommt ein Mitarbeiter vom AA Bodenpersonal zu uns und teilt uns fröhlich mit, dass der Koffer doch noch gefunden wurde. Ich verabschiede mich von meinen Gedankenspielen, was sich in Panama für die Entschädigungssumme alles kaufen lässt. 

Wir sind in der Luft und es ist laut. Der erste Getränkewagen kommt und ich greife mir eine Cola. Zum Mittag gibt es neben Salat und einem Minibrötchen die Auswahl zwischen Nudeln und Chicken. Genau vor meiner Nase wird das letzte Chicken verteilt. Die Nudeln schmecken aber auch. Zum Essen gibt es neben einer kleinen Flasche Wasser ein Becher Wunschgetränk und nach dem Essen wieder eine komplette Dose Cola. 

<figure>
	<img src="{{ site.url }}/images/2014-panama-2-lunch.jpg" alt="Mittagessen im Flugzeut">
	<figcaption>Das Mittagsmenü</figcaption>
</figure>

Beim Versuch die Zeit meiner Armbanduhr zu verstellen, geht sie mir kaputt und das ist ziemlich Scheiße. Macbook, iPhone und iPad halten die Umstellung besser aus. Nach amerikanischer Zeit ist es jetzt 7 Uhr morgens und wir werden 15:30 ankommen. Der Flug ist noch sehr lang.   

Auf allen Plätzen liegen Decken aus und die sind auch bitter nötig. Meine Beine sind zwar warm, aber meine Finger kalt. Dieses Gefühl ist mir wohlbekannt und die Erinnerungen an unsere erste und leider kalte Wohnung in Halle sind noch frisch. Im Prinzip ist es wie zu Haue hier, nur das ich mich mit 900km/h in 11.000 Metern Höhe fortbewege und kein Internet habe.

## 18:30 Sitzen

Der größte Teil der Strecke liegt hinter mir. In genau drei Stunden soll die Ankunft sein. Das teure Nackenkissen ist wirklich gut und sein Geld wert. Schlafen fällt mir trotzdem schwer, denn hinter mir sind scheinbar die Kinder aufgewacht und kreischen nun herum. Musik ist ein ganz guter Lösungsansatz, nur leider scheint mein iPod Shuffle nicht mehr in den normalen Abspielmodus zu kommen. Rockmusik a lá Blink-182 oder Foo Fighters klingt ziemlich schrill. Gloria oder Bob Dylan vertragen sich sehr gut mit der Geräuschkulisse. 

<figure>
	<img src="{{ site.url }}/images/2014-panama-3-movies.jpg" alt="Filme und Serien im Flugzeug">
	<figcaption>Die Film- und Serienauswahl war okay</figcaption>
</figure>

Noch musste ich das stille Örtchen noch nicht aufsuchen und so  richtig scharf bin ich auch nicht drauf. Ich sehe von meinem Platz sehr gut wie hoch der Andrang ist und bin froh nicht direkt davor zu sitzen.     
Die Film- und Serienauswahl ist überraschend aktuell und so könnte ich den zweiten Hobbit Teil, Frozen, Nebraska und weitere aktuell noch im Kino laufende Filme anschauen. Aber die kleinen Bildschirme sind ziemlich räudig und so lasse ich mir lieber die Flugroute inklusive Messdaten anzeigen.

## 20:00 Toilette

Nun doch ab zur Toilette, das Wasser ließ sich nicht mehr halten. Sie ist aber echt sauber, was ich gar nicht erwartet hätte. Ich konnte sogar einige Stunden schlafen und verbringe die restliche Zeit mit dem Anschauen meiner mitgebrachten Serien.   

<figure>
	<img src="{{ site.url }}/images/2014-panama-4-wc.jpg" alt="Rauchverbot">
	<figcaption>Rauchen war im gesamten Flugzeug verboten</figcaption>
</figure>

Es gibt einen letzten Snack. Eine Art Pizza. Schmeckt.   
So langsam habe ich vom Flug genug. Ich habe zwar luxuriös viel Beinfreiheit und mir geht es sicherlich besser als den meiste, aber 9 Stunden sind heftig.

Anflug Texas! Ich sehe aus den Fenstern nicht viel, aber was ich sehe ist verdörrt und trocken. Texas eben.  
   
<figure>
	<img src="{{ site.url }}/images/2014-panama-5-dallas.jpg" alt="Flugzeug nach Dallas">
	<figcaption>In dieser Maschine waren wir über 10 Stunden drin</figcaption>
</figure>

## 22:30 USA

Um in den USA umzusteigen braucht man schon nerven. Zuerst muss man, idealerweise im Flugzeit, eine Zollerklärung ausfüllen. Warum das ganze, obwohl man ja eigentlich weiterfliegt? Naja, wir könnten quasi auch ohne Visum in die USA spazieren und deshalb muss auch der folgende Quark gemacht werden. Zuerst wartet man bei der Passkontrolle. Dort werden die Finger gescannt und ein Foto gemacht. Dann holt man entweder sein Gepäck oder eben nicht. Mit dem Zeug geht es dann noch mal zum Zoll und danach ist man im Flughafen und sucht entweder sein Gate oder lässt es.

Insgesamt haben wir über eine Stunde gebraucht, kamen aber rechtzeitig zum Flieger. Warum man nicht einen extra Transit für Weiterfliegende organisiert, ein Rätsel.

## 23:30 Vorletzter Flug

Vor uns liegt ein knapp 4-Stunden Flug nach San Salvador. Das liegt übrigens in El Salvador, dem kleinsten südamerikanischen Land. Zumindest mir war dessen Existenz nicht bewusst.   

<figure>
	<img src="{{ site.url }}/images/2014-panama-6-dallas.jpg" alt="Dallas von oben">
	<figcaption>Dallas von oben beim Flug nach San Salvador</figcaption>
</figure>

Das ist wieder ein kleineres Flugzeug mit 4 Personen pro Reihe. Erstaunlich sind die Hightech-Sitze. Naja, sie haben auch einen Touch-Bildchirm wie beim großen Flugzeug, aber die Software ist definitiv besser. Die Filme und Serien sind größtenteils identisch. Richtig gut ist aber der USB-Anschluss! So lasset unsere iPhones laden...

<figure>
	<img src="{{ site.url }}/images/2014-panama-7-movies.jpg" alt="Eingebaute Monitore in Fliegersitzen">
	<figcaption>Das beste an diesem Flugzeug: USB-Anschluss!</figcaption>
</figure>

Zum Essen gibt es wieder die Wahl zwischen Nudeln und Hühnchen und diesmal ist **pollo** auch für mich da. Schmeckt. Ein großes Lob an dieser Stelle für den Muffin, der war richtig gut.

<figure>
	<img src="{{ site.url }}/images/2014-panama-8-dinner.jpg" alt="Abendbrot im Flugzeug mit Polo">
	<figcaption>Endlich auch Pollo für mich</figcaption>
</figure>

## 03:00 San Salvador

Ich wundere mich beim Landeanflug über die Hauptstadt. Es ist dunkel, aber man sieht richtig wenig Lichter und die wenigen sind Straßenlaternen. So als ob die Häuser nicht beleuchtet sind. (Nachtrag: Der Flughafen liegt etwas sehr weit außerhalb. Warum aber am Horizont nicht viel zu erkennen war?).

Der überraschendste Moment: Wir steigen aus der Maschine aus und gehen die Treppe hinunter. Ich denke zuerst, dass die Triebwerke ja ganz schön viel Wärme abstrahlen und muss wenige Meter später bemerken, dass das einfach die ganz normale Temperatur ist. Es ist warm und feucht. Wow. Wie in einem Tropenhaus. Ich muss grinsen. 

Der Flughafen ist klein. Alberne Kontrollen gibt es nicht. Wir schlendern die Gates einmal hoch und runter und bewundern ein Gemälde einer salvadorischen Persönlichkeit ([Erzbischof Romero](http://de.wikipedia.org/wiki/Oscar_Romero)). Dann geht es zum Flugzeug, das gleiche Modell.

<figure>
	<img src="{{ site.url }}/images/2014-panama-9-salvador.jpg" alt="Erzbischof Oscat Romero">
	<figcaption>Oscar Romero, ein katholischer Erzbischof, dessen Ermordung der Beginn des Bürgerkriegs war</figcaption>
</figure>

## 05:30 Ankunft

Wir setzen zum Landeanflug an! Endlich. Nach einer 28 Stunden Reise sind wir am Ziel angelangt. Zumindest fast. Es gab wieder das gleiche Essen, nur diesmal sogar mit Beef als Auswahlmöglichkeit. Ich bin satt und bestelle nur ein Vino und Agua. Weil Panama unsere Endstation ist, müssen wir eine Zollerklärung ausfüllen. Nicht nur die USA lassen grüßen, sondern die Fragen sind ziemlich identisch und das Procedere auch. Allerdings geht es in Panama schneller und die Zollbeamtin interessiert nicht wirklich für unseren Zettel.

Am Gepäckband gibt es dann doch die schlechte Nachricht: Mein Koffer schafft es heute nicht mehr. Er wird morgen nachgeliefert. So eine Scheiße. 

Draußen erwarten uns die Gastgeber und ein tropisches Klima. Es geht mit dem Auto nach Panama-Stadt und in unser Hotel. Kurz vor 8 Uhr liegen wir in unseren Betten und sind kaputt. Wenn doch nur das Schnarchen meines Vaters nicht wäre...

<figure>
	<img src="{{ site.url }}/images/2014-panama-10-city.jpg" alt="Panama-Stadt bei Nacht">
	<figcaption>So fühlt man sich nach 4 Flügen und 28 Stunden auf den Beinen (Panama-Stadt)</figcaption>
</figure>