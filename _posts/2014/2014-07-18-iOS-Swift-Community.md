---
layout: post
title: "iOS und Swift Community"
date: 2014-07-18
category: it
tags: [iOS, Coding]
published: true
comments: false
share: true
image: 
  feature: 2014-swift-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Ursprünglich sollte das hier ein Artikel über Swift werden. Aber zur Geschichte von Swift haben andere schon genug Wörter verloren und um über Swift als Programmiersprache zu urteilen, müsste man viel mehr Erfahrung mit Programmiersprachen haben als ich.     

Mir ist auf jeden Fall schon mal nichts negatives aufgefallen. Im Vergleich dazu war ich schon nach 2 Stunden Objective-C von den [eckigen Klammern] genervt. Mich hat Swift neugierig gemacht und ich werde definitiv weiter damit rumspielen (und vielleicht auch zwei, drei App Ideen umsetzen).

Eine Sache hat mich in den letzten Wochen ungemein positiv überrascht, ja geradezu vom Hocker gehauen. Ich wusste ja vorher schon, dass die iOS Community aktiv ist, doch mit dem Ausmaß nach Swift hätte ich nie gerechnet.       
Nicht nur wurde sich ab der ersten Minute mit Swift auseinandergesetzt, sofort sprossen Swift-Tutorials und Videos aus dem fruchtbaren Webboden.    
Wir müssen das mal festhalten: Swift wurde vor nicht einmal 6 Woche vorgestellt und schon jetzt finden sich über 2000 Repositories auf github. Alles wird ausprobiert: wie man einen Server baut, am besten POST Requests macht, mit JSON umgeht, Core Data wrappen kann,...     
Zahlreiche Frameworks und OpenSource Projekte haben schon auf Swift umgestellt. Ob die vor wenigen Tagen vorgestellte Datenbankalternative *Realm* oder die *PaintCode* App. Alles lässt sich mit Swift nutzen.

Eine aktive Community ist ein starker Motor.     
Die Zukunft von Swift ist aufregend und mit dieser tollen Community macht es unglaublich viel Spaß sich damit zu beschäftigen. Wenn ich vorzeigbare Projekte habe, stelle ich sie natürlich auch hier vor.