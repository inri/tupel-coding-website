---
layout: post
title: "Geduld beim Brillenkauf"
date: 2014-10-22
category: blog
tags: [Wirtschaft]
published: true
comments: false
share: true
---

Die nächste Dienstreise nach Südamerika steht an und ich habe immer noch keine Sonnenbrille. Den deutschen Sommer überstehe ich ganz gut auch ohne, aber in Panama habe ich den Schutz für die Augen vermisst. 


Einfach eine kaufen geht nur bedingt, denn ich brauche Brillengläser mit Stärke. Die letzten Wochen habe ich leider nicht an die Sonnenbrille gedacht, kein Wunder bei dem Wetter. Nun bleiben mir noch 6 Tage bis zur Abreise und eine Sonnenbrille mit Stärke zu bekommen.

### 5 bis 14 Tage
Um die Ecke ist ein Laden von Apollo-Optik, wo ich eine schicke *Ray Ban* Brille entdeckte. Wie lange sie für die Brille brauchen? *10 bis 14 Tage* antwortete man mir. Gibt es keine Möglichkeit sie schneller zu erhalten? *Nein, zeitiger können wir das nicht garantieren*. Na toll.     
400m weiter ist ein Fielmann und dort erhalte ich die gleiche Aussage, nur dass es mit *7 bis 10 Tage* etwas schneller geht bzw. gehen könnte. Eine Garantie möchte man mir auch nicht geben. Ich entscheide mich vorerst für Fielmann, habe eine ähnliche Brille wie bei Apollo gesehen. Doch man möchte gerne einen Brillenpass, bevor es zur Auswahl geht.  

Also ab nach Hause und erst einmal im Web nachschauen, was Mister Spex so anbietet. Dort sind es zwar auch *5 bis 13 Tage*, aber die Preise erheblich günstiger.

### Kein Express

Wieso bietet man den Kunden keine Express-Fertigung der Gläser an? Ich wäre durchaus bereit gewesen bis zu 40€ mehr zu bezahlen, wenn ich die Sonnenbrille schon nach 3 Tagen hätte abholen können.     
Für Brillengläser gibt es dutzende Optionen: dünner, kratzfester, entspiegelt,… wieso nicht eine Möglichkeit diesen langen Vorgang der Herstellung zu beschleunigen? 

Ich habe mich natürlich für Mister Spex entschieden. Dort besteht zumindest die größte Chance, dass die Brille rechtzeitig ankommt. Ich verlasse in 6 Tagen Halle, die Reise an sich beginnt aber erst in 9 Tagen, so dass die Chance besteht, die Brille am 7. oder 8. Tag per Post zu erhalten. Noch dazu sind die Preise einfach unschlagbar, aber das wäre für mich nicht der ausschlaggebende Grund für mich gewesen.

So oder so, ich gehe nicht gerne zum Optiker.

## Nachtrag

Am Mittwochnachmittag habe ich die Bestellung bei Mister Spex aufgegeben. Am Donnerstag bekam ich eine Rückfrage bzgl. der Brillenwerte und dann ging sie in Produktion. Heute am Freitag erhielt ich die Bestätigung des Versands. Ob ich noch einmal eine Brille bei Mister Spex bestellen werde, kann sich jeder selbst denken.