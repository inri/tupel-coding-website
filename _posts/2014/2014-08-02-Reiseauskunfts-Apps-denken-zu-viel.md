---
layout: post
title: "Reiseauskunfts-Apps denken zu viel"
date: 2014-08-02
category: it
tags: [App, Design]
published: true
comments: false
share: true
---

Letzte Woche war ich in unserer tollen Hauptstadt unterwegs und nutzte die öffentlichen Verkehrsmittel. Seit mindestes vier Jahren ist mein Kompass im Meer von Abfahrtzeiten die *Bahn* App. Wann fährt welcher Zug oder S-Bahn wohin und bei der Gelegenheit kaufe ich doch gleich mal mein Ticket.     

Mein Weg führte auch etwas in die brandenburgische Provinz hinein und dort wurde mir der Ticketkauf per Bahn App versagt (ein herzlichen Dank an die Verbünde!). Dafür gab es dann die *VBB* App. In Halle nutze ich sowieso die App *easy Go* und hantierte somit an einem Tag mit drei Reiseauskunfts-Apps herum. 

## Intelligente Berechnung

In 95% der Fälle nutze ich die App zur Suche der Verbindung von A nach B und meist bin ich im Moment der Suche noch  auf dem Weg zu Punkt A (Haustür > Haltestelle). Jede dieser Apps möchte super intelligent sein und zeigt mir nicht an, wann bspw. der Zug abfährt, sondern wann ich meine aktuelle Position in Richtung Punkt A verlassen muss um diesen und jenen Zug zu bekommen. 

Ich weiß nicht wie es euch geht, aber mich stört diese Funktion, dieses Design der Apps. Warum? Weil die Berechnungen fast nie korrekt sind.     
Auf dem Weg zu mir bekannten Bahnhöfen/Haltestellen kenne ich den Weg gut, so dass ich schneller bin und selbst am besten einschätzen kann, wie lange ich brauche. Bei fremden Haltestellen ist es umgekehrt und je nach Verkehrslage kann man richtig angeschissen sein.

<figure>
	<img src="/images/2014-ios-easyGo.jpg" alt="image">
	<figcaption>5 Minuten für 200m?</figcaption>
</figure>

Doch es geht ja nicht nur um die bloße Zeitberechnung, sondern die Folgen daraus. Der Algorithmus entscheidet unter Umständen, mir gewisse Verbindungen gar nicht erst anzuzeigen. Beispiel: Wir liefen von Ikea los und die App zeigte die nächste Abfahrt der S-Bahn in 25 Minuten an. Großzügig aber dumm. Denn wir brauchten zu Fuß keine 15, sondern nur 10 Minuten und schafften somit die frühere Abfahrt.     
Es ist ja auch nicht so, dass die Apps klüger werden und sich merken, wie flink ich war. 

Ich bin jedenfalls regelmäßig verstört, wenn ich durch einen Bahnhof hetze und am Gleis noch tote Hose ist. Man sieht ja erst beim Öffnen der Detailansicht die richtigen Zeiten und kann dann aufatmen.     

Wie gerne würde ich diese Funktion deaktivieren.