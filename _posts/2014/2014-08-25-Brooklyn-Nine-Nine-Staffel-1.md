---
layout: post
title: "Brooklyn Nine-Nine, Staffel 1"
date: 2014-08-25
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2014-brooklyn-nine-nine-lg.jpg
  credit: Fox
  creditlink: http://fox.com
---

Wer *Scrubs* mochte, sollte sich *Brooklyn Nine-Nine* mal genauer anschauen. 

Statt eines Krankenhauses haben wir eine Polizeiwache in Brooklyn, statt eines begabten Arztes (J.D.) einen begabten Detective und Dr. Cox als Mentor wird durch einen Captain verkörpert. Daneben gibt es natürlich noch zahlreiche andere Charaktere, wie den Tollpatschigen, die Streberin, die Gefühlskalte und den Familienvater.   
 
Das soll sich aber keinesfalls langweilig anhören, denn die Serie macht sehr viel Spaß! Sie erregte meine Aufmerksamkeit mit der Golden Globe Auszeichnung als beste Comedy-Serie. Neugierig schaute ich mir die Pilot-Episode an und war ziemlich begeistert.

In Zentrum stehen die Detectives der Wache und ihr Alltag mit der Verbrechensbekämpfung. Einen ganz guten Eindruck bekommt man von [diesem Trailer.](https://www.youtube.com/watch?v=X3oaLWaoz-k) Der Humor ist albern, intelligent und herzlich. Eben genau wie bei Scrubs.

Die erste Staffel, bestehend aus 22 Episoden, lief bis März und die zweite beginnt Ende September. Euch bleibt also genug Zeit die alten Epsioden nachzuholen und auf die neuen zu freuen. 
     
**Schaut euch die Serie an!**