---
layout: post
title: "Meinung zu Slingshot"
date: 2014-07-14
category: blog
tags: [Facebook, App]
published: true
comments: false
share: true
---

Facebook hat nicht immer ein gutes Händchen für die *so called* Innovationen und tat sich in der Vergangenheit eher mit überteuerten Käufen hervor. Anfang des Jahres brachte Facebook mit *Paper* eine erste App mit eigener Idee und schicker Umsetzung heraus. *Slingshot* ist nun die zweite.

[Marcel kritisierte](http://uarrr.org/post/89843320471/facebooks-slingshot-is-basically-broken) die neue Foto- und Video-Teilen App von Facebook namens *Slingshot* und bezeichnet sie sogar als *&quot;basically broken&quot;*. Was ist dran?     

## Die Idee: Gegenseitigkeit

Slingshot bietet eine Lösung für ein durchaus nicht zu unterschätzendes Problem mit der Natur der sozialen Netze. Sie werden von zwei verschiedenen Sorten von Menschen bevölkert: aktive und passive Nutzer. Die ersteren schreiben, teilen und posten Bilder und Videos. Die letzteren konsumieren, lesen still mit und bleiben stumm. 

In Slingshot teilt man Fotos und Videos, aber muss sich an eine wichtige Regel halten: Du siehst Mitteilungen erst, wenn du dem Absender auch eine Mitteilung schickst.   
Mitteilungen müssen quasi freigeschaltet werden und das geht nur, wenn man selbst etwas teilt. Die introvertierten Nutzer werden also hinter dem Ofen hervorgelockt und das ist erst einmal eine interessante Idee.

## &quot;Basically broken&quot;?

Zurück zu Marcels Kritik. Er beschreibt den Verlauf der Mitteilungen als verwirrend und hat nicht ganz unrecht. Sein Beispielablauf sieht wie folgt aus:

> Conventional messaging happens like this:
>
> Person 1: Update.   
Person 2: Answer to update.    
Person 1: Answer to answer to update.
> 
> Slingshot “conversations” look like this:
>
> Person 1: Update A      
Person 2: Update B (Unlocks Update A)     
Person 1: Update C (Unlocks B)     
Person 2: Update D (Dies confused)

Er sieht die Schwierigkeit darin, eine Konversation aufzubauen, weil man scheinbar für jede Mitteilung eine Mitteilung senden muss und somit natürlich das Gespräch unterbrochen wird und niemand mehr weiß, welche Antwort sich auf welche Antwort bezieht.    

Aber es gibt zwei Ausnahmen. Direkte Antworten auf eine Mitteilung, also wenn man auf die Mitteilung klickt und den halben Bildschirm sieht, sind sofort sichtbar. (Wenn man die Mitteilung wegwischt, wird sie gelöscht (siehe unten) und man kann nicht mehr darauf reagieren.)      
Außerdem schaltet man mit **einer** Mitteilung **nicht nur eine** Mitteilung frei, sondern alle Mitteilungen einer Person, die auf mich warten. Das bedeutet zwar, ich werde gezwungen etwas zu teilen, kann dann aber sofort eine zweite und dritte oder noch mehr Mitteilungen hinterherschicken. Wenn der Angeschriebene wiederum etwas mit mir teilt, bekommt er **alle** meine Antworten zu sehen.  

> Person 1: Update A      
Person 2: Update B (Unlocks Update A)     
Person 2: Update C (Answer to Update A)   
Person 2: Update D (New Update)     
Person 1: Sees Update C      
Person 1: Update E (Unlocks B and D)     
Person 2: Update F (Unlocks Update E and possibly more!)

So kann ich mir schon viel eher eine Art Konversation vorstellen. Trotzdem denke ich, dass es weniger um Konversationen bei Slingshot geht, besonders weil man nur einmal direkt auf eine Mitteilung antworten kann.

## Meinung

Ich hatte es ja schon geschrieben, mir gefällt der Lösungsansatz ganz gut. Mich wundert es ehrlich gesagt, das Marcel da anderer Meinung ist, denn auf seiner Seite nutzt er über tumblr ein ganz ähnliches System. Dort können andere tumblr-Nutzer seine Beiträge liken oder reposten, er bekommt also auch etwas zurück.    
Hier auf diesen Blog habe ich aktuell noch keine solche Möglichkeit eingerichtet. Kommentare möchte ich nicht betreuen und dann bleibt nur das Kontaktformular oder die Umwege über Email und Twitter, die bisher kaum jemand genutzt hat. 

Mir ist Feedback nicht ganz unwichtig. Als ich damals als Austauschschüler in Norwegen war und einen Blog schrieb um meine Familie und Freunde auf dem laufenden zu halten, sank meine Schreibrate nach den ersten drei Monaten erheblich. Und das war auf so gut wie allen Blogs von Austauschschülern zu bemerken. Zu Beginn wurde fleißig unter den Beiträgen kommentiert, aber das hörte schnell auf. Entweder sank das Interesse der Leserschaft einfach so, oder man schrieb nicht besonders gut und deshalb sank das Interesse.

Blogs sind vielleicht nicht der beste Vergleich, denn da steckt man sowieso mehr Arbeit hinein. Das Gefühl ist in einem Foto-Tauschen Netzwerk aber das gleiche. Wenn ich keine Reaktionen bekomme, macht es irgendwann auch weniger Spaß. Und ein Like oder kurzer Kommentar ist mal ganz nett, ändert aber nichts daran, dass ich der alleinige Sender bin.    

Oder dass die Position des passiven Stalkers ganz bequem ist, wenn die anderen ständig von sich hören lassen. Auch diese Situation kenne ich gut. Bei Instagram poste ich sehr selten etwas, obwohl ich zu den meisten meiner Bildern Likes und Kommentare bekomme. Wenn ich nicht unbedingt Sender sein will oder nach Likes giere, fehlt einfach die Motivation.

Slingshot probiere ich nun mit meiner Freundin aus und sie findet es witzig. Dabei fokussieren wir uns weniger auf zusammenhängende Konversationen, sondern viel mehr auf schöne kurze Momente, die wir dem anderen mitteilen wollen.     
Warum man Bilder von einer Beerdigung *slingshotten* sollte, oder von sich selbst, wie man auf dem Klo sitzt, weiß ich nicht. 

## Snapchat Klon und Selbstzerstörung

Bis hier hin mag ich die Idee. Doch man hörte kurz nach der Vorstellung von *Slingshot* die Kritik, dass Facebook die Idee von *Snapchat* geklaut hat. Das Original funktioniert ganz ähnlich, nur dass im Fokus nicht das Freischalten von Mitteilungen steht, sondern der Nervenkitzel, dass eine Mitteilung sich nach maximal 10 Sekunden selbst zerstört [^1].     
Facebook dachte nun, dass es eine pfiffige Idee sei, diese Funktion zu kopieren, also die Selbstzerstörung. Ich weiß nur nicht wieso.

Man kann zwar die Bilder automatisch auf dem iPhone speichern lassen, aber in der App selbst sind sie nicht mehr erreichbar. Es entsteht also kein Stream von Bildern und Videos, ich kann nur neue Bilder schicken oder auf ein erhaltenes reagieren. Das ist meiner Meinung nach die größte Schwäche der App und völlig zurecht wird Facebook das Kopieren vorgeworfen. 

Angeblich gibt es hinter den Kulissen auch noch einen Rechtsstreit und um Slingshot ist es leider auch wieder ruhiger geworden. Ob sich da noch etwas entwickelt?

[^1]: Mit *Snapchat Stories* halten sich mehrere Mitteilungen wohl bis zu 24 Stunden. 


