---
layout: post
title: "Grosses iPhone?"
date: 2014-06-25
category: it
tags: [iPhone, Apple]
published: true
comments: false
share: true
---

Als ich mich meinem Wohnaufgang näherte, kam eine junge Frau aus der Tür heraus. In der linken Hand hielt sie die Leine zu ihrem kleinen Hund und ihre rechte umklammerte krampfhaft ein riesiges Smartphone. Ich hielt die Luft an und sah das Gerät schon vor meinem geistigen Auge die Treppe runterpurzeln. Doch sie schien Übung darin zu haben.

Das war nicht die erste Beobachtung dieser Art. Besonders bei Frauen sieht es nie elegant aus, wenn sie ein Smartphone jenseits der 4 Zoll Größe bedienen. Noch einmal: **Es sieht nicht gut aus.** Und vor allem braucht mir keiner erzählen, dass diese Menschen eigentlich ganz gut mit ihrem Geräten klarkommen [^1]. Das ist eine Lüge.   

Ich beobachte aber nicht nur fremde Menschen mit ihrem Telefonen, ich besitze ja selbst einige. Zum Entwickeln nutze ich zur Zeit ein Galaxy S2, das ist genauso hoch wie das iPhone 5s, aber knapp 1cm breiter. Dieser eine Zentimeter reicht aber schon aus um mit der Hand komische Verrenkungen zu machen, wenn man bspw. mit dem Daumen die linke obere Ecke erreichen möchte. Für meinen Geschmack ist das 5s die Grenze und meine Freundin empfindet das iPhone 4S schon als etwas zu groß. Sie hat sehr zierliche Hände. 
  
Auch sei darauf hingewiesen: Es gibt keinen iPhone-Rivalen mit großem Bildschirm, der auch nur annähernd so viele Geräte absetzen kann wie Apple mit dem iPhone. Vermutlich nicht einmal die größten drei zusammen. Deshalb ist und bleibt die Größe von 4 Zoll auch der beste Kompromiss für die meisten Menschen.    
Das bedeutet aber nicht, dass für andere ein größeres Smartphone nicht durchaus attraktiv erscheint. Ich kann mir vorstellen, dass sich dieser Kundenwunsch auch erst in den letzten Jahren entwickelt hat.    
Warum die Flaggschiffe der anderen Hersteller stets > 4,7 Zoll sind und sie sich damit um einen beträchtlichen Teil potentieller Käufer bringen [^2], ist mir schleierhaft.

Die [iPhone-Gerüchteküche brodelt](http://www.macrumors.com/2014/06/24/apple-rejects-catchers-iphone6-casings/) und gemunkelt wird über ein herbstliches 4,7 Zoll und spätherbstliches 5,5 Zoll iPhone 6. [John Gruber](http://daringfireball.net/linked/2014/06/24/bloomberg-iphone-6) stellt die richtige Frage: Was ist mit dem 4 Zoll iPhone? Ich glaube kaum das Apple ihm kein Update spendiert, schließlich ist es das Zugpferd.    
Oder anders gesagt: Wenn das iPhone Update dieses Jahr aus der Innovation [^3] der Vergrößerung des Geräts zusammen mit einigen Designänderungen (in Richtung iPad Air) besteht, wäre das ziemlich enttäuschend.

Oder Apple stellt ein iPhone 5s+ vor, zwei neue iPhone 6 mit größeren Bildschirm und identischer Technik und das alles wird von der iWatch in den Schatten gestellt. Wer ein größeres iPhone will, bekommt es, wer ein technisch aktuelleres 4 Zoll iPhone will, bekommt es auch und für den Rest gibt es iWatches!

[^1]: Das ist für App Entwickler besonders tragisch. Man gibt sich Mühe für ein gutes Design und dann werden deine Apps auf solchen Geräten benutzt. Wie soll man eine gute Usability mit der App erreichen, wenn das Trägergerät nicht usable ist?

[^2]: Come on, entweder entscheiden sich kleinhändige Menschen gar nicht erst für ein solches Gerät, oder es ist ihr letztes. Ein Loose-Loose-Situation. 

[^3]: Falls tatsächlich diese größeren iPhone vorgestellt werden, wird der Narrativ der Medien höchstinteressant sein. Nicht. Apple entgeht gerade so dem Untergang weil es sich endlich die Monstergeräte der Konkurrenz abkupfert.