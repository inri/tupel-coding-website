---
layout: post
title: "Kritik an Apple"
date: 2014-10-29
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Ich habe vor einigen Wochen einen Artikel geschrieben, ihn jedoch noch nicht veröffentlicht. Thematisch ging es um die Frage, ob die neuen iPhones wirklich die besten iPhones für die Mehrheit der Kunden sind. Ich denke nämlich nicht und würde sie eher als faulen Kompromiss bezeichnen. 

Ich verstehe durchaus, warum Apple auch Liebhaber größerer Bildschirme bedienen möchte. Dass es kein Update für das ein Jahr alte iPhone 5s gab, ist dagegen sehr enttäuschend. Beobachter haben allerdings angemerkt, dass das iPhone 5s immer noch ein sehr gutes Smartphone ist, auch in den nächsten 12 Monaten. Für dieses Jahr war ein Update also kein *Muss* und so konnte Apple den Fokus auf die neuen iPhones legen.    

Weil ich optimistisch davon ausgehe, dass nächstes Jahr ein iPhone 7s vorgestellt wird, stelle ich meinen Artikel vorerst hinten an. Ich kenne allerdings mehr als zwei Personen, die sich gerne ein neues iPhone gekauft hätten, aber das Lineup nicht berauschend finden. Doch neben den neuen iPhones, gibt es auch anderes zu kritisieren.

## What matters

Kritik sollte sich auf Wichtiges beziehen. Kritik, die das nicht macht, kann man in die Kategorien: *Falsche Tatsachen* und *Falsche Blickwinkel* unterteilen. Mehr oder weniger. Erstere findet man meist in der allgemeinen, medialen Berichterstattung und letztere eher bei Bloggern.     

### #Bendgate
Ein schönes Beispiel für falsche Tatsachen war der Aufreger um angeblich verbiegbare iPhones. Das ging sogar so weit, dass Apple der Computerbild völlig zurecht die *Freundschaft* gekündigt hat.[^1] Fakt ist: Die neuen iPhones sind genauso stabil wie alle anderen Smartphones dieser Größe. Ja, die 4er und 5er iPhones waren etwas stabiler, aber der Unterschied ist keinen Skandal und keine Meldung wert. Es war eine falsche Tatsachenbehauptung [^2].

### OS X Yosemite, Design, Android
Es gibt einige Blogger und Journalisten, die sich sehr ausführlich mit den neuen Systemen beschäftigen und tolle Reviews schreiben. Manchmal driften ihre subjektiven Meinungen aber zu sehr ins nerdige ab. Zum Beispiel wurden die transparenten, Milchglas-ähnlichen Flächen in einigen Teilen von Yosemite kritisiert. *Wieso gibt es sie, wenn sie keine offensichtliche Funktion haben und in seltenen Fällen etwas störend sein können?* Weil sie dem System etwas mehr Persönlichkeit einhauchen, antworte ich.     

Oft werden auch Trivialitäten wie Icons hart diskutiert, siehe iOS 7 letztes Jahr. Ich, als Leser ohne professionellen Hintergrund in Design, kann nicht immer urteilen, ob in der Tat ein Versäumnis von Apple vorliegt, oder es nur eine Frage von persönlichem Geschmack ist. Und manchmal schäme ich mich auch ein wenig, wenn mir ein Designdetail gefällt und es gleichzeitig von *Designern* kritisiert wird.

Als letztes Beispiel in dieser Reihe sind stellvertretend die Android-Verfechter zu nennen, die auf die angeblichen Mängel und fehlenden Möglichkeiten von Apples Systemen hinweisen. Dabei aber nicht merken, dass die tollen Freiheiten für die meisten Benutzer keinen Mehrwert darstellen, sondern gerade das Gegenteil bewirken. Sie bewerten nur aus ihrer eigenen Perspektive heraus.

Am Ende sollte man sich als Kritiker öfter die Frage stellen, ob der diskutierte Aspekt wichtig ist und die Benutzbarkeit für die Mehrheit der Kunden einschränkt, oder nur mein persönliches Empfinden oder meine Benutzbarkeit einschränkt, wenn überhaupt.

## Kritik ist doch nicht schwer

Dabei ist es zur Zeit gar nicht so schwierig, Kritik an Apple zu äußern. 

### 16 GB Speicher
Wieso es eine Firma, die im letzten Quartal einen irren Gewinn von 31 Milliarden Dollar erwirtschaftet hat, nicht schafft, allen Grundkonfigurationen von iPhone und iPad 32 GB Speicher zu geben, wird mir wohl ein Rätsel bleiben. Es geht weiterhin bei 16 GB los. Mag sein, dass 32 GB für Bildungseinrichtungen zu viel Speicher ist, aber für normale Nutzer sind 16 GB einfach zu wenig. Hier spart Apple an der falschen Stelle und das haben sie nicht nötig.     
Gruber und Co kritisieren diesen Umstand auch und sagen klipp und klar, dass mit den 16 GB Varianten im Jahr 2014 bei einem 7 GB großem Update für iOS 8 erstmals keine *&quot;guten&quot;* sondern nur *&quot;bist du dir sicher?&quot;* Einsteigermodelle im Lineup platziert wurden. Bei einem Gerät für 600€, 700€ oder 800€ möchte ich mir verdammt noch mal keine Gedanken über den Speicherplatz machen müssen.[^3] 

### iPad mini 3 
Apple Produkte werden normalerweise entweder billiger, erhalten mehr Leistung für den gleichen Preis oder erhalten mehr Leistung für einen geringen Aufpreis. Beim iPad mini 3 kann man kaum von mehr Leistung reden, schließlich wurde nur TouchID neu verbaut und vielleicht eine etwas bessere Kamera [^4]. Doch dafür verlangt Apple stolze 100€ mehr als für das mini 2. Natürlich für die lächerliche 16 GB Variante. Bisher habe ich auch noch keine Kaufempfehlung gelesen.

### iOS 8 for iPad
Viel Kritik gab es für das Update auf iOS 8 für iPads. Der Grund ist simpel: Es war noch nicht bereit. Apple möchte mit neuer Hardware auch neue Software ausliefern, aber die iPhones sind den iPads voraus. Das merkte man deutlich an einem verbuggten System.      
Dazu kommt die Frage, wieso das erste iPad mini im Lineup verbleibt, obwohl es einen deutlich schwächeren Chip hat als alle anderen i-Geräte. Die Entwickler finden das nicht besonders toll, weil es keine Möglichkeit gibt Apps einzuschränken. Das Spiel *xyz* kann man nicht nur für das iPad mini 2 und höher anbieten, was dazu führt, das iPad mini 1 Besitzer erbost über Abstürze und Performance sind. Wenn die Hardware der Software nicht mehr Schritt halten kann, raus damit.

### Verschnaufpause für iOS und OS X  
Im gleichen Atemzug fordern Entwickler, dass Apple mal einen Gang runter schaltet und nächstes Jahr keine großen iOS und OS X Updates mit hunderten Features herausbringt, sondern die bestehenden Systeme flickt. Ob Bugs am System, die erst mit dem x.1 Update behoben werden oder immer noch unfertigen Apps, Baustellen gibt es genug.      
Doch ob die Marketingabteilung eine Pause zulässt? Die Medien werden ihre *&quot;Wo bleiben die Innovationen?!&quot;* - Mistgabeln bereithalten.

### App Store ohne Weiterentwicklung
So toll der App Store für Entwickler und Kunden sein mag, so sehr stagniert seine Entwicklung seit einigen Jahren. Zwei häufig gewünschte Funktionen sind zum einen Testversionen von Apps und zum anderen bezahlte Updates.    
Testversionen kann man entweder auf der eigenen Entwickler-Website anbieten, wo sie nicht so prominent zu Finden sind wie im App Store. Oder als Light-Version im App Store, wobei man in der App und der App Beschreibung keine Hinweise auf die Vollversion geben darf.      
Bezahlte Updates [^5] sind längst überfällig. Aktuell muss der Entwickler eine komplett neue App in den Store stellen oder das Update kostenlos anbieten. Beides keine optimalen Lösungen, dabei wäre eine Lösung so simpel.   
Apple ist auf Entwickler angewiesen, denn die Plattform ist nur so gut wie ihre Auswahl an kreativen und hochwertigen Apps. Neue APIs sind auch schön, aber am Ende des Tages muss man die Software verkaufen und dabei kann der App Store hilfreicher sein.

## Zukünftige Entwicklung

Das sind ziemlich gravierende Kritikpunkte und bei weitem nicht alle. Skeptiker sehen darin ein Beweis für den Qualitätsverlust von Apples Produkten. Wir kritisieren jedoch auf einem hohen Niveau und so oft ich in letzter Zeit auch den Kopf schütteln musste, meine Apple-Geräte benutze und update ich ganz gern. Würde ich einen neuen Retina iMac, das neue iPad Air 2 oder die kommende Apple Watch gern mein Eigen nennen wollen? Aber sowas von!     
Ich kann auch keinen Konkurrenten ausmachen, der Apple unter Druck setzen könnte und mir gute Alternativen anbietet. Vielleicht liegt genau darin das Problem? Apple ist so erfolgreich wie nie. Die neuen iPhones wurden ihnen förmlich aus den Händen gerissen und die Produktion kommt nicht hinterher. Bei aller Kritik, für viele sind die benannten (noch) nicht ausschlaggebend.   

Auf jeden Fall konnte ich vor zwei Jahre noch wesentlich leichter Apples Standpunkte in verschiedenen Diskussionen vertreten. Oder ich rutsche mehr und mehr in ein Nerd-Loch und betrachte neue Entwicklungen skeptisch und potentiell feindlich.    
Warnt mich bitte, wenn es so weit ist.


[^1]: Die Computerbild war etwas spät dran, wie meistens. Als die Aufregung in den USA schon wieder beruhigt war, weil u.a. Apple ein Video über den Testparcour der iPhones zeigte und auch unabhängige Hardware-Experten dem iPhone keine besondere Biegsamkeit attestierten, goss die Computerbild mit einem eigenen Skandal-Video wieder Öl in deutsches Feuer. Sie versuchten sich in einem offenen Brief an Tim Cook mit Journalismus herauszureden, aber es ging nur um Klicks und Aufmerksamkeit. Ergebnis: Sie bekommen von Apple zukünftig keine Testgeräte und Einladungen zu Events mehr. Ätsch.

[^2]: Dass diese Behauptungen durchaus vom Mainstream registriert werden, erlebte ich, als ich bei einer Familienfeier von meinem Cousin (21) darauf angesprochen wurde. *Verbiegen sich die neuen iPhones wirklich so leicht?* Kann man es einem Unternehmen übel nehmen, wenn es mit den Verbreitern solcher Behauptungen nicht arbeiten möchte? Journalismus hin oder her.

[^3]: Bei anderen Firmen nennt sich das Abzocke nennen. Wenn ich Samsung wäre, würde ich für meine zukünftige Werbung genau diesen Umstand anvisieren, doch dummerweise beginnen ihre Galaxy S5 auch bei 16 GB (sind allerdings auch 200€ billiger als das iPhone 6).

[^4]: Die Berichte sind diesbzgl. noch nicht eindeutig.

[^5]: Paradoxerweise suggerierte der Update-Mechanismus bis iOS 6, glaube ich, aber genau das Gegenteil. Man musste für jedes Update sein iTunes Passwort eingeben, eben wie bei einem Kauf. Erst nach ein paar Monaten wurde mir bewusst, dass es scheinbar keine bezahlten Updates gibt, niemals wollte iTunes für ein Update Geld von mir.