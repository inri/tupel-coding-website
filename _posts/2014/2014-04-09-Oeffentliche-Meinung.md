---
layout: post
title: "Meinung in der Öffentlichkeit"
date: 2014-04-09
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
---

Die Reaktionen rund um den Skandal des neuen CEO von Mozilla bestätigen mich in meinem Entschluss, meiner persönliche Meinung so wenig wie möglich Ausdruck zu verleihen.

   
Der besagte, Brendan Eich, hat vor über 5 Jahre Befürworter eines Gesetzentwurfs gegen gleichgeschlechtliche Ehen mit 1000$ unterstützt ([heise](http://www.heise.de/newsticker/meldung/Nach-Kritik-an-Homo-Ehe-Mozilla-CEO-tritt-zurueck-2162771.html)). Als er vor drei Wochen zum neuen CEO von Mozilla benannt wurde, stand er in der Kritik. Bei Mozilla selber sollen sich wohl weniger als 10 Mitarbeiter über ihn beschwert haben, aber in der Öffentlichkeit wurde er stark angegriffen und trat nach nur zwei Wochen von seinem Posten zurück.

fefe hat [alles wichtige dazu](http://blog.fefe.de/?ts=adc06a6b) schon gesagt:

> Dass der Geld an Prop8 gegeben hat, IST SEIN RECHT. 
> Das ist Demokratie. So funktioniert das System in den USA.

Ich würde es vielleicht noch ein wenig einschränken. Eich war der neue Chef von Mozilla und ich sehe beim besten Willen nicht was ein Browser mit gleichgeschlechtlichen Ehen oder Homophobie zu tun hat. Wenn er zum neuen Familienminister benannt worden wäre, könnte ich die Aufregung verstehen.     
In meinen Augen sollte man vorsichtig mit Parallelen zwischen einer Meinung/Haltung und einer beruflichen Qualifikation umgehen, besonders wenn beide verschiedene Themengebiete betreffen.  

## Meine Meinung

Ich puste sehr wohl meine Meinung ins Internet und gänzlich kann und möchte ich auch gar nicht darauf verzichten. Aber vor einigen Jahren habe ich auf meinem alten Blog noch über politische Themen geschrieben und zumindest diese Art von Meinung ist mittlerweile auf Twitter reduziert. Ganz logisch kann ich meine Twitter-Ausnahme nicht begründen. Ich glaube einfach, dass tausende Tweets weniger überschaubar sind als ein ordentlich gepflegter Blog mit Tags. Ein Tweet ist zwar auch eine Meinung, aber insgesamt sind 140 Zeichen eher dürftig und oft fehlt rückblickend betrachtet auch die Referenz. 

Hier auf diesem Blog wird man kaum noch politische Diskussionen finden. Es ist mir einfach zu heikel und Politik nicht mein Fachgebiet. Über Technik und die mobile Welt schreibe ich meine Gedanken unverblümt auf, denn damit kenne ich mich aus und kann jederzeit mit jedem darüber diskutieren. 

## Was andere denken

Vielleicht ist meine Zurückhaltung bei politischen Themen feige oder das Gegenteil eher dumm. Doch man weiß leider nie auf welche Menschen man im Leben noch trifft. Ein Beispiel: Ein Kommilitone hatte in unserem Studienabschlussbuch aus Spaß bei der Frage *Was mache ich nach dem Studium?* mit *Das kapitalistische System stürzen!* geantwortet. Das Buch liest doch sowieso keiner, denkt man. Er bekam einige Wochen später eine Email von Hasso Plattner persönlich, der ihn darauf ansprach und hinwies, dass eben das kapitalistische System ihm erst dieses Studium ermöglicht hat. 

Nun kann man diese Situation etwas uminterpretieren und du stehst plötzlich vor einem Personalleiter, der dich wenig amüsiert auf einen alten Tweet anspricht... ach verdammt! 

Ich lese meine Tweets mal durch und dann überdenke ich meine Haltung bzgl. Twitter auch noch. Aktuell habe ich einfach das Gefühl, dass man in unser eigentlich meinungsfreien Welt nicht zu viel der eigenen Meinung preisgeben sollte. 