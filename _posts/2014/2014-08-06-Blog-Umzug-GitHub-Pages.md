---
layout: post
title: "Blog-Umzug zu GitHub Pages"
date: 2014-08-06
tags: [Web, Tupel Coding]
category: blog
published: true
comments: false
share: true
---

Auf diesen Blog hat sich intern dies und das getan, aber die Chancen stehen gut, dass ihr davon gar nichts mitbekommen habt. Tupel Coding ist nun auf GitHub gehostet und sogar eine [GitHub Page](https://pages.github.com). Aber beginnen wir erst einmal beim Anfang.

## Jekyll und Uberspace

[Uberspace](https://uberspace.de) und das Team dahinter sind klasse. Wenn ihr eine Website hosten wollt und vielleicht noch etwas mehr, nutzt Uberspace! Ich habe meine Website bisher auf Uberspace gehostet und war und bin sehr zufrieden.    
Dieser Blog hier ist ein [Jekyll-Blog](http://jekyllrb.com). Das bedeutet kurz gesagt, dass ich meine Artikel als Markdown schreibe und Jekyll (Ruby Framework) mir daraus die Website generiert. Heraus kommt eine statische Website, deren Aussehen ich natürlich nach Belieben anpassen kann.

Das coole und nerdige ist die Versionierung des Blogs mit Git. Ein neuer Artikel oder einzelne Änderungen sind einfach Commits. Diese pushe ich zu Uberspace, wo ein Git Hook dafür sorgt, dass Jekyll eine Website generiert und diese in meinen html Order auf Uberspace kopiert, damit ihr sie besuchen könnt. Das funktioniert alles wunderbar.     
Damit man übrigens beim Schreiben eines Artikels eine Vorschau erhält, kann Jekyll auch als lokaler Testserver fungieren, so dass man unter seinem Localhost die aktuelle Website begutachten kann.

## Problem: #url: ingorichter.org

Ein kleines Problem sind interne Verlinkungen mit Jekyll. Woher soll Jekyll bei der Generierung wissen, auf welcher Website bzw. Domain die Seite mal landet? Damit interne Links trotzdem korrekt sind, muss man in der _config-Datei eine URL setzen. Wenn ich meine Seite aber lokal testen will, setze ich besser keine URL, sonst kann ich zwar mein lokale Startseite besuchen, aber alle Links führen dann zur URL ins &quot;richtige&quot; Web. 

Das führt zu dem Effekt, dass man zwei verschiedene _config-Dateien hat, eine lokale mit dem auskommentierten *#url:* und eine auf Uberspace mit *url: ingorichter.org*. Das ist zwar nicht tragisch, aber auch nicht schön. 

## GitHub Pages

Von GitHub Pages hatte ich schon gehört, mich aber bis dato nicht damit beschäftigt [^1]. Sie machen im Prinzip nichts anderes als ich auch schon mit Uberspace. Vielleicht etwas komfortabler. Aber sie lösen auf jeden Fall das Problem. Man muss **keine** URL setzen und trotzen werden die internen Links korrekt gesetzt.

Noch dazu kommt die Möglichkeit, neben der GitHub Page Domain (bei mir bspw. [inri.github.io](http://inri.github.io) [^2]) über einen A Record eine Custom Domain zu vergeben. Und genau das habe ich getan bzw. das Team von Uberspace hat das für mich getan. Hatte ich schon erwähnt, dass die klasse sind?

Mir war es auch wichtig, dass ich weiterhin Subdomains nutzen kann, die dann natürlich wieder auf Uberspace liegen, und das geht auch problemlos. 

## Was hat sich jetzt geändert?

Für den Besucher nichts. Die Domain ist gleich geblieben und alles funktioniert wie vorher. Wer neugierig ist, kann sich gerne den Code von Tupel Coding auf GitHub anschauen, siehe Footer.     
Für mich ist das Schreiben etwas komfortabler geworden und ich belaste Uberspace auch etwas weniger. 

[^1]: Der Auslöser war nicht nur pure Neugier. Irgendwie hatte ich es geschafft mein lokales Jekyll zu zerschiessen und war mir nicht sicher, ob mir das auf Uberspace nicht auch passiert. Eine robuste Lösung musste her und GitHub Pages ist direkt auf Jekyll zugeschnitten.

[^2]: Aufgrund des Setzen der Custom Domain wird der Browser vermutlicher als Adresse ingorichter.org anzeigen.
