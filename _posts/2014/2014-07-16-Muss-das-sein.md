---
layout: post
title: "Muss das sein?"
date: 2014-07-16
category: blog
tags: [Persönlich, Medien]
published: true
comments: false
share: true
image: 
  feature: 2014-gaucho-lg.jpg
  credit: ARD
  creditlink: http://ard.de
---

Mein Vater hat mir eine wichtige Maxime beigebracht. Sie ist auf so gut wie alle Lebenslagen anwendbar und ein ganz guter Kompass für das eigene Verhalten. 


Als ich noch einige Jahre jünger war, erschloss sich mir nicht immer, wieso es oft klug ist, sich an sie zu halten. Dabei ist sie natürlich auch eine Frage der Interpretation und muss auch nicht ganz genau genommen werden. 

Die Kurzform ist die Frage *&quot;Muss das sein?&quot;*. Wenn du irgendetwas tun möchtest, aber eventuell zweifelst **ob** du es tun solltest, dann stell dir selbst diese Frage oder eine Abwandlung davon: Wäre das sinnvoll? Ist es klug?    
Besonders wenn du mit anderen Menschen interagierst und auf etwas reagierst, kann diese Fragestellung helfen.

## Gaucho-Tanz [^1]

Ich musste gestern und heute wieder daran denken, als ich zum einen einige Spieler unserer Fussballnationalmannschaft diesen Siegestanz gehen sah und zum anderen die unterschiedlichen Reaktionen darauf. Einige Medien kritisierten den Tanz und fürchten um das *Ansehen unserer Nation*, andere Kommentatoren verstehen die Aufregung nicht und wollen dass man *unsere Jungs* einfach feiern lässt.     
Zurecht wird auf das vorbildliche Verhalten des DFB-Teams in Brasilien verwiesen. Sei es der Respekt auf dem Fussballplatz oder die Schenkungen an die Bewohner beim Trainingslager. 

Doch hier folgt ein **doch**. Legen wir probehalber mal die Maxime an das Verhalten der Spieler an. Ihr habt die letzten Wochen dieses Turnier gespielt, euch vorbildlich verhalten und dafür viel Lob bekommen. Noch dazu ist eure Mannschaft als Sieger hervorgegangen und nun wollen euch viele tausend Menschen in der Hauptstadt begrüßen und feiern. Ein Programmpunkt ist das gruppenweise Erscheinen auf der Bühne. Ihr wisst zwar nicht wieso, aber ihr sollt euch etwas tolles ausdenken und nicht nur einfach auf die Bühne laufen.    

Man könnte Bälle ins Publikum werfen, einen Stunt mit dem Pokal machen oder einfach stumpfe Fan-Gesänge anstimmen, aber einer von euch bringt die Idee für den Gaucho-Tanz.    
Muss das sein?     
Muss man vor tausenden Menschen und dutzenden Kameras einen Siegestanz vorführen, der die Verlierer verhöhnt?     
Möchte man auf diese Weise als Vorbild agieren und seiner Freude Ausdruck verleihen? Gefeiert wurde schliesslich der Titelgewinn und nicht allein das letzte Spiel gegen die Argentinier. 

## Nein, muss nicht sein

Nein, das hätte nicht sein müssen. Ganz einfach. Besonders skurril sind die Verteidigungen für diese dumme Idee:    

* Die bösen Argentinier hätten ja auch die Brasilianer verhöhnt und so schlimm war der Tanz nicht. Mag sein, aber diese Logik öffnet ganz neue Möglichkeiten. So lange die anderen etwas noch dümmeres/schlimmeres/... tun, ist unser nicht ganz so dummes/schlimmes Verhalten schon okay?
* Die Spieler haben sich in den letzten Wochen so toll verhalten, selbst nachdem sie die Gastgeber überragend geschlagen haben. Mag auch sein, aber wieso hält man nicht daran fest? Und es gehört nicht viel Verstand dazu, nach einem desaströsen 7:1-Sieg die andere Mannschaft nicht weiter zu demütigen.
* *Lasst sie doch feiern! Was ist schon dabei?* Das ist mein Favorit. Meist kommt noch ein *Wir haben doch alle schon mal was dummes gemacht* hinterher. Das ist in so weit vollkommen richtig, dass wir Menschen ständig Fehler begehen. Der Unterschied ist nur, sie tun es nur sehr selten vor tausenden Menschen und dutzenden Kameras. Und sie sind auch keine gut bezahlten Profisportler, die auch eine Verantwortung tragen. Das fand Spiderman schon ätzend. 

Es gibt noch weitere Variationen dieser Argumente. Ein überzeugendes habe ich noch nicht gelesen.

## Fazit: Schade 

Wer mich kennt der weiß, dass ich mit Fussball nichts am Hut habe. Trotzdem schaute ich einige Spiele und unsere sowieso. Ich habe mich auch über den Sieg gefreut und es der Mannschaft gegönnt.     
Mir ist nicht ganz klar wieso tausende Menschen zu dieser Begrüßungsfeier pilgern und wieso sie nicht arbeiten müssen, aber sei es drum. Ich habe mir ein halbes Stündchen das seltsame Spektakel im Fernsehen angeschaut, war erheitert und dann doch negativ überrascht, als ich diese Tanz-Szene sah. Nicht negativ genug um sofort auszuschalten, das schaffte erst Helene Fischer.

Eigentlich ist es einfach nur schade. Es hätte nicht sein müssen, man hätte sich auch anders präsentieren können. Wie tragisch es ist, sei dahingestellt, aber es wirft definitiv ein schlechtes Licht auf die Feier.  


[^1]: Zugegeben, ich musste erst einmal den Begriff googlen. Mit [Gauchos](http://de.wikipedia.org/wiki/Gaucho) *&quot;werden die Nachkommen iberischer Einwanderer und Indigos, die in den Pampas Viehzucht betreiben,&quot;* bezeichnet. Meines Erachtens also kein freundlicher Begriff, sicherlich aber auch nicht auf einer Stufe mit *Neger* oder *Nazi*. Der Tanz und der Spruch sind in Fussballstadien beliebt.

