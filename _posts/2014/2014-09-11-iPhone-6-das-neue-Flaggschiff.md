---
layout: post
title: "iPhone 6 , das neue Flaggschiff"
date: 2014-09-11
category: it
tags: [iPhone]
published: true
comments: false
share: true
image: 
  feature: 2014-iphone-6-lg.jpg 
  credit: Apple
  creditlink: http://www.apple.com
---

Es gab bezüglich der neuen iPhones leider keine Überraschungen von Apple und die neue Richtung lässt mich verwundert zurück.     

Kurz gesagt: Es gibt zwei neue iPhones, welche zum einen größer als die bisherigen 4-Zoll-iPhones (5s und 5c) sind und zum anderen ein neues iPhone-Design mitbringen. Die beiden alten Modelle bleiben unverändert im Sortiment und wurden lediglich im Preis angepasst. Es stellen sich einige Fragen über die Zukunft des iPhones.

## Welches ist das Flaggschiff iPhone?

Viele Jahre lang präsentierte Apple **ein neues iPhone pro Jahr** und das war das Flaggschiff. Das Vorjahresmodell wurde im Preis reduziert und konnte mit 8 GB Speicher weiterhin gekauft werden. Die erste Änderung dieses Patterns gab es letztes Jahr mit dem iPhone 5c. Es hatte ein Gehäuse aus Kunststoff und nicht die neuste Hardware verbaut. Quasi das Einsteigermodell.      
Apple hat zwar nie die genauen Verkaufszahlen veröffentlicht und das zeitgleich erschienene iPhone 5s war wesentlich erfolgreicher, aber sie ließen das 5c dieses Jahr im Sortiment.

Was sagen uns die diesjährigen iPhones? Jahrelang ignorierte Apple die 4,x und 5,x Zoll großen Flaggschiffe der Android-Konkurrenten[^1] und jetzt springen sie auf den Zug auf und vernachlässigen gleichzeitig das bisherige Flaggschiff-iPhone mit der erfolgreichsten Bildschirmgrösse überhaupt. Es gibt meiner Meinung nach zwei mögliche Szenarien. 

1. Das iPhone 5s pausiert. Wirtschaftlich ist das sinnvoll, denn die verschiedenen Komponenten des iPhones (Chips, Kamera, Gehäuseteile,…) werden von Monat zu Monat billiger in der Produktion. Und das 5s ist immer noch sehr beliebt. Außerdem legt Apple damit den Fokus komplett auf die neuen iPhone 6. Nächstes Jahr gibt es dann ein Update für alle iPhones auf die Nummer 7 mit einheitlichem Design[^2] und neuer Hardware[^3]. Oder Apple wartet bis zum Frühjahr und spendiert den 5er iPhones ein Update.

2. Das iPhone 6 und 6 Plus sind die neuen Flaggschiffe und das 5s bekommt nächstes Jahr nur ein Update auf iPhone 6 Niveau während die beiden Flaggschiffe die Chips und Kameras der nächsten Generation bekommen.

Vielleicht weiß Apple selbst noch nicht, ob es mehrere Flaggschiffe pflegen wird. Besitzer eines Mobilfunkvertrags, der jetzt ausläuft, haben zur Zeit leider nur die Wahl zwischen einem ein Jahr alten 5s oder dem neuen iPhone mit den beiden nicht unerheblichen grösseren Bildschirmgrössen. Was ist das bitte für eine Wahl?

## Wieso, Apple?

Wieso beginnt Apple bei 16 GB als Speichergröße und springt dann auf 64 GB? Besonders beim iPhone 6 Plus mit der enormen Auflösung und **Retina HD** Display sind allein die Bild-Ressourcen der Apps schon erheblich größer (also benötigen die Apps allein schon mehr Speicherplatz). Von den Bildern und Videos der Benutzer ganz abgesehen.     
Apple möchte eine möglichst hohe Gewinnmarge und 16 GB sind billiger als 32 GB. Aber das kann doch nicht ihr ernst sein!? Ich bin mir sicher dass einige Benutzer mit einem 16 GB iPhone 6 Plus sehr schnell an das Maximum ihres Speichers stoßen und sich dann ganz schön verarscht fühlen werden. 

Wieso muss das neue iPhone noch dünner sein als das 5s, welches schon verdammt dünn ist? Das 6 Plus profitiert dank seiner Grösse auch von einem grösseren Akku. Aber das kleinere 4,7 Zoll iPhone 6 ist in diesem Punkt nicht besser als das 5s und beide haben eine zu kurze Akkulaufzeit. Wieso ignoriert Apple seit Jahren diesen Kritikpunkt?     
Während meines Urlaubs in Paris, spürte ich die magere Akkulaufzeit des 5s deutlich. Ich nutzte es zu 80% zum Tracken, schoss ab und zu mal ein Foto oder Video und nutzte eine Offline Karten App. Trotzdem musste ich fast jeden Tag den portablen Akku anschließen. Unter den Touristen gab es viele, die mit ihrem Smartphones Fotos schossen. Entweder hatten sie nur sehr kurze Trips geplant oder konnte ab Nachmittags nichts mehr Fotografieren.

Wieso muss ich das grosse iPhone 6 Plus wählen, wenn ich einen optischen Bildstabilisator haben möchte? Wieso kann man die iPhones nicht einen Tick dicker machen? Dann passt die bessere Kamera hinein und es ist etwas Platz für mehr Akku.     
Wenn ich kein 5,5 Zoll Smartphone möchte, weil ich es einfach nicht bedienen kann, muss ich die schlechtere Kamera in Kauf nehmen. Normalerweise entscheidet der Geldbeutel über die Features, die man sich leisten kann. Mit dem iPhone 6 Plus entscheidet aber die Biologie - besitzt du eine grosse oder kleine Hand.      
Falls sogar der oben erwähnte 2. Punkt eintritt und das iPhone 5s nicht als Flaggschiff nachzieht, wird dieser Umstand noch unschöner.

## Fazit

Ich verstehe wieso man neue grössere iPhones anbieten möchte. Über bestimmte Entscheidungen kann ich aber nur den Kopf schütteln, siehe Speichergrössen und mangelnde Akkulaufzeit. Am meisten beunruhigt mich aber die Frage nach dem Flaggschiff.      
Wenn ich die neuste Technik haben möchte, muss ich in Zukunft ein für mich zu großes Smartphone in Kauf nehmen? Das weist in die Richtung des aktuellen Markts für Android Smartphones, wo die Flaggschiffe nur noch riesig sind. Ich hoffe sehr, dass Apple diesen Weg nicht weiter verfolgt. Denn das wäre definitiv nicht der beste Weg für den Benutzer.

[^1]: Und das aus gutem Grund. Für die Mehrheit der Benutzer sind 3,7 bis 4,0 Zoll die optimalen Bildschirmgrößen und am besten zu bedienen. 

[^2]: Die neuen iPhones und das alte 5c haben sich dem Design der iPads angepasst und lediglich das iPhone 5s ist mit seinen Kanten der Außenseiter und wird wohl nächstes Jahr nachziehen. 

[^3]: Das 7c wird mit der alten Hardware des 5s ausgestattet, aber 7s, 7 und 7 Plus sollten identisch sein.