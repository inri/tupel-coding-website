---
layout: post
title: "Einsatz eines Pulsmessers"
date: 2014-08-30
tags: [App, iOS]
category: blog
published: true
comments: false
share: true
image: 
  feature: 2014-brustgurt-crosstrainer-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Der iPhoneBlog [berichtete vor einigen Tagen vom Pulsmesser von Beurer](http://www.iphoneblog.de/2014/08/18/in-vorbereitung-auf-ios-8-ein-plusmesser-von-beurer/). Auch ich bin in Besitz eines solchen Accessoire und zwar eines Brustgurts von *wahoo*.      

Ich habe den Brustgurt nach dem Kauf einige Male beim Laufen benutzt, aber kann seit einer Verletzung nicht mehr Joggen. Er lag eine Weile rum und nun kommt er wieder zum Einsatz, weil wir uns einen Crosstrainer zugelegt haben, den *Christopeit CX 6* [^2]. Mit dem kann ich wieder schmerzfrei laufen, auch wenn es nur in den eigenen vier Wänden ist. 
          
Den eigentlichen Nutzen eines solchen Brustgurtes für die Pulsmessung, deutet der iPhoneBlog an:

> Ich hatte bislang keine Motivation meine Herzfrequenz in die (mir durchaus wichtigen) Laufstatistiken aufzunehmen [...]. Mit dem Beurer-Brustgurt, der hier wie erwähnt nur durch Zufall aufschlug, änderte sich das. Es ist durchaus informativ zu sehen, wann und wo die Belastung zu hoch war.

Es ist nicht nur informativ, sondern gar nicht so unwichtig. Der menschliche Körper reagiert auf unterschiedliche Arten von Belastung auch unterschiedlich. Natürlich kann man einfach so durch die Weltgeschichte joggen oder Sport treiben, aber nicht selten bezweckt man ja etwas mit dem Sport. Der eigene Puls hilft dabei.

### Pulszonen

Hier kommen die Pulszonen ins Spiel. Diese berechnen sich aus dem Maximalpuls bzw. der maximalen Herzfrequenz (ca. 220/226 minus Alter). Beispielsweise die *Fettverbrennungszone* liegt bei ca. 60% - 70% der maximalen Herzfrequenz, das wäre bei mir mit meinen 26 Jahren ein Puls zwischen 130 und 150 [^3]. Bei einer Belastung, die zu diesem Puls führt, verbrennt mein Körper die meisten Kalorien aus Fett.      
Dann gibt es noch die Aerobe (ca. 70% - 80%) und Anaerobe (ca. 80% - 90%) Zonen, welche ihren Energiebedarf nicht nur aus Fett gewinnen.     
Für weitere Theorie bitte [Google](https://www.google.de/search?client=safari&rls=en&q=pulszonen&ie=UTF-8&oe=UTF-8&gfe_rd=cr&ei=hxj5U_efDIivOrjKgKgP).

Mein Training sieht wie folgt aus: Ich schnalle mir den Brustgurt um, lege mein iPhone neben mir aufs Regal und steige auf den Crosstrainer. Auf dem iPhone zeichnet Runtastic leider nur die Zeit und meinen Puls auf [^4]. Trotzdem ist das sehr hilfreich denn ich sehe jederzeit wie mein Puls ist und kann meine Laufgeschwindigkeit anpassen. Nach dem Training trage ich die gelaufene Strecke in Runtastic ein und habe meine komplette Trainingseinheit [^5].

<figure>
    <a href="/images/2014-runtastic-pulszonen-800.jpg">
	   <img 
		  src="/images/2014-runtastic-pulszonen-800.jpg" 
		  alt="Runtastic Pulszonen Statistik"></a>
    <figcaption>Das Kuchendiagramm bitte ignorieren, denn die Verteilung steht im Verhältnis zur zurückgelegten Strecke in der jeweiligen Pulszone, nicht der Zeit. Eine Strecke gibt es nicht, denn ich ändere meine Position nie (Crosstrainer).</figcaption>
</figure>


Tendenziell laufe ich zu schnell und habe deshalb einen zu hohen Puls, aber das wusste ich vorher nicht. Nun kann ich es besser steuern und wenn ich bspw. mehr Muskelaufbau betreiben möchte, weiß ich wo meine anaerobe Zone ist.

Ich denke mit einem Pulsmesser kann man das Training wesentlich effizienter gestalten. Am liebsten wäre mir ein Crosstrainer, der nahtlos mit Runtastic arbeitet und gute Pulswerte liefert, aber das dauert wohl noch ein paar Jahre, bis das Standard ist.

### Nachtrag

Eigentlich wollte ich den *wahoo* Brustgurt empfehlen, aber nachdem ich diesen Artikel beendet hatte, ging er scheinbar kaputt. Er findet die Verbindung gar nicht oder zeigt entweder keinen oder einen gleichbleibenden Puls an und die Batterie ist nicht der Grund. Wir haben ihn insgesamt keine 30 mal benutzt, aber die Garantie ist abgelaufen. Vielleicht ist die Herstellerfirma zuvorkommend. Mein nächster Brustgurt wird sonst der Beurer.

[^2]: Über den Crosstrainer folgt auch noch ein kurzer Artikel. Hier sei nur angemerkt, dass er in den Griffen auch Pulsmesser eingebaut hat, aber deren Werte erheblich vom Brustgurt abweichen. Für eine ernsthafte Messung leider nicht zu gebrauchen. Man könnte jedoch einen passenden Brustgurt dazu kaufen. Der Beurer-Brustgurt funkt aber mit Bluetooth und das unterstützt der Crosstrainer leider nicht.

[^3]: Das sind keine genauen Werte und z.b. die Runtastic App ordnet meine Zonen etwas anders ein als nach der exakten Berechnung.

[^4]: Es gibt auch Crosstrainer und andere Sportgeräte, die eine iOS-Anbindung bieten. Allerdings sind diese Geräte in den höheren Preislagen angesiedelt.

[^5]: Runtastic berechnet auch die Kalorien, die man angeblich verbrannt hat und die fallen zumindest bei mir immer sehr hoch aus. Der Crosstrainer ist wesentlich sparsamer mit seiner Schätzung, kennt aber auch mich und meinen Körper nicht. Die Mitte zwischen beiden Werten trifft die Berechnung auf [erdbeerlounge.de](http://diaet.erdbeerlounge.de/Kalorienverbrauch-berechnen-mit-Kalorienverbrauchsrechner).