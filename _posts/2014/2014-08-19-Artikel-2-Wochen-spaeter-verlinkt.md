---
layout: post
title: "Artikel zwei Woche später verlinkt"
date: 2014-08-19
tags: [Web]
category: blog
published: true
comments: false
share: true
---

In den ersten Wochen nach der ersten Xcode 6 Beta, pflegte ich viele Seiten von Leuten, die über Swift schreiben, in mein Feedly und Twitter. Tagtäglich schaffen es unzählige Neuigkeiten, Erkenntnisse und tolle Tutorials in meine Filterbubble. Ich kann mittlerweile nur einen Bruchteil davon lesen, aber wie ich bereits schrieb, bin ich von der Masse beeindruckt. 

Umso überraschter war ich über die verspätete Verbreitung eines [Performance-Vergleich von Jesse Squires](http://www.jessesquires.com/apples-to-apples-part-two/). Das war sogar sein zweiter Vergleich und er stammte vom 06.08., kurz nach Veröffentlichung der 5. Xcode 6 Beta. Ich fand ihn schon vor zwei Wochen interessant.

Es begann mit dem Twitter Account *Coding in Swift*, welcher Montagabend den Vergleich verlinkte, kurz nachdem die 6. Beta online ging. Ich vermute, sie haben gedacht, dass der Vergleich *frisch* war, also zur 6. Beta und nicht zur 5.

<blockquote class="twitter-tweet" lang="de"><p>Apples to apples, Part II <a href="http://t.co/D30DIf53NM">http://t.co/D30DIf53NM</a> via <a href="https://twitter.com/jesse_squires">@jesse_squires</a> An analysis of sorts between Objective-C and Swift <a href="https://twitter.com/hashtag/swift?src=hash">#swift</a> <a href="https://twitter.com/hashtag/swiftlang?src=hash">#swiftlang</a></p>&mdash; Coding in Swift (@codinginswift) <a href="https://twitter.com/codinginswift/statuses/501456515090239488">18. August 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Ich las den Tweet und wunderte mich. Noch mehr wunderte ich mich, als [John Gruber den Artikel auch noch verlinkte](http://daringfireball.net/linked/2014/08/18/swift-performance), ebenfalls Montagabend. Vermutlich hat er den gleichen Tweet gelesen oder ihn hat jemand darauf aufmerksam gemacht. Wiederum *Coding in Swift* gratulierte Jesse Squires zur Verlinkung von Gruber.

<blockquote class="twitter-tweet" lang="de"><p>Congrats, Jesse Squires <a href="https://twitter.com/jesse_squires">@jesse_squires</a>,mentioned over at <a href="https://twitter.com/daringfireball">@daringfireball</a> his blog on <a href="https://twitter.com/hashtag/Swift?src=hash">#Swift</a> Performance. <a href="http://t.co/pdIqQbUOxD">http://t.co/pdIqQbUOxD</a> <a href="https://twitter.com/hashtag/swiftlang?src=hash">#swiftlang</a></p>&mdash; Coding in Swift (@codinginswift) <a href="https://twitter.com/codinginswift/statuses/501498370490253313">18. August 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Das war schon ein wenig amüsant, aber die Dominos fielen weiter. Heute schrieb [auch Heise](http://www.heise.de/newsticker/meldung/Swift-Benchmarks-Neue-Programmiersprache-flotter-als-Objective-C-2294477.html?wt_mc=rss.ho.beitrag.atom) [einen Artikel](http://www.heise.de/developer/meldung/Swift-Benchmarks-Neue-Programmiersprache-flotter-als-Objective-C-2294477.html?wt_mc=rss.developer.beitrag.atom) über den Performance-Vergleich. Höchst faszinierend wie ein 2 Wochen alter Artikel solche Kreise zieht.