---
layout: post
title: "Hyperlapse"
date: 2014-08-27
tags: [App, Technologie]
category: blog
published: true
comments: false
share: true
---

Vor einigen Wochen hat [Microsoft Research](http://research.microsoft.com/en-us/um/redmond/projects/hyperlapse/) eine Methode für ein *first-person Hyperlapse* Video vorgestellt. Leider gab es noch keine App und angekündigt wurde vorerst auch nur eine Windows App. Nun hat Instagram die Theorie umgesetzt und die [iOS App Hyperlapse](http://hyperlapse.instagram.com) veröffentlicht. 

Der Begriff *Hyperlapse* beschreibt nichts anderes als ein Zeitraffervideo. Die Kunst besteht darin, ein stabiles Bild zu erhalten und deshalb war das Aufnehmen solcher Video relativ kompliziert. Meist nutzt man Stative. Was Instagram und Microsoft nun vorstellen, ist eine erhebliche Erleichterung. Doch Hyperlapse ist nicht gleich Hyperlapse.

Microsoft nutzt einen Algorithmus, der aus einem Video ein Hyperlapse-Video mit stabilem Bild generiert. Das Ergebnis sieht toll aus, benötigt aber eine gewisse Rechenleistung, immerhin muss das Video analysiert werden. 

> Imagine a video clip, taken from a moving car. To even the juddering camera motion, image stabilization algorithms typically analyze a movie frame by frame, identifying image fragments common to each. By recording how those shared points jump around across frames, algorithms can then infer how the camera has been moving. By reverse engineering that motion data, software can recreate a new, steadier version of a film clip. Yet every step in that process requires processing muscle. That’s fine for a movie studio, which has massive computers that crank overnight to re-render a scene. It’s ridiculous for a smartphone.

Instagram dagegen bedient sich bei den Gyroskop-Sensoren des iPhones. Ein verwackeltes Videobild stammt von einem verwackeltem iPhone und den Grad des Wackelns erfährt man von den Sensoren.

> Inspired by a demo in which he saw gyroscopes attached to cameras to de-blur their images, Karpenko had an aha moment: Smartphones didn’t have nearly enough power to replicate video-editing software, but they did have built-in gyroscopes. On a smartphone, instead of using power-hungry algorithms to model the camera’s movement, he could measure it directly. And he could funnel those measurements through a simpler algorithm that could map one frame to the next, giving the illusion that the camera was being held steady. 

[Mehr Einblicke in die Entwicklung der App gibt es bei Wired.](http://www.wired.com/2014/08/hyperlapse-instagrams-new-app-is-like-a-15000-video-setup-in-your-hand/)

Die App ist ziemlich toll und macht viel Spaß. Ich bin auf jeden auch auf das Programm von Microsoft gespannt, solche Hyperlapse-Videos nachträglich zu Erzeugen.

<iframe width="853" height="480" src="//www.youtube-nocookie.com/embed/P9SbbITBQ8s?rel=0" frameborder="0" allowfullscreen></iframe>
Der Blick vom Balkon aus in 12-facher Geschwindigkeit. Das iPhone steckte in einer Halterung und auf einem Gorillapod. Auf Youtube gibt es weitere Hyperlapse-Videos von mir.

<iframe width="853" height="480" src="//www.youtube-nocookie.com/embed/wO3VAqwhly0?rel=0" frameborder="0" allowfullscreen></iframe>       
Meine Freundin beim Kürbis schneiden. Diesmal habe ich das iPhone selbst gehalten.