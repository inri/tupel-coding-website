---
layout: post
title: "Planet der Affen - Revolution"
date: 2014-08-08
tags: [Film]
category: medien
published: true
comments: false
share: true
image: 
  feature: 2014-planet-der-affen-revolution-lg.jpg
  credit: Twentieth Century Fox
  creditlink: http://www.planetderaffen-revolution.de
---

**Was. Für. Ein. Grossartiger. Film.** Wow! Ich bin verdammt beeindruckt und meine Knie zittern noch leicht. Aber wieso war dieser Film so beeindruckend? Ich möchte versuchen euch auf diese und weitere Fragen möglichst gute Antworten zu geben.

## Sag mir kurz worum es geht!

Wir sind im 10. Jahr nach dem Ausbruch einer Epidemie, welche die Menschheit fast ausgelöscht hat. Gleichzeitig hat das Alzheimer-Heilmittel, dessen Erforschung die Epidemie auslöste, bei den Versuchs-Affen zu erhöhter und vererbbarer Intelligenz geführt. Während die Menschen ums Überleben kämpften, begannen die Affen mit dem Aufbau einer Zivilisation.     
Nun treffen beide Lager wieder aufeinander. 

## Muss ich den 1. Teil vorher anschauen?

Nicht zwangsläufig, denn der Film bleibt trotzdem toll, auch ohne Vorwissen. Es hilft den Zuschauer aber dabei, unter anderem Caesars Beweggründe besser zu verstehen. Ganz davon abgesehen dass auch der erste Teil schon toll gewesen ist. Deshalb: Unbedingt anschauen! Zur Not auch danach.

## Bitte mehr Details zum Film!

In den ersten Minuten erfährt man, wieso eine Begegnung zwischen Menschen und Affen unausweichlich ist. Die Menschen kommen in die Wälder der Affen weil sie Strom benötigen und auf der Suche nach dem Wasserkraftwerk sind. Sie wissen aber nicht, dass die Affen dort leben.

Beide Parteien treffen aufeinander und es kommt zu Konfrontationen. Auf beiden Seiten gibt es die Misstrauischen, die der anderen Seite feindlich gesinnt sind. Doch es gibt auch die Guten auf beiden Seiten, die versuchen zusammenzuarbeiten und Vertrauen aufzubauen.

Es ist eine Geschichte über Verrat, Vertrauen und Hoffnung. 

Ich habe selten einen Film gesehen, der so logisch und gut durchdacht war, und trotzdem abwechslungsreich. Man hat in der ersten Hälfte ständig das Gefühl, das gleich irgendetwas schlimmes passiert und alles eskaliert. Jedem Zuschauer ist bewusst, dass die Konfrontation zwischen beiden Rassen nicht gutgehen kann. Es ist unglaublich spannend und man verzehrt jede Minute des Films, weil es einfach keine einzige langweilige Minute gibt.

Auch die Figuren sind authentisch und das CGI will ich damit noch gar nicht ansprechen. Jede Handlung hat das richtige Timing um die intensivste Wirkung auf den Zuschauer zu entfalten und dabei trotzdem plausibel zu bleiben [^1].     

## Dressierte Affen?

Ich decke hiermit eine Verschwörungstheorie auf. Angeblich hat man die Affen am Computer animiert und ihre Bewegung und Ausdrücke mit dem Motion-Capturing-Verfahren an richtigen Schauspielern aufgenommen. Das ist aber nur das Märchen um die Tierschützer nicht aufzubringen. In Wirklichkeit waren erstklassige Affendresseure am Werk und ob das ganz ohne Tierquälerei ablief? Man weiss es nicht.

Doch im Ernst, die Affen sehen unglaublich echt aus. Alle Effekte sind in diesem Film grandios, wie zum Beispiel das zugewachsene und verlassene San Francisco. Die ersten Minuten des Films zeigen das Leben der Affen und eine Jagd. Dabei wird kein Wort gesprochen. Trotzdem werden die Emotionen über Mimik und Gestik perfekt transportiert, beispielsweise durch die Zeichensprache der Affen. 

Manchmal reden die Affen einige Worte, aber es sind stets nur wenige, doch dafür auf den Punkt gebrachte Worte. Wenn bei uns Menschen die Mimik das Gesagte unterstützt, ist es bei den Affen umgekehrt. 

## Fazit

Grossartige Handlung, bei der jeder Zuschauer einige Denkanstösse mit nach Hause nehmen kann.      
Grossartige Computertechnik, die verdammt echt aussehende Affen zeigt. 
Grossartige Figuren, die in dieser düsteren Welt über sich hinauswachsen.

Was soll ich sagen? Schaut euch den Film an und drückt mit mir die Daumen, dass der dritte Teil in absehbarer Zeit erscheint.

[^1]: Ein Kommentator verwies auf folgende Szene als Beispiel für das plausible Verhalten bzw. fehlende Blockbuster Dummheiten: Beim zweiten Trip zu den Affen, geht der Menschenheld allein zu den Affen und gibt seinen Leuten die Instruktion in den Autos zu bleiben. In jedem anderen Blockbuster wäre einer ausgestiegen, in den Wald gelaufen und hätte die Situation zum eskalieren gebracht. Doch es warten alle brav.