---
layout: post
title: "Kalte Wohnung, Update 1/2"
date: 2014-03-10
category: blog
tags: [Persönlich]
published: true
comments: false
share: true
---

Ende November habe ich von unserer kalten Wohnung in Halle berichtet. Nun, nachdem wir es endlich warm in unseren vier Wänden haben und alles geklärt ist, kann ich von den vergangenen 3 Monaten berichten.

## Außerordentliche Kündigung

Unsere Wohnungsverwaltung **hatte** ihren Sitz in Leipzig. Die Firma an sich sitzt in Berlin, aber bis Ende diesen Jahres gab es noch eine Verwaltung in Leipzig, die sich um uns kümmern sollte. Eigentlich, denn das tat sie nicht.    
Die kleinen Mängel, die wir gleich nach Einzug gemeldet haben, wurden sofort ausgebessert, aber das Problem mit der kalten Wohnung nicht. Genauer gesagt gab es nach unserer auffordernden Nachricht, Ende November, keinen Kontakt mehr. 

Wir dachten der Hinweis auf eine Mietminderung könnte Druck erzeugen, aber da täuschten wir uns. Als es keine Rückmeldung gab, waren wir beunruhigt und mussten in Hilfeforen lesen, dass der Vermieter nichts gegen das Problem tun **muss**.   
Also zogen wir die Notbremse und verschicken eine außerordentliche Kündigung. Uns war bewusst, dass die Temperaturen in unserer Wohnung nicht niedrig genug waren um eine ausreichende Begründung darzustellen, doch mam konnte es ja versuchen. 

## Verwaltung geschlossen

Es gab keine Reaktion auf die Kündigung. Einige Tage später kam der Hausmeister zu uns um die Schlitze zwischen Fenster und Außenwand mit Silikonmasse zu verstopfen (was natürlich genau gar nichts an unserer Raumtemperatur änderte). Dabei erzählte er uns, dass die komplette Verwaltung in Leipzig Ende 2013 geschlossen wird und auch der Belegschaft gekündigt wurde. Das war übrigens auch der Grund, warum es keine Reaktion mehr gab.

Unsere Sachbearbeiterin bekam ihre Kündigung und ärgerte sich so sehr darüber, dass sie keine Fälle mehr bearbeitete. Uns also mit der kalten Wohnung alleine ließ, wohlwissend dass Wochen vergehen würden bis sich aus Berlin überhaupt mal jemand meldet. Ein solch unprofessionelles Verhalten habe ich selten gesehen. Hoffentlich wird diese Frau nicht noch mal auf die Menschheit losgelassen.

Die Tage vergingen und wir bekamen den Kontakt zu einem befreundeten Anwalt. Dieser schrieb der Verwaltung in Berlin und bat um eine Antwort bzgl. unserer Kündigung. 

## Neue Verwaltung

Kurz vor Weihnachten hing ein Schreiben im Schaukasten des Hausflurs und darauf fanden sich die Kontaktdaten unserer neuen Sachbearbeiterin in Berlin. Sofort riefen wir sie an uns bekamen sie sogar an den Hörer. Doch das Gespräch war unerfreulich.

Sie sagte, dass die Fassade gar nicht marode sei und man unseren Temperaturangaben auch gar nicht glaubt. Wow. Das Wort marode nahm der Fenstermonteur in den Mund, ob es der korrekte Begriff ist oder nicht, ändert nichts an der Raumtemperatur. Stattdessen war sie davon überzeugt, dass wir nur umziehen wollen, um den Einzugs-Bonus abzustauben und dann die Miete nicht weiter zahlen zu müssen. 

Das ganze Gespräch war eine bodenlose Frechheit. Sie wollte sich noch beim Hausmeister erkundigen und zumindest er sagte mir später, dass er ihr gesagt hätte, wie schlimm es wirklich ist. Trotzdem erhielten wir einige Tage später einen Brief mit der Ablehnung der Kündigung. 

Eine Anmerkung zur Kündigung: Wir haben einen, durchaus üblichen, Passus in unserem Mietvertrag, welcher beiden Parteien für 12 Monate verbietet den Mietvertrag zu kündigen. Das gute: Man kann nicht rausgeschmissen werden, das schlechte: man kommt nicht so schnell raus. Das war nun unser Problem. 