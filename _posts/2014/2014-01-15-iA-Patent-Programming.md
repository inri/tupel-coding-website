---
layout: post
title: "iA Patent Programming"
date: 2014-01-15
category: it
tags: [iOS, Coding, App, Kritik]
published: true
comments: false
share: true
---

Lange hat es nicht gedauert bis das als Patent angemeldete Feature *Syntax Control* in iA's neuer Schreib-Software Writer Pro in die Kritik geriet.    

Ich war zunächst auch beeindruckt von der neuen Funktion. Sich einzelne Wortarten in einem Text anzeigen zu lassen ist hilfreich und genial. Und deshalb stellte ich mir die Frage: Wieso implementierte das noch niemand vorher? 

Wie sich nun herausstellt, leistet das Cocoa Framework von Apple einen Großteil der Arbeit. Stichwort: [**NSLinguisticTagger**](http://nshipster.com/nslinguistictagger/).   
Nachdem iA versprachen keine Entwickler zu verklagen die etwas ähnliches implementieren, traten sie mittlerweile sogar von ihrem Patentantrag zurück und [fordern absurderweise](http://ia.net/blog/writer-pro/) immer noch ein

> If you want to use it in your text editor, just give us credit for introducing it.

## Mehr als 4 Jahre Arbeit (?)

Ohne zu wissen, dass Apple die Funktionalität bereitstellt, hätte ich den Aussagen, dass mehr als 4 Jahre Entwicklung in Writer Pro und insbesondere *Syntax Control* geflossen sind, geglaubt. Es kam mir zwar etwas lang vor, aber okay.    
Nun frage ich mich: Was haben die 4 Jahre lang gemacht? Und wieso lügen sie über die Entwicklung von *Syntax Control*?

Ernsthaft, was haben sie da genau entwickelt um der Meinung zu sein, dass man es sogar patentieren könnte? 

Die beste Einschätzung kommt von MOApp: [Sei kein Arschloch, sei besser!](http://createlivelove.com/803/ia-troller-not-so-pro/) Patente in der Softwareindustrie sind kaum zu rechtfertigen. Jede neue Idee für ein tolles Feature hat bestimmt schon jemand vor dir gebaut, du weißt es nur nicht. Diesem Thema müsste ich einen eigenen Eintrag widmen.

Was ich eigentlich nur sagen will: Ich hoffe dass iA sich ausführlich zu dem Debakel äußert und uns erklärt, was sie sich eigentlich gedacht haben. Noch steht ein Kommentar aus. Oder man möchte das Thema gar nicht mehr anfassen.


