---
layout: post
title: "Placescore"
date: 2014-03-05
category: blog
tags: [App, Game, iOS]
published: true
comments: false
share: true
---

Schon seit einigen Monaten machten Marcel Wichmann (und Freunde) immer mal wieder Andeutungen über ein Spiel welches sie entwickeln. Der Name **Placescore** war relativ zeitig bekannt. Gestern gab es endlich den Startschuss und Placescore fand sich im App Store wieder.

## Was is Placescore?

In dem Spiel gibt es zwei Ebenen. Das eigentliche Spiel ist ein Drei-gewinnt mit sich selbst. Kurz gesagt: Baue in x Sekunden möglichst viele Ketten von gleichfarbigen Kugeln. Pro Kugel gibt es einen Punkt. 

<figure>
    <a href="/images/2014-placescore-game-1600.jpg">
	   <img 
		  src="/images/2014-placescore-game-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Drei-gewinnt als Mini-Game. In diesem Fall spiele ich gegen mich selbst.</figcaption>
</figure>

Die zweite Ebene ist der eigentliche Clou von Placescore. Ein simples Drei-gewinnt gibt es sicherlich schon im App Store, aber die Jungs führen den Highscore nicht global (und unter Freunden, wie in Apples Game Center üblich), sondern standortabhängig. Wo der Spieler sich befindet, gibt es Places, das kann eine Stadt sein, ein Bahnhof, ein Einkaufszentrum, eine Schwimmhalle, ein Hotel und so weiter.   
Zu Beginn &quot;gehören&quot; diese Places niemanden. Ein Spieler &quot;erhält&quot; ein Place wenn er ein erfolgreiches Spiel abschließt. Die Punktzahl aus diesem einen Spiel wird mit dem Place gekoppelt. Möchte man ein Place erspielen, welches schon im Besitz eines anderen Spielers ist, muss man dessen Punktzahl schlagen. 

Natürlich muss man sich in geographischer Nähe zu einem Ort befinden. Luchst mir ein anderer Spieler einen Ort ab, kann ich (freigeschalten über einen In-App-Kauf) drei Mal versuchen diesen Ort zurückzuspielen, auch wenn ich nicht der Nähe dieses Ortes bin. 

<figure>
    <a href="/images/2014-placescore-list-1600.jpg">
	   <img 
		  src="/images/2014-placescore-list-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Einige Orte in meiner Nähe, die ich natürlich alle besitze.</figcaption>
</figure>

## Simpel wie genial

Wie auch schon bei [watched.li](http://watched.li), haben Wichmann und Freunde eine API kreativ genutzt. Placescore wäre in dieser Form ohne die Daten von **Foursquare** womöglich auch gar nicht realisierbar gewesen.    

Die Idee der lokalen Eroberungen ist noch dazu eine elegante Lösung für ein aktuelles Problem im Game Center bzw. generell globaler Bestenlisten. Denn in diesen Listen gibt es immer nur begrenzt viele &quot;gute&quot; Plätze, ebene eine Top 10 oder vielleicht noch Top 100. Schon bei 10.000 Spielern schaffen es rein rechnerisch 99% der Spieler nicht in die Top 100 und das ist natürlich nicht besonders motivierend.   
Bei Placescore ist die Anzahl gegnerischer Spieler sehr begrenzt, da ich auf lokaler Ebene spiele. Somit ist es auch viel wahrscheinlicher gegen andere Spieler zu bestehen und meine Motivation aufrecht zu erhalten.

Ansonsten spielt sich Placescore sehr gut. Der Plopp-Sound erinnert bestimmt nicht ohne Grund an die gute alte [Bubble-Wrap-Folie](https://www.google.com/search?q=bubble-wrap&client=safari&rls=en&tbm=isch&tbo=u&source=univ&sa=X&ei=jukWU9qEIoPRtQawroHACg&ved=0CC0QsAQ&biw=1440&bih=764). Die Animation der verschwindenden und nachrutschenden Kugeln finde ich persönlich zu statisch. In diesem Punkt fehlt dem Spiel die Leichtigkeit eines **Letterpress** oder **Threes**.

## Motivation und weitere Features

Wie wird sich Placescore entwickeln? So sehr ich die Idee der lokalen Eroberungen auch gut finde, sie hat trotzdem ihre Grenzen. Eine Punktzahl über 300 ist schon ziemlich gut. Zwar ist die Motivation relativ groß immer mal wieder zu versuchen diesen oder jenen Ort zu erobern, doch irgendwann fällt auch das schwer.    
Abhilfe schaffen hier vielleicht weitere Arten von Powerups, die später von den Entwicklern eingepflegt werden könnten. Die Motivation kann auf diese Art aber nur schubweise gesteigert werden.

Mir persönlich fehlt die Übersicht meiner Eroberungen. Ich sehe nur die Orte, die aktuell in meiner Nähe sind. Gestern habe ich während der Zugfahrt einige Eroberungen getätigt, kann diese nun aber nicht mehr sehen. Hier wäre eine Übersicht auf einer Karte ganz schick, auf welcher mit farbigen Punkten meine Eroberungen eingezeichnet sind (Farbe in Abhängigkeit zur Punktzahl).

Ich weiß nicht wo die Orte sind. Die Bezeichnung *Bahnhof xyz* ist klar, aber in meiner Liste taucht bspw. &quot;Krimis&quot; auf. Was und wo ist das?  Manchmal ist eine Straße aufgeführt, manchmal nicht. Ich wünsche mir die Möglichkeit mehr Informationen über einen Ort herauszufinden, inkl. genauen Standort, also wieder eine Karte.

## Fazit

Placescore ist ein nettes Spiel für Zwischendurch. Die Idee der standortbasierter Highscore bzw. Eroberungen könnte sicherlich auch anderen Spielen guttun. Es bleibt abzuwarten wie sich die Motivation über einen längeren Zeitraum entwickelt, doch zu [empfehlen ist Placescore auf alle Fälle](https://itunes.apple.com/de/app/placescore/id585262289).