---
layout: post
title: "Das Du"
date: 2014-09-20
category: blog
tags: [Medien]
published: true
comments: false
share: true
---

Im NDR lief am 11.03.2014 eine Reportage über das Dänische Bettenlager. Im Kern ging es darum, dass das Unternehmen in Dänemark hoch angesehen ist, aber bspw. in Deutschland seine Mitarbeiter nicht so gut behandeln soll. Mir geht es gar nicht um die Vorwürfe, sondern um eine ziemlich wirre Aussage eines ehemaligen Abteilungsleiters und ein Grund, wieso die skandinavischen Länder toll sind.

### Das böse Du

Misstrauisch hinterfragt die Reportage die Verwendung des *Du* in allen Hierarchien des Dänischen Bettenlagers. Chefs werden von jedem Mitarbeiter geduzt, sogar von Auszubildenden! Da muss doch etwas faul sein? Der ehemalige Abteilungsleiter Peter Wegner beschreibt es so:

> Das Du ist eine gefährliche Sache. Das Du ist sehr gezielt eingesetzt worden in den Filialen und in der Zentrale um die Leute unter Druck zu setzen. 

Und ein Beispiel hinterher:

> Wenn eine Verkäuferin einen freien Tag haben wollte, dann wurde ihr mit dem Du deutlich gemacht &quot;Du Maria, das geht jetzt nicht, kannst du deinen freien Tag nicht verschieben? Wir haben hier und da noch was zu tun. Das geht nicht.&quot;

Und die Sprecherin im Off schlussfolgert:

> Das Du ist offensichtlich ein betriebswirtschaftliches Instrument. 

Selbst wenn man nicht weiß, welche Bedeutung dieses ominöse und von allen benutzte *Du* in den skandinavischen Ländern hat, müssten doch bei solchen Aussagen die Alarmglocken klingeln. Ich sehe nicht andeutungsweise in dem Beispiel wie das *Du* den Druck erzeugt haben soll, wenn überhaupt Druck da gewesen ist. Die ganze Aussage ist völlig wirr und zeugt davon, dass der ehemalige Abteilungsleiter scheinbar die skandinavische Mentalität nicht begriffen hat.

### Das Du in Skandinavien und bei uns

Warum ist diese Aussage völliger Blödsinn? Weil das *Du* in Skandinavien Normalität ist. Die Menschen duzen sich, gesiezt wird **nur** die Königsfamilie. Das ist für Deutsche vielleicht schwer zu verstehen. Und für Redakteure des NDR und ehemalige Abteilungsleiter des Dänischen Bettenlagers sowieso. Aber es ist die Realität in diesen Ländern. Daraus abzuleiten, dass firmenintern Druck erzeugt werden soll, ist nicht nur lächerlich sondern schädigt die gesamte Reportage. 

Das herzliche und offene *Du* in Skandinavien ist ein wichtiger Wert für die Gesellschaft und das Miteinander. Und das betrifft nicht nur Skandinavien, sondern auch andere Länder. Oder kann mir jemand verraten, wie sich englisch-sprechende Menschen siezen?      
Unser *Sie* erzeugt stets eine Barriere und verkompliziert das Miteinander nur unnötig. Wann sagt man zu wem *Sie* und wer erwartet das *Sie* und wer nicht? Einige mögen einwenden, es ist eine Frage des Respekts. Doch kann man Respekt auch anders zeigen. In anderen Länder funktioniert es ja auch.    

Wie komisch sich das *Sie* anfühlen kann, habe ich vor einigen Jahren selbst erlebt. Es war definitiv nach meinem Austauschjahr in Norwegen und ich war mit meiner damaligen Freundin schon seit über zwei Jahren zusammen. Als wir mal wieder bei ihr waren, kam ihre Mutter auf mich zu und verkündete stolz, dass ich sie jetzt *Duzen* darf. Ich weiß nicht ob sie meinen verwirrten Blick bemerkt hat. Zu ihrer Verteidigung muss man dazu sagen, dass sie eine Richterin ist und den ganzen Tag lang nur gesiezt wird. 

Für mich war das *Du* in Norwegen eines der tollsten Aspekte. Und vielleicht war es für mich als Austauschschüler von besonderer Bedeutung? Wenn man schon der *Neue* ist und nicht einmal die Sprache der Mitmenschen richtig beherrscht, hilft es auch nicht durch die Sprache und einem unsinnigen *Sie* weitere Barrieren aufzubauen. 

Dass das tolle *Du* als ominöses, betriebswirtschaftliches Instrument abgestempelt wird, stimmt mich traurig. 