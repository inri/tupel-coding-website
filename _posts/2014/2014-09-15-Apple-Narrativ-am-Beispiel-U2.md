---
layout: post
title: "Apple Narrativ am Beispiel U2"
date: 2014-09-15
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Da nimmt ein Konzern viel Geld in die Hand, kauft die Exklusivrechte für das Album einer bekannten Rockband und verschenkt dieses Album für einige Wochen an die eigenen Kunden. 

### Frage: Ist das etwas positives oder negatives?

Wenn es nach den Medien geht, dann etwas negatives. Das verwundert nicht besonders, denn der Konzern ist Apple und seit einigen Jahren hat sich ein Narrativ gebildet, der jede Meldung über Apple in ein negatives Licht rückt. 

Noch einmal ausführlich: Apple hat für **100 Millionen Dollar** die Exklusivrechte an dem neuen Album von **U2** gekauft und verschenkt es bis Oktober an ihre **500 Millionen Nutzer** mit Apple ID.      
Statt diese Geste zu loben und zu würdigen, liest man darüber dass sich einige Nutzer nicht darüber freuen und sich bei Apple beschweren, weil das Album automatisch ihrem iTunes hinzugefügt wurde.  

Ein weiteres Mal: Da verschenkt ein Konzern ein neues Album einer weltweit berühmten und anerkannten Band und einige Medien meinen, dass man über die wenigen kritischen Stimmen berichten sollte? 

Mir wären auch die Foo Fighters lieber gewesen und vielleicht hat der automatische Download des Albums auf iPhone und iPad am monatlichen Web-Kontingent einiger Nutzer genagt. Aber das ist doch lange kein Grund einem geschenkten Gaul ins Maul zu schauen oder gar in den Medien von der Kritik zu berichten. 

Doch so funktioniert der aktuelle Narrativ über Apple. Keine Innovationen mehr, enttäuschte Börsenanalysten, der drohende Untergang ohne Steve Jobs. Die Liste ist so lang wie bescheuert.