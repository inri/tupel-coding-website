---
layout: post
title: "Detailliebe in The Walking Dead"
date: 2014-10-16
category: medien
tags: [Serie, The Walking Dead]
published: true
comments: false
share: true
image: 
  feature: 2014-The-Walking-Dead-Zombies-lg.jpg 
  credit: AMC
  creditlink: http://www.amc.com
---

Seit Anfang dieser Woche gibt es die 5. Staffel der grandiosen Serie *The Walking Dead* zu sehen. Es war eine sehr gelungene Auftakt-Episode und besonders die Nachbesprechung, *The Talking Dead*, war interessant.     
  


Ein Aspekt, den ich an *The Walking Dead* beachtlich finde, ist die hohe Qualität der gesamten Serie. Das betrifft nicht nur die schauspielerische Leistung und das gute Storytelling, sondern auch die Kostüme. Die Zombies sehen wirklich lebensecht aus und besonders im Vergleich zu ihren Kollegen in anderen Zombiefilmen  wesentlich besser.

Die Geschichte befindet sich nun in der 5. Staffel und im zweiten oder dritten Jahr nach Ausbruch der Katastrophe. Auch wenn nicht jede Frage (z.b. &quot;*Wie können sich verwesende Muskeln und Sehen bewegen?*&quot;) beantwortet werden muss, sollen die Zombies trotzdem gut aussehen. Verantwortlich dafür ist Greg Nicotero, der nicht nur tolle Masken für die Serie entwickelt, sondern auch eine Zombieschule ins Leben gerufen hat.     
In *The Talking Dead* hat er auch davon berichtet, dass sie die Zombiemasken mit jeder Staffel weiterentwickeln, mehr und mehr verwesen lassen. Sie sind sich dem Zeitverlauf bewusst und passen die Masken an. Es gibt z.b. mehr Zombies ohne Nasen.

Das ist einfach Detailliebe und einer von vielen Gründen für die Beliebtheit der Serie.     

Sehr enttäuscht war ich von der neuen Zombieserie *Z Nation*. Die ganze Story und Aufmachung ist mau, aber auch die Zombies sehen einfach nicht überzeugend aus. Alles wirkt billig und so verlor ich sofort das Interesse. Für einen Artikel reicht es vielleicht noch.








