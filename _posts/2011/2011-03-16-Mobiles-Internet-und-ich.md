---
layout: post
title: "Mobiles Internet und ich"
description: "Wie und seit wann ich mobiles Internet nutze."
date: 2011-03-16
tags: [Technologie]
published: true
category: it
comments: false
share: true
---

Erst seit einem Jahr schwimme ich im See des mobilen Internets. Vorher hatte ich ein untaugliches Nokia-Smartphone und auch erst Anfang 2010 schien mir ein guter Zeitpunkt zu sein, mit diesem Quatsch anzufangen.       

Viele Menschen in Deutschland sind sicherlich durch das iPhone in die damals noch unentdeckten und neuen Fluten des mobilen Internets gestoßen worden. Der Anfang liegt sicherlich um 2008 rum, wobei das mobile Internet damals noch teuer und eher exklusiv war [^1]. Ich fristete in diesem Jahr mein Dasein als Abiturient und Zivi, hatte also anfangs kein Geld und später so viel Geld, dass ich Führerschein und Laptop kaufte - keine Gedanken an teuren Datentarife.

Auch war damals das Angebot nicht berauschend. Das ganze UMTS war langsam, die Smartphones ebenfalls und deren Bedienung bestenfalls bescheiden. Für dieses langsame Internet musste man tief in die Tasche greifen und mir erschloss sich damals einfach nicht der Sinn. Aber Gottseidank gab es viele Nerds, die das mobile Internet nutzten und wie [Sascha Lobo](http://www.spiegel.de/netzwelt/web/0,1518,744352,00.html) es schon beschrieb, verdanken wir ihnen vieles.  
    
Nun gab es 2010 bzw. schon ein wenig eher, endlich günstige Datentarife, die Geschwindigkeiten sind auch gestiegen und damit sind auch die Smartphones gemeint. Ich bin zwar immer noch unzufrieden mit dem mobilen Internet, aber das könnte an meinem EPlus-Netz liegen.

Seit einem Jahr surfe ich mit meinem HTC Desire im Internet rum, lade mir Apps herunter, bekomme regelmäßig Wetterdaten und neue E-Mails. Mein mobiles Leben ist sehr bequem geworden. Dank Twitter erhalte ich auch interessante Neuigkeiten schnell und problemlos auf mein Handy und kann mich mitteilen, ein sozialer Gewinn sozusagen.     
Trotzdem ist meine *Internetzentrale* immer noch meine Wohnheimwohnung in Potsdam. Hier halte ich mich den Großteil eines Tages auf und Internet ist in Form von Rechner und MacBook leicht erreichbar. Das mobile Netz ist also eine Art Erweiterung meiner *Internetzentrale* und gerade deshalb auch unverzichtbar geworden.

Besonders deutlich wurde mir das in den letzten Tagen. Wir waren in Rostock und Hamburg unterwegs, haben bei Freunden übernachtet und es gab kein WLAN oder wenn doch, funktionierte es nicht. Mein Desire fungierte in vielerlei Hinsicht als unverzichtbares Gerät für mobiles Netz. Es war das Tor zur Welt und zu all den interessanten Fakten (bspw. dem Kernreaktorunglück in Japan).     

Aber auch das zeitnahe Antworten auf Emails via Smartphone nutzte ich sonst nie, wieso auch? Meine Wohnung ist nah genug. Einige relativ wichtige E-Emails bekam ich und konnte sofort reagieren. So z.b. noch während der Bahnfahrt nach der Adresse unserer Schlafunterkunft in Hamburg fragen und die Antwort auch in der Tat zu lesen. Ja, SMS gibt es auch noch, aber die bezahle ich zum einen und zum anderen ist es für den Angeschriebenen leichter, viele Infos in die Tastatur anstatt ins Handy zu tippen.

Besonders witzig war auch folgende Szene. Die Gastgeberin wollte uns in der Stadt ein Geschäft zeigen, wir verlaufen uns dabei. Sie fragt eine Eingeborene aus einem Geschäft und diese entpuppt sich als unwissende Verkäuferin. Während dessen hatte ich GoogleMaps schon beauftragt und eine Route zum gesuchten Geschäft zur Hand. Schnell und einfach.      
Generell half das Desire sehr bei der Orientierung und dem Auffinden von Straßen, Geschäften und Orten. Auch um mal abzuschätzen, wie weit es nun bis dorthin oder hierhin ist und ob sich der Weg lohnt.

Ich sehe momentan nur zwei Schwächen und diese sind nicht furchtbar tragisch, aber existent.    

**Nummer 1:** Das Netz ist nicht gut genug ausgebaut. In Potsdam gefällt mir die Geschwindigkeit nicht sehr gut, in Rostock war sie auch nur mittelprächtig, in Hamburg besser. Das deckt sich mit der EPlus-Abdeckung, also keine Überraschung. Für ein datenhungrigeres Gerät werde ich vermutlich mal zur Telekom wechseln.     

Die Geschwindigkeiten sollten in den nächsten Jahren wirklich verbessert werden und das vor allem auch in großstadtfernen Gegenden. Ach ja, bitte nicht auf Kosten der Kunden. Ich bin nicht bereit für läppische 1 GB Datenvolumen in Monat den gleichen Betrag wie für meinen Festnetzanschluss zu bezahlen.

**Nummer 2:** Akku! Wenn ich mit meinem Desire spiele oder das Internet nutze (besonders GPS + GoogleMaps) geht der Akku rasant zur Neige. Ich habe es nicht getestet, aber einen 6-Stunden-City-Trip mit durchgängig GPS und mobilen Internet hält das Desire nicht aus. Dabei hat das Desire einen ganz guten Akku. Das scheint eher ein technisches Problem zu sein, oder gibt es ein Super-Akku-Phone irgendwo?     
Hier in Potsdam bin ich immer in Reichweite meiner Docking Station, bemerke also den Akkuverbrauch nicht wirklich.

Wenn irgendwann in den nächsten Jahren die Mobiltelefone eine ordentlicheLaufzeit haben und man flächendeckend mit vernünftiger Geschwindigkeit im mobilen Netz surfen kann,... ja, dann werde ich vermutlich irgendwas anderes vermissen. Aber so ist es ja immer. Im Vergleich zu 2008 ist in den drei Jahren vieles besser geworden (schnellere Smartphones und bezahlbare Tarife). Vielleicht nähern wir uns in weiteren drei Jahren einem Optimum.

[^1]: Aha, ein Blick in [Wikipedia](http://de.wikipedia.org/wiki/Mobiles_Internet#Entwicklung_und_Situation_in_Deutschland) bestätigt meine Vermutung, im Jahr 2008 nutzten 62% der Deutschen ein Smartphone davon 13% das mobile Netz - also insgesamt ca. 8%, wenn ich mich nicht verrechnet habe. Das ist irgendwie so gut wie nichts.