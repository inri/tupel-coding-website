---
layout: post
title: "Prezi statt Powerpoint"
description: "Ein kleiner Überblick zum Powerpoint-Konkurrenten Prezi."
date: 2011-09-07
tags: [Web, Design]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-prezi-lg.jpg
  credit: Prezi
  creditlink: http://www.prezi.com
---

Für meinen letzten Vortrag habe ich Prezi verwendet. Einfach ausgedrückt ist Prezi wie Powerpoint, nur moderner. 


Man kann mit Prezi Präsentationen erstellen und das besondere ist dabei die Technik des Zoomens. Prezi wird auf der Website auch als *The Zooming Presentation Editor* beschrieben und das ist sehr passend.       
Am besten begreift man das Prinzip, wenn man sich einige Präsentationen anschaut. Free-User müssen ihre Präsentationen veröffentlichen und können sie nicht als privat deklarieren, daher finden sich viele Beispiele auf der Prezi-Seite. Mehr zu den Preismodellen später.

Im Prinzip hat man in Prezi eine riesige Leinwand und schmeißt dort alles rauf, was man zeigen möchte. Das Zoomen in dieser Leinwand gibt einem die Möglichkeit, gewisse Dynamiken aufzubauen und Informationen zu schachteln und anzuordnen. So kann im Kreis eines O wieder ein ganzer Satz oder ein Bild oder sonst was sein. Und darin wieder etwas anders, noch kleiner.

Ich möchte besonders betonen, dass die Entwickler mittlerweile ihre Tools mit richtig fantastischen Features erweitert haben. Anfangs konnte man Text darstellen, einige Rahmen ziehen und hatte eine Hand voll vorgefertigter Stile. Mittlerweile gibt es Pfeile und Striche, mit denen man Beziehungen zwischen den Informationen darstellen kann. Stile kann man sich nun selbst auswählen und jede Schrift- und Rahmenfarbe bestimmen. Daneben wurden einige nützliche Änderungen vorgenommen: z.b. dass man nun Rahmen samt Inhalt verschieben und vergrößern kann. Das war früher sehr umständlich.

Das Erstellen einer Präsentation geschieht in zwei wichtigen Schritten. Erstens müssen die Informationen angeordnet werden und im zweiten Schritt legt man fest, in welcher Reihenfolge die Informationen gezeigt werde. Die Elemente werden dabei pfadartig verbunden. Im Bearbeitungsmodus sieht man sehr gut, wie durcheinander die Pfade über die gesamte Präsentation verlaufen.

Prezi ist mittlerweile ein richtig tolles und mächtiges Werkzeug für das Erstellen von Präsentationen geworden. Die Prezis sind entweder online anschaubar, oder man lädt sie sich herunter und spielt sie mit Flash offline ab. Man kann sich auch PDFs daraus generieren lassen, was aber teilweise zu großen Redundanzen führt, weil eben einige Ansichten fast identisch sind.      
Auch zu erwähnen ist die iPad App, mit der man aber die Prezis nicht bearbeiten kann, sondern nur zeigen. Hoffentlich ermöglichen die Entwickler später auch das Bearbeiten am Touchscreen.

Prezi kann man kostenlos benutzen. Dabei hat man aber nur 100 MB Speicher für seine Projekte und kann sie nicht als privat kennzeichnen. Im Enjoy-Preismodell (60$ / Jahr) hat man schon 500 MB Speicher und darf auch private Prezis erstellen. Und beim Pro-Modell (160$ / Jahr) bekommt man 2 GB Speicher und ein Prezi-offline Programm. Für Studenten empfehle ich die Edu-Lizenzen, bei der man entweder das Enjoy kostenlos bekommt oder mit Edu Pro alle Vorteile für nur 60$ / Jahr.

Ich kann Prezi nur wärmstens empfehlen. Es bietet viele neue Möglichkeiten Informationen interessant und spannend für den Zuschauer darzustellen. Probiert es aus!