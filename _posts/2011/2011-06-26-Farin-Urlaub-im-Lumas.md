---
layout: post
title: "Farin Urlaub im Lumas"
description: "Wir waren zu Gast bei Farin Urlaubs Vernissage in der Lumas Galerie in Berlin."
date: 2011-06-26
tags: [Kunst, Fotografie]
published: true
category: blog
comments: false
share: true
---

Vor einigen Wochen bekam ich eine Einladung zur Vernissage von Farin Urlaubs Fotos in der Lumas Galerie in den Hackerschen Höfen. Verantwortlich war meine Anmeldung beim Newsletter der Galerie, denn ich las von Farins Ausstellung dort und fand die ganze Kunstsache interessant. 


Nun lag überraschenderweise eine Einladung im Briefkasten und ich dachte anfangs, sie käme vom die ärzte Fanclub. Die hatten ein Gewinnspiel veranstaltet und Karten für die Vernissage verlost. Ich nahm nicht teil, hatte aber anscheinend trotzdem Glück, dank Lumas.

Ich wusste nicht, dass viele andere auch Einladungen bekamen und einen Tag vorher kam sogar Newsletter eine weitere Einladungen von Lumas (man solle der Security &quot;*Bambus*&quot; ins Ohr flüstern). Nun stellte ich mich auf eine volle Ausstellung ein und das erst Recht, weil der Künstler höchstpersönlich anwesend sein sollte!

Wir fuhren mit der S-Bahn direkt hin und schauten uns erstmal die Hackeschen Höfe an. Eine richtige Kunstszene existiert dort. Das war mir bisher neu, doch man sollte das auf jeden Fall mit mehr Zeit erkunden. Die Galerie fand sich recht schnell und sie war auch schon relativ voll.    
Wir schlenderten in einen vielleicht vierzig Quadratmeter großen Nebenraum und schauten uns Farin Fotos an, einige Nahaufnahmen von Bambus und Oktopussen. In der einen Ecke stand ein kleines Mischpult und zwei Mikrofone. Es war kurz vor 19 Uhr, offiziell sollte die Vernissage genau dann beginnen. Es wurde innerhalb von den wenigen Minuten, die wir dort in dem Nebenraum standen auch merklich voller. 

Wenn Farin tatsächlich vorbeikommt, ein wenig skeptisch war ich noch, dann wird er ja wohl auf dieser &quot;Bühne&quot; einige Worte sagen. Wir blieben also stehen und warteten mit den anderen. Es wurde wärmer und auch voller und so langsam die Massen ungeduldig, doch auf einmal lief Farin tatsächlich am Fenster vorbei, schaute kurz rein und grinste. Wenige Minuten später bahnte er sich zusammen mit der Galeristin den Weg durch die Massen und stellte sich wie erwartet vor das Mikro.

Farin Urlaub höchstpersönlich stand zwei Meter vor uns! Die Galeristin erzählte etwas über Farin und führte danach noch ein kurzes Interview mit ihm. Er scherzte viel, lachte und war ziemlich gut drauf.

Nun lasse ich den Plural mal links liegen und spreche aus meiner Fan-Seele: Als er zum Mikrofon ging, musste er an uns vorbei und in dem Moment hatte ich durchaus weiche Knie. Er ist ein Stück größer als ich, aber nicht ganz so groß, wie ich es erwartet habe. Und alt ist er geworden, hehe. Er kam am Mittwoch aus dem Urlaub und hatte nun bestimmt noch seine originale Haarfarbe. Die Seiten sind bereits ergraut, aber mit Mitte vierzig auch nicht verwunderlich. Wie man auf den Fotos erahnen kann, war Farin auch sehr locker gekleidet. Zu dem grauen Langarmshirt und den sehr lockeren dunklen Hose, hatte er weiße Turnschuhe an. Auf seiner eigenen Vernissage! Das nenne ich Punkrock.

Nachdem die Vernissage eröffnet wurde, wollte er ein wenig durch die Galerie schlendern und Fragen beantworten, schaffte es jedoch nur zwei Meter in die Masse und stand neben uns. Dort wurde er sofort von den anderen Fans belagert, die CDs, Poster und Bücher zum Signieren dabei hatten. Ich war nicht ganz sicher wie er darauf reagieren würde, schließlich ging es um seine Fotos und nicht seine Musik. Aber er war völlig cool, lächelte und scherzte mit allen. Sehr sympathisch eben!

Wie man sehen kann, habe ich nur mit meiner Smartphone-Kamera Fotos und Videos gemacht. Natürlich ärgere ich mich ein wenig, dass ich die Lumix nicht mitgenommen habe. Andererseits ist es eine Vernissage gewesen, also die Fotos eines anderen anschauen und nicht wie verrückt Fotos vom Fotografen machen. Teils war es ein wenig nervig, was die anderen Gäste zusammenklickten.

Wir standen noch eine Weile neben Farin und ich freute mich einfach nur, wusste allerdings nicht, weswegen ich ihm hätte ansprechen sollen und zum Signieren hatte ich nichts mit. Es war weiterhin voll und wir kamen nur langsam aus dem Pulk von Fans heraus. Der Rest der Galerie war etwas leerer. Wir holten uns noch Sekt für den nach Hause Weg und gingen. Draußen wartete noch eine lange Schlange von Fans. 

Es war ein überraschend guter Abend und ich habe einen meiner Lieblingsmusiker richtig in echt und in Farbe gesehen!