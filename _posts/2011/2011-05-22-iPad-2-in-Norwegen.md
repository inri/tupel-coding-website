---
layout: post
title: "iPad 2 in Norwegen"
description: "Was ich mit meinem neuen iPad 2 in Norwegen so gemacht habe."
date: 2011-05-22
tags: [iPad]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-norwegen-lg.jpg
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Wen interessiert schon ein Urlaubs-Erfahrungsbericht mit einem iPad, wenn dieser sechs Wochen zu spät kommt? Er sollte trotzdem aufgeschrieben werden.     
In der ersten Aprilwoche war ich in Norwegen Angeln. Gefangen habe ich nichts, aber ein iPad hatte ich dabei, Glück gehabt! Die Ausgangssituation sah also folgendermaßen aus: acht Tage Urlaub, ein iPad 2 und kein Internet.

Mittlerweile habe ich viel zu viele Spiele auf meinem iPad, aber vor sechs Wochen fanden sich vorerst nur Angry Birds, Zombie vs. Plants, Field Runner und einige weniger schöne Games. Gespielt hat meistens mein Vater und ich glaube sogar, er hat Zombie vs.Plants tatsächlich durchgespielt. Zumindest war er begeistert und vermisst seit dem beim Spielen auf seinem Desire einen 10 Zoll Bildschirm.

Ich habe das iPad die meiste Zeit zum Lesen genutzt. PDFs lassen sich blitzschnell durchblättern, beliebig zoomen und vor allem in großer Stückzahl transportieren. Auch für Vorlesungsfolien ist das iPad-Format sehr gut geeignet. Am besten lesen sich aber die iPad-freundlichen Epub-Dateien. Dabei skaliert das Endgerät den Inhalt des Epubs entsprechend der Bildschirmgröße und das sieht auf dem iPad eben sehr komfortabel aus.

PDFs dagegen richten sich größtenteils ans A4-Format mit Rand, aber diesen braucht man eigentlich nicht oder zumindest reichen 5 mm aus. Sehr gut lesbar ist dabei alles. Ich verstehe, dass es keinen Spaß macht PDFs bzw. generell Bücher am normalen PC-Bildschirm zu lesen, oder selbst auf einem Notebook. Entweder sind die Buchstaben fest angeordnet (eben dort wo der Monitor steht) oder man hat noch 2+ kg Gewicht am Bildschirm hängen.     
Das iPad ist dagegen wunderbar leicht und ich kann es drehen und wenden, kippen und legen wo und wie ich es möchte. Auch mit einer Hand halten und liegend über das Gesicht haltend geht ganz gut. Selbst Taschenbücher werden bei diesem Gebrauch nach kurzer Zeit schwer. Und der Nachteil der Bücher: Ich muss meistens mit beiden Händen das Buch aufhalten.

Der Bildschirm macht einen guten Job. Meine Augen brenne auch nach einer Stunde Lesen nicht und ich werde auch nicht schneller müde. Wer da immer noch an der neuen Technik rummäkelt: Fahrt doch wieder mit der Pferdekutsche!      
Der Bildschirm des Kindle liest sich vielleicht besser, aber mit diesem kann ich auch nur lesen, zu etwas anderem taugt er nicht. Ich hege die Hoffnung, dass es in naher Zukunft technisch möglich ist, den Bildschirm auf unterschiedliche Wiedergabe-Modi zu stellen. Das Spart Strom und ist dann endgültig der Todesstoß für Bücher.

Ansonsten habe ich fleißig Bilder auf das iPad geladen. Also richtig aktuelle Bilder - vor zehn Minuten noch mit dem Boot übers Wasser geflitzt, nun die Landschaftsfotos begutachten. Dabei kann man mit einem Laptop sicherlich schneller Bilder aussortieren, aber das Vorzeige-Potential ist leider sehr gering. Das iPad lässt sich schnell herumgeben und jeder wischt sich durch die Galerie. Wunderbar.

Praktisches Helferlein war die Dropbox. Internet gab es zwar im Urlaub nicht, aber im Vorfeld habe ich einfach die Dateien, die ich gebrauchen könnte vorgeladen und dann war alles in einer übersichtlichen Ordnerstruktur. Für Zwischendurch war GarageBand genial. Ich habe zwar keine musikalisch-wertvollen Lieder gebastelt, aber viel Spaß gehabt.

## Fazit

Es gibt noch keine gute Angler-App. Auf das Internet kann man für eine Woche mal verzichten, aber länger muss es doch nicht sein. Einige Apps sind leider völlig nutzlos ohne Netz und in diesen Momenten vermisst man es sehr. Das iPad ist ein super Reisebegleiter, weil es viele Dinge vereint.