---
layout: post
title: "Paper accepted"
description: "Unser Paper wurde zur AGTIVE Konferenz akzeptiert."
date: 2011-08-03
tags: [HPI, Uni]
published: true
category: it
comments: false
share: true
---

Am [Lehrstuhl für Systemanalyse und Modellierung](http://www.hpi.uni-potsdam.de/giese/home.html) bin ich seit drei Semestern als Hiwi tätig. Unsere Forschungsgruppe KorMoran (= **Kor**rekte **Mo**dellt**ran**sformation) hat vor einigen Wochen ein Paper über automatische Conformance-Tests von optimierten Triple Graph Grammatiken eingereicht. Nun bekamen wir die freudige Nachricht, dass unser Paper für die AGTIVE Konferenz akzeptiert wurde.

Im Oktober stellen unsere *Chefs* auf der Konferenz in Budapest das Paper vor und bis dahin wird es noch überarbeitet. Es wurde sogar von *Springer Press Lecture Notes in Computer Science* veröffentlicht. Alles sehr spannend.

Wer Interesse hat, kann sich das Paper gerne anschauen: [Link](http://link.springer.com/chapter/10.1007%2F978-3-642-34176-2_20) zum Springer-Artikel.
