---
layout: post
title: "Gegenmittel für Androids Fragmentierung"
date: 2011-05-16
tags: [Android]
published: true
category: it
comments: false
share: true
---

Gestern startete die I/O Konferenz von Google und brachte natürlich viele interessante und sogar gute Neuigkeiten. Wer diesen Blog ab und zu liest, weiß vielleicht sogar, was ich meine.

## Die Richtlinie gegen Fragmentierung!

Auch bei Google hat man gemerkt, dass diese elende Fragmentierung ihrer einzelnen Android-Versionen ziemlich scheiße ist. Vor einem halben Jahr erschien die momentan aktuelle Version von Android (2.3.3) für Smartphones und die ist bisher auf nur 4% aller Androiden vertreten. Auf meinem Desire nicht. Und das ist einfach nur richtig Mist.

Höchste Zeit dieses Problem anzugehen! Dazu hat Google eine Richtlinie mit den großen Hardwareherstellern und den Mobilfunkanbietern ausgehandelt. Diese garantieren 18 Monate lang aktuelle Updates für ihre Android-Geräte. Das war es aber irgendwie auch schon mit den Infos darüber. Wann die Updates kommen sollen (z.b. max. 4 Wochen nach Erscheinen) oder ob es nach den 18 Monaten (ein Mobilfunkvertrag läuft regulär 24 Monate) weitere Optionen gibt, ist leider nicht bekannt. Ich hoffe aber, dass es diesbezüglich gute Nachrichten geben wird.

Ansonsten soll die kommende Android-Version (Ende des Jahres) Smartphones und Tablets zusammenführen. Schön. Auch sehr interessant finde ich die Implementierung von Arduino - damit kann man Roboter, Kühlschränke und sonstige technische Geräte steuern und programmieren. Google möchte gerne ein vollständig vernetzte Umgebung schaffen, sehr coole Idee. Außerdem ein guter Markt außerhalb des Apple-Territoriums.

Ich bin gespannt welche Neuigkeiten es noch geben wird. In Sachen Tablets ist es ruhiger geworden. Das iPad 2 verkauft sich seit knapp zwei Monaten wie geschnittenes Brot, von der Konkurrenz hört man dagegen nicht so viel.      
Das Xoom ist völlig untergegangen und ansonsten wartet man auf die Verfügbarkeit weiterer Android Tablets. Samsung bringt wohl langsam sein 10.1V heraus, also das ist die erste, dickere und schwerere Version des 10.1er. Irgendwann im Juni soll dann das *nach-dem-iPad-2-neu-designte* 10.1 erscheinen, man ist gespannt. Die Entwickler auf der I/O haben welche geschenkt bekommen.

Ich sehe es ja schon kommen. Anfang Juni soll es losgehen mit dem 10.1er und genau dann stellt Apple das iPhone 5 vor. *Show-steeling deluxe.*