---
layout: post
title: "Strategen und Unsinn"
description: "Es ist manchmal erstaunlich wie viel Blödsinn Starteten von sich geben können."
date: 2011-12-07
tags: [Microsoft]
published: true
category: it
comments: false
share: true
---

Ich konsumiere momentan zwar diverse Nachrichten und schaffe es gerade noch über sie nachzudenken, aber zum Diskutieren komme ich zeitlich nicht. Schade. 


Aktuell plätschert es im Großkonzernbereich. Apple ist weiterhin unglaublich erfolgreich, bei Smartphones, wie auch Computer. Google wird für einige Entscheidungen kritisiert und angezweifelt (Google+, RSS Reader,...). Microsoft experimentiert viel und ist in Sachen Kinect erfolgreicher als in Sachen Smartphones. Und zwischen diesen dreien gibt es diverse Überschneidungen.      

Ursprünglich wollte ich zu verschiedenen Dingen jeweils Meinungen abgeben, aber nach der ersten Meinung (die folgende), schrieb ich sehr viel zum zweiten Punkt und das wurde ein eigenständiger Artikel, der bald fertig und veröffentlicht wird. Hier also nun meine Meinung zu der Meinung eines Strategen, von Windows.

## R.I.P. Windows Phone

Microsofts Chef-Stratege Mundie lästert über Apple und Siri, was ja keine Neuerung sei, Windows Phone seit einem Jahr habe und die guten Verkäufe nur ein Marketing-Erfolg. Mein erster Gedanke: Da beschwert sich der richtige (Ironie!). Mein zweiter Gedanke: Fühle ich mich persönlich angegriffen? So als stünde die Anschuldigung im Raum, ich hätte mich von Apples Marketing um den Finger wickeln lassen und mit mir auch viele andere Millionen Menschen.     
Ich musste dabei an die Situation auf dem PC-Markt denken. Vor allem wie sie in den letzten 15 bis 20 Jahren war. 

Als Privatanwender konnte man zwischen drei Betriebssystemen wählen. Da gab es zum einen das teure System von Apple. Damit hatte man Probleme mit Programmen, denn die Auswahl war nicht sehr groß und die Computer waren eben teuer. Die günstige Alternative war Linux. Das Problem bei Linux waren und sind zum einen die Bedienung und das notwendige Expertenwissen, denn man möchte einen Computer und kein Hobby. Sicherlich hat sich in beiden Bereichen viel zum Positiven geändert, aber grundlegend war und ist das einfach eine Hürde für einen normalen Benutzer.

Außerdem gab es für Linux ein großes Problem: Windows. Ich würde sagen, dass mindestens 80% aller Computer nur mit Windows verkauft wurden und werden. Die Zahl sinkt zwar, aber besonders in der Vergangenheit war das einfach so. Linux war im Technikhandel kaum anzutreffen und die Apple Computer waren erheblich teurer als die Windows Computer und auch nicht so flächendeckend zu erhalten.     
Ich möchte auf die Tatsache hinaus, dass man als privater Anwender mit wenig oder gar keinem IT-Wissen einfach nicht an Windows vorbeikam. Und genau das ist Marketing, wenn nicht sogar eine Form der Erpressung. Deshalb finde ich es umso erstaunlicher, von einem Microsoft-Mitarbeiter (Strategen!) solche Worte zu hören. Solch dumme Worte.

Herr Mundie scheint den Unterschied nicht begriffen zu haben. iPhones sind nicht die billigsten Geräte auf dem Markt, und trotz Alternativen wie Android und Co. entscheiden sich sehr viele Menschen für dieses Gerät. Heutzutage habe ich eine Wahl, eine richtige Wahl. Ich will damit nicht sagen, dass eine Auswahl zwischen zwei großen Systemen perfekt ist. Aber es ist mehr Auswahl, als zu Hochzeiten von Windows.

Der Kommentar von Mundie spiegelt auch wieder, dass er nicht versteht wieso sich Windows Phone so schlecht verkauft. Sie hätten das Sprach-Feature bei Windows-Phone ja schon längst. Ich bin mir sicher, selbst wenn Windows Phone noch 10 tolle Features hätte, würden sich die Verkaufszahlen nicht ändern. Es tauchte kurz nach Mundies Aussage ein Video auf, welches eindrucksvoll bewies, dass Windows Phone eben kein Siri hat, sondern nur eine tröge, schlechte Sprachsteuerung. Die konnte keinen einzigen Befehl ausführen, hat einfach nur bei Bing nach den gesagten Worten gesucht.

## Neuer Markt ohne Windows?

Mein Vater ist der Meinung, da gebe ich ihm Recht, dass abgesehen vom viel zu spätem Auftauchen von Windows Phone 7 und anderen grundlegenden Hindernissen, viele Kunden sich einfach nicht noch einmal an ein Windows-System binden wollen.
Windows ist ein gutes Betriebssystem und viel Kritik erhielt es ungerechtfertigt. Aber den Ärger vieler Benutzer, kein anderes System nutzen zu können, welches nicht teurer oder ein Informatikstudium voraussetzt, muss Microsoft nun einstecken. 

Wie sehr es ihre Schuld ist, darüber kann man spekulieren. Sie hatten und haben gute Jahre im Desktop-Bereich und sollten sich lieber weiter dort konzentrieren, anstatt im Smartphone-Markt rumzupfuschen.    
Andererseits stellt sich die Frage, ob wir über Smartphones zu den Tablets kommen und damit ein wirklich großer Markt geschaffen wird, der die herkömmlichen PCs langsam verdrängt? Im Business-Bereich mag das zwar eher unwahrscheinlich sein, denn da wird Rechenkraft benötigt, aber kann das für Windows genug sein? Den Consumer-Markt Apple und Android überlassen? 
Vermutlich nicht und deshalb wird Windows weiter fleißig an Windows 8 arbeiten.