---
layout: post
title: "Google+, das neue Facebook?"
description: "Ein Vergleich zwischen Facebook und dem neuen Google+ Netzwerk."
date: 2011-07-05
tags: [Google, Facebook]
published: true
category: it
comments: false
share: true
---

Einige Tage ist das neue soziale Netzwerk nun online und schon kann man erste Meinungen abgeben. Die Technikblogger von [PCWorld haben dies getan](http://www.pcworld.com/article/234825/9_reasons_to_switch_from_facebook_to_google.html) und ich übertrage ihre Erkenntnisse mit meinen Ergänzungen ins Deutsche. 


### 1. Google Services

Google+ integriert sich nahtlos in die vielen anderen Services, die Google anbietet. Allem voran wäre da natürlich GoogleMail zu nennen. So wurden mir meine Mail-Kontakte für Circles vorgeschlagen, sehr praktisch. Wenn ich mal meine Kontakte vernünftig sortiert habe, werden sie auch in Google+ geworfen. Wünschenswert wäre noch Twitter, kommt aber bestimmt bald. Spätestens, wenn Google die API für Google+ veröffentlicht.

### 2. Google ist vertrauenswürdiger

Das hat eine Ursache: Google ist älter als Facebook. In meinem letzten Artikel habe ich das auch schon angesprochen. Die eine Firma existiert seit Jahrzehnten und hat sich langsam zu etwas sehr großem mit vielen Bereichen entwickelt und die andere ist wenige Jahre jung und hat sich seitdem nur verbreitet, aber nicht im Kern verändert oder entwickelt.

Wir wissen alle, dass Google mit Werbung Geld verdient und wenn ihnen meine sozialen Verknüpfungen helfen, bessere Werbung für mich zu generieren, bin ich dabei. Wir sollte sowieso damit Leben, dass es schon lange kein Leben ohne Werbung gibt und auch nicht geben wird. Dann doch lieber gute Werbung.     

Was weiß ich aber von Facebook? Woher setzt sich ihr Wert zusammen? Irgendwie ein bisschen Werbung machen sie auch, aber das ist doch eine ganze Ecke kleiner als Google es betreibt.
Auch hatte Facebook wesentlich mehr Datenskandale als Google. Bei letzteren wird mal der Mailserver gehackt, okay. Bei Facebook konnte man zu verschiedenen Zeiten an verschiedene Nutzerdaten gelangen. Nicht so schön.

### 3. Freunde in Kreisen

Der Begriff *Freundeskreis* kommt nicht von ungefähr. Aber wie sind heutzutage unsere Freundschaften eigentlich definiert? Alle wissen es, die *Facebook-Freunde* sind nicht zwangsläufig die richtigen Freunde. Bekannte, Kollegen, Geliebte. Und genau so möchte ich diese Menschen gerne einordnen und selbst entscheiden, was ich von wem erfahre (siehe 7.).    
Google+ ermöglicht das sehr intuitiv und einfach mit den Circles. Facebook kann seit kurzer Zeit auch Freunde nach Gruppen sortieren, jedoch ist die Erstellung der Gruppen umständlicher.

### 4. Ich gehe und meine Daten auch.

Zugegeben, das ist ein Feature, was ich noch nicht ausprobiert habe. Mit dem Google+ Tool *Data Liberation* soll es möglich sein, die gesamten Daten, die man bei Picasa, in seinem Profil, in den Google+ Stream bei Buzz und den Kontakten gespeichert hat, herunterzuladen. Und was dann? Dann ist man anscheinend richtig draußen.     
Das sollte den CCC sehr freuen. Bei StudiVZ und Facebook erscheint nach der Löschung z.b. in den Streams und Pinnwänden die Meldung *Gelöschte Person*. Und was mit den Daten nach dem Löschen passiert, wissen wir bei Google+ zwar auch nicht, aber immerhin finden sie schnell alle meine Daten zusammen. Vielleicht klappt das Löschen dann auch wirklich so schnell.

### 5. Kontrollierte Foto-Markierung

Bedingung für dieses Feature ist, dass die betreffende Person bei Google+ sein muss. Ist sie bei Google+, dann wird sie sofort informiert, wenn sie auf einem Bild markiert wird. Auch, wenn sich das Bild in einem Album befindet, welches für die Person gar nicht sichtbar ist (weil sie z.b. nicht im Kreis des Albumbesitzers ist). Das ist ein sehr gutes Feature, welches Facebook in dieser Form nicht besitzt. Man muss erst umständlich in den Einstellungen verschiedenes deaktivieren. Google+ hat es immer drin.     
Bei Facebook rennt ein Algorithmus über alle Bilder und schlägt gleich vor, wer das sein könnte. Das kann man zwar deaktivieren, ist aber trotzdem umständlich. Google könnte das sicherlich auch, lässt es aber sein und gibt so den eigenen Benutzers nur die Möglichkeit Menschen zu erkennen.

### 6. Gute Gruppenchat Features

In Google+ kann man sehr leicht sogenannte Hangouts mit seinen Freunden veranstalten, ein Gruppenchat per Video. Sehr schön, in Facebook ist das nicht vorhanden.

### 7. Was sehe ich, was zeige ich?

In Facebook sehe ich jeden Scheiß. Ob ein alter Klassenkamerad von vor 5 Jahren gerade ein Bier trinkt oder eine andere Person gerade irgendwelche Tiere in *Farmville* benötigt. Im Facebook-Stream ist alles zu sehen, man kann lediglich nach der Art der Infos sortieren. Man kann zwar auch Meldungen von irgendwelchen Facebook-Apps verbieten, muss dafür aber wieder in die umständlichen Einstellungen.     

Bei Google+ kann ich die Menschen in Circles sortieren und mir entsprechende Infos aus jedem Circle anzeigen lassen. Die App-Spielereien gibt es für Google+ sowieso noch nicht. Wenn ich irgendwas veröffentliche, eine Link, ein Bild, sonst was, kann ich entscheiden, welcher meiner Circles diese Information bekommt. Das ist in Facebook gar nicht möglich.

### 8. Googles Sparks

Die cleveren Algorithmen von Google lassen sich nicht nur für Suchen und Werbung nutzen, sondern auch für die Benutzer. Sparks soll dabei helfen, dass die Dinge und Themen zu mir kommen, die mich interessieren und diese kann ich dann mit einem Klick gleich mit Freunden teilen.     
Bei Facebook müsste ich dafür... eine neue Seite öffnen und irgendwie selber suchen. Hier fehlt Facebook einfach die Integration einer guten Suche.

### 9. Bessere mobile Anwendungen

Facebook hat bisher noch keine mobile Anwendung für iOS Geräte herausgebracht und nur eine eher bescheidene für Android. Google dagegen glänzt mit einer guten Android-App und die iPhone und iPad-Besitzer warten nur noch auf die Freigabe von Apple, dann gibts Google+ auch für iOS.    
Besonders da soziale Kontakte immer mehr via mobiler Geräte gepflegt werden, ist es unverständlich, wieso es Facebook noch nicht geschafft hat, eine gute App für alle Plattformen zu schreiben.

## Fazit

Ich bin selbst ein wenig erstaunt. Das Aufschreiben dieser Unterschiede hat mir eines deutlich gemacht: Google ist verdammt noch mal innovativ und Facebook ist tröge und veraltet. Welche großen Innovationen gab es zuletzt auf Facebook? So gut wie keine!      
Gesichterkennung, (endlich) bessere Privacy Einstellungen und Änderungen am Design. Ansonsten ist Facebook nur gewachsen, in dem es in den Sprachen vieler Ländern übersetzt worden ist. Aber nicht, weil es auf einmal coole Features hatte.

Diese Woche wird Facebook irgendwas neues zeigen. Ich bin gespannt, ob es eine ähnliche Tragweite haben wird wir Google+.

**Nachtrag:** Sascha Lobo hat [in seiner wöchentlichen SPON-Kolumne](http://www.spiegel.de/netzwelt/web/0,1518,772656,00.html) ebenfalls über Google+ geschrieben. Er sieht dabei in erster Linie nicht Facebook bedroht, sondern Twitter, dessen Konzept sich in den letzten Jahren auch nicht verändert hat. Google+ positioniert sich als mediales Netzwerk, es bietet den Zugang zu einer Vielzahl von Informationen und sortiert sie dem Nutzer mundgerecht. 

Unterstützung erhält es von seinen Sparks (8) und den Circles (3). Es ist ein Facebook für Erwachsene, eher auf Medien ausgerichtet, als auf soziale Verbindungen. Deshalb wird Facebook weiterhin seine Berechtigung haben.      
Wie sich Google+ als Twitter-Konkurrent mausern wird, sehe ich gespannt entgegen. Momentan ist es mir noch zu überladen, aber die Grundfunktionalität ist vorhanden und wird von einigen meiner Freunde rege genutzt.