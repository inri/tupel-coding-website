---
layout: post
title: "News, News und doppelte News"
description: "Die gleiche Neuigkeit trudelt bei verschiedenen News-Seiten teilweise Tage später erst ein. Ich mache mir oberflächliche Gedanken darüber wieso das so ist."
date: 2011-01-04
tags: [RSS]
published: true
category: blog
comments: false
share: true
---

Via Thunderbird empfange ich die RSS Feeds meiner favorisierten News-Seiten. Dabei handelt es sich besonders um Seiten mit technischem Schwerpunkt, logisch. Dabei fiel mir schon seit längerer Zeit eine negative Tendenz auf: News wiederholen sich. 


So ganz vermeiden lässt sich dieser Umstand natürlich nicht, schließlich haben viele der Seiten sich überschneidende Themengebiete. Aber wieso erscheinen die News über Steve Jobs Schwangerschaft bei der einen Seite heute und bei der anderen erst morgen oder noch später?

Teilweise dauert es richtig lange, bis so eine Nachricht die Runde macht. Zuletzt habe ich es am Beispiel der iPad-Aufnahme des neuen Gorillaz Album gesehen. Die einen wussten Mitte November davon zu berichten (also vor den Aufnahmen), die anderen am 27.12. und gestern am 02.01. trudelten auch noch Berichte ein (jeweils nach den Aufnahmen). Hier sind es zwar nur fünf Tage, aber so ähnlich ist es meistens. Was wird da so lange gewartet?

Ich stelle mir das so vor. Eine Redaktion hat diverse Quellen und checkt auch die Konkurrenz. Dies können ausländische Seiten sein, oder eben andere deutsche. Erzählt mir eine Quelle was, schreibe ich einen Artikel darüber. Lese ich was interessantes auf einer ausländische Seite, übersetze ich die Infos und verlinke auf die englische Seite. Stolpere ich über eine deutsche Nachricht, schreibe ich eine Zusammenfassung und verlinke auf die andere Seite. Fertig. So gebe ich meinen Lesern die News schnell und sie bekommen sogar noch die Originalquellen.

Das wird aber meistens nichts so gemacht. Ich vermute eher, dass Redaktionen, wie die von *Chip.de* zum Beispiel, eine Nachricht lesen und dann selbst &quot;recherchieren&quot; und vielleicht nach der ursprünglichen Quelle suchen. Am Ende kommen die News später und Quellen gibt es auch keine.

Am liebsten wäre es mir, Thunderbird wäre so intelligent, die neuen News zu checken und zu urteilen, welche Original bzw. mehr Infos und Quellen erhält.   
Bei der Gelegenheit kann er auch gleich die Spam-Posts rausschmeißen. Wenn Chip mal wieder einen tollen Preisvergleich hat, den ich nicht brauche, oder PCGH zum gefühlten tausendsten Mal Schnäppchen-Angebote verteilt.