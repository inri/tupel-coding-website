---
layout: post
title: "Vorstellung des iPhone 4S"
description: "Einige Anmerkungen zum neuen iPhone."
date: 2011-10-04
tags: [iPhone]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-iphone-4S-lg.jpg
  credit: Apple
  creditlink: http://www.apple.com
---

Vor wenigen Minuten wurde das neue iPhone vorgestellt.     
Leider kann man über das neue iPhone auch nicht viel sagen. Die Vorstellung war sterbenslangweilig, aber vielleicht sind die Wiederholungen bekannter Features für die Fachpresse wichtig. Unglaublich viel neues gab es ansonsten nicht zu sehen.

* Das Design des iPhone 4S ist unverändert.      
* Die Antennenproblematik soll durch zwei separate Antennen gelöst werden.     
* Erwartungsgemäß werkelt der A5 Chip im iPhone.      
* Der Kamera wurde sehr viel Aufmerksamkeit gewidmet und sie wird mit den 8 MP und 1080 HD-Videos sicherlich wieder sehr gut werden.     
* Ebenfalls ausführlich wurde Siri vorgestellt. Mit Siri kann man auf sprachlicher Ebene mit dem iPhone kommunizieren, also eine Sprachsteuerung. Zum Beispiel das Wetter oder Börsenkurse erfragen, Nachrichten diktieren, und so weiter. 

Wohin geht die Reise mit dem iPhone? Eine Revolution wurde auf jeden Fall nicht vorgestellt. Wie auch? Das iPhone ist ein gutes Smartphone, welches sich auch gut verkauft. Was will man an der Technik viel verändern? Die Frage wird sich auch in einem halben Jahr wieder stellen, wenn ein iPad 2S vorgestellt wird. Technisch sind die Geräte nämlich sehr gut ausgestattet und erfüllen die Wünsche der breiten Benutzerschicht.

Der Fokus wird mehr und mehr auf die Software gerichtet. Nicht ohne Grund nahm die Vorstellung der besten iOS 5 Features und iCloud während der Präsentation so viel Zeit in Anspruch.
Die Software muss man selber testen und kann sie nicht von einem *Technische Details*-Blatt ablesen. Deshalb fällt es momentan vielleicht schwer die ganzen Neuerungen einzuordnen und das wird sich wohl eher in den nächsten Wochen genauer zeigen. Ob Siri was taugt und ob dann die Enttäuschung weicht.

Ich bin gespannt was Google und Samsung in einer Woche zu zeigen haben. Und bald wird auch das Amazon Fire zu haben sein, welches ja auch weniger durch seine technische Stärke als viel mehr die Integration diverser Inhalte zu überzeugen weiß.