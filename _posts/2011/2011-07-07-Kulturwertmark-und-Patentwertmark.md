---
layout: post
title: "Kulturwertmark &amp; Patentwertmark"
description: "Wie funktioniert die Kulturwertmark und wie könnte eine Patentwertmark aussehen?"
date: 2011-07-07
tags: [Technologie]
published: true
category: blog
comments: false
share: true
---

In letzter Zeit sind vermehrt Urheberrechte und Patente im Gespräch. Es gibt verschiedene Parteien in diesem Szenario und verschiedene Nutznießer. Zu nennen wäre da auf jeden Fall die Unterhaltungsindustrie. 


Popmusik, die sich millionenfach verkauft, Kinofilme, die das zigfache ihrer Produktionskosten wieder einspielen. Daneben ein undefinierbarer Haufen an gegenseitigem Verklagen zwischen Herstellern von Hard- und Software, besonders im mobilen Bereich.

Man muss nicht tief in der Materie stecken um zu merken, dass irgendwas mächtig schief läuft. Im folgenden möchte ich darlegen, was das meiner Meinung nach ist und welche Ansätze ganz sinnvoll wären diesen Umstand zu ändern. Dabei lege ich besonderen Augenmerk auf das Konzept der Kulturwertmark des Chaos Computer Clubs und den Überlegungen der Google-Denkfabrik (siehe unten). Und vergesst nicht, ich schildere meine Gedanken aus der Sicht eines mitte-20-jährigen Informatikstudenten, deshalb gibt es einige Links zu relativ aktuellen Klagen. 

## Die Kulturwertmark

Der CCC hat eine interessante Idee, wie man kreative Arbeit in Zukunft entlohnen könnte: Die Kulturwertmark. Ich mag die Idee, aber ich bin auch realistisch und glaube nicht so richtig an ihre Durchführbarkeit in naher Zukunft.      
Man zahlt monatlich einen Betrag und kann dafür die Künstler, die man mag, unterstützen. Entweder einfach so, weil man mag, was sie tun, oder in Verbindung mit dem Download eins Liedes oder entsprechend. Die Künstler müssen natürlich an diesem System teilnehmen und sich dafür registrieren. Wichtig ist der Schwellenwert: Erhält der Künstler für ein Produkt, was auch immer das sein mag, eine gewisse Anzahl Kulturwertmark, geht dieses Produkt an die Öffentlichkeit (genauer gesagt: das Verwertungsrecht). Dieser Schwellenwert wird vorher festgelegt, logisch.      
Das sind die Kernpunkte des Systems.

Natürlich bleiben wichtige Fragen offen, aber wenn man mal einige Minuten darüber nachdenkt, scheint das eine ganz gute Idee zu sein. Zu einem ähnlichen Schluss kam auch die Google-Denkfabrik. Das Urheberrecht ist veraltet und z.b. mit seinen 70 Jahren Laufzeit viel zu lang. Auch hat der Nutzer zu wenig Rechte, obwohl er dafür bezahlt.      
Das ist eben genau die eine Sache, die mich stört. Ich nannte es in der Einleitung schon, die pervers hohen Gewinne in der Unterhaltungsindustrie. Für was bitte? Zum Beispiel Michael Jackson hat niemals so fantastische Lieder geschrieben, dass auch nach seinem Tod noch Millionen Dollar an Umsatz mit ihnen gemacht wird. Das gleiche trifft für viele Musiker und Gruppen zu, der Tod ist dabei keine Bedingung.

## Überzogenes Beispiel

Wenn eine Musikgruppe ein neues Musikalbum aufnimmt, dauert das im Normalfall einige Wochen. Bessere (= wohlhabendere) Gruppen nehmen sich mehr Zeit. Mieten des Studios, die Arbeitszeit der Gruppe, die Arbeitszeit der Studio-Crew und die kreative Arbeit im Vorfeld (Texte schreiben,...) sind die kostspieligen Faktoren. Ich weiß, es kommen noch Marketing dazu und Designentscheidungen, aber sehen wir uns das ganze mal nackt an. Sagen wir an einem Album arbeiten 25 Menschen, davon fünf ein ganzes Jahr (die Gruppe) und die anderen nur insgesamt jeweils vier bis sechs Monate. Ich hab mal einen Wert gegoogelt und weiß nicht genau, wie realistisch dieser ist. Ein Arzt in einer Uniklinik verdient um die 55.000€ im Jahr bei einer 42 Stunden Woche. Angenommen die gesamten 25 Menschen würden etwas ähnliches Leisten, kämen wir nicht einmal auf 1 Mio. Euro für die Truppe. 

Die KWM könnte die Künstler vielleicht endlich unabhängig werden lassen und dabei trotzdem auch den Kunden ein gutes Gefühl geben. Das gleiche trifft auch für Kinoproduktionen zu. Damit tritt man den großen Medienkonzernen auf die Füße und deshalb wird diese Art der Vergütung, die in der Tat mal wieder etwas mit realer Leistung zu tun hätte, sich so bald nicht durchsetzen.

Dabei wäre sie notwendig. Nicht nur in der Unterhaltungsindustrie. Ich verfolge ja die technischen Neuigkeiten und da zeichnet sich seit einiger Zeit ein anderes Schlachtfeld ab: Patente. Diese Form des Urheberrechts wird momentan hart erstritten. Oracle fordert von Google 2,6 Mrd. $ für Softwarepatente, Apple hält kuriose Patente auf Touchzeug und könnte alle Smartphone-Hersteller mit dieser Technologie verklagen, oder eine Firma lässt sich ein sicherlich nicht innovatives Bezahlkonzept patentieren und verklagt nun App-Entwickler, die dieses Verfahren benutzen müssen, weil das Framework nun einmal so funktioniert.      
Mit dabei sind auch Microsoft, Samsung und andere. Jeder will irgendwas von jedem. Kindergarten. Von diesen ganzen Rangeleien profitieren am Ende auch nie die Käufer sondern nur die Anwälte.

## Die Patentwertmark

Man müsste ein ähnliches System einführen: die **Patentwertmark**. Patente müssen geschützt werden, das ist klar. Genauso klar wie die Tatsache, dass Künstler auch Geld verdienen möchten. Bei Patenten könnte man ebenfalls das Kaufprinzip einführen, wer sich ein Patent *kauft* bzw. lizensieren lässt, kann es benutzen. Nun wird das Patent aber nicht erst nach Jahrzehnten frei nutzbar, sondern viel eher. Nämlich wenn der Patentinhaber eine gewisse Menge von Geräten verkauft hat, die dieses Patent nutzen. Ich würde sogar noch die Geräte der Hersteller hinzuziehen, die sich entsprechende Patente lizensiert haben.

Ein Patent hat zum einen den innovativen Wert, denn es ist etwas neues. Zum anderen besitzt ein Patent aber durchaus auch einen Geldwert (wie künstlerische Erzeugnisse), der durch Verkauf von Lizenzen und Geräten erreicht wird. Man kann auch hier einen Schwellenwert bestimmen.
Wenn Apple dann 5 Mio. iPhones verkauft hat, war es das mit Touchscreenpatenten. Sie sind frei verfügbar und keiner muss sie mehr lizensieren. So kann man doch ganz einfach sicherstellen, dass ein Patent durchaus auch einen großen Gewinn bringen kann. Am Ende wird es das innovativste Konzept sein, denn die Firmen behindern sich nicht gegenseitig mit Klagen.

## Die Realität

Die Wirklichkeit sieht momentan aber anders aus. Solche Ideen müssen auf staatlicher Seite ein- und durchgeführt werden. Nur wenige Künstler und vermutlich keine Firmen würden sich darauf einlassen. Deshalb muss es anscheinend so weitergehen. Die Unterhaltungsindustrie fährt immer geringere Gewinne ein, wir laden uns Filme und Musik weiter illegal herunter und die Anwälte lecken sich die Finger nach Patentverletzungen.

Auf Twitter bekam ich vor kurzem diesen passenden Spruch von Alexander Weidel ist:

*Erst wenn der letzte Programmierer eingesperrt 
und die letzte Idee patentiert ist, werdet ihr merken, 
dass Anwälte nicht programmieren können.*