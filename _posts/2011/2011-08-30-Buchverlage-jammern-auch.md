---
layout: post
title: "Buchverlage jammern auch"
description: "Nun beginnen auch die Buchverlage über die bösen Raubkopierer zu jammern."
date: 2011-08-30
tags: [Technologie, Ebooks]
published: true
category: blog
comments: false
share: true
---

Auf heise.de wurde heute ein Bericht veröffentlicht: *Buchindustrie: Piraten machen E-Book-Markt kaputt* - und es stand genau das drin, was ich auch befürchtet hatte. 


Der Börsenverein des deutschen Buchhandels hat eine Studie in Auftrag gegeben und deren Ergebnisse bezeugen ihre Befürchtungen. Denn die Hälfte aller EBook Downloads sind angeblich illegal und vermiesen ihnen das Geschäft.

Buhu. 

Nein, es sind nicht die bösen Raubkopierer, die ihnen in die Quere kommen, sie sind es selbst. EBook-Reader gibt es für den Mainstream nun seit über drei Jahren, aber in Deutschland spielen sie eher eine untergeordnete Rolle. Dafür gibt es meiner Meinung zwei Gründe: Preise und Angebot. Große Verlage waren und sind sehr zögerlich, was die Digitalisierung ihrer Produkte angeht und vor allem wollen sie fast die gleiche Summe Geld wie für das analoge Produkt.      
Doch ich finde, dass bei der digitalen Schreibkunst einige erhebliche Einsparungen gemacht werden können. Sei es das Drucken / Pressen der Bücher, sei es der Vertrieb über verschiedene Buchläden. Bücher sind in Deutschland nicht billig und in digitaler Form sowieso nicht. 

Aber das wäre ja zu viel Wahrheit für die Branche. Lieber redet sich der Geschäftsführer des Börsenverein ein, dass:

*Eine gute Angebotsstruktur schützt in keiner Branche vor illegalen Downloads: Obwohl es zum Beispiel auch auf dem noch jungen EBook-Markt von Anfang an große und bekannte legale Download-Angebote wie beispielsweise das Branchenportal libreka! gibt, haben die Nutzer sich über die Hälfte aller heruntergeladenen EBooks illegal beschafft. Das zeigt, wie hoch das Gefährdungspotenzial für die Rechteinhaber ist.*
   
Die Buchindustrie hatte, und hat vielleicht immer noch, eine Chance, nicht die gleichen Fehler zu machen, wie die Musikindustrie. Durch solche Studien und Kommentare beginne ich aber sicherlich nicht damit, legal EBooks zu erwerben. 

Ich fände es mutig, wenn ein *berühmter* Autor ein Buch in Eigenregie veröffentlicht und den Buchverlagen ins Gesicht lacht. Er kann weniger Geld dafür verlangen und gleichzeitig mehr in die eigene Tasche stecken. 

Mehr Unabhängigkeit für die Kunst, ob Musik oder Literatur.