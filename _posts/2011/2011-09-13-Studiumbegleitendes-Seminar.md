---
layout: post
title: "Studiumbegleitendes Seminar"
description: "Habt ihr auch ein Studiumbegleitendes Seminar in eurem ersten Semester gehabt? Wenn nicht, dann erzähle euch etwas darüber."
date: 2011-09-13
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Bevor ich zu den ersten *richtigen* Kurse komme, wird erst einmal das Studiumbegleitende Seminar vorgestellt. Der Grundgedanke ist hier wieder sehr ähnlich zum Vorkurs Mathematik. Denn in dem Seminar soll jeder Student ein Thema vorstellen, also das Sprechen / Vortragen / Präsentieren vor einer Gruppe Menschen üben. 

Gleichzeitig lernt man auch die anderen Studenten des Seminars kennen. Alle anderen Kurse des ersten Semester sind als Vorlesung konzipiert und da sitzt man eben nur in einem Hörsaal und hört zu, mit 80 anderen.     
Im Studiumbegleitenden Seminar hat man eine gemütliche Runde von 15 bis 20 Studenten und kann sich viel Zeit nehmen die Vorträge zu besprechen. Ein Student älteren Semesters leitet die Runde. Jeder Student überlegt sich ein Thema, welches inhaltlich mit dem Studium zu tun haben sollte. Am besten über eine Programmiersprache, die man bereits beherrscht, oder auch ein *alter* Vortrag aus dem gymnasialen Informatikunterricht. 

Jede Woche stellen dann zwei bis drei Studenten ihr Thema vor und die Vorträge werden besprochen und kritisiert. Die Idee mag ja gut gemeint sein, aber besonders im ersten Semester, wo alles noch neu ist und man sich erst auf die Studiumsituation einstellen muss, stört dieser Kurs leider.      
Nicht nur mir ging das so. Viele habe ich ächzen hören *Ach Mist, mich muss noch den blöden Vortrag machen...* und so fühlt es sich an. Der Kurs ist zwar Pflicht, aber benotet wird er nicht.

Außerdem kann man wieder über den Nutzen diskutieren. Ich habe fast Respekt vor Studenten, die es im Gymnasium geschafft haben Vorträgen auszuweichen. Und letztlich steht auch die Frage im Raum, wann man das Erlernte bzw. die Kritik produktiv umsetzen kann.     
In Seminaren hält man meistens Vorträge, aber das sind in Summe vielleicht auch nur zwei bis drei und das auch erst ab dem dritten Semester, also ein Jahr nach dem Studienbegleitenden Seminar.

Ich glaube das Seminar sollte auch für andere Fragen rund ums Studium da sein. Aber letztlich richtig hilfreich fand ich nur weniges. Die Tips der älteren Studenten flossen in die Gerüchtewelt des HPI ein und waren am Ende auch nur noch Halbwahrheiten.    
Die Intention des Seminars mag gut sein, aber es ist meines Erachtens nicht notwendig. Es bietet eine Art Unterstützung und Starthilfe. Man versucht sich gut um die Studenten zu kümmern.      

Das ist eben das HPI. Vermutlich vermissen Studenten anderer Unis so einen Kurs, aber an anderen Unis ist vieles unter Umständen auch unübersichtlicher. Es kann nicht überall so angenehm übersichtlich sein wie an unserem kleinen Institut.