---
layout: post
title: "Model Driven Software Development"
description: "Einige Worte zum Seminar MDSD und meinem Projekt darin."
date: 2011-06-28
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Dieses Semester habe ich das Seminar *Grundlagen der Modellgetriebenen Softwareentwicklung* belegt. Das ist zum einen ein interessantes Thema und zum anderen auch ganz nützlich für meinen Studentenjob. Die Inhalte überschneiden sich und es ist eine Win-Win-Situation (Studium - Arbeit).     
Zu Beginn des Seminars wurde eine Anzahl Themen vorgestellt und jeder Seminarteilnehmer sollte ein Thema auswählen, eine Ausarbeitung darüber schreiben und diese dann in einem Vortrag vorstellen.

Ich habe die Konzepte der drei Transformationssprachen TGG, QVT und ATL vorgestellt. Hier das Abstract dazu:

*Abstract. Neben vielen Modellierungssprachen, haben sich in den letzten Jahren auch Sprachen für die Transformation von Modellen entwickelt. Die drei am weit verbreitetsten Sprachen sind ATL, QVT und TGG. Jede von ihnen vertritt unterschiedliche Konzepte. In dieser Ausarbeitung werden diese Sprachen vorgestellt und miteinander verglichen. QVT unterstützt verschiedene Konzepte, in dieser Arbeit liegt der Fokus auf QVT Operational, einer imperativen Sprache mit programmierartigen Definitionen von Transformationen und einzelnen Regeln. Die deklarativen Triple Graph Grammatiken unterstützen bidirektionale Transformationen. ATL deckt als hybride (imperativ / deklarativ) Sprache beide Konzepte ab. Dadurch lassen sich  Transformationsregel auf verschiedene Arten definieren.*

Klingt doch interessant, oder? 

[Meine Präsentation in Prezi](http://prezi.com/thyfjkpggw0-/modelltransformation/)

[Die Ausarbeitung als PDF bei Google Drive](https://docs.google.com/viewer?a=v&pid=explorer&chrome=true&srcid=0ByxYMUB42ZXmZjBjMTE2OTgtYTFjMy00MzVkLTljOWEtYTIwNmQ2OGNlMmU1&hl=en_US)

Viel Spaß beim Lesen.