---
layout: post
title: "iPad 2 und das Smart Cover"
description: "Kurze Gedanken zum Smart Cover"
date: 2011-03-28
tags: [iPad]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-ipad-2-smart-cover-lg.jpg
  credit: Apple
  creditlink: http://www.apple.com
---

Nachdem die ersten iPads aus Amerika in dt. Redaktionen eingetrudelt sind, wurde natürlich auch das Smart Cover getestet. Teilweise folgten ernüchternde Berichte. Sie werden auf der Aussen- und Innenseite schnell schmutzig und auf dem Bildschirm sind die Zwischenräume nicht sauber. Sie bestehen zudem nur aus einigen Magneten und überhaupt haben sie von Apple mehr erwartet.

Diesen Text schreibe ich auf dem iPad und als Unterlage dient mir das Smart Cover. Vorhin haben wir die Heute-Show auf dem iPad geschaut und dabei das Smart Cover verwendet. Ein iPad ist ein Gebrauchsgegenstand und deshalb muss es meiner Meinung nicht in eine Ganzkörperschutzhülle gepackt werden. Wozu ist das iPad denn sonst aus Aluminium? Nur der Touchscreen sollte nicht unbedingt zerkratzt werden. Wenn man es also transportiert, kann es ruhig geschützt werden und da kommt das Smart Cover ins Spiel. Leicht, schick und schützt den Touchscreen.

Nun zeigt mir mal ein iPad-Zubehör, dass diese Funktionen umsetzt (leicht, funktional, schützend). Dass der Bildschirm nicht blitzeblank sauber und das Smart Cover auch schmutzig wird, ist eben so. Wenn etwas vor Schmutz schützen soll, dann wird dieses etwas eben schmutzig.     
Dass man das Smart Cover beim funktionellen Falten nur auf die Microfaser-Seite legen kann, ist sicherlich eine Diskussion wert, aber ich denke mir, die Apple-Designer haben darüber gründlich nachgedacht und es geht vermutlich nicht optimaler. Da muss der Benutzer eben selbst ab und zu den Touchscreen sauber machen und aufpassen, dass kein gröberer Schmutz auf diese Seite gelangt.

Wer lediglich den Bildschirm seines iPads bei Transport schützen möchte und gleich noch das praktische Aufstellen nutzen möchte, der findet vermutlich kein besseres Utensil.