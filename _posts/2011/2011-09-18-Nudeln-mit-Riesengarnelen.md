---
layout: post
title: "Nudeln mit Riesengarnelen"
description: "Ein leckeres Rezept, inspiriert von Jamie Oliver."
date: 2011-09-18
tags: [Rezept]
published: true
category: blog
comments: false
share: true
image: 
  feature: 2011-nudeln_garnelen-lg.jpg
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Seit einigen Wochen probieren meine Freundin und ich verschiedene Rezepte aus der [iPad-App](http://itunes.apple.com/gb/app/jamies-recipes/id398011800?mt=8) eines berühmten, britischen Kochs.     
Zwar werde ich hier ein Rezept nur grob umreißen, aber ich weiß nicht genau, wie das mit den Urheberrechten genau ist. Deshalb nenne ich das Gericht: **Nudeln mit Riesengarnelen**.

## Zutaten (2 Personen)

* 200g Riesengarnelen 
* 250g Nudeln, z.b. Spaghetti oder Barilla bavette n.13
* 150g Cocktailtomaten
* 1 Chili
* 1 Knoblauchzehe
* 1 Hand voll Basilikum
* Olivenöl, Salz, Pfeffer, Topf, Pfanne,

## Zubereitung

Ich bereite immer alles vor. Das bedeutet bei diesem Rezept: Tomaten halbieren, Knoblauchzehe schälen und zerstückeln, die Chilischote entkernen und in Streifen schneiden und den Basilikum waschen.  
    
Sodann werden die Nudeln angesetzt und gekocht. Parallel werden die Garnelen mit dem Chili und Knoblauch in Olivenöl angebraten (2 min), die Tomaten hinzugegeben (5 min) und alles mit Salz und Pfeffer gewürzt. Am Ende noch einen Schluck Olivenöl hinein, Nudeln abgießen und die Garnelenpfanne untermischen.

Zum Servieren die Basilikumblätter darauf streuen und fertig! Das Zubereiten dauert ungefähr eine halbe Stunde.

## Wie schmeckt es?

Die Garnelen schmecken natürlich supi, keine Frage. Der Knoblauch und die Chili geben dem ganzen eine sehr angenehme Schärfe. Ich war erstaunt, dass man keine Soße benötigt. Aus den Garnelen inklusive Tomaten und Olivenöl erhält man genug Flutschigkeit. Der Basilikum zaubert seine edel-schmeckende Note hinein.

Wir haben auch Parmesan dazu probiert, aber das schmeckt man aufgrund der leichten Schärfe und dem Basilikum kaum heraus. Wer es mag, kann den Hartkäse dazu streuen, aber man vermisst ihn auch nicht.