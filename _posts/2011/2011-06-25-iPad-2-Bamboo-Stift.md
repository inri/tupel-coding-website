---
layout: post
title: "iPad 2 mit Bamboo Stift"
description: "Ein toller Eingabestift für das iPad."
date: 2011-06-25
tags: [iPad]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-bamboo-lg.jpg
  credit: Wacom
  creditlink: http://www.wacom.eu/index4.asp?pid=9269&lang=de

---

Steve Jobs sagte einmal sinngemäß, mit den iPad soll man keinen Eingabestift verwenden. Er meinte damit, dass die Apps und das System so gut benutzbar sein sollen, dass man keinen Stift benötigt. Diese Eigenschaft ist zweifelsfrei erfüllt.

Trotzdem wollte ich gerne wissen, welche Möglichkeiten sich mit einem Eingabestift bieten. Vor einigen Wochen wurde der Bamboo Stylus für das iPad vorgestellt. Ich musste bei drei Händlern bestellen und zwei Stornierungen erdulden, bis ich ihn endlich in den Händen halten konnte.

Der Stylus sieht schick aus und hat ein handliches Gewicht (Waage sagt 17 g). Überraschend war für mich die Spitze des Stylus, eine gummierte Halbkugel. Sie ist auch relativ dick, Bamboo spricht von 6mm - das ist schon etwas dicker als ein Bleistift, aber immer noch wesentlich dünner als meine Finger.

Mit dem Stift lässt sich das iPad aber genauso gut bedienen! Ich war wirklich positiv überrascht. Im normalen Betrieb wird man ihn freilich nicht nutzen, aber für einige Notizen und Zeichen Apps ist der Stift wirklich ein großer Gewinn. Dank der im Vergleich zur Fingerkuppe feineren Spitze kann man die Worte viel besser auf den Bildschirm unterbringen. Aber auch gezielte Bearbeitung von Fotos oder generell das Malen von Bildern ist wesentlich leichter möglich.

Ich mag den Stift sehr.