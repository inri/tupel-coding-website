---
layout: post
title: "Vorkurs Mathe"
description: "Sollte man den Vorkurs nur für den Mathestoff besuchen?"
date: 2011-09-10
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Während Dominic das Grundstudium von hinten aufrollt und mit 3D-Computergrafik beginnt, schlage ich den gegenteiligen Weg ein und berichte von der ersten *Vorlesung*.

Das HPI bietet vor Beginn des Studiums einen Vorkurs in Mathematik an. Ziel soll es sein *mögliche mathematische Wissens- und Fähigkeitslücken zu schließen und sich auf die Hochschulmathematik vorzubereiten*. Dieses Jahr dauert der Kurs wieder drei Wochen und wird den Studenten insgesamt um die 80 Stunden Vorlesung mit Dr. Börner schenken.

Das offizielle Ziel kann ich nicht ganz nachvollziehen. Inoffiziell hat der Kurs einen ganz anderen Nutzen: Man lernt seine Kommilitonen kennen und schließt erste Freundschaften. Außerdem hat man später genau gesehen, wer im Kurs war und wer nicht. Das mit den Freundschaften klappt zwar später auch, aber man legt einen besseren Start hin.

Die Studenten kommen aus verschiedenen Bundesländern und besitzen einen unterschiedlichen Kenntnisstand in Mathe. Vor allem hatte nicht jeder einen Leistungskurs Mathe im Abitur belegt. Was diesen Aspekt angeht, kann man sicherlich einige Lücken schließen. Wobei auch ganz klar gesagt werden muss, dass der Stoff nicht mit gemächlicher Geschwindigkeit behandelt wird. Noch dazu fast sechs Stunden pro Tag, fünf Tage die Woche, so viel Vorlesung hat man im restlichen Studium nicht mehr.

Der Vorkurs ist teilweise eine Zusammenfassung von Mathe II und in dieser Hinsicht ist das sicherlich eine Art Vorbereitung auf die Hochschulmathematik. Dummerweise hat man Mathe II erst im zweiten Semester und da liegt der Vorkurs schon wieder ein halbes Jahr zurück. Für Mathe I ist der Vorkurs weniger nützlich, denn da steht die Logik im Vordergrund.

Lohnt es sich den Vorkurs zu besuchen? Für den sozialen Aspekt auf jeden Fall. Ob man in diesen drei Wochen viel vom komprimierten Stoff mitnimmt, hängt von jedem selber ab.
Mathe II erstreckt sich über ein Semester und ist mit Übungen unterfüttert. Wer fleißig ist, kann den Stoff auch im Semester bewältigen. Oder zumindest ist das Nichtbesuchen des Vorkurses sicher kein Grund für ein Scheitern. Das Besuchen allerdings auch kein Versprechen zum Bestehen von Mathe II.

P.S. Man muss sehr lange auf die Ergebnisse der Mathe II Klausur warten. Im Nachhinein weiß man, die werden unter anderem wegen des Vorkurses verzögert, der parallel stattfindet. Wann soll Dr. Börner denn Klausuren kontrollieren, wenn er bis 16 Uhr in der Uni ist?