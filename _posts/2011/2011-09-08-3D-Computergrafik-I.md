---
layout: post
title: "3D-Computergrafik I"
description: "Dominic beschreibt den Kurs 3D-Computergrafik I."
date: 2011-09-08
tags: [HPI, Studium, Gastbeitrag]
published: true
category: it
comments: false
share: true
---

Nun denn, ich beginne meine Fächerrückblicke mit einem spannenden aber zugleich auch überaus anstrengenden Teil des letzten Semesters: Computergrafik.


**Dies ist ein Gastbeitrag von Dominic Petrick.**

Als alter Gamer war ich von diesem Fach von vornherein begeistert und wollte unbedingt endlich mal meine Finger an Polygone, Texturen und Weltkoordinaten legen.     
Also bin ich guter Dinge in die erste Vorlesung gegangen und habe Prof. Döllners Ausführungen seitdem sehr genau zugehört. Tatsächlich war die Vorlesung  insgesamt sehr interessant, ungefähr vergleichbar mit Datenbanksysteme bei Prof. Naumann, so dass man eigentlich immer gut zuhören konnte. Mein einziges Problem war eigentlich nur, dass Herr Döllner teilweise mit einer dermaßen hohen Geschwindigkeit über schwierige Dinge philosophierte und bei einfachen Dingen unnötige Erklärungen abgab. Weiterhin gab es einige, meines Erachtens, nicht ganz sinnvolle Abstiege in theoretische Tiefen, wie beispielsweise das Abhandeln einer vierdimensionalen Divisionsalgebra über dem Körper der reellen Zahlen. Ah, genau.

Zum Glück war aber nicht alles vierdimensional und es wurde eine Fülle von Themen behandelt. Aber was kann man sich nun allgemein unter Computergrafik vorstellen? Es wird abstrakt über Grundlagen der Computergrafik gesprochen. Aha, was sollen denn die abstrakten Grundlagen sein? Vereinfacht gesprochen ist es nicht das modellieren von Objekten oder tollen Animationen etc. in einem Tool wie z.B. Cinema4D, welches für Filmproduktionen usw. benutzt wird.     
Es ist das Arbeiten des Tools unter der Haube, also die Engine. Wenn man genau darüber nachdenkt, ist es gar nicht ersichtlich, wie z.B. eine Linie gezeichnet wird. Ja klar, irgendwie Pixel einfärben. Aber welche? Was ist zu beachten? Wie verwalte ich 3D-Modelle als abstraktes Datenmodell? Auf diese Grundlagen und noch mehr wurde detailliert eingegangen.

Dazu zählen allgemeine Kenntnisse über Raster und Vektorgrafiken und Bildräume, sowie Grundlagen von 2D-Bildmanipulationen, sog. Bildfaltungen. Es folgten traditionelle Algorithmen zum Zeichnen und Rendern von Primitiven (Linie, Dreieck, ..). Dann ging es gleich weiter mit geometrischen Transformationen von 3D-Objekten, also z.B. Rotation und Translation. Es folgte viel Matrizen- und Vektorraumkram, welches die mathematische Basis dessen zementierte, was wir gerade genau mit Transformationen eigentlich machten.      
Anschließend wurde auf Dinge wie z-Buffer der Grafikkarte, Sichtvolumen der virtuellen Kamera, Weltkoordinaten usw. eingegangen, wodurch man eine ungefähre Ahnung von dem bekam, wie ein virtueller Raum aufgebaut ist.     
Darauf aufbauend kamen dann Polygone, Polygon- und Dreiecksnetze der geometrischen Modellierung. Gegen Ende des Semesters wurde dann auf höhere Konzepte wie Oberflächennormalen und der damit verbundenen Beleuchtungsberechnung und Schattierung von Polygonen eingegangen, sowie abschließend Grundlagen der Texturierung von Objekten als Vorgeschmack auf Computergrafik II.

Neben der Vorlesung hatten wir insgesamt 5 Übungen zu meistern, von welchen 4 praktisch und eine theoretisch war. Die Übungen wurden benotet und gingen dann zu 25% in die Endnote ein. So weit so gut. Im Laufe der Zeit hat sich dies allerdings als deutlicher Minuspunkt rausgestellt, da ich jede Übung perfekt meistern wollte, um ja keine Notenpunkte zu verlieren. So fraßen die Übungen einen riesigen Teil meiner Wochenzeit auf, mit bis zu mehr als 20h Bearbeitungszeit.

In den Übungen selbst musste man mit einem Partner mit OpenGL Objekte rotieren, animieren und Datenstrukturen erstellen, mit welchen man Objekte manipulieren konnte, was alles in der Sprache C++ geschah und im Nachhinein betrachtet einfach war. Tja, wie gesagt, im Nachhinein. Als man zuerst davor saß, probierte man stundenlang an einer Lösungsvariante rum, bis man feststellte, dass sie nicht funktioniert und man sich kurzerhand eine neue ausdenken musste. So ging das 4 bis 5 mal und dann hatte man die Lösung erraten. Aber man konnte auch sehr viel Spaß beim rumprobieren haben, wenn dann das Modell einer Kuh, die man eigentlich virtuell explodieren lassen wollte, seltsame Verrenkungen auf dem Bildschirm vollzog, die einfach nur zum brüllen komisch waren.

Insgesamt kann ich im Prinzip jedem 3D-Computergrafik empfehlen, der bereit ist sehr viel Zeit zu investieren, sich dafür interessiert (sehr wichtig!) und sich nicht vor Mathematik scheut. Am besten, man belegt dieses Fach aus Interesse und nicht, weil man die Note braucht.     
Am Schluss möchte ich noch ein paar Worte zu der Klausur verlieren, welche mich persönlich sehr enttäuscht hat. 
Sie ging über 3 Stunden und somit rechnete ich mit einer sehr theorielastigen Klausur, was im Anbetracht des Stoffs angemessen zu sein schien.      

Was jedoch tatsächlich abgefragt wurde hat meinen Puls in die Höhe schießen lassen vor Wut. Es wurden dermaßen konkrete Dinge abgefragt, welche man so noch nicht gesehen hatte und welche meines Erachtens ungerechtfertigt waren. Ein Beispiel ist das Erkennen von Bildfaltungskernen. Das sind Matrizen, welche Gewichte für eine 2D-Bildfaltung beinhalten, um so z.B. Kanten zu erkennen oder das Bild zu schärfen. Davon waren eine Hand voll gegeben plus ein paar Namen daneben. Nun sollte man die Kerne den Namen zuordnen. Ja nee, ich sehe an der Matrix ganz sicher, ob das nun ein Gauß oder ein Mean-Filter ist. Bullshit. 

Natürlich waren auch schöne Theoriefragen dabei, für die ich letztendlich gelernt habe, aber der Bullshit-Faktor war zu hoch, um mit einem dem Aufwand des Lernens angemessenen Ergebnis zu rechnen.