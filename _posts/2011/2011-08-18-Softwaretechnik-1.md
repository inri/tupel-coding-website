---
layout: post
title: "Softwaretechnik I"
description: "Einige Informationen darüber was wir im Kurs Softwaretechnik gemacht haben."
date: 2011-08-18
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Vor einigen Tagen wurden die Noten für Softwaretechnik I (SWT I) veröffentlicht. Sie fielen ganz gut aus, so ähnlich wie in Softwarearchitektur (SWA), außer dass die Projektnoten einen Tick schlechter waren.

Der Aufbau von SWT I war analog zu SWA. Wir wählten ein Projekt, bearbeiteten dieses und hörten uns parallel dazu die Vorlesung an. Am Ende hielten wir einen Vortrag über unser Projekt und schrieben eine Klausur. Thematisch standen verschiedene Softwareentwicklungsprozesse im Mittelpunkt. Namentlich: Extreme Programming, Scrum, RUP, TDD, RDD.

Ein Unterschied war die Auswahl des Projekts. Mussten wir in SWA noch ein eigenes Projekt finden bzw. ausdenken, durften wir diesmal aus bestehenden Projekten auswählen. Es waren Projekte, die auf Seaside basierten, ein Webframework für Squeak (natürlich). War das nun gut? Seaside kennenzulernen war sicherlich interessant, aber an bereits bestehenden Projekten zu arbeiten, beschränkt einem in der Entwicklungsfreiheit. Es kommt auch nicht besonders viel Spaß auf, wenn die Projekte frühere Studentenprojekte waren und nicht unbedingt *schön* programmiert wurden.

Unser Engagement für das Projekt hielt sich in gewissen Grenzen. Zum einen gab es stets anderes zu tun und wie gesagt, die Motivation war auch nicht riesig. Außerdem ahnten wir, dass das Projekt genauso wichtig sein würde wie im letzten Semester.     
Ich fand unser Pokemon-Spiel von der Umsetzung her sehr gut, aber wir sind leider nicht so sehr auf den Design Pattern rumgeritten. In der Noteneinsicht von Softwarearchitektur kam heraus, dass unsere Vorträge die Projektnoten bestimmt haben und die Codedurchsicht lediglich eine kleine Änderung nach oben oder unten bewirkt hatte.

Dieses Semester waren nicht primär die verwendeten Design Pattern im Projekt wichtig, sondern wie wir das Projekt organisiert haben. Dazu wurde am Anfang der Vorlesung passenderweise Extreme Programming vorgestellt, welches eigentlich der einzige Prozess ist, der sich einigermaßen vollständig im Studentenleben umsetzen lässt.

Nachdem die Projekte vergeben wurden, trafen wir uns jede Woche zweimal, machten untereinander viel Pair Programming und arbeiteten unsere Use Cases ab. Nicht!     
Die ersten Wochen passierte gar nichts. Jeder arbeitete sich in Seaside ein und ab und zu wurde mal einige Tasks angesehen. Wir bekamen vom Lehrstuhl Tasks, also Aufgaben, die wir in die bestehenden Projekte bzw. Systeme einprogrammieren sollten. Mehr Funktionalität oder bessere Umsetzung von bestehenden Dingen. 

Unser *Glück* war vielleicht, dass unsere *gegnerische* Gruppe ihren Vortrag sehr zeitig halten musste. Jedes Projekt wurde an zwei Teams vergeben und beide mussten die gleichen Tasks bearbeiten. Nun war ihr Vortrag aber so zeitig, dass sie viel eher als wir etwas fertig haben mussten. Die Tasks wurden aber erst nacheinander freigegeben, was bedeutet: Einen Task lösen, einen neuen bekommen. Dadurch hatten wir sehr viele Tasks in der Pipeline und genug zu tun.

So richtig aktiv wurden wir drei Wochen vor unserem Vortrag. Da setzten wir uns drei Tage zusammen und arbeiteten intensiv an den Tasks und unserem Vortrag. Der von uns verwendete Entwicklungsprozess (XP) floss nur stichpunktartig im Vortrag mit ein. Die Implementierung nahm einen große Teil ein. Es gab noch eine Konsultation und da teilte man uns mit, dass das Verhältnis Prozess vs. Implementierung ruhig 1:1 sein. Auch andere Gruppen hatten wesentlich mehr dazu formuliert. Also zogen wir einen mehrwöchigen Entwicklungsverlauf aus dem Hut. Natürlich mit einigen Abstrichen, Probleme, die auftauchten und einigen Ecken und Kanten.

Aber eigentlich entstand das meiste mit viel Phantasie und basierte weniger auf unseren echten Prozess. Doch wir haben uns ausführlich damit beschäftigt und das wollten sie doch nur hören. Andere Vorträge klangen auch nicht nach voller Wahrheit und waren definitiv geschönt. Wenn ihnen wirklich etwas daran gelegten hätte, dass die Gruppen die Entwicklungsprozesse nutzen, hätte man das anders organisieren müssen.

Unser Vortrag lief ganz gut. Man muss besonders darauf achten, dass man keine Fehler in den Diagrammen macht. Darüber diskutiert der Lehrstuhl gerne. Vor nicht mal zwei Wochen haben wir die Projekt abgegeben und nach einer Woche gab es schon die Noten. Das bedeutet, sie haben in weniger als einer Woche die Projekte durchgesehen. Das bestätigt die Wichtigkeit der Vorträge und die Nebensächlichkeit des gesamten Projekts.

Resümee: Projektarbeit ist interessant. Vorlesung ist keine Notwendigkeit. Squeak hat eine doofe Entwicklungsumgebung und macht uns keinen Spaß. Juhu, kein Squeak mehr. Flunkern im Vortrag ist manchmal ein guter Weg.