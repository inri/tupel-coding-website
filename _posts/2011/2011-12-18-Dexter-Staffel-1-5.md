---
layout: post
title: "Dexter, Staffel 1 bis 5"
description: "Ein Rückblick auf 5 Staffeln Dexter."
date: 2011-12-21
tags: [Serie]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-dexter-season-1-lg.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/dexter/home
---

Die Serie Dexter beendete jüngst ihre sechste Staffel und bevor ich ein kurzes Review dieser Staffel zum besten geben, gibt es hier die bisherige Geschichte von Dexter.

## Ich bin Dexter

Dexter ist ein Blutspurenanalyst bei der Miami Polizei, das ist sein *erstes* Leben. Sein zweites Leben wird von der dunklen Seite in ihm bestimmt, der Drang zu Töten. Sein Stiefvater, welcher Polizist gewesen ist, bemerkte diesen Drang an Dexter als er noch ein Junge war und erzog ihn zur Vorsicht. Das bedeutet, Dexter tötet nicht wahllos Menschen, sondern sucht sich seine Opfer sorgfältig aus.     
Er wählt nur Menschen, die ebenfalls Mörder sind und meist durch das juristische System rutschen und nicht bestraft werden. Dabei spielt Dexter im Prinzip die Rolle eines *Müllentsorgers*, so sieht er sich selbst. Er tötet diese Menschen und lässt ihre Überreste im Meer verschwinden.

In der ersten Staffel wird Dexters Drang zu Töten thematisiert und analysiert. Sehr wahrscheinlich hat es mit einem Kindheitstrauma zu tun, er erlebte als kleiner Junge den Mord an seiner Mutter. Der Polizist, der schon eine Tochter namens Debra hat, adoptierte Dexter daraufhin. Debra wird später ebenfalls Polizistin und steht stets in einem engen, familiären Verhältnis zu Dexter.     
Als roter Faden die Staffel kam der *Kühllasterkiller* ins Spiel. Dieser entpuppte sich als Dexters Bruder, ebenfalls mit einem Drang zu Töten, aber im Gegensatz zu Dexter wahllos unschuldige Menschen.    

Die erste Staffel löste viele Diskussionen über die Moral der Serie aus. Tatsache ist: es gibt Menschen, die für ihre Verbrechen nicht bestraft werden, weil das System nicht fehlerfrei ist. Natürlich haben Mörder nicht den Tod verdient, aber wenn das System es versäumt sie zu bestrafen, was passiert dann? Das ist eine interessante Frage.     
Die Autoren der Serie haben mit Dexter einen *Racheengel* als Antwort geschaffen. Aufgrund seines Drangs, Dexter selbst nennt ihn seinen *dunklen Begleiter*, kann er nicht anders und kanalisiert das Töten eben auf diese Weise. Sehr oft wartet Dexter ab, bis die Täter wieder zuschlagen wollen und greift erst dann ein, rettet also sogar Leben.

Das ist die grundsätzliche Idee hinter Dexter. Wer moralische Bedenken hat, der kann die Serie nicht schauen, verpasst aber einige interessante Denkansätze und spannende Momente.

## Beziehung und Gefahr des Geheimnisses

Schon in der nächsten Staffel werden Dexters Unterwasserleichenberge gefunden und die Jagd nach dem Bay-Harbor-Metzger beginnt. Zu Hilfe kommt der Polizei von Miami der FBI-Agent Lundi, in welchem sich Dexters Halbschwester verliebt und mit dem sie auch eine Affäre beginnt. Gefährlich wird es für Dexter auch, weil sein Kollege Sergeant Doakes ihn verdächtigt und beginnt ihm hinterher zu schnüffeln.      
Des weiteren hat Dexter eine *richtige* Freundin. Rita hat zwei Kinder und einen Arschloch-Ex-Freund, den Dexter durch gefälschte (Drogen-)Beweise ins Gefängnis bringt. Dort stirbt dieser zwar bei einer Prügelei, aber Rita erfährt was Dexter getan hat und glaubt dieser sei drogensüchtig. In der Gruppentherapie lernt Dexter Lila kennen, die scharf auf ihn ist. 

Doakes findet Beweise bei Dexter zu Hause und will ihn zur Rede stellen, wobei er von Dexter in einem Käfig in einer abgelegenen Hütte gesperrt wird. Dexter hat eine Selbstmordkrise, merkt aber wie wichtig er für seine Schwester ist. Lila findet Doakes in der Hütte und töten ihn. Doch Dexter will seine neue Familie nicht für sie verlasen, worauf Lila die Kinder umbringen will. Die Staffel endet mit der Ermordung von Lila durch Dexter, in Paris.

## Ein Mitwisser

In der dritten Staffel stößt Dexter auf einen Mitwisser. Der Bezirksstaatsanwalt Miguel findet das, was Dexter tut ganz gut und hilft ihm auch dabei. Wobei sich dieser mit Dexters Kodex nicht anfreunden kann, was dazu führt, dass Dexter ihn am Ende tötet.      
Des weiteren baut Dexter sein öffentliches Leben weiter aus. Seine Freundin ist schwanger und beide heiraten auch. Dexter wird Vater. Seine Schwester Debra wird zum Detective befördert. Der Bösewicht war der Häuter, aber zu dem fällt mir jetzt nicht viel ein.

## Das Familienleben

Nach einem Zeitsprung von einigen Monaten sehen wir Dexter in der vierten Staffel als Vater eines Sohnes und wie er seine Mordlust mit dem Familienleben im Einklang zu bringen versucht. Debra und der FBI-Agent Lundi, der nun wieder auftaucht, beginnen eine Beziehung, die schnell von einem Anschlag beendet wird. Lundis tot nimmt sie ziemlich mit.       
Der Bösewicht ist diesmal der Trinity-Killer, der seit Jahrzehnten immer mal wieder mordet. Dexter findet schnell heraus wer dieser Mann ist und nähert sich ihm. Er sieht wie dieser sein Familienleben mit seinem Drang zu Morden im Einklang bringt. 

Im Finale dieser Staffel wird Dexters Frau das letzte Opfer des Trinity-Killers, bevor ihn Dexter im Meer verschwinden lässt.

## Ein Partner

In der fünften Staffel muss Dexter mit seinen Schuldgefühlen umgehen. Zudem laufen wieder einmal Ermittlungen, welche Dexters Geheimnis aufdecken könnten. Nachdem der erste Versuch sein dunkles Geheimnis mit einem Menschen zu teilen gescheitert ist, kommt es zu einer zweiten Begebenheit. 

Dexter rettet die junge Frau Lumen. Sie wurde von einer Gruppe von Männern entführt, die eben Frauen entführen, vergewaltigen und töten. Da Lumen weiß, was Dexter macht, möchte sie ihm helfen die restlichen Männer der Gruppe zu finden und zu töten. Im Unterschied zu Miguel aus der dritten Staffel, akzeptiert sie seinen Kodex. Besonders schön ist die Chemie zwischen den beiden Schauspielern.

Debra beginnt eine Beziehung mit ihrem Kollegen Quinn, welcher wiederum Dexter verdächtigt etwas mit dem Mord an seiner Frau Rita zu tun haben. Dexter kann wie immer Spuren verwischen und falsche Fährten legen. Am Ende sind alle aus der Vergewaltiger-Gruppe zur Strecke gebracht und Lumen verlässt Dexter wieder.