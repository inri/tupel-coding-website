---
layout: post
title: "Bachelorprojekt am HPI"
description: "Was ist das Bachelorprojekt überhaupt?"
date: 2011-08-15
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

In schnellen Schritten kommt das 5. Semester auf mich zu und damit auch das Bachelorprojekt. Eine generelle Beschreibung des Projekts findet sich auf der HPI-Seite:

*Eine wichtige Besonderheit unseres Studiengangs ist der Praxisbezug durch ein integriertes Bachelorprojekt. Es findet im fünften und sechsten Semester statt. Bearbeitet werden konkrete Aufgabenstellungen aus Industrie und Gesellschaft. Dabei lernen unsere Studenten, in einem Team von vier bis acht Personen mit verteilten Rollen strukturiert und zielgerichtet Softwarelösungen zu entwickeln und komplexe IT-Systeme zu beherrschen.*

[Siehe HPI Website](http://www.hpi.uni-potsdam.de/studium/studien_projekte/bachelorprojekte.html)

Welche Erfahrungen ich mit dem Bachelorprojekt machen werde, dokumentiere ich in den nächsten Monaten in diesem blog. Erstmal war aber die Auswahl der Projekte wichtig.    
Wir können uns keine eigenen Projekte ausdenken, sondern bekommen von den Lehrstühlen (pro Lehrstuhl mindestens ein, meistens zwei Projekte) eine Auswahl an Projekte. Viele befreundete Studenten anderer Unis blicken mir bei dieser Erklärung neidisch ins Gesicht. Aber nein, so wundervoll ist das nicht immer.

Mein Jahrgang hatte 13 Projekte zur Auswahl. Im schlechtesten Fall hat man damit eine Chance von 1:20 in sein Wunschprojekt zu kommen. Wir mussten bis letzte Woche unsere Top 5 abgeben und daraus werden den einzelnen Projekten die Studenten zugeteilt. Die Chancen sind natürlich in der Realität nicht so schlecht. Vor einem Jahr hatte ein zahlenmäßig starker Jahrgang seine Bachelorprojekte gewählt und nach den Zahlen, die wir *zugespielt* bekamen, war die schlechteste Chance nicht einmal 1:3. So ähnlich wird es auch dieses Jahr wieder sein, aber trotzdem bekommt man nicht unbedingt sein Traumprojekt.

Ich bin auf jeden Fall schon sehr gespannt, wie die Ergebnisse aussehen. Schlecht wird kein Projekt sein. Es bieten sich im Rahmen von zwei Semestern viele Möglichkeiten und die Projektbeschreibungen sind auch immer nur Anhaltspunkte.

Vor einigen Wochen haben Studenten aus dem letzten Jahrgang einen inoffiziellen Vortragsabend über ihre Projekte veranstaltet und das war höchst interessant. Zum einen konnten wir uns fachlich tiefergehende Informationen anhören, als bei der offiziellen Vorstellung, und zum anderen plauderten sie über organisatorische Bedingungen. Wie gut oder schlecht dieser und jener Lehrstuhl das Projekt betreut hat oder wie sich die Anforderungen mit der Zeit änderten.      
Eigentlich wollte ich etwas über einige Projekte schreiben, aber damit warte ich noch, bis ich weiß in welchem ich sein werde.