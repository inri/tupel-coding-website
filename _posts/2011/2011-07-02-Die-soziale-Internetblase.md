---
layout: post
title: "Die soziale Internetblase"
description: "Einige Gedanken zu Facebook und dem überschätzen von sozialen Netzwerken."
date: 2011-07-02
tags: [Facebook]
published: true
category: it
comments: false
share: true
---

Ich las einen kurzen Artikel auf macnews.de. Darin ging es um den Steve-Job'schen Führungsstil von Marc Zuckerberg und unter anderem kam dieser Satz darin vor: *Auch in der Forbes-Liste der reichsten Personen überholt Zuckerberg mit einem geschätzten Vermögen von 6,9 Milliarden US-Dollar den Apple-Chef, der nur 6,1 Milliarden US-Dollar zählt.* Ich dachte mir so: Das ist ja lächerlich.

Man hätte ruhig erwähnen können, dass Steve Jobs seit Jahren für nur einen Dollar bei Apple arbeitet. Ansonsten erhält er &quot;Geschenke&quot;, wie z.b. Aktienanteile. Der Wert eines Steve Jobs für Apple ist genauso unschätzbar wie ein Bill Gates für Microsoft es war. Aber ein Marc Zuckerberg für Facebook?      
Die Überbewertung von sozialen Netzwerken ist fatal und lässt uns auf die nächste Internetblase zusteuern. Facebook hatte im Januar einen Wert von 50 Milliarden Dollar . Der richtige Wert ist aber wesentlich geringer, denn die Nutzer spülen kein Geld in die Kassen und so viel Werbung gibt es auf Facebook auch noch nicht.

Mir macht es Sorgen, dass Zuckerberg mit solchen wichtigen Menschen der Informatikbranche verglichen wird. Er hat ein soziales Netzwerk gebaut, *so what?!* Die Idee dieser Art Netzes im Internet hat er nämlich nicht erfunden. MySpace gab es schon ein Jahr vorher (2003) und der deutsche Ableger StayFriends öffnete schon 2002 seine Pforten. Da steckte Zuckerberg noch tief in seiner Nerdwelt.      
MySpace richtete sich aber schon immer eher an Musiker und StayFriends war seiner Zeit zum einen voraus und torpedierte sich zum anderen selber mit dem Premium-Mitgliedschaften. Die kostenlose Variante von StayFriends ist witzlos und das Premium hat man mittlerweile bei StudiVZ und Facebook automatisch.

Zuckerberg hat etwas bestehendes gut umgesetzt und verbessert. Interessant ist die große Masse an Menschen, die Facebook anlocken konnte. Aber allein aus dieser Tatsache ergibt sich noch lange kein Milliardenwert, wo soll dieser auch herkommen? Wenn es um Werbung geht, dürfte man auch nur die Facebook-Mitglieder in Betracht ziehen, die auch in der Tat auf Facebook aktiv sind.      
Ich habe über 160 Facebook-Freunde, aber davon sind vielleicht ein Dutzend sehr aktiv und ein bis zwei weitere Dutzend ab und zu mal zu sehen, ich würde auf 20% tippen. Ich kann mir nicht vorstellen, dass der echte Wert der aktiven Facebook-Mitglieder sehr viel höher sein könnte. Wo findet man Zahlen dafür?

Man sollte gute Ideen wie Facebook kritisch betrachten und erforschen, was man damit so machen kann. So eine ungeheure Masse an Menschen war bisher nirgendwo so kompakt versammelt. Welche Möglichkeiten ergeben sich. Welchen Einfluss auf bestehende System hat das eventuell (Stichwort: Direkte Wahlen im Internet).     
 
Man sollte aber nicht kapitalistische Maßstäbe ansetzen und sich fantastische Geldsummen aus der Nase ziehen, die definitiv nicht fundiert vorhanden sind. Apple (Wert 300 Mrd. $ im Januar 2011) verkauft Hardware, Microsoft (Wert 240 Mrd. $ im Januar 2011) verkauft Software und Facebook verkauft nichts. Selbst Google (Wert 193 Mrd. $ im Januar 2011) ist mittlerweile auf vielen Märkten tätig und hat die Werbung im Internet revolutioniert. All diese Firmen sind seit Jahrzehnten im Geschäft und haben erst nach und nach diese Werte erreicht.

Wo steht Facebook in 10 Jahren?