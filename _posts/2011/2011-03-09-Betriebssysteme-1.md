---
layout: post
title: "Betriebssysteme I"
description: "Eine kurze Zusammenfassung zum Kurs Betriebssysteme."
date: 2011-03-09
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Ein Kurs über das Innere von Windows! Eines vorneweg: Das Thema ist in der Tat hochinteressant!     
Da es aber sowieso ein Pflichtkurs ist, muss ich niemanden überreden. Ich möchte einige Worte zum Aufbau und der Übungen schreiben, diese sind schon eine Sache für sich, und am Ende auf die Klausur zu sprechen kommen.

Die Vorlesung beschreibt den Aufbau von Windows und beinhaltet größtenteils Theorie. Die Übungen gibt es alle drei Wochen und sie fragen neben der ganzen Theorie auch einen harten Anteil Praxis ab. Die Übungen können in Gruppen von bis zu drei Studenten bearbeitet werden und das ist auch bitter nötig. Anfangs muss man sich mit dem Windows Research Kernel herumschlagen, sich also einen eigenen Windows-Kern kompilieren. Später sind es dann Programmieraufgaben in diesem WRK (in C natürlich). Und genau die haben es in sich.

Die meiste Zeit wird man mit Suchen in der Doku zu tun haben. Man hantiert mit Handles und seltsamen Methodenbezeichnungen rum und grübelt darüber, welche Systemaufrufe welche Effekte haben oder noch weiter verzweigt sind. Es ist eine mühselige Aufgabe und in nur geringer Weise produktiv für unser Verständnis, darin liegt meine Kritik. Sei es nun das Programmieren oder das Verständnis um die Funktionen in Windows.

Umständlich wird es vor allem, weil man keine schicke Programmierumgebung wie Eclipse hat. Die C-Programme und Funktionen, die wir in den Kern bauen, müssen wir durch Neustart testen und bekommen kaum hilfreiche Fehlermeldungen. Ich weiß allerdings auch nicht, wie man sich alternativ an den Windowskernel tasten kann.

Die Klausur ist wieder theoretischer. Witzige Geschichte: Man zeigte uns in der Vorlesung eine Klausur aus dem Vorjahr und natürlich wurden einige Schnappschüsse angefertigt und mit OCR in PDF gepresst. Die Klausur bestand aus einigen Multiple-Choice-Fragen und einem Aufgabenteil. Es wurden einige Betriebssystemziele, Schedulingverfahren mit praktischer Aufgabe, Memory Management (Pagelisten...), Synchronisierungsprobleme auch mit jeweiligen Aufgaben, und Security/Zugriffszeug gefragt.         
Oh, das war ja unsere Klausur! Die habe ich dann wohl verwechselt... und warum? Weil die beiden sich sehr ähnlich waren! Es fehlten zwar einige kleine Aufgaben, aber die großen waren bis auf Kleinigkeiten 1:1 übernommen worden. Wie ärgerlich, ich habe so unglaublich viel über Windows gelernt und musste einen lächerlichen Bruchteil nur anwenden.    
Schade, oder auch nicht. So soll es ja sein!

Eine furchtbar interessante Vorlesung bzw. eine Vorlesung mit furchtbar interessantem Stoff, sehr, sehr zeitaufwendigen Übungen und einer fairen Klausur.