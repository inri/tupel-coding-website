---
layout: post
title: "Old School vs. Mobil"
description: "Was sind die unterschiede zwischen Old School und mobilen Geräten und warum werden letztere nicht so schnell verschwinden."
date: 2011-08-08
tags: [Technologie]
published: true
category: it
comments: false
share: true
---

Wenn man mal 5 Minuten durch diesen Blog klickt, merkt man schnell: *Der interessiert sich für mobile Technik.* Das ist korrekt. Ich mag mobile Geräte sehr gerne. Warum das so ist, werde ich gleich erläutern.
 

Nicht jeder meiner Kommilitonen teilt dieses Interesse, einige können es kaum nachvollziehen. Ab und zu gibt es Diskussionen und es kommt zur Glaubensfrage *Old School vs. Mobil*.

Old School sind für uns dabei die Computer, wie man sie seit zwei Jahrzehnten kennt. Diese Kisten mit den vielen Kabeln dran, welche mindestens zu einem Monitor, einer Tastatur und einer Maus führen. Im Laufe der Zeit kamen einige Kabel für externe Festplatten, Internet, Drucker, usw. hinzu. Old School sind auch noch Laptops und Notebooks. Das sind im Allgemeinen die Systeme, mit denen man die ersten Texte schrieb, erste Spiele zockte und das Internet entdeckte. 

Mobil waren bei weitem noch nicht die ersten Mobiltelefone Ende der 90er Jahre. Das fing vielleicht erst vor circa sechs Jahren an, als man begann Mobiltelefone Smartphones (= Alleskönner) zu nennen. Mit dem iPhone etablierten sich kleine Programme, genannt Apps, auf mobilen Geräten. Android erschien ebenfalls zeitnah und zusammen wuchs man zu einer neuer Gattung von Computern: mobile Computer. Das iPad machte den Spagat zwischen den kleinen Handys und den grossen Computern / Notebooks und zeigte dabei auch in welche Richtung es in Zukunft gehen wird.

## Der weitere Verlauf

Ich prognostiziere, dass in 10 bis 20 Jahren in den Industrieländern der Anteil der Old School Geräte enorm zurückgehen wird. Das beginnt schon in diesem Moment. Am längsten werden sich dabei die Notebooks halten (vor allem dünne und leichte, siehe MacBook Air), aber normale Computer werden im privaten Umfeld völlig aussterben und nur noch dort benutzt wo sie notwendig sind. Bildschirme nicht unbedingt, aber stationäre Rechner, die großen Kisten, Tower, auf jeden Fall.

Die Hardware ist in den letzten Jahrzehnten immer leistungsstärker geworden. Seit einigen Jahren stagnieren aber die Leistungen einzelner Rechenkerne und man begann mehrere Kerne zusammen arbeiten zu lassen. Noch dazu machte man die Hardware immer kleiner und so kommt es, dass ich mit einem HTC Desire auch gleichzeitig einen kleinen Computer in der Tasche herumtrage und der ist mit seinen 1 GHz nicht zu unterschätzen. Wobei das schon ein alter Hut ist, denn in den nächsten Monaten werden uns die Mehrkernprozessoren im mobilen Bereich überrollen.

Ich sage also: Old School wird in den nächsten Jahrzehnten fast vollständig von Mobil ersetzt. Mobil werden dabei vor allem Tablets sein. Spätestens bei dieser Aussage fragen sich konservative Menschen, wie das denn gehen solle. Man ist seit Jahren an die Computer gewohnt, wie sollen Tablets sie einfach so ersetzen? Der Mensch ist ein Gewohnheitstier. Ein großer Teil der Bevölkerung kann leidlich mit den Old School Rechnern umgehen und nun kommen schon wieder neue Maschinen? Ja.

## Simple Einsatzgebiete

Das neue iPad und die neuen Android-Tablets sind schon sehr leistungsstark und für die alltäglichen Zwecke gemacht. Emails schreiben, im Internet surfen, Videos anschauen, sich über irgendwas informieren, Spiele zocken. Ich kann das gerne hier im einzelnen aufdröseln.    

Ein Großteil der Emails heutzutage hat nicht mehr viel Inhalt. Man schreibt sich immer kürzere Nachrichten, vor allem auch über soziale Netzwerke. Für diese Art Nachrichten braucht man doch keinen großen Computer mit Tastatur und so weiter. Schön aufs Sofa lümmeln und los gehts. Von Twitter bis Facebook gibt es für alles mindestens eine App. Und sehr bald werden wir mit unseren Geräten reden und ihnen die Texte diktieren, wie in Star Trek.

Durch das Touchscreen-Interface erlebt man u.a. das Internet auf eine ganz neue Art und Weise. Vor allem für Menschen, die in den letzten 10 Jahren nicht so ans Web herangekommen sind, ermöglicht das neue Ansätze. Youtube und Co sind nur einen Wisch entfernt. Wenn man mal Spielfilme schauen möchte, kann man das Tablet mit dem Fernseher oder großen Monitor verbinden, das wird in absehbarer Zeit hoffentlich problemlos und drahtlos passieren.      
Oder muss man mal wirklich einen längeren Text schreiben, eine Bewerbung oder so, dann setzt man sich an den Schreibtisch, wo der Monitor steht, nimmt sich die drahtlose Tastatur und schreibt einfach. Für solche einfachen Arbeiten ist kein grosser PC vonnöten.

Spiele? Ich meine nicht nur die coolen Puzzlespiele wie Angry Birds, sondern auch richtige 3D-Spiele. Die ersten Spieleschmieden entwickeln jetzt schon sehr schicke Spiele für die Tablets. Aber das ist noch nicht das Ende der Entwicklung. Es wird in einigen Jahren auch möglich sein, via Tablet auf großen Bildschirmen 3D-Spiele zu spielen. Das Spiel an sich läuft dann nicht auf dem Tablet, sondern auf einem Server irgendwo in der Cloud und zum Benutzer werden die Bilder gestreamt. Unglaublich coole Sache, die aber wesentlich schnellere Internetleitungen benötigt.

## Einfache Bedienung und Funktion

Viele wichtige Funktionen erfüllen die Tablets heute schon und sind deshalb nicht ohne Grund enorm beliebt bei den Nutzern. Wer immer noch widerwillig den Kopf schüttelt, der stelle sich mal einen Menschen mittleren Alters vor, der vielleicht in seinem ganzen Leben ein Dutzend Mal mit einen Computer in Berührung gekommen ist. Allein schon dieses Eingabegerät mit seinen teils kryptischen Tasten wirkt einschüchternd. Dieser Mensch hat dann auch noch einen Job, 40 Stunden in der Woche, und möchte sich abends nicht noch stundenlang mit dieser Maschine auseinandersetzen.
     
Es soll verdammt noch mal bequem funktionieren und das möglichst schnell. Das war in der Vergangenheit nicht immer selbstverständlich. Natürlich wundern wir Informatiker und Netzmenschen uns über diese &quot;*normalen*&quot; Menschen.     *Ist doch alles ganz einfach mit dem PC.* Nein, ist es nicht! Selbst junge Menschen verzweifeln an einigen Programmen und Funktionen. Ich habe auch erst jahrelang lernen müssen und das geht den meisten Menschen so. Aber man kann nicht verlangen, dass jeder sich die Zeit dafür nimmt. 

Die Systeme wurden technisch stets besser, aber nicht ihre Bedienung. Oder anders beschrieben: Ein Computer war schon immer ein Gerät, welches man sich gezwungenermaßen zulegen musste. Ansonsten sind es doch nur die in der IT-Branche berufstätigen Menschen und die Hobby-Informatiker, die sich gerne einen Computer kaufen (wegen mehr Leistung, neuen Möglichkeiten,...).      
Für ältere Menschen (60+) erfordert es sogar große Neugier sich mit dem Computer zu beschäftigen. Das meiste ist ihnen nicht intuitiv klar und muss viele Male wiederholt und geübt werden. Einige beschäftigen sich gar nicht mehr mit dieser Thematik, andere haben PCs nur irgendwo in der Ecke stehen, damit sie mal einen Text schreiben und ausdrucken können. Man kann nicht behaupten, dass diese Menschen in Hinblick auf die Computertechnik aktiv wären.

Aber genau auch diese Menschen muss man von dort abholen, wo sie sich befinden. Ein immer größer werdender Anteil unseres Lebens spielt sich virtuell ab und alle Menschen sollten einen Zugang daran haben. Markttechnisch sind das auch potentielle Kunden.     
Die Old School-Geräte haben es trotz ihrer rasanten, technischen Entwicklung und fallender Preise nicht geschafft, diese Bevölkerungsgruppe für sich zu gewinnen. Wer behauptet, ein Windows- oder Linux-Rechner sei doch einfach zu bedienen, der steckt schon so tief in der Informatikwelt, dass er einfach nicht mehr die Realität erkennt.

Apple legt seit Jahren großen Wert auf gute Bedienung und hat sein gesamtes Wissen in ein absolut bedienungsfreundliches Gerät gegossen. Und genau aus diesem Grund verkauft sich das iPad auch wie geschnittenes Brot, obwohl es eine neue technische Sparte geschaffen hat und das vor gerade mal 16 Monaten. Wer diesen Fakt ignoriert, ist nicht nur in seiner Nerdwelt eingeschlossen, sondern leidet vermutlich an Realitätsverzerrung.

## Rechner für mich, Tablet für dich 

Wer wird in einigen Jahrzehnten noch einen PCs benutzen? Ich auf jeden Fall. Ich bin Informatiker und Computer sind meine Werkzeuge. Das trifft bspw. auch auf Designer zu und teils auch auf Büromenschen, wobei man da auch ganz verrückte Ideen entwickeln kann (man biete den Firmen ein technisches System, welches billiger ist, alles kann, was das alte System konnte und womit die Arbeitnehmer besser zurechtkommen und deshalb produktiver sind). 

Beim Designer kann man auch diskutieren. Wie viel Rechenleistung braucht man? Ist ein Touch-Interface vielleicht nicht besser? Oder eine Mischung? Generell werden Computer in ihrer heutigen Form zu Werkzeugen, denke ich. Die Gamer werden den Rechnern auch noch einige Zeit treu bleiben. Zum einen bietet ein stationärer Rechner noch die beste Rechenleistung und zum anderen sind Maus und Tastatur gute Steuermöglichkeiten.      
Wenn in einigen Jahren die ersten Cloud-Spiele erhältlich sind und ein Jahresbao Rechenleistung deutlich weniger kostet als ein neuer Computer, war es das auch dann mit den Gamern. Tastatur und Maus kann man sicherlich auch mit Tablets gut verbinden.

Schaut euch an, wie die Tablets in den nächsten Monaten den Markt erobern. Die aktuellen Zahlen weisen in eine deutliche Richtung. Die Prognosen wurden in letzter Zeit nur dahingehend verändert, dass die Verkaufs- und Umsatzzahlen für mobile Geräte nach oben und für sonstige Computer nach unten korrigiert wurden.

P.S.  Mir ist völlig klar, dass das provokative Thesen sind. Und es lesen vermutlich größtenteils Menschen diesen Artikel, die schon länger in der Welt der Informatik tätig sind. Bei großen Bedenken kann ich nur raten: Versetzt euch mal in die Situation eines Menschen, der mit unserer Welt nichts zu tun hat. Besucht eure Großeltern und zeigt ihnen mal ein Notebook. Kommt mit Menschen in Kontakt, für die diese Art der Technik Neu- oder Fremdland ist.