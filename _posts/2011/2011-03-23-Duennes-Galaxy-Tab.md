---
layout: post
title: "Dünnes Galaxy Tab"
description: "Einige Gedanken zum neuen Galaxy Tab."
date: 2011-03-23
tags: [Samsung]
published: true
category: it
comments: false
share: true
---

Gestern wurden neue Galaxy Tabs von Samsung vorgestellt. Das &quot;alte&quot; 10.1er wurde wohl eingestampft und an seiner Stelle traten ein 8.9er und ein neues 10.1er - und beide sind dünner als das iPad. Zwar nur einige Millimeter, aber immerhin: **Dünner als das iPad!**       


 
Auch bei den anderen Spezifikationen hat man sich anscheinend am iPad orientiert und das sollten alle Hersteller so halten. Akkulaufzeit und Gewicht sind ähnlich oder gleich, man ist bestimmt richtig stolz bei Samsung.

Doch was ist davon zu halten? Erstmal halte ich nichts davon, weil die neuen Galaxy Tabs erst im Frühsommer erscheinen sollen. Was heißt das genau? Mai, Juni? Bis dahin ist das neue iPad 2 schon wieder monatelang auf dem Markt und daneben vermutlich auch andere Tablets (mit Android). Bei den Vergleichen mit diesen Honeycomb-Tablets wird das iPad 2 besser abschneiden und das macht es für die Samsung-Tabs nicht leichter.      

Des weiteren wundern mich die Größen, 8,9 Zoll und 10,1 Zoll liegen doch sehr nah beieinander. Kurz nach meinem Wundern kam die Erkenntnis und dabei verweise ich auf meinem Android-Analyse-Artikel. Was bezweckt Samsung mit diesen Größen? In die Hosentasche passt nicht einmal ihr erstes Tab mit seinen 7 Zoll. Die 100 g Gewichtsunterschied sind auch nicht so marginal. Aber für Menschen, die sich fragen, ob ein 10 Zoll Tablet vielleicht nicht zu unhandlicher, schwer, usw. ist, stellt diese Größe einen tollen Verblendungseffekt dar.

Samsung hat diese beiden Größen im Angebot, weil dadurch mehr Geräte am Markt sind bzw. Samsung mehr Geräte anbieten kann. Das ist das typische Verhalten, wie wir es auch schon bei den Handys erlebt haben: möglichst viele Geräte am Markt, jemand wird sie schon kaufen.

Ich denke rein technisch besteht zwischen den 8,9er und 10,1er kein großer Unterschied, also für Samsung ein Klacks noch einen kleineren Bildschirm reinzubauen. Vom optimalsten Design her ist das sicherlich umstritten. Apple bevormundet den Käufer seiner Produkte. Die Auswahl ist meistens beschränkt und lässt nur wenige Freiheiten offen. Dafür funktioniert alles und das Design ist gut durchdacht. Apple hat bestimmt nicht ausgelost, wie groß ihre iPads werden sollen. Es gibt in Cupertino garantiert iPad-Prototypen mit anderen Bildschirmgrößen, aber nach internen Tests entschied man sich für die 9,7 Zoll Variante.

Ist Samsung nun der große Heilsbringer, der den Benutzer die Wahl lässt oder geht es hier nur um eine gute Positionierung am Markt, der bald von 10 Zoll Tablets überschwemmt wird?

Hoffentlich wird das Galaxy Tab dann auch gründlich untersucht und herausgefunden, wieso es so dünn ist. Apple hat z.b. die Glasdicke verändert und man hat schon Angst, dass dadurch das Glas leichter zerbricht. Ob bei Samsung designtechnisch alles gut durchdacht ist?