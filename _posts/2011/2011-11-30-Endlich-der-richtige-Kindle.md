---
layout: post
title: "Endlich der richtige Kindle"
description: "Ein Vergleich zwischen iPad und Kindle, der gar kein Vergleich ist."
date: 2011-11-30 
tags: [Amazon, iPad, Ebooks]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-kindle-lg.jpg
  credit: Amazon
  creditlink: http://www.amazon.com
---

Vor einigen Monaten habe ich aus Interesse einen Amazon Kindle bestellt. Das war die 3. Generation und sie war auf jeden Fall beeindruckend. Zwei Dinge störten mich aber damals besonders: Zum einen die kleine Tastatur, die sich am unteren Ende des Kindles befand, und zum anderen mein iPad bzw. dessen Existenz. 


Ich mag mein iPad sehr, aber da ich eben eines besass, war der Kindle überflüssig oder konnte mir einfach nicht genug bieten.

## Kindle passt in eine Hand

Amazon hat die Kindle Reihe erneuert, den Preis gesenkt und einen neuen Kindle (Fire) als Android-Tablet ins Rennen geschickt. Ich wollte mir wieder einmal den Kindle anschauen und den gibt es nun für 99€ (Preis um 40€ gesenkt).    
Mein erster Gedanke beim Auspacken war: Sie haben auf mich gehört! Die Tastatur ist nun Vergangenheit. Es finden sich *nur* noch fünf Tasten auf der Vorderseite und jeweils zwei Tasten an den Seiten (für das Umblättern).    
Alles andere am Kindle ist wie erwartet. Er ist sehr leicht, hat einen wunderbar lesbaren Bildschirm und passt sogar in eine mittelgroße Hand.

Der Kindle in seiner momentanen Ausstattung ist genau so, wie ich mir die E-Reader auch in Zukunft vorstellen würde. Die Bildschirme werden noch farbig und die Geräte noch ein wenig leichter. Aber am Formfaktor muss man nichts mehr ändern. Auch ein Touchscreen scheint mir nicht notwenig, denn so viel interagiert man mit seinem E-Reader nicht. Außerdem kann man ruhig mal einen Finger auf den Bildschirm legen, ohne dass man gleich ein Umblättern auslöst.     
Allerdings werden sich vermutlich Touchscreens auch in diesem Bereich durchsetzen, denn einen Kindle Touch gibt es in Amerika schon und z.b. die Sony Reader reagieren auch auf Berührung.

## Kindle Füttern

Den letzten Kindle habe ich nicht benutzt. Innerhalb einer Woche wurde er zurückgeschickt und damit hatte sich das Thema vorerst erledigt. Mit dem neuen Kindle habe ich mittlerweile schon viele Seiten gelesen und ich bin begeistert.      
Anfangs hatte ich Sorgen, wie ich den Kindle mit Inhalt füttere. Ein Kommilitone sagte mir, der Kindle sei nur Amazon-beschränkt und man müsse ihn umständlich hacken. Das ist aber Quatsch.

Eine Möglichkeit ist natürlich über Amazon die EBooks zu kaufen. In Deutschland gab es bisher aber noch immer keine Anpassung der Preise, so dass man bestenfalls wenige Euro Unterschied zum richtigen Buch hat.     
Falls ihr Bücher im Epub- oder Mobi-Format auf dem PC habt, dann ist es kein Problem sie auf den Kindle zu transportieren. Die Lösung heißt: Calibre. Das ist ein Verwaltungs- und Konvertierungsprogramm für Textdateien (PDF, Epub, Mobi,…) und es läuft auch auf allen gängigen Betriebssystemen.

Nach der Installation lädt man die Bücher in das Programm, kann sie noch ordnen oder die Metadaten ändern und natürlich auch konvertieren. Im letzten Schritt verbindet man den Kindle über USB an den Rechner und kann die Bücher ganz einfach darauf kopieren.      
Wenn man wie ich, Epubs kopieren möchte, müssen diese vorher in das Mobi-Format umgewandelt werden. Das macht Calibre vor dem Kopieren von ganz alleine und das geht auch recht zügig. Epubs sind funktionell mit den Mobi-Dateien vergleichbar. Deshalb ist keine große Konvertierung nötig, denke ich.

## PDFs druckt man!

PDFs kann Calibre auch Konvertieren, aber mit PDFs ist das so eine Sache. Man sollte zuerst akzeptieren, dass der Kindle zum Lesen von Texten geschaffen wurde. Dabei ist der Text auch skalierbar, wie es die Augen gerade benötigen. Lediglich Bilder sind statisch und stets gleich groß.    
PDFs wurden geschaffen, um Inhalte auszudrucken. Das bedeutet, es kann unter Umständen sein, dass man eine PDF-Datei nicht 1:1 konvertieren kann. PDFs haben oft ein bestimmtes Layout, bestimmte Schrift und bestimmte Felder, die keine Bilder sind, aber auch kein Text.

Beim iPad hatte ich auch Probleme mit dem Konvertieren zu Epub und mache es auch nicht mehr. Jeder, der sich eine zeitlang mit einem iPad oder E-Reader beschäftigt hat, kommt wahrscheinlich zu der Erkenntnis, dass gewisse Dinge so sind, wie sie sind.     
Einige wünschen sich eine Darstellung von PDFs auf einem kleinen Gerät wie dem Kindle. Aber das ist Quatsch. Das Gerät ist viel zu klein um solche Dateien gut darzustellen.     Natürlich könnte man ein Zoomen integrieren, aber das ständige Klicken geht irgendwann auf die Nerven und dann nutzt man es sowieso nicht mehr.      
Selbst auf dem iPad sind PDF-Dateien nicht immer schön lesbar. Auch wenn man mit Fingern rein- und rauszoomen kann, ist das ein erheblicher Verlust im Lesevergnügen, im Vergleich zur eigentlichen EBook-Reader App iBook.

Ich werde das Konvertieren von PDFs noch testen, verspreche mir aber nicht viel davon.

## iPad vs. Kindle

Als ich den letzten Kindle in der Hand hatte, hätte ich keine Sekunde über diese Gegenüberstellung nachdenken müssen. Mittlerweile zeichnen sich gewisse Unterschiede ab. Ein Kindle ersetzt selbstverständlich kein iPad, aber ein iPad auch nicht unbedingt ein Kindle.    
Der Kindle ist weniger als halb so groß wie das iPad und wesentlich leichter (600 g vs. 170 g!). Er ist damit auch leichter als ein Taschenbuch! Die Frage ist nicht, ob iPad oder Kindle, sondern viel mehr: Buch aus Papier oder Kindle. Ich wünsche mir sehnsüchtig, dass ich alle Bücher, die ich gerade in meinem *to-read*-Schrank liegen habe, auf dem Kindle hätte. Das wäre super.

Der Kindle lässt sich besser halten als manche Bücher. Die Lesbarkeit des Kindle-Bildschirms ist ebenfalls mindestens genauso gut, wenn nicht sogar besser (da man eben die Schriftgröße einstellen kann und ich so z.b. auch mal ohne Brille lesen kann). Noch dazu kommt die bequeme Möglichkeit des Buchkaufs (durch integrierten Amazon-Shop) und die große Auswahl an Büchern auf einem Gerät. Das organisieren meiner Bücher mit Calibre ist auch nicht schwer. Und dass der Stromverbrauch minimal ist, muss ich auch nicht erwähnen, oder?

Macht ein iPad den Kindle überflüssig? Der Kindle ist meiner Meinung wirklich nur zum Lesen von Text sinnvoll. Also Romane, Sachtexte und so, keine Magazine oder Zeitungen. Dafür müssten die Verlage ihre Inhalte entsprechend simpler aufbereiten und zur Verfügung stellen.     
Auf dem iPad habe ich mit iBooks die gleiche Funktion, aber nicht das gleiche Gerät. Ich lese zwar auch gerne mit dem iPad, aber das höhere Gewicht und der Formfaktor machen sich schon bemerkbar. Dagegen kann man auf dem iPad zumindest PDFs gut erkennen, auch wenn dieses Format einfach eine Einschränkung ist.

## Fazit

Heute musste ich zehn Minuten warten bis die S-Bahn losgefahren ist. Ein Griff in die Tasche, Kindle raus und lesen. Dabei strahlte die Sonne auf den Bildschirm, wodurch die Schrift gleich noch viel besser aussah. Es war ein anderes, angenehmeres Gefühl in diesem Moment mit dem Kindle zu lesen. Mein iPad hätte ich zum Lesen nicht aus der Tasche geholt.