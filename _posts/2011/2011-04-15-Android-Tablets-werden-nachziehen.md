---
layout: post
title: "Android-Tablets werden nachziehen?"
description: "Wieso ich nicht glaube, dass Android Tablets das iPad in naher Zukunft einholen können."
date: 2011-04-15
tags: [Android]
published: true
category: it
comments: false
share: true
---

In meinem Bekanntenkreis bin ich derjenige, der sich am meisten mit der Technologie rund um Tablets beschäftigt. Ich erlese mir Informationen aus zahlreichen Quellen und bilde mir eine Meinung. Deshalb bin auch meist ich derjenige, der die anderen über Neuigkeiten informiert, eher selten wird mir etwas neues verraten.      


Heute saß ich unter anderem mit einem Freund zusammen, der sich zwar für Technik interessiert, aber für den das eben nur ein Interesse ist. Er verfolgt nicht jeden Pups eines iPads, aber er weiß, dass es das iPad gibt und auch Tablets mit Android kommen werden.

Dieser Freund konfrontierte mich mit einer Meinung bzgl. Tablets, die mir zwar nicht neu ist, ich aber seit einigen Monaten schon gar nicht mehr weiter verfolgt habe. 

&quot;*iPads sind nur dieser schnelle Hype, oberflächlich produzierte Geräte, usw. Android Tablets werden bald nachziehen und dann die iPads hinter sich lassen.*&quot;

Das war sinngemäß seine Aussage. Genauso dachte ich auch noch vor einem Jahr. Damals kam das erste iPad heraus und war in seiner Art und Weise völlig neu. Apple macht sehr gute Produkte, ist aber eher selten der Initiator einer völlig neuen Technik. Sie verbessern bestehende Geräte und machen sie effizient und schick. Aber das iPad war die Ausnahme.

## Vergangenes Jahr

Das iPad stellte etwas Neues dar. Alle anderen Tablet-artigen Geräte zu dieser Zeit, waren aufgrund ihres Betriebssystem nicht zu bedienen oder einfach nicht mobil, dank großen Gewicht oder 1-stündiger Akkulaufzeit. Auch die Touchscreens gibt es erst seit knapp 1<sub>1</sub>/<sub>2</sub> Jahren als kapazitive Version.     
Auf was hätte Apple warten sollen? Ihr iOS-System hatte sich auf iPods und iPhones bewährt, die Hardware konnte man ab diesem Zeitpunkt endlich relativ kostengünstig verbauen und so feuerten sie das iPad auf die Technikwelt.

Sehr schnell wurden Apps auf das iPad angepasst und neue Ideen für den größeren Bildschirm umgesetzt. Die Entwicklergemeinde hatte nun Zugang zu einer Goldgrube! Etwas anderes ist es ja nicht. Zu gleicher Zeit wurde ein deutsches Tablet angekündigt, das weTab / wePad. Darüber schrieb ich ja schon, die Veröffentlichung zog sich ewig hin und als es dann endlich zum Verkauf stand, war die Software unendlich schlecht und das weTab ein Flop.

Interessant war zu sehen, dass es selbst ein halbes Jahr nach dem iPad kein Hersteller gab, der nachzog. Die Technik war zu haben, auch das weTab war technisch nicht schlecht. Aber eine gute Grundlage (= Betriebssystem) für die Apps zu programmieren, war nicht so einfach. Das erfordert Zeit und Vorbereitung und kein Hersteller hatte diese Kapazitäten frei, man wartete einfach auf Google. Sicherlich war das iPad von Anfang an nicht perfekt, aber als die ersten Kunden endlich dieses neuartige Gerät in den Händen hielten, bekam Apple Feedback und verbesserte die Software ständig weiter.

Während dessen arbeitete Google daran, den Vorsprung Apples aufzuholen. Als Apple nun nach einem Jahr die nächste Version des iPads vorstellte, schaffte es Google gerade mal, eine auf Tablets angepasste Android Version zu veröffentlichen. Diese ist sicherlich genauso unperfekt wie das iOS vor einem Jahr und vor allem gibt es noch keine Geräte, die dieses System nutzen. Okay, es gibt das Xoom, aber auf diesem ist die Implementierung nicht sehr toll und verkauft wurden auch erst eine handvoll Exemplare.

Apple hat bisher ca. 3 Millionen iPads unter die Menschen gebracht, sie beherrschen den Markt der Tablets völlig und die Konkurrenz bringt erst in den nächsten zwei Monaten ihre Geräte auf den Markt.      
So sieht die aktuelle Situation bis heute aus. Das allein müsste ausreichen, um die Behauptung bzw. Hoffnung zu relativieren. Apples Vorsprung ist in den nächsten Monaten nicht einzuholen. Man bräuchte sofort eine genauso gut angepasste Plattform und billigere Geräte.

## Nahe Zukunft

Wie geht es vielleicht weiter? In einigen Wochen sind die ersten Android-Tablets auf den Markt. Man wird ihnen bestätigen, dass sich die reine Hardware in Sachen Leistung mit dem iPad messen kann, aber die Tablets momentan noch zu wenig Apps und bestimmt auch einige Ecken &amp; Kanten haben werden. Gegen Ende des Jahres gibt es unter Umständen so viele Apps wie das iPad jetzt schon zur Verfügung hat. Aber mit der gleichen Qualität? Ich denke nicht, aber schön wäre es. 

Dann wird Google die nächste Android-Version für Tablets herausbringen, welche die besagten Ecken &amp; Kanten ausbügeln kann. Der App Markt wächst weiter und im nächste Frühling werfen die nächsten Hersteller ihre zweite Generation auf den Markt, genauso wie Google eine neue Android-Version, die auf den Geräten der ersten Generation der Android-Tablets aber erst nach zwei Monaten implementiert wird. In dieser Zeit kann man eben keine Apps mit den coolen Features benutzen.

Ach ja! Die andere Seite fehlt noch: Apple wird sein System weiter verbessern und mit den iOS-Updates sogar noch das erste iPad unterstützen. Die Apps werden weiterhin zahlreicher und qualitativ besser. Und in einem Jahr wird Apple die dritte Generation vorstellen, die technisch sicherlich besser wird und vom Design her die anderen Herstellern deklassiert.      
Mit den ganzen Gewinnen aus dem iPad wird Apple wahrscheinlich weiter neue Technologien finanzieren. Man spekuliert ja jetzt schon mit einer Anbindung an die Cloud. Es wird etwas sein, womit die anderen Hersteller nicht gerechnet haben und dass sie dann schnell versuchen werden zu kopieren.

Ich wüsste gerne wie groß die Kapazitäten der Hersteller für eine Tablet-Entwicklung ist. Ich denke, Apple hat mittlerweile einen richtig großen Apparat hinter dem iPad sitzen und wird daher den anderen immer eine Nasenlänge voraus sein. Das beste wäre es, wenn die Hersteller auch technisch zusammen arbeiten und ein gemeinsames Tablet herausbringen. Wegen mir in drei verschiedenen Größen, aber dafür auch ein richtig gutes Ding. Auch muss dieser ganze Android-Versionen-Quatsch aufhören.
Vielleicht schafft Android dann doch noch ein größeres Stück des Tablet-Kuchen abzubekommen.