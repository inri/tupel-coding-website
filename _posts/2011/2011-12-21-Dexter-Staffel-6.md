---
layout: post
title: "Dexter, Staffel 6"
description: "Ein Review der 6. Staffel von Dexter"
date: 2011-12-21
tags: [Serie]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-dexter-season-6-lg.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/dexter/home
---

Nach dem Ende der 6. Staffel Dexter gab wieder einige Diskussion. Aus verschiedenen Ecken war Kritik zu vernehmen, die Serie würde ihr hohes Niveau an Unterhaltung verlieren. Ich bin nicht dieser Meinung und werde zum einen Dexters bisherige Geschichte kurz umreissen und ein Staffelreview abgeben.

## Religion 

Im Unterschied zu den vorherigen Staffeln, steht Dexter nicht mehr allein im Vordergrund. Es wird viel Gewicht auf die Entwicklung seiner Schwester Debra gelegt. Sie wird zum Lieutanant ernannt und leitet eine komplette Abteilung. Dadurch geht ihre  Beziehung zu einem Arbeitskollegen in die Brüche und sie steht generell unter großem Druck.      

Nachdem man gezeigt hat, wie Dexter mit seinem *Dunklen Begleiter*, seinem Doppelleben, seiner Schuld und mit Menschen umgeht, die sein Geheimnis teilen, liegt der Fokus in dieser Staffel auf Dexters Umgang mit der Religion. Relativ zeitig wird aber klar, dass Religion keine Option für Dexter ist.     
Diese frühe Erkenntnis drückt natürlich die Spannung, denn ab der Hälfte der Staffel jagt Dexter *nur* noch den Doomsday-Killer. Erst gegen Ende deuten sich große Ereignisse an.
Ein Praktikant des Polizeireviers führt etwas in Schilde und schickt Dexter ein aus der Aservatenkammer gestohlenes Beweisstück des Kühllasterkiller-Falls.     
Debra gesteht sich ein, dass sie in ihren Stiefbruder verliebt ist und entdeckt ihn am Ende, wie er den Doomsday-Killer tötet. In dieser Staffel war Dexters Geheimnis gar nicht so sehr gefährdet, wie in den früheren Staffeln teilweise, aber am Ende gab es für den Zuschauer doch die Überraschung.

Insgesamt war das sicherlich eine schwächere Staffel von Dexter, aber wenn man sich den groben Handlungsverlauf näher betrachtet, war sie wahrscheinlich notwendig. Dexter hat sich in jeder Staffel weiterentwickelt. Sein *dunkler Begleiter* wurde ausgiebig reflektiert, die Polizei war ihm mehrmals dicht auf den Fersen, ein anderer Serienkiller hat seine Identität entdeckt, er hat sein Geheimnis einmal *erfolglos* und einmal *erfolgreich* mit einem anderen Menschen geteilt. Was sollte jetzt noch kommen?

Es fehlt ein Mensch, der Dexters Geheimnis dauerhaft teilt. Doch genau diese Handlung konnte man nach der fünften Staffel nicht einfach so einfügen, denn zu diesem Zeitpunkt gab es diesen Menschen noch nicht. Erst mit dieser Staffel hat sich Debra zu diesem Menschen entwickelt. Man kann sich fragen, wie Debra wohl in der siebten Staffel reagieren wird, doch ich denke die Antwort ist klar. Zum einen liebt sie Dexter, wahrscheinlich sogar bedingungslos, zum anderen braucht sie ihn und außerdem hat sie auch schon in der vorherigen Staffel Sympathie für den Rächer gezeigt.

## Ausblick
   
Ich denke, wir werden in der nächsten Staffel sehen, wie Dexters Leben mit einem gleichberechtigten Partner aussehen wird. Dazu wird sich der Praktikant als große Gefahr entpuppen.     
Die sechste Staffel hat die Weichen für die beiden letzten Staffeln gestellt. Am Ende wird Dexters Geheimnis bestimmt das Licht der Welt erblicken. Aber das erfahren wir wahrscheinlich erst nächstes oder übernächstes Jahr.