---
layout: post
title: "Im Studium kürzer treten"
description: "Wieso ich in diesem Semester weniger Kurse belege."
date: 2011-04-13
tags: [Studium]
published: true
category: blog
comments: false
share: true
---

Die erste Woche des Semesters! Vor weniger als sieben Tagen war ich noch auf einer kleinen Insel in Norwegen und habe erfolglos eine Woche lang geangelt. Aber entspannend war es und viel gelesen habe ich dank dem iPad auch. Dazu folgt auf jeden Fall auch noch ein Artikel.

Das letzte Semester hat mich ziemlich eingeengt. Ich verbrachte mit meinen Übungsgruppen mehr Zeit als mit meiner Freundin (inkl. Schlafen, vermutlich), schaffte es meistens noch zum Sport und so gut wie nie zu anderen Dingen. Keine Tätigkeiten im IT-Klub, kein Vorankommen mit Android, kein nicht-IT-Buch gelesen, keine Freunde getroffen und nur ab und zu auf der Gitarre geschrammelt.      
Es ging irgendwie und die Noten sind auch einigermaßen okay, aber dieser Mangel an alternativen Tätigkeiten möchte ich auf Dauer nicht hinnehmen. Es kann sein, dass andere Studenten weniger Zeit für die Uni brauchen, ich aber nicht und deshalb muss ich eine gute Lösung für mich finden.

Ich werde in diesem Semester weniger Kurse belegen und meine neu gewonnene Freizeit eben mit oben besagten Dingen füllen. Ein Musterstudiumplan ist eben nur ein allgemeines Muster. Das besagt, man sollte fünf Kurse machen. Aber wenn mich vier Kurse schon unglaublich beschäftigen, wieso sollte ich mir noch mehr zumuten? Damit ich nachher genau drei Jahre für meinen Bachelor gebraucht habe? Weil ich dann ein halbes Jahr jünger bin?

Ich will gar nicht wissen, welche Auswirkungen diese reine Fixierung auf das Studium haben kann. Von Burnout und Magengeschwüren hat man hier schon gehört, ein reales Problem. Und vor allem steht die Frage des *Warum?* im Raum. Ich sehe ein, dass einige Studenten aufgrund von Geldmangel kein verlängertes Studium machen können, aber das ist ja auch genau das Problem in diesem Land.

Ich bin gespannt wie dieses Semester wird.