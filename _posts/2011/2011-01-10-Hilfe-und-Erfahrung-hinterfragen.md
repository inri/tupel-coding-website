---
layout: post
title: "Hilfe und Erfahrung hinterfragen"
description: "Greift auf die Erfahrung von älteren zurück und lasst euch Materialien geben. Aber hinterfragt auch stets gute Ratschläge und Tips."
date: 2011-01-10
tags: [Studium]
published: true
category: blog
comments: false
share: true
---

Es ist immer wertvoll auf bereits gemachte Erfahrung zurückzugreifen, besonders wenn man ein Studium beginnt. Ältere Semestern haben Materialien gesammelt, Übungen archiviert, Klausuren heimlich mitgehen lassen und sind insgesamt nicht minder wertvoll als die Wikipedia. 

    
Wissen ist wichtig und viel Material verspricht auch viel Wissen. Auch ich habe viele Materialien von vergangenen Jahrgängen bekommen und sie freudig genutzt. Und ich war sehr dankbar dafür.

## Mitdenken

Doch wie das mit Geschenken so ist, gibt es ein **aber**. Mir fallen da in erster Linie zwei Aspekte ein. Den einen habe ich erst heute wieder hautnah erlebt und deshalb schreibe ich mal einige Worte zur Thematik.

Die Anekdote: In dem Kurs Softwarearchitektur gibt es ein semesterbegleitendes Projekt, das Programmieren eines kleinen Spiels in der Programmiersprache Squeak (Ableger von Smalltalk). Der Kurs mit seiner Vorlesung beschäftigt sich mit dem Design von Programmiercode. Es werden Design Patterns erklärt und vorgestellt. Das Spiel soll natürlich diese Pattern beinhalten und *schön* programmiert sein.

Es gibt dabei auch Prinzipien für die Struktur eines Programms (Merken!). Eine andere Gruppe hatte einen Studenten im Team, der diesen Kurs wiederholte [^1], und dadurch natürlich einige Informationen mehr als wir besass. 
Die Gruppe verriet uns, dass der Lehrstuhl gerne eine MVC-Struktur im Programm sehen möchte. Uh, gut zu wissen! Gehört und getan. Wir implementierten diese Struktur, fanden sie später aber nicht mehr so gut und passend, deshalb schmissen wir sie wieder raus und programmierten ohne weiter.

Nun stellten wir zuerst dem Lehrstuhl unsere Folien vor um später in der Vorlesung unser Programm vorzustellen. Ein Punkt in den Folien war die Diskussion von Designentscheidungen und genau da wählten wir u.a. eben dieses MVC-Problem. Wir sagten den Betreuern, dass es Gerüchte gäbe, der Lehrstuhl wolle diese Struktur in den Projekten sehen. Da wurden große Augen gemacht und einer fragte sich laut *&quot;Wer setzt nur diese Gerüchte immer in die Welt?&quot;*.

Es sah so aus, um die Anekdote mal zum Abschluss zu bringen, dass mit Squeak diese Struktur nur schwer zu verwirklichen ist und der Lehrstuhl eigentlich den Gruppen immer davon abrät sie zu implementieren. In den Vorlesungen wurde das wohl auch immer mal angedeutet. Warum der Wiederholer den Kurs wiederholt, wussten wir nun auch.

## Fazit

Aber die Message ist hoffentlich angekommen. Die älteren Studenten wissen zwar einiges und haben viel Material, aber das bedeutet nicht, dass ihr uns blind vertrauen könnt. Denkt lieber selbst nach und entscheidet richtig.

Und der zweite Aspekt: Macht es auch selbst und nicht euer Studium kaputt! Was man nicht selbst macht, kapiert man meist auch nicht. Zur Klausur müsst ihr es sowieso nachholen und habt dann viel mehr Arbeit. Auch Gruppenarbeit ist zwar sehr effektiv, aber man muss danach alleine weitermachen. Die Aufgaben noch einmal selbst lösen oder zumindest überdenken.

Ihr bekommt das schon hin!

### Nachtrag, August 2013

Mittlerweile gibt es dank Dropbox einen gigantischen Ordner mit diversen Materialien zu allen Vorlesungen. Das ist zwar einerseits gut, denn so ist alles schön an einem Ort, aber ich vermute, dass noch weniger über den Stoff reflektiert wird.   
Kleines Beispiel: Für eine Vorlesungsklausur gab es die Aufgaben aus den letzten beiden Jahren in der besagten Dropbox. Sie waren nur Gedächtnisabschriften, keine Kopien der Klausur an sich. Mich wunderte vor allem der geringe Umfang, denn es waren jeweils nur 4 oder 5 Aufgaben. Wie sich herausstellte kamen in meiner Klausur fast alle Aufgaben und weitere neue dran, so dass es insgesamt ein Dutzend Aufgaben waren. Schade für diejenigen, die nur für die alten Aufgaben gelernt haben. Oder auch nicht.

[^1]: Man kann einen Kurs wiederholen, wenn man sich verbessern möchte. Oder muss wiederholen, wenn man ihn nicht bestanden hat.      