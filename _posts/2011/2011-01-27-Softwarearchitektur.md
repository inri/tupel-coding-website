---
layout: post
title: "Softwarearchitektur"
description: "Eine kurze Zusammenfassung zum Kurs Softwarearchitektur."
date: 2011-01-27
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Die Architektur eines Programms ist beim Entwickeln von Software natürlich wichtig und selbstverständlich gibt es auch am HPI eine Vorlesung zu diesem Thema. 


Neben der Theorie sollen wir uns im Rahmen eines Projekts auch praktisch mit der Thematik auseinandersetzen. Die Aufgabe des Projektes ist es, ein Spiel zu entwickeln (es kann ein Klon sein). Die Programmiersprache der Wahl ist Smalltalk bzw. der Dialekt Squeak.

So zur Info: Squeak ist eine reine objektorientierte Sprache. Wenn ihr z.b. denkt &quot;*Naja, Java doch auch*&quot; muss ich verneinen. Java ist keine reine OO-Sprache und genauer gesagt entstand Java zum Teil aus Smalltalk. Squeak ist irgendwie anders, aber für das Erstellen eines Spiels eignet es sich erstaunlich gut.

## Die Präsentation

Unsere Aufgabe war es also ein Spiel zu programmieren und mir kam die Idee: Pokemon! Wir sind ja der Jahrgang, der die Anfänge miterlebt hat und irgendwie waren wir damals alle süchtig. Die anderen waren einverstanden und eine bessere Idee gab es auch nicht.     
Wir programmierten also wochenlang und mit *wir* meine ich vor allem Marcel und Dominic. Heute hatten wir unsere Präsentation und sie war okay, aber es wurden einige komische Dinge kritisiert und als die zweite Gruppe ihr Spiel vorstellte und sehr gelobt wurde, wunderte ich mich.

Im Folgenden möchte ich einige Tipps für eine gute Projektvorstellung in Softwarearchitektur loswerden, auch wenn wir kein Musterbeispiel waren. Wichtig sind die Design Pattern! Glaubt es mir, die sind das A und O bei der ganzen Sache. Im Fokus stand bei unserer Vorstellung leider nicht so sehr die tollen Pattern, die wir (ehrlicherweise eher nicht) verwendet haben, sondern einige interessante Programmierkniffe.

Das flüssige Bewegen in der Pokemon-Klon-Welt zum Beispiel oder wie die verschiedenen Teile zusammenarbeiten. Gewundert haben wir uns dann über die Kommentare vom Professor. Ja, unser Klassendiagramm war in der Tat murks und ein Diagramm wurde vergessen einzufügen. Alles völlig richtig, aber das kann man in drei Minuten kritisieren. Der Professor nahm sich dafür aber mehr Zeit und ebenfalls sehr ausgiebig wurde eine Designentscheidung von uns in Frage gestellt.

## Unser Spiel

Wie wird unsere Welt aufgebaut? Die erste Idee war ein rasterartiger Aufbau, bei dem die Figur in der Mitte steht und je nachdem in welche Richtung sie sich bewegt, werden am Rand die Kästchenreihen und -spalten nachgeladen. Das war aber von der Performance her richtig mies und unsauber programmiert. Da fragte der Professor schon, ob wir die Performance gemessen haben. Nein, die Figur hat sich in Zeitlupe bewegt, da braucht man nichts messen. Als wir dann erklärten, dass wir deshalb auf die zweite Idee ausgewichen sind, stellte er auch das in Frage.

Nun wird jeweils ein Quadrant geladen und geht die Figur aus dem Bild raus, wird der nächste geladen. Das war spürbar schneller und vor allem auch programmiertechnisch sauberer. Der Professor deutete dies dann als eine Art Flucht. Wir hätten ja nicht die Performanceprobleme bei der ersten Version (mit Design Pattern?) gelöst, sondern wären ausgewichen. Darüber har er tatsächlich mindestens 5 Minuten diskutiert.    
Erst sein Doktorand hat dann mal gesagt, dass er die Umsetzung sehr gelungen findet. Ja, das ist sie auch, verdammt noch mal! Die anderen Spiele sahen bescheidener aus. 

Interessant war die Gruppe nach uns. Sie hatte ein *Bejeweled* gebastelt und das war programmiertechnisch simpler. Dafür haben sie einige Pattern gezeigt und erklärt und das fand der Professor ganz toll. Ich verstand nicht, wieso sie jedes Pattern noch einmal erklärt haben, das wurde schließlich in der Vorlesung schon ausführlich getan.   

Deshalb meine Tipps:   

 * Wählt ein simples Spiel aus!    
 * Programmiert das zügig runter und bastelt dann an den Pattern!    
 * Stellt bei der Präsentation mindestens drei Pattern ausführlich vor! 

### Nachtrag

Trotz der nicht so tollen Kritik nach der Vorstellung war die Teilnote dafür relativ gut. Die Noten für die Projektvorstellungen fallen jedes Jahr überdurchschnittlich gut aus und werden quasi durch die Klausurnoten korrigiert.    
Ich bin mir nicht ganz sicher, aber es kann sein, dass der Lehrstuhl, der Softwarearchitektur anbietet, rotiert. Dementsprechend ändern sich eventuell auch die Projektaufgaben.   
