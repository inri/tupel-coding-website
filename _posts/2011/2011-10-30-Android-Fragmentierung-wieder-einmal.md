---
layout: post
title: "Android Fragmentierung, wieder einmal"
description: "Die leidige Geschichte um die miserable Situation rund um die Android Fragmentierung."
date: 2011-10-30
tags: [Android]
published: true
category: it
comments: false
share: true
---

Ein amerikanischer Blogger hat sich durch die Welt der aktuellen Smartphones gewühlt und interessante Informationen gesammelt. Konkret ging es ihm um die Fragmentierung der Androiden. Ich habe dieses Problem ja auch schon angesprochen und es ist eines der Hauptgründe für meinen Wechsel zum iPhone.     


Im Gegensatz zum iPhone und Apple werden die Betriebssysteme der Androiden nicht von Google direkt aktualisiert, sondern durchlaufen beim jeweiligen Hersteller und eventuell auch Mobilfunkanbieter einen Anpassungsprozess. Wenn dieser Prozess schnell und effizient wäre, gäbe es daran gar nichts auszusetzen. Das ist er aber gerade nicht und deshalb werden fast alle Androiden immer erst Monate später aktualisiert, oder nach einer gewissen und meist kurzen Zeit: gar nicht mehr.

Einzige Ausnahme dieser Regel sind die Nexus Phones, denn die werden direkt von Google mit den neusten Updates ausgestattet. Von diesen Phones wird aber jedes Jahr nur eines vorgestellt und dieses Jahr war es z.b. ein viel zu großes 4,6 Zoll Smartphone. Sehr schade.     
Abgesehen von den Nexus Phones, zeigt sich bei den anderen Androiden ein schlimmes Bild. Viele Smartphones sind nicht mal bei Erscheinen auf den aktuellen Stand der Software. Im zweiten Jahr ihres Daseins hängen fast alle Smartphones mindestens eine Android-Version hinterher. Meistens sogar mehr.

[Der komplette Blog-Artikel](http://theunderstatement.com/post/11982112928/android-orphans-visualizing-a-sad-history-of-support)

Leider sind das in erster Linie die amerikanischen Namen für die verschiedenen Smartphones, die bei uns anders heißen oder gar nicht bzw. technisch verändert auf den Markt kommen. Aber grundsätzlich ist das Update-Verhalten außerhalb Amerikas nicht besser. Würde ich eine Liste mit den auf dem deutschen Markt erhältlichen Mobiltelefonen machen, gäbe es noch weniger grün (= aktuellste Version).     
Der Blogger hat Androiden von Mitte 2009 bis Mitte 2010 ausgewählt. Zum einen war es zeitaufwendig die genauen Infos zu den Update-Zeitpunkt jedes einzelnen Gerätes zu finden und zum anderen sind diese knapp 1,5 Jahre ein ausreichend großer Zeitraum um Aussagen zu treffen.

Bei Android sieht es nicht gut aus. Die Skala geht bis 3 Jahre nach Erscheinen des jeweiligen Smartphones und dieser Wert ist definiert von Apple. Ein iPhone hat drei Jahre lang die aktuellste Version des iOS. Im letzten Jahr sind unter Umständen einige Features eingeschränkt (auch der Technik geschuldet), aber grundsätzlich ist man Up-to-Date. Die Androiden in der Liste sind fast nur 1,5 bis 2 Jahre auf dem Markt, aber schon da zeigt sich der gravierende Unterschied.
Das älteste Smartphone in der Liste, das HTC G1, wurde schon nach einem Jahr seines Erscheinen nicht mehr mit Updates verpflegt.

Die Liste zählt auch andere Werte auf und zwar den Preis des Smartphones bei Abschluss eines 2-Jahre-Vertrags. Die genauen Modalitäten des jeweiligen Vertrags wurden zwar nicht benannt, aber ich denke, sie werden äquivalent sein.      
Erschreckend ist dabei die Tatsache, dass ab dem iPhone 3G die Androiden zu gleichen Preisen verkauft wurden (das erste iPhone war richtig teuer...) wie iPhones. Fast alle Telefone wurden für ungefähr 200$ verkauft. Ein enormer Preisunterschied kann also nicht der Grund für die schlechte Update-Politik sein.

### Gut für den Hersteller, schlecht für den Kunden

Der Blogger liefert den Grund gleich mit und ich habe es in einem früheren Blog-Eintrag auch schon benannt. Die nicht-Apple Hersteller wollen so viele Mobiltelefone wie möglich auf den Markt bringen. Nach dem Verkauf eines Smartphones, verdienen sie nicht mehr daran und sind deshalb auch nicht an einem Update des Systems interessiert. Praktisch wäre ein Update kein grosses Problem, denn die Modder-Szene schafft es auch. Aber man will es einfach nicht.    
Apple verfolgt dagegen eher die Politik, dass sie ihre Kunden glücklich machen wollen, damit sein nächstes Gerät wieder eines von Apple ist. Und es funktioniert sehr gut. Es ist mittlerweile doch so, dass der Wert eines Smartphones nicht nur von dessen inneren Werten bestimmt wird, sondern auch von seinem System.

Das ist eine Sache, bei der man sich als Kunde nicht gut behandelt fühlt. Man gibt hunderte Euro aus und bekommt vielleicht mindestens noch ein Update und das war es dann. Nach einem Jahr hat man im Prinzip ein veraltetes Smartphone. Das ist auch aus technischer Hinsicht Betrug, denn es dauert immer einige Zeit, bis Programmierer Apps programmieren können, die die Ressourcen eines Gerätes richtig ausnutzen. Man hat ein technisch sehr leistungsstarkes Gerät, aber kann die neuen Apps nicht nutzen, weil das System nicht aktualisiert wird.

Für die Programmierer wird es auch unnötig kompliziert eine App für sämtliche Geräte anzubieten. Sie laufen mit verschiedenen Systemversionen, haben verschiedene technische Ausstattung und verschiedenste Bildschirmgrößen.     
Es ist zum Beispiel schön, dass Google Android 4 vorgestellt hat, aber erstmals habe ich mir nicht die neuen Features angeschaut. Wozu auch? Mein Desire bekommt dieses Update sowieso nicht und was ich momentan programmiere, ist zu simpel für diese Features und zum anderen gibt es keine Zielgruppe. Auf den meisten Smartphone auf dem Markt, könnte eine App für Android 4 sowieso nicht laufen.     
Ebenfalls nicht zu verachten ist der sicherheitstechnische Aspekt. Lücken im System tauchen ständig auf. Aber wie soll man die Lücken eines Android-System stopfen, wenn es keine Updates bekommt?

Zusammenfassend würde ich sagen, dass ich aktuell niemanden ein Android Phone empfehlen kann. Google will das Fragmentierungsproblem seit Frühjahr in den Griff bekommen, aber anscheinend sind die Smartphones, die im Sommer davor auf dem Markt kamen, nicht mehr inbegriffen.    
In einem halben Jahr kann man mal wieder nachschauen, ob die Tablets und Smartphones von diesem Jahr auf dem aktuellsten Stand sind. Ich hoffe es, glaube jedoch nicht daran.