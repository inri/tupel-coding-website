---
layout: post
title: "Über MP3-Player"
description: "Ein kleine Abhandlung über die Geschichte der MP3-Player."
date: 2011-02-18
tags: [Technik]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-ipods-lg.jpg
  credit: Apple
  creditlink: http://www.apple.com

---

Vor gar nicht mal so langer Zeit lösten MP3-Player den Walkman und dessen Nachfolger, den Diskman, ab. Das war damals noch richtig teure Technik und so langsam massentauglich wurden die Player auch erst Anfang der 00er Jahre. 


## Die Anfänge

Wie auch bei den Speicherkarten der ersten Digitalkameras zu dieser Zeit, erreichten auch die Player nur Größen von bis zu 1GB und das waren dann die Luxusgeräte. Mein erster MP3-Player hatte bescheidene 256MB. Okay, momentan steht Apple mit seinem iPod Classic bei 160GB. Die Entwicklung ist schnell.   
Man könnte sicherlich die weiteren Jahre ausführlich skizzieren, aber an sich änderten sich nur die Speichergrößen. 1GB &gt; 2GB &gt; 4GB &gt; 16GB &gt; 32GB &gt; 64GB &gt; 120GB [hier bricht die 2er-Potenz ab!] &gt; 160GB &gt; 220GB?   

Okay, die Größen der Player an sich änderten sich auch. Neben der Unterbringung des Speicherplatzes, wollte man auch die Bildschirme vergrößern und dem Player mehr Funktionen geben. Bilder und Videos anschauen - was für ein Quatsch! Ich kenne niemanden, der das mal ernsthaft benutzt hätte. Torpediert wurden diese vermeintlich tollen Funktionen auch von den aufkommenden Smartphones, die mehr können als telefonieren. Das war dann ab dem Jahre 2006/2007 würde ich schätzen.

## Unnütze Funktionen

Die super Funktionen konnten sich aber auch bei den Handys nicht durchsetzen. Die Bildschirme wurden zwar auch größer, aber ob man nun auf 4cm oder 7cm-Durchmesser einen Film schaut, völlig egal! Sieht alles doof aus. Die Bildschirme waren auch qualitativ nicht toll und die Minikameras in den Handys totaler Müll.   

Also zurück zu den MP3-Playern: Ich konzentriere mich mal auf Apple, denn schließlich waren sie stets der Vorreiter in diesem Bereich. Sie führten 2002 den iPod Classic ein, der im Gegensatz zu anderen Playern mit 5GB richtig viel Speicher hatte. Dazu sah er schick aus und ein Kult war geboren. Es dauerte drei Jahre bis Apple den iPod nano veröffentlichte. Logisch, denn mittlerweile hatten die andere Hersteller auch größere Speicherkapazitäten, aber kleinere Player.    
Diesen Platz nahm nun der nano ein: Kleiner, mit weniger Speicher und auch schick wie der große Bruder. Wiederum ein Jahr später kam erst der iPod shuffle, noch kleiner und ohne Bildschirm. Erst 2007 gab es das erste Handy von Apple und daraus speckten sie einen neuen iPod ab, den iPod touch. Die Bildschirme waren nun die Steuerelemente und Speicherplatz spielte eine untergeordnetere Rolle.

Was war aber mit den Funktionen geschehen? Die Kameras sind mittlerweile besser geworden und die Bildschirme auch. Zum Video schauen sind sie trotzdem ungeeignet, dafür kam ja dann das iPad und ein Jahr später der Rest der Tabletflut. Musik spielt heute auch nur noch eine untergeordnete Rolle. Im Fokus treten nun Apps, kleine Programme. Allen voran kurzweilige Spiele - was auf dem Gameboy geklappt hat, kann auf iPod touch und iPhones auch klappen.   
Doch zurück zu MP3-Playern. Apple hat vier verschiedene im Angebot. Ich gebe mal meine Einschätzung, was man von den Geräten so erwarten kann.

## Apples MP3-Player (Stand 2011)

#### iPod Classic 
In gewissen Zeitabständen aktualisiert Apple seine Produkte, aber der classic hat seit September 2009 keine Updates mehr bekommen. Schlechtes Zeichen? Momentan stagnieren die kleinen Festplatten bei 160GB, erst im Januar wurde eine erste mit 220GB vorgestellt. Bis diese im iPod Platz findet, wird es noch einige Wochen oder Monate dauern. Dann kommt eventuell ein Update, ansonsten war es das mit dem Classic. Aber ehrlich, wer braucht den? Ich hatte einen und der Gedanke war, die komplette Musiksammlung bei sich zu haben. Doch die brauche ich nie und dafür ist der Player mit seiner Größe wie ein zweites Handy. 

#### iPod Touch
Ein nicht-Handy für Leute, die kein Geld für ein iPhone haben? Hier liegt der Reiz auf den Apps und das ist ein nicht zu unterschätzender Faktor. Witzigerweise sind sich die aktuellen iPod touchs und iPhones technisch ähnlich (Stand 2011), wobei ein iPhone fast das dreifache kostet. Ich denke, die Leute (=Jugendliche) sollen mit dem touch angefixt werden und danach ein iPhone kaufen. Damit sind die Möglichkeiten ein Stück größer und es macht Spaß. Musik hören kann man auch!

#### iPod Nano
Er wurde nach seinem letzten Update kleiner gemacht und hat nun sogar einen Touchscreen. Aber wozu? Das weiß keiner! In einem Unikurs von uns (Human Computer Interaction) sollten die Studenten die Oberfläche eines Programmes für ein mobiles Gerät entwerfen, einem Nano! Total dankbare Aufgabe, fragt mal die Studenten.   
Abgesehen davon hat der Nano noch keinen Anschluss an den AppStore und somit kann man bisher keine Apps laden und benutzen. Lediglich die Albumbilder und sonstige Informationen, die keiner braucht, werden angezeigt. Ich will Musik hören und nicht lesen!   

Es tut sich noch ein weiteres Problem auf. Wie navigiert man mit dem Nano? Man tippt auf den Bildschirm. Aha. Und ich muss den Bildschirm sehen, damit ich weiß, wo ich hintippe? Dafür muss ich den Nano entweder irgendwo ranklippen, so dass ich ihn sehe, oder in die Hand nehmen. Dann ist es aber nur ein Mini-Handy, welches nichts kann. Ich denke, das Fehlen mechanischer Tasten ist richtig übel. Noch dazu ein Touchscreen, der bisher nutzlos ist.
Entweder kommt da demnächst ein Produktupdate oder keine Ahnung.

#### iPod Shuffle
Der einzige richtige MP3-Player! Ohne Bildschirm, ohne Firlefanz, so muss es sein. Mit jedem Musik-abspiel-Gerät, dass ich bisher hatte, erlebte ich das gleiche: Nach einigen Wochen bediente man den Player blind in der Hosentasche, fertig. Der shuffle ist superleicht und ein toller Begleiter, wenn man Sport macht oder einfach nur so durch die Gegend läuft. Ich habe einen von der dritten Generation, also ohne Tasten. Ich finde es okay, er ist eben noch kleiner als die neuen mit Tasten. Aber ich gebe zu, ja, es kann zu Problemen kommen. 

Neue, originale Ohrstöpsel schlagen mit 30€ zu da darin die Steuerung eingebaut ist. Der Player selber hat mich über Ebay nur 35€ gekostet. Deshalb ist die neue Tastenversion sicherlich praktischer als die alte Version mit Ohrstöpseltasten, die teuer sind. Aber mich regt die Kapazität auf. Der kleine hatte noch bis zu 4GB, habe ich auch, und das ist fast schon zu viel, wenn man sich nur mit den Ohrstöpseltasten durchklicken kann. Der neue ist in der Hinsicht besser, geizt aber mit 2GB und kostet genauso viel wie der alte. Verstehe ich nicht.

## Fazit

Was nun kaufen? Der iPod Touch ist mehr ein Handy. Der iPod Nano ist ein nichtsnutziger Zwitter und vor allem mit 160€ überteuert. Der iPod Shuffle ist ein richtiger MP3-Player, aber mit 2GB ein wenig mickrig geraten (wobei 2GB bei mir auch mal eben 24 Stunden die ärzte sind - also eigentlich ausreichend, außer man möchte 24 Stunden lang Abwechslung).

Meine Meinung müsste klar sein. Updates erhalten die Player vermutlich auch erst im Herbst. Mal sehen was dann aus diesem Nano-Desaster wird.

## Bonus: Mein erster MP3 Player

Könnt ihr euch noch an euren ersten MP3-Player erinnern? Ich besass sogar ein oder zwei Diskman davor. Muss man sich mal vorstellen, die Dinger waren deutlich grösser als gängige Smartphones heutzutage. Mein erster MP3-Player war etwas kleiner als eine Zigarettenschachtel. Es war kein Markengerät, aber einer der ersten kompakten Geräte. Er glänzte mit sagenhaften 256 MB, die ausreichten. Falls ich ihn finden sollte, werden Fotos nachgereicht.