---
layout: post
title: "Hank Moody vs. Tommy Gavin"
description: "Ein Vergleich zwischen zwei sich sehr ähnelnden Seriencharakteren."
date: 2011-08-07
tags: [Serie]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-hank-moody-lg.jpg
  credit: Showtime
---

Unter einer *Californication*-Meldung las ich einen Kommentar. Der Schreiber mag Hank Moody und einen gewissen Tommy Gavin als Seriencharaktere am liebsten. Ich schlussfolgerte daraus, dass die beiden ähnliche Persönlichkeiten sein könnten und schaute mir mal die Serie *Rescue Me*.      


Sie zeigt das Leben von einigen Feuerwehrmännern in New York, einige Jahre nach 9/11. Die Hauptrolle spielt der besagte Tommy Gavin. Er lebt in Trennung mit seiner Frau, hat ein Alkoholproblem und ist ein Sturkopf und Raufbold.

## Hank Moody in Californication

Durchaus zeigen sich viele Parallelen zwischen *Californication* und *Rescue Me*. Der Streit um die Kinder, zahlreiche Romanzen, viele Abenteuer mit Frauen und auch die mitunter sehr verrückten Begebenheiten sind in beiden Serien wichtige Elemente. Die grundsätzliche Konzeption ist aber unterschiedlich, denn *Californication* ist eine Übersteigerung vom Leben in Hollywood gemischt mit viel Ironie und Sarkasmus. Dabei ist Hanks Beziehung zu seiner Frau und Tochter im Vordergrund und man weiß ab der ersten Episode, dass die Familie für ihn das wichtigste ist. In *Rescue Me* steht nicht unbedingt das Feuerwehrleben im Vordergrund, aber Tommy hat definitiv nicht die gleiche Bindung zu seinen Kindern wie Hank.

## Tommy Gavin in Rescue Me

Mir gefällt Tommys Charakter nicht besonders. Ich denke es ist durchaus möglich, dass Hank von ihm inspiriert wurde, aber Hank wurde sympathischer geformt. Tommy ist enorm starrköpfig und jähzornig. Sicherlich sind diese Charaktereigenschaften auch von seinen traumatischen Erfahrungen geprägt. Aber er überschreitet sehr oft Grenzen mit seinen Reaktionen. 

<figure>
    <a href="/images/2011-tommy-gavin-1600.jpg">
	   <img 
		  src="/images/2011-tommy-gavin-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Der Feuerwehrmann als Antiheld</figcaption>
</figure>

Wenn er z.b. mit einem Baseballschläger eine Wohnung klein haut oder einen Drogendealer fast verbrennen lässt, weil dieser Mitschuld an einem Feuer trug. Dazu kommen seine Bekanntschaften mit der Polizei, die er ständig provoziert. Allerdings hat er sehr oft ein Auffangnetz, wie seinen Bruder, der bei der Polizei ist und ihn aus der Patsche hilft. Oder seine Feuerwehrfamilie, die immer für ihn da ist und ihm viel zu viel durchgehen lässt.

## Der Vergleich 

Auch Hank tut oft unüberlegte Dinge, ist dabei aber nicht so unberechenbar wie Tommy. Auch emotional spielt er in einer anderen Liga als der Feuerwehrmann. Ich bin mir nicht sicher, ob dessen abgeklärte Cooleness gewollt ist, oder die Figur es einfach nicht rüberbringen kann. Wenn Tommy mit seiner Frau über Gefühle spricht, steckt dahinter meistens ein Manöver, ein bestimmter Plan, den er verfolgt. Dementsprechend wirkt sein Seelenstriptease nicht so richtig.      
Hanks Umgang mit seiner Frau und den anderen Frauen ist sensibler. Ihm kauft man ab, dass er für jede etwas empfindet und sich Mühe gibt. Vielleicht ist dieser Anteil an Emotionen der grösste Unterschied zwischen den beiden und wirkt sich auch dementsprechend auf ihre Taten aus?

Es gibt noch einen weiteren Unterschied, welchen die beiden Hauptdarsteller der Serie betrifft, aber auch für die gesamte Konzeption gilt. Und zwar sind das die sozialen Schichten, in denen die Serien handeln. Bei den Feuerwehrmännern sind logischerweise keine Einsteins beschäftigt. Einige Figuren sind generell ein wenig schwer von Begriff und die anderen sind eher unterer Durchschnitt. Bei allen geht es immer darum genug Geld für den laufenden Monat zusammenzukratzen. *Rescue Me* spielt eher in der Unterschicht bzw. unteren Mittelschicht.      
Hank und sein soziales Umfeld kommen aus einer höheren Schicht. Geld spielt da zum Beispiel keine grosse Rolle. Die Dialoge sind dementsprechend auch intelligenter gestaltet. Der Smalltalk zwischen den Feuerwehrmännern ist zwar meist sehr amüsant, hat aber ein anderes Niveau. Dementsprechend handeln die Figuren auch anders als bei *Californication*.  

Mein Favorit bleibt natürlich Hank Moody. Das mag daran liegen, dass ich Hank seit 4 Staffeln kenne, Tommy dagegen erst seit einer. Trotzdem spielen beide sehr ähnliche Rollen und vermutlich würden sie sich ähnlich verhalten, wenn sie in dem Umfeld des jeweils anderen auftreten würden.