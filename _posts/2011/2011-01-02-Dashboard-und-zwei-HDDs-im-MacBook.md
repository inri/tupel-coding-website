---
layout: post
title: "Dashboard und zwei HDDs im MacBook"
description: "Es kann zu einem kleinen Performance Problem kommen, wenn man eine zweite Festplatte in einem MacBook einbaut und ein bestimmtes Dashboard Tool benutzt."
date: 2011-01-02
tags: [MacBook, Hardware]
published: true
category: it
comments: false
share: true
---

Vor einigen Wochen habe ich mir ein OptiBay für mein MacBook Pro gekauft. Das ist im Prinzip ein Schacht für eine zweite Festplatte im MacBook, wobei man aber das DVD-Laufwerk ausbauen muss. 


Eine zweite Festplatte ist aus Platzgründen im MacBook nicht möglich und auch nicht nötig, denn es gibt schon 750er Platten mit der benötigten Höhe von 9,5 mm.
   
Wesentlich interessanter ist die 2-Festplatten-Option aber mit SSDs geworden. Eine ausreichend große SSD ist zu teuer, aber eine kleine SSD gepaart mit einer normalen HDD klingt doch optimal. Das war auch mein Plan. Ich besorgte mir eine kleine OCZ und packte mein Mac OS X darauf und nutzte eine 500er Platte als Datenspeicher.

Der Ausbau funktionierte gut und alles war danach schneller. Aber ein Problem tat sich auf. Vielleicht nicht unbedingt ein schlimmes Problem, aber ein nicht akzeptables Geschwindigkeitsproblem. Mein Dashboard hatte seit dem Wechsel teilweise lange Ladezeiten. Die Widget aktualisierten sich erst nach fast 10 Sekunden und es stockte, was es vorher nicht tat! Und das mit einer SSD!      
Meine Vermutung war, dass er irgendwelche Daten von der zweite HDD lesen will und diese aus dem Ruhezustand geholt wird, was natürlich einige Sekunden dauert. In diversen Foren fand ich einige Einstellungstips und eliminierte damit den Ruhezustand. Besser wurde es nicht.

Es war eine langsame 5400er Platte mit wenig Cache, vielleicht war sie einfach nur zu langsam? Obwohl ich sie vor dem Umbau eingesetzt hatte und es nie zu Problemen kam. Es hätte ja trotzdem ein Grund sein können. Also kaufte ich keine zweite SSD, aber eine schnelle Seagate mit einem 4 GB Flash-Speicher (eine Hybridplatte). Diese nun eingebaut, verursachte die gleichen Probleme bzw. gab es keinen Unterschied. Aus rein logischer Betrachtung heraus, konnte es also nicht an der verbauten Platte liegen.     
Was kann ansonsten das Problem sein?

Das Dashboard bzw. ein Widget könnte das Problem verursachen! Ich hatte ein Systemüberwachungtool, welches beim Öffnen des Dashboards auch Infos von den Platten holt. Nach Entfernen dieses Widgets lief mein Dashboard wieder ohne Probleme. Ich tauschte die Platten auch noch einmal aus und auch mit der vermeintlich 5400er gab es keine Ladepausen.

Doof ist die ganze Sache trotzdem. Mit einer Platte gibt es diese Probleme nicht, vermutlich weil die Platte dauerhaft aktiv ist. Andere User sind auf ähnliche Probleme gestoßen z.b. beim Speichern von Dateien werden auch die Platten abgefragt und das dauert.

Ich bin trotzdem zufrieden.