---
layout: post
title: "Rückblick auf einen Dauerlauf - 4 Semester HPI"
description: "Dominic lässt die letzten zwei Jahre am HPI Revue passieren."
date: 2011-09-01
tags: [HPI, Studium, Gastbeitrag]
published: true
category: it
comments: false
share: true
---
Nachdem hier nun bereits einiges über das 4. Semester am HPI veröffentlicht wurde, wird es auch für mich, Dominic, Zeit, einmal zurückzublicken. 


**Dies ist ein Gastbeitrag von Dominic Petrick.**
   
Ich hatte mir bereits vor geraumer Zeit vorgenommen, ein Fazit über das Grundstudium zu ziehen, aber ich habe es immer wieder verschoben. *Das hat später Zeit...*, *Erstmal ausruhen, jetzt bin ich so kaputt...* und ähnliches hatte ich mir gesagt. Nun sitze ich hier und überlege mir, wo ich anfange. Es ging alles enorm rasant, alles erinnert an einen Sprint. Mit voller Kraft gestartet und ohne Rücksicht auf Verluste bis zum bitteren Ende durchgezogen. Nicht denken, machen.

Seit dem 2. Semester dachte ich bisher: *Schlimmer kann es nicht mehr werden, vom Aufwand!*. Jedes Semester musste ich feststellen, wie sehr ich mich getäuscht hatte. Am Ende sitzt man dann, wie ich, da und reflektiert ein wenig und man erkennt, dass einzelne Gedanken an das Semester nur mit Mühe zu entwirren sind.      
Da sind die Erinnerungen an morgendliche Vorlesungen, in welchen man nur mit Müh und Not die Augen aufhalten konnte, da man am Abend zuvor wieder bis halb zwei gearbeitet hatte, um irgendeine Übung vernünftig zu bearbeiten. Dazwischen Gedanken an Selbstanfeuerungsversuche, den eigenen Ehrgeiz, an welchen ich selber immer wieder appellierte und an welchen ich mich klammerte.   
    
Erinnerungen an Tiefphasen, in denen man sich fragte, was man hier eigentlich macht, wozu das Ganze und kontraproduktive Gedanken hegte, der Sorte *Du-schaffst-das-doch-eh-nicht!-Das-kannst-du-nicht!-Oh-Gott-du-bist-so-schlecht!*. Einige wenige positive Erinnerungen dazwischen gestreut, wie beispielsweise eine Zwischenklausur, die sich trotz des massiven Zeitaufwandes gelohnt hat. Ja, es war ein Dauerlauf. Einer, der sich vielleicht aus der  beruflichen Perspektive gelohnt hat, aber wie nach jedem Dauerlauf ist man danach fertig und braucht Ruhe.

Diejenigen, die mich kennen, wissen um mein doch recht pessimistisches und ehrgeiziges Gemüt und meinen Hang, Dinge negativer zu betrachten als sie sind. Deswegen klingt das jetzt alles furchtbar. Allerdings bin ich daran nicht unschuldig. Vielen erging es auch nicht besonders gut im Semester und sie hatten auch wenig Zeit und demnach viel zu tun, aber ich habe im 4. Semester gleich 6 Kurse gewählt. Ehrgeiz kann auch ungesund sein ;)

Allein rechnerisch, der klinische Tod an Freizeit und Lebensfreude. Wenn wenigstens vom Aufwand her vertretbare Fächer dabei gewesen wären, wäre das alles nicht ganz so schlimm gekommen, jedoch musste ich mir mit militärischer Präzision die Fächer aussuchen, die alle hochkomplex und mit viel Arbeit verbunden waren. So kam es wie es kommen musste: Die Tage fingen monatelang früh morgens an und endeten spät in der Nacht.     
Aber es bringt eigentlich, außer einer persönlichen Erkenntnis: dies nie wieder zu tun, nichts, jetzt darüber zu jammern, zumal es von vornherein jedem normalen Menschen ersichtlich gewesen wäre. Es ist ja nun auch nicht so, dass ich nichts gelernt habe. Natürlich war nicht alles schlecht! 

Es gab auch dieses Semester wieder eine breite und hochinteressante Angebotspalette seitens des Instituts, was der Grund war, mich zu vielen Fächern hinreißen zu lassen. Insgesamt belegte ich 3D-Computergrafik, Internet- und WWW-Technologien, Betriebssysteme II, Softwaretechnik I, Theoretische Informatik II und ein Seminar über *Grundlagen der Modellgetriebenen Softwareentwicklung*. In jedem dieser Fächer habe ich eine ganze Menge gelernt und viel getan.
In den folgenden Wochen werde ich hier über die einzelnen Fächer Beiträge schreiben, welche das Fach inhaltlich beschreiben, was für Erfahrungen ich gemacht habe, was und wie die Übungen waren, was das Fazit ist und vieles mehr.
Also wenn euch das interessiert, schaut in den nächsten Wochen noch einmal vorbei :)

Was ist nun mein Fazit? Viel Arbeit, viel Frust, aber auch die tiefe Befriedigung viel gelernt zu haben. Also eigentlich wie immer, aber doch Nuancen anders als die vergangenen Semester.
Mittlerweile sehe ich z.B. das kommende Bachelorprojekt, über welches schon geschrieben wurde, als Herausforderung und nicht mehr als drohenden Untergang. Das liegt vor allem an den vielen (praktischen) Aufgaben, die ich im Laufe dieses Semester bewältigen musste (natürlich auch die Aufgaben des letzten Semesters).      

Mal die Programmierung eines Treiber da, dann ein HTTP-Server hier, auch ein Projekt an einem größeren System und grafische Programmierung mit OpenGL. Ich finde es schon beinahe verwunderlich, was für ein breites Verständnis ich durch die Bank weg durch eine bewusst breit gefächerte Kurswahl erlangt habe, vor allem bedingt durch die bereits angedeutete große und spürbare Praxisnähe dieses Semester am HPI, welche ich in den letzten Semestern stellenweise doch sehr vermisst habe und eigentlich ja als Aushängeschild hoch gehalten wird.     
Insgesamt blicke ich also auf ein stressiges *Grundstudium* – also 4 Semester – zurück, welches viel Leid und Glück gebracht hat und eine gute Grundlage für kommende Herausforderungen gelegt hat – Ich bin gespannt und blicke ausnahmsweise positiv in die Zukunft.