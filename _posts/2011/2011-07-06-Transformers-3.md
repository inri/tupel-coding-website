---
layout: post
title: "Transformers 3"
description: "Warum Transformers 3 ein fantastischer Actionfilm ist."
date: 2011-07-06
tags: [Film]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-transformers-3-lg.jpg
  credit: Paramount Pictures
---

Gestern Abend war ich dank o2 in Berlin zur Premiere vom dritten Teil der Transformers-Reihe. Ich versprach mir nicht weniger als viel guter Action und genau das bekam ich auch. Deshalb das Fazit vorneweg: **Wer Action-Filme mag, kommt um Transformers 3 nicht herum.**

Das erste Drittel ist grenzwertig als Komödie konzipiert, aber trotzdem sehenswert. Da waren einige richtig gute Szenen dabei. Dem erzählerischen Verlauf ist bis knapp nach der Hälfte auch nichts auszusetzen. Am Ende wird es wieder sehr rund und vorhersehbar, aber es kann ja nicht alles Inception sein.

Die Bilder... ja. Geil! Was der Herr Bay an Action abfeuert ist enorm sehenswert. Auch wie haargenau die Transformers zusammengesetzt sind, ist wirklich bemerkenswert.      
Das 3D wurde dezent eingesetzt und drängt sich nicht zu sehr in den Vordergrund. Also es sah alles unglaublich gut aus und man merkte auch die Plastizität, aber es war kein Durchbruch wie Avatar. Wobei man auch sagen muss, dass der große Kampf an Ende von Avatar auch nicht die Muster-3D-Szene war. In schnellen Actionszenen ist man sowieso damit beschäftigt der Handlung zu folgen und kann sich vielleicht gar nicht auf die 3D-Vielfalt konzentrieren.      
Vielleicht merke ich den Unterschied zu 2D erst, wenn ich ihn gesehen habe. Bei Avatar war mir aber gleich nach dem ersten Anschauen bewusst, dass nur durch 3D der Film perfekt ist. Bei Transformers 3 sieht der Film in 2D bestimmt auch noch unglaublich gut aus.

Unglaublich gut sah auch Rosie Huntington-Whiteley aus, siehe Titelfoto. Ich oute mich jetzt, Megan Fox vermisse ich nicht. Beides sind heiße Schnitten, aber Rosie wirkt auf mich nicht so kühl wie Megan. Auch ihre Filmrolle ist milder, mehr das Mädchen. Und diesen Blick auf dem Bild hat sie fast den ganzen Film lang. Der Kinosaal hat sich bei jeder Szene mit Rosie hörbar gefreut.

Es gibt einige erzählerische Schwächen, das mag sein. Es wirft schon Fragen auf, wenn Optimus Prime, als *Superheld* der guten Seite, während des großen Kampfes zwanzig Minuten in einigen Stahlseilen festhängt.      

Was fand ich neben Rosie noch bemerkenswert? Die Szene in Tschernobyl war sehr schick, davon hätte ich gerne mehr gesehen. Dass die Menschen richtig was Reißen können, hat mich auch gefreut. Ich fragte mich immer, ob die Soldaten nur Transformers-Futter sind. Sind sie nicht.

Ich wiederhole mich gerne noch einmal: **Wer Action-Filme mag und nicht zu viel Wert auf eine schlüssige Story legt, kommt um Transformers 3 nicht herum.**