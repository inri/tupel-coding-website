---
layout: post
title: "Android Flashen mit Frust"
description: "Android ist ein offenes System, aber normale Benutzer können diese Möglichkeiten gar nicht nutzen."
date: 2011-09-03
tags: [Android]
published: true
category: it
comments: false
share: true
---

Was habe ich für einen Krampf hinter mir?! Am Anfang stand einfach nur der Wunsch mein Android-ROM zu wechseln. Am Ende dagegen die bittere Erkenntnis: Android mag zwar mehr Möglichkeiten und Freiheiten haben als iOS, aber diese können normale Nutzer nicht erreichen.

Ich hatte mir vor einigen Wochen ein CyanogenMod auf mein Desire geladen. Das ROM ist zwar nicht schlecht, aber einige Dinge haben mir nicht sehr gefallen. Ich fand die Performance zum Beispiel nicht gut. Außerdem hatte ich ein Problem beim Programmieren. Der FileBrowser im Debugging Modus gab mir leider keinen Zugriff mehr auf die internen Daten des Desires.
Das lag an der Partitionierung der SD-Karte bzw. des App-Bereiches.      

Android stellt zwar mittlerweile auch das App2SD zur Verfügung und das schont einiges an Speicher, aber die ROM-Hacker haben noch einige andere Features eingebaut. Zuerst partitioniert man einen kleinen Teil der SD-Karte in ein Linux-Format, EXT3 zum Beispiel. Die ROMs legen dann automatisch alle Apps auf diesen Bereich ab und man hat auf diese Weise sehr viel Speicher zu Verfügung.      
Oder anders ausgedrückt: Man kann die ROM nicht verwenden, wenn man seine SD-Karte nicht korrekt partitioniert. Durch diese Verschiebung der Apps und ihrer Daten kann der FileBrowser in Eclipse nicht mehr auf sie zugreifen. Ich wollte zu einem anderen ROM wechseln und erhoffte mir Besserung.

## Partitionsgröße ist wichtig

Vor kurzem wurde das neue LeeDroid 3.3.3. veröffentlicht und das wollte ich nun ausprobieren. Zugegeben, ich überlas den Hinweis, dass eine 1GB große EXT3 Partition benötigt wird. Mit der App RomManager lassen sich nur 512 MB Partitionen anlegen. Erwartungsgemäß war der Speicher nach Neustart auch fast voll und ich konnte nur noch eine Hand voll Apps installieren.         
Okay, also einfach eine 1 GB Partition anlegen. Aber wie? Und da bricht sich der normale Windows-Nutzer schon den Hals. Nach ewigem Suchen fand ich ein Programm, Partition Wizard 6. Die Bedienung war enorm gut und es war auch kein Problem EXT-Partitionen zu erstellen, wow. Aber der Knackpunkt: Es funktionierte nicht.

Da ich die Daten auf meiner alten 512 MB EXT-Partition nicht gesichert hatte, musste ich das LeeDroid ROM noch einmal installieren (flashen) und danach hätte es funktionieren müssen. Aber dem war nicht so. Ich hing in einer Bootschleife fest. Aus terminlichen Gründen hatte ich Eile und brauchte ein funktionierendes Handy. Ich suchte wieder mal eine Weile nach einem normalen ROM, ohne App2SD via EXT-Partition. Ich fand ein altes Froyo und war erst einmal zufrieden.

## Partitionieren mit GParted

Mein Rat: Partitioniert eure SD-Karte nur mit GParted. Das ist eine ISO, die ihr euch auf CD oder DVD brennt und dann euren Rechner von dieser startet. Dort die EXT-Partition und die normale FAT-Partition einrichten (FAT zuerst und beide Primär). Nachdem ich dies tat wiederholte ich den Installationsvorgang wieder und es funktionierte.      

Das neue LeeDroid startete und hatte massig Platz für Apps. Nachdem ich nun einiges installiert habe, waren schon 600 MB belegt, da können 512 MB natürlich nicht ausreichen. Das Problem im FileBrowser ist nicht gelöst. Ich hacke mich über Konsole ins Handy und schau mir dort die Datenbanken an. Ist zwar nicht ganz so komfortabel, aber vielleicht finde ich dafür auch noch eine Lösung.

Das liest sich hier alles so einfach. Aber da stecken etliche Stunden Recherchieren drin. Mit Unrevoked wurde das Rooten zwar leicht gemacht und der Rom Manager hilft auch bei der Rom Auswahl und Installation. Aber sobald man einen Fehler macht, ist man erschossen und muss stundenlang nach Informationen suchen. Für mich ist das okay, das ist mein Hobby und mein Beruf.

Erklärt aber einem normalen Smartphone-Nutzer mal, was eine Partition ist und was ein ROM. Und wofür braucht man das? Damit man mal ein neues System benutzen kann, bessere Leistung hat und keine Probleme mit der Installation vieler Apps. Deshalb sind die Freiheiten von Android zwar schön, aber für normale Benutzer nicht erreichbar und dadurch kein Vorteil für die Plattform. Traurig.

**Nachtrag:** Einige Wochen später, ist mein Desire heftig abgeschmiert. Es liess sich zwar starten, wurde aber mit Fehlern bombardiert. Keine Chance. Das hat mich so frustriert, dass ich wieder ein normales HTC Rom installiert habe bzw. das letzte aktuelle von HTC.    
Wiederum einige Wochen später kaufte ich mir ein iPhone und bin sehr zufrieden. Sicherlich kann ich mit dem iPhone nicht alles machen, wie mit dem Desire oder einem Android, aber dafür funktionieren andere Dinge wesentlich besser und machen auch mehr Spaß in der Benutzung. Ich bereue den Wechsel nicht.