---
layout: post
title: "iPhone statt Nexus Galaxy"
description: "Wieso mich das riesige Nexus Phone so enttäuscht und ich das iPhone toll finde."
date: 2011-10-19
tags: [Android, iPhone]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-galaxy-nexus-lg.jpg
  credit: Google
  creditlink: http://www.google.com
---

Google und Samsung haben in der letzten Nacht die neue Android Version vorgestellt und gleich dazu das neue Referenzsmartphone Galaxy Nexus. Nachdem ich mich jüngst über die mangelnden Update-Möglichkeiten meines Desires geärgert habe, wartete ich sehr gespannt auf das neue Nexus. Denn das tolle an den Nexus-Phones ist die volle Google Unterstützung. 

Sie erhalten neue Android Versionen als erstes und es ist kein Schnickschnak der Hardwarehersteller installiert. Nun liege ich früh im Bett, wische mit meinem Finger über das iPad und lese die [Android Neuigkeiten auf androidnews.de](http://www.giga.de/androidnews/google-galaxy-nexus-offiziell-vorgestellt?utm_source=twitterfeed&utm_medium=twitter&utm_campaign=Feed%3A+androidnewsde+%28androidnews.de+-+Wir+sind+Android%29). Und bin enttäuscht. Eine kurze erste Meinung.

## Große Hände sollst du haben

Das Galaxy Nexus ist technisch gut ausgestattet, wie auch die anderen Android Flaggschiffe. 1,2 GHz Prozessor, 1GB RAM und ein 4,65 Zoll Bildschirm. Lasst euch das auf der Zunge zergehen: vierkommasechsfünf Zoll groß. Und dann kann man solche Sätze auf androidnews lesen *Beim Design war Google die Ergonomie sehr wichtig.* - aber wieso haben die Ingenieure dann ihr Monsterphones nicht mal in die Hand genommen!

**Ich bin wirklich sauer, das wird wahrscheinlich ein Rant.** 

Einige Monate hatte ich das Vergnügen mit einem HTC HD2 und das hatte nur einen 4,3 Zoll Bildschirm. Das Nexus ist zwar 2 mm dünner, dafür aber 1 cm länger als das HD2. Ich kann euch sagen, ein 4-Zoll-Display zu bedienen, ist möglich, aber nicht schön. Es ist letztlich sehr groß und klobig, da kann man machen was man will, da kann Samsung das Handy noch so krumm machen, es wird verdammt noch mal nicht ergonomisch.

Ich bin über 1,80 m groß, habe nicht die zierlichsten Hände, aber auch keine Fleischerklauen und trotzdem würde ich wetten, für den Großteil der Frauen ist dieses Handy zu groß. Zumindest die Frauen, die mein Handy in den Händen hielten, fanden es sofort zu groß.

Bisher fiel der Name noch nicht, aber es wird Zeit: iPhone. Das iPhone verkauft sich dumm und dämlich. Selbst als im Sommer schon die Gerüchteküche brodelte, wurden noch massig iPhones verkauft und das neue iPhone 4S bricht in dieser Hinsicht alle Rekorde. Für einige überraschend, gab es keine Designveränderungen, kein größeres Display und kein Mini iPhone.     
Was denken sich diese Apple Menschen also? Abgesehen vom iPod nano hat Apple seit Jahren maximal 3,x Zoll Touchscreens verbaut und das hat vielleicht so seine Gründe, zurecht. Aber was haben sich die Samsung Ingenieure gedacht? Ich denke ja, sie wollen gar keine Telefone verkaufen. Ihnen ist vermutlich wichtiger, einfach den größten zu haben. Oder vielleicht sollte ihnen jemand verraten, dass sich Apple die Displaygröße nicht patentiert hat. Glaube ich.

## Altbewährtes

Das neue iPhone sieht rein äußerlich wie das alte iPhone aus. Es gab deswegen zwar kritische Stimmen, aber verkaufen tut es sich trotzdem wie geschnittenes Brot. Warum sollte sich das Design auch ändern? Das iPhone 4 war ein gutes Handy, abgesehen vom Antennenproblem. Alle Zubehörausstatter haben ihre Produkte auf das iPhone 4 angepasst. Wäre doch unpraktisch nun wieder alles umzustellen.      
Daneben gibt es bestimmt auch noch weitere Gründe, ökologische zum Beispiel. Ansonsten hat Apple die Innenausstattung verbessert. Das bekannte Antennenproblem wurde gelöst, ein schnellerer Prozessor kommt zum Einsatz und die Kamera hat sich auch weiterentwickelt. Viel mehr kann man nicht dazu sagen. Es ist gut.    
In der Software hat Apple mit iOS 5 zahlreiche kleinere Verbesserungen eingeführt. Ein Feature sticht aber hervor und das ist die Spracherkennung Siri. Es ist verdammt noch mal Star Trek.

Apple hat also ein gutes Telefon weiterentwickelt und ist damit erfolgreich. 

Die abschliessende Frage: **iPhone 4 oder 4S?**
