---
layout: post
title: "iPad 2 im Media Markt Potsdam"
description: "Ich habe im Media Markt mein iPad gekauft."
date: 2011-03-26
tags: [iPad]
published: true
category: blog
comments: false
share: true
---

Neben den überfüllten Gravis-Shops in Berlin und den sicherlich nur knapp ausgestatteten Apple Reseller in Potsdam, kam ich auf die Idee, dass es ja auch noch einen Media Markt in Potsdam gibt. Deshalb Donnerstag noch telefonisch nach den neuen iPads gefragt und ja, sie haben alle Varianten erhalten.

Wie groß wird der Andrang am Freitagnachmittag werden? Hat man in Berlin eine Chance? Ich war zu faul, extra nach Berlin zu fahren und versuchte mein Glück in Potsdam Sterncenter bei Media Markt. Die Freundin wollte sowieso einige Besorgungen machen. Also hinfahren, schauen ob jemand wartet und wenn ja, wie viele, und wenn sich das Warten lohnt, bleiben.     
Vorm MM und auch in der Apple Abteilung war kurz nach 15 Uhr keine iPad-Seele. Zur selben Zeit waren die Schlangen vor den Gravis-Shops schon lang. Ich fragte einen MM-Mitarbeiter höflich, wie das hier abläuft und er antwortete nur unfreundlich zurück, dass die iPad 17 Uhr hier verkauft werden. Toller Verkäufer und ein Glück, dass ich nicht erst 17 Uhr zurückkam!

Ich schlenderte mit meiner Freundin durch das Sterncenter und kurz nach 16 Uhr schaute ich noch einmal vorbei, sicher ist sicher. Da begannen die Mitarbeiter den Apple-Tisch vorzubereiten und es standen eine handvoll Leute herum. Und alle hielten einen Zettel in der Hand!     
Ich besorgte mir auch einen und hatte die wunderbare Nr. 13. MM bekam wohl insgesamt 22  iPads und nach den Nummern aufsteigend konnten wir dann unsere iPads aussuchen.

Es war kurz vor 17 Uhr in der Abteilung relativ voll und es waren definitiv mehr Menschen da, als iPads. Ich würde sagen, so ca. 40 Menschlein standen um den Tisch mit den iPads herum. Die gingen weg wie warme Semmeln und ich ergatterte ein schwarzes 64 GB ohne 3G. Wieso weshalb warum, schreibe ich später.

Insgesamt habe ich eine Stunde gewartet. Ich finde es okay. In Berlin wäre es mit einer Stunde nicht getan gewesen. Die Häme im Internet ist nicht gerade klein, &quot;*Wie können die Deppen für ein Produkt so lange anstehen?*&quot;, &quot;*Diese Apple-Idioten...*&quot; blabla. Ja, es gibt tatsächlich noch Menschen, die stehen für eine Sache auch mal eine Stunde an. Ich fahre in einigen Tagen in den Urlaub und wollte es da gerne mitnehmen.       
Ob man nun 10 Stunden den Spaß mitmachen muss, ist natürlich fraglich, aber wem wird denn dabei weh getan? Genau.

Ich finde es übrigens merkwürdig, mit welcher Energie auf die Apple-Freaks gewettert wird. Die Sache mit den langen Schlangen bei neuen Apple-Produkten kennt man ja. Ich habe dies früher registriert, mich gewundert und war fertig mir darüber Gedanken zu machen. Wenn mich die Produkte nicht interessierten, war es mir gleich viel mehr egaler.

Ich bin gespannt, wie lang die Schlangen beim Galaxy Tab, Xoom, TouchPad,... werden.      
Das war jetzt Häme von mir.