---
layout: post
title: "iPad 2 und Überraschungen"
description: "Das iPad 2 kommt in den nächsten Wochen und ich teile meine Gedanken darüber mit."
date: 2011-01-16
tags: [iPad]
published: true
category: it
comments: false
share: true
---

Es kommt. Es kooooommt! Das zweite iPad!!!!! Wohooooo! Oléolé!

Apple muss eine richtig gut fahrende PR-Abteilung haben. Soweit ist das ja nichts neues, aber es ist immer wieder erstaunlich wie gut es klappt, jedes Mal. Die Informationen aus Apple-nahen Quellen sprechen die gleiche Sprache: Am 1. Februar wird das neue iPad vorgestellt. Einen Monat vorher sickern dann im Wochentakt neue Informationen durch.   
Besonders witzig fand ich den Kommentar auf einer Seite, als man im Quellcode der neuen Beta des iOS Hinweise bzgl. der Kamera im iPad und verbauten Grafikchip fand - man wunderte sich darüber wie Apple solche *Programmierfehler* machen kann. Auch, wenn ich jetzt die Welt einiger Apple-Fans zerstöre, es muss gesagt werden: It is just PR!

Ich möchte kurz einige Worte bzgl. meiner Wünsche an das neue iPad loswerden und mal sehen, wie überraschend die Eckdaten letztlich werden. Ich bin erstmal sehr enttäuscht, dass Apple tatsächlich am 12-Monats-Rhytmus festhält und das iPad erst ab April in Amerika verkaufen wird. Vielleicht haben wir es dann im Mai/Juni in Deutschland? Au Mann, fast ein halbes Jahr noch.    

Zu dieser Zeit wird auch Google seine Tablet-Munition anfangen zu verfeuern. Eigentlich interessant, dass Apple sich da noch so lange Zeit lässt. Technisch werden die neuen Android Geräte dem iPad leicht überlegen sein. Ich würde ja mein Tablet so bald als möglich in die Welt hinauslassen, auch an Apples Stelle. Kann natürlich auch sein, dass Apple gesehen hat, dass es bisher **immer noch kein** Konkurrenzprodukt gibt und sie allen anderen ein Jahr voraus sind. Auch ein Jahr an dicker Erfahrung bzgl. eines laufenden System auf Tablet-Hardware.

## Erwartungen &amp; Vermutungen

Einige Vermutungen zu den bisherigen technischen Eckdaten. Eine Kamera ist okay, aber keine Revolution. Schon frech, dass sie nicht im ersten drin war. Mal sehen ob Apple das als *&quot;Everything changes, again&quot;* abfeiern wird. Flacher soll es werden und damit auch handlicher und leichter. Gut so, aber nichts weiter als eine normale Erwartung. Die Technik hat sich im letzten Jahr auch weiterentwickelt und ist noch winziger geworden.  

Ein neuer Prozessor kommt auch rein, auch nur Erwartung. Und bitte nicht den iPhone-Prozessor! Ich finde es unglaublich albern, in meinem Tablet ein Handy-Prozessor drin zu haben. Da ist mehr Platz drin, baut also was größeres ein. Das gleiche bei der Grafik, aber da kommt wohl was neues.   
Der Bildschirm war ja schon gut, aber die Auflösung kann ruhig ein Stück hochgesetzt werden. Fast noch wichtiger wäre aber Arbeitsspeicher, davon hatte das iPad weniger als das iPhone 4 und das muss doch echt nicht sein.

Der Preis! Ja, ich denke, eine 32GB-Version wird Einsteiger und hoffentlich nicht für 500€ verkauft, sondern eher 400€. Allerdings ist es auch bedenklich dass das schnöde iPhone auch mit 16GB beginnt, aber mal eben 630€ kostet - wobei da momentan noch die bessere und kleinere Hardware drin ist.

Da hört meine Kreativität auch schon auf. Mal sehen was Apple noch so aus dem Hut zaubert.
**Again!**