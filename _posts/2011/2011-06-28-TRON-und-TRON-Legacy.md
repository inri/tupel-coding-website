---
layout: post
title: "TRON &amp; TRON Legacy"
description: "Die mangelnde Logik im Sequel des Originals finde ich nicht besonders toll."
date: 2011-06-28
tags: [Film]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-tron-legacy-lg.jpg
  credit: Disney
  creditlink: http://disney.de
---

In den 80er stellte der Film TRON eine kleine Revolution dar und war aus stilistischer Sicht etwas neues und durchaus sehenswertes. Heute bekommt man Augenkrebs davon. Aus Neugier schaute ich mir das moderne Sequel an und wiederum danach und auch wieder aus Neugier das Original. Nun folgt eine kleine Kritik, aus Sicht eines skeptischen Informatikers.

Nach dem Anschauen von TRON Legacy, fragte ich mich, wie wohl die beiden Filme in Verbindung stehen. Es war ernüchternd zu sehen, dass sie es eigentlich nicht tun. Irgendwie gibt es zwar diese Computerwelt, die große Firma Encom und die Hauptfigur Flynn, aber das ist auch schon das einzige. Ach ja, und die Spielhalle!      
Der zweite Film erzählt aber eine ganz andere Geschichte. Im ersten jagte Flynn noch den Beweisen hinterher, dass er einige erfolgreiche Spiele programmiert hatte. Er stolperte dabei in den Computer und musste auf dem Weg zurück das oberböse Programm MCP bekämpfen.

Im zweiten Film stolpert wiederum der Sohn in den Computer, findet seinen verschollenen Vater und will mit ihm hinaus. Verschollen war der Vater, Flynn natürlich, weil er durch ein selbst geschaffenes Programm im Computer festgehalten wurde.
Da gruselt es den Informatiker und nicht nur diesen. Warum, warum, warum? Ich habe hunderte Fragen! Das wenigste wird irgendwie ansatzweise logisch erklärt. In der Spielhalle ist seit 20 Jahren durchgängig Strom um den einen Computer mit der tollen Welt am Laufen zu halten? Und dass dieser sogar ein Touchscreen hat?! Was in dieser Welt abgeht, ist ja sowieso grenzwertig sinnig.

Schon im original TRON konnte niemand so richtig verstehen, was in dieser Welt abgeht. Selbstständig handelnde Programme? Von was reden wir hier? Eine Art organische Algorithmen oder wie? Beide Filme sind inhaltlich sehr verschwurbelt und schwach. Legacy sieht jedoch optisch lecker aus, eine konsequente Entwicklung der Augenkrebsgrafik.

Ja, schaut sie euch beide an. Dann habt ihr einen Klassiker gesehen und einen schicken Film. Erwartet aber bitte nicht zu viel.