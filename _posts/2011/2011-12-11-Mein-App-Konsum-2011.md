---
layout: post
title: "Mein App Konsum 2011"
description: "Einige Zahlen zu meinen App Käufen bzw. alle Käufen digitaler Güter."
date: 2011-12-11 
tags: [App]
published: true
category: it
comments: false
share: true
---

Vor kurzem habe ich mich gefragt wie viel Geld ich überhaupt schon in Apps gesteckt habe. Das ließ sich leicht herausfinden, hatte ich die Emails von iTunes doch fein säuberlich sortiert.

## Game Apps

Ich hatte für eine kurze Zeit ein iPad im Oktober letzten Jahres. Für das erste Antesten dieser Geräteklasse habe ich auch gleich einige Spiele gekauft und dabei mehr ausgegeben (für Spiele) als jemals danach. Ich kaufte für insgesamt 18€ die Klassiker Angry Birds, Fieldrunner und Plants vs. Zombie. Später, als ich das iPad 2 hatte, kamen noch weitere Angry Birds und einige andere Spiele dazu.       

Bisher habe ich damit 18€ + 10,80€ = 30,80€ für Spiele ausgegeben. **Das sind 2,80€ pro Monat für Spiele** (11 Monate in Besitz von iPad, iPad 2 und iPhone, inkl. Dezember) und auch durchschnittlich **2,80€ pro App**. 

Es sind zwar zwei, drei Spiele dabei, die ich nur sehr selten nutze, aber im Großen und Ganzen habe ich mit den anderen viel Spaß.     
Das letzte Spiel habe ich im Mai gekauft (für das iPad) bzw. im Oktober (Tiny Wings für das iPhone). Viele Spiele gibt es auch kostenlos und da kommt man eigentlich auch ganz gut aus. Oder man wartet auf Sonderaktionen zu Weihnachten, Ostern oder ähnlichen Feiertagen, denn dann sind die Spiele, wie auch andere Apps, entweder reduziert oder eben kostenlos.

## allgemeine Apps

Apps im Allgemeinen kaufe ich ab und zu. Je nachdem, was ich *entdecke* oder was ich brauche. Dafür habe ich insgesamt bisher wenige Euro mehr ausgegeben als für die Spiele (**3,40€ pro Monat**), aber die Apps sind im Durchschnitt auch nicht so teuer (**2€ pro App**).    

Das schöne an den Apps ist, dass sie oft auf dem iPad und iPhone  benutzt werden können. Bei Spielen ist das seltener, so müsste ich z.b. die Angry Birds Spiele für das iPhone noch einmal kaufen.    
Auch bei meinen Apps benutze ich nicht alle gekauften zwangsläufig oft und regelmäßig. Aber die meisten sind ihr Geld auf jeden Fall wert gewesen.

## Comics

Mein größter Posten sind Comics. Zugegeben, das sind keine Apps, aber digitale Wertgüter, die ich auf dem iPad kaufe und konsumiere. Ich mag besonders die Walking Dead Reihe und eine Ausgabe kostet 2,40€. Allein für TWD kamen in den letzten 9 Monaten also 21,60€ zusammen und jeder Cent war es wert! Ich habe in den ersten Wochen meines iPad-Besitzes diverse Comics probiert und auch mal eine Ausgabe gekauft. So komme ich auf durchschnittliche **5€ pro Monat für Comics**, aber immerhin *nur* 1,80€ pro Comic.     
Andere kaufen sicherlich auch Musik, Filme und anderes auf ihrem iOS-Gerät. In diesen Bereichen überzeugt mich das Preis-Leistung-Verhältnis noch nicht. Bei Comics habe ich aber gar keine andere Wahl, denn z.b. die Walking Dead Reihe kann man in Deutschland einfach nicht zeitnah kaufen.

## Mac App Store

Seltener habe ich bisher den App Store meines MacBooks besucht. Für diesen gibt es einfach noch zu viele Alternativen, aber prinzipiell zeigt er uns, in welche Richtung es gehen wird. Mein erster Kauf war die neue Version von Max OS X, Lion. Ansonsten habe ich ein hilfreiches Tool und das Programm gekauft, auf welchem ich diese Zeilen gerade tippe (iAWriter). Zum Vergleichen lohnen sich diese Käufe noch nicht, aber in die Gesamtstatistik sollten sie eingehen.

## Fazit

Insgesamt habe ich bisher um die **150€ für Apps** bezahlt. Das sind **13€ pro Monat**, 13€ für einige richtig tolle Apps. Im Durchschnitt **2,60€ pro App**, natürlich sind die zahlreichen kostenlosen Apps nicht mitgerechnet.       
Im gleichen Zeitraum habe ich 0€ für Windows Apps ausgegeben und nur einige Euro für Android Apps (wobei ich das Desire über ein Jahr genutzt habe). Die Gründe für diese Unterschiede liegen zum einen an der Qualität der Apps (Android) und zum anderen an den Vertriebsstrukturen (Windows).      
Von Microsoft gibt es noch keinen einheitlichen App Store, aber er wird in Windows 8 kommen (welches wiederum im Herbst 2012 erscheint) und wurde vor wenigen Tagen auch vorgestellt.
Bis dahin wird sich der App Store mehr und mehr etablieren und am Ende vielleicht sogar das Suchen nach bestimmten Programmen für Mac-Benutzer leichter machen als es für Windows-Benutzer momentan ist.