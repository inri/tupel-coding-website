---
layout: post
title: "iPad 2 und die Verkaufsstrategie"
description: "Wieso Apple die ersten iPads nicht über den Online Store verkauft."
date: 2011-04-01
tags: [iPad]
published: true
category: it
comments: false
share: true
---

Ich schreibe euch mal meine Gedanken über die interessante Verkaufsstrategie von Apple bzgl. des neuen iPads. Es wurde vielfach kritisiert und bemängelt, dass man kein iPad vorbestellen konnte, im Netz sogar erst nachts am Verkaufstag eine Bestellung aufgeben. Ich glaube, das hatte gleich mehrere Gründe.

Erst einmal: Ich war auch nicht darüber begeistert, hätte mein iPad lieber online bestellt, das ist eben bequemer. Dummerweise sind Apples Produkte meist so gut, und das neue iPad wurde ja schon in Amerika eine Woche vorher gelobt, dass in der Tat einige Hundert Menschen in einen Laden gehen und einige Stunden in einer Schlange stehen.      
Da ist auch schon der erste Grund: **1. Apple kann es eben machen.** Kein anderer Hersteller könnte sein Produkt so anbieten, nur Apple kann das und deshalb machen sie es auch.

**2. Marketing ist alles.** Dieser wichtige Punkt dürfte jedem klar sein, der am besagten Freitag mal Fernsehen geschaut, Radio gehört oder Zeitung / News gelesen hat. Sicherlich hätten bestimmt 90% der Käufer in den regulären Apple Stores ihr iPad lieber online gekauft, also wären die besagten Liefermengen sicherlich auch im Web wie warme Semmeln weggegangen. Aber was hat Apple davon? Für die restlichen 10% hätten in den Läden noch kleinere Mengen an iPads zur Verfügung gestanden, was wiederum Kritik hervorruft. Außerdem sieht man den Online-Bestellern ihre Freude beim Erhalt nicht an, der grinsenden Menge vorm Apple Store sehr wohl.

Die Leute, die wirklich ein iPad haben wollen, stellen sich in die Schlange. Dadurch bekommen andere Menschen mit, dass da irgendwas los war. &quot;*Warum stehen die da an?*&quot; und so wird die Kunde in die Welt getragen, dass es ein neues Apple-Produkt gibt. Dieser Hype ist einfach nur gutes Marketing.

Als letzten Punkt könnte man vielleicht **3. Ein wenig Geld sparen** aufzählen. Es läppert sich vielleicht doch zusammen, wenn ich statt tausende Haushalte, nur einige Stores beliefern muss. Zumindest besonders in den ersten Tagen des neuen Produkts, wenn jeder Interessent lieber den bequemeren Weg wählen würde. Später verteilt sich der Verkauf ganz gut.

Letztlich bleibt mir nur übrig, Apple für seine Marketing-Strategien zu gratulieren. Andere Hersteller versuchen auch immer mal wieder witzige Werbespots oder Aktionen, siehe Microsoft zum Release von Windows Phone 7, aber diese wirken eher unbeholfen und manchmal peinlich.