---
layout: post
title: "iPad 2 und der erste Eindruck"
description: "Mein erster Eindruck nach einigen Tagen mit dem neuen iPad."
date: 2011-03-30
tags: [iPad]
published: true
category: it
comments: false
share: true
---

Nun komme ich endlich mal dazu, mir das iPad richtig anzuschauen! Ich entschied mich für die non-3G Version. Das muss jeder für sich selbst entscheiden.

Ich bin die meiste Zeit in Potsdam, habe entweder mein eigenes WLAN, Wohnheim-WLAN, Uni Potsdam WLAN oder HPI WLAN. So wirklich oft fahre ich nicht mit dem Zug oder Auto durch die Weltgeschichte, dass ich genau in diesen raren Stunden Internet benötige. Und wenn doch mal, dann wird mein Handy eben ein Hotspot. Ich schreibe gerade noch einen separaten Artikel zu diesem Thema, Provider und Flatrate-Angebote.
Mir reicht die only-WLAN Version und damit ich schön viel Bilder, Bücher, Musik und Filme/Serien mitschleppen kann, ist es die 64 GB Version geworden. Ich wäre auch mit 32 GB zufrieden gewesen, aber die gab es im MM nicht mehr. Also die große.

Mein erster Eindruck? Leicht und handlich. Ja, ich hatte einige Tage lang das alte und erste iPad und das hat sich definitiv anders angefühlt. Einer der Gründe, wieso ich dieses iPad nicht behielt, war der Mangel an guten Apps und irgendwie gefiel mir die Performance noch nicht so richtig. Ich ahnte damals im Oktober, dass &quot;demnächst&quot; ein neues iPad vorgestellt wird und meine Hoffnungen und Erwartungen wurden auch erfüllt. Schade nur, dass es 6 Monate dauerte, aber hier bin ich nun.      

Die Performance und die Geschwindigkeit sind sehr gut. Alles startet schnell, keine Ruckler, es flutscht. Genauso stelle ich mir die Zukunft mit diesen Geräten vor. Sehr begeistert bin ich auch von den Apps. Vor dem iPad-Kauf habe ich mir schon 70 Apps geladen und war bereit für die ersten Tage. Einige Apps flogen dann zwar gleich raus, aber dazu später in einem anderen Posting mehr. Man kann sich stundenlang mit dem iPad beschäftigen. Es ist in der Tat irgendwie magisch.

Das Smart Cover habe ich ja schon vor einigen Tagen kritisiert und verteidigt. Meine Meinung dazu steht noch.     
Was kann Apple nun noch besser machen? Nicht sehr viel. Das Gewicht kann natürlich noch leichter werden, aber es ist schon sehr gut, für diesen Formfaktor. Ich mag den Bildschirm, aber wenn es in einem Jahr bessere Technik gibt, gerne doch. Wegen mir können sie die 16 GB Version rausschmeißen, das ist lächerlich wenig.

Und die Kamera! Was soll denn das? Was soll dieses Stück antike Technik auf der Rückseite meines iPads? Für fehlende SD-Kartenslots und USB habe ich ja Verständnis. Aber die Kamera ist ein schlechter scherz. Ich glaube dass Apple denkt, die iPad-Nutzer haben keine Kamera zu brauchen. Sie bekommen nur diese winzige Etwas, damit sie beim Videochat mal eine andere Sicht zeigen können. Nicht nur das eigene Gesicht, sondern auch mal die Umgebung.    
Weil als Kamera kann man das ja nicht ernst nehmen, 0,7 Megapixel, was soll das? Hätte es Apple so viel mehr Geld gekostet eine 2 MP oder 4 MP Kamera einzubauen? Ich begreife es wirklich nicht. Oder vielleicht wollen sie beim nächsten iPad gefeiert werden, wenn sie eine bessere Kamera verwenden (Steve Jobs: &quot;*Die Kamera des iPad 3 macht nun 15x bessere Fotos!*&quot;). Das war doof und hätte nicht sein müssen.

Aber ansonsten ist es ein tolles Tablet. Ich glaube auch, das bleibt die nächsten Monate das Beste auf dem Markt (als Gesamtpaket aus Hardware, Apps und Preis). Hoffentlich kauft sich bald jemand aus meinem Bekanntenkreis ein Android-Tablet. Neugierig machen mich die Dinger ja schon.