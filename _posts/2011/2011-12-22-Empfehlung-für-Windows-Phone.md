---
layout: post
title: "Empfehlung für Windows Phone?"
description: "Kritik eines Windows Phone 7 Review oder warum ich nicht verstehe, wie jemand Windows Phone empfehlen kann."
date: 2011-12-22
tags: [Microsoft, Design]
published: true
category: it
comments: false
share: true
---

Ich bin auf ein Review von [Marcel Wichmann](http://uarrr.org) über das erste Nokia mit Windows Phone gestoßen. Neugierig hat mich seine Antwort auf die Frage &quot;*Welches Smartphone solle man sich kaufen?*&quot; gemacht. 

&quot;*Nimm ein iPhone. Wenn du das nicht möchtest, achte darauf, dass es Windows Phone 7 nutzt. Ob nun iPhone oder Windows Phone 7-Phone, mit beidem wirst du glücklich.*&quot;

Bitte was? Diese Antwort stellt Windows Phone nicht nur mit iOS auf einer Stufe, sondern positioniert auch Android darunter, eine sehr gewagte These. Ich las das Review und schüttelte mehr als einmal den Kopf. Fairerweise muss man sagen, dass er das Review unter dem Gesichtspunkt *Hat endlich jemand ein Betriebssystem gebaut, welches mich zufrieden stellen könnte?* geschrieben hat. Ich bin der Meinung, dass seine Zufriedenheit nicht zur Zufriedenheit einer großen Masse von Nutzern führt.

## Vergleich muss sein

Microsoft verdient mindestens genauso viel Respekt für das noch sehr junge Windows Phone 7, wie es auch genauso viel Prügel einstecken muss, dass sie überhaupt so lange dafür gebraucht haben.     
Doch der Kunde stellt diese Art der Überlegung nicht an. Ihm ist egal, ob Microsoft den Trend verpennt hat und deshalb in viel kürzerer Zeit die Entwicklung nachholen musste (und noch muss), was zwangsläufig auch zu Fehlern führt. Es mag sein, dass Windows Phone in einem Jahr auf einer Augenhöhe mit dem heutigen iOS sein wird, aber dann ist iOS auch schon weiter und Android ebenfalls. Mich interessiert z.b. auch nicht, ob das Auto, das ich heute kaufe, erst in einem Jahr 200km/h fahren oder besonders benzinsparend sein wird.

Kurz gesagt: Für die kritische Betrachtung sind mir die näheren Umstände um Windows Phone völlig egal. Im mobilen, wie auch vielen anderen, Bereich(en) herrscht ein Wettbewerb und dieser orientiert sich an der aktuellen Leistung. Ganz einfach.

## Design

Bevor ich einige grundsätzliche Überlegungen diskutiere, bespreche ich kurz das Review. Zu Beginn stellt der Autor das Nokia vor und vergleicht es mit dem iPhone. Ich bin der Meinung, man kann in Tränen ausbrechen, wenn sich ein Kratzer auf der iPhone Rückseite bemerkbar macht, man kann es aber auch einfach hinnehmen. Ein Smartphone ist ein Gebrauchsgegenstand. Fertig.     
Ich finde auch, über Look&amp;Feel lässt es sich sehr gut streiten. Ein Gefühls-Vergleich mit den abgerundeten Ecken und die gläserne Rückseite des iPhones, ist eben subjektiv. Wenn die Funktion und Bedienung beeinträchtigt wird, dann ist es ein Design-Fehler. Deshalb sind die genannten Kritikpunkte am Nokia viel schlimmer, weil sie die Benutzung beeinträchtigen:

* alle Buttons sind auf der rechten Seite angeordnet und pieksen dem Rechtshänder in die Hand 
* die Verschlussklappe des USB-Anschlusses fühlt sich *unangenehm empfindlich* an, 
* die Kameralinse ist so positioniert, dass Finger ständig drauf liegen
* einen der fest verbauten Touch-Buttons löst man ständig ungewollt aus

Das ist einfach nicht akzeptabel. Mag sein, dass es sich besser anfühlt, aber die genannten Schwächen können eben durch das subjektive Look&amp;Feel nicht ausgeglichen werden. Das sind heftige Designfehler von Nokia und nicht die Schuld von Microsoft (bis auf die fest verbauten Touch-Buttons, denn die sind verpflichtend, glaube ich).     
Ein Windows Phone-System muss ziemlich gut sein, damit ein Nutzer mit solchen Fehlern leben kann.

(Damit der Vergleich richtig einzuordnen ist: Das iPhone hat keine Buttons, die irgendwo piecksen, keine empfindliche Verschlussklappe, keine Buttons, die man ungewollt auslösen könnte und die Kameralinse hat einen guten Platz in der linken oberen Ecke. Die runden Ecken stören nicht und das Glas ist z.b. auch rutschfest. Ich mag das verbaute Glas und den Metallrahmen auch. Mein Desire hat sich ebenfalls gut angefühlt und wies keine Schwächen auf, bis auf die Plastikrückseite.    
Nokia ist schon zu lange im Smartphone-Geschäft, als das man ihnen diese Fehler durchgehen lassen würde.)

## Konzept der Tiles (Kacheln)

Im Review ging es aber zentral um Windows Phone und so werden ausführlich die Tiles als Bedienungskonzept beschrieben und was man tolles damit machen kann. Es ist löblich, dass sich Microsoft ein neues Konzept ausgedacht hat. In diesem Punkt ähneln sich iOS und Android sehr, und ein Windows Phone hätte mit der normalen App-Oberfläche gar keine Chance gehabt.    
Aber an einigen Ecken und Kanten gibt es noch Probleme mit ihrem Bedienkonzept und das dürfte eigentlich nicht sein.

Die Tiles klingen ansonsten ganz interessant, aber auch nicht so weltbewegend toll, dass ich mein iPhone sofort eintauschen würde. Man bekommt sehr das Gefühl, dass sie besonders für den Autor sehr passend sind und darum ging es ihm ja auch in seinem Artikel. Er schildert z.b. wie er eine bestimmte Notiz auf Evernote öffnet. In iOS sind es mehrere *Klicks* und bei Windows Phone nur ein Klick auf eine Tile.      
Eine Hand voll Apps kann das Prinzip der Tiles also ausnutzen. Aber der Autor sagt selber, dass viele Apps noch nicht darauf getrimmt sind. Damit ergibt sich das größte Problem an Windows Phone: Die Apps.

## Apps

In der zweiten Hälfte beschreibt der Autor einige Apps genauer und dabei offenbaren sich keine Überraschungen. Die Auswahl ist mies und wie oben schon gesagt, sind nur wenige Apps überhaupt auf Windows Phone getrimmt. Die hauseigenen Windows Phone-Apps sind auch nur teilweise zu gebrauchen.     
Das ganze Thema Apps ist ein Desaster auf Windows Phone und ich wüsste zu gern, wie man diesen Mangel mit einem iOS oder Android gleichsetzen kann.

Der Autor sieht interessanterweise großes Potential für Windows Phone-Apps und ist fest überzeugt, dass sehr bald gute Apps kommen werden. Ich weiß leider nicht, woher dieser Optimismus kommt. Wieso sollten sich Entwickler auf Windows Phone stürzen? Ja, es hat interessante Konzepte und damit kann man sicherlich coole Apps zaubern, aber wenn die nachher keiner kauft?

Der Autor deutet es an und ich denke auch, dass im Vergleich zu den Möglichkeiten in iOS und Android die Möglichkeiten in Windows Phone (für Drittanbieterapps) noch eingeschränkt sind. Man muss ja heute schon mit verschiedenen Funktionsumfang für Android-Systeme klarkommen. Wer mag sich da auf ein völlig anderes System einlassen?     
Ich wüsste auch nicht, dass Java und Objective-C so viel schwerer zu lernen und beherrschen sind als C#.

## Kauftip

Windows Phone bringt gute Konzepte mit und hat eigene Ideen, aber alles ist zu spät. Der Mehrwert eines Systems ist nicht mehr länger das System selber, sondern die Apps, die das System bereichern. Wäre das Review zwei Jahre alt, würde ich dem Autor zustimmen. Damals blickte ich voller Hoffnung zu Microsoft und wartete auf das Nachfolgesystem von Windows Mobile 6.5. Vergeblich.     
Wäre der Smartphone-Markt nicht schon von Android und iPhone besetzt, könnte das Microsoft-System eine ernsthafte Konkurrenz darstellen. Aber der Zug ist abgefahren.

Ich würde deshalb auch niemanden ein Windows Phone empfehlen, denn damit wird er auf lange Sicht nicht glücklich. Wird es dieses System in zwei Jahren überhaupt noch geben? Oder zieht Microsoft die Notbremse?

## Von wegen Design

Geärgert habe ich mich über das großzügige Hinwegschauen von Design-Fehlern in diesem Review. Der Autor bezeichnet sich als Designer, beschreibt die Unzulänglichkeiten des Gerätes und des Systems und spricht am Ende ernsthaft eine Empfehlung aus. Sehr seltsam.      
Über das Geräte-Design hatte ich ja schon geschrieben. Auch bei den Apps gibt es so einige Fehler. Ist der Autor nur erstaunt, dass es so wenige Fehler sind oder was genau ist daran empfehlenswert?     

Das Review ist sehr großzügig mit der Bewertung. Fast in jedem Absatz schwingt ein hoffnungsvoller, optimistischer Ton mit. Zufällig sind einige Funktionen von Windows Phone für den Autoren ganz gut geeignet, aber deshalb gleich eine Empfehlung mitgeben, finde ich insgesamt gewagt.