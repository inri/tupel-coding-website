---
layout: post
title: "Steve Jobs"
description: "Meine Gedanken zum Tod von Steve Jobs."
date: 2011-10-06
tags: [Apple]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-steve-jobs-lg.jpg
  credit: Apple
  creditlink: http://www.apple.com
---

Ich möchte einige Gedanken zu Steve Jobs und Apple äußern.
Apple-Produkte haben mich bis vor kurzem nicht interessiert. Ich hatte mir vor Jahren mal einen iPod Classic gekauft, die vorletzte Generation. Der iPod war zwar von Apple, doch das war damals nicht der entscheidende Grund für mich. 


Ich wollte einen MP3-Player, der meine Musiksammlung beherbergen konnte und das Preis-Leistungs-Verhältnis war bei Apple am besten. Später erst merkte ich, wie sehr das für so viele ihrer Produkte zutrifft.

## Mein Weg zu Apple

Während meines Studium kam ich schnell in Kontakt mit Besitzern von MacBooks. Mir fiel auf, dass die Geräte gar nicht so schlecht waren. Nach den ersten beiden Semestern wollte ich meinen eeePC ersetzen und suchte wochenlang nach einem Windows-Notebook. Es sollte eine gute Akkulaufzeit haben, leicht und ein wenig leistungsstark sein. In meinen Preisgrenzen fand sich kein geeignetes Gerät, also suchte ich in den höheren Gefilden und stieß auf das MacBook Pro. In seiner Preisklasse gab es zwar auch Notebooks mit Windows, die mir ähnliches bieten konnten, aber ich war neugierig auf ein für mich neues Betriebssystem.    
 
Das war es auch. Seit nun schon über einem Jahr arbeite ich sehr gerne mit meinem MacBook, auch in diesem Moment zum Beispiel. Ich habe auch begonnen mich mit der Firma Apple zu beschäftigen und verfolge die Entwicklung von iPhone, iPad und iOS sehr gespannt.    
Ziemlich oft stelle ich mir selbst die Frage, ob ich nicht zu einem dieser sogenannten *Fanboys* verkomme. Alles blind und ohne Reflexion gut finden was von einer Firma, wie zum Beispiel Apple, kommt.

## Apple als Maßstab

Steve Jobs hat über Jahre hinweg seinen Traum gelebt und seine Vision verfolgt. Stetig wurden neue Produkte getestet und bestehende regelmäßig verbessert. Gab es anfangs noch Flops und Rückschläge, waren die technischen Zaubereien aus Cupertino in den letzten Jahren enorm erfolgreich und beliebt. Über die Jahre hatte nicht nur Steve Jobs sich verbessert, sondern auch seine Firma Apple.    
Ich betrat eine ausgereifte und in vielen Aspekten beängstigend perfekte Welt. Ich gehöre nicht den ersten oder zweiten Generation der Apple-Fans an, die vor Jahren noch mit viel Optimismus die Produkte gut finden mussten. Heute ist es leicht geworden Apple zu mögen.

Heute kann man von den anderen Erklärungen verlangen, wieso keine andere Firma ein ebenbürtiges Tablet herausbringen kann zum Beispiel. Apple schreibt einer ganzen Industrie vor, in welche Richtung sie sich zu entwickeln hat. Von all den zahlreichen Technikunternehmen, ist es gerade die Firma mit dem kreativen und gleichzeitig sturen Kopf an der Spitze, die sich so sehr von allen abhebt. Die Mitarbeiter von Apple haben auch einen großen Anteil daran, aber die Richtung hat Steve Jobs festgelegt. Das ist allein seine Leistung.     
Dafür kann man nur große Bewunderung für ihn empfinden.

## Zukünftige Innovationen

Welche Auswirkung wird der Tod von Steve Jobs für Apple haben? Und wann wird man diese Auswirkungen spüren? Die Öffentlichkeit weiß natürlich nicht genau, wie groß der Anteil von Jobs an den Produkten der letzten Jahre war. Wenn man sich aber Anekdoten wie die folgende ins Gedächtnis ruft, scheint der Anteil bis ins kleinste Detail gewirkt zu haben: Spät an einem Wochenende rief Steve einen iPhone Verantwortlichen an und besprach mit ihm das Design eines App Icons, welches nach Meinung von Steve noch Potential zur Verbesserung hatte.

Apples Produkte werden 3 bis 5 Jahre im voraus entwickelt. Man kann davon ausgehen, dass Jobs am iPad 3 seine Finger hatte. Interessant wird es also spätestens ab 2017, denn ab diesem Zeitpunkt werden Apple Produkte erscheinen, die erst in den nächsten Monaten und Jahren entwickelt werden. Doch wer weiß das schon genau, vielleicht gibt es in der Apple Zentrale einen Safe mit Ideen und Entwürfen von Steve Jobs?

Doch ich glaube auch ohne ihn wird Apple weiterhin gute Produkte entwickeln und herstellen. Wie ich es oben schon beschrieben habe, ist nicht nur Jobs seiner Vision näher gekommen, sondern Apple als Firma wuchs mit ihm. Auf den Chefposten der einzelnen Bereiche sitzen viele fähige Entwickler und und kreative Köpfe.

Steve Jobs hat der Welt viel hinterlassen. Neben kreativen Entwicklungen auch Inspiration. Ich blicke weiterhin gespannt in die Zukunft, was auch immer kommen mag.

Danke Steve.