---
layout: post
title: "Softwarearchitektur (Noten)"
date: 2011-03-06
tags: [Studium, HPI]
published: true
category: blog
comments: false
share: true
---

Die Noten für das Softwareprojekt und die Klausur wurden veröffentlicht. Erwartungsgemäß sind die Projektnoten besser ausgefallen und die Klausurnoten zogen den Schnitt herunter.

Das Projekt wurde im Schnitt mit 1,3 bewertet, meine Gruppe hat es nur zu einer 1,7 geschafft. Einige Mitglieder waren selbst darüber positiv erstaunt, aber im Kontext der anderen Spiele und sehr guten Noten ist das kein Grund in übermäßiger Freude auszubrechen. Es bestätigt auch mein Verdacht, dass in der Tat sehr genau auf den Quellcode geachtet wurde und weniger auf das Spiel und die Spielbarkeit. Zumindest ist das der Fall, wenn meine vielleicht arrogante Einschätzung stimmt und es kaum besser spielbarere Spiele als unseres gab.

Die Klausurnoten zogen den Schnitt auf 1,7 herunter - das war nun keine große Überraschung. Ich mochte die Klausur auch nicht besonders. Ein zahlenmäßiger Fehler trat aber zutage, meines Erachtens. Die schlechteste Projektnote war eine 2,0 und man musste mindestens eine 4,0 in der Klausur schaffen um zu bestehen. Das war natürlich für die 5,0er doof, die sind durchgefallen. 

Nach ihnen kommen rein rechnerisch die 3,0er (2,0 Projekt und 4,0 Klausur) und dazwischen gibt es nichts.    
Da auch keine Nachprüfungen angeboten werden, dürfen diese armen Würste nächstes Semester den ganzen Projektkram noch einmal machen. Fühlt sich nicht sehr fair an. Man kann ihnen doch die Chance geben ihre Endnote auf eine 4,0 zu verbessern, was nicht toll ist, aber zumindest bestanden.

Noch dazu suggeriert diese Notenskala ein seltsames Bild bzgl. der Leistungen der Studenten, eben diese Lücke zwischen 3,0 und 5,0. Und da hat man es wieder: Eine Klausur schlecht geschrieben, schon Pech gehabt. Keine Nachprüfung, nicht einmal mündlich. Tolle Wurst!

#### Nachtrag 1

Es gibt doch mündliche Nachprüfungen. Die Studenten wurden direkt vom Lehrstuhl kontaktiert. Man denkt nun vielleicht, dass die Korrektur so erfolgte wie ich es beschrieben habe. Nein. Man macht die Nachprüfung für die Klausur, nicht für die Gesamtnote. Da können die Durchfaller also von einer 5,0 zu einer 3,0 kommen und wenn man den Schnitt der Projektnoten nimmt (1,3) ist sogar eine 2,7 drin.

#### Nachtrag 2

Die Klausureinsicht zeigte, dass weniger das Spiel insgesamt, oder der Quelltext, sondern viel mehr der Vortrag wichtig für die Projektnote war. Wie gut man die eingesetzten Design Pattern im Vortrag vorgestellt hat. Die Durchsicht des Spiels änderte die Note um einen halben Punkt nach oben bzw. unten. Das ist irgendwie enttäuschend.