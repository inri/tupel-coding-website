---
layout: post
title: "Betriebssysteme I und II"
description: "Dominica ausführliche Zusammenfassung beider Betriebssysteme-Kursen."
date: 2011-10-07
tags: [HPI, Studium, Gastbeitrag]
published: true
category: it
comments: false
share: true
---

Komme ich nun in meiner Rückblickserie auf den Grund zu sprechen, wieso ich nicht schon längst wieder einen Artikel veröffentlicht habe: Betriebssysteme. Um genau zu sein die Prüfung in Betriebssysteme II. Aber alles der Reihe nach.


**Dies ist ein Gastbeitrag von Dominic Petrick.**

Da es sich gerade anbietet, blicke ich zunächst zurück auf den ersten Teil der Vorlesung, welcher im 3. Semester stattfand, also Betriebssysteme I.

## Betriebssysteme I

Betriebssysteme war damals ein klassisches Fach, von dem man wenig Ahnung hatte, also noch weniger als sonst. Dies liegt vielleicht auch daran, dass man zwar mit dem Begriff etwas anfangen kann, jedoch wenn es ans Eingemachte geht die Segel streichen muss. So gut wie jeder, außer vielleicht den Großeltern, nutzt heutzutage ein Operating System (OS). Sei es Windows, Linux oder auch für Leute mit mehr Geld: Mac.

Aber was passiert, wenn man einen Prozess startet? Ja nun, ein Fenster geht vielleicht auf... oder so. Wie wird das alles von OS verwaltet, was ich da so anschließe? Betriebssysteminterna sind eine ganz eigene Welt, und wenn man in diese Welt eintaucht, dann sieht man, welch verschiedene Aspekte beachtet werden müssen und welch ungeheure Komplexität hinter der Implementierung eines OS steht.

Ein OS ist prinzipiell erstmal ein Haufen Code, allgemein C Code, welcher sich auf magische Art und Weise zum fertigen System zusammenfügt. Es gibt Komponenten, welche dafür verantwortlich sind, die CPU auf die Prozesse, genauer die Threads, zu verteilen. Es gibt Komponenten für IO Handling, Interrupt Servicing, Windowing, ... und noch viele mehr, wobei der Interessante Teil dabei ist, dass man nicht die üblichen Abstraktionsgrade betrachtet, wie es bei Software im Allgemeinen der Fall ist. Denn normalerweise ist das Betriebssystem diese Abstraktion, die das Entwickeln von Software so schön vereinfacht. 

Bei Betriebssystemen muss man sich auf einmal mit hardwarenahen Geschichten rumschlagen, was eine ganz andere Sichtweise auf Probleme erfordert. Das Ziel der Lehrveranstaltung Betriebssysteme I war, erst grundlegende OS Techniken anhand des Windows Research Kernels (WRK) und am Beispiel des Windows OS zu vermitteln. Das heißt konkret, dass grundlegende Strukturierung eines OS, dessen Aufgaben und Kompetenzen und solche Sachen anfänglich relativ allgemein über alle OS hinweg erklärt wurde. Dann kamen Themenbereiche, die zwar Windows-spezifisch sind, jedoch von den Grundgedanken her allgemeingültig sind und wichtige Ideen von Betriebssystemen erklären.       
Dazu zählen Threadscheduling (mit Synchronisationsprinzipien), virtuelle Speicherverwaltung, Sicherheit in einem OS und Networking. Diese Themen wurden mehr oder weniger ausführlich abgedeckt und vermittelten sehr gut die Grundlagen von Betriebssystemen, dabei hatten wir praktische Aufgaben, wo wir im *echten* Code von Windows rumspielen konnten.

Die Übungen mussten gelöst werden und man wurde von einem Tutor betreut, dieses Konzept war äußerst effizient und hat maßgeblich zu meinem Lernerfolg und Verständnis beigetragen. Die Übungen waren gut durchdacht und hielten trickreiche Theoriefragen und zugegeben teilweise etwas heftige Praxisaufgaben bereit. Das Ganze bespricht man dann mit seinem Tutor, bzw. *erklärt* es ihm in einer Präsentation, und klärt etwaige Fragen, die beim Lernen und Bearbeiten auftraten.

Da Betriebssysteme I ein Pflichtfach für jedermann ist, müssen alle Studenten, ob sie wollen oder nicht, dieses Fach bestehen. Und das ist auch gut so, denn meiner Meinung nach würde ein äußerst wichtiger Baustein in der informatischen Ausbildung fehlen, welcher ungeheuer zum Gesamtverständnis beiträgt.    
Zum Schluss von Betriebssysteme I sei vielleicht noch angemerkt, dass die Endklausur definitiv machbar war und eigentlich alles, was man ausgiebig gelernt hatte für die Übungen, zur Anwendung kam, sodass man mit einem guten Gefühl aus der Prüfung gehen konnte und auch nicht die Lust an einer Fortsetzung vermiest wurde, so wie das in Computergrafik der Fall war :-)

## Betriebssysteme II

Mich persönlich hatte dieses Themengebiet sehr interessiert und ich wollte noch tiefer in die Materie einsteigen, Betriebssysteme II war da der offensichtlichste Kandidat, um genau dies zu tun.     

Man merkt dabei deutlich, dass Betriebssysteme II ein Fach für Interessierte ist und eben kein Pflichtfach. Die Inhalte sind sehr detailliert, aber auch gleichsam interessant. Und die Übungen sind vollkommen freiwillig (wenn auch ratsam).     
Die Übungen unterschieden sich nicht nur darin, dass sie nur bis auf eine Ausnahme vollständig praktisch waren und man somit über die Übungen nicht so gut wie in Betriebssysteme I die Theorie verinnerlichen konnte (es gab auch keinen Tutor), sondern sie hatten auch mit dem Vorlesungsstoff nur das Themengebiet gemein.

Die Vorlesungsinhalte gingen von Betriebssysteme I nahtlos in die von Betriebssysteme II über, sodass man sein Wissen aus Betriebssysteme I am besten noch komplett griffbereit haben sollte.     
Zunächst wurde über das Windows IO System geredet. Das IO System ist der Part am System, welcher die angeschlossenen Geräte des Systems verwaltet und Interaktionen mit ihnen erlaubt. Es wurde auch konkret über Treiberarchitektur in diesem Kontext gesprochen und man musste auch selber Hand anlegen, indem man in einer Übung einen kleinen Windows-Treiber selber programmiert.      
Weiter ging es mit Dateisystemen, wie z.B. NTFS, FAT, Unix FS. Es wurde sehr detailliert auf Architektur und Funktionsweise eingegangen, z.B. Verschlüsselungsmechanismen und Recovery. Dann kamen noch die Themen Echtzeitsysteme und eingebettete Systeme, was sie für Charakteristika haben, und warum Ausführungsgeschwindigkeit nicht alles ist und wo man das überhaupt braucht. Damit Atomkraftwerke nicht explodieren. Ok :-)     
Es folgten kleinere Themen wie Fehlertoleranz, also wie Systeme (Rechencluster) bestimmte Fehler wie Crashfaults oder Timingfaults überstehen können, ohne das deren angebotene Services ausfallen. Weiterhin noch eine kleine Networking-Wiederholung und Windows Scripting/WMI &amp; Registry, Windows/Linux/Unix Vergleich und Interoperabilität.     

Einen großen Teil der Vorlesung nahm das Thema Virtualisierung ein, anhand des Beispiels der Lösung von VMWare. Das heißt, es wurde Lehrmaterial des VMWare Curriculums verwendet, um Grundzüge und allgemeine Ideen der Virtualisierung zu erklären. Dort wurde der Stoff dann vollkommen abgefahren und plötzlich hatte man in meta-meta über Betriebssystemkonzepte gesprochen. Ein OS im OS, über einem OS. (Random Inception-Witz hier einfügen ;-))     
Am Schluss wurden noch alte Betriebssysteme und deren Interna beleuchtet.

Im Anbetracht der Tatsache, dass wir unter 20 Mann stark waren und die Vorlesung in einem Seminarraum im neuen Gebäude stattfand, war die Atmosphäre äußerst entspannt, Prof. Polze hat den bereits in Betriebssysteme I angeschlagenen Mix aus entspannter Vorlesung und kleinen Anekdoten weiterhin beibehalten, was ich sehr gut fand.     
Die Prüfung war auch nicht mehr schriftlich, sie war mündlich. Das in Verbindung mit der schieren Menge an Stoff war ein enormer Overkill. Für die Prüfung habe ich Vollzeitlernen über drei Wochen hinlegen müssen und ich kam gerade so hin. Das mag zwar zum einen an meiner perfektionistischen Veranlagung liegen (= alle Details lernen, könnte ja sein...), aber auch einfach an der vielen Themen die abgedeckt wurden.

Die Prüfung selber war entspannt, auch was man aus den letzten Jahrgängen und den Leuten so hört, und er fällt nicht über einen her, wenn man etwas nicht weiss, das ist nicht schlimm.

**Fazit also:** Das Fach kostet Unmengen an Energie und Zeit und ist wirklich nur für Leute, die das Themengebiet interessiert, zum Glück kann ich mich dazu zählen. Aber wenn es das tut, wird es den Erwartungen gerecht.