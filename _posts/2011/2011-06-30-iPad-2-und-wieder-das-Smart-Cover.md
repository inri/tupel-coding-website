---
layout: post
title: "iPad 2 und wieder das Smart Cover"
description: "Eine kleine Bestandsaufnahme nach 3 Monaten Smart Cover."
date: 2011-06-30
tags: [iPad]
published: true
category: it
comments: false
share: true
---

Seit drei Monaten nutze ich das iPad 2 mit dem Smart Cover. Kurz nach Erscheinen dieses magnetischen Schutzes für den Bildschirm, wurde es schon kritisiert. Es sei billig verarbeitet, es zerkratze den Bildschirm, es sei überteuert, wird schnell schmutzig, macht den Bildschirm nicht richtig sauber, und so weiter. Allein die Tatsache, dass in den letzten Monaten nichts weiter davon zu hören war, deutet es bereits an: Die Kritik war Quatsch.

Das SmartCover ist meines Erachtens der beste Schutz für das iPad. Es gibt mir einfach ein gutes Gefühl die Magneten klicken zu hören und zu wissen, dass der Bildschirm nun geschützt ist. So transportiere ich mein iPad auf jeden Fall sehr gerne. Und vor allem vergrößert sich das Gewicht nicht enorm, mit Smart Cover 736 g.

## Bestandsaufnahme

Die gummierte Vorderseite des Smart Covers ist bei mir sauber. 
Die Rückseite ebenfalls, lediglich einige Staubflusen finden sich, aber die kann man mit einigem Wischen abbekommen. 
Ansonsten franst nichts aus, die beiden Seiten halten fest zusammen.       
Die Magneten klicken noch.      
Der Bildschirm ist nicht zerkratzt.      
Die Rückseite hat einige ganz leichte Kratzer, aber das war ja auch abzusehen. Davor kann das Smart Cover nicht schützen.

Folgendes entdeckte ich erst heute bei genauer Betrachtung. Anscheinend haben sich winzig kleine Metallteile auf die Hauptmagneten gesetzt. Man sieht das an der Kerbe in der Leiste. Daraus folgte natürlich auch einige Kratzer an der Seite des iPads. Aber auch diese sind nicht tief. 

Was die Funktion der Reinigung des Bildschirm angeht, bin ich immer noch erfreut. Natürlich bleiben die Zwischenräume etwas schmutziger. Aber das Smart Cover macht sie nicht erst schmutzig und mit ein wenig hin- und herreiben werden auch die noch sauber. Auf jeden Fall müsste ich ansonsten mein iPad öfter mal selbst putzen. Und wer will das schon?

Ich würde immer noch eine volle Empfehlung für das Smart Cover aussprechen. Eigentlich dachte ich, dass andere Hersteller schon ähnliche Produkte auf den Markt bringen würden. 

P.S. Es gibt auch schon eine App zum Lernen, die das SC pfiffig ausnutzt: [Evernote Peek](http://itunes.apple.com/de/app/evernote-peek/id442151267?mt=8&affId=1315358)