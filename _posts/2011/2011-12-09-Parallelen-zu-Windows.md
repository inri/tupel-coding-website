---
layout: post
title: "Parallelen zu Windows"
description: "In wie fern lässt sich die Entwicklung der mobilen Systeme mit Windows vergleichen?"
date: 2011-12-09
tags: [Apple, Microsoft, Android, Technologie]
published: true
category: it
comments: false
share: true
---

Letzte Woche wurden zwei verschiedene Statistiken bzw. Zahlen veröffentlicht. Zum einen die Verkaufszahlen von Tablets in Amerika und zum anderen die Höhe der Gewinne von Android- und iOS-Entwicklern. Beide Zahlen haben direkt nichts mit Microsoft oder Windows zu tun, aber indirekt eben doch.

Wie kaum anders zu erwarten war, lag Apple bei beiden Werten vorne. Sie verkaufen ca. 90% aller Tablets in Amerika und die restlichen 10% teilen sich die Android Anbieter. Viel beeindruckender fand ich aber die Zahlen der beiden App Stores. Dass iOS-Entwickler sicherlich mehr Geld aus der i-Familie schöpfen können, ist klar, aber dass es ein Vielfaches von dem der Android-Entwickler sein würde, hätte ich nicht gedacht.      
Letztere haben bisher (seit es den Android Store gibt) 330 Mio. $ erhalten. Apple hat dagegen schon 4,5 Milliarden $ an seine Entwickler gezahlt. Das ist 14x so viel(!) und selbst wenn man großzügig den zeitigeren Start des Apple App Stores nicht mitzählt, kann man einfach nur zum Ergebnis kommen, dass Apple hier einen riesigen Vorsprung hat. Und das ist kein unwichtiger Vorsprung.

Wenn Apple eine Produktvorstellung hält, werden auch immer die Summen genannt, die iOS-Entwickler im App Store verdient haben. Für den i-Nutzer sind diese Summen nicht weiter spannend, aber sie sind ungemein wichtig.     
Bei Google dagegen wird nur auf die schnell steigende Anzahl von Android Geräten verwiesen: *Soundsovieltausend Neuregistrierungen pro Tag!*      
Natürlich ist Android das vorherrschende System. Es kostet den Hardwarefirmen nichts und läuft auf vielen verschiedenen Geräten. Mit dieser Tatsache hat Android die Systeme aller früheren Hersteller verdrängt, weshalb z.b. auch Nokia mit sinkenden Verkäufen zu kämpfen hat. Android gräbt aber größtenteils den früheren Nieder- und Mittelklassensmartphones das Wasser ab.       
Wo Nokia und Co. ihre Flaggschiffe in Stellung brachten, ist nun das iPhone getreten. Und nicht nur das. Das iPhone gräbt auch ein wenig an der Mittelklasse und erzeugt sich gleichzeitig quasi ein neue Kundenschicht (= Wer früher einen iPod hatte, hat sich nicht zwangsläufig das neueste und teuerste Nokia gekauft. Aber wie sieht das heute aus?).

Mein Windows-Vergleich möchte ich gerne an folgender Stelle ansetzen: Früher ist man nicht an Windows vorbeigekommen, heute kommt man nicht an Android vorbei. Lief Windows nicht immer auf allen Computern rund, tut das Android heute auch nicht. Das liegt natürlich nicht am System selber, aber die Kunden fragen nicht nach der Schuld. Sie sind nur genervt.
Bei den Mobiltelefonen hat Apple im Prinzip eine ähnliche Stellung wie mit ihren Computer. Sie sind teurer als alle anderen. Trotzdem sind sie erfolgreicher, denn eine entscheidende Komponente hat sich verändert.

## Wie Systeme wachsen

Die Gründe für diesen Erfolg liegen nämlich nicht allein am System und dem Telefon mit seiner verbauten Technik. Elementar wichtig sind die verfügbaren Apps auf der Plattform. Zum einen möchte ich als Softwareentwickler auch Geld verdienen und wähle mir einen Markt (= Plattform) mit viel Potential.     Äußerst lukrativ war in der Vergangenheit Windows und der Grund dafür war vor allem die enorme Verbreitung dieses Betriebsystems. 

Auf dem mobilen Markt war iOS eine zeitlang auch wesentlich weiter verbreitet als Android. Die ersten *mobilen Entwickler* entschieden sich für Apples Plattform und bekamen einen wundervollen Markt mit identischen Geräten. Und es funktioniert sehr gut für sie. Die Plattform und die Anzahl verkaufter Geräte wuchs, die potentiellen Kunden damit auch und Gewinne wurden ebenfalls erzielt, wie Apple oft betont.      
Nun kam Android als Gegenspieler hinzu, ein neues System mit viel Potential. Aber es brauchte am Anfang etwas Zeit, denn das Framework war nicht ausgereift und später wurde es zwar besser, aber es stellte sich heraus, dass Android-Benutzer kauffaul sind. Und so blieb es bis heute.      
Es sind nur 1,x Prozent der Apps im Android Store kostenpflichtig. Bei iOS sind es 12% und iOS-Nutzer haben durchschnittlich mehr Apps und geben mehr Geld für sie aus.
Dazu kommt, dass man als Android Entwickler Rücksicht auf eine Vielzahl von Geräten nehmen muss, was den Aufwand erhöht. Mehr Aufwand bei weniger Gewinnaussichten.

Paradoxerweise ist aktuell also nicht das am weit verbreitetste System finanziell erfolgreich, sondern der Zweitplatzierte. iOS in Form von iPhone und iPad verkauft sich wie geschnittenes Brot. Auf dem Smartphone-Markt fährt Apple zwei Drittel der Gewinne ein. Und bei Tablets sowieso. Den veröffentlichten Zahlen zu urteilen, scheint es sich für die Entwickler auch zu lohnen.     
Android muss sich überlegen, in welche Richtung es gehen möchte. Von den Hardwareherstellern wird es als Billigsystem eingesetzt. Es gibt keine Updates und oft Probleme mit Apps.      
Apple hält seine Geräte mindestens drei Jahre lang aktuell und damit auch aktuell für neue Apps. Mit weniger Aufwand kann ich bei Apple tolle Apps für eine größere und vor allem kaufbereitere Anzahl von Benutzern entwickeln.

Bei Android kommt es mir so vor, als ginge es vor allem um das Verkaufen eines Gerätes. Danach ist alles egal und der Kunde soll doch sehen wo er bleibt. In dieser Philosophie nähert man sich auch dem früheren Microsoft. Hauptsache eine weitere Windows-Kopie verkaufen, egal ob der Computer damit umgehen kann. Wichtig war, dass so viele Computer wie möglich mit dem Windows-System ausgestattet sind.

Nun kann man das als schnödes Thema über Smartphones abtun. Aber Apple hat vor 1,5 Jahren die nächste Stufe eingeleitet. Das iPad hat sich am perfektem iOS-System angekoppelt und sorgt nun für einen Wandel in der Computerwelt. Und in dieser Hinsicht kann Android nichts bieten und Microsoft schaut momentan auch ziemlich ratlos aus der Wäsche. Ihr Windows 8 soll zwar erstmals auf Tablet-Computer zugeschnitten sein, aber erst in einem Jahr erscheinen.

Apple hat die Konkurrenz abgehängt und macht es sich in diesem neuen Markt bequem. In dieser Hinsicht ist wiederum Apple auf dem Weg das Windows von morgen zu werden. Allerdings kann man nun darüber diskutieren, wo die Unterschiede liegen. Apple bietet sein System nur auf seinen eigenen Geräten an.      Einerseits wird dadurch eine Bedingung gestellt (Füge dich dem Apple System), andererseits auch noch die Alternative etwas anderes zu kaufen. Auch wenn es momentan so aussieht, als ob die Alternativen nicht mit Apple mithalten können, ist es eine Wahl, die der Kunde treffen kann. Bei Windows war und ist das eben nie so gewesen.

## Fazit

Wenn sich ein Kunde für Apple entscheidet, wird er mit einem tollen Gesamtsystem verwöhnt, kann tolle Apps kaufen und ist scheinbar sehr zufrieden mit seiner Wahl. Was bekommt ein Windows-Kunde für seinen Kauf?
Für einen Softwareentwickler ist Windows auch nicht mehr die offensichtlichste Wahl als Plattform und Android ebenfalls nicht. Bei Apple bekommt er gute Tools und viele potentielle Kunden.      
Darüber müssen sich die größeren nicht-Apple-Firmen Gedanken machen. Weiter die Billig-Schiene fahren und nur versuchen viele Geräte loszuwerden? Oder begreifen, dass der Kunde mehr als ein *Shiny Device* möchte?

#### Nachtrag 1

Auf der Le Web Konferenz in Paris hat Eric Schmidt (Google) einige Fragen beantwortet und interessante Statements gegeben. Er lobte Apple für iOS und sagte, es sei ein gutes System. Er verstünde auch, warum momentan noch so viele Entwickler vorwiegend und in erster Linie für iOS programmieren. Aber er ist sich sicher, dass sich das in einem halben Jahr (!) ändern wird. Dafür verantwortlich macht er das neue Android Ice Cream Sandwhich. Das bringe alles zusammen und mit der enorm steigenden Anzahl an Android Geräten, wird sich dieser Effekt eben verstärken.      
Ich finde seine Aussage sehr gewagt. Ich bin gespannt, wie es im Juni nächsten Jahres aussieht.

#### Nachtrag 2

Ich überarbeite aktuell die alten Artikel für den neuen Blog und wollte nur zu Protokoll geben, dass selbst jetzt, im August 2014, die Situation unverändert ist. Viele Entwickler präferieren immer noch iOS und eine Trendwende gab es nicht.
