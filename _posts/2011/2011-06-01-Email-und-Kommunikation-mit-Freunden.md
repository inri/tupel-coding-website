---
layout: post
title: "Email und Kommunikation mit Freunden"
description: "Einige Gedanken zur aktuellen Lage der Email und zukünftigen Trends."
date: 2011-06-01
tags: [Kommunikation, Web]
published: true
category: it
comments: false
share: true
---

Im Februar war mal wieder ein Workshop der HPI School of Design Thinking und thematisch ging es um Kommunikation und wie diese in naher Zukunft aussehen wird. Ich habe mir die Podiumsdiskussion mit Plattner, Meinel und Kembel teilweise angeschaut und kann größtenteils zustimmen. 

     
Ich leite kein Institut oder bin im Vorstand eines Unternehmens, aber Kontakte, mit denen ich kommuniziere, habe ich einige. Mir fiel in den letzten Jahren auch der beschriebene Trend, weg von der Email hin zu den sozialen Netzwerken, auf. Bei der Podiumsdiskussion wurde sogar die These aufgestellt, dass die Email in einigen Jahrzehnten fast ausgestorben sein wird.

Aber durch was wird sie ersetzt werden? Richtig klar, war das nicht. Herauskristallisiert hat sich aber, dass Emails immer seltener gelesen werden, je mehr Text sie haben umso seltener. Plattner und Meinel meinten, alles was länger als ein Absatz ist, lesen sie nicht mehr. Dann rufen sie lieber an und besprechen das persönlich, weil es schneller geht und man mehr vermitteln kann. Oder sie schreiben eine kurze Email mit der Aufforderung um Kürzung zurück.

Im privaten Bereich wird die Email in einigen Jahrzehnten vielleicht den Stellenwert des heutigen Briefes haben. Der Austausch normaler Nachrichten wird sich auf soziale Netzwerke verlagern. Dort hat man Bilder, Videos, Statusmeldungen seiner Freunde und kann auch flexibler kommunizieren. In einigen Jahren wird Telefonie größtenteils übers Internet laufen und Videotelefonie auch nichts exotisches sein. Dann quatscht man über diese Kanäle und muss keine E-Mails mehr schreiben.
 
Diesen Trend sehe ich auch so. Eine Frage: Mit wie vielen anderen Personen schreibt ihr euch sonst noch Emails mit mehr als 500 Worten? Also in der Tat richtig lange, ausführliche Emails über private Themen. Kein Herumschicken von Hausarbeiten oder gecopypasten Texten.       
Es wird in der Tat weniger werden und den technisch eher wenig versierten Menschen wird das ausreichen. Um auf den laufenden zu bleiben, reichen die oberflächlichen Informationen der sozialen Netzwerke.

Über diese Netze können wir auch mal schreiben. So in 2006 / 07 flutete eine große Masse meines Bekanntenkreises StudiVZ. Vorher waren einige vereinzelt in StudiVZ, SchülerVZ, MySpace und hatten vielleicht einen Blog. Das waren aber wirklich nur sehr wenige. So bis 2008 rum setzte sich die größte Masse in StudiVZ ab. Es wurden viele Bilder gepostet, Statusmeldungen veröffentlicht und irgendwie waren viele von ihnen relativ aktiv.      
Diese Entwicklung nahm ab 2009 langsam ab und ich würde sagen, mittlerweile sind <sub>2</sub>/<sub>3</sub> meiner Freunde auf StudiVZ nicht mehr aktiv, haben seit mindestens ein Jahr ihre Profile nicht mehr angefasst. 

Nach meinem Austauschjahr war ich bei Facebook und kannte dort fast nur Norweger. Gleichzeitig mit dem langsamen Abnehmen der StudiVZ-Aktivität, kamen immer mehr Leute aus meinem Bekanntenkreis zu Facebook.     
Dieses Jahr scheint das regelrecht zu explodieren. Viele nicht-aktive StudiVZ-Freunde habe ich in letzter Zeit zu meinen Facebook-Freunden gemacht. Meines Erachtens hat Facebook &quot;gewonnen&quot;, ich würde StudiVZ verkaufen und jetzt noch einige Millionen einstreichen.

Ich bin fest überzeugt, dass die lange und mittellange Kommunikation über Netzwerke wie Facebook ablaufen wird. Ich bin mir nicht mal sicher, ob die Email-Adressen meiner Freunde und Bekannten noch aktuell sind. Warum sollte ich dann noch schreiben?      
Emails werden für nahe Verbindungen wichtig sein, also Freunde im Sinne von &quot;*richtigen*&quot; Freunden und Arbeitskollegen. Da muss man vielleicht auch mal mehr kommunizieren.

Ich breche das Nachdenken mal ab. Aber dieses Thema müsste mal vertieft werden...