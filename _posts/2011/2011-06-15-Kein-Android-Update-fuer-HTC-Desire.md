---
layout: post
title: "Kein Android Update für HTC Desire"
description: "Scheinbar bekommt mein HTC Desire kein Update auf Android 2.3 und das finde ich ziemlich unschön."
date: 2011-06-15
tags: [Android]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-htc-desire-lg.jpg
  credit: Ingo Richter
  creditlink: http://www.ingorichter.org
---

Ich bin sauer und ich bin enttäuscht. Laut der Facebook-Seite von HTC, soll es kein Update für das Desire auf Gingerbread (Android 2.3) geben, da der Speicher für ihre Erweiterung Sense nicht ausreicht. 


Seit Dezember  letzten Jahres, als das Gingerbread Update erschienen ist, programmiert HTC an dem Update und stellen erst jetzt fest &quot;*Oh, unsere Sense-Oberfläche frisst zu viel Speicher. Tja. Doof.*&quot;?

Da brauchen sich HTC, Samsung, Google, etc. gar nicht wundern, wenn ihnen die Kunden weglaufen, denn ich bin sicher nicht der einzige, der sich von ihnen verarscht fühlt.     

Vor 15 Monaten kam das Desire auf den Markt und war damals das Flaggschiff aller Android Smartphones. Es dauerte eine Weile, bis das Update auf 2.2 veröffentlicht wurde und man versprach auch ein Update auf 2.3. Scheinbar war das Desire aber schon im Dezember, 9 Monate nach Erscheinen und als als Gingerbread vorgestellt wurde, zu alt für das neueste Android-System.
Diese Tatsache allein ist schon nicht mehr akzeptabel.     
Man möchte vielleicht einwenden, dass ab jetzt alles besser wird, da die Hersteller eine 2-jährige Update-Garantie geben. Aber wird diese Garantie in der Tat funktionieren? Oder brauchen die Hersteller dann ein Jahr für ein Update um am Ende sagen zu können, dass es technisch nicht geht?

Das Problem liegt an der Sense-Oberfläche von HTC. Kann man uns nicht zumindest die Wahl geben: 2.3 ohne Sense oder 2.2 mit Sense? Nein, da wird der Kunde bevormundet, muss sich also entweder damit abfinden, viele Neuerungen und den Zugang zu neueren Apps nicht zu erhalten, oder selbst sein ROM flashen. 

Bei Apple wird der Kunde auch gerne bevormundet, aber da komme ich mir nicht so betrogen vor. Komischerweise ist der Aufschrei bei Apple in den Medien immer gleich viel größer.
Ich glaube, es ist HTC scheißegal, ob die Desire-Besitzer ein Update erhalten oder nicht. Die Arbeiten längst an den neuen Handys, denn schließlich muss bald die nächste Flaggschiff-Generation vom Stapel gelassen werden, das Desire HD und Desire S sind ja nun schon 8 und 4 Monate alt. In diesem Punkt hat sich HTC perfekt an den bestehenden Markt mit Samsung, Nokia und Co. angepasst. Möglichst viel Hardware verticken, der Rest kann uns egal sein.

Das iPhone der letzten Generation (3GS) erschien im Juni 2009 und wird das im Herbst angekündigte Update auf iOS 5 auch erhalten - also mindestens 27 Monate nach Erscheinen bekommt dieses alte iPhone noch ein Update. Das ist  drei mal so lang wie das Desire! Das ist so, als würde das HTC Hero (Juli 09) noch ein Update auf das im Spätsommer angekündigte Android Ice Cream (3.x) erhalten. Völlige Utopie! Natürlich hat es das Hero nur bis 2.1 geschafft, 10 Monate aktuell.

Ich hab mir noch mehr Zahlen angeschaut. Die Quelle ist Wikipedia, sucht euch die Infos selbst zusammen. Bei Apple sieht der Rhythmus homogen aus. Im Abstand von fast immer einem Jahr, erscheint seit 2007 eine neue Version ihres iOS und ein neues iPhone. Die iPhones haben bisher immer zwei Jahren vollen Funktionsumfang für die neuste iOS-Version gehabt und im dritten Jahr bekommen sie zwar ein Update, aber eben mit einigen Einschränkungen, was aber teils auch an der älteren Hardware liegt. Man kann also 36 Monate lang ein aktuelles Handy haben und die neuen Features und Apps nutzen.

Bei Android sieht es viel bescheidener aus. Signifikant ist auf jeden Fall, dass die Updates für die Handys, erst Monate nach dem offiziellen Vorstellen der neuen Android-Version veröffentlicht werden.     
Das HTC Magic (April 2009) bekam im Dezember letzten Jahres ein Update auf Android 2.2, zur gleichen Zeit wurde 2.3 vorgestellt.       
Das HTC Hero schaffte es nur 10 Monate lang, ein aktuelles Handy zu sein.       
Das Motorola Milestone bekam vor einigen Wochen ein um ein Jahr verspätetes Update auf 2.2., immerhin.       
Für das Desire bekommen wir in einem halben oder einem Jahr vielleicht doch noch ein Update, wer weiß. Dann ist aber Android 4.0 draußen und mit ihm ein Dutzend neuer Handys.

Die einzige Ausnahme sind die Developer Phones, die allein von Google gepflegt werden. Davon gab es zwar bisher erst zwei (Nexus One und Nexus S), aber die sind immerhin aktuell mit dem 2.3.3er Update. Wobei das Nexus One auch erst 1,5 Jahre alt ist. Positiv fällt sonst noch Samsung mit dem Galaxy S auf, welches zwar auch ein halbes Jahr auf ein 2.2er Update warten musste, aber gerade ein 2.3.3er Update bekam.      
Sicherlich sind die Smartphonehersteller größtenteils Schuld daran, dass es keine Updates gibt oder wenn, dann nur sehr spät. Und es ist auch bitter notwendig, dass Google endlich mal einschreitet und diese Regelung der 2-Jahre-Update-Garantie einführt.

Den anderen Herstellern würde es das Genick brechen, wenn Apple ein Handy für unter 200€ auf den Markt bringt. Das machen sie natürlich nicht, schließlich kostet das über zwei Jahre alte iPhone 3GS immer noch 500€. Und außerdem würden sie dann entweder wesentlich schlechtere Hardware verwenden = eingeschränkte iOS-Funktionen = weniger Apps... = ... = Android, oder sehr viel Gewinn einbüßen. Sie können ihre teuren iPhones wunderbar verkaufen und sind zufrieden.

Für mich macht es am Ende keinen Unterschied, ob ich ein teures iPhone einmal kaufen muss oder zwei Android-Phones, nur um aktuelle Funktionen zu haben. Vor allem hat das iPhone auch nach über zwei Jahren noch 200 - 300€ Wiederverkaufawert

Ich bin gespannt, was im Herbst auf uns zukommt. Neues Android, neues iOS, neues iPhone. Ob mein nächstes Handy dann noch ein Android sein wird, kann ich nicht versprechen.

**Nachtrag:** Es soll nun doch ein Update für das Desire geben. Aber wann, das haben sie nicht gesagt. Wir warten ja erst 7 Monate. Wenn Apple das mit seinen iPhone-Käufern so handhaben würde....

**Nachtrag 2:** Das heiß ersehnte Desire Update rollt wohl momentan aus bzw. kann es geladen werden. So richtig überzeugt sind HTC von ihrem Werk nicht, sie empfehlen das Update nur Experten (für Entwicklungszwecke). Ansonsten funktioniert es nicht mit allen Geräten, viele persönliche Daten gehen verloren und sollten vorher gesichert werden. Es ist also nicht geeignet für normale Benutzer.     
 
Abgesehen davon, dass jeder Entwickler längst ein modifiziertes Android installiert haben sollte, scheint die Update-Politik bei HTC ziemlich gescheitert zu sein. Das sollte man beim nächsten Smartphonekauf im Hinterkopf behalten.