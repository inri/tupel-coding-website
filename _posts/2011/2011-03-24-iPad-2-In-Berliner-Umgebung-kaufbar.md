---
layout: post
title: "iPad 2 in Berliner Umgebung kaufbar?"
description: "Kann man das neue iPad in der Berliner Umgebung kaufen? Was sagen die Händler dazu?"
date: 2011-03-24
tags: [iPad]
published: true
category: blog
comments: false
share: true
---

Wie, wo und wann bekommt man nun das iPad 2? Ich weiß nicht welche Möglichkeit die beste ist. 


In Potsdam schreibt ein Apple-Reseller:

*Leider können wir für morgen keine Vorbestellungen entgegennehmen. Der Verkaufsstart ist morgen um 17 Uhr. Wir laden Sie morgen herzlich ein sich das neue iPad anzuschauen und mit viel Glück können Sie es morgen sofort mitnehmen.*

Viel Glück? Aha.     
In Berlin gibt es zwei Gravis-Stores, die interessanterweise unterschiedliche Infos angeben:

*Vielen Dank für Ihre Anfrage. Der offizielle Verkaufsstart des iPad 2 beginnt am Freitag den 25.03.2011 ab 17:00 Uhr.
Über die Verfügbarkeit der einzelnen Modelle können wir derzeit leider keine Aussage tätigen. Verbindliche Reservierungen nehmen wir Aufgrund dieser Situation derzeit nicht entgegen. Ich empfehle Ihnen am 25.03.2011 unseren Store zu besuchen und die Verfügbarkeit der Geräte ab 17:00 Uhr zu erfragen.*

*Gerne merke ich Sie völlig unverbindlich für das von Ihnen favorisierte Gerät vor. Die Vormerkung gilt nicht als Auftragsbestätigung. Sollten die Geräte Aufgrund des zu erwartenden Ansturms vergriffen sein, können wir Sie nach erneutem Wareneingang anhand der Vormerkung kontaktieren.*

*Bitte teilen Sie mir, für eine von Ihnen gewünschte Vormerkung, Ihre Adresse mit, welche auf der späteren Rechnung erscheinen soll. Bitte nennen Sie mir für eine unverbindliche Vormerkung ebenfalls das von Ihnen gewünschte Modell.*

Das war Berlin-Steglitz und der &quot;reine&quot; Berliner Store schreibt:

*Der offizielle Verkaufsstart des [...] Ab 16:00 Uhr haben Sie die Möglichkeit sich in unserer Filiale in die Schlange der Interessenten einzureihen. Leider haben wir keine Aussage über die Stückzahlen die uns erreichen. Auch wissen wir nicht wie viele Kunden uns besuchen. Der Verkauf wird jedoch nicht nach 5 oder 10 iPad 2 beendet sein.*

*Ich kann sehr gut nachvollziehen, dass Sie am Freitag gern
mir einem neuen iPad 2 unsere Filiale in ein glückliches
Wochenende verlassen möchten. Ich wünsche Ihnen ganz viel
Glück und bedanke mich für Ihr Verständnis.*

Wieder mal viel Glück, ist das eine Lotterie?     

Wie sieht es denn online aus? In Neuseeland und Australien sind die Store schon *offen* und es wird eine Lieferdauer von 2 - 3 Wochen angegeben. Im Rest der Welt geht es 2 Uhr nachts los. Allerdings sind z.b. die Smartcovers schon bestellbar und auch mit 2 - 3 Wochen Wartezeit deklariert. Meines Erachtens wird das auch bei den iPads so ausgeschrieben sein.

Wer es sofort will, sollte ab in die Schlange, ansonsten heißt es warten. Weitere Elektronik-Shops habe ich nicht angeschrieben, bei denen wird es bestimmt nicht besser aussehen.