---
layout: post
title: "Wirtschaft und Universität"
description: "Wie kann man die enge Bindung zwischen Universität und Wirtschaft bewerten?"
date: 2011-06-06
tags: [Uni, HPI]
published: true
category: blog
comments: false
share: true
---

Die Technische Universität Berlin steht momentan in der Kritik, sie lasse sich von der Wirtschaft kaufen. Der Grund sind die nun bekannt gewordenen Verträge zwischen der TUB und der Deutschen Bank. 


Ihnen wurde ein Institut inklusive zweier Professuren bezahlt und dafür bekommen sie Forschungsaufträge von der DB und natürlich jährlich etwas Geld (3 Mio. €).     
Besonders kritisch wird gesehen, dass die DB Veröffentlichungen zurückhalten kann und Studenten der TUB für die DB rekrutiert werden. [In diesem Interview](http://www.spiegel.de/unispiegel/studium/0,1518,766535,00.html) verteidigt sich der Präsident der TUB und will im Prinzip sagen, dass er solche Art Verträge gut findet, notwendig findet, einige Punkte heute sicherlich anders aushandeln würde, aber prinzipiell dazu steht.

Das HPI finanziert sich ja indirekt auch von dem Geld Hasso Plattners (SAP). Er zahlt Geld in die HPI Stiftung ein und diese finanziert das HPI. Bisher hat er um die 200 Mio. Euro investiert. Als ich in meinem Bekanntenkreis von dem Institut erzählte und das z.b. die Bachelorprojekte immer mit Partnern aus der Wirtschaft realisiert werden, war die Skepsis nicht gerade klein. Der Vorwurf, das HPI forsche und arbeite für die Wirtschaft, schwang in kritischen Bemerkungen stets mit.

Was konnte ich damals groß erwidern? Nicht sehr viel, irgendwie sah es auch danach aus. Mittlerweile weiß ich es aber besser. Weder Herr Plattner noch SAP oder sonst wer hat die gleichen Zugriffsrechte wie die Deutsche Bank bei der TUB. In meiner Forschungsgruppe haben wir heute ein Paper eingereicht und ich wüsste nicht, dass irgendjemand die Veröffentlichung autorisieren musste. Genauso weiß ich von keinem Bachelorprojekt, das nicht vorgestellt wurde, weil der Partner Einspruch erhoben hätte.

Hasso Plattner wollte eine Informatikinstitut schaffen, welchen eben nicht so ist, wie die tausend anderen Institute, die es an jeder Universität gibt. Informatik ist theoretisch und ohne Frage wird man auch weiterhin theoretisch forschen. Aber es ist schade, dass diese theoretisch arbeitenden Studenten in der Wirtschaft eine andere Situation vorfinden, die sich deutlich von der Forschung unterscheidet. Was liegt also näher, als der Zugang zu praktischen Projekten in Verbindung mit dem Studium. Die Wirtschaftspartner können Problemstellungen vorgeben, die sich aus bloßer Theorie nie so ergeben würden.

Außerdem stellt sich doch die Frage, warum die Wirtschaft so evil sein soll. Die Angst der Akademiker rührt aus der Befürchtung, dass irgendwann die Wirtschaft die Forschung bestimmt und gewisse Forschungsbereiche, die keine Relevanz für die Wirtschaft haben, verkümmern. Auf der anderen Seite brauchen die Universitäten Geld, welches sie vom Staat nicht mehr in ausreichender Menge erhalten.       
Was soll man also machen? Studiengebühren einführen bzw. erhöhen, weil das Studium noch nicht teuer genug ist? Oder mal die zur Kasse bitten, denen die Ausbildung der Studenten nützt, also Unternehmen. Ich weiß, Unternehmen zahlen Steuern und aus dem Steuertopf wird auch die Bildung bezahlt. Aber es könnte doch für ein Unternehmen ein viel größerer Anreiz sein, bestimmte Unis oder Institute zu fördern, persönliche Bindungen aufzubauen. Diese Art der Förderung ist in den USA viel verbreiteter. 

Ich denke, in dieser Richtung gibt es viel Potential und wie genau die Umsetzung aussehen soll, weiß ich auch nicht, denn ich bin kein Unternehmen und keine Uni.    
Vielleicht sollte man lieber jetzt schon mit den Unternehmen verhandeln und nicht erst, wenn man als Uni vor dem bankrott steht und keine andere Wahl hat, als Unternehmen anzubetteln.