---
layout: post
title: "iPad 2 und Überraschungen, again"
description: "Das iPad 2 kam doch noch nicht heraus."
date: 2011-02-26
tags: [iPad]
published: true
category: it
comments: false
share: true
---

Au Mann! Mitte Januar sprachen alle Quellen vom 1. Februar als Event für das iPad 2. Eine Woche später, als der besagte Tag schließlich näher rückte, wurde die Gerüchteküche still. Die Einladungen zum iPad 2 Event wurden auch vier Tage vorher noch nicht verschickt und es dämmerte mir, dass man wohl noch warten müsse.

Generell verstummten die Gerüchte ein wenig, wobei natürlich schon das meiste gesagt wurde, von USB bis Retina-Display war alles dabei. Was gäbe es da noch zum raten? Im Februar gehörten die Nachrichten den Android-Tablets, die endlich mal auftauchten! Sie wurden erstmals vorgestellt, Samsung zeigte sein Galaxy Tab 10.1 (schlechter Name), Motorola das Xoom, und HP und HTC haben nun auch welche.      
Die Preise sind teilweise bekannt und interessanterweise nicht billiger als bei Apple, teils sogar teurer. Dieser Umstand ließ sich schon erahnen, als Samsung sein kleines 7-Zoll-Galaxy Tab vorstellte, welches in der 16 GB Ausführung mit 450€ zu Buche schlägt.

Andererseits kann ich die Hersteller verstehen, dass sie die Tablets nicht billiger anbieten (können/wollen). Geht man davon aus, dass in den Tablets jeweils die neuste mobile Technologie verbaut ist (war vor einem Jahr bei Apple der Fall, nun natürlich nicht mehr) und sie lediglich ein wesentlich größeres Gehäuse samt Touchscreen haben als Mobiltelefone, dann kann der Preis natürlich nicht günstiger sein, als die jeweils aktuellen Smartphone-Flaggschiffe kosten und das sind meistens um die 500€+.

Nächste Woche, am 2. März, wird mit großer Wahrscheinlichkeit das iPad 2 vorgestellt. Ich habe eine Meinung gelesen, die basierend auf angebliche Lieferschwierigkeiten von Bildschirmen darauf spekulierte, dass Apple vielleicht kein iPad 2 vorstellt, sondern ein iPad 1.5 und das richtige Update dann erst im Herbst kommt. Ich hoffe doch nicht.  
   
Zum einen kündigt Apple ein Produkt an, welches das Produkt des Jahres 2011 werden soll. Ein iPad mit Kamera-Update wird es dann wohl eher nicht reißen. Und sie hatten ja auch mehr als genug Zeit zum Tüfteln und neu entwickeln, was die anderen Hersteller noch vor sich haben.          
Zum anderen sind die Gerüchte mal wieder blabla. Vielleicht gibt es tatsächlich Schwierigkeiten bei der Produktion und dann kann Apple eben nicht xxxx Stück ausliefern sondern vielleicht nur xxxx minus 50.000 Stück. Wer weiß das schon?

Bei meinen technischen Wünschen gebe ich kurzbündig zu Protokoll: Besserer Prozessor (non-handy!), bessere Grafik, wesentlich mehr RAM, 32 GB als Einsteiger, flacher/leichter, vielleicht höhere Auflösung und wenn es etwas billiger wird, wäre das ganz cool, aber ich zweifle.