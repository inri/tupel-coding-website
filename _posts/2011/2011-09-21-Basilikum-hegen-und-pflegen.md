---
layout: post
title: "Basilikum hegen und pflegen"
description: "Tips wie die Basilikumpflanze nicht so schnell verkümmert."
date: 2011-09-21
tags: [Tips]
published: true
category: blog
comments: false
share: true
image: 
  feature: 2011-basilikum-lg.jpg
  credit: Ingo Richter
  creditlink: http://www.ingoricher.org
---

Wer gerne Pesto isst und zubereitet, wird sicherlich die eine oder andere Basilikum-Pflanzen im Zimmer stehen haben. Kann man für ein gutes Pesto mal eben 3 bis 4 Pflanzen entpflücken, reichen wenige Blätter aus, wenn man mal hier mal da, das Essen verfeinern möchte. Es lohnt sich also immer frisches Basilikum im Haus zu haben.

Aber Basilikum ist eine empfindliche Pflanze. In den letzten Monate habe ich ein halbes Dutzend pflanzen gepflegt und gebe einige Tips wie das am besten geht.
 
### Wasser von unten!

Die wichtigste Regel: Den Basilikum NUR von unten gießen! Nicht von oben, denn dann beginnt die Erde zu schimmeln und der Basilikum geht sehr schnell ein.     
Am besten man stellt die Pflanze in einen großen Übertopf. Groß genug, dass man das Wasser in den Übertopf gießen kann. Ich benutze eine längliche Tupperschachtel und habe zwei Pflanzen darin stehen. Als ich mit meiner Zucht begann, habe ich sehr penibel darauf geachtet, dass der Basilikum immer genügen Wasser im *Übertopf* hat. Letztlich konnte ich aber nur zwei mal Pesto machen und danach waren die Pflanzen dahin.
Was war passiert? 

### Wurzelfäule

Wenn die Wurzeln des Basilikum ständig im Wasser sind, beginnen sie zu faulen und sterben langsam ab. Nach und nach trocknet der Hauptstiel des Basilikum ab und die Blätter trocknen aus. Dieser Vorgang dauert Monate, aber wenn man merkt, dass die Stiele braun und trocken werden, wird die Pflanze absterben.     
Wie man Wurzelfäule verhindern kann, weiß ich leider noch nicht. Ich befolgte den Tip, dem Basilikum nicht ständig Wasserfüßchen zu geben. Also goss ich ihn alle 2 Tage (ca. 250 ml pro Topf). Er nahm das Wasser gut auf und stand nun nicht mehr im Wasser, aber die Wurzelfäule kam trotzdem.    
Das untere Bild ist mein aktueller Basilikum, ca. 4 Monate alt. Noch lebt die Pflanze, aber die Blätter sterben langsam ab. Ich muss bald wieder Pesto machen...

Ich werde bei der nächsten Pflanze weitere Ideen ausprobieren. Vielleicht hilft das Umtopfen in einen größeren Topf? Grundsätzlich sind vier Monate aber kein kurze Zeit. Da kann man ungefähr 2 bis 3 mal Pesto zaubern.

### Weiteres

Wenn ihr dem Basilikum die Blätter zupft, lasst die mittelgroßen und kleinen dran. Alles, was kleiner als ein Daumen ist, bringt sowieso kaum Masse. Wenn ihr zu viel zupft, schafft die Pflanze es vielleicht nicht mehr und stirbt ab.

Der Basilikum wird irgendwann Blüten bekommen. Die riechen noch intensiver als die Blätter und vor allem steckt die Pflanze ihre Energie in die Blüten. Aber wir wollen eigentlich keine Blüten essen, also ab damit. Die Energie soll in die Blätter gehen.

Mit diesen Tips wird eure Basilikum-Pflanze einige Monate leben und euch lecker Essen bescheren. Man muss ja nicht für jedes Gericht eine neue Pflanze kaufen...