---
layout: post
title: "iPad 2 und erfüllte Erwartungen"
description: "Einige Gedanken zum neuen iPad und wieso Android keine Geräte auf dem Markt hat."
date: 2011-03-02
tags: [iPad]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-ipad-2-lg.jpg
  credit: Apple
  creditlink: http://www.apple.com
---

Sehr schönes Gerät. Ich bin tatsächlich positiv überrascht. Allerdings muss ich noch für die Betriebssystemklausur lernen, also fasse ich mich kurz.

Ein paar Eckdaten-Vergleiche: DualCore Prozessor mit 1GHz, Grafik zwar noch unbekannt, soll aber 9x schneller sein, Speichergrößen sind die gleichen, Akkulaufzeit zum Glück auch (10 Stunden), aber die Größe hat sich positiv verändert und damit auch das Gewicht. Wir sprechen hier von einem 100 g weniger und 4,6 mm dünneren iPad 2 (<sup>1</sup>/<sub>3</sub> dünner). Das sind in der Tat sehr angenehme Zahlen, 600 g Gewicht. Je eine Kamera vorne und hinten gibt es nun auch, die gleichen wie im iPhone und der Bildschirm ist vermutlich auch ähnlich.

Genauso wie die Preise, was vielleicht schade ist, aber andererseits die Neuerungen auch nicht wenig sind. Mein Favorit ist ja das Softcover, eine Art Hülle, welche magnetisch funktioniert und das iPad auch bequem kippen kann. Neben dem Schutz des Bildschirmes wird wohl auch gleich auf Fingerabdrücke gereinigt. Es sieht genial aus.

Die Software wurde natürlich auch verbessert und einige neue Programme sind hinzugekommen. Das muss man dann in echt mal anschauen. Aber sie haben schon *gedroht* das alles wieder mal sehr gut auf die Hardware angepasst ist.     
Und genau das ist auch der Grund, wieso es mit den Google-Tablets noch ewig dauern wird. Vor einigen Wochen erst, haben die Hersteller die neue Version von Android bekommen und hacken schnell ihre Software drauf und schicken es dann zur Produktion, damit es noch vor Ende des Sommers erscheint. Und das, was dann erscheint, wird von der Performance trotzdem nicht mithalten können.       
Mag sein, dass sie alle einen tollen nVidia Tegra 2 Grafikchips verbaut haben. Aber optimal wird er sowieso nie ausgenutzt und bis die ersten Entwickler gute Anwendungen/Apps für diesen Chip gebastelt haben, vergehen noch Monate. Monate, in denen für Apple leistungsstarke Apps ins Portfolio kommen und sie schon an der nächsten Generation des iPads arbeitet.     

In einem Jahr denken wir dann *Okay, nun sind ein paar richtig gute Programme da und die Tablets okay* und dann -BÄM! stellt Apple das iPad 3 vor und alles ist dahin. So war es dieses Jahr doch auch. Die Hersteller haben endlich den explodierenden Tabletmarkt erstürmt (mit unfertigen Geräten, die erst in Monaten erscheinen) und kaum zwei Wochen später -BÄM! zeigt Apple ein neues Gerät, welches technisch mithalten kann, sofort verfügbar ist und die bessere Software hat.

Wenn ich schon dabei bin, mehr Kritik zum Verkauf. Apple versorgt mit dem iPad 2 Amerika ab nächste Woche und andere wichtige Länder bis Ende März. Dann haben wir vielleicht mit etwas Glück die ersten Android-Tablets auf dem Markt, wobei Markt hier der falsche Begriff ist. Motorola und Samsung verticken ihren Kram drei Monate lang bei Mobilfunkanbietern.   
Rechnet mal mit, angenommen und utopischerweise das Galaxy Tab 10.1 erscheint auch nächste Woche, dann kann man es erst Mitte Juni im freien Handel kaufen. Bis dahin wird es vermutlich für 700€ angeboten werden, beim Xoom weiß man, dass es erst Ende April erscheint und bis Ende Juli bei der Telekom schmort.

Bis dahin hat Apple wieder sämtliche Verkaufsrekorde gebrochen und das ist ja auch kein wundert, ihr Gerät ist billiger, leichter und kleiner, läuft 10 Stunden und hat abwechslungsreichere Software.

Noch einmal zur Klarstellung: Ich bin kein *Apple-Fanboy*. Mit Apple beschäftige ich mich erst seit knapp einem Jahr ernsthaft. Genauso neugierig, wie die Entwicklung von iOS, verfolge ich auch Android. Der Tabletmarkt ist die Zukunft, das ist meine Meinung und ich erwarte viel von Android.     
Die ganzen Android-Tablets hätten schon Ende letzten Jahres kommen müssen, dann wäre man ansatzweise auf den Fersen von Apple. Aber momentan hängt man mindestens ein Jahr hinterher und ich sehe da keine Besserung in naher Zukunft.