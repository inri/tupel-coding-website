---
layout: post
title: "The Next Web Revolution?"
description: "Einige Gedanken zu zukünftigen Entwicklungen und Innovationen im Web Bereich."
date: 2011-09-05
tags: [Web, Technologie]
published: true
category: it
comments: false
share: true
---

In den letzten Jahren gab es diverse Entwicklungen im Internet. Einige wenige waren der Beginn kleiner Revolutionen, an dessen Ende eine große Anzahl Menschen die Projekte, zumeist verschiedene Arten von Webdiensten, auch benutzten. 

Beispielhaft sind folgende: Google mit seiner Suchmaschine, MySpace trat die sozialen Netze los, Facebook überrollte alles, und Twitter änderte die Art und Weise der Weitergabe kurzer Informationen. All diese Beispiele benötigten zwei grundlegende Kernbedingungen: Die Idee zum einen und die Technik zum anderen.

Twitter hätte vor 10 Jahren keiner genutzt, sie wären kaum bekannt gewesen. Aber mit dem Aufkommen der internetfähigen Mobiltelefone war die technische Grundlage geschaffen, die damals noch gefehlt hatte.     
Ähnlich erging es sozialen Netzwerken vor Facebook. Die rudimentäre Idee gab es schon vorher, aber es waren viel zu wenig Menschen zu dieser Zeit online (Technik) und vor allem wurden die falschen Zielgruppen ins Visier genommen (Erweiterung der Idee). Facebook richtete sich anfangs an Studenten, also an gebildete Menschen mit Zugang zum Netz (an dieser Stelle müsste eine Studie zitiert werden, die belegt, dass gebildete Menschen öfter Zugang zum Internet haben, als andere).

Für revolutionäre Ideen im Bereich des Internets, muss der Zeitpunkt von Idee und technischer Reife übereinstimmen. Diese Erkenntnis bzw. These soll mir im folgenden dienen. Ich habe mich gefragt, worin die nächste kleine Revolution besteht. Welche Plattform, oder Technik wird in den nächsten zehn Jahren eine große Menge der Menschen im Netz erfassen und mitreißen?

## Revolution Right Now

In welchen mehr oder weniger kleinen Entwicklungen befinden wir uns zur Zeit? Im Fokus stehen Mobiltelefone und deren Systeme. Vor allem die Frage: Wie werden Apps, Medien und Games in Zukunft organisiert und behandelt. Diese Fragestellung wird sich auf Tablets erweitern und sich in dieser Technik vermutlich sogar manifestieren.

Um neue Ideen zu entdecken, sollte man die Möglichkeiten in diesem Bereich ausloten. Was kann man mit der mobilen Technik machen, mit GPS, Datenspeicher in der Cloud und möglicherweise komfortableren Entwicklungssprachen? Welche bestehenden Ideen kann man einfach auf die neuen Bereiche verschieben und ergänzen, welche Probleme erfordern neue Antworten? Und wie lässt sich neue Technik in bestehende Lebensbereiche integrieren.     
Beispielhaft wäre das iPad im Krankenhaus zu nennen oder wie man dafür Apps für autistische Kinder entwickeln kann (siehe Apple Werbeclip). Meiner Freundin kamen grandiose Ideen, wie man mit dem iPad auch Patienten mit Sprachstörungen helfen kann, denn die bestehenden Geräte sind technisch veraltet, zu schwer, zu umkomfortabel.

Aber das sind alles aktuelle, absehbare Entwicklungen. Mit den rasanten Anstieg der Tabletverkäufe ist vielen Entwicklern und Ideensucher klar geworden, in welche Richtung man schnuppern sollte. Doch was wird die kommende Überraschung sein?

## A Revolution?

Ich weiß es nicht. Und wenn doch, ich würde es nicht verraten. Ätsch. Aber aktuell sieht es eher nach dem Verbessern von bestehenden Ideen aus. Hier ein neues Feature, dort eine Erweiterung und alles fühlt sich runder, neuer und am Ende besser an. Sehr deutlich ist das an den beiden mobilen System iOS und Android zu spüren.     
Im 12-Monats-Rhytmus erscheinen neue Versionen und bringen viele, kleine Änderungen mit sich. Dagegen gibt Apple seinem OS X nur alle zwei Jahre ein Update und auch an Windows Systemen ändert sich nach Erscheinen nur wenig am Design und Bedienung.

Wie erkennt man neue Trends? Diaspora wird bestimmt Aufsehen erregen. Aber gleich eine Revolution entfachen? Wie erschafft man neue Trends?     
Vielleicht sollte man damit beginnen, für noch unentwickelte Technik Ideen zu sammeln. Zum Beispiel wie genau man Games nur übers Internet spielt und die leistungsstarke Hardware nur noch mietet. Dafür wird auf jeden Fall schnelleres Internet benötigt und das kommt garantiert irgendwann.
Grundsätzlich hat man die Nase vorn, wenn man mit seinen Ideen auf die Technik wartet. Zumindest ist das besser, als zusammen mit allen anderen an Ideen für bestehende Technik zu arbeiten. Doch auch hier ergeben sich sicherlich interessante Perspektiven und neue Möglichkeiten, die vielleicht noch niemand ahnt. Und wer weiß schon, wie lange man warten muss?