---
layout: post
title: "The Joneses"
description: "Eine kurze Review zu diesem gelungenem und nachdenklich machenden Film."
date: 2011-08-10
tags: [Film]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-the-joneses-lg.jpg
  credit: Echo Lake Entertainment
  creditlink: http://www.thejonesesmovie.com
---

Habt ihr euch auch manchmal gefragt, wie Luxusfirmen ihre Produkte an die reiche Frau und den reichen Mann bringen? Die Wohlhabenden mögen vielleicht mehr Geld haben, aber einfach so aus Spaß kaufen sie überteuerte Produkte vielleicht doch nicht. 


Sind sie für gewöhnliche Fernsehwerbung, Plakate und Werbeseiten in Zeitungen empfänglich? Oder schielen sie eher mal zum Nachbarn, was der sich mal wieder tolles geleistet hat?

Wenn man letzteren Gedanken weiterspinnt, kommt man zur Familie Jones. Das Prinzip ist simpel. Man stellt eine wohlhabende Durchschnittsfamilie aus Schauspielern zusammen und pflanzt sie in eine reiche Vorstadtgegend. Dort machen die Familienmitglieder nichts anderes, als viele soziale Kontakte zu knüpfen und dabei ihre neuen, tollen und teuren Produkte vorzuführen. Diese haben sie natürlich nicht selbst gekauft, sondern sie vermarkten sie lediglich.

## Die perfekte Familie

Ich bin richtig begeistert von dieser Idee und sie ist auf eine verstörende Art und Weise durchaus realistisch. Sehr gut ist auch die Umsetzung dieser Idee gelungen. Der Film dokumentiert nicht nur, wie die Familie Produkte anwirbt, sondern hat stets einen hohen Spannungsbogen. Familie Jones kann sich gut in die Nachbarschaft einleben und wirkt in der Tat sehr perfekt. Doch schon nach einer Viertelstunde wird klar, dass es kein Urlaub für die *Familie* ist.     
Die Verkaufszahlen des Vaters (David Duchovny) sind wesentlich geringer als die der anderen Familienmitglieder. Die Chefin würde ihn am liebsten auswechseln, aber die Mutter (Demi Moore) will es weiter versuchen. Obwohl er sich daraufhin wesentlich verbessert, kommen schon die nächsten Probleme auf die Familie zu.

## Moral

Der Film diskutiert zum einen die Frage, wie moralisch richtig diese Art der Werbung ist und zum anderen, wie die Protagonisten, die *Familie*, damit umgehen. Obwohl alles rein geschäftlich ist, können sich trotzdem Gefühle zwischen den Personen entwickeln. Wie versteckt man seine eigene, wahre Persönlichkeit? Oder wie reagiert man, wenn ein Auffliegen der Scheinidylle droht?     
Neben den persönlichen Aspekten, wird man auch angeregt über die gesamte Idee nachdenken. Was ist noch bloße Werbung und ab wann zwingt man die Anzuwerbenden indirekt ein Produkt zu kaufen? Auch wenn diese es sich gar nicht leisten können.

Mir hat der Film großen Spaß gemacht und auch schauspielerisch bewegen sich alle vier Hauptdarsteller auf einem Niveau. Ich mag David Duchovny sowieso und auch hier spielt er seine Paraderolle des lockeren, charismatischen Mannes. Mit mehr Sex und Drogen, hätte man ihn auch für Hank Moody halten können.

Sehr empfehlenswert.