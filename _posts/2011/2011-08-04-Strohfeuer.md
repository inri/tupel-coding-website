---
layout: post
title: "Strohfeuer, Sascha Lobo"
description: "Ein kurzes Review zu Lobes Roman."
date: 2011-08-04
tags: [Literatur]
published: true
category: medien
comments: false
share: true
---

Vor einigen Monaten hat Sasha Lobo einen Roman namens *Strohfeuer* veröffentlicht. Es ist eine amüsante Geschichte über das kurze Leben einer Agentur in Zeiten der Dotnetblase am Anfang der 2000er Jahre. 

Mir hat das Lesen des Buches sehr viel Spaß gemacht, denn es liest sich einfach so weg. Anfangs ist man fasziniert von der etwas seltsam und größenwahnsinnig anmutenden Mentalität, die die Personen und der Ich-Erzähler Stefan an den Tag legen. Später wird immer mehr Blödsinn gemacht und man ahnt schon nach weniger als der Hälfte des Buches, dass die Geschichte nicht gut ausgehen kann.

In der kurzen New Economy Zeit, haben einige Menschen richtig viel Geld verdient. Die Werbeagentur von Stefan war sicherlich mittendrin und hätte zu den Gewinnern zählen können. Aber letztlich kann man nur zu dem Schluss kommen, dass sie früher oder später gescheitert wären, auch wenn die Blase zu dieser Zeit nicht geplatzt wäre.     
Die Hauptfigur tänzelt auf einem Drahtseil. Sind erste Entscheidungen vielleicht gewagt, ist das Risiko dennoch überschaubar. Wackelig wird es später, wenn der egomanische Agenturmitgründer Thorsten seine beknackten Aktionen startet und falsche Entscheidungen verlangt. Am Ende verkalkuliert sich Stefan vollends und das ärgert den Leser irgendwie schon. Ich mag Entwicklungen bei Personen, aber diese findet nur begrenzt statt.    
Scheitern kann eigentlich etwas gutes und nützliches sein. Der Erkenntnisgewinn findet im Buch zwar immer statt, aber es werden keine Folgen daraus gezogen. 

Nun wollte Sascha Lobo aber diesen Roman so aufziehen und diese Art Menschen, die nach dem schnellen Geld streben, portraitieren. Das ist ihm in jeder Hinsicht gelungen. Nicht selten schüttelt man den Kopf, grinst dabei aber über das ganze Gesicht. Herr Lobo, davon möchte ich gerne mehr lesen!