---
layout: post
title: "The Rise of the Governor"
description: "Eine Einführung in die Welt von The Walking Dead und ein kurzes Review zum ersten Roman der Reihe."
date: 2011-12-23
tags: [Literatur, The Walking Dead]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-rise-of-the-governor-lg.jpg
  credit: Thomas Dunne Books
  creditlink: http://us.macmillan.com/thomasdunne
---

Mit meinem Kindle habe ich innerhalb von einer Woche den ersten Walking Dead Roman gelesen. Es war für mich mal wieder der erste Roman seit gefühlten Ewigkeiten. Da er mir wirklich gut gefallen hat und ich sowieso mal einen Artikel über The Walking Dead schreiben wollte, wird dies hiermit getan.


Beginnen werde ich mit einer kurzen Einführung in das The Walking Dead Universum und was man generell davon zu erwarten hat. Im Anschluss gibt es die Buchrezension und meine Meinung.

## The Walking Dead

TWD erzählt die Geschichte einer Gruppe von Menschen in einer postapokalyptischen Welt voller Zombies. Die Idee entstand 2003 als Comic, wurde letztes Jahr als Serie adaptiert und feiert nun auch in der Literatur Premiere.      
Im Zentrum steht der Polizist Rick mit seiner Familie und die Gruppe der Überlebenden. Ihre Reise fängt bei Atlanta an, von wo aus sie sich durch die gefährliche Welt schlagen und eigentlich nur nach einem Ort der Sicherheit suchen. 

Wer ein actionreiches Zombieszenario wie *Dawn of the Dead* erwartet, wird vermutlich enttäuscht werden. Die Untoten sind zwar ein wichtiges Stilmittel, aber sie bilden im Prinzip *nur* den äußeren Rahmen der Handlung. Im Gegensatz zu den meisten Zombiefilmen, die *nur* den Kampf der Menschen gegen die Zombies darstellen, hinterfragt TWD jedoch, wie die Menschen mit der gegebenen Situation umgehen.      
Vor ihnen liegt eine neue Welt, unsere heutige ist Vergangenheit und auch die alten Regeln und Gesetze gelten nicht mehr in ihrer alten Form. So macht die Gruppe nicht nur bittere Erfahrung mit den Untoten, sondern auch mit anderen Menschen, Überlebende wie sie selbst.

Der TWD-Comic ist aktuell bei Ausgabe Nr.92 und erzählt bisher die Geschehnisse eines Zeitraums von gut einem Jahr. Die Serie befindet sich mitten in der zweiten Staffel und lässt sich auch sehr viel Zeit mit dem Erzählen. Das ist auch notwendig, denn den Figuren wird im Comic auch viel Zeit zur Charakterentwicklung gegeben. Eine Serienepisode deckt sich zeitlich ungefähr mit einer Ausgabe des Comics, allerdings verläuft die Geschichte nicht identisch, das wäre ja auch langweilig.     
Die grobe Handlung der Comics ist zwar der rote Faden der Serie, aber die Schicksale der Charaktere sind unterschiedlich. So stirbt der beste Freund und Kollege von Rick in einer der ersten Ausgaben des Comics, ist in der Serie aber noch am Leben und spielt auch eine ziemlich wichtige und spannende Rolle.

## The Rise of the Governor

Die Gruppe um Rick findet in den Comics nach einigen Wochen ein Gefängnis und damit auch den ersten sicheren Ort für einen längeren Zeitraum. Auf einer Erkundungsfahrt, um andere Überlebende zu finden, gelangen Rick und zwei andere der Gruppe in den kleineren Ort Woodbury. Dort haben sich einige Dutzend Menschen vor den Zombies verbarrikadiert. Ihr Anführer ist ein sadistischer Mann, den alle nur Governor nennen. 

Anfangs freundlich empfangen, geraten sie schnell in Gefahr und müssen um ihr Leben fürchten. Der perverse, kranke Charakter des Governor wird nicht nur deutlich, als er Rick unvermittelt die Hand mit einem Beil abhackt. Er hält seine eigene Tochter, die ein Zombie ist, als eine Art Haustier. Sie ist in seiner Wohnung, er füttert sie mit Leichenteilen und sogar eine erotische Beziehung wird im Comic angedeutet.

Der Governor ist abgrundtief böse und man fragt sich beim Lesen des Comic oft, was ihm zu dem gemacht hat, was er in dieser Stadt ist. Der Roman geht dieser Frage nach und beschreibt dabei die gefährliche Veränderung des Wesen eines relativ normalen Menschen.

Der Governor , Philip Blake, beginnt seine Reise zusammen mit seiner Tochter Penny, seinem Bruder Brian und seinen Freunden Nick und Bobby. Sie ziehen sich anfangs einige Tage in einer Wohnsiedlung zurück, welche interessanterweise auch Rick und die Gruppe später besuchen. Philips Ziel ist Atlanta, denn dort soll es Rettungszentren geben. Der Weg dorthin ist nicht einfach, aber Philip als treibende Kraft bringt die Gruppe voran und sicher aus Gefahren. Auch als sie in Atlanta einen Unterschlupf mit anderen Überlebenden teilen und es eigentlich ein sicherer Ort ist, breitet sich die Finsternis in Philips Seele aus.

Sicherlich war ein Hang zur Gewalt immer ein Teil von ihm. Er prügelte sich öfters als sein Bruder und geriet häufiger in Schwierigkeiten. Und in der neuen Welt voller Untoten, hat er sich schneller als alle anderen an die veränderte Situation gewöhnt. Das Töten der Zombies macht ihm nichts aus, das Kämpfen bereitet ihm sogar Spaß. Argwöhnisch beobachten ihn seine Mitreisenden und besonders sein Bruder ahnt, dass etwas mit ihm nicht stimmt.      
Erst im letzten Drittel des Romans beginnen die Geschehnisse, die die Gruppe letztlich in das Örtchen Woodbury führen.

## Review

Der Roman bleibt dem Stil des Comics und der Serie treu. Die Personen werden genau beschrieben und charakterisiert, und besonders die beiden Brüder stehen dabei im Mittelpunkt. Vom Innenleben der beiden erfährt der Leser mit Abstand am meisten. Dadurch ist auch die Verwandlung des Antihelden gut nachvollziehbar und gleichzeitig kann man die Bedeutung der anderen Mitreisenden der Gruppe in diesem Kontext einordnen.
Paradoxerweise entwickelt man als Leser für alle Charaktere Sympathie, auch wenn man weiß, dass Phillip als gnadenloser Governor enden wird.

Schön ist auch, dass seine Tochter nicht im Zentrum der teils psychologischen Betrachtung steht. Es ist klar, dass sie für ihn sehr wichtig ist und ihr Tod eine tragende Rolle für ihn haben wird. Aber unnötige Handlungsstränge werden ihr nicht gegeben.

Im Vergleich zu dem Comic und der Serie, kann ein Roman die innere Entwicklung deutlicher beschreiben. Gedanken spielen im Comic und der Serie keine oder zumindest keine große Rolle. Das gesprochene Wort und die Mimik und Gestik der Personen sind entscheidend um die Gefühlswelt zu transportieren.      
Wie man den Rahmen der Handlung am liebsten konsumiert, ist Geschmacksache. Je nach Phantasievermögen kann die textuelle Beschreibung der dröhnenden Stadt Atlanta, voll mit hunderttausenden Zombies, die sich wie ein riesiger, zäher Schwarm durch die Straßen bewegen, auch sehr anschaulich sein. Die Bilder der Comics und der Serie möchte ich aber trotzdem nicht missen.

Das ist das tolle am Walking Dead Universum: Alle drei Formate ergänzen und unterstützen sich gleichzeitig. Man bekommt drei unterschiedliche Medien, die einem die Geschichte näher bringen und sich dabei aber nicht überflüssig machen.       
Das zeigt zum einen, dass Robert Kirkman überall seine Finger im Spiel hat und zum anderen, dass an jedem einzelnen Format exzellente Köpfe beteiligt waren.

Wer TWD mag, dem sei dieser Roman sehr empfohlen.     
Abschließend möchte ich das Ende des Romans verraten. Wer sich die Spannung nicht verderben lassen möchte: Nicht weiterlesen!

### Spoiler

Ein erzählerisch-stilistisches Mittel der Comics ist die ständige Drohung: *Niemand ist sicher!* Kein Charakter hat eine Lebensversicherung, jeder kann jederzeit einem Zombie zum Opfer fallen. Das schmälert den *Happy-End*-Effekt, ist dafür aber unglaublich förderlich für die Spannung. Man fiebert die ganze Zeit mit und weiß eben nicht, was mit wem als nächstes passiert.

Als die Serie im Fernsehen ausgestrahlt wurde, bezweifelte ich, dass dieser Effekt ähnlich wirkungsvoll übertragen werden kann. Aber besonders das Halbzeitfinale der aktuellen zweiten Staffel hat bewiesen, dass die Geschichte nicht eins zu eins aus den Comics übernommen wurde und man trotzdem überraschende, tragische Wendungen in der Serie serviert bekommt.

Beim Roman hatte ich anfangs auch Zweifel. Wo und wie die Geschichte endet, ist ja aus den Comics bekannt. Trotzdem ist der Überraschungseffekt auch vorhanden und besonders das vorletzte Kapitel haut den Leser aus den Latschen und lässt ihn mit offenem Mund dasitzen!     
Phillip ist brutal, Phillip lässt seine Tochter als Zombie *weiterleben* und Phillip lässt seine Gewalt auch an seinem Bruder und seinen Mitmenschen aus. Brian ist der schwächere und irgendwie im gesamten Roman auch der feigere von beiden. Die Brüder sind trotzdem eng miteinander verbunden, auch wenn sie es selbst vielleicht nicht zugeben würden.

Am Ende fällt Phillip überraschend den Zombies zum Opfer und der Verlust seines Bruders nimmt Brian enorm mit. Sein einziger Halt, sein Schutz vor dieser grausamen Welt ist tot und er nun allein. Seine Psyche zerbricht daran und um trotzdem zu Überleben wird er zu seinem Bruder. Er schmeißt die Peiniger von Woodbury raus und übernimmt die Rolle, die auch sein Bruder übernommen hätte: Die Rolle eines Anführers.
Damit endet der Roman.      
Wie es zu den unmenschlichen Gladiatoren-Kämpfe kommt und wie Brian mehr und mehr zu einem brutalen und perversen Anführer wird, kann sich der Leser denken bzw. wird dies in den nächsten Romanen beleuchtet.

Der Governor ist das erste menschliche Monster, das von dieser Welt geschaffen wurde und gleichzeitig auch ein tragisches Opfer.
