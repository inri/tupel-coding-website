---
layout: post
title: "Amazons Kindle als Buchersatz"
description: "Ich habe mir den dritten Amazon Kindle näher angeschaut."
date: 2011-05-15
tags: [Amazon, Ebooks]
published: true
category: it
comments: false
share: true
image: 
  feature: 2011-kindle-keyboard-lg.jpg
  credit: Amazon
  creditlink: http://www.amazon.com
---

Ende April begann der offizielle Verkauf des Amazon Kindle endlich auch in Deutschland. Ich ließ es mir nicht nehmen, ein solches Gerät mal auszuprobieren. Dazu einige Worte und warum ich es sehnsuchtslos wieder zurückschickte.

Die technischen Daten kann man überall nachlesen. Das coolste ist der E-Ink-Bildschirm, der sieht tatsächlich wie echtes Papier aus und spart Strom, später mehr zu ihm. Ansonsten fällt auf, dass das Gerät angenehm leicht ist und eine richtige, kleine Tastatur hat. Ach ja: Mit nur 139€ stürzt man sich auch nicht in Unkosten, zumindest wenn in Deutschland die EBooks vernünftige Preise hätten.

Ich ahnte es zwar schon, aber man will ja doch sichergehen: Wer schon ein Tablet hat, braucht dieses Gerät nicht. Zumindest nicht so, wie es momentan zu kaufen ist. Es ist wunderbar leicht, leichter sogar als gängige Taschenbücher.
Das E-Ink sieht einfach klasse aus, auch wenn es nur Grautöne darstellen kann. Man kann noch so nah an den Bildschirm herankriechen, Pixel findet man nicht. Stromspartechnisch ist es auch gut durchdacht, denn es verbraucht nur Strom und Akku, wenn man sich eine neue Seite anzeigen lässt.

Das Problem an dem ganzen Gerät sind die Tasten! Wozu brauche ich die Tasten? Jaja, man kann auch nach Büchern suchen und wohl Notizen schreiben, aber diese Tasten sind einfach für ein tabletartiges Gerät nicht geeignet. Man möchte den Bildschirm anfassen, auf die nächste Seite wischen und nicht am unteren drittel des Kindles diese Tasten kleben haben.     
Ich verstehe übrigens auch die Menschen nicht, die sich dieses Asus Transformer mit Tastatur-Dongle zulegen. Man kann auf den Tablets schreiben, auch ohne Tastatur. Extra noch 150€ dafür zu bezahlen, ist irgendwie nicht sinnvoll. Dann kauft doch gleich ein Netbook.

Wenn Amazon also die Tasten weglässt und stattdessen den Bildschirm größer macht und dabei das Gewicht und den Preis beibehält, könnte es sogar gute Gründe geben dieses Gerät zu benutzen, obwohl man schon ein iPad hat.     
In Deutschland wird der Andrang auf den Kindle trotzdem nicht sehr groß sein. Das verdanken wir unseren tollen Buchverlagen, die Kumpels der Musikindustrie. Momentan gibt es nämlich so gut wie keinen Preisunterschied zwischen Buch und EBook.  Obwohl ersteres natürlich auch noch Produktionskosten hat. Dank der Buchpreisbindung in Deutschland und diese werden die Verlage nicht so schnell abschaffen.

Ich glaube und hoffe, dass die Schriftsteller sich von den Verlagen abwenden und beginnen ihre Werke selbstständig online zu stellen. So bekommen sie mehr Geld und sind unabhängig.
Eine schöne Vorstellung.

**Update 1:** [Hier](http://www.spreeblick.com/2011/05/12/amazon-kindle-ein-test-in-funf-kapiteln/) gibt es auch eine lesenswerte Kritik über den Kindle. Er sieht einiges ganz ähnlich wie ich, aber moniert sich meines Erachtens sehr über die enge Bindung an Amazon. Und warum er bei seinem iPad Angst vor Kratzern hat, aber beim Kindle Kratzer sogar toll findet, dass leuchtet mir leider nicht ein.

**Update 2:** Am 04.10.2011 hat Amazon einen neuen Kindle vorgestellt und damit gleichzeitig den größten Kritikpunkt beseitigt: Die Tastatur. Die gibt es beim neuen Kindle nämlich nicht mehr. Außerdem wurde der Preis auf 99€ reduziert. Sehr verlockendes Gerät, welches ich mir zu Weihnachten schenken liess.