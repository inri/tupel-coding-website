---
layout: post
title: "Google+ nach einigen Wochen"
description: "Um Google+ ist es leider etwas ruhiger geworden. Wann kommen die Massen?"
date: 2011-08-01
tags: [Google]
published: true
category: it
comments: false
share: true
---

Um Google+ ist es ein wenig ruhiger geworden. Was soll man auch ständig darüber schreiben? Nach einigen Wochen in diesem Netzwerk, habe ich mich gefragt: *Was ist Google+ für mich?*

Weniger als ich anfangs erwartet habe. Der Grossteil meiner Bekannten ist bei Facebook aktiv. Der Grossteil meiner Informationsgeber ist bei Twitter und in meinem Feed-Reader aktiv. Google+ könnte ein Spagat werden und es käme mir sehr gelegen, aber das ist noch in ferner Zukunft. Aktuell sind vor allem Kommilitonen auf Google+ aktiv und posten interessante Artikel im Stream.

Die Aktivitäten über Google+ haben für mich persönlich zwei Vorteile: Erstens kann man diskutieren. Das geht bei Twitter nicht sehr gut und im Feed-Reader gleich gar nicht. Zweitens werde ich nicht mit jedem persönlichen Furz vollgesülzt. Was aber daran liegen mag, dass Informatiker generell weniger persönliche Informationen veröffentlichen und dass meine Bekannten immer noch bei Facebook aktiv sind. Sollten sie aber irgendwann bei Google+ sein, kann ich die *Schwafelheinis* schön in den Zirkel der Verdammnis stecken.
 
Vielleicht liegt es an meinem Bekanntenkreis, vielleicht steckt Google+ trotz 20 Mio. Benutzern immer noch in den Kinderschuhen? Wenn die Entwicklung so weitergeht und Google+ zum Mainstream wird, könnte es mir sehr gefallen. Momentan ist es noch zu ruhig und ich muss immer noch Twitter und Facebook besuchen um meine Informationen zu bekommen.