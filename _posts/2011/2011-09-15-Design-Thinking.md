---
layout: post
title: "Design Thinking"
description: "Buchrezension über Design Thinking."
date: 2011-09-15
tags: [Literatur, HPI]
published: true
category: blog
comments: false
share: true
---

Für die 7. Ausgabe des HPI Magazins habe ich vor einem Jahr eine Buchrezension über das Design Thinking geschrieben. Diesen Text habe ich etwas überarbeitet und stelle ihn nun vor.

Gute Ideen und Innovationen entstehen an der HPI School of Design Thinking, wie man weiß. Doch wie genau diese Ideen entstehen, wird nun erstmals in einem Buch enthüllt.

Es ist Dienstagabend und Michael setzt sich vor seinen Computer. Er programmiert dort weiter, wo er einige Stunden zuvor im Büro aufgehört hat. Im Flur und in der Küche brennt noch das Licht und der Fernseher zeigt stumm eine Nachrichtensendung. Nach einigen Minuten piept es aus der einen Ecke des Zimmers, Michael horcht kurz auf und geht zur Kommode, wo sein iPad liegt. Er nimmt es in die Hand und liest die Meldung, die darauf erscheint. Sie informiert über den aktuellen Stromverbrauch in Michaels Haushalt. Das Gerät hat erkannt, dass der momentane Energieverbrauch höher ist, als normalerweise zu dieser Zeit.    
 
Das kann Michael aus einer Grafik ablesen und entsprechend handelt er sofort: Er schaltet die unnötige Beleuchtung und den Fernseher aus. Sofort gibt das iPad eine neue Information preis: Hält er diesen Stromverbrauch, verringert sich die CO2 Belastung seines Haushaltes stündlich um 100 kg. Neben dieser Zahl sind symbolisch einige Bäume abgebildet und die Fläche, 5 Quadratmeter. Das Gerät empfängt die Daten wireless vom Stromzähler und berechnet den Verbrauch, zuzüglich möglichen Einsparpotentialen.     
- eine Idee von Studenten des Design Thinking.

Doch wie entstehen solche Ideen? Was für ein Prozess steckt dahinter? Neben der Beantwortung genau dieser Fragen, beschreibt das Buch von Hasso Plattner, Christoph Meinel und Ulrich Weinberg, wie dieser Prozess in Potsdam an der D-School zum Einsatz kommt und welche Erfahrungen man bisher gemacht hat.     

Seit drei Jahren wird an der D-School *Erfinden* gelehrt, Zeit genug also, ein Resümee zu ziehen. Die Autoren beschreiben im ersten Kapitel wieso Innovationen für Forschung und Technik wichtig sind und warum gerade Deutschland in diesem Bereichen mehr investieren muss. In dieser Hinsicht wird von unserem Land nämlich noch kein Spitzenplatz belegt, dabei ist das kreative Entwickeln von Ideen wichtig, um den wirtschaftlichen Aufschwung anzukurbeln und die Zukunft stets neu zu erfinden.

Die D-School soll dabei eine Vorreiter-Rolle spielen und Begeisterung entfachen, so dass auch andere Standorte Deutschlands ihr Augenmerk auf das Entwickeln neuer Innovationen richten.

## Theorie der Kreativität

Wer sich mit den Techniken des Design Thinking schon einmal beschäftigt hat, wird im nächsten Abschnitt des Buches keine neuen Informationen finden. Für alle anderen bietet dieser Teil eine kompakte Zusammenfassung über den Prozess. Es wird die Grundlage eines Design Thinking Teams beschrieben und wieso dieses multidisziplinär ist und aus Studenten verschiedener, wissenschaftlicher Bereiche besteht, die besonders talentiert in ihrem Fach sind. 

Welche Charakterzüge der Teilnehmer sind wichtig und notwendig, und welche innere Einstellung sollte man mitbringen um Innovationen gemeinsam in diesen Teams und auf diese Art und Weise zu entwickeln.     
Die 6 Schritte des Design Thinking Prozesses werden auch behandelt, so wie weitere Regeln und Prinzipien, die zur Findung einer Lösung unabdinglich sind. 

Natürlich wird Design Thinking nicht nur an der D-School praktiziert. Im dritten Kapitel werden weitere Bereiche und Felder aufgezeigt, in denen man mit dieser Methode schon arbeitet und große Erfolgte erzielen konnte.      
Es seien hier beispielhaft das Innovationsberatungunternhemen [IDEO](http://www.ideo.com/) und das Unternehmen Procter&amp;Gamble genannt, dessen Marken wie Always, Meister Proper und blend-a-med weltweit bekannt sind.

Das Kapitel widmet sich aber auch dem zukünftigen Aspekt. Bisher weiß man noch nicht genau, wieso gerade das Konzept des Design Thinking so gut funktioniert. An der wissenschaftlichen Erforschung dessen, arbeitet man im *Design Thinking Research Program*, welches vor einigen Monaten die ersten Ergebnisse im Buch *Design Thinking Understand - Improve - Apply* veröffentlicht hat. 

## Erfolgreich in der Praxis

Die ersten Studenten haben ihre einjährige Ausbildung im Design Thinking abgeschlossen und dabei interessante Innovationen entwickelt. Der Abschluss des Buches zählt einige dieser Projekte auf und beschreibt die Entwicklung der Ideen im Prozess des Design Thinking.     

Eines davon, das Veranschaulichen des Energieverbrauchs durch ein iPad, wurde oben bereits beschrieben. Andere Projekte beschäftigen sich damit, wie man den privaten Einkauf verändern oder das Schreiben von Fernsehserien optimieren kann. Relativ aktuell ist zum Beispiel ein Projekt zur effizienteren Gestaltung des Check-In Prozesses an Flughäfen.

Design Thinking spielt in den verschiedensten Bereichen unseres Lebens eine Rolle, auch wenn einem das nicht sofort auffallen mag. Nach den größtenteils theoretischen Teilen des Buches, gibt dieses Kapitel eine gute Zusammenfassung darüber, wo Design Thinking schon erfolgreich angewendet wird.

## Fazit

Der Leser erhält eine kompakte Zusammenfassung über das Wesen und den Prozess des Design Thinking. Man erfährt in spannenden Anekdoten viel über den bisherigen Werdegang und die Fortschritte, die hier in Potsdam in den letzten Jahren erzielt wurden. Eine äußerst gelungene Bestandsaufnahme.