---
layout: post
title: "House of D"
description: "Ein Review über einen warmherzigen Film von und mit David Duchovny."
date: 2011-08-20
tags: [Film]
published: true
category: medien
comments: false
share: true
---

Letztens habe ich mir einige Filme mit David Duchovny angeschaut und bin dabei auf House of D gestoßen. Leider ist Davids Auftritt nicht viel länger als eine Folge Californication, aber das trübt das Sehvergnügen nicht. 


Die Geschichte hat er nämlich selbst geschrieben. Der Film erzählt die tragischen Begebenheiten von Toms Jugend und wie er als Amerikaner letztlich in Paris gelandet ist.

Am 13. Geburtstag seines Sohnes erzählt er ihm (und einem gesamten Innenhof), was er selbst kurz vor und nach seinem 13. Geburtstag erlebt hat. In New York besuchte er eine katholische Schule und sparte zusammen mit einem debilen Freund (Robin Williams) auf ein grünes Fahrrad. Sein Vater war schon lange tot und seine Mutter kämpfte mit Drogenproblemen. Zum  Reden fand er eine inhaftierte Frau in einer Besserungsanstalt, mit der er regelmäßig Gespräch führte, brüllend von der Straße zum 5. Stock. 

House of D hat sehr viel Herz. Der Jugendliche, der öfter mal in verschiedene Konflikte gerät, ist zwar ein Klischee in dramatischen Filme, jedoch hier sehr abwechslungsreich erzählt. Erstaunlich auch die Konsequenzen, die der Junge zieht und ihn, der Anfang des Filmes verrät es ja schon, letztlich nach Paris führen. Ebenfalls emotional ist der Besuch in der Heimat, bei der Duchovnys Schauspiel eigentlich erst beginnt und leider viel zu kurz währt.

Wer  Lust auf einen ruhigen Film mit guter Geschichte hat, dem sei House of D empfohlen.