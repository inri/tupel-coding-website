---
layout: post
title: "Ein neuer Begleiter: iPhone"
date: 2011-10-23
tags: [iPhone]
published: true
category: it
comments: false
share: true
---

Seit über einem halben Jahr ärgere ich mich mal mehr und mal weniger über mein Handy. Da gibt es die guten Seiten wie zum Beispiel der Formfaktor, der gute Bildschirm und die vernünftige Akkulaufzeit.     
Leider stechen aber auch die weniger guten Seiten immer mal wieder hervor. 

## Tschüss Android

Hier wäre der Untertitel *Android - Segen und Fluch* sehr passend. Was mich öfters fluchen lässt, habe ich schon einige Male beschrieben. Das hängt weniger mit der Plattform und dem Betriebssystem an sich zusammen, sondern viel mehr mit dessen UpdateP-Politik.

Zähneknirschend bekam mein Desire im August noch ein Update auf Android 2.3, aber mit eingeschränkten Funktionen und eigentlich nur für Entwickler empfohlen. Von offizieller Seite wird es wahrscheinlich kein Update auf Android 4 geben.

Mein Desire ist also offiziell veraltet. Zwei Jahre hat es durchgehalten. Es ist langsam Zeit geworden, nach Alternativen Ausschau zu halten. Ich war sehr gespannt auf das neue Nexus, denn dieses Smartphone bekommt garantiert länger als zwei Jahre lang frische Android Updates. Gleichzeitig war ich aber auch auf das neue iPhone gespannt.

## Willkommen iPhone

Nun, das iPhone stellte sich zuerst vor. Meine erste Meinung habe ich ja schon kundgetan. Ich bin vor Überraschung zwar nicht vom Hocker gefallen, aber das hatte ich auch gar nicht vor. Ich spekulierte darauf, dass es sich technisch verbessern würde und einige Macken des Vorgängers beseitigt. Das tat es auch und mit Siri wurde sogar Star Trek in Bereich des möglichen gerückt.

Kurz überlegen.     
Kontostand kontrollieren.     
Noch mal überlegen.       
Apple Store öffnen.      
Noch ein weiteres Mal überlegen.      
iPhone 4S in Warenkorb.      
Kaufen.     
Bestätigen.      
Länger überlegen.     
Vorfreuen!

Seit einer Woche ist das neue iPhone nun schon mein Begleiter. Zwischendurch atmete ich auf, weil das neue Nexus in meinen Augen enttäuschend ist und ich es mir niemals gekauft hätte.     Das iPhone dagegen begeistert mich noch immer sehr.
Es fängt damit an, dass es sich unglaublich wertig anfühlt. Die Verarbeitung ist absolut Spitzenklasse. Nun kann man diskutieren, ob eine leicht gummierte Plastikrückfläche ein wenig rutschfester ist, aber zum einen ist das Glas in dieser Hinsicht auch völlig okay und zum anderen fühlt es sich einfach anders und besser an. 

Das neue Nexus wird bestimmt damit werben, dass es ja so leicht sei und das ist es für seine Größe auch tatsächlich. Aber das Gewicht erreicht Samsung durch die Verwendung von sehr viel Kunststoff am Gehäuse und auch wenn dieses nicht mehr knirscht, so ist es doch qualitativ eine völlig andere Welt zu dem Glasbody.

## Erste Eindruck

Die zweite Begeisterung kam zusammen mit den ersten Bildern auf dem Bildschirm des iPhone. Es ist gestochen scharf und kann unglaublich hell sein. Wenn man die Helligkeit auf Automatik lässt, ist das auch noch mehr als ausreichend. Die zwei Vergleichsfotos zeigen unfairerweise die unterschiedliche Farbintensität und Helligkeit. Unfair, weil das Motiv zwar fast das gleiche ist, aber jeweils mit der Smartphone-internen Kamera geschossen wurde. Ich werde mal identische Fotos auf den Bildschirmen vergleichen.

Zusammen mit den Bildern wären wir auch schon bei der dritten Begeisterung: Die Kamera. Es war ja schon bekannt, dass die Kamera des iPhone 4 ausgesprochen gute Bilder schoss. Die Kamera des neuen 4S ist nicht schlechter und ich muss sagen, mir macht das fotografieren unglaublich Spaß. 

Das neue iOS kenne ich ja schon vom iPad. Es fühlt sich auf dem iPhone ein wenig anders an, aber auf keinen Fall schlechter. Die Bedienung geht enorm einfach von der Hand, was natürlich an der starken Leistung des iPhones liegt, aber auch an dem guten System. Näheres werde ich später ausführen.    
Die vierte große Begeisterung hatte ich mit dem Sprachassistenten Siri. Noch ist es nicht Star Trek, aber es zeigt auf jeden Fall schon mal in welche Richtung es in Zukunft geht. Ihr Verständnis ist wirklich gut. Später kommt sicherlich die Verbindung zu mehr Apps hinzu und dann wird es so richtig spaßig. Aber Siri den Wecker stellen zu lassen, geht jetzt schon schneller als selber zu tippen.

## Kleine Schatten

Es ist nicht alles perfekt am iPhone. Das wäre ja auch schlimm. Nicht überragend positiv ist mir die Akkulaufzeit aufgefallen. Beispielsweise war ich mit meiner Freundin Shoppen und habe knapp vier Stunden lang Bilder gemacht und mit dem iPhone rumgespielt. Dabei näherte sich der Akkustand schon den 50%. An einem normalen Tag in der Uni mit wenig rumspielen, kommt er auch in diesen Bereich.     
Ich würde sagen, das Desire war in dieser Hinsicht ausdauernder. Es könnte aber auch sein, dass ich doch noch oft mit dem iPhone rumspiele und man das natürlich nicht direkt mit meiner Benutzung des Desire vergleichen kann. Das wird sich später genauer zeigen.

Was fällt mir sonst noch ein? Ich hoffe dass Apple in naher Zukunft auch Widgets implementieren wird. Das fehlt dem iOS nämlich. Ansonsten macht das iPhone einen exzellenten Eindruck auf mich. Ich bin gespannt wie es sich in den nächsten Monaten anfühlt.