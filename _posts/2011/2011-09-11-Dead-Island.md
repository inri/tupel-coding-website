---
layout: post
title: "Dead Island"
description: "Ein Action-Rollenspiel mit Zombies und auf einer Südseeinsel!"
date: 2011-09-11
tags: [Game]
published: true
category: medien
comments: false
share: true
image: 
  feature: 2011-dead-island-lg.jpg
  credit: Deep Silver
  creditlink: http://deadisland2.deepsilver.com/access/
---

Der Plot ist schnell erklärt. Man ist Protagonist auf einer idyllischen Südseeinsel, bzw. der gigantischen Hotelanlage darauf und wird Zeuge wie eine Zombie-Epidemie ausbricht. Die Hauptaufgabe lautet: Überleben!    


## Inhalt
 
Nach einem kurzem Prolog im Hotel, rauscht man mit dem Fahrstuhl in den Keller und landet am Strand. Von dort beginnt man sich über riesige Areale zum Hotel durchzuschlagen. *Schlagen* ist genau das treffende Wort, denn anfangs wehrt man sich vorerst nur mit Rohren und Holzpaddel gegen die Untoten. Mit dem, was eben am Strand so rumliegt.

## Spielweise

Dead Island ist ein Shooter und Rollenspiel. Aus der Ego-Perspektive kämpft man gegen Zombies und sammelt dabei Erfahrungspunkte. Diese bekommt man ebenfalls, wenn man die Aufgaben (Quests) der Mitstreiter erledigt. Mal sucht man nach bestimmten Überlebenden (oder auch nicht), oder nach speziellen Gegenständen, usw. - kennt man ja.     
Die Erfahrungspunkte erhöhen das Spieler-Level und so kann man nach und nach neue Fähigkeiten erlernen und bessere Waffen einsetzen. Und sich bauen! Denn neben Waffen sammelt man auch Gegenstände wie Nägel, Klebeband oder Batterien. Zusammen mit Anleitungen für selbstgebaute Waffen, schustert man sich an Werkbänken abgefahrene Waffen zusammen (Bsp. Baseballschläger mit Nägeln).    

Die Waffen haben eine relativ kurze Lebensdauer. Je nachdem kann es sein, dass man nach 10 bis 15 getöteten Untoten die Waffe wechseln muss. An der Werkbank kann man sie gegen Geld wieder reparieren oder gleich upgraden (erhöht Angriff, etc.). 
Wie man selbst mit jedem Level besser und kräftiger wird, haben auch die Zombies verschiedene Level und Fähigkeiten. Ab und an kommen auch ganz fiese Gesellen.

<figure>
    <a href="/images/2011-dead_island-1600.jpg">
	   <img 
		  src="/images/2011-dead_island-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Zombies!!</figcaption>
</figure>


## Grafik &amp; Zombies

Ich habe schon lange keine neueren 3D-Spiele gespielt, aber mir gefällt die virtuelle Umgebung von Dead Island sehr. Man hat in der Tat das Gefühl, man befindet sich auf einer herrlich schönen Insel und macht Urlaub, wenn da die Untoten nicht wären.      
Aber auch die Zombies sehen erschreckend echt aus. Vergleichend fällt mir nur Left4Dead ein, doch selbst da ist die Detailtiefe nicht so hoch wie hier. Aber auch die Lebenden sehen schick und realistisch aus.     
Da man die Zombies ordentlich zu Matsch und Brei schlagen kann, disqualifiziert sich Dead Island nicht nur für Kinder, sondern sogar für ganz Deutschland. Hier erscheint das Spiel nämlich nicht offiziell bzw. nur indiziert.

## Meinung

Ich spiele erst seit einigen Stunden, habe dabei aber enorm viel Spaß. Dead Island möchte kein rasanter Shooter wie Left4Dead sein. Ich sehe es daher eher als realistisches Überlebensspiel. So würde sich die Comic-Reihe The Walking Dead vermutlich als Spiel anfühlen.     
Da kommen die Zombies nicht zu dutzenden von irgendwo angerannt, sondern lungern rum, fressen andere Leichen und schlurfen vielleicht los, wenn man sich ihnen nähert. Man hat auch in der Realität nicht die härtesten Waffen auf der Straße liegen, sondern schnappt sich, was man kriegen kann. Nehmt euch mal ein Metallrohr und prügelt einige Male auf einen großen Baumstamm ein - so ein Rohr hält nicht ewig und Holzpaddel erst recht.

Wer Zombies und ein stimmiges Ambiente mag, dem sei Dead Island wärmstens empfohlen. Aber bitte erst, wenn ihr 16 oder 18 Jahre alt seid.