---
layout: post
title: "Analyse zu Android"
description: "Eine Analyse zu Smartphones und wieso ich am Ende lieber ein i-Gerät statt einem Android wählen würde."
date: 2011-03-18
tags: [Android, Technologie]
published: true
category: it
comments: false
share: true
---

**oder: Wieso ich lieber ein iPad und iPhone haben möchte.**

## Innovationen

In den letzten 10 Jahren waren Smartphones eine lukrative Einnahmequelle. Im Jahrestakt erschienen neue Flaggschiffe mit immer besserer Hardwareausstattung und zwischen diesen Superphones warfen die Hersteller unzählige Low- und Mittelklassenmobiltelefone auf den Markt, welche technisch meist wie die Flaggschiffvorgänger ausgestattet waren. Die Auswahl war stets unglaublich groß und viele namenhafte Hersteller versuchten sich im Telefongeschäft.     

Der Grund um wieder mal ein neueres Smartphone zu kaufen, war stets eine Verbesserung der Hardware und das Ablaufen des 2-Jahres-Vertrages. Die Reihenfolge der Innovationen war ungefähr: Graustufen-Display &gt; Farbdisplay &gt; Kamera &gt; (mobiles Internet) &gt; Touchscreen &gt; Computerleistung. Aber wo stehen wir nun?

Technisch habe ich das Gefühl, dass seit ca. einem Jahr eine Stagnation eingetreten ist. Alle Smartphones haben tolle Farb- und Touchdisplays, mindestens eine Kamera, sind nicht klobig und reagieren dank der starken Prozessoren schnell. Angekündigt sind zur Zeit größtenteils noch bessere Prozessoren, noch schönere Displays und vielleicht ein noch schlankeres Design. Und 3D natürlich, was aber sicherlich noch eine ganze Weile dauern dürfte.

Ich war immer interessiert an den neuen Flaggschiffen und was die kleinen Geräte nun wieder tolles zaubern können. Aber momentan muss ich gähnen, wenn ich die Ankündigungen und Spekulationen lese. HTC mit neuem 2-Prozessorkern-Smartphone! Na und? Für einen normalen Anwender ist dieser Geschwindigkeitzuwachs kaum spürbar.

Die letzte große Innovation spielt sich nicht mehr auf der Hardwareebene ab, sondern nennt sich App - Programme für das Smartphone. Im Zuge der Apps kamen auch neue Betriebssysteme für Smartphones auf dem Markt und diese Systeme sind mindestens genauso entscheidend wie die Apps selber. Es ist hochinteressant wie hier ein Paradigmenwechsel stattfand. Man entscheidet sich nun nicht mehr nach der technischen Ausstattung, sondern auch nach dem Betriebssystem des Smartphones. Worauf ich eigentlich mit diesem Eintrag hinaus will: Mir dünkt es, als ob nicht alle Hersteller diesen Wechsel begriffen haben.

## Konkurrenten

Was die Systeme angeht, haben wir ein Kopf-an-Kopf-Rennen zwischen Apple (iOS) und Google (Android), und im hinteren Bereich findet ein Rangeln zwischen Microsoft (Windows Phone 7) und Blackberry statt. Von der Art&amp;Weise sind sich Apple und Blackberry nahe, wogegen Android und Windows Phone 7 zusammen eine andere Schiene fahren. Bei ersteren gibt es das System nur mit der Hardware zusammen, bei letzteren haben verschiedene Hersteller die Systeme auf ihren Smartphones. Soweit ist das ja nichts neues.

Mir geht es hier primär um Android. Windows Phone 7 ist vielleicht nett, aber kam mindestens ein Jahr zu spät. Hier spielt der oben genannte Fakt eine Rolle, das System ist mindestens so wichtig wie die Apps. Nun kann das System schick und schnell sein, aber wenn es nur wenige Apps gibt, was soll man damit? Apps müssen sich entwickeln und bis es für Windows Phone 7 viele und gute Apps gibt, werden noch einige Monate verstreichen. Bis dahin sind iOS und Android viel weiter. Daneben bietet Microsoft nichts interessantes an, wie zum Beispiel Apple mit seiner Hardware und dem damit verbundenen Design.

Blackberry ist so eine Sache für sich. Die Marktanteile werden geringer, aber ähnlich wie bei Microsoft mit Windows Phone 7, wird es immer jemand geben, der dieses System nutzt. Und nicht zuletzt haben die ihre Blackberry-Phones, die man eben nur bei Blackberry bekommt. Apple mit seinem iOS werde ich als Vergleich heranziehen, denn in Sachen Apps ist es einfach die Referenz.

Der AppStore von Apple hatte nur einige Monate Vorlauf, aber Android ist die ersten 18 Monate nicht so richtig in Fahrt gekommen. Bei Apple war die Entwicklung meistens relativ konstant und nachdem die dritte iPhone Generation erschien (2009), gab es sogar einen Anstieg. Beim iPad wurden sicherlich viele Apps nur angepasst, dennoch ist die Entwicklung dort sehr beachtlich.     
Die Anzahl der Apps für Android begann erst Mitte 2010 zu explodieren (18 Monate nach offizieller Veröffentlichung), aber zur Zeit ist sie auch die am stärksten wachsende Plattform. 

Dies sind die großen Konkurrenten auf Softwarebasis, aber in Hinblick auf die Hardware, ergeben sich neben Apple und Blackberry (und unter Android und WP7) weitere Hardwarehersteller (HTC, Samsung, Nokia, LG, Motorola, Sony Ericsson,...). Und nun wird es kritisch.

## Das Problem des Gewinnstrebens

Mit diesem Problem meine ich die Tatsache, dass jede Firma einen Gewinn einfahren möchte und dieser soll möglichst hoch sein. Wie oben beschrieben, gab es in den vergangenen Jahren von vielen Herstellern viele Smartphones und damit fuhren die Hersteller auch ganz gut. Interessanterweise fuhren auch wir Konsumenten ganz gut damit. Man entwickelte tolle Telefone und diese wurden gekauft. Hierbei wurde auf Masse gesetzt: mindestens ein Flaggschiff pro Jahr und dazwischen viele mittelmäßige Telefone.     
Das Entwickeln eines Telefons bestand aus innovativer Hardware (siehe oben) und dem System des Smartphones. Bei letzteren braute jeder Hersteller sein eigenes Süppchen, sicherlich gab es Unterschiede in den einzelnen Systemen, aber sie waren niemals gravierend.
Bis Apple sein iPhone auf dem Markt warf! Aber kam das überraschend? Ich würde sagen: nein.      

Seit die Displays der MP3-Player und Smartphones größer wurden und die Leistung zunahm, experimentierten die Hersteller mit weiteren Funktionen (neben Musik und Telefonie). Apple erkannte als erstes das Potential dieser mobilen Geräte. Sie standen an der gleichen Stelle wie normale Computer noch vor einigen Jahrzehnten auch. Die Systeme waren vorhanden, aber irgendwie öde. Erst als neben den Entwicklern der Systeme auch andere begannen Programme zu schreiben, kam leben in die Sache und der Computer entwickelte sich zum Massengerät.       
Egal ob ein Creative-MP3-Player oder ein Nokia-Telefon, die Systeme waren nicht offen für Programmierer bzw. wenn doch, dann waren sie umständlich und nicht massentauglich. Die Programme und Funktionen waren deshalb auch eher mittelmäßig und wurden nicht benutzt. 

Apple fokussierte alles auf einheitliche Geräte, die Geburt des iPod Touch, und gab den Entwicklern auch das Werkzeug für das Entwickeln von Anwendungen (XCode). Mit dem iPhone, eben ein richtiges Telefon, verschafften sie sich die Aufmerksamkeit einer breiten Masse von Menschen und es stellte sich heraus, dass die Menschen trotz des hohen Preises und technisch sicherlich nicht führenden Gerätes irgendetwas toll fanden: die **Apps**!

Google ahnte diese Möglichkeiten wesentlich eher als Microsoft und kaufte schon 2005 das kleine Android Unternehmen. Einige Jahre tüftelte man und Ende 2008 sah das neue Betriebssystem das Licht der Welt. Die zentrale Idee sind in Android auch die Apps und mit Java als Programmiersprache ging man einen guten Weg. Der Unterschied zu Apple ist Googles fehlendes Wissen im Hardwarebereich und deshalb musste das neue System anderen Hardwareherstellern schmackhaft gemacht werden. Heraus kam die Open Handset Alliance und für die Hersteller ein kostenloses System für ihre Smartphones.      
Als diese beiden neuartigen Systeme sich etablierten, endete auch die letzte große technische Innovation im Smartphone-Bereich, die enorme Leistungsstärke. Das kam auch den Apps zugute und eine Goldgrube tat sich auf.

Microsoft verschlief die ganze Geschichte und brachte erst Ende letzten Jahres ein eigenes System heraus. Schon das Windows Mobile 6.5 hatte Mitte 2009 keine Daseinsberechtigung mehr auf Smartphones. Ich kaufte Ende des besagten Jahres ein HTC HD2 und es machte mich einfach nur traurig, wie viel Potential durch die fehlenden Apps und generell durch das System verschwendet wurde.      
Ein Neuanfang war natürlich die einzige Möglichkeit, wenn man in diesem Marktsegment bleiben wollte. Aber wieso wollte das Microsoft? Einfach nur um Konkurrent zu sein? Wieso konnten sie sich nicht auf Windows 7 beschränken und den Smartphone-Markt ruhen lassen? Oder mit Google eine Allianz eingehen. Das wäre mal was gewesen! Aber mit diesem WP7 machen sie sich lächerlich.

Wo ist denn dieses Problem mit dem Gewinn? Wie oben schon erwähnt, sehen Smartphones heute technisch nahezu gleich aus und die große Innovation lässt auf sich warten. Dazu kommt die Tatsache, dass auch die Systeme gleich aussehen, überall ist Android drauf. Damit die Nutzer zumindest noch etwas persönliches vom Hersteller bekommen, wursten diese ihre eigenen Oberflächen in die Systeme und auch die Mobilfunkanbieter pfuschen mit.     
Dazu kommt das Masse-statt-Klasse-Denken und wir haben wieder einen Smartphone-Markt mit unzähligen Telefonen. Warum das schlecht ist? Dadurch bekommen wir Fragmentierung. Android wird, wie die anderen Systeme auch, ständig weiterentwickelt und erhält Updates. Aber weil die Hersteller ihren persönlichen Touch ins Android bringen wollen, verzögern sich die Updates oder bleiben teilweise ganz aus. 

Man freute sich in der Presse, dass Android 2.2 schon zu 60% &quot;angekommen&quot; ist. Da muss man sich doch fragen, ob die nicht total blöde sind?! Ein Update bringt Sicherheit, ein besseres System und dadurch bessere Apps. Hänge ich hinterher, funktionieren meine Apps irgendwann nicht mehr oder mein Smartphone wird schlicht und ergreifend unsicher. Das ist ein Zustand, den man nicht akzeptieren kann.     
Aber die Hersteller bringen mit Absicht Smartphones heraus, die technisch gar nicht oder nur kaum, das neueste System nutzen können. Das untere Preissegment. Wobei das nicht die Wahrheit ist, auch relativ neue Smartphones, ehemalige Flaggschiffe, werden nach einem Jahr kaum noch beachtet.

Apples neues iOS-System (4.3) läuft immer noch auf dem iPhone 3GS und iPod touch der 3. Generation, also Geräte, die vor 21 bzw. 18 Monaten auf den Markt gekommen sind. Bitte zeigt mir ein so altes Android Smartphone mit dem neuesten bzw. fast neusten System (2.3), welches auch offiziell vom Hersteller herausgegeben wurde. Bei vielen Smartphones programmieren Hobby-Entwickler die neuste Android Version auf das Smartphone, aber wirklich rund laufen diese Systeme selten, denn nur der Hersteller kennt die genauen Hardwarespezifikationen.      

Aber die Hersteller haben gar kein Interesse daran, dass jemand ein Smartphone länger als 12 Monate benutzt. Das führt zu der Situation, dass ich mich einfach von denen beschissen fühle. Das allein wäre ja nicht tragisch, aber durch ihre extrem gewinnorientierte Haltung, machen sie das ganze System um Android kaputt.

## Die Auswirkungen

Zum einen fühlen sich Kunden nicht gut behandelt, wenn sie schon nach einem Jahr nicht mehr die aktuelle Software bekommen. Gerade in der heutigen Zeit, in der viele Menschen das mobile Internet nutzen, möchte ich auch, dass mein mobiles Gerät sicher ist. Den Computer schützt man ja auch mit Firewalls und Software-Updates. Wenn diese auf dem Smartphone ausbleiben, war es das einfach.     
Und selbst wenn sich die Updates nur verzögern, schadet das dem ganzen Wettbewerb bzw. in erster Linie Android. Bei Apple bekomme ich die Updates sofort und nicht erst in zwei Monaten oder noch später.     

Die Fragmentierung und generell die große Anzahl an Smartphones, macht es für Entwickler immer schwieriger Apps zu programmieren, die ein breites Gerätespektrum abdecken. Irgendwann muss man Abstriche machen und diejenigen, die das betrifft, sind dann enttäuscht - und schauen sich nach Alternativen um. 

## Aus den Smartphones erwuchsen Tablets

Keine Frage, dass sie irgendwann kommen mussten war klar, aber dass es nur einen Hersteller geben würde, der dieses Konzept passend umsetzt und seit einem Jahr alleine auf weiter Flur steht, ist erstaunlich. Dabei ist das Prinzip nicht unbedingt überraschend. In den meisten Tablets steckt die gleiche Hardware, wie sie auch in Smartphones zu finden ist. Lediglich der Bildschirm ist größer. Eine Übertragung des Smartphone-Betriebssystems liegt also nahe, zumindest näher als die Übertragung eines reinen Desktopbetriebssystem wie Windows 7, Linux oder Mac OS X. 

Apple hatte schon bei iOS (2007) knapp ein Jahr Vorsprung zu Google mit Android (2008), aber das sich dieser Vorsprung nicht verkleinert hat, verwundert doch. Im Unterschied zum Smartphone-Markt, muss Apple sich bei den Tablets nicht erst etablieren, es hat die Referenzen vorgegeben. Mühsam entwickelten die Hersteller nun erste Tablets und warteten auf Googles Tablet-Android (Honeycomb). Das ist vor knapp einem Monat erschienen und endlich können langsam erste Release-Termine angekündigt und Preisübersichten veröffentlicht werden. Doch diese tun einfach nur weh.

Abgesehen davon, dass alle Tablets (außer dem iPad) erst in den nächsten drei Monaten zu kaufen sein werden, hauen die Preise mich richtig um. In der unteren iPad-Preisklasse (&lt;= 500€) tummeln sich nur 7-Zoll-Tablets. Bei allem darüber schafft es kein Hersteller den Preis zu unterbieten. Womit wird das gerechtfertigt? Technisch liegen keine großen Welten zwischen den Tablets.

Vor allem in Anbetracht der Tatsache, dass Googles Honeycomb noch so neu, &quot;ungetestet&quot; und mit wenigen Apps bestückt ist, werden im nächsten halben Jahr die Android-Tablets langweilig bleiben. Und wenn die Hersteller nicht aus dem Knick kommen und die Updates nicht schneller auf die Tablets bringen (wie es bei Smartphones der Fall ist) verzögert sich der ganze Prozess noch mehr.
Und mal ehrlich, warum soll ich ein halbes Jahr auf ein Tablet warten, mit dem ich dann nicht ansatzweise so viel machen kann, wie jetzt schon mit einem iPad, welches sogar weniger kostet?

## Fazit

Vor einem Jahr war ich Feuer &amp; Flamme für Android. Das iPad habe ich misstrauisch beäugt und mir erst ein halbes Jahr nach dem Erscheinen mal eines aus der Nähe angeschaut. Ich war begeistert, aber technisch hatte es mich noch nicht überzeugt. Nun ist ein technisch sehr gutes iPad erschienen, das iOS wurde verbessert und die App-Auswahl ist gestiegen. Aber meine Hoffnungen auf einige Konkurrenzprodukte wurde herbe enttäuscht.

Schlimmer noch, es hat sich ein beängstigendes Muster in der Updatepolitik der Hersteller gezeigt. Wer garantiert mir, dass ich in einem Jahr noch das aktuellste Tablet-Android auf meinem Tablet geupdatet bekomme, und nicht längst zwei neuere Updates erschienen sind, die ich nicht bekomme, weil mein Hersteller ein neues Tablet herausgebracht hat und sich nun einen Scheiß darum kümmert, ob ich mit meinem alten Tablet neue Apps, Features und Sicherheiten nutzen möchte? Vor allem da ich vermute, dass erst in einem halben Jahr der tolle Tegra-2-Prozessor ansatzweise von Androidsystemen genutzt wird, wiegt diese Befürchtung umso schwerer.

Bei Apple weiß ich, dass ich auch noch nächstes Jahr die neuesten Updates bekomme, wie es bei normalen Computersystemen immer der Fall ist. Die Smartphone Hersteller sind für meinen Geschmack zu sehr darauf fixiert, möglichst viele Telefone an den Mann zu bringen. Sie haben ihre Einstellung von vor 10 Jahren nicht verändert, dabei aber verpasst, dass sich der Markt geändert hat.    
Früher konnte ich den Hersteller wechseln, hatte danach aber ein technisch ähnliches Smartphone mit einer ähnlichen Software und das zusammen war nach einem Jahr wieder veraltet. Es war überall doof. Heute kann ich aktuelle Software benutzen und das auch mit einem &quot;alten&quot; Smartphone. Ich habe nun eine reale Wahl.     
Noch dazu hat Apple den Herstellern gezeigt, dass man mit nur einem Smartphone-Modell pro Jahr, trotzdem unglaublich große Mengen verkaufen kann.

Wenn die Hersteller bei Tablets den gleichen Fehler machen und die Software behindern, wird das noch fatalere Folgen haben. Schließlich ist die Software auf einem Tablet noch wichtiger.

## Idee

Ich habe auch eine Idee, wie sie ihren persönlichen Touch behalten können. Warum nicht Oberfläche und herstellereigene Apps im Market anbieten? Auf den Telefonen vielleicht vorinstallieren (wenn genug Zeit ist) und ansonsten für Besitzer des jeweiligen Smartphone-Modells die Apps im Market gratis anbieten und für andere Smartphone-Modelle einen Obolus verlangen. So schnuppern auch andere mal in die Apps und man verdient nebenbei noch was. Außerdem werden die Updates nicht mehr behindert, sie müssen nur portiert werden. Den persönlichen Touch kann sich jeder alleine abholen.

Natürlich kann man jetzt eine Grundsatzdiskussion führen, welche Technik für den Benutzer relevant ist oder nicht, und ob vielleicht ein Smartphone von 2006 nicht alle Bedürfnisse befriedigt hätte und wir seit Jahren nur noch übertriebenen Quatsch kaufen. In der Tat glaube ich aber, dass alle Innovationen den Umgang mit Smartphones für den Benutzer angenehmer und leichter gestaltet haben.

Farbe ist besser als Grautöne, Kamera um unterwegs Schnappschüsse zu machen ist praktisch, mobiles Internet ist einfach nur toll, Touchscreen ermöglicht größere Displays und bessere Interaktion mit dem System, bessere Computerprozessoren verkürzen das Warten enorm und dienen auch der Interaktion, und Design ist eben das, was man sieht und fühlen kann.      
Eine richtige Innovation lässt aber momentan auf sich warten.