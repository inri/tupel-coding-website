---
layout: post
title: "Bachelorprojekt: tele-Task for School"
description: "Eine kurze Vorstellung meines Bachelorprojekts."
date: 2011-11-09
tags: [HPI, Studium]
published: true
category: it
comments: false
share: true
---

Bevor ich das Projekt vorstelle, einige Worte zu [tele-Task](http://www.tele-task.de/). tele-Task ist ein Portal, auf welchem einige unserer Vorlesungen online gestellt werden. Zum Umfang des ganzen gehört auch das Aufnehmen der Vorlesungen. Dabei wird der Dozent und seine Folien, die er gerade zeigt, aufgenommen und später in einem Video verbunden.      


So speichert man zum einen das Wissen und zum anderen ist es ganz praktisch, wenn man mal nicht in der Uni ist oder so. Passiert ja mal.     
Es werden natürlich nicht alle Vorlesungen aufgenommen, denn es klappt auch nicht für alle Vorlesungen gleich gut. In Mathe wird viel an der Tafel geschrieben, da geht es zum Beispiel nicht. 

Unser Bachelorprojekt beschäftigt sich mit der generellen Frage, wie man tele-Task in die Schule bringen kann. Wir mögen allerdings den Untertitel *Wie kann man den Schülern das Lernen mit Hilfe von neuen Medien erleichtern?* lieber, denn nach einigen Gesprächen mit Lehrern war schnell klar: Kein Lehrer lässt seinen Unterricht filmen. Das ist auch nicht sinnvoll.     
Bis Ende diesen Jahres arbeiten wir an Ideen und Konzepten, wie man dieses Ziel erreichen kann und führen dabei Gespräche mit Schulen, Lehrern und Schüler.

Jedes Bachelorprojekt hat einen externen Partner. Diese Partnerschaft ist vorgesehen um dem Anspruch des praktischen Studium gerecht zu werden. Der Partner möchte meist ein genaues Produkt, aber in unserem Fall gibt es **DIE** eine optimale Lösung nicht. Wir müssen sie erst noch finden. Ehrlich gesagt war das auch einer der Gründe, warum ich mich für dieses Projekt entschieden habe.      

Unser initialer Partner ist eine Oberschule in Berlin. Mit ihr hatten wir auch schon zwei Treffen und werden bei dem nächsten schon einen groben Prototyp vorstellen. Trotzdem arbeiten wir auch mit anderen Schulen zusammen und wollen natürlich möglichst viele Meinungen bekommen. Jede Schule hat andere Ideen und Präferenzen, und aus dieser Menge von Input finden wir hoffentlich die besten Teile.

Wie genau unsere momentanen Ideen aussehen und in welche Richtung wir uns bewegen, werde ich zu einem späteren Zeitpunkt genauer erläutern.