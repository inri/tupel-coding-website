---
layout: post
title: "Rezept für zukünftige Android Reviews"
description: "Wieso aufwendige Reviews schreiben, wenn man es auch leichter haben kann?"
date: 2011-10-04
tags: [Android]
published: true
category: it
comments: false
share: true
---

Basierend auf einem mittlerweile leider verschollenem Beitrag auf [512 Pixels](http://512pixels.net) habe ich die Eckpunkte für Kritiken zukünftiger Android Phones mal übersetzt. Spart euch die Mühe eine aufwendige Technikbesprechung zu schreiben, mit folgenden Punkten liegt man in nächster Zeit bei Android Phones immer richtig:

* Der Bildschirm ist besser, heller und größer als die der früheren Smartphones.
* Es ist dünn, aber nicht so dünn wie das iPhone.
* Die Android-Oberfläche des Herstellers ist nicht besonders gut, aber an manchen Stellen könnte sie es sein.
* Der neue Launcher kommt mit vielen Widgets, welche Dinge tun.
* 4G ist cool, außer man mag Akkulaufzeit. Aber verdammt, das Surfen im Netz war verdammt schnell, zumindest 28 Sekunden lang.
* Die Software der Kamera ist immer noch nicht gut. Auch die der Front-Kamera! (kann durch 3D ersetzt werden)
* Weil Android offener ist, finden sich leider auch viele schlechte Apps im Market.
* Das könnte vielleicht das beste Android Phone werden. Aber wenn nicht, warte einfach ein oder zwei Wochen.
* Die aktuelle Version von Android hat noch Ecken und Kanten, aber die nächste Version wird jeder haben wollen, versprochen.
* Es wird iOS Konkurrenz machen. 
* Natürlich wird dieses Smartphone neue Android Versionen vermutlich nicht bekommen.

Ist natürlich nur lustig gemeint, aber enthält auch einige dicke Körner Wahrheit. Die Android-Hersteller werfen in letzter Zeit ein Smartphone nach dem anderen auf den Markt. Keines hebt sich besonders davon ab und die Kritiken der letzten Monate klangen wie die obigen Punkte.