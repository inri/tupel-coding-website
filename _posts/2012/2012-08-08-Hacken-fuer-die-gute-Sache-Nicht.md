---
layout: post
title: "Hacken für die gute Sache, nicht"
description: "Wie man mit Adressdaten und Kreditkartennummern Konten bei Apple, Google und Amazon infiltrieren kann."
date: 2012-08-08 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Die Geschichte: Hacker haben sich über die virtuelle [Identität von Mat Honan](http://www.wired.com/gadgetlab/2012/08/apple-amazon-mat-honan-hacking/all/) hergemacht. Heraus kam dabei die Übernahme seiner Amazon, Googlemail, Twitter und iCloud Accounts. Das, was die Hacker gemacht haben, überhaupt als Hack zu bezeichnen ist meines Erachtens grenzwertig. Mit Technik hatte das nämlich nichts zu tun.

Amazon konnte man sehr leicht austricksen. Allein mit der Information aus Benutzer-Email und Adresse konnte man bei Amazon eine neue Kreditkartennummer (s)einem Konto hinzufügen. Meldete man sich danach bei Amazon und sagt, dass der Email-Account z.b. gehackt wurde, kann man dem Konto eine neue Email-Adresse hinzufügen und über diese schickt man sich einen Kontoreset. Danach hat man Zugriff auf das Amazon-Konto, inklusive alter Kreditkartennummern. 

Diese Nummer braucht man für das Benutzerkonto bei Apple. Apple gab bereitwillig den Zugriff für ein Konto heraus, wenn man Email, Adresse und die letzten vier Ziffern der Kreditkarte kannte. 
Hat man Zugriff auf die iCloud, kann man z.b. die Geräte aus der Ferne zurücksetzen lassen, was alle Daten löscht. Dem Herrn Honan ist eben genau das passiert. Zusätzlich hat man auch Zugriff auf das Googlemail Konto und auch Twitter. 

Das Hauptproblem ist die Wichtigkeit der Kreditkartennummer bei den Konten. John Gruber beschrieb es sinngemäß folgendermaßen: 

*Ich würde niemals meine Passwörter notiert auf einem Zettel in meiner Brieftasche herumtragen. Aber genau das ist der Fall, wenn die Sicherheit von der Kreditkartennummer abhängt. Jeder Dieb kann sich über meinen iCloud Account hermachen, nur weil er meine Kreditkarte und Führerschein hat.*

Deshalb haben Amazon und Apple die Notbremse gezogen und die beschriebenen Möglichkeiten deaktiviert. Mal sehen ob es da in den nächsten Wochen neue Mechanismen geben wird.

## Stockholm Syndrom

Mich brachte das Stockholmsche-Gewinsel von Mat Honan auf die Palme. Er sei vor allem böse auf sich selbst, da er hätte einige Sicherheitsmechanismen mehr verwenden können. Die Hacker, mit denen er danach in Kontakt standen, wollten nur auf die mangelhafte Sicherheit aufmerksam machen.      
Ach ja? Und dafür muss man einem Fremden seine Gerätschaften inklusive aller Daten komplett löschen? Natürlich ist er selbst Schuld, dass er keine Backups gemacht hat. Aber wenn ich z.b. mein iPhone in der Mensa für 5 min unbeaufsichtigt lasse und jemand klaut es mir, ist das natürlich von mir doof, aber trotzdem noch Diebstahl. 

Er würde keine rechtlichen Schritte gegen die Hacker einleiten, wenn sie ihm sagen, wie sie es gemacht haben. Auch so ein Schwachsinn. Wenn sie sowieso Aufmerksamkeit haben wollten, wäre es herausgekommen.      
Herr Honan bekommt die Bilder von seiner 1-jährigen Tochter aber nicht zurück. Beim Zurücksetzen des MacBooks wird ein 4-stelliger Code mitgegeben, mit dessen Hilfe man die Daten wiederherstellen kann. Den bekam er auch nicht. 

Als Hack würde ich es übrigens nicht bezeichnen, da ich in dieser Sparte eher technische Schwachstellen sehe und nicht das Ausnutzen konzeptioneller Schwächen.      
Es ist zwar schön, dass sich Apple und Amazon, und hoffentlich weitere Konzerne, Gedanken machen, aber das ändert nichts an der Tatsache, dass diese sogenannten Hacker Arschlöcher sind.