---
layout: post
title: "Will the Real iPad please stand up?"
description: "Das iPad mini ist das wahre iPad."
date: 2012-11-10 
category: it
tags: [iPad]
published: true
comments: false
share: true
---

**Das iPad mini ist das wahre iPad.** Absolut. Es ist so. Kopiert ist dieser Satz von [Dan Frommer](http://www.splatf.com/2012/11/ipad-mini/) via [John Gruber](http://daringfireball.net/linked/2012/11/05/the-real-ipad). 


Letzterer sagt das gleiche wie ich, dass ein kleines iPad vor drei Jahren technisch noch nicht möglich gewesen wäre. Und er erwähnt einen Aspekt, der mir mit dem iPad mini auch aufgefallen ist. 

Das mini fühlt sich richtiger an und eben wie das wahre iPad, aber genau über dieses wurde in 2010 noch gelächelt, dass es ja nur ein großes iPhone sei. Damals wäre es für Apple auch aus diesem Grund vielleicht etwas schwieriger gewesen diesen Gerätetyp zu etablieren.

Das iPad hat sich etabliert und nun ist dieses neue iPad in klein auf dem Markt, für das viele die nächste Ära von massenhaft verkauften i-Geräten vorhersagen. Das ist durchaus wahrscheinlich, denn der Preis ist relativ gering und das Tablet besser als das originale iPad. Für die meisten Anwendungsfälle zumindest.