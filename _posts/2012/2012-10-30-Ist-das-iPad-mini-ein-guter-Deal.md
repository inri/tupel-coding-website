---
layout: post
title: "Ist das iPad mini ein guter Deal?"
description: "Einige Vergleiche zwischen dem iPad mini und anderen Tablets."
date: 2012-10-30 
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Im letzten Artikel habe ich mir Gedanken zum Wechsel auf ein iPad mini von einem iPad 2 gemacht. Nun stelle ich mir die Frage, ob das iPad mini besonders im Vergleich zu seinen iPad Brüder und auch dem Fire HD und Nexus 7 überhaupt ein guter Deal sein kann.


Als Grundlage dient die 16 GB Ausführung. Ich habe nur die interessanten Werte verglichen, denn einiges lässt sich schwer bewerten (Prozessor, RAM). 

#### iPad mini vs. iPad 2 - 329€ vs. 399€

* mit 308 g halb so leicht wie das iPad 2 (601 g) und mit 7,2 mm sogar dünner als das neue iPhone 5
* dank gleicher Auflösung wie das iPad 2 (1024 x 768) werden auch alle iPad Apps perfekt laufen und aussehen
* etwas höhere Auflösung als das iPad 2 (163 ppi vs. 132 ppi)
* gleicher Prozessor wie das iPad 2
* Facetime HD Front Kamera (1,2 MP) und 5 MP Back Kamera (besser als iPad 2)
* besseres Bluetooth als iPad 2 (Bluetooth 4 vs. 2.1)
* kleinerer Bildschirm (7,9 vs. 9,7 Zoll)

#### iPad mini vs. iPad 3 - 329€ vs. 499€

* kein Retina Bildschirm
* *langsamer* A5 Prozessor
* auch Bluetooth 4
* gleiche Kameras
* leichter und dünner (iPad 3 652 g)
* kleinerer Bildschirm (7,9 vs. 9,7 Zoll)

#### iPad mini vs. Nexus 7 &amp; Kindle Fire HD

* größerer Bildschirm (7,9 vs. 7 Zoll)
* kleinere Auflösung (1024 x 768 bei 163 ppi vs. 1280 x 800 bei 216 ppi)
* Front Kameras sind gleich, aber das mini hat eine Back-Kamera
* mini ist leichter (308 g vs. 340 g vs. 395 g)
* mini ist mehr als 3 mm dünner (7,2 mm vs. 10,45 mm vs. 10,3 mm)
* Preise 329€ vs. 200€ vs. 200€

## Zusammenfassung

Diese Vergleiche stellen natürlich nicht die ganze Wahrheit dar. Beim iPad mini wird man von Anfang an mehr verfügbare Apps im Store vorfinden. Das Fire HD ruckelt ganz leicht, dafür hat es NFC verbaut. Das Nexus hat GPS, ist etwas schmaler als die beiden Kontrahenten und hat eine Android Update-Garantie.       
Man kann auch noch weitere Vergleiche heranziehen. So beginnt der neue iPod Touch bei 329€, hat 32 GB Minimum, den iPhone 5 Retina Bildschirm und die neuen Kopfhörer. Dafür aber auch nur einen 4 Zoll Bildschirm. 

Hat man kein iPad, macht man im Vergleich zum iPad 2 ein ganz gutes Geschäft, wenn man denn mit dem kleineren Bildschirm zufrieden ist. Der Abstand zum iPad 3 kommt vor allem durch den Retina-Bildschirm zustande. Den Prozessor vermisst man im mini vermutlich nicht so sehr.      
Gravierender ist der Preis-Unterschied zu den beiden Androids. Der Unterschied kommt dabei in erster Linie durch die Größe zustande, etwas mehr Bildschirm in einem dünnerem Gehäuse. Die Pixeldichte der Androids mag etwas höher sein, nur 53 ppi mehr, ist aber trotzdem zu gering um sich hochauflösend oder Retina zu nennen. Ein iPad mini mit Retina Auflösung müsste zwischen 280 ppi und 300 ppi haben. 

Da ist die nächste Frage: Kommt in einem halben Jahr schon ein Retina Upgrade? Das Update des iPad 3 (Lightning Anschluss und besserer A6x Prozessor) hat zumindest bei Käufern des selbigen nicht für Freude gesorgt. Ich glaube aber, dass wir erst in einem Jahr damit rechnen können. Dann wird es ein neues großes iPad geben, ein neues iPad mini Retina und die alten minis und iPad 2 werden komplett aus dem Sortiment geschmissen (zusammen mit dem iPhone 3GS).  So hätte Apple eine komplettes Retina-Sortiment. 

Wie so oft, muss jeder für sich selbst entscheiden, was wichtig ist und was nicht. Über die neuen superleichten Apple Geräte verfasse ich bald auch noch einen Eintrag. Denn diese Geräte sind nicht einfach nur etwas leichter als andere Geräte, sie wirken moderner. Zusammen mit dem stets flotten iOS erhält man eine perfekte Symbiose und damit hat Apple aktuell ein Alleinstellungsmerkmal. 