---
layout: post
title: "HTCs Windows Phone 8X und 8S"
description: "Kurze Zusammenfassung zum HTCs neuen Windows Phone Geräten."
date: 2012-09-19 
category: it
tags: [Windows Phone]
published: true
comments: false
share: true
image: 
  feature: 2012-windows-phone-8X-lg.jpg
  credit: HTC
  creditlink: http://htc.com
---

HTC hat heute seine beiden Windows Phone 8 Smartphones vorgestellt, das 8X (oben) und das kleinere 8S (unten). Wobei es gar nicht mal so viel kleiner ist, denn das 8X ist nur 10 mm höher, 3 mm breiter und sogar 0,16 mm dünner. Das eine kommt mit 4,3 Zoll und das andere mit einem 4 Zoll Display.

      
Die technischen Daten stehen auf der Webseite von HTC, ich gehe nur auf einige Punkte ein. Selbstverständlich auch im Vergleich zum neuen iPhone. Das Design erinnert Nokias Lumia-Reihe. Die Telefone sind quietschbunt und wirken abgerundet quadratisch. 

Das 8X ist das Flaggschiff, das 8S eher die günstigere Alternative. Aber wieso wählt man so ähnliche Größen? Ich ahne wieso und komme gleich darauf zurück. Das Display des 8S ist fast genauso groß wie das des längeren iPhone 5, aber es hat eine geringere Auflösung. Machen wir uns nichts vor, es wird nicht toll aussehen. Die Gehäuse sind natürlich aus Plastik und dadurch wiegt das 8S genauso viel wie das iPhone. Obwohl das 8X nur minimal größer ist, bringt es 20g mehr auf die Waage. 

<figure>
    <a href="/images/2012-windows-phone-8S-800.jpg">
	   <img 
		  src="/images/2012-windows-phone-8S-800.jpg" 
		  alt="DESCRIPTION IN HERE"></a>
    <figcaption>Das 8S</figcaption>
</figure>



Woher kommen diese 20 g? Das Display ist scheinbar nur 5 mm größer. Das 8X hat einen 1,5 GHz Dualcore (statt 1 GHz), mit 1 GB doppelt so viel RAM, nur 16 GB Speicher und mit 1800 mAh eine etwas höhere Akkukapazität wie das 8S. Mein Tip ist nach wie vor: Die leistungsstärkeren Komponenten sind schwerer und die Hersteller bekommen das gesamte Smartphone selbst mit Plastikgehäuse nicht leichter und kleiner und wählen deshalb so riesige Displays.     
Interessant ist die Dicke beider HTCs im vergleich zum iPhone: Sie sind 2,5 mm dicker. Das ist auf dem Lineal nicht besonders viel, aber bei mobilen Geräte einfach eine andere Welt. 

Was gibt es noch zu den beiden zu sagen? Die Größen für den Speicher sind frech. Das 8X hat keine erweiterbaren 16 GB und das 8S kommt wohl nur mit 4 GB daher, die man aber mit MicroSD erweitern kann (immerhin). Preislich liegen wir bei 550€ und 300€, besonders letzteres ist in Anbetracht der Tatsache, dass 2-4 Jahre alte Technik verbaut ist, auch grenzwertig. Dafür hat das 8X NFC, juhu!      
Die Pixeldichte des 4,3 Zoll Display ist etwas höher als die des iPhones (341 dpi vs. 326 dpi), aber das macht kaum einen sichtbaren Unterschied aus, im Gegensatz zu den 234 dpi des 8S. Viel interessanter werden die Farben des Displays sein. Aber das erfahren wir im November.

Insgesamt ist nichts dabei, was irgendwie visionär oder innovativ wäre. Zumindest das 8X ist ein recht solides Gerät, aber nicht mehr und nicht weniger. Über die Software reden wir später.