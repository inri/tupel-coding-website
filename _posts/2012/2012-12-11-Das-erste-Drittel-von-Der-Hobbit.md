---
layout: post
title: "Das erste Drittel von Der Hobbit"
description: "Was passiert im ersten Drittel des Hobbit Romans?"
date: 2012-12-11 
category: medien
tags: [Literatur]
published: true
comments: false
share: true
---

Der erste Teil der Hobbit-Trilogie kommt in die Kinos und da ließ ich es mir nicht nehmen, die ersten Seiten des Buches zu lesen.      


Besonders zeitaufwendig ist das nicht, denn im Gegensatz zu der Herr der Ringe ist Der Hobbit eine eher kurze Geschichte. Auf meinem iPad Mini ist der Roman 412 Seite lang, wovon ein Drittel ungefähr um die 136 Seiten beträgt. Diese Seiten habe ich hinter mir und aus der erzählerischen Perspektive müsst das eigentlich passen. Was wird also im ersten Film alles passieren? Im folgenden eine kurze Inhaltszusammenfassung vom ersten Drittel des Buchs *Der Hobbit*. 

### Vorsicht Spoiler

Zu Beginn hat unser Held Bilbo Beutlin ungebetenen Besuch von einem Dutzend Zwergen. Durch Gandalf, seinem Freund, wird er Teil einer Abenteuerexkursion um alte Schätze der Vorfahren der Zwerge aus dem Klauen des Drachen Smaug zu befreien. Ausführlich wird der gemütliche Abend mit den erzählenden und singenden Zwergen geschildert. Wobei das Singen nichts außergewöhnliches ist, alle Völker singen gerne in Der Hobbit.
Am nächsten Tag beginnt die Reise. Die muntere Truppe reitet durch die Lande und in Richtung des Gebirges. Das erste Abenteuer sind die drei Trolle, die sich kurzerhand alle Zwerge schnappen. Durch eine Liste von Gandalf werden sie bei Tagesanbruch zu Stein und die Zwerge wieder frei. Die Truppe sucht die Trollhöhle und findet dort unter anderen alte Elbenschwerter.

Die Reise geht weiter und sie kommen als nächstes nach Bruchtal, wo sie Elrond und singende Elben treffen. Sie erholen sich einige Wochen und Bilbo würde am liebsten gar nicht mehr fort von diesem Ort. Danach geht es weiter ins Gebirge hinein. Wegen eines schweren Gewitters flüchten sie in eine kleine Höhle, werden aber in der Nacht von Orks gefangen genommen, welche natürlich auch singen. Gandalf kann sich natürlich in Sicherheit bringen und er rettet die anderen mal wieder, wie schon bei den Trollen.       
Auf der Flucht vor den Orks in den engen Gängen des Gebirges, stößt sich Bilbo den Kopf, wird ohnmächtig und wacht allein im Dunkeln wieder auf. Genau an dieser Stelle findet ihn der Ring, oder er findet den Ring. Wie man es nimmt.

Kurz darauf trifft er auf Gollum und spielt ein Ratespiel, damit dieser ihm den Ausgang der Höhle zeigt. Als Bilbo merkt, dass Gollum nicht die vertrauenswürdigste Person ist, trickst er ihn aus und flüchtet. Gollum bemerkt, dass er seinen Schatz verloren und Bilbo ihn wohl gefunden hat. Dieser wiederum findet durch Zufall die Fähigkeit des Ringes heraus, kann Gollum belauschen und ihm zum Ausgang folgen.       
Draußen trifft er auf seine Truppe, denen er nichts vom Ring erzählt. Sie führen ihre Reise fort, vermutlich verfolgt von wütenden Orks.

### Spoiler Ende

## Eine kurze Filmvorlage

Ich vermute mal, dass die Handlung des ersten Filmteils bis zu dieser Stelle gehen wird. Das sind ungefähr fünf Ereignisse und vom Spannungsaufbau würde das ebenfalls sehr gut passen. 
Kritisch anmerken muss ich allerdings, dass die Romanvorlage viel zu dürftig für eine 1:1 Verfilmung ist. Da wird Peter Jackson an einigen Stellen genauso ausführlich werden, wie er bei den Herr der Ringe Filmen kürzen musste. Es mag aktuell schon Inhaltsbeschreibungen über den Film geben, aber ich wollte vorerst nur das Buch lesen und schaue mir als nächsten den Film an. Unvoreingenommen und neugierig.