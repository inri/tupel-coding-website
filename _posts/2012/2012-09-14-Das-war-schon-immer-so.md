---
layout: post
title: "Das war schon immer so"
description: "Immer das gleiche mit den Produkten."
date: 2012-09-14 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Apple versucht Geräte zu bauen, die den Durchschnittsmenschen ansprechen und seine Bedürfnisse an Technik befriedigen. Nicht mehr und nicht weniger. Gelingt ihnen das, kauft die Masse an Menschen ihre Geräte. Das gleiche wollen andere Hersteller auch.

Mir drängt sich der Verdacht auf, dass mit diesen sogenannten neuen Innovationen dem potentiellen Käufer suggeriert werden soll, er müsse unbedingt ein Smartphone kaufen welches diese neuen Technik  (NFC, kabelloses Laden) unterstützt. 

Dabei sind diese Geräte meist an genau den Stellen mangelhaft, die essentiell und wichtig für die alltägliche Benutzung sind. 

Das ist nicht neu, das passierte schon immer und wird immer passieren.