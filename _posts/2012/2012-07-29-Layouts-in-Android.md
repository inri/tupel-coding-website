---
layout: post
title: "Layouts in Android"
description: "Eine grobe Übersicht wie das unschöne Layout System in Android funktioniert."
date: 2012-07-29 
category: it
tags: [Android]
published: true
comments: false
share: true
---

Ich schlage momentan meinen Kopf gegen die Wand. Dabei hoffe ich, dass mir doch noch eine Lösung über den Weg läuft. Diese ganze Bildschirm-Problematik bei Android-Geräten ist ja bekannt. Wenn man sich aber mal richtig damit beschäftigt, merkt man erst wie beknackt das ganze wirklich ist.

Wie funktioniert das mit dem Layout bei Android überhaupt?
Grob gesagt hat man zwei Komponenten: Layouts, im XML-Format, und Ressourcen, Bilder z.b. PNG. Ressourcen werden in verschiedenen Auflösungen gespeichert, wohingegen Layouts für die verschiedenen Bildschirmgrössen angepasst werden müssen.
Für die einzelnen Dateien gibt es entsprechende Order, aus denen sich Android bedient. Automatisch bedient, je nachdem welche Art Bildschirm der App zur Verfügung steht.

## Das Problem

Verschiedene Bildschirmgrössen sind ja nicht so schlimm. Das Problem sind viel eher die verschiedenen Pixeldichten der Bildschirme. Best Practice für die Layouts ist das Benutzen von dp, Dichte-unabhängige Pixel (density-independent pixel). Leider funktionieren diese dps nicht besonders gut.     
Ich baue gerade ein Layout und nutze dafür ein Nexus One mit einer Bildschirmgrösse von 3,7 Zoll und 250 ppi (Pixel per Inch). Diese Grösse ist sehr gut geeignet und weit verbreitet. Nun soll aber auch ein Design für ein 4,7 Zoll Smartphone her, was in Anbetracht der grossen Verbreitung vom z.b. Galaxy S3 kein Wunder ist. Diese grossen Smartphones haben meist 300+ ppi und das ist ungünstig.

Android wählt sich die Layouts selbst aus und leider fallen 3,7 Zoll und 4,7 Zoll Bildschirme in die gleiche Kategorie (normal). Das bedeutet, dass man das Design so gut machen muss, dass es zwischen 3 Zoll und 5 Zoll Bildschirmen (Reichweite von normal) skaliert und ansehbar ist. Dies erfordert viel Aufwand, denn allein mit der Verwendung von dp ist es nicht getan.

Mit Android 3.2 wurde dem doofen, automatischen System ein Ende bereitet. Nun bestimmt der Entwickler welches Layout für welche Bildschirmweite genutzt wird und kann entsprechende Ordner anlegen. Das ist auch ganz sinnvoll, denn vor allem länger werden die Bildschirme, breiter eigentlich seltener.

Einziges Problem: Android 3.2 und höher sind erst auf 13% aller Telefone zu finden. Aber wie gesagt, vielleicht finde ich doch noch eine gute Lösung für niedrigere Android-Versionen.
