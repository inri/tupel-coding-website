---
layout: post
title: "Es ist nicht alles Gold, was Apple ist"
description: "Eine Aufzählung diverser Mängel in Apples Betriebssystemen."
date: 2012-11-23 
category: it
tags: [Apple, iOS, Design, macOS, OS X]
published: true
comments: false
share: true
---

Ein gewisser Kontra stellte [auf seinem Blog](http://counternotions.com/2012/11/05/sirjony/) die berechtigte Frage, ob ein John Ive die zahlreichen UI-Baustellen in OSX und iOS beseitigen kann. Denn das skeuomorphe Design sei nicht die Schwäche an Apples Betriebssystemen, sondern es tun sich weitere grundlegende Probleme und Notwendigkeiten auf.

Hier seine Aufzählung von mir übersetzt, kursiver Text ist mein Zusatz.

* 6 elementare Einstellungen für die Akkulaufzeit (GPS, WiFi, UMTS, Bluetooth, Notifications und Bildschirmhelligkeit) kann man nach wie vor nur über mehrere Schritte erreichen, die für den Nutzer keinesfalls intuitiv sind. Noch dazu sind sie nicht gruppiert sondern in den Einstellungen verteilt.
	* *Bei Android legt man sich ein Widget auf eine Seite und dann lässt sich dies alles mit einem Tippen erledigen.*
* Viele Apple Programme haben schon lange keine richtigen Updates mehr bekommen und andere benötigen dringend ein neueres Design (Preview, TextEdit, Contacts).
	* *Das betrifft Programme und Apps von Apple.*
* Kernfunktionalitäten wie das Wörterbuch, die iOS Tastatur und die Autokorrektur sind nicht die besten ihrer Art.
* Apps kann man nur in kleinen Ordner verwalten, in welchen sie dann als Mini-Icon zu sehen sind.
	* *Wieso kann man die Ordneransicht nicht auf beispielsweise 4x4 Icons vergrößern? Dann erkennt man zumindest die kleinen Icons darin. Was auch immer die Lösung sein kann, sie sollte kommen, denn der aktuelle Zustand ist nicht benutzerfreundlich.*
* Das Navigieren zwischen laufenden Apps ist unelegant und nicht klar für den Nutzer, genauso  hat auch der Datenaustausch zwischen Apps noch ernsthafte Probleme (zu wenig Möglichkeiten für Entwickler).
* Die iCloud Integration und generell das Austauschen von Dateien zwischen verschiedenen Programmen klappt noch nicht. Auch die Entwickler beschweren sich über die ungenügende API.  Die iCloud ist erst ein Jahr alt, aber das ist in der IT-Welt eine kleine Ewigkeit. 
	* *Das gleiche trifft auch bei OS X.* 

Einige zusätzliche konkreten Beispiele von mir:

* Wieso gibt es immer noch keine Widgets oder andere ähnliche Konzepte? Es gab in iOS alle Jahre zahlreiche kleine Neuerungen, aber große Änderungen blieben aus. Vor zwei Jahren, als die Widgets in Android noch lange nicht optimal waren, hatte ich vermutet, dass Widget-ähnliche Konzepte bei iOS später folgen, aber dafür nicht so *unfertig* wären. Es kam bis heute nichts.
* Wenn ich während eines Telefonats oder schon während des Verbindungsaufbau eine Email erhalte, ploppt die Benachrichtigung auf und das Telefon-UI reagiert nicht mehr auf Eingaben, bis man die Benachrichtigung weg klickt. Besonders toll ist das, wenn man nur jemanden kurz anklingeln möchte und plötzlich 5 Emails geladen werden und das weg klicken mehrere Sekunden dauert.
* Jahrelang musste man bei Updates von Apps das Passwort eintippen. Wieso? Das weiß keiner so genau. Als ich mein erstes iPad in der Hand hielt und es Updates gab, erschrak ich, weil ich dachte dass sie nochmals berechnet werden. Aber kostenpflichtige Updates gibt es gar nicht. Das finden beispielsweise die Entwickler nicht so toll, die ihre Programme im Mac App Store anbieten.       
Größere Updates kann man entweder kostenlos verschenken oder man reicht ein völlig neues Programm ein. Letzteres wiederum behindert die Möglichkeit den Käufern der vorherigen Version einen Rabatt anzubieten. Eine Baustelle seit es den Mac App Store gibt. Zumindest die Sache mit dem Passwort bei einem Update wurde endlich abgeschafft.
* Die iOS Kalender App ist in einigen Aspekten auch fehlerhaft. Es gibt einige Termine, die, obwohl längst aus Google gelöscht, in der Kalender App immer noch zu finden sind. Was soll das? In Android und auf dem Mac klappt es, in iOS nicht. Den Fehler hab ich seit über einem Jahr.
* Ach ja, und da war noch die Macke seit iOS 6, dass meine Einstellungen für mobiles Internet einfach so gelöscht wurden. Ich habe noch nicht herausbekommen, was diesen Fehler auslöst, vielleicht ist es ein Feature? Einem anderen Kommilitonen passierte das auch schon mehrmals.

Oder auch einige Beispiele aus OS X, wobei darunter auch Fehler sind, die mit UI und Usability nicht so viel zu tun haben:

* Ich arbeite mit 5+ Schreibtischen. Wenn ich auf Schreibtisch Nr. 3  Chrome im Fenster geöffnet habe (Schreibtische 4 und 5 sind leer!) und es dann auf Vollbild umschalte, knallt mir OS X das neue Vollbild nach Schreibtisch Nr. 5 ganz nach hinten. Nicht vor oder hinter Schreibtisch Nr. 3, wie man es erwartet. 
	* Wobei man sagen muss, dass die Schreibtischverwaltung zu Beginn in OS X Lion noch wesentlich mehr Fehler hatte. Herumspringende Fenster oder sich lustig verändernde Schreibtische habe ich zum Glück nicht mehr.
* Oder wenn man mit einem externen Monitor arbeitet und ein Programm auf Vollbild schaltet, ist der zweite Bildschirm (egal ob MacBook oder Monitor) grau und man kann kein Programm dort ablegen. Warum? Weiß nur Apple allein. Es kommt zwar nicht oft vor, ist aber nervig.
* Den Mountain Lion Anmeldungs-UI-Fehler hatte ich auch einige wenige Male. Quasi nach dem Öffnen des MacBooks sehe ich nur seltsame Pixel und in der Mitte ein Feld, wo ich meinen Benutzernamen tippen muss (normalerweise muss ich nur das Passwort neu eingeben) und danach das Passwort. Bewege ich die Maus lichten sich die seltsamen Pixel etwas. Komisch ist es trotzdem. 
* Generell gibt es ab und zu Seltsamkeiten was den Sleep Modus angeht. Meistens ist nach dem Anmelden alles wie es sein soll, aber manchmal sind die Hälfte der geöffneten Programme geschlossen und es erscheinen Meldungen wie *Das Programm xyz hat den Abmeldevorgang unterbrochen*, welchen Abmeldevorgang?! Ich hab mein MacBook nur zugeklappt.
* Unerwähnt sollen auch diverse Abstürze von Programmen nicht bleiben. Das passiert nicht oft, aber dennoch häufiger als bei meinem Windows 7 Computer.

All diese Dinge wiegen in Summe trotzdem nicht so schwer, dass ich mein iPhone und MacBook zurückgeben wollen würde. Es sind aber Aspekte, derer man sich annehmen muss. Bei nicht wenigen spielen Fragen des UI und Benutzbarkeit eine wichtige Rolle und nun ist man natürlich gespannt, ob diese unter John Ive gelöst werden.      
Interessant fand ich Kontras rhetorische Frage, ob es überhaupt einen Human Interface Designer für ein UI für über 500 Millionen Benutzer geben kann. Und wenn ja, ob man das wirklich möchte. Und wenn man es will und es theoretisch geht, kann eine Person allein für ein solch großes Ökosystem verantwortlich sein?     
Besonders, wenn nicht wenige Aspekte auch technischer Natur sind?

An Windows 8 traue ich mich nicht heran. Mittlerweile habe ich nur sehr wenig gutes und viel schlechtes gehört. Ich mag die Stabilität von Windows 7 und einige Funktionen, die man durch Drittanbieter aber auch auf OS X haben kann. Was mich aber unsäglich stört, ist die fehlende Weiterentwicklung. Ich weiß das Microsoft ständig Updates rausschickt um in erster Linie die Sicherheit des Systems zu gewährleisten. Andere grundlegende Fehler im UI bleiben nach wie vor und Windows 8 kann doch nicht die Antwort sein?

Ist Windows 8 das neue Windows Vista und muss ich ernsthaft 3 Jahre auf Windows 9 warten? Ärgerlich ist meiner Meinung nach auch, dass Microsoft für ihr neues System nicht bestraft wird.       
Baut Apple ein schlechtes iPhone, hat man zahlreiche alternative Hersteller aller Preiskategorien zur Verfügung. Aber bei Computern ist das nicht der Fall. Entweder das eine schlechte System oder ein teurer Apple Computer. Linux gibt es auch noch, aber werden da schon *Fertigsysteme* angeboten? Besser wäre es.

Die nächsten Jahre in Sachen Betriebssysteme werden interessant.