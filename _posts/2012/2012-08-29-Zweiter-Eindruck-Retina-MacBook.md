---
layout: post
title: "Zweiter Eindruck vom Retina MacBook"
description: "Der zweite Tag mit dem Retina MacBook."
date: 2012-08-29 
category: it
tags: [MacBook]
published: true
comments: false
share: true
---

Der zweite Tag mit dem Retina MacBook Pro liegt hinter mir und ich beginne die nächste Runde meiner Erfahrungen mit etwas negativen Dingen.     

 
Ich hatte einen Kernel Panik, also einen Absturz des Macs. Was habe ich getan? Nichts natürlich, nur einige höher auflösende Cover für iTunes geladen. Die Anzahl von solchen Abstürzen in 2,5 Jahren Mac OS X kann ich an einer Hand abzählen.     
Seltsam war auch ein Bildschirmdarstellungsfehler nachdem ich den Mac aus dem Ruhezustand geholt hatte. Vergaß leider ein Foto davon zu machen, aber es war nur der halbe Anmeldebildschirm zu sehen. 

Des weiteren setzt Lion meine geöffneten Fenster nach z.b. einem Neustart fast immer auf den ersten Schreibtisch. Ich würde vorsichtig behaupten, dass bei meinem alten MacBook die Zuordnung korrekt war. Andererseits habe ich teilweise auch Uptimes von mehreren Wochen gehabt, da achtet man vielleicht nicht so sehr drauf.     
Das waren soweit erst mal die kleinen negativen Aspekte. Fairerweise muss man dazusagen, dass es sich um softwaretechnische Probleme handelt. Ich bleibe wachsam.

Vom Hocker haut mich immer noch die Geschwindigkeit und Performance des MacBooks. Ein Kommilitone und halber Profigamer war zum Beispiel von Diablo 3 sehr beeindruckt. Auch in der normalen Benutzung war das MacBook so flink, dass er nur kopfschüttelnd meinte *Es regt mich richtig auf, dass das MacBook so gut ist*.    
iPhoto ist nun auch Up-to-Date und zeigte, dass nicht nur Schrift unglaublich gut aussieht, sondern Bilder ebenfalls. An und für sich ist so ein Retina-Bildschirm aktuell das beste Gerät um Fotos in bester Qualität anzuschauen. iPhone und iPad sind auch hochauflösend, aber dann doch etwas klein. Oder man druckt die Bilder in Profiqualität und Originalgröße aus, was auf Dauer auch am Geldbeutel nagt.

Fotos sehen enorm knackig und scharf aus, wie eben auch die Schrift. Umso bitterer sind aber Bilder, die nicht groß genug sind und deshalb schwammig und unscharf wirken. Im Web trifft man immer mal wieder auf solche Exemplare.
Sehr angenehm sind auch die Boxen, wenn gleich sie kaum Bass haben. Das war zu erwarten.     
Heute werde ich ein wenig Programmieren und generell fast nur mit dem Mac arbeiten. 

*Coconut Battery* sagt übrigens, dass mein MacBook 5 Wochen alt ist. Und ich hatte vorhin sogar einen zu 101% geladenen Akku.