---
layout: post
title: "Lob für Nexus Reihe"
description: "Google gar die Nexus Reihe aufgefrischt und das sieht gar nicht mal schlecht aus."
date: 2012-11-02 
category: it
tags: [Android]
published: true
comments: false
share: true
image: 
  feature: 2012-google-nexus-family-lg.jpg
  credit: Google
  creditlink: http://google.com
---

Still und heimlich hat Google am Montagabend die Katzen aus dem Sack gelassen: Ein 10 Zoll Tablet und ein neues Nexus Phone, das Nexus 10 und das Nexus 4. Dazu natürlich ein kleines Android Update (4.2), dessen Änderungen ich aber überschaubar finde (Panorama Sphere-Funktion, Videostreaming, Multi-Benutzer, Tastatur-Verbesserungen und Kleinigkeiten). Wie Apple auch, pflegt Google sein System. Die neue Hardware ist bei weitem interessanter.

## Nexus 10

Das Nexus 10 finde ich aus mehreren Gründen beachtenswert. Zum einen hat es eine höhere Pixeldichte als das iPad 3, eine angeblich ähnlich lange Akkulaufzeit und ist dabei einen halben Millimeter dünner. Zum anderen kostet es 100€ weniger als das iPad (16 GB: 499€ vs. 400€ und 32 GB: 500€ vs. 599€). Das sind in der Tat mehrere Gründe, die die Entscheidung für ein iPad schwieriger machen (um es mal subjektiv auszudrücken).      
Aber John Gruber hat es auch schon gesagt, das Nexus wird weniger dem iPad Konkurrenz machen, wohingegen Amazon und Co mit ihren mittelmäßigen Tablets ins Schwitzen geraten werden. Und Microsoft natürlich. 

## Dünner und leichter

Ich bin mir nicht sicher, ob Apple in Sachen Pixeldichte und Auflösung nachziehen wird. Immerhin hat Apple bald in seiner gesamten Produktlinie einen Retina-Standard der ausreichend und gut ist. Die Geräte decken alle die gleichen Farbräume ab und so sehen Fotos überall gleich fantastisch aus. Eine neue Auflösung würde die Entwickler zu erneuten Änderungen zwingen und sinnvoll wäre das nur beim iPad mini (nächstes Jahr mit dem Retina Update). Natürlich könnte Apple in ihren mobilen Produkten mehr RAM, noch bessere CPU und *wasweißichnicht* alles einbauen. Aber es kommt darauf an, das optimalste auszuwählen.

Interessant ist aber auch, dass das Nexus 10 genug Akku verbaut hat um das Display zu versorgen und trotzdem leichter (50 g immerhin) und dünner als das iPad 3 ist. Mittlerweile ist das iPad 3 zwar auch wieder über ein halbes Jahr alt, aber die Entwicklung geht rasant weiter. In einem Jahr wird vermutlich ein iPhone 5-dünnes 9,7 Zoll iPad erscheinen. Man darf gespannt sein.

## Preisschlager

Beeindruckt haben mich auch die Preise beim Nexus 4. Für 8 GB geht der Spaß bei 300€ los. Das sind 380€ weniger im Vergleich zum Einsteiger-iPhone 5 und noch 100€ weniger als für das immer noch im Verkauf stehende iPhone 4, welches natürlich technisch zum alten Eisen zählt. Selbst wenn wir auf gleicher Speicher-Augenhöhe argumentieren, stehen sich ein 16 GB Nexus 4 für 350€ und ein 16 GB iPhone für 680€ gegenüber.

Wiederum sind die Preise aber vor allem für die anderen Smartphones auf dem Markt ungünstig. Preislich waren fast alle Android und Windows Phones stets unter dem iPhone angesiedelt. Nun haben wir aber ein Gerät, welches technisch modern und gut ist, aber preislich auch noch die Flaggschiffe der Konkurrenz unterbietet. Wer mag noch ein Galaxy S3 von Samsung für 450€ kaufen, wenn er das gleiche und mehr viel billiger haben kann?
Ich habe mir vor kurzem die 4+ Zoll Androiden angeschaut und da gibt es nur sehr wenige Geräte für 350€, welche dann meist schon etwas älter sind. Standard sind 450€ aufwärts.

Auch das Design des Nexus 4 schreit nicht gerade *Ich bin das neue iPhone!*. Man muss nicht erst die Spezifikationen lesen um herauszufinden, dass das Nexus 4 wesentlich dicker und schwerer als das iPhone 5 ist (5 mm und 27 g). Im Vergleich zu anderen Androiden ist es aber okay. Und dazu gibt es die Update-Garantie von Google.     
Wenn der Bildschirm nicht so groß wäre und die Situation bei den Apps etwas anders, würde ich es empfehlen. Vielleicht bekomme ich bald eines in die Hand.

## Nexus ist vorne dabei

Googles Nexus-Reihe ist beeindruckend gut. Genau so hätte ich es mir vor einem Jahr schon gewünscht. Wer weiß ob ich dann zum iPhone gewechselt wäre. Aber die Geräte sind ja bekanntlich nicht alles.      
Zum Nexus 7 folgt sehr bald ein Unpacking, ein Vergleich mit dem Kindle Fire HD und ein dritter Bericht über die Situation von Android in Sachen Apps. Ich teste aktuell sehr viel.