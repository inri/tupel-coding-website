---
layout: post
title: "Camera+ Update 3.7"
description: "Ich bin über das Camera+ Update begeistert"
date: 2012-12-17 
category: it
tags: [App]
published: true
comments: false
share: true
---

Heute erschien ein Update für die beliebte Kamera und Fotobearbeitungs-App: **Camera+**. Das Update ist ausgesprochen gut.

Erst nach einem Jahr des iPhone-Besitzes, fiel mir im Oktober auf, dass die Bilder, die Camera+ exportiert, kleiner als die Originalbilder sind. So war und ist es auch bei den Panorama-Apps. Ich tweetete die Entwickler von *taptaptap* an, bekam aber keine Antwort, denn sie waren anscheinend mit diesem Update beschäftigt! 

In der Qualitätsauswahl steht nun eine bessere Stufe zur Verfügung, die Fotos in ihrer Originalgröße aufnimmt, bearbeitet und exportiert, nämlich in 3264 x 2448 Pixel beim iPhone. Im Update-Text schreiben die Entwickler deutlich, dass nur diejenigen diese Größe wählen sollten, die es brauchen. Denn in der Tat ist der Preis nicht unerheblich: Nämlich Längere Ladezeiten.        
Zumindest beim iPhone 4S dauert es 1 bis 4 Sekunden bis ein Effekt in das Bild geladen wurde. Es ist okay, aber man merkt deutlich, dass es länger dauert. Auch das Importieren oder Exportieren kostet mehr Zeit.

Zugegebenermaßen hatte ich mich im Oktober über die kleine Bildgröße geärgert. Auch wenn ich das Update nun sehr begrüße, kann ich erst jetzt besser verstehen, wieso die Entwickler nicht von Beginn an die höhere Auflösung gewählt haben. Eventuell waren ihre Algorithmen damals noch ineffizienter und die großen Bilder daher für eine gute Benutzererfahrung einfach nicht geeignet. Für ein schnelleres iPhone sind sie okay und auf dem alten läuft es auch ganz passabel.

Ein sehr gutes Updates!