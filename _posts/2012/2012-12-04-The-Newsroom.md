---
layout: post
title: "The Newsroom, bis 4. Episode der 1. Staffel"
description: "Schau euch unbedingt The Newsroom an, eine tolle Serie über Nachrichten."
date: 2012-12-04
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2012-the-newsroom-lg.jpg
  credit: HBO
  creditlink: http://www.hbo.com/the-newsroom
---

Im Sommer stolperte ich über einige positive Meinungen über eine neue Serie namens *The Newsroom*. Leider war ich zu dieser Zeit mit der Bachelorarbeit beschäftigt und hatte keine Zeit für eine neue Serie. Mittlerweile ist sie auch in Deutschland angekommen und ich schaute aus Interesse die erste Episode. 

Oh ja, das Lob war absolut berechtigt. *The Newsroom* ist intelligent, kritisch, witzig und natürlich unterhaltsam. Nach den ersten vier Episoden eine Empfehlung von mir.

## Worum gehts?

*The Newsroom* handelt von einer fiktiven Nachrichtensendung. Ihr Sprecher Will McAvoy wird auf einer Uni-Veranstaltung von einer Studentin gefragt, wieso für ihn Amerika das großartigste Land der Welt ist. Nach einigem Zögern haut er den Zuhörern im Saal Fakt um Fakt um die Ohren, wieso Amerika gerade nicht das tollste Land der Welt ist. Allein nach dieser einen Szene etabliert sich der Hauptcharakter als sympathischer Antiheld a lá Dr. House.

Eine alte Freundin von ihm und Journalistin wird zur Sendung geholt und das Konzept der Nachrichten umgekrempelt: Wir scheißen auf die Quote, wie wollen gute Nachrichten machen. Fakten sollen nun die Nachrichten bestimmten und nicht mehr der mögliche Sensationsgehalt. Natürlich geht es auch um die Menschen vor und hinter der Kamera, aber diese Nebenschauplätze soll sich der Leser besser selbst anschauen.

## Wahrheit statt Meinung

Unsere Medien driften in die gleiche Richtung wie die amerikanischen. Immer öfter wird es wichtiger, welche Schlagzeile den Leser lockt, unabhängig davon ob die Fakten hinter der Schlagzeile stimmen und belegt werden können. Besonders im Amerika setzt sich ein komisches Tolerieren von Meinungen durch. Personen lügen über bestimmte Sachverhalte und tun dies als ihre Meinung ab und gegen eine Meinung kann man schließlich nichts sagen.       
Mir fiel das letztens bei Sarah Kuttners Sendung auf, in welcher der Typ von der FDP wieder mal das Steuermärchen servierte und es keine Einwände gab.

Die Nachrichten in *The Newsroom* sollen wieder anders werden. Was die interviewten Personen sagen, kann von Fakten widerlegt werden. In die Nachrichten kommen nur Themen, die tatsächlich relevant sind und für die es genug Belege gibt. Die Zuschauer sollen informiert werden. Die Fakten entscheiden und nicht, ob fünf andere Nachrichtensender das gleiche bringen.

## Fazit

Eine hochinteressante Serie, die man unbedingt schauen sollte! So sollten Nachrichten sein.