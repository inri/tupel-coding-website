---
layout: post
title: "Sinofsky verlässt Microsoft"
description: "Ein hochrangiger Manager eines großen IT Unternehmens verlässt kurz nach Produktvorstellung das Unternehmen. Die Geschichte kennen wir doch..."
date: 2012-11-15 
category: it
tags: [Microsoft, Surface]
published: true
comments: false
share: true
image: 
  feature: 2012-steven-sinofsky-lg.jpg
  credit: Microsoft
  creditlink: http://microsoft.com
---

Der Hauptverantwortliche der Windows Plattform, Steven Sinofsky, verlässt Microsoft. Oder er wurde gebeten zu es zu verlassen. Oder er wurde sogar verlassen worden. Man weiß es nicht und wird wie bei Forstall und Apple die Wahrheit wohl nie erfahren. Aber die Umstände lassen einige Schlüsse bezüglich Windows 8 und den Surface Tablets zu.

Vielleicht war er ein schwieriger Mensch und, wie angeblich auch Forstall, bei den Mitarbeitern nicht sehr beliebt. Vielleicht sind es wirklich private Gründe gewesen. So oder so, das Timing von beiden passt einfach nicht zusammen. Bei einem war es kurz nach iOS 6, bei dem anderen kurz nach der kompletten Frischzellenkur Windows 8. Hochrangige Manager verlassen doch kurz nach der Veröffentlichung der Produkte, für die sie sich verantwortlich gezeichnet haben, nicht das Unternehmen!

Selbst wenn man kürzer treten möchte wie Bob Mansfield vor einem halben Jahr, behält man meist eine Beraterposition im Unternehmen. Forstall und Sinofsky gehen endgültig. Es wirkt eher wie ein *Wir behalten sie bis die Produkte draußen sind und dann war es das*. Einen neuen Verantwortlichen einige Monate vor Release einzuwechseln führt mit Sicherheit zu Verzögerungen.

## Der mögliche Grund

Ich schreibe an einem Eintrag über den aktuellen Zustand von OS X und iOS, und wieso es gar nicht so abwegig ist, dass man mit der Arbeit von Forstall unzufrieden war. Der Grund bei Sinofsky muss bei Windows 8, Windows Phone und Surface liegen.      
Dass sich das Surface nicht besonders gut verkauft, ist mittlerweile bekannt. Dass sich auch die Upgrades auf Windows 8 in Grenzen halten, ebenfalls. Offizielle Zahlen gibt es von Microsoft noch nicht, aber sie werden intern ganz genau wissen, wie es läuft.       
Und das es kritisch wird, war vorher bekannt. Über Windows 8 hat man in den vergangenen Monaten kaum etwas gutes gehört. Unternehmen ließen verlauten, dass sie nicht so schnell upgraden werden, der Steam Chef kritisierte es als mangelhafte Spieleplattform und schon in der öffentlichen Preview wurde das neue Design von vielen in Frage gestellt. 

Auch Windows Phone macht keinen soliden Eindruck. Generell die Absatzzahlen, dann das Abstoßen von 7.x, was alle aktuellen Windows Smartphones veralten lässt und dazu die tiefroten Zahlen vom Partner Nokia.      
Das sind alles Dinge, die wesentlich schwerer wiegen als Apples mangelhafte Karten App. Was wird Microsoft tun, wenn Apple und langsam auch Android den Tablet-Markt für sich beanspruchen und gleichzeitig die Computer Verkaufszahlen kleiner werden?