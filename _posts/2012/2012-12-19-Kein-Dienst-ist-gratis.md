---
layout: post
title: "Kein Dienst ist gratis"
description: "Nichts ist umsonst, Webdienste auch nicht."
date: 2012-12-19 
category: blog
tags: [Technologie]
published: true
comments: false
share: true
---

Gerade ist die Überraschung groß, weil der von Facebook aufgekaufte Fotodienst Instagram ab Mitte Januar die [Daten seiner Nutzer an Facebook weitergeben kann](http://www.heise.de/mac-and-i/meldung/Instagram-will-Nutzerdaten-mit-Facebook-teilen-1770816.html). 


Das war spätestens seit der Bekanntmachung des Verkaufs sonnenklar. Sicherlich helfen die Instagram-Entwickler auch bei Facebook in Sachen Foto-App, aber dafür hätte man sich ja auch die Programmierer einkaufen können. Doch im Fokus standen die Nutzerdaten, denn irgendwie muss Facebook Geld verdienen.

Eigentlich müsste das mittlerweile jedem bewusst sein: **Kein Dienst ist umsonst**. Generell ist nichts umsonst. Alles kostet etwas und muss entsprechend entlohnt werden. Besonders bei Webdiensten wird das gerne mal vergessen. Doch für Firmen wie Facebook oder Google, gibt es nur zwei Wege um mit ihrer Software Geld zu verdienen. Entweder zahlt der Nutzer ihnen Geld oder jemand anderes.      
Da die Nutzer tendenziell eher sparsam sind, um das mal vorsichtig auszudrücken, bleibt nur letzteres. Oftmals geschieht das durch Werbung, die den Nutzern gezeigt wird. Und je zielgerichteter die Werbung ist, desto wertvoller wird sie für die Unternehmen, so dass man versucht so viele Daten wie möglich über die Nutzer zu sammeln.

## Mehr Informationen = Mehr Geld

Google stellt seine Vielzahl an Diensten nicht aus Nächstenliebe zur Verfügung. Sie dienen dem Zweck mehr Informationen über den Nutzer herauszufinden und damit bessere Werbung zu machen, ob nun für den Kunden oder für den Werbenden, das ist Ansichtssache. Das ist auch in Ordnung und beispielsweise ich nutze die Dienste von Google sehr gerne.      
Ich fände es gut, wenn dieser Deal mehr Menschen bewusst wäre.
Es würde sicherlich nicht bedeuten, dass Google Millionen von Nutzern verliert, denn dafür sind die Dienste einfach zu gut und kostenlose oder kostengünstige Alternativen gibt es meist nicht, aber es würde ihnen einfach mal bewusst sein.

So ist es fast mit allen kostenlosen Diensten. Sei es Facebook, Twitter, Instagram oder Google. Einerseits leben diese Firmen davon, dass viele Nutzer ihren Dienst nutzen, andererseits müssen sie irgendwie Geld verdienen. Dabei gibt es auch Zwitter wie Dropbox, die man relativ kostenlos und werbefrei, aber auch kostenpflichtig mit mehr Speicherplatz nutzen kann.       
Wie sich die verschiedenen Formen der Bezahlung entwickeln, wird interessant sein zu beobachten. Ein Vorreiter ist zum Beispiel [App.net](http://en.wikipedia.org/wiki/App.net), eine Art kostenpflichtiges Twitter. Die Entwickler haben sich ganz bewusst für dieses Businessmodel entschieden, um zum einen die Nutzerdaten nicht zu missbrauchen und zum anderen einen unabhängigen Dienst zur Verfügung zu stellen. 

Twitter ist in letzter Zeit in der Kritik, immer verschlossener zu werden. Da sie nur durch Werbung Geld über den eigenen Vertriebskanälen (sprich ihre eigenen Apps und ihre Website) verdienen können, schränken sie den Zugang zu ihrer API ein. So kann ein Twitter-Client für Windows 8 schon nicht mehr genutzt werden, da diese Anzahl überschritten ist.      
App.net versucht das zu verhindern und stattdessen viele, kreative Entwickler für die eigene Plattform zu begeistern und tolle Apps zu schreiben. 5$ im Monat bzw. 36$ im Jahr sind zwar keine Unkosten, aber ich warte erst einmal vorsichtig ab, wie sich der Spaß entwickelt. 

## Richtung der Entwicklung

Insgesamt stellt sich die Frage: Wohin geht die Richtung? Die Datensammelwut der Unternehmen ist wahrscheinlich nur bedingt rechtens. Was gesammelt und wie genutzt wird, ist meist unklar. Doch ist das für die meisten Nutzer von Bedeutung? Zwar rebellieren immer mal wieder einige Facebook-Nutzer, aber das eigene Konto deaktivieren die wenigsten. 

Ob sich demnächst die Akzeptanz für das Bezahlen von Diensten in unserer Gesellschaft einstellt, kann auch bezweifelt werden. Ich glaube es wäre ein großer Gewinn, wenn jedem bewusst wäre, wem er was preisgibt. 