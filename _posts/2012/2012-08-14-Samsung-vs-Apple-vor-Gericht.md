---
layout: post
title: "Samsung vs. Apple vor Gericht"
description: "Wieso es egal ist, wie der Prozess ausgeht und warum Samsung sich schon längst blamiert hat."
date: 2012-08-14 
category: it
tags: [Design, Apple, Samsung]
published: true
comments: false
share: true
---

Diese ganzen Patentkämpfe sind ein *pain in the ass*, wie es Tim Cook so schön sagte. Ich denke die Industrie muss grundsätzlich etwas ändern, aber bevor das nicht geschehen ist, können wir gespannt den aktuellen Prozess zwischen Samsung und Apple in Amerika verfolgen. 


Der Kern des Prozesses ist die Frage, ob Samsung Design von Apple kopiert hat. Es geht nicht um technische Patente sondern um die Gestaltung von mobilen Geräten, Industriedesign.

Es ist unbestritten, dass das iPhone einen großen Einfluss auf andere Mobiltelefone hatte und sicherlich immer noch hat. Die Geschworenen müssen im aktuellen Prozess entscheiden, ob dieser Einfluss eher geringfügig war und die Designentwicklung sowieso in diese Richtung ging, oder ob zahlreiche Merkmale des iPhones einfach nur kopiert wurden.

Ich habe die Unterschiede des Mobiltelefondesign in den letzten sechs Jahren nicht detailliert verfolgt, aber die Informationen über den Prozess lassen Samsung in keinem besonders guten Licht dastehen.

## Samsung verwehrt Lizenzabkommen

Am meisten hat mich Samsungs Ablehnung gegenüber eines Lizenzabkommen mit Apple gewundert. Da Apple grundsätzlich so gut wie keine internen Informationen an die Öffentlichkeit lässt, wirkt die Firma ein wenig kühl und arrogant. Taucht mal wieder eine Patentklage von Seiten Apples in den Medien auf, sieht man vor seinem geistigen Auge den unbeliebten Nachbarsjungen, der zu seiner Mutti rennt um zu Petzen. Der Hintergrund dieser Klagen ist meist nur aus anwaltlichen Dokumenten ersichtlich und zeigt auf jeden Fall kein vollständiges Bild der Situation.

Im Gegensatz dazu erfuhren wir nun, dass Apple keinesfalls immer die Patentpetze ist, sondern durchaus Angebote macht und mit den anderen Firmen kommuniziert. Das gebe ich zu, hat auch mich positiv überrascht.

Das Angebot in diesem Fall war eine Gebühr von 30$ pro Mobiltelefon bzw. 24$ wenn Samsung ebenfalls Patente teilt. Ob diese Summe angemessen ist oder nicht, können wir Laien vermutlich nicht einschätzen. Aber zur Erinnerung: Wir Deutschen müssen von jedem Smartphone 36€ an die Gema abdrücken. Ernsthaft.

Aus wiederum anderen internen Dokumenten von Samsung wurde deutlich, dass man sehr wohl wusste, dass das Design des iPhone gut gelungen war und dass man deshalb das eigene Design dahingehend anpassen muss. Die entsprechenden Folien mit dem ausführlichen Vergleich des ersten Galaxys sind im Netz legal zu finden und sorgten zumindest bei mir für großes Erstaunen.

Apple argumentiert, dass es in der Entwicklung des Designs bei Samsung einen Bruch gab, nachdem das iPhone vorgestellt wurde. Es sei nicht so, dass sie nicht an Touchscreen-Smartphones gearbeitet hätten und welche herausbrachten. Aber ab einem bestimmten Zeitpunkt sahen ihre Smartphones dem iPhone sehr ähnlich, nicht nur äußerlich, sondern auch die Software mit Icons und allem drum und dran.

Wenn man bei Samsung also wusste, dass das iPhone gutes Design darstellte und sehr detailliert die eigenen Geräte anpasste, wieso konnte man sich nicht mit ihnen einigen? Stattdessen wurde von ihnen weiterhin unlizenzierte *Inspiration* eingeholt, auch im Tablet-PC Bereich.

## Ist Samsung arrogant?

Ein Bekannter hat mal in einer deutschen Niederlassung von Samsung gearbeitet. Er schilderte mir einige Eigenheiten der Koreaner und das waren nicht besonders positive. Sicherlich ist das alles persönlich und nicht 100%ig objektiv, aber was ist schon objektiv? Beispielsweise Samsungs Umgang mit Journalisten ist professionell, aber was sagt das schon aus?

In Südkorea ist Samsung eine gewaltige Grösse. Der Familienkonzern erstreckt sich auf zahlreiche Gebiete und hat überall seine Finger im Spiel, auch politisch. Diese Überlegenheit spiegelt sich in Arroganz wieder und die ist gegenüber Europäern wohl deutlich zu spüren. Der Satz *Hinter jedem deutschen Samsungmanager steht ein Koreaner.* bringt es auf den Punkt.

Man kann jetzt darüber streiten, in wie fern dieses Verhalten für einen Grosskonzern verständlich ist. Aber nehmen wir an, dass in ihrer Grundhaltung so eine gewisse Überlegenheit ist, mischen dazu ein wenig Neid und Missgunst auf den neuen Spieler im Smartphone-Markt, und schon haben wir eine wunderbare ablehnende Haltung gegen jegliche Kompromisse oder Zugeständnisse.

Das Verhalten der Anwälte von Samsung verstärkt den Eindruck von Arroganz. Oder wie will man es sonst nennen, wenn trotz des richterlichen Verbots Dokumente an Medien gegeben werden, welche sie natürlich veröffentlichen. Es kann ein Versehen der Pressearbeit gewesen sein oder eiskalte Kalkulation.

## Apples Anstrengungen

Jedem Menschen wurde irgendwann schon einmal irgendwas weggenommen. Meist sind es materielle Güter, aber eine geklaute Idee kam sicherlich auch schon einmal vor. Je nachdem wie viel Mühe in eine Idee oder ein Produkt gesteckt wurde, desto wichtiger ist es einem, ganz klar.

Spulen wir die Zeit einige Jahre zurück. Apple hat seinen ersten richtig großen Erfolg mit einem tragbaren Musikplayer: dem iPod. Nach einigen Jahren ist genügend Kapital vorhanden um in andere Gebiete zu expandieren und Apple wählte den Smartphone-Markt. Es ist unbestritten, dass Apple sich eben nicht nur die damals aktuellsten Flaggschiffe der Konkurrenz genommen und sie *verbessert* hat. Sie haben das gesamte Konzept vom Smartphone und tragbaren Computer neu erdacht und entwickelt.

Natürlich konnte das erste iPhone auf dem Papier nicht viel mehr als die Smartphones der damaligen Zeit, aber das *Wie?* war eine völlig andere Welt. Programme gab es schon immer auf Mobiltelefonen. Kleine Spiele, Foto und Multimedia Funktionen gibt es eine ganze Weile, aber die Art und Weise wie sich diese nach dem Erscheinen des iPhones geändert haben, ist eben keine natürliche Entwicklung.

Da kam eine neue Firma und hat viel Mühe und Investition in die Entwicklung neuer Konzepte gesteckt. Es war für Apple ein riskantes Vorhaben, denn sie betraten einen neuen Markt, hatten weniger Erfahrung und ein Scheitern, aus welchen Gründen auch immer, wäre nicht das erste Mal für sie gewesen.
Man sollte sich mal in die Lage der Verantwortlichen bei Apple versetzen. Das Smartphone wird vorgestellt und wurde, sicherlich auch für Apple überraschend, ein enormer Erfolg. Wenige Monate später bringen die Konkurrenten Smartphones auf den Markt, die in vielen Dingen dem iPhone ähneln. Das bezeugen mittlerweile unzählige Dokumente und Fotos.

## Industriegrößen

Auf Giga drückte ein Kommentator sein Unverständnis über den Prozess mit der Feststellung aus, dass es sich bei Apple und Samsung ja um große Unternehmen handelt und keiner von beiden um seine Existenz kämpfen muss. Aber genau das ist es ja!
Man kann von einem riesigen Konzern wie Samsung doch mehr erwarten als das stumpfe Kopieren. Am Ende des Prozesses steht entweder die Erkenntnis, dass Samsung von Apple kopiert hat oder das Samsung das Design nicht kopiert hat und es nur zufälligerweise wie Apples aussieht. Beides ist für Samsung einfach nur eine Blamage.

Dass es anders geht, zeigt das Patentabkommen zwischen Apple und Microsoft. Es ist nichts genaues bekannt, nur dass Microsoft keine Klone von Apple-Produkten bauen darf, ganz logisch. Was dabei genau herauskommt werden wir bald sehen, aber auch die Windows Phones zeigen schon, dass nicht alles wie ein iPhone aussehen muss.

Ein aktuelles Beispiel ist auch noch die Benutzeroberfläche von Samsungs SmartTV Entwicklungsumgebung. Wie der Autor des Posts schrieb, ähneln sich Entwicklungsumgebungen immer in verschiedenen Aspekten, auch vom Design her natürlich. Aber wer bestreitet, dass Samsungs UI von Apples XCode 1:1 kopiert ist, hat vermutlich auch die Eigenleistung von Guttenberg in seiner Doktorarbeit gelobt. Samsung buhlt scheinbar um XCode Entwickler und möchte ihnen den Umstieg einfach machen. Auch wenn das gerichtlich keine Folgen haben wird, spricht es eine deutliche Sprache über Samsungs Respekt vor fremden Leistungen.

## Die Zukunft im Blick

Apple geht es nicht primär um eventuell entgangene Gewinne, weil jemand ein iPhone nicht von einem Galaxy unterscheiden konnte. Dass Samsung sich zumindest viel Inspiration von Apple geholt hat, steht fest, aber wie viel davon bewegt sich noch im rechtlichen Rahmen? Die Antwort auf diese Frage wird auch in Zukunft wichtig sein, denn das nächste iPhone steht vor der Tür und ein Fernsehgerät von Apple wird uns sicherlich auch in nicht all zu ferner Zukunft beglücken.      
Wird es ein Erfolg? Und werden die Samsung Fernseher samt Software plötzlich auch einen Bruch in der Designentwicklung haben und dem neuen Apple Gerät ähnlich sehen?

Patentstreitigkeiten sind nicht sympathisch. Aber als Firma muss man sich auch wehren können. Der Streit zwischen Samsung und Apple ist kein David gegen Goliath Kampf. Samsung als Weltkonzern *inspiriert* sich sehr viel mehr von Apples Produkten als andere Konkurrenten, und das auf verschiedenen Gebieten. Dazu kommen die internen Informationen bei Samsung, die diesen Umstand bestätigen.     
All das zusammen wiegt, zumindest in meinen Augen, sehr schwer gegen Samsung. Es sind eben nicht nur runde Ecken und bunte Icons, es geht um die grundlegende Frage nach dem Wert von Design.