---
layout: post
title: "Neues in Mountain Lion und iOS 6"
description: "Die Neuerungen in den kommenden Updates, die mir am besten gefallen."
date: 2012-06-15 
tags: [OS X, iOS]
category: it
published: false
comments: false
share: true
---

Am Montag hat Apple auf der Keynote seiner Entwicklerkonferenz WWDC die Neuerungen in der kommenden Mac OS X Version Mountain Lion und dem im Herbst erscheinenden iOS 6 vorgestellt. Über alle Kleinigkeiten könnte man viele Einträge schreiben und deshalb beschränke ich mich auf die Neuerungen, die ich besonders gelungen, sinnvoll und toll finde.

### Personen kontaktieren

Die beste Neuerung ist meiner Meinung nach das Paaren von Telefonnummern und Kontakten bzw. Apple IDs. Aktuell ist es doch so: Möchte ich jemanden anrufen, wähle ich seine Nummer. Möchte ich jemanden schreiben, schicke ich die Nachricht an seine Email-Adresse. Möchte ich mit jemandem chatten, dann muss er und ich beim gleichen Chat-Programm angemeldet sein und wir müssen uns online treffen.       
Bisher hat man nicht zu einer Person Kontakt aufgenommen, sondern zu einer Nummer, einer Adresse, oder sonst einer abstrakte Repräsentation einer Person.

Apple ändert dies nun in dem die Nummer einer Person mit der jeweiligen Apple-ID verbunden wird. Das bedeutet, wenn ich zum Beispiel einem Freund, der natürlich auch eine Apple-ID hat und seine Telefonnummer mit dieser verbunden ist, eine Nachricht schreiben möchte (wie früher SMS), dann schicke ich die Nachricht an diese Person und nicht mehr an eine Nummer oder Email-Adresse. Je nachdem ob er online ist oder unterwegs, wird die Nachricht als SMS verschickt oder als Textnachricht über das Internet.       
Genauso funktioniert es auch mit FaceTime. Möchte ich mit jemanden einen Videochat machen, erkennt Apple ob derjenige nur sein iPhone dabei hat oder vielleicht gerade mit seinem MacBook online ist. Entsprechend erhält er die Anfrage auf dem jeweiligen Gerät.

Der einzige Haken an der Sache: Die Kontakte müssen auch bei Apple angemeldet sein. Aber der Ansatz ist auf jeden Fall sehr gut und es wäre natürlich richtig cool, wenn es irgendwann auch plattformübergreifend funktionieren würde mit Google oder Microsoft. Man wird ja wohl noch träumen dürfen.

## iCloud für alle

Sehr bequem wird die systemübergreifende Integration der iCloud. Dropbox ist ja gut und schön, aber die Art und Weise wie Apple seinen Cloud-Dienst in allen Mac-Produkten zur Verfügung stellt ist fantastisch.       
Die meisten Apple Programme arbeiten mit der iCloud, das bedeutet, dass alles synchronisiert wird. Ob es Emails, Kalender-Einträge, Notizen, Musik oder Kontakte sind. Bearbeite ich ein Dokument auf meinem MacBook, kann ich es wenige Augenblicke später auf meinem iPad weiterbearbeiten. Die entsprechenden APIs sind auch für Entwickler verfügbar und es werden demnächst hoffentlich viele Apps nachziehen.

Auch einige Apple-Apps sind für die iCloud verbessert worden, wie z.b. die Reminder App, die eben Erinnerungen auf alle Geräte synchronisieren kann und auf allen Geräte auch Ort-abhängig reagiert. Sage ich der App, sie soll mich im Edeka daran erinnern, dass ich noch Salat kaufen soll, ploppt im Edeka die entsprechende Nachricht auf meinem iPhone auf.       
Und nun kann ich diese Erinnerungen auf dem Mac schreiben und mit dem iPhone empfangen.

Auch eine coole Erweiterung ist der neue Photostream. Wenn ich auf meinem iPhone Fotos schieße, landen diese u.a. im Photostream, der auf all meinen iGeräten synchronisiert wird (und immer die letzten 1000 Fotos enthält). Nun kann ich auch weitere Photostream-Alben mit Freunden teilen, in welchen jeder der Album-Inhaber Fotos reinwerfen und kommentieren kann.      
Bisher hätte ich das entsprechende Foto entweder irgendwo hochladen und den Link oder gleich das Bild schicken müssen. 

## Siri lernt fleißig

Im Laufe der nächsten 12 Monate werden diverse Autohersteller einen Siri-Button am Lenkrad einbauen, mit dem man sein iPhone über die Stimme steuern kann. Das betrifft mich persönlich nicht weiter, aber es ist eine ziemlich interessante Entwicklung.

Siri wird mächtiger! Endlich. Sie wird Informationen besser erfassen und wiedergeben, Fragen besser beantworten und auch endlich Apps öffnen und steuern können. Apple stellt eine Siri API bereit und ich denke, da werden richtig gute Dinge entstehen. Siri wird von einer Beta-Version zu einer fertigen, fast schon *Star-Trek-Approved* sozusagen. 

Auf dem neuen iPad wird Siri auch integriert sein und das auch in mehreren Ländern. Das wird die ganze Sprachsteuerung allgemein ziemlich vorantreiben. Ob die Firma hinter Siri jemals gedacht hätte in so kurzer Zeit so viele Daten zu erhalten?

Es tut sich auch wieder eine Frage auf: Wo bleibt die Konkurrenz? Samsungs S-Voice ist ja nicht gerade so toll. Google hat zwar eine Sprachsteuerung, aber das ist eben eine Sprachsteuerung. Siri geht viel mehr in diese Star Trek Richtung und hat einfach eine bessere Usability.
Teile dieses Eintrags habe ich übrigens per Diktierfunktion (die in iOS 6 erweitert wird) gesprochen und nur noch ein wenig überarbeitet. 

## Weiteres

Dass Facebook tief ins System integriert wird, ist mir nicht so wichtig. Aktuell grüble ich noch darüber nach, in welche Richtung Facebook gehen wird.      
Apples Maps Anwendung werde ich sicherlich auch mal nutzen. Die Maps App auf dem iPhone ist eher rustikal, da wird es nicht so schwer sein etwas eigenes zu zaubern. Viel interessanter wird Googles Konter sein. 

Im Großen und Ganzen sind es viele, viele kleinere Neuerungen, die das ganze Apple-System unglaublich angenehm zu benutzen machen werden. Es gibt ja keine großen Baustellen mehr, wie vor einigen Jahren das Cut&amp;Paste Problem. John Gruber schrieb letztens darüber, dass man als Apple-Benutzer keine dringenden Wünsche mehr hat. Die Kritikpunkte sind nur noch Meckern auf hohem Niveau.     
Je mehr dieser kleinen, coolen Dinge Apple umsetzt, desto mehr setzen sie sich auch von ihrer Konkurrenz ab.

**Anmerkung:** Zarte Gemüter bekommen bei den ganzen Datendiensten eventuell Pickel vor lauter Datenschutz-Angst. Mich tangiert das weniger. Ich passe auf, dass wirklich sensible Daten nirgendwo in der Cloud gespeichert werden und ansonsten genieße ich die Bequemlichkeit des Fortschritts. Jemand muss es ja tun.
