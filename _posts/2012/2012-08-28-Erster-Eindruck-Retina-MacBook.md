---
layout: post
title: "Erster Eindruck vom Retina MacBook"
description: "Meine ersten Stunden mit dem neuen Retina MacBook."
date: 2012-08-28 
category: it
tags: [MacBook]
published: true
comments: false
share: true
---

Meine Finger berühren in diesem Moment die Tasten eines äußerst dünnen MacBook Pro mit 15 Zoll Retina Bildschirm und schon offenbart sich ein erster Unterschied zu meinem alten MacBook Pro im 13 Zoll Format: Mehr Platz neben der Tastatur, so wie ober- und unterhalb des Trackpads. Das war natürlich abzusehen, aber ich hatte nicht erwartet dass meine Handballen den zusätzlichen Platz rechts und links zu schätzen wissen.

Ich schreibe hiermit meine ersten Eindrücke von diesem beeindruckenden Gerät auf. Zur Technik verliere ich nur wenige Worte, denn das dürfte alles bekannt und besprochen sein. Ich habe die Variante mit 500 GB SSD Speicher und dem um 0,3 GHz schnelleren i7 QuadCore-Prozessor (also ein 2,6 GHz, der wohl bis auf 3,6 GHz hochtakten kann). Und natürlich vergesse ich nicht das Retina-Display mit wahnsinnigen 2880 x 1800 Pixeln. Retina!

Das Gerät ist erst seit einigen Stunden bei mir und somit ist das folgende in der Tat ein erster Eindruck. Überraschenderweise ist noch Mac OS X Lion installiert. Das Upgrade werde ich demnächst beantragen, aber eventuell auf das nächste Update warten damit die Akkulaufzeit wieder normal ist.

Das MacBook ist wirklich dünn und leicht. Obwohl es nur 40 Gramm leichter als mein 13er ist, wirkt es insgesamt eleganter und eben leichter. Die oder der Bezel, also der schwarze Rahmen um das Display, sind dünn und wie schon bekannt ist, hat der *MacBook Pro*-Schriftzug nicht mehr drunter gepasst. Angenehm flach ist generell die Oberseite und der Bildschirm. Das kommt zwar zu Lasten der Reparaturfähigkeit, doch das ist zumindest für mich völlig okay.      

Der  Bildschirm lässt sich angenehmer kippen als beim MacBook Air. Bei letzteren rutscht meist das ganze Geräte mit, was seinem Gewicht geschuldet ist. Die knapp 700 Gramm zusätzlich beim Retina geben einen besseren Halt.      
Eine weitere tolle Neuerung: Die Kante ist nicht mehr so scharf wie beim alten 13er. Schon das zweite Mal heute, dass sich meine Extremitäten bedanken.

Nun aber zum wichtigsten Feature: Das sagenumwobene Retina Display. Wenn der Mac das erste Mal startet, schaut man etwas ernüchtert auf den Bildschirm, zumindest bis man den ersten Schriftzug sieht. Ab diesem Moment ahnt man langsam, wie der Unterschied ausfallen wird.      
Hat man nicht gerade seine Fotos oder Grafikprogramm zur Hand, erkennt man vor allem an der gestochen scharfen Schrift, dass dass MacBook eine tolle Pixeldichte von 220 ppi hat. Die Systemfenster und Browser (Safari und Chroms) laden zum Lesen ein.

Wirklich unschön sehen Programme aus, die noch nicht angepasst wurden. Bei mir sind das als erstes Twitter, Adobe Reader, und die Dashboard-Version von iStat gewesen. Generell sind meist die Installationsfenster unscharf. Ich schüttelte immer mal wieder den Kopf wenn ich in den ersten Reviews von der Kritik las, dass ja noch nicht so viele Programm angepasst wurden.      
Das ist zwar keine Überraschung, hat aber wirklich bittere Auswirkungen, denn die non-Retina Programm sehen einfach nur Scheiße aus. Dass Chrome beispielsweise in dieser Hinsicht schon zu Safari aufgeschlossen hat, ist ein ziemliches Fail von Firefox, den würde ich zur zeit garantiert nicht nutzen, so wie er Schrift darstellt.

Ansonsten habe ich einen Haufen Programme und Updates installiert, da blieb kaum Zeit um eine größere Anzahl anzutesten. Eine Seltsamkeit habe ich aber schon gefunden: Eclipse besitzt in der ersten Instanz die hässliche, pixelige Schrift, aber in der zweiten Instanz ist plötzlich die Retina-Schrift da. Dem gehe ich noch nach.

Was ist mir sonst noch aufgefallen? Diablo 3 sieht sehr gut aus, wobei man den Lüfter hört, welcher wiederum aber in der Tat nicht zu laut ist.     
Ich finde die USB-Anschlüsse an beiden Seiten des Macs gut.      Das DVD-Laufwerk vermisse ich überhaupt nicht.     
Es gibt scheinbar noch keine guten Schutzhüllen. Da warte ich jetzt noch sehnsüchtig drauf.     
Installationen und diverse Programmstarts wurden vom Mac mit der Geschwindigkeit des Lichts ausgeführt. Das fühlt sich sehr gut an.