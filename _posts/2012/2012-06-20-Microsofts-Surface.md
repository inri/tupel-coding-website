---
layout: post
title: "Microsofts Surface"
description: "Microsoft hat endlich sein erstes Tablet namens Surface vorgestellt."
date: 2012-06-20 
category: it
tags: [Microsoft, Surface]
published: true
comments: false
share: true
image: 
  feature: 2012-surface-lg.jpg
  credit: Microsoft
  creditlink: http://microsoft.com
---

Gestern Nacht stellte Microsoft seinen ersten Tablet-Computer, genannt Surface, auf einem mysteriösen und kurzfristigen Event vor. Das Surface-Tablet hat MS selbst entwickelt. Man könnte fast sagen, sie haben endlich begriffen, dass auch außerhalb vom Spielekonsolen-Markt eine gemeinsame Entwicklung von Soft- und Hardware zu den besten Ergebnis in Sachen Bedienbarkeit führt.

Das Event, eröffnet von Steve Ballmer, beschrieb die Vergangenheit von Windows-Hardware. Dass MS stets dann eigene Hardware entwickelte, wenn ihnen bewusst war, dass sie besonders gut für den Benutzer sein muss. Diese Aussage wird einigen Herstellern von Windows Hardware nicht gefallen.       
Windows 8 wird das Windows-System komplett verändern und braucht deshalb auch eine eigene Hardware, logisch. Die zahlreich während der Präsentation verwendeten Rechtecke ließen nur einen Schluss zu: ein iPad von Microsoft. Egal.

**Surface**. Nein, nicht der gigantische [Touch-Tisch](http://de.wikipedia.org/wiki/Microsoft_PixelSense). Der musste seinen Namen abgeben, heißt nun PixelSense und Surface ist eine neue Gerätegattung. Ein völlig neues Gerät, laut Ballmer. Ein Tablet? Ja, aber ein Gerät auch für Profis. Das war MS auf diesem Event besonders wichtig, es ist nicht nur ein Tablet, es ist mehr als das. Das Surface soll einen Computer ersetzen können, was ein iPad angeblich nicht kann (indirekt behauptet). 

## Die Technik

Die Eckdaten sind nicht besonders spektakulär. So ähnlich groß, schwer und vermutlich auch leistungsstark wie ein iPad, zumindest die ARM-Version des Surface. Diese bekommt zwar MicroSD, aber nur USB 2.0. Dafür gibt es keine lächerliche 16 GB-Größe wie beim iPad, sondern 32 GB und 64 GB spendiert. 
Interessant wird die Akkulaufzeit. Das Surface hat einen 31,5 Wh Akku, im Vergleich dazu das neue iPad üppige 42,5 Wh, womit es aber auch ein Retina Bildschirm befeuern muss. Das iPad zweiter Generation kam mit 25 Wh gute 10 Stunden aus. Der 10,6 Zoll Bildschirm ist zwar größer als beim iPad (9,7 Zoll) wird aber für den Akku vermutlich nicht viel zu Buche schlagen. Weniger als es die verbauten MicroSD- und USB-Anschlüsse tun. Bekanntermaßen aus Stromspar-Gründen ließ Apple diese weg. Mal sehen welche Auswirkung das hat.

Erscheinen wird das Surface erst mit Windows 8, also irgendwann im Herbst. Die auf einen Intel i5-Chip basierende Surface-Version kommt nicht nur später, sondern ist auch schwerer (900 g) und dicker (13,5 mm) als das ARM-Surface. Dafür hat es statt HD sogar einen FullHD Bildschirm, kommt damit aber auch nicht am Retina des iPads heran.      
Der bessere Bildschirm bekommt neben der CPU noch weitere Anschlüsse als potentielle Akkufresser: MicroSDXC, USB 3.0 und einen Mini Display Port. Super, dann kann man die Apple-Adapter gleich zweitverwerten. Aber warum gibt es kein HDMI? Lizenzgebühren, Akku oder Platzgründe?   

Der Speicher ist auch höher als beim ARM-Bruder: 64 GB oder 128 GB. Da müsste Apple mit dem nächsten iPad auch nachziehen, besonders wenn viele Apps auf Retina aufrüsten. Wobei 128 GB für die angesprochenen professionellen Anwender auch grenzwertig sind. 

## Zubehör

Fast genauso viel Zeit wie für das Surface an sich, wurde dem erhältlichen Zubehör gewidmet. Größter Hingucker ist wohl das nur 3 mm dicke TouchCover, auf welchem es sich besser Tastatur-tippen lässt als auf dem Bildschirm und man letztlich auch mehr vom Bildschirm hat.      
Ebenfalls interessant ist das dickere TypeCover mit fast richtiger Tastatur. Man merkt, Microsoft hat sich viele Gedanken zum Tippen gemacht. Meine Bedenken schildere ich später. Vorerst soll das dritte Zubehör erwähnt werden: Ein Eingabestift der magnetisch am Gehäuse heftet, aber nur am Gehäuse des Intel-Surface. 

Als ich das Event verfolgte, dachte ich, das Gehäuse hat einen eingebauten Standfuss. In einer Vergleichstabelle las ich unter *Zubehör* das VaporMG Case, welches aber wohl das Gehäuse ist. Keine Ahnung wie ein Gehäuse Zubehör sein kann, aber das scheinbar sehr schicke aus einer Magnesium-Legierung gefertigte Gehäuse ist inklusive. Mit praktischem Standfuss.
Dass die Tastatur-Cover per Magnet am Gehäuse haften und einige Zeitgenossen auf den Apfel schielen, geschenkt. Wer eine bessere Idee für eine elegante Halterung hat, der möge sich erheben. Oder für immer schweigen.

## Zielgruppe

Das Surface zielt ganz klar auch auf professionelle Anwender, das hat MS mehrmals beim Event betont. Man wolle kein Entertainment-Tablet anbieten, sondern eines, auf das man auch produktiv arbeiten kann. Bei solchen Aussagen schwingt immer die Behauptung mit, das ginge ja bei anderen Tablets, und damit meine ich das iPad, gar nicht.       
Ich denke, das Surface hat den Vorteil, dass unter der Oberfläche immer noch ein Windows läuft, mit welchem man eben Windows-Programme benutzen kann. Für iOS erscheinen allmählich erste Apps für professionellere Anwendungsfälle und auch Apple arbeitet daran, dass sich eine große Anzahl iPads leichter in eine Firma integrieren lassen. 

Viel wichtiger für das Surface wird es sein, dass eben auch nicht-professionelle Apps auf dem Gerät gut laufen. Ich glaube kaum, dass die Profis, die das iPad gemieden haben, sich plötzlich auf das Surface stürzen. Es wird erst abgewartet, wie realistisch Microsofts Versprechen in dieser Hinsicht ist. 

Auch Microsofts Informationspolitik ist wirklich richtig schlecht. Wie kann man ein neues, tolles Gerät ankündigen, **ohne** die genaue Verfügbarkeit und den Preis zu verraten? 
Auch über Apples Events kann man sich streiten, aber wenn sie etwas vorstellen, gibt es das auch innerhalb weniger Tage oder Wochen zu Kaufen. Warum die Entscheidung für die gestrige Veröffentlichung? Habt ihr ein fertiges Produkt, dann bringt es verdammt noch mal heraus. Es gibt genug Windows-treue Menschen, die sich das kaufen wollen. Wenn ihr kein fertiges Produkt habt, wieso wartet ihr nicht noch mit der Vorstellung? 
Es wurde zwar im Vorfeld über ein Tablet gemunkelt, aber konkrete Informationen gab es nicht. Das Geheimnis kann man doch einige Wochen bewahren.
 
## Meine Meinung

Ja, meine Meinung liest man mehr oder weniger schon zwischen den Zeilen, aber ich kann einige wichtige Punkte noch mal aufgreifen. Ich bin auf jeden Fall neugierig. Im Gegensatz zu den ganzen Android-Tablets, habe ich erstmals das Gefühl, dass da etwas interessantes kommen kann. Das Gerät sieht gut aus, man merkt, dass sich MS viele Gedanken über das Design gemacht hat. Das ist eine Erkenntnis, die erst so langsam in das Bewusstsein der Hersteller sickert.       
Der Benutzer möchte ein Gerät, dass sich einfach gut anfühlt. Der MS-Designer hat genau dieses Gefühl beschrieben: Das Surface soll sich so angenehm anfühlen, dass der Benutzer es vermisst, wenn er es eine Weile weglegt. Einige Menschen sind gegenüber solcher Ästhetik resistent, aber viele Menschen reagieren darauf. 

Ich hatte viele Jahre einen normalen PC, dann einen eeePC und auch mal ein Notebook benutzt. Dazu mal Nokia, mal HTC als Smartphone. Ein Produkt von Apple das erste Mal in die Hand zu nehmen, fühlt sich einfach wertiger und besser an, als all diese Geräte. Ich bin überzeugt, dass das nicht nur Apple kann und bin deshalb gespannt auf das Surface.

Die technischen Daten sehen okay aus. Ich denke die ARM-Variante wird bei 350€ bis 500€ angesiedelt sein, ansonsten wird es schwer mit dem iPad mitzuhalten, besonders da beide Surfaces auch kein hochauflösenden Bildschirme haben. Dass die Intel-Variante schwerer und dicker ist, finde ich nicht so gut. Beim neuen iPad kritisierten einige, dass es schwerer ist und weniger gut zu halten und dabei Betrug die Gewichtsdifferenz nur 50 g.        
Mit 100 g mehr, kommen wir in die Gewichtsklasse der MacBook Airs und vielleicht auch einiger Ultrabooks, da kenne ich mich aber nicht aus. 

Wie oben schon erwähnt, finde ich die Fixierung auf die Tastatur problematisch. Apple ging sicherlich sehr rigoros vor, in dem es es sich von fast allen Tasten verabschiedet hat. Damit zwang es aber auch sich selbst und Entwickler, das Design von Programmen grundlegend zu überdenken und anzupassen. Microsofts Tastatur-Cover wirken auf mich, wie als würden sie selbst nicht so richtig an die Touch-Bedienung ihres Surface glauben.       
Ich dachte vor zwei/drei Jahren, die [Convertibles](https://www.google.de/search?hl=de&q=convertable&ion=1&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.,cf.osb&biw=1280&bih=702&um=1&ie=UTF-8&tbm=isch&source=og&sa=N&tab=wi&ei=8hDhT820EMWyhAeRnKW3DQ#um=1&hl=de&tbm=isch&q=convertible+pc&revid=321091308&sa=X&ei=9hDhT5r8HIfB0gXkqeDfDA&ved=0CBIQgxY&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.,cf.osb&fp=83cb67b8356cacd8&biw=1280&bih=702) würden eine große Verbreitung finden. Aber die Touchscreen-Laptop-Zwitter fristen immer noch ein Nieschendasein. Sie sind nicht richtig *touchig* oder zumindest ist es Windows 7 nicht. Ich hoffe, das Surface gesellt sich nicht dazu, denn es wäre wirklich schade um das schicke Gerät. 

Technik hin oder her, die größte Hürde wird das neue System in Form von Windows 8 darstellen. Das System und die möglichen Apps, die auf einem Tablet gut laufen und aussehen. Das iPad hatte anfangs auch einige App-Probleme, die Apple clever löste, in dem es zwar die iPhone Apps skalierte, aber die skalierten Apps so schlimm aussahen, dass die Entwickler sich schämten und die Apps schnell anpassten.             
Aktuell ist Microsofts Strategie in Sachen Tablets- und Mobiltelefon-Entwicklung noch unbekannt, aber wenn sie in 2 bis 3 Monaten Windows 8 und erste Tablets auf den Markt werfen wollen, sollte sich da langsam was tun. 

Die Entwickler-Konferenz steht zum Glück vor der Tür.     
Eine spannende Zeit.