---
layout: post
title: "Wunderlist 2 statt Wunderkit"
description: "Die 6Wunderkinder stellen Wunderkit zu Gunsten von Wunderlist 2.0 ein. Richtig so."
date: 2012-09-07 
category: it
tags: [App]
published: true
comments: false
share: true
---

Das Team der 6Wunderkinder [hat beschlossen](http://www.6wunderkinder.com/blog/the-future-of-6wunderkinder-hello-wunderlist-2) die Entwicklung an ihrem Wunderkit einzustellen und stattdessen den Fokus auf eine neue, verbesserte Version der Wunderlist zu richten. Das ist eine gute Entscheidung.

Der Grund war schlicht und einfach das mangelnde Interesse von Seiten der Nutzer und leider auch der Entwickler. Das Team konzentrierte sich einerseits auf Wunderlist und andererseits zeitgleich auf die Entwicklung von Wunderkit. Das führte zu einigen Fehler, die sich erst später rächten. Aber nicht die Softwarefehler waren der Grund für das geringe Interesse, sondern einfach kleine Fehler in der Benutzung. Wunderkit war einen Tick zu kompliziert. Die Downloadzahlen sanken schnell wieder und ein Erfolg wie bei Wunderlist, welches immer noch oft geladen wird, stellte sich einfach nicht ein.

Das Team überlegte, ob ein Redesign helfen könne, aber sie entschieden sich richtigerweise für den beliebteren Dienst. 
Ich muss zugeben, ich habe Wunderkit auch nur zu Beginn getestet. Das lag aber weniger daran, dass ich es nicht gut fand, sondern schlicht und ergreifend an der Tatsache, dass ich zu diesem Zeitpunkt kein Projekt zu Organisieren hatte. Wobei das auch nur die halbe Wahrheit ist. 

Ja, ich hätte das eine oder andere kleine Projekt verwalten können, aber Wunderkit bedeutete für mich keine große Verbesserung in der Organisation dieser kleinen Projekte. Es war vermutlich dieses bisschen zu viel Komplexität, die es zu keiner Arbeitserleichterung für mich machte. Es fühlte sich einfach nicht so simpel wie Wunderlist an.       
Wunderkit soll aber weiterhin als Beta existieren und deshalb habe ich es noch einmal installiert und schau es mir ein weiteres Mal an. 

Grundsätzlich finde ich die Entscheidung von den 6Wunderkindern gut. Sie haben die harten Fakten in Form von Downloadzahlen und Nutzermeinungen auf den Tisch gelegt und vernünftig entschieden. Wobei ich anmerken würde, dass mit einem Projektverwaltungsservice sicherlich nie vergleichbar hohe Nutzerzahlen erreicht worden wären, wie mit der Wunderlist. Die Zielgruppe ist einfach zu gering. Es geht also zurück zu den Wurzeln und ich bin sehr gespannt auf Wunderlist 2.

Hochinteressant finde ich ihre Entscheidung nicht mehr mit der Cross-Plattform-Dienst Titanium zu entwickeln. Damit schreibt man Code in Javascript und kann ein Programm für verschiedene Plattformen exportieren. Die Wunderkinder wollen für alle Plattformen separat und mit den entsprechenden nativen Werkzeugen entwickeln.
 
Ich denke auch, dass man die beste App für jede Plattform einzeln schreiben muss.