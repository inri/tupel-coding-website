---
layout: post
title: "Wieso es Patentstreitigkeiten gibt"
description: "Das Patentwesen funktioniert heutzutage nicht mehr."
date: 2012-10-09 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Wer etwas mehr über die aktuellen Patentstreitigkeiten erfahren möchte, dem lege ich [diesen längeren Artikel](http://www.nytimes.com/2012/10/08/technology/patent-wars-among-tech-giants-can-stifle-competition.html?_r=1&) aus der New York Times ans Herz. Er schildert die Entwicklung der letzten Jahre, insbesondere auch in Bezug auf Apple.

Kurz zusammengefasst: Das Patentrecht ist großer Mist. Die ursprüngliche Idee mag sinnvoll gewesen sein, aber mittlerweile wirken Patente hinderlich. Wichtig ist die Feststellung, dass das Patentsystem in der heutigen Zeit und besonders bezogen auf Software nicht mehr funktioniert.

In den Medien werden die Unternehmen kritisiert, die ihre Patente schützen und vor Gericht ziehen. Das ist meistens auch etwas zu kurz gedacht. Da gibt es zum einen die Patent-Trolle. Das sind Firmen, die nur Patente aufkaufen um dann andere Firmen, die diese Patente verletzten, zu verklagen. Das sind einfach nur widerliche Zeitgenossen. Zum anderen gibt es aber auch Firmen wie Apple und Google, die tatsächlich ihre Erfindungen schützen wollen.       
Apple fuhr die Patent-Maschinerie in 2006 mit dem Erscheinen des iPhones hoch. Steve Jobs wollte einfach alles am iPhone patentieren lassen und seit dem wird für jede Kleinigkeit bei Apple ein Patentantrag gestellt.

Bei Apple entschied man sich nicht für dieses Vorgehen, weil man so gemein ist oder es Spaß macht, sondern weil Apple auch dazu gezwungen wurde. Irgendwie. Sie zahlten an eine Firma aus Singapur 100 Mio. $ um sich bzgl. deren Patents eines *portable music playback device* zu einigen. Die Firma erhielt dieses Patent im Jahre 2001, bevor der iPod vorgestellt wurde.       
Weil Apple nicht schon früher Aspekte des iPods hat patentieren lassen, wurden sie eben verklagt. Damit sich das mit weiteren Technologien nicht wiederholt, begann man bei Apple das große Patentieren. Im Gegenzug mussten auch andere Firmen ihre Anstrengungen vergrößern. Ein Teufelskreis.

Die Frage, die sich nun stellt: Wie kann man das System verändern, so dass es wieder sinnvoll ist? Patente auf Softwarekonzepte generell zu verbieten, ist vermutlich nicht die beste Lösung. Aktuell kann bereits die Idee oder ein Konzept patentiert werden, wohingegen früher derjenige das Patent bekam, der den ersten Prototypen gebaut hat.      
In der Diskussion ist auch eine zeitliche Beschränkung und das wäre auch mein Favorit. Software-Patente sind nur eine begrenzte Zeit geschützt und danach kann sie jeder nutzen. 

Was auch immer sich verändern wird, es muss sich etwas verändern.

**Nachtrag:** Ein Patent zu beantragen kostet relativ viel Geld, vor allem für einen Privatmenschen. Einerseits muss die Überprüfung eines Patents auch bezahlt werden, aber für Unternehmen ist diese finanzielle Hürde wesentlich geringer.