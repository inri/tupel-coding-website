---
layout: post
title: "Falling Skies, bis Staffel 2"
description: "Review zu Falling Skis bis Staffel 2."
date: 2012-11-24 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2012-falling-skies-lg.jpg
  credit: TNT
  creditlink: http://www.tntdrama.com
---

Ich habe mir die zwanzig Episoden der zwei Staffeln Falling Skies angeschaut und war positiv überrascht. Im Prinzip ist Falling Skies eine Hollywood-Version von The Walking Dead und statt Zombies machen Aliens die Welt unsicher. Das Setting ist ansonsten äquivalent: Die Welt steht am Abgrund und die Serie begleitet eine Gruppe Überlebender. 

Falling Skies ist eher an Hollywood angelehnt, da weitaus weniger (Haupt)-Charaktere sterben und es mehr Happy Ends gibt. Trotzdem ist die Geschichte und sind die Menschen interessant.      
Schon sehr zeitig wird man mit den Aliens konfrontiert, aber erst nach und nach werden mehr Informationen preisgegeben. Der Spannungspegel ist gleichbleibend hoch und man hat so gut wie nie das Gefühl, dass eine Episode nur ein Lückenfüller ist. Stets erfährt man etwas über die Welt, mal mehr und mal weniger.

Die Charaktere sind ebenfalls sympathisch. Der kantige Gruppenanführer, der schlaue inoffizielle Anführer und Held der Serie, die Ärztin, die pfiffigen Jugendlichen, der Arschloch-Held. Es gibt ein, zwei Romanzen, die aber erst in der 2. Staffel richtig zur Blüte kommen. Die erste dient vor allem der Charakterzeichnung. 

Ebenfalls sehr positiv ist die hohe Qualität der Serie. Die Aliens und Animationen allgemein sehen sehr gut aus. Es steckt definitiv viel Geld drin. Herr Spielberg hat als ausführender Produzent seine Finger mit im Spiel und das merkt man eben. 

## Fazit

Eine spannende, hochwertige SciFi-Serie. Wem The Walking Dead zu hart ist oder wer keine Zombies mag, aber Weltuntergangsszenarien gerne sieht, dem sei Falling Skies wärmstens ans Herz gelegt.