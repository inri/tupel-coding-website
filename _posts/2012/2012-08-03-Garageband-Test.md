---
layout: post
title: "Der Garageband Test"
description: "Was ist der Garageband Test"
date: 2012-08-03 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

&quot;Den Garageband Test probiere ich auf jedem neuen Gerät oder Plattform aus&quot;

Fraser Speirs schreibt ein [Review über das Nexus 7](http://speirs.org/blog/2012/8/2/thoughts-on-the-google-nexus-7.html):

*[...] was ich den &quot;Garageband Test&quot; nenne, welchen ich auf jedem neuen Gerät oder Plattform ausprobiere. Es ist ganz simpel: Zeig mir etwas das genauso beeindruckend ist wie Garageband auf iOS. Es muss keine Musik App sein, es soll einfach nur eine App sein, die mich sagen lässt &quot;Wow, ich wusste nicht das man das auf so einem Gerät machen kann&quot;.*
