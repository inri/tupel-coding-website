---
layout: post
title: "Populismus über Apple klickt sich gut"
description: "Ein unsäglicher Text über Apple."
date: 2012-12-06
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Worüber soll man sich wundern? Dass der Spiegel einen [solchen Text](http://www.spiegel.de/netzwelt/gadgets/hajo-schuhmacher-rechnet-mit-apple-ab-a-870653.html) publiziert? Wie ein Mensch eine derart verdrehte Weltsicht haben kann? Oder wie geil man auf Aufmerksamkeit und Klicks sein muss?       

Zugegeben: Meine Aufmerksamkeit hat er bekommen. Aber ich werde seinen Text nicht Absatz für Absatz auseinander nehmen, das machen andere. Ich biete eine kurze Korrektur einiger Kritikpunkte. Eine ruhige, aber kritische Analyse kann man auch [bei Giga](http://www.giga.de/unternehmen/apple/news/schumachers-hasstirade-noch-ein-frustrierter-abschied/) lesen. 

Die persönlichen Probleme von Hajo Schumacher sind überschaubar. Scheinbar wurde sein Account gekapert und jemand hat ein Abo bestellt. Das ist ärgerlich. Ob Apple sein Problem nicht lösen kann oder er nicht in der Lage ist Apples Anweisungen zu folgen, wissen wir nicht. Er füttert uns nur mit Häppchen.      
Des weiteren hat er Probleme mit *Adresssalat* und die Synchronisierung klappt nicht. Kenne ich persönlich nicht, ist aber auch doof.        
DRM und die Frage, auf wie vielen Geräten man nun seine gekaufte Musik hören darf, ist ein allgemeines Problem. Ich denke ja, dass Apple ihr iTunes Match weiter ausbauen wird, zu einer Art Flatrate wie Spotify. Immerhin hat Apple ihm erneute Downloads zugestanden.        
Seine Probleme können auftauchen, aber das kann ihm bei jedem Anbieter passieren. Die ganze Cloud-Technik ist noch so neu, da gibt es überall Ecken und Kanten. Apple muss auch in anderen Cloud-Dingen schnell nachbessern.

## Transparenz, Richtlinien und Datenschutz

Von allen Technik-Firmen ist Apple die transparenteste. Sie veröffentlichen beispielsweise die Arbeitszeiten der Angestellten in Foxconn und setzen viel daran die Bedingungen zu verbessern und eben transparenter zu machen. Sie verpflichten sogar die Firmen, die nur Zubehörprodukte herstellen, sich an ihren Vorgaben zu halten. Das kann man natürlich auch Zwang und Drangsalierung nennen.       
Nochmal: Apple sind bisher die einzigen, die das machen. Oder andere Firmen schweigen darüber, was jedoch sehr abwegig wäre. Dass Apple nächstes Jahr vielleicht sogar iMacs komplett in Amerika herstellen wird, ist auch beachtlich. 

Apples Richtlinien für iTunes und ihre Stores sind umstritten und keinesfalls perfekt. Dass sie wegen einer Brust ein Buch / App / etc. nicht zulassen, ist natürlich albern, aber wenn es die Kontrollen und Überprüfung gar nicht gäbe, würden Menschen wie der Schumacher als erstes Schreien, dass Pornographie im Store zu finden ist. Oder einfach ein riesiger Haufen schlechter, nutzloser Apps zum Beispiel.      
Der Apple Store ist ein Kompromiss zum Vorteil des Kunden bzw. wiegen die Nachteile weitaus weniger als die Vorteile. Die Art Freiheit, die Schumacher gerne hätte, wollen die meisten Menschen in diesem Zusammenhang gar nicht haben. Die Absatzzahlen der Geräte und Stores bestätigen das. Oder man sieht das als weiteren Beleg dafür wie dummtreu Apple-Kunden sind. 

Apple beim Thema Datenschutz Vorwürfe zu machen ist reichlich naiv, besonders wenn Facebook und Google im gleichen Absatz auftauchen. Man kann nicht oft genug daran erinnern: **Apple verdient sein Geld mit Produkten. Facebook und Google verdienen ihr Geld durch das Belästigen ihrer Nutzer mit Werbung.** Je mehr Daten sie über ihre Nutzer haben, desto besser können sie Werbung verkaufen. Das ist ein riesengroßer Qualitätsunterschied. 

Das Märchen vom Fachgeschäft ist auch sehr gut. Ebenfalls Nordkorea oder die ganzen anderen verqueren Vergleiche. Teilweise dachte ich beim Lesen, dieser Text ist nur ein Fake und ironisch geschrieben. Doch er meint alles wirklich ernst.

## Fazit 

Zusammenfassend kann man feststellen, dass es in der gesamten Industrie noch viel zu tun gibt. Subjektiv mag Apple ein lohnenswerter Buhmann sein, objektiv passt das aber nicht. Sie gehen mit gutem Beispiel voran, relativ gesehen. Vieles klappt bei ihnen besser als bei der Konkurrenz, aber das kann Hajo Schumacher nicht wissen. Er sollte wirklich mal seine Apple Geräte eintauschen, dann wird er sein blaues Wunder erleben.