---
layout: post
title: "Cook äußert sich zu Maps"
description: "Wieso es toll ist, dass Tim Cook für die unausgereifte Maps App um Verzeihung bittet und Alternativen anbietet."
date: 2012-10-01 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Apple wurde in den letzten Wochen für die neue Maps App kritisiert. Teilweise berechtigt, teils auch unberechtigt. 


Es finden sich noch viele Fehler in ihr und das Kartenmaterial ist schlechter als das von Google. Einige Kritiker waren aber auch einfach nicht in der Lage die App zu bedienen. So gab es Beschwerden über fehlende Straßen, die sich erst in einer tieferen Zoomstufe offenbaren. Außerhalb chinesischer Großstädte sind die Karten von Apple beispielsweise detailreicher als die der Konkurrenten. 

Wie gravierend die Makel sind, hängt vermutlich vom Nutzer ab. Ich persönlich spiele mit der Navigation meist nur rum. Andere Menschen brauchen sie beruflich und waren entsprechend etwas mehr enttäuscht als ich.       
Die objektive Sichtweise der ganzen Kritik wäre meines Erachtens: *shit happens*. Die Maps App ist kein völliges Desaster. Abgesehen von fehlenden *Innovationen* ist sie der einzige negative Aspekt am neuen iPhone. Außerdem ist bereits bekannt, dass Apple mit Hochdruck daran arbeitet. 

Umso überraschter war ich über den [offenen Brief](http://www.apple.com/letter-from-tim-cook-on-maps/) von Tim Cook, oder viel mehr über die ausführliche Entschuldigung. Aber nicht nur, dass der CEO von Apple sich entschuldigt, er schlägt den Kunden alternative Apps von anderen Unternehmen vor. Besucht man den AppStore über sein iPhone, ist der *Karten für dein iPhone*-Banner gut sichtbar.

An und für sich ist dieser Lösungsvorschlag nichts unglaublich besonders, wenn man länger darüber nachdenkt. Apple möchte seinen Kunden ein gutes Produkt bieten und da die neue Karten App dieses Kriterium nicht erfüllt, ist es die beste Lösung Alternativen anzubieten. Was hätte man sonst tun können? Abgesehen davon haben sich immer mal wieder hochrangige Apple-Manager zu Fragen und Problemen geäußert. 

## Vorbildfunktion bitte

Ich wünsche mir von anderen Unternehmen ebenfalls eine solche Ehrlichkeit. Dass ein 12 Monate altes Android-Smartphone nicht mal mehr das neuste Update bekommt, ist in meinen Augen ungefähr eintausendmal schlimmer als eine Karten App als Beta Version. Und von letzteren kann man mit Sicherheit davon ausgehen, dass daran gearbeitet und sie in absehbarer Zeit besser wird. 

Ich wünsche mir auch weniger Feindseligkeit zueinander. Wäre beispielsweise Samsung an Apples Stelle und hätte sich utopischerweise zu einer solchen Entschuldigung durchgerungen, wären wohl kaum die Produkte anderer Unternehmen als Alternativen angeboten worden. Das zeugt von einer gewissen Größe, die man Apple nur schwer absprechen kann. 

Ich meine aber auch generell den Umgangston im Wettbewerb. Es ist einfach nicht cool, wenige Tage nach der Produktvorstellung des Konkurrenten eine Fernsehwerbung zu schalten, die sich auf seine und vor allem auch auf den Kosten seiner Kunden lustig macht. Mir ist schon klar, dass damit die *Hater* angesprochen werden, aber hat man das wirklich nötig. 

Unternehmen sollten einfach öfter daran denken, was am besten für ihre Kunden ist.