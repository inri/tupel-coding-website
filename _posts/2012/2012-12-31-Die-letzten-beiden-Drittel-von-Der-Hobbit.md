---
layout: post
title: "Die letzten beiden Drittel von Der Hobbit"
description: "Ich verrate die restliche Handlung des Der Hobbit Romans."
date: 2012-12-31 
category: medien
tags: [Literatur]
published: true
comments: false
share: true
---

Über die Weihnachtsfeiertage habe ich nicht nur den neuen Hobbit-Film genossen, sondern auch das Buch komplett gelesen. Nun folgt meine Vermutung darüber, wie die nächsten beiden Filme der Trilogie aufgeteilt sind. Wie immer mit Spoilern gewürzt.

Nachdem die Gemeinschaft von den Greifen abgesetzt wurden, finden sie ihren Weg zu Beorn eine Fellwandler, eine Art Mensch der sich in einen Bären verwandeln kann. Bei ihm bleiben sie einige Tage bevor sie in Richtung des Düsterwalds aufbrechen. Dort angelangt gibt es die schlechte Nachricht für die Zwerge und uns: Gandalf kommt nicht mit. Er hat sich um andere Angelegenheiten zu kümmern und am Ende des Buches erfahren wir am Rande, dass es dabei um den im Film angedeuteten Nekromanten geht.

## Reise durch den Düsterwald

Und hier sind wir auch schon bei 80% des zweiten Hobbit-Films! Die vierzehn Kleinwüchsigen wandern durch den dunklen, deprimierenden Wald und werden fast verrückt vor Hunger und Finsternis. Dann sehen sie mehrmals Waldelben, die sie aber stets verscheuchen. Dagegen nicht verscheuchen tun sie die Spinnen, die sich sogar die Zwerge als Leckerbissen schnappen. Rettung eilt durch Bilbo samt Ring herbei.       
Aber eine lange Verschnaufpause haben sie nicht, denn als nächstes werden sie von den Waldelben gefangen, Bilbo natürlich wieder nicht. Der kundschaftet wochenlang unsichtbar die Elbenstadt aus und befreit wieder mal die dreizehn Zwerge.
Über Wasser gelangen sie nach Seestadt, wo man sie feiert und hofft das die Legenden um den Einsamen Berg wahr werden und der Reichtum nach Seestadt kommt.

Und das war es auch schon. Im Film wird hoffentlich die Nebenhandlung rund um den Nekromanten und Gandalf behandelt. Es hört sich insgesamt nach wenig an, aber das war beim ersten Teil auch schon der Fall. Sicherlich sind ihnen noch die Orks auf den Fersen, aber der Showdown findet im dritten Film statt.

## Der Einsame Berg

Oder auch, die Drachentötung nach der Hälfte des Films. Ich bin mir nicht ganz sicher, ob die Suche nach dem geheimen Eingang am Einsamen Berg nicht doch noch im zweiten Teil stattfindet. Das wäre von der Dramaturgie her aber unpassend, denn schließlich wartet man auf den Drachen. Diesen finden die Zwerge wesentlich schneller als den geheimen Eingang und besonders lange lebt er dann auch nicht mehr.       
Die zweite Hälfte des Films wird mit großer Wahrscheinlichkeit der Schlacht um die Schätze gewidmet sein. Hier tauchen die Orks, andere Zwergenvölker, Menschen, Elben und nicht zuletzt auch Gandalf wieder auf und aufeinander. Raffinierte Kriegsführung a lá *Die Rückkehr des Königs* kann man aber nicht erwarten.

## Drei Filme sie zu knechten

Wären es nur zwei Filme geworden, wo befände sich die beste Stelle für den Cut? Beim Buch wäre es vor dem Auftreffen der Spinnen, aber definitiv im Düsterwald gewesen. Man hätte den ersten Teil vielleicht nach der ersten Befreiung durch Bilbo enden lassen können bzw. sobald die Waldelben auftauchen und Bilbo als einziger nicht in deren Gefangenschaft gerät und allein zurückbleibt.      
Oder noch besser: Die Elben ziehen mit ihren Gefangenen in ihre Stadt und Bilbo schlüpft unsichtbar in letzter Sekunde durch das Tor bevor es sich schließt. Ende.     
So hätte man die Zuschauer mit einem für Bilbo heldenhaften Moment, Befreiung von den Spinnen, entlassen und genau das wollte Jackson scheinbar auch, denn diese Stelle gab es im ersten Film, obwohl Bilbo im Buch erst bei den Spinnen der Retter wird!