---
layout: post
title: "Der sinnvolle Lightning Anschluss"
description: "Zwei Gründe wieso Apples Lightning Anschluss sinnvoll für Apple und die Benutzer ist."
date: 2012-09-17 
category: it
tags: [Apple]
published: true
comments: false
share: true
image: 
  feature: 2012-lighting-lg.jpg
  credit: Ingo Richter
  creditlink: http://ingorichter.org
---

Eine kurze Erörterung zu Apples neuen Lightning Anschluss. Eine gängige Meinung ist scheinbar, dass Apple nur einen neuen Stecker verwendet um seine Kunden zu zwingen neue Drittherstellergeräte zu kaufen, da die alten nun nicht mehr verwendbar sind. Warum sollte Apple das tun? 


Aus bloßer Gemeinheit oder um noch reicher zu werden? Das ist natürlich Blödsinn. Es lassen sich mindestens zwei wichtige Beweggründe finden.

## Der alte Anschluss

Der Dock-Connector hatte verschiedene Funktionen. Er war Ladekabel, übertrug Daten und sendete auch Videosignale die über ein Adapter beispielsweise an HDMI übergeben werden konnten (ja, das sind am Ende auch nur Daten).      
Meines Wissens gibt es keinen *freien* Anschluss, der diese Funktionalität hätte bereitstellen können. Nur eine Kombination aus USB und HDMI wäre möglich gewesen, aber zum einen wollte Apple nicht mehrere Anschlüsse in ihrem Smartphone und zum anderen wären für HDMI auch Gebühren fällig geworden.

Es ist noch nicht bekannt, was der neue Lightning-Anschluss genau können wird, aber gewisse Einschränkungen der Alternative sind schon bekannt. So hat MicroUSB nur fünf Pins, davon einen für Strom und zwei für Daten. Ein Pin für Strom ist zu wenig um das iPad 3 mit voller Geschwindigkeit laden zu können. Da der Lightning Anschluss aus 8 Pins besteht, wird Micro USB vermutlich auch andere Einschränkungen für Apple haben und einfach nicht alle gewünschten Funktionen unterstützen können.

Ein weiterer Nachteil wird sichtbar, wenn man mal so einen Stecker anschaut und mit dem neuen Lightning vergleicht. MicroUSB ist ein kleiner fummeliger Stecker und mit der Metallummantelung nicht gerade ein Ausdruck von Eleganz. Der Lightning Stecker dagegen ist abgerundet und man kann ihn beidseitig hereinstecken. Ein Stecker, den man von Apple nicht anders erwartet und der beispielsweise auch wesentlich eleganter als der alte Dock Connector ist.

Soviel zu dem ersten Beweggrund. MicroUSB reicht einfach nicht aus.

## Kontrolle ist besser

An den Drittherstellergeräten verdient sich Apple keine so goldene Nase wie einige scheinbar denken. Möchte ein Hardware-Hersteller ein Produkt für ein Apple Gerät herstellen, nimmt er an Apples MFi Programm teil (*Made For iPod*). Dafür bekommt er die Spezifikation für die Anschlüsse, Dokumentation, Support und ein Siegel von Apple (*For iPhone*). Man munkelt, dass der Kostenpunkt für die Hersteller bei 4$ pro verkauften Gerät liegt.       
Damit erwirtschaftet Apple aber nicht seine Milliardengewinne, sondern erkauft sich eine gewisse Kontrolle über die Zubehörgeräte. Das Prinzip ähnelt dem des App Stores. Wenn ich ein Zubehör mit dem Apple Logo für mein iPhone kaufe, kann ich sicher sein, dass dieses Gerät den Standards von Apple entspricht und getestet wurde. Und funktioniert.

Wenn Apple nun einen gewöhnlichen MicroUSB Anschluss in seinen Geräten verwenden würde, könnte jeder Hersteller Zubehörgeräte bauen und zum Verkauf anbieten. Das Ergebnis wäre zwar ein größerer Markt und geringere Preise, aber auch eine wesentliche größere Unsicherheit für die Kunden. Wer weiß schon genau, ob das Ladegerät aus China funktioniert oder warum der AirPlay Lautsprecher von der unbekannten Marke sich einfach nicht mit dem iPod verbinden will?      
Man kann zwar argumentieren, dass der Kunde ja selbst Schuld ist, wenn er solche Produkte kauft, aber so funktioniert die Psychologie nicht. Erlebt der Kunde Frustration, färbt das auch auf das Gerät ab, das womöglich gar nicht der Grund des Fehlers ist.     
Apple möchte seinen Kunden aber eine gute Benutzererfahrung bieten und das kann man nur, wenn man eben auch Zubehör reguliert und kontrolliert.

Meiner Meinung nach sind diese zwei Beweggründe wesentlich besser nachzuvollziehen, als diverse Unterstellungen. Ob man das nun mag oder nicht, ist eine ganz andere Frage. Der Großteil der Benutzer möchte aber ein Gerät haben das funktioniert und ganz einfach Zubehör erwerben können, welches ebenfalls mit diesem Gerät funktioniert. Und genau das bietet Apple.     
Bastler werden sich deshalb mit Apple Geräten schwer tun. Dafür gibt es Arduino und Android.  