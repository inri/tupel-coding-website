---
layout: post
title: "Arbeitsbedingungen kosten Geld"
description: "Apple zwingt den Herstellern, die das MFi-Segel wollen, Mindeststandards für deren Arbeiter, die die Geräte zusammen bauen."
date: 2012-11-09 
category: it
tags: [Apple, Technologie]
published: true
comments: false
share: true
---

In den neuen iPads, iPhones und iPods ist ab sofort der neue Lightning Connector verbaut und ersetzt damit den in die Jahre gekommenen Dock Connector. Apple sah sich bei der Bekanntmachung mit verschiedenen Kritikpunkten konfrontiert:

* Alte Dritthersteller-Geräte mit Dock Connector funktionieren, wenn überhaupt, nur mit einem Adapter!
* Man muss den Adapter extra dazu kaufen!
* Lightning ist kein MicroUSB!
* Properitär!

Habe ich etwas vergessen? Einige Aspekte hatte ich schon besprochen. Nun kam ein weiterer interessanter Punkt hinzu. Wie MacRumors berichtet, gab es ein Treffen der MFi-Hersteller mit Apple um über den neuen Lightning Connector Standard zu informieren.     
Die Zubehör-Hersteller müssen sich scheinbar an Apples Vorgaben bzgl. Arbeitsbedingungen halten, wenn sie den Lightning Standard nutzen wollen. Es wird wohl auch Kontrollen geben, aber wie genau das aussieht, ist noch nicht bekannt. Ich kann es ja mal anders, aus der Perspektive von Apple zusammenfassen:

*Wenn du für unsere Geräte Zubehör verkaufen willst, brauchst du eine Zulassung von uns, um den Lightning Standard zu nutzen. Und diese Zulassung bekommst du nur, wenn deine Arbeiter zu fairen Bedingungen arbeiten.*     

An alle Apple-kritischen Menschen: Lasst das mal auf euch wirken. Denkt mal eine ruhige Minute darüber nach. 
Natürlich wird man das irgendwann umgehen können und *illegale* Geräte werden auf den Markt kommen. Ja und leider kann nicht mehr jeder Hobbybastler eigene Geräte zusammenlöten. Aber den Schritt, den Apple hier geht, ist für die Technikindustrie sehr wichtig.  

Apple [informiert nicht nur besser](http://www.apple.com/supplierresponsibility/code-of-conduct/labor-and-human-rights.html) über die Arbeitsbedingungen der eigenen Arbeitern, beispielsweise bei Foxconn, sondern Apple will diese Standards auch für alle Hersteller durchsetzen, die im Apple Universum eine Rolle spielen. Mir ist nicht bekannt, dass andere Hersteller es ähnlich handhaben. Aber es könnte in Zukunft notwendig werden, wenn die Konsumenten beginnen auch solche Dinge zu achten.

Am Ende wird man vermutlich einige Euro mehr zahlen müssen. Aber gerade als Konsument, sollte man sich die Frage stellen, was es einem wert ist, abgesehen von dem Risiko, dass das Billigkabel auch China das iPhone in Brand stecken könnte. 
Ich habe sowieso das Gefühl, dass besonders wir Menschen in den westlichen Staat sehr wenig für unsere Elektronik bezahlen, weil wir andere Völker ausbeuten. Das ist aber ein anderes Thema. 

Bessere Arbeitsbedingungen kosten eben Geld. Auch uns.