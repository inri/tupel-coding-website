---
layout: post
title: "iPhone 5 Reviews"
description: "Eine Zusammenfassung verschiedener Reviews über das iPhone 5."
date: 2012-09-21
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

Das iPhone 5 ist nun offiziell erhältlich und am Mittwoch gab es schon die ersten Meinungen und Reviews von Journalisten, die es schon seit der Keynote testen durften. Ursprünglich wollte ich die Pro und Kontras einiger Reviews auflisten, aber nachdem ich die ersten drei gelesen hatte, ließ ich es sein. Sie sind nämlich größtenteils gleich.

Kurz zusammengefasst: Das iPhone ist eine tolle Weiterentwicklung. Es fühlt sich sehr wertig an, ist flott und Apple hat das iPhone 4S damit in fast allen Facetten verbessert.

**Die Verbesserungen:**

* besseres, wertigeres Design
* enorm viel leichteres Gerät
* Display hand- und daumenfreundlich vergrößert
* Display ist besser, mehr Farben
* deckt gleichen Farbraum ab wie das iPad 3
* bestes Smartphone Display
* Kamera verbessert
* schneller und bessere Bilder bei wenig Licht
* schnellerer Prozessor 
* Akkuleistung gleichbleibend, teils sogar etwas besser
* bessere Kopfhörer, vor allem besser im Ohr sitzend
* simpleres Ladekabel dank Lightning Anschluss
* LTE, wenn man es denn nutzen kann

**Die negativen Aspekte:**

* Karten App ist schlechter als die Google App
* viele Apps müssen noch auf den neuen Bildschirm angepasst werden
* Zubehör kann nur bedingt weiter verwendet werden

**Naja, noch einige *negative* Aspekte:**

* kein Flash (haha, der war von mir)
* kein NFC
* kein kabelloses Aufladen (siehe Lumia)
* keine anderen unerwarteten Innovationen

Zwei negative Aspekte sind softwareseitiger Natur und werden in wenigen Wochen oder Monaten nicht mehr relevant sein. 
Einige Journalisten vermissten das gewisse etwas, aber so richtig traurig waren sie doch nicht. Auch die Sache mit dem neuen Lightning Anschluss wurde zwar kritisiert, aber das spielt in wenigen Monaten auch keine Rolle mehr.

Zusammengefasst: Das neue iPhone wurde sehr gelobt. Und die Käufer scheinen das auch zu ahnen.

Ich überschlage im Kopf, wie viel ich draufzahle, wenn ich mein iPhone 4S bei ebay verkaufe. Es ist auf jeden Fall sehr verlockend, wobei fast alle neuen Funktionen in iOS 6 ebenfalls mit dem 4S genutzt werden können. Respekt an Apple, dass sie es immer schaffen dieses *Haben wollen*-Gefühl zu wecken.