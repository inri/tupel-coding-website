---
layout: post
title: "Der ewige Streit"
date: 2012-08-06 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Neulich unterhielt ich mich mit einem Informatikerfreund und wir kamen auf das Thema Betriebssysteme und solche Sachen. Fast schon nebenbei liess er einen äusserst negativen Kommentar zu Apple fallen. Ein Kommentar von dieser Art, dass du als Gegenüber sofort weisst: Es bringt einfach nichts sich mit diesem Menschen über das Thema zu unterhalten. 


Die Meinung und Haltung ist einfach zu verfestigt, siehe auch Angehörige von Religionen. Ähnliches liest man natürlich auch stets in den Kommentaren einer News-Meldung zum obigen Thema: iPhone, Android und Windows.

Als ich vor kurzem meinem Papa über diverse Stolpersteine in der Android Softwareentwicklung berichtete, kamen wir auf allgemeine Smartphone-Themen zu sprechen. Er beklagte, dass es bei Systemen für Computer schon immer eine strikte Einteilung in verschiedene *Lager* gab. Früher mit IBM und Windows, dann Apple und Windows und nun vor allem Android und iOS. Warum könne man nicht akzeptieren, dass es verschiedene Systeme für verschiedene Nutzungsszenarien gibt und sich entsprechende Käufer schon finden werden.

Er veranschaulichte das schizophrene Denken der Informatiker mit einem Beispiel einer anderen Branche: Autos. Jeder, oder zumindest wesentlich mehr Menschen als im Computerbereich,  akzeptiert die Vielfalt an Autoherstellern. Es gibt eben nicht nur zwei Topmarken zwischen denen man sich *entscheiden muss*. Es haben sich unzählige Hersteller auf dem Markt durchgesetzt und existieren mehr oder weniger friedlich miteinander.

Ich grüble seit dem Gespräch über den Grund und die Ursache des Informatiker-Zwists. Natürlich hat er viele Faktoren und die Wurzel allen Übels werde ich nie finden, da meine Erfahrung und Wissen nicht so weit zurückreicht. 1988 ist mein Geburtsjahr.      
Aber einige Gemeinsamkeiten fallen mir immer mal wieder auf. Deshalb beginne ich hier eine kleine Reihe und diskutiere verschiedene Aspekte dieses Streits. Der erste Teil beschäftigt sich mit der Anatomie von Veränderungen.