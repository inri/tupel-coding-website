---
layout: post
title: "Werbung für Technologie"
description: "Was soll Werbung für tolle Geräte eigentlich bezwecken?"
date: 2012-10-17 
category: it
tags: [Technologie, Microsoft, Surface]
published: true
comments: false
share: true
---

Kurz vor dem offiziellen Verkaufsstart von Microsofts iPad-Konkurrenten, dem Surface, wird selbstverständlich die Werbe-Maschinerie befeuert. Etwas irritiert sah ich [das neue Werbe-Video für das Surface](http://www.youtube.com/watch?feature=player_embedded&v=8mSckyoAMHg) und überlegte so vor mich hin, was Werbung für Technologien bezwecken soll.

Ich fand das Surface-Video nicht sehr gelungen. Da veröffentlicht Microsoft endlich dieses unglaublich wichtige Stück Technik und ihnen fällt nichts besseres ein als ein hyperkreatives Video, welches nur den Fokus auf die ansteckbare Tastatur legt? Mal ganz abgesehen von der Tatsache, dass die kleinste und billigste Tablet-Version ohne das Touch Cover daherkommt. Was soll mir das Video sagen? Oder anders: In wie fern erleichtert mir dieses Video die Entscheidung zwischen einem iPad, Android Tablet oder dem Surface?

Die Werbung für Android Tablets ist eher rar. Samsung versucht mich mit der Stifteingabe zu beeindrucken. Naja, ohne Kommentar. Apple setzt den Fokus beim iPad hingegen auf die Beantwortung der Frage *Was kann ich mit einem iPad machen?*. Nach meiner Erfahrung ist das eine wirklich wichtige Frage für die Menschen. Schon öfter wurde ich mit dem Hinweis konfrontiert *Ich habe doch einen Computer, wozu brauche ich ein iPad*. Die Werbevideos von Apple geben eine Antwort auf diese Frage oder versuchen es zumindest.        
Auch die neuen iPhone 5 Clips zeigen tolle Funktionen des Geräts, wieso es nun 4 Zoll groß ist und wie man die Panorama-Funktion nutzen kann. Bei einem Video ist auch das Gerät an sich mit seiner dünnen und leichten Bauweise im Vordergrund, aber alles in allem sind die Videos von Apple eher dezent.

Oft ist der Vorwurf zu hören, die meisten Menschen kaufen Geräte von Apple nur wegen des Designs und Trend. Möchte Microsoft womöglich auf diesen Zug aufspringen und mit dem Video zeigen wie *trendig* und *hip* ihr Surface ist?      
Ich finde diese Art Werbung aber noch tausendmal stilvoller als das sich-lustig-machen in Samsungs Galaxy Werbevideos. Aber auch bei diesen Videos wird eine bestimmte Zielgruppe anvisiert (die *Hater*, die Apple aus Prinzip nicht gut finden). 

Das war bestimmt noch nicht das letzte Werbevideo von Microsoft, aber ich hoffe für sie, dass die nächsten in puncto Technik und Features aussagekräftiger sind.