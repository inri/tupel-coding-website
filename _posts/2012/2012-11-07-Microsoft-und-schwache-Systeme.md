---
layout: post
title: "Microsoft und das schwache System"
description: "Wieso bekommt es Microsoft nicht hin, ein flüssiges Betriebssystem zu bauen?"
date: 2012-11-07 
category: it
tags: [Microsoft]
published: true
comments: false
share: true
---

In einigen Reviews zum Surface war es schon zu lesen, aber dieser [Bericht von Farhad Manjoo](http://www.slate.com/articles/technol/technology/2012/11/microsoft_surface_why_is_the_new_tablet_so_much_worse_than_the_ipad.single.html) drückt es am deutlichsten aus: Das Microsoft Tablet ist zu langsam. Es reagiert zu langsam auf Eingaben, das Drehen des Bildschirm dauert eine kleine Weile und auch die Programme, insbesondere Office, bekleckern sich nicht mit Ruhm. 

     
Das erste iPad war auch keine Rakete, aber es erschien auch vor über zweieinhalb Jahren. Ab dem iPad 2 war die Bedienung sehr flüssig und wird seit dem ständig verbessert. 

In meinem Informatiker-Bekanntenkreis gibt es diese Meinung und generell heißt es öfter mal, dass Apple nur schönes Design kann und Microsoft die besten Betriebssysteme baut. Teilweise mag das vielleicht stimmen und gestimmt haben, aber mit dem Surface und Windows 8 RT beweist Microsoft gerade das Gegenteil.

Es ist einfach nicht akzeptabel, dass sie jahrelang an einem neuen System schrauben und es dann zu langsam für eine normale Benutzung ist. Auch die Hardware ist keine Ausrede, schließlich schafft ein 200€-Tablet auch eine flüssige Bedienung. Und der Verweis auf ein Intel-Surface ist ebenfalls nicht angebracht, wenn dieses mehr als 500€ kostet.

Dass es in Zeiten von Quadcore-Prozessoren nicht schwer ist ein flüssiges System für Desktop-Computer zu entwickeln leuchtet ein. Anscheinend sind eben besonders die kleinen Geräte die eigentliche Herausforderung und auch äußerst wichtig, wenn die Firmen weiterhin ihre Produkte verkaufen wollen.

Was Microsoft mit dem Surface abgeliefert hat, ist in Anbetracht der Tatsache, dass sie über zwei Jahre hinterherhinken, entweder okay (*Es ist ihr erstes Tablet, seid nett zu ihnen.*) oder ganz im Gegenteil ziemlich scheiße (*Sie hatten schließlich genug Zeit.*). Ich finde es nicht gut.

Vielleicht müssen sie mit Windows 8 und dem Surface scheitern, damit sie Projekte [^1] wie den Internet Explorer und Bing endlich einstampfen und sich wieder auf ihre wahre Stärke besinnen und dort ihre Energien bündeln?

[^1]: Projekte, die wenig finanziellen Nutzen haben und vor allem von den Benutzern negativ bewertet werden.