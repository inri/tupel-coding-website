---
layout: post
title: "Forstall verlässt Apple"
description: "Kein Wunder das Scott Forstall Apple verlässt."
date: 2012-10-31 
category: it
tags: [Apple]
published: true
comments: false
share: true
image: 
  feature: 2012-scott-forstall-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Apple strukturiert seine Firmenspitze um. Die wichtigste Veränderung: Scott Forstall verlässt Apple, John Ive übernimmt neben dem Hardware-Design auch das Software-Design, die OS X und iOS Entwicklung wird komplett von Craig Federighi geführt und Eddy Cue kümmert sich um Siri und den Kartendienst. 


Auch der erst vor kurzem dazugekommene John Browett ist nicht mehr länger Chef der Apple Stores. Wie so oft bei Apple, kann man über die genauen Gründe nur spekulieren. Letzterer hat vermutlich einfach keinen guten Job gemacht. Da gab es ja einige Probleme, als er Mitarbeiterkürzungen vornahm und wieder zurücknahm und letztlich kennt sich Tim Cook mit der Materie bestens aus und weiß wann etwas nicht gut läuft.

## Scott Forstall verlässt Apple 2013

Forstall hingegen ist schon seit Next-Zeiten bei Apple und hat einen großen Anteil an den Erfolg der letzten Jahre, schließlich war er maßgeblich an der Entwicklung von iOS beteiligt. Aber es soll in letzter Zeit hinter den Kulissen Spannungen gegeben haben. So sei Forstall einer der wenigen Befürworter des Skeuomorphen Design einiger Anwendungen von Apple. Ausschlaggebend sei aber seine fehlende Bereitschaft gewesen, den *Maps-Entschuldigungsbrief* zu unterzeichnen, was letztlich Tim Cook übernahm.    

Ich kann mir schon vorstellen, dass man sich für so eine Kampagne nicht entschuldigen mag. Mein Gefühl war auch, dass zum einen die Technikmedien diese Sau durchs Dorf trieben und zum anderen die Apple *Hater* ihren Spaß daran fanden. Aber das Ergebnis, trotz der Entschuldigung, ist folgendes: Apples Karten App wird noch jahrelang einen schlechten Ruf haben.       
Völlig egal ob einige der angeblichen Fehler durch die Dummheit ihrer Benutzer entstanden sind oder die Updates so schnell kamen, dass man zwei Wochen später nur noch sehr wenig kritisieren konnte. Es ist auch unwichtig geworden, dass die Benutzererfahrung abgesehen von den Fehlern insgesamt besser geworden ist und die App einige wertvolle Neuerungen enthielt (Vektoren,...). Noch heute werden iPhones auf Antennagate hin untersucht. Noch in einigen Jahren wird es Apple Maps Witze geben.

Fehler macht jeder und vielleicht wäre das Desaster an Forstall vorübergegangen, aber er in seiner Position als Chef der iOS Abteilung kann sich nicht erlauben bockig zu sein. Besonders viele trauern ihm bei Apple wohl nicht hinterher. Und es hat ja auch seine guten Seiten.

Der Zuständigkeitsbereich von Ive wurde erweitert und das finde ich einfach nur gut. In meinen Augen ist er der eigentliche *Steve Jobs* hinter den i-Geräten. Das wusste auch Steve Jobs und so ist es nur logisch, dass sein Gespür für das Design bei Apple ausgeweitet wird.    
Wir dürfen alle auf die kommenden Software-Updates gespannt sein. 