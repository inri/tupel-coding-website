---
layout: post
title: "Das Intel-Surface kann nicht überzeugen"
description: "Microsoft veröffentlicht die Spezifikationen für das Intel-Surface und die lesen sich nicht besonders rosig."
date: 2012-11-29 
category: it
tags: [Microsoft, Surface]
published: true
comments: false
share: true
---

Microsoft hat erste Informationen zum *richtigen* Surface veröffentlicht und die haben es in sich. Es gab diese Meinung oder besser gesagt Vermutung, dass viele Käufer auf das Intel-basierte Surface warten und sich deshalb das Interesse am Surface RT in Grenzen hält. Das Warten lohnt sich nicht.

* Die Preise beginnen bei 900$ für das 64 GB Modell und 1000$ für doppelt so viel Speicher, 128 GB. 
* Es wird mit 900 g nicht nur schwer sein, sondern auch 250 g schwerer als das iPad. 
* Die Akkulaufzeit beträgt halb so viel wie vom Surface RT und iPad, also 4 bis 5 Stunden. 
* Es wird auch wesentlich dicker sein als das Surface RT und iPad      
13,5 mm vs 9,4 mm beim iPad.
* Das Touch- oder Type-Cover sind nicht inklusive und kosten 120€ bzw. 130€ extra.
* Intels Core-i Prozessoren sind verbaut. 
* Office ist ebenfalls nicht inklusive.
* Der Bildschirm ist zwar besser als beim RT, aber Retina ist er auch nicht (1920 x 1080 vs. 2048 x 1536 iPad).

Jetzt mal ernsthaft. Wer kauft denn dieses Gerät? Die Idee eines leistungsstarken Tablet-Computer ist zwar nicht verrückt, aber wie stellt sich Microsoft das vor? Es ist ja nicht nur so, dass man leistungshungrige Programme im Angebot haben muss, sondern für ein Tablet müssen diese Programme auch Touch-optimiert sein. Da ist doch aktuell weit und breit nichts in Sicht.       
Angeblich soll das für Geschäftskunden ganz toll sein, aber wenn ich einige dieser enorm teuren Surfaces + Office Lizenzen kaufen muss, dann überlege ich doch zwei mal, ob ich nicht komplett auf Google Drive mit billigen Android Tablets umsteige. 

Anyway, das soll nur ein kurzer Eintrag werden. Ich schreibe demnächst einen etwas längeren und führe dann detaillierter aus, wieso Microsoft mit seiner aktuellen Strategie den Abhang hinunterfährt. Ungebremst.