---
layout: post
title: "40 Minuten im Apple Store"
description: "Ich fuhr nach Dresden in einem Apple Store um dort mein knarzendes MacBook vorzuführen."
date: 2012-09-05 
category: it
tags: [MacBook]
feature: true
published: true
comments: false
share: true
---

Mein Retina MacBook gab knarzende Geräusche von sich. Das passierte meist beim normalen Tippen, wenn die Handballen aufliegen oder beim in die Hand nehmen des Geräts. Scheinbar taten sich nur 2 Möglichkeiten für mich auf: Entweder zum Händler zurückschicken und auf ein Neugerät warten, oder der Besuch in einem Apple Store. 

Meine Vermutung war ein Problem mit den Federn im Inneren des Gehäuses. Diese sind an strategisch sinnvollen Stellen angebracht und halten die Gehäuseunterseite davon ab das Mainboard zu berühren. Gute Idee. Aber leider knarzen und quietschen Federn wenn sie nicht richtig eingestellt oder geölt sind. Und genauso klangen meine Probleme. Dachte ich.

Ich begab mich also auf eine 4-stündige Reise nach Dresden zum nächsten Apple Store. Meine Hoffnungen waren: entweder ein Umtausch des gesamten MacBooks oder eine Reparatur. Ehrlich gesagt hatte ich für letzteres weniger Hoffnung. Wie auch immer der Spaß am Ende ausgeht, ich war auf jeden Fall gespannt auf die berühmte Apple Geniues Bar. Die Ernüchterung vorne weg: Getränke gab es keine.

## Knarzende Lösung

Ich kam pünktlich am Store an und musste einige Minuten auf mein *Genie* warten. Das Problem war schnell erklärt, besonders auch, weil neben mir ein weiterer Kunde mit gleichem Problem saß. Bei ihm war das Knarzen aber wesentlich leichter zu erzeugen und auch lauter als bei mir. Das Genie sagte uns, dass es sehr wahrscheinlich an der Federn liegt, aber so oder so noch im Toleranzbereich läge.     
Wenn man mir schon so anfängt, kann ich das gar nicht leiden. 

Einen Fehler zugeben, aber sagen, dass man es eben ertragen muss. Nö. Ich berichtete von den englischsprachigen Berichten im Apple Forum. Das Genie bot uns an das Gerät mal aufzuschrauben und zu reinigen und während dessen schauten wir beide Betroffenen die ausgestellten Retina MacBooks genauer an. Ergebnis: Kein Knarzen.      
Aber das sage ich mit Vorbehalt, denn im Apple Store wurde Musik gespielt und viele Menschen haben sich unterhalten, also es war laut. Beispielsweise ein Knarzen von meinem MacBook war dort kaum zu hören, aber zum Beispiel zu Hause oder in einem Bürozimmer, ist es sehr deutlich und störend zu vernehmen. 

Zugeben muss ich aber auch, dass meines nicht nach quietschender Feder klang. Zumindest nicht so wie beim anderen Kunden, sondern es kam eher vom Gehäuse. Wie auch immer, nach zehn Minuten kam das *Genie* mit meinem Gerät zurück und hielt uns triumphierend ein Tuch mit einem grauen Fleck entgegen. Aluminiumstaub von der Produktion befand sich noch in den Zwischenräumen des Unterdeckels. Okay, mit dieser Erklärung konnte ich leben.      
Er drückte auf dem MacBook herum und ich drückt auf dem MacBook herum während mein Ohr fast das Gehäuse berührte. Das einzige was ich knarzen und knacken hörte waren meine Finger. 

Wenn man wirklich sehr nah mit dem Ohr am Untergehäuse rangeht und auf die eine Stelle über der Feder drückt, hört man zwar die Feder, aber sie quietscht nicht, sie klingt eben normal und das ist wie gesagt auch sehr leise. Scheinbar waren es tatsächlich Aluminiumstaubreste. 

Diesen ersten Teil des Textes schrieb ich im Zug. Auch da kein Knarzen, wobei der Lautstärkepegel vermutlich nur etwas leiser als im Apple Store war. Zu Hause wird sich offenbaren, ob das Knarzen tatsächlich ausgemerzt ist.
.
.
.
Ich schreibe nun zu Hause weiter und bin sehr glücklich, denn es knarzt in der Tat nichts mehr. Sehr gut. Dann haben sich die acht Stunden Fahrt also gelohnt.

## Gedanken zum Apple Store

Ich bin ein wenig enttäuscht. Ich fühlte mich mit meinem Problem genauso behandelt, wie als Kunde fast überall in Deutschland: Fehler werden nicht oder kaum eingeräumt, man solle sich nicht so haben und am Ende hat man das Gefühl, als Kunde ist man eine Last. Dieses *Der Kunde ist König* ist doch nur noch eine Phrase.

Der erste Ansatz des Genies war ein simples *Ist eben so, liegt im Toleranzbereich.* Je länger ich über diese lapidare Äußerung nachdenke, desto wütender macht sie mich. Verdammt, der Typ arbeitet bei Apple. Apple! Ein solches Knarzen ist eben nicht normal und Steve Jobs hätte es sicherlich auch nicht akzeptiert. Wobei mein Knarzen wesentlich harmloser als das vom anderen Kunden war. Das ist niemals in irgendeinem Toleranzbereich. Okay war auf jeden Fall, dass uns doch recht schnell die Säuberung angeboten wurde, wobei ich nicht weiß, ob es beim anderen Kunden danach besser war. Ich musste zum Zug eilen.

Der Store an sich ist aber sehr übersichtlich und viele Mitarbeiter kümmern sich um die Kunden. Diese Art Service sucht man von anderen Herstellern vergeblich.     
Mit einem ähnlichen Problem bei einem nicht-Apple Notebook hätte ich entweder gleich ein Umtausch in die Wege leiten müssen oder zu einem normalen Techniker gehen müssen. Je technisch komplexer und in der Bauweise kompakter neue Notebooks aber werden, desto schwieriger wird es aber für solche Techniker Probleme zu erkennen und zu lösen. 

Fazit: Bei Apple kümmert man sich um die Kunden, aber Wunder braucht man auch nicht erwarten.