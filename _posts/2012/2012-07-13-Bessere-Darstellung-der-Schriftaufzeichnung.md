---
layout: post
title: "Bessere Darstellung der Schriftaufzeichnung"
description: "Ist es besser wenn die Schrift wie durch unsichtbarer Hand erscheint oder man einen Stift oder eine Person sieht?"
date: 2012-07-13 
category: it
tags: [Design]
published: true
comments: false
share: true
---

Ich schaue mir zur Zeit einige Vorlesungen auf [Udacity](http://www.udacity.com/) an. Dabei erklärt der Dozent eine Thematik und schreibt mit einem digitalen Stift Notizen. 


Im Video sieht man zum einen aus einer Top-Shot-Ansicht (senkrecht von oben) die Hände und den Stift des Dozenten und gleichzeitig die aufgeschriebenen Informationen. Wobei das Geschriebene halbdurchsichtig über die Hand gelegt wurde, so dass es nicht von der Hand verdeckt wird, und die Hand nicht vom Geschriebenen.

In unserem Bachelorprojekt haben wir die Idee der gleichzeitigen Aufzeichnung eines digitalen Stifts zusammen mit einem Video aufgegriffen und entsprechend eine parallelisierte Darstellung beider Ansichten entworfen. So kann man im Video an eine beliebige Stelle springen und die Stiftaufzeichnung passt sich an und umgekehrt genauso. Das ist ziemlich cool.

Beim Betrachten des Udacity-Videos wird mir aber bewusst, dass wir bei der Darstellung der vom Stift geschriebenen Informationen einfach nicht weit genug gedacht haben. Das Geschriebene erscheint bei uns einfach so, wie es auf dem Blatt auftaucht. Man sieht keinen Stift, keinen Schreiber, gar nichts. Es wirkt fast ein bisschen magisch, aber ist das für den Betrachter auf Dauer angenehm?      
Mir persönlich gefällt unsere Variante nicht so gut wie die von Udacity. 

Wir preisen unsere Lösung ja auch als Stiftaufzeichnung für einen Schülervortrag an, was in der Praxis natürlich totaler Quatsch ist. In meinen Augen ist Stiftaufzeichnung in Verbindung mit einem Video vor allem für kurze *Wissensclip* sinnvoll. Eben genau das, was Udacity macht. Das Wissen ist dabei in angenehme kleine Bausteine geteilt und kann sehr angenehm konsumiert werden. Das ist meiner Meinung nach auch eine schwäche von Vorlesungsaufzeichnungen, die sind einfach zu lang. 