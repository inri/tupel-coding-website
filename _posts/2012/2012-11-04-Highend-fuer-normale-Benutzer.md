---
layout: post
title: "Highend für normale Benutzer"
description: "Wieso gerade normale Benutzer nicht die billigere Technik kaufen sollten."
date: 2012-11-04 
category: blog
tags: [Technologie]
published: true
comments: false
share: true
---

Über [John Gruber](http://daringfireball.net/linked/2012/11/04/crappy-computers) bin ich auf einen kurzen Rant von [Lukas Mathis](http://ignorethecode.net/blog/2012/11/04/crappy_computers/) gestoßen, der sich auf eine verbreitete Meinung in Sachen notwendige Computer bezieht und einfach nur verdammt wahr ist. Ich bin so frei und übersetze ihn.

Das ist eine Aussage, die man immer mal wieder hört: *Für die normale Benutzung brauchen die meisten Leute nur Einsteiger-Computer mit geringer Performance.*         
Sogar diese Leute selbst sagen *Ich mache mit meinem Rechner ja nicht so viel, da reicht mir auch die billigste Option.* Und dann kaufen sie ein schreckliches, schwachbrüstiges Netbook, finden heraus, dass es einen winzigen Bildschirm hat, unglaublich langsam ist, die Tastatur unbenutzbar ist und sie es am Ende entweder nie benutzen oder zum Ergebnis kommen, dass sie Computer hassen. In Wirklichkeit ist es genau umgekehrt: Erfahrene Benutzer können auch mit schlechten Computern umgehen, aber gerade normale Nutzer brauchen die besten Computer.  

Schauen wir uns den Speicherplatz an. Ich habe kein Problem mit einem Netbook das nur eine 128 GB SSD verbaut hat, weil meine iTunes Bibliothek und Fotos auf einem Home Server sind. Ich nutze sehr viel Speicher in der Cloud und speichere nicht viel lokal ab. Die meisten normalen Nutzer andererseits, speichern alles auf ihren lokalen Festplatten ab: Musik, Bilder, Filme, einfach alles ist dort.       
Genauso ist es auch bei kleinen iPhones. Weil ich regelmäßig synchronisiere, meine Fotos ins Backup lege und auf dem Gerät lösche, reichen mir auch 16 GB Speicher. Die meisten normalen Nutzer tun dies alles aber nicht und schleppen alles mit sich herum. Natürlich stoßen sie bei 16 GB an ihre Grenzen, das System läuft langsamer und ständig erscheinen Fehler und Aufforderungen Daten manuell zu löschen,was keinen Spaß macht.
 
Schauen wir uns die Geschwindigkeit an. Wenn mein MacBook langsam ist, öffne ich das Terminal, finde heraus welches Programm zu viel CPU oder RAM frisst und beende es. Ich weiß wie ich herausfinde welche meiner Grafikkarten läuft. Ich finde den Flaschenhals und löse das Problem. Wenn der Computer eines normalen Nutzers langsam ist, war's das. Die einzige Reaktion ist ein Neustart und wenn das nicht hilft, ist er sauer.      
Ich weiß auch wie man vorinstallierte Crapware los wird. Normale Nutzer wissen es vermutlich nicht und enden mit einem Computer voller Müll. Je langsamer der Computer ist, desto größer ist dieses Problem.       
Schauen wir uns die Bildschirmgröße an. Ich weiß, wie man mit Exposé, und Schreibtischen das meiste aus kleineren Bildschirmgrößen herausholt. Ich weiß wie man Werkzeuge wie Moom oder Cinch installiert, welche beim Window-Management helfen. Normale Nutzer benutzen einfach das, was da ist. Je kleiner der Bildschirm ist, desto schwieriger wird es für sie die ganzen Fenster manuell zu organisieren und zu wissen, wo was ist.       
Also ist die verbreitete Meinung, dass die einfachen Computer für normale Nutzer völlig ausreichend sind, genau umgekehrt. Normale Nutzer brauchen Highend Computer, während erfahrene Nutzer diejenigen sind, welche mit den Limitierungen von einfachen Computern umgehen können.

Es geht um die Aussage, nicht den genauen Wortlaut. Natürlich sind im Highend-Bereich kaum Grenzen gesetzt, aber den allerletzten Schrei braucht ein normaler Nutzer auch nicht. Doch den billigsten Mist sollte er sich auch nicht anschaffen, denn damit gibt es immer Ärger.

Ein sehr kluger Gedanke.