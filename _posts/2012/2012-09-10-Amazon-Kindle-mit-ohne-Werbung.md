---
layout: post
title: "Amazon Kindle mit ohne Werbung"
description: "Wieso muss ich Geld bezahlen um keine Werbung auf meinem Gerät sehen zu müssen."
date: 2012-09-10 
category: it
tags: [Amazon]
feature: true
published: true
comments: false
share: true
---

Amazon stellte vergangene Woche die neuen Geräte seiner Kindle-Familie vor. In Amerika gab es bei den *nur-Lese-Kindle* der letzten Generation schon zwei verschiedene Versionen: Mit und ohne Werbung. 


Kurz nach der Neuvorstellung wurde bekannt, dass neuerdings nicht nur die neuen Kindle Fire Modelle Werbung in Form eines Bildschirmschoners haben werden, sondern dass es überhaupt keine werbefreien Fire zu Kaufen geben soll.

Die Fachpresse war empört und Amazon ruderte zurück. Es gäbe dann doch eine Opt-Out Möglichkeit um den Kindle Fire werbefrei zu schalten, Kostenpunkt 15€. Über diese Summe philosophiere ich später noch einmal, aber mir geht es um Amazons Verständnis bei der Sache.      
Ein Sprecher des Konzerns teilte mit, dass viele Käufer die Werbung auf den alten Kindles mochten. Auch Jeff Bezoz, Chef von Amazon, verriet in einem Interview dass der werbefreie Kindle praktisch kaum verkauft wurde. 

Es stellt sich eventuell die Frage, ob die Käufer nur besonders sparsam sind oder die Werbung unbedingt möchten. Aber bei Amazon ging man davon aus, dass es okay sei die Benutzer zu bevormunden und die Werbung zur Pflicht zu machen. Nun diese Verhaltensweise kennen wir zu gut von Apple. Oder doch nicht? Ich meine nämlich, dass es ein Unterschied macht, ob ich in meinen Möglichkeiten eingeschränkt werde oder für mich eine Entscheidung getroffen wird.       
Oder anders gesagt: Der Zwang die Werbung akzeptieren zu müssen, stellt für mich keinen Mehrwert für den Benutzer dar. Diverse Grenzen, die Apple seinen Benutzern setzt, mögen einen kleinen Nutzerkreis einschränken, sind für die Mehrheit aber kaum spürbar. 

Ich finde es interessant, wie unterschiedlich beide Unternehmen diese Art von Entscheidungen treffen. Steht immer nur der Käufer im Fokus? Warum muss man für ein werbefreien Kindle eine Gebühr bezahlen? Wenn die Nutzer die Werbung so gern mögen, könnte man die Deaktivierung den Benutzer selbst überlassen.       
Es gibt sicherlich viele Sichtweisen auf diese Thematik. Mal sehen ob sich andere Hersteller ähnliche Konzepte ausdenken.