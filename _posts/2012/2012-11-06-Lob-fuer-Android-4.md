---
layout: post
title: "Lob für Android 4"
description: "Mein Eindruck nach einigen Tagen mit dem Nexus 7 und Android 4."
date: 2012-11-06 
category: it
tags: [Android]
published: true
comments: false
share: true
---

Durch das Nexus 7 habe ich seit einer kleinen Weile wieder ein aktuelles Android in der Hand und ich bin wirklich beeindruckt. Oder wie es Markus Lanz sagen würde: *Wow*.     
Man merkt deutlich dass sich seit Version 2.3 einiges getan hat und vor allem auch, dass die Hersteller ihre eigenen Oberflächen und Schnickschnack lieber optional anbieten sollten, denn Android 4 ist schön so wie es ist.

Wo fange ich am besten an? Das System läuft flüssig und das auf einem 200€-Gerät. Es gibt zwar einige Apps, die so ihre Schwierigkeiten haben (ZDF Mediathek), aber insgesamt kann man sehr zufrieden mit der preisgünstigen Hardware sein.

## Einstellungen und Task Manager 

Der Aufbau und die Navigation in den Einstellungen gefällt mir fast eine wenig besser als bei iOS. Noch dazu hat Google einige wirklich nützliche Dinge eingebaut. Zum Beispiel einen automatischen Traffic-Zähler, welcher zwar nur zählt und nicht warnen kann, aber immerhin. Man sieht eine Gesamtübersicht und wie viel die einzelne App verbraucht hat, absteigend. Bei mir ist der Spitzenreiter natürlich der Play Store mit 726 MB und insgesamt sind es schon 1,36 GB seit dem 31.10.12.

Für mich persönlich sind die zahlreichen Entwickleroptionen sehr, sehr nützlich. Ursprünglich waren die Nexus-Geräte ja Entwicklergeräte, aber mittlerweile möchte Google sie ja unter die Leute bringen, egal ob Entwickler oder sonst wer. Umso schöner, dass diese Optionen nicht wegrationalisiert wurden.
Als letztes sei noch die Kontenverwaltung lobend erwähnt. iOS unterstützt zwar mittlerweile auch Twitter und Facebook, aber Android lässt zum Beispiel auch Dropbox zu. Was noch möglich ist, weiß ich leider nicht, aber generell sollte man alle Anmeldungen auf diese Art verwalten können.

Ein interessantes Feature ist die Liste an zuletzt benutzten Apps. Bei iOS kommt man in diese Ansicht über doppeltes Drücken des Home-Buttons. In Android ist dieses Feature so wichtig, dass es einen eigenen Button im UI bekommen hat, direkt neben dem Home- und Zurück-Button. Die Liste stellt nicht nur ein Vorschau-Bild bereit, sondern hat auch mehr Funktionen als bei iOS. Nach einem langen Tippen auf ein Fenster kann man sich entweder App-Details anschauen (Speicher, Cache,...) oder die App aus der Liste herausnehmen (= beenden).

## Wo bleiben die Widgets, iOS?

Für das Thema Widgets möchte ich etwas weiter ausholen und die Kündigung des ehemaligen iOS-Verantwortlichen Forstall thematisieren. Er wurde für die Plattform iOS gelobt und ein großer Teil des Erfolges von iPhone und iPad liegt ja auch genau in ihr begründet. Einige Gründe seiner Kündigung sollen die Siri-Beta und die fehlerhafte Karten App sein. Sind das die einzigen Software-Gründe?       
Wenn ich mir die wirklich guten und nützlichen Widgets auf Android anschaue, frage ich mich, wieso sie es noch nicht auf iOS geschafft haben. Technisch sind die i-Geräte locker dazu in der Lage, das mag vor 2 Jahren noch anders gewesen sein. Ich glaube auch nicht, dass sie zu komplex sind und Menschen verwirren. Es gibt auch mäßige Widgets, aber die kann man ja per App-Review aussortieren.

Wollte man sich bei Apple eventuell auch von Forstall trennen, weil sich iOS seit zwei Jahren kaum verändert hat? Siri, Panorama-Funktion, Informationsleiste, und so weiter, sind alles tolle Dinge, aber entweder nicht besonders neu oder kein *Durchbruch* für die Plattform. Mit Widgets und anderen ähnlichen Mechanismen würde man sie mal richtig erneuern und zeigen, dass die Entwicklung des UI nicht stehen bleibt.
Aber genau das tut sie im Moment bei iOS.

Zurück zu Android Widgets. Sie sind toll und nützlich. Die Auswahl passiert über den Widgets-Reiter in der App-Übersicht. Man kann hin und her blättern und sich dabei über die schicken Effekte freuen. Es ist klar dass Apple nicht zu fancy und verspielt sein mag, aber ein bisschen mehr würde auch nicht schaden und auf OS X können sie es ja auch.     

Wie bereits gesagt, ich finde einige Widgets wirklich gut und nützlich. Und eigentlich warte ich schon seit zwei Jahren darauf, dass Apple etwas ähnliches implementiert.

Bevor ich auf das Hauptproblem mit Android zu sprechen komme, seien einige besonders schicker Apps erwähnt. Das System insgesamt läuft gut, die verfügbaren Telefone und Tablets werden besser und zahlreicher und so schaffen es auch immer bessere Apps auf die Plattform. Viele Standard-Apps wie beispielsweise die Angry Birds Reihe gibt es schon und sie bekommen gleichzeitig mit iOS Updates.

## Keine Perlen, kein Wechsel

Halten wir die positiven Aspekte des Nexus noch mal fest: Ein solides, leistungsstarkes Gerät, ein schnelles, schickes System mit guten Features und immer besser werdende App für die Plattform. So weit so gut.      
Das Problem liegt im letzten Aspekt. Die Apps werden zwar immer besser und zahlreicher, aber sie sind es zur Zeit einfach noch nicht. Nach wie vor erscheinen neue, innovative Apps zuerst auf dem iPhone oder iPad und wandern erst später zu Android. Ich suche Perlen wie iAWriter, Paper, Cockatils #8, iFinance und Garageband auf Android vergebens. Sie werden definitiv kommen, aber wann? 

Noch dazu gibt es nur sehr wenige Apps, die auf Android besser sind bzw. aussehen als auf iOS, die meisten sind eher schlechter. Beispielsweise die ebay App hat immer noch das alte UI in Android. Sieht nach nichts aus, möchte man nicht benutzen.      
Mit wachsender Popularität der Nexus-Geräte werden hoffentlich auch kreative Entwickler angelockt. Wenn die Geräte in Zukunft billiger werden, dann gebt auch mal Geld für Apps aus, liebe Kunden. Wenn ihr mit einem Spiel einen guten Nachmittag habt, dann sind das die 3€ Kosten einfach wert. Aber ich schweife ab.

Android hat sich sehr gut weiterentwickelt und beeindruckt mich. Jetzt sind Entwickler gefragt. Neue Android Apps braucht das Land! So lange ich warte, wäre ich noch nicht zu einem Wechsel bereit.