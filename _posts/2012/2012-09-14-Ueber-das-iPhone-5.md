---
layout: post
title: "Über das iPhone 5"
description: "Ausführlicher über das neue iPhone 5."
date: 2012-09-14 
category: it
tags: [iPhone]
published: true
comments: false
share: true
image: 
  feature: 2012-apple_iphone_5-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Die ersten Journalisten hatten das neue iPhone bereits in der Hand und in den weiten des Internets schwirren die ersten Meinungen umher. Teils ist eine gewisse Ernüchterung, wenn nicht sogar Enttäuschung, herauszuhören. 


Beispielsweise der [Kommentator von heise](http://www.heise.de/newsticker/meldung/Kommentar-Das-neue-iPhone-ist-Modellpflege-ohne-Visionen-1706243.html) scheint nicht sehr zufrieden zu sein. Er vermisst ein ominöses Killer-Feature. Es gibt keine Notizen wie auf dem Galaxy Note, keine kabelloses Aufladen wie beim Lumia, kein Bezahlen per NFC wie bei einigen Androids. Kann man dieses fehlende Feature ernsthaft vermissen? Ich finde nicht.

## Gewicht

Ich hatte Anfang August schon geschrieben, dass ich nicht **die** große Veränderung bei den bisher geleakten Informationen erkenne. Nach der Präsentation bin ich mir sicher, dass das Redesign die bedeutendste Änderung am ganzen Telefon ist und gleichzeitig die am meisten unterschätzte. Um das mal klarzustellen: Wir reden hier von einem leistungsstarken Smartphone, mit 4 Zoll Bildschirm, keinem Plastikgehäuse und trotzdem einem lächerlich leichten Gewicht von nur ca. 110 Gramm. Das sind mindestens 20 Gramm weniger, als vergleichbare Smartphones (Galaxy Nexus, S3 und One X) und sogar 30 Gramm im Vergleich zum alten iPhone.

Mit diesen 110 Gramm ist man wieder in einem sehr angenehmen Bereich, welchen man eigentlich vor 5 bis 10 Jahren verlassen hatte als die Bildschirme der Smartphones größer wurden.
Einer mag einwenden, dass die oben genannten Smartphones etwas größere Bildschirme haben als das iPhone 5. Das ist völlig korrekt, aber das zusätzliche Gewicht kompensieren sie durch das Verbauen von Kunststoff, welches immer noch leichter als Aluminium ist.

Ich würde wirklich gerne ein kleineres Samsung Galaxy sehen. Ich bezweifle zwar, dass es bei ähnlichen Materialien auf das gleiche Gewicht und Größe wie ein iPhone kommt, aber es wäre ein Anfang. Doch wie ich bereits vermute, würde es anderen Herstellern eher schwer fallen die ganze Leistung ihrer Telefone in kleine Gehäuse zu zwängen. Oder glauben sie ernsthaft, dass die 4,x Zoll Größen die Zukunft für Smartphones sind?     
Apple hat sich mehrere Jahre Zeit gelassen um die Höhe um nur einen Zentimeter zu verlängern. Sie hätten es bei der Gelegenheit auch breiter oder noch ein Stück höher machen können, aber in der Größe des iPhone 5 steckt sehr viel Überlegung.

## Material

Auch was das verwendete Material angeht, ist eine interessante Entwicklung bei Apple festzustellen ([siehe John Gruber](http://daringfireball.net/2012/09/iphone_5_event)). Am grundsätzlichen Design hat sich seit 2007 kaum etwas verändert, aber bei Apple war man auf der Suche nach dem richtigen Material für die Rückseite. Man wollte scheinbar gerne Aluminium nutzen, aber das vertrug sich vermutlich nicht mit dem Gewicht der Komponenten und der Empfangsleistung der Antennen. Deshalb kam nur das erste iPhone mit einem Aluminiumrücken daher und bei den Nachfolgern 3G und 3GS wurde Plastik verwendet. Plastik fühlt sich aber nicht wertig an und ist auch unstabiler als Aluminium.

Das iPhone 4 und 4S gingen daher einen anderen Weg. Die Wertigkeit wurde durch die gläserne Rückseite erreicht und die Antenne in den Metallrahmen verlegt. Man blieb bei dem Gewicht der Vorgänger und hatte am Ende ein sehr erfolgreiches Smartphone.      
Mit dem Glas gab es aber zwei Probleme, denn es ist zum einen schwerer als Plastik und Aluminium und zum anderen ist es weniger robust. Bilder von zerbrochenen iPhone-Vorder- und Rückseiten gibt es sehr viele und beispielsweise meine Rückseite ist auch schon heftig zerkratzt. In den letzten Jahren hat Apple aber viel Erfahrung im Entwickeln kompakter Designs gesammelt und die verfügbare Technologien werden auch immer kleiner.       
Man konnte also zurück zu dem ursprünglichem Aluminium-Design und dabei das Gewicht reduzieren.

Das Ergebnis ist ein ziemlich leichtes Smartphone. In allen Berichten und Videos zu ersten Erfahrungen mit dem iPhone wurde stets Erstaunen ausgedrückt, wie leicht es plötzlich sei und wie schwer das alte sich anfühlt.      
Ich hatte mal ein HTC One X in der Hand und das fühlte sich trotz seiner Größe angenehm leicht an. Dabei ist es nur 10 Gramm leichter als mein iPhone. Das HTC Wildfire meiner Freundin wiegt mit 118 Gramm sogar noch etwas mehr als das iPhone 5 und ihr Smartphone finde ich im Vergleich zum iPhone oder auch HTC Desire enorm viel leichter. Also auch wenn ich noch kein iPhone 5 in der Hand hatte, kann ich mir lebhaft vorstellen, wie leicht es ist.

Aber die Leute wollten irgendwas anderes. Eventuell ein völlig neues Design, was natürlich Schwachsinn ist der seines gleichen sucht. Ein Design, das so erfolgreich ist, kann man nicht mit etwas neuem ersetzen. Wie es [MG Siegler](http://techcrunch.com/2012/09/13/the-iphone-5-event/) schon beschrieben hat, die gleichen Leute, die jetzt rumjammern, dass das iPhone sich ja nicht verändert hat, würden genauso über ein neues Design schimpfen.      
Die größte Veränderung am iPhone sieht man nicht. Man spürt sie eben nur und wenn man einmal das neue iPhone in der Hand gehabt hat, wird man nicht so schnell ein neues Telefon finden, welches sich genauso leicht und wertig anfühlt.
Übrigens trifft das gleiche auf den neuen iPod Touch zu, der ja noch einmal leichter ist als das iPhone.

## Killer-Feature

Ich komme mir fast schon albern vor, aber bzgl. der oben benannten Features möchte ich auch noch einige Worte schreiben. Das [Galaxy Note](http://www.androidpolice.com/2012/08/21/samsung-galaxy-note-10-1-review-an-embarrassing-lazy-arrogant-money-grab/) ist von enorm schlechter Qualität und die Software ruckelt und stockt. Für einige wenige mögen diese schriftlichen Notizen ein Feature sein, ich sehe sie eher als Ablenkung vom mittelmäßigen Gerät.        
Wie lange dauert wohl das drahtlose Aufladen? Wenn man unterwegs ist und sein Smartphone an ein Laptop hängen möchte muss man entweder das Kissen, welches wohl noch extra gekauft werden muss, mitnehmen oder auf das Kabel zurückgreifen. Es ist eine Nettigkeit wie früher auswechselbare Smartphone-Cover. Aber es ist bei Weitem kein Feature oder Kaufgrund.

Und dieser NFC-Quatsch ist ja wohl das beste Beispiel für Deutschland. Es ist schon ein Glückstreffer, wenn man mal ein Geschäft oder Restaurant findet, bei dem man mit Kreditkarte bezahlen kann. Bis diese NFC-Technik weiträumig verfügbar ist, wird Apple noch einige iPhones vorstellen.      
Auch die Kamera braucht keine Möchtegern-Features wie 50 Megapixel. Jemand der mit seinem Smartphone fotografiert möchte genau zwei Dinge: Ein Foto schnell schießen können und ein gutes Foto machen. Und genau daran hat Apple gearbeitet.

## Lightning

Der neue Lightning Anschluss bereitet auch Unmut. Aber warum? Weil alte Geräte nicht mehr passen? Ich bezweifle sowieso, dass von diesem Problem ein Großteil der iPhone-Besitzer betroffen ist. Zudem gab es für Apple viele gute Gründe, einen neuen Stecker zu entwickeln.      
Der DockConnector war zu dick und wenn man ehrlich ist, auch kein besonders schöner Stecker ([siehe Gruber](http://daringfireball.net/2012/09/iphone_5_event)). Hätte Apple das verbreitete USB-Format gewählt, hätten sie die Kontrolle über die Dritthersteller-Hardware verloren. Außerdem: Kann mir jemand verraten, wieso es der Spezifizierung von USB 3.0 nicht gelungen ist den Stecker universell steckbar zu machen, zumindest den Micro-Stecker ([Wiki](http://en.wikipedia.org/wiki/USB_3.0#Connectors))? Das ist ein riesiger Vorteil von Apples Lightning Stecker, welcher ungefähr 1000x eleganter ist.      
Aber das schrieb Gruber ja auch schon, immer wenn Apple sich von einem Anschluss verabschiedet, jammern die Technikjournalisten rum. Völlig egal ob dieser veraltet ist: *Wie kann Apple nur?*

## Medien

Apple ist groß geworden. Entscheidungen, die früher niemanden interessiert hätten, werden von den Medien unreflektiert kritisiert. Soweit keine Neuigkeit. Für viele schien vorher bereits festzustehen, entweder revolutioniert Apple irgendwas oder es wird eine Enttäuschung. Ein neues Design hätte man den Lesern als große Veränderung verkaufen können. Eine beispiellose Reduzierung der Größe und des Gewichts dagegen eher nicht, deshalb fährt man lieber die Drama-Schiene.
Dass der Apple Kurs nicht eingebrochen ist, verschwieg man in deutschen Medien auch, obwohl jeder zu ahnen scheint, dass es jetzt bergab geht.

Nein, ich denke nicht. Das iPhone 5 ist eine konsequente Weiterentwicklung und Verbesserung.  Oder um es mit den Worten des heise-Kommentars zu sagen: Das iPhone hat eine klare Vision und Apple verfolgt diese wie kein anderes Unternehmen. Es wird das optimale Telefon für den normalen, durchschnittlichen Benutzer bleiben, wenn er es sich denn auch leisten kann.     
Es wird sich mit Sicherheit gut verkaufen und Anfang nächsten Jahres wird die Apple Aktie weitere Rekorde brechen.