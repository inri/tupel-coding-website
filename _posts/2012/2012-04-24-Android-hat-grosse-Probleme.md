---
layout: post
title: "Android hat große Probleme"
description: "Eine grobe Übersetzung zu einem kritischen Artikel zu Android."
date: 2012-04-24
category: it
tags: [Android]
published: true
comments: false
share: true
---

Dieser Eintrag ist eine grobe Übersetzung [eines Artikels von Jay Yarow in der Business Insider vom 21. April](Android is Suddenly in a Lot of Trouble. http://www.businessinsider.com/android-is-suddenly-in-a-lot-of-trouble-2012-4). Seine Ausführungen sind nicht neu, aber sie bilden meiner Meinung nach eine sehr gute und genaue Zusammenfassung der aktuellen Lage von Android.     
Einiges habe ich auch schon vor Monaten in ähnlicher Form beschrieben und kritisiert, anderes sind aktuelle Zahlen, die den Trend bestätigen.

Die *mobile Erfolgsstory* des letzte Jahres war der *Aufstieg von Android*. Momentan sieht es dagegen für das mobile Betriebssystem nicht so rosig aus. Android treibt in gefährlichen Gewässern und wird bald vor großen Problemen stehen.

## Erfolg ohne Android

Beginnen wir bei der wichtigsten Nachricht des Jahres: Facebook kauft Instagram für 1 Milliarde Dollar. Das ist eine schlechte Nachricht für Android, denn es zeigt, dass Entwickler ein Produkt bauen können, welches enorm erfolgreich ist und nach dem alle verrückt sind, obwohl es das Android-System nicht unterstützt.       
An die Zweifler: Facebook machte Instagram schon lange vor der offiziellen Android-App Angebote und auch ohne Android hätten sie mit Sicherheit weit über 500 Millionen Dollar bekommen.
Letztlich ist das eine klare Ansage an Entwickler: Zuerst für iOS und danach für Android programmieren.      
Diese Entwicklung setzt schon ein. Das Entwicklerinteresse für Android geht bereits zurück. Und wenn die Entwickler Android als zweitklassig betrachten, werden die Verbraucher es auch bald tun.

Das nächste große Problem für Android ist die Unfähigkeit der Hersteller im Tablet Markt Fuß zu fassen. Sie hatten die Chance iOS abzuhängen und die Führung in dem Wettstreit zu übernehmen, aber das ist nicht ansatzweise passiert.
Das bedeutet letztlich auch, dass der Hauptgrund für den Erfolg Androids viel eher die Restriktionen der Mobilfunkprovider waren. Android war so erfolgreich, weil man ein iPhone sehr lange Zeit nur bei AT&amp;T (in Deutschland bei der Telekom) erhalten konnte. So toll das iPhone auch ist, nicht jeder wollte sofort seinen Provider wechseln.      
Der Erfolg des iPad gegen ein Dutzend Android Tablets zeigt deutlich, dass in einem Markt, in welchem beide Systeme auf dem gleichen Level agieren, iOS dominanter ist.

## iOS dominiert den Markt

Wenn der Verbraucher sich einfach das beste Gerät aussuchen kann, ohne dafür Mobilfunkverträge abzuschließen o.ä., wählt er fast immer das iOS Gerät.      
Das sollte Android ziemliches Unbehagen bereiten, denn es zeigt, dass selbst in den Augen der Verbraucher Android noch lange nicht Apple eingeholt hat. Ob das daran liegt, dass das Betriebssystem schlechter ist oder weil Apple besseres Marketing macht, ist völlig egal. Es ist nicht gut für Android.

Es gab bisher nur ein erfolgreiches Android Tablet: Der Kindle Fire. Doch auch das ist schlecht für Android.      
Die guten Verkaufszahlen des Kindle Fire beweisen, dass man nicht die *Akzeptanz* von Google braucht um ein gutes Android Produkt zu entwickeln. Dass man also nicht mit Google zusammenarbeiten muss und auch nicht die neusten Versionen von Android braucht. Das wird letztlich dazu führen, dass immer mehr Unternehmen ihre eigenen Android-Geräte herstellen, das Android System somit weiter fragmentieren und Google völlig die Kontrolle über *sein System* verliert.

Wir sprachen vor einigen Wochen mit einer Quelle aus der Industrie und er sagte uns, dass dieses Jahr das *Jahr der Android Zersplitterung* wird. Dass also die Basis von Android verwendet wird, aber obendrauf etwas völlig anderes entsteht, wie es Amazon beim Kindle Fire gemacht hat.      
Große Mobiltelefonhersteller, welche nicht nur *weitere Android Handlanger* sein wollen, werden beginnen ihre eigenen Android-Anpassungen auf die Telefone zu bringen und es dabei belassen. Das ist das Risiko ein offenes System zu haben - Google kann die Kontrolle seiner Plattform verlieren.

Eine weitere Quelle verriet uns, dass die großen Hersteller sehr aufgebracht über Googles Entscheidung, Motorola zu kaufen, sind. Sie befürchten, dass Google versucht wie Apple zu werden und seine eigene Hardware und Software herstellen könnte. Dadurch wird sich Google gewissermaßen von seinen großen Partner entfremden.       
Die gute Nachricht für Google: Seine Partner haben gar keine Wahl. Sie kommen nicht an iOS ran und Microsoft ist so eng mit Nokia verbunden wie es Google bald mit Motorola sein wird.
Wie auch immer, ihre Ärger darüber wird sich in der weiteren Zersplitterung von Android bemerkbar machen. Sie werden von Partnern von Google zu Rivalen von Google. Selbst wenn sie dabei scheitern, wird das System Android weiter fragmentiert.

## Die Provider ziehen sich zurück

Aber nicht nur die Hersteller rebellieren, auch einer von Google Schlüssel-Partnern, der Mobilfunkprovider Verizon, hat bekannt gegeben, dass sie ihre Bestes tun werden um Android zu untergraben, sinngemäß. Ihr CFO teilte mit, dass Verizon plane in Zukunft hauptsächlich Windows Phone zu unterstützen. Sie halfen dabei Android zu einer unglaublichen Plattform zu machen und wollen nun das gleiche für Windows Phone tun.
Es ist nicht nur Verizon, auch AT&amp;T gab kürzlich 150 Millionen Dollar zur Vermarktung von Windows Phone aus. Wir sind zwar skeptisch, ob Mobilfunkprovider in der Tat die Nachfrage der Verbraucher beeinflussen können, aber allein die Tatsache, dass sie sich öffentlich von Android distanzieren ist ein schlechtes Zeichen für Google.

Die meisten Punkte, die bisher genannt wurden, sind eher nebensächlich. Im folgenden sind einige Zahlen aufgeführt, die die Android-Entwickler beunruhigen.      
    
* Die letzte Version von Android wurde von Verbrauchern wie auch Herstellern völlig ignoriert.
* Die offiziellen Daten von Google belegen, dass nur 2,9% aller Android Geräte auf *Ice Cream Sandwhich* bzw. Android 4.0 laufen. 
* Der Großteil (63,7%) hat Gingerbread (2.3) installiert und danach folgt Froyo (2.2) mit 23,1%. 
* Das bedeutet, ungefähr 86% aller Android Benutzer haben ein veraltetes System!

## 86% aller Android Geräte veraltet

Unabhängig davon, ob sie sich einfach nur kein neues Android Smartphone kaufen oder ihr Smartphone kein Update erhält, es ist ein Desaster für Google. Besonders da so viel getan wird um mit jedem Update ein noch besseres und sicheres Betriebsystem zu entwickeln.       
Das führt auch zu dem Problem, dass iPhone Benutzer stets die tollsten Features eines neuen System genießen können, während Android Benutzer bei einem System festhängen, welches Jahre hinterher hinkt.

Hier ist eine weitere interessante Zahl: Verizon gab bekannt, dass sie in diesem Quartal 3,2 Millionen iPhones und insgesamt 6,2 Millionen Smartphones verkauft haben. Das bedeutet, es wurden mehr iPhones verkauft, als alle anderen Mobiltelefone (samt verschiedenen. Systemen) zusammen. Die beiden anderen großen Provider werden sehr wahrscheinlich ähnliche Zahlen vorlegen.       
Anders formuliert: Bei den drei größten amerikanische Providern ziehen die Verkaufszahlen von iPhone an denen von Android vorbei. Stagnierten letztes Jahr eher die Marktanteile von Apple, hat sich nun das Blatt gewendet.

Verbindet man diese Tatsache mit den benannten Problemen: die schwindenden Entwickler, wütende Hersteller, ein viel zu sehr fragmentiertes System und Provider, die lieber Windows unterstützen wollen, erkennt man langsam, dass Android auf unsicherem Boden steht.

**Hinweise zur Übersetzung:** Ich bin kein Profi. Einige Dinge habe ich umgeschrieben, verkürzt oder angepasst, aber die Grundaussage ist unter Garantie die gleiche.

Mich schockieren besonders zwei Fakten: Die fehlende Unterstützung der Hersteller für neue Versionen und das völlig konkurrenzlose iPad. An beiden muss sich bald etwas ändern, sonst wird Android Apple in naher Zukunft keine Paroli bieten können.