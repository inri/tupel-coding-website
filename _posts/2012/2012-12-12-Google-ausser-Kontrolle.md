---
layout: post
title: "Google außer Kontrolle?"
description: "Wie Google eine marktbeherrschende Position als Tor zum Internet missbraucht."
date: 2012-12-12 
category: it
tags: [Google]
published: true
comments: false
share: true
---

Ich mag reißerische Aufmacher auch nicht besonders, aber je mehr ich über die folgende Thematik nachdenke, desto düsterer scheinen mir die Folgen aus dieser Entwicklung zu sein. 


Mir geht es um Google und wie diese Firma ihre Macht über den Markt der Suchmaschinen (aus)nutzt. Was ich hier *kurz* beschreiben werden, sind keine Geheimnisse oder Verschwörungstheorien. Mir war das alles vorher bekannt, wenn auch nicht zusammenhängend und strukturiert in Beziehung gesetzt. Anstoß für meine Überlegungen war der [lange Bericht](http://pip.net/monopoly) von Philipp Klöckner auf pip.net, den man auf jeden Fall lesen sollte um die Einzelheiten und Argumentationsstrukturen besser nachvollziehen zu können. 

## Ich google das mal...

In der westliche Welt ist das Wort *Googlen* zum Inbegriff der Suche nach Informationen mit Hilfe der Suchmaschine Google geworden. Für den Großteil der Menschen ist Google der Einstiegspunkt des Internets. Allein in Deutschland werden 96% aller Suchanfragen über Google aufgegeben. Oder anders ausgedrückt: Entweder kennt jemand die Adresse der Website, die er besuchen möchte, oder er sucht mit Google nach ihr. Dazwischen gibt es praktisch nichts. 

Damit fängt das erste Problem an: Eine einzige Firma entscheidet allein darüber, wer oder was gefunden wird. Diese Entscheidung passiert einerseits durch Googles mysteriöse Algorithmen, die in regelmäßigen Abständen angepasst und verändert werden und andererseits durch bewusste Beeinflussung der Suchergebnisse. Pornographie ist beispielsweise nicht erwünscht, oder andere Inhalte je nach Land aus dem die Suchanfrage kommt, Stichwort *Zensur*.

Jemand muss einen Algorithmus zur Verfügung stellen und dieser muss geheim sein, sonst nützt die Suchmaschine nichts. Doch schon heute sind wichtige Kriterien, um eine Website möglichst weit oben bei Google zu ranken, relativ bekannt, was dazu führt, dass eventuell nicht die beste Seite für den Suchenden, sondern die beste Google-optimierte Website auf Rang 1 erscheint. Wobei man auch trefflich darüber streiten kann, was für den Suchenden die beste Seite sein kann.

Bevor wir zum Stein des Anstoßes kommen, kann man erst einmal nach den Zielen von Google fragen. Meines Erachtens ist das Bereitstellen von *guten* Suchergebnissen eben nicht das Hauptziel von Google. Doch genau das müsste das Ziel einer Suchmaschine sein. Google hingegen ist mittlerweile ein riesengroßer Konzern und solchen Konzernen geht es vorwiegend um eine Sache: Profit.       
Apple mag noch so oft betonen, dass ihnen ein gutes Produkt wichtig ist, aber das bringt sie nicht dazu, die Preise für ihre Produkte zu senken. Ich mag Apple Produkte, sie sind ihr Geld Wert und können auch teurer als die Produkte der Konkurrenz sein. Trotzdem möchte Apple hohe Gewinnspannen erzielen und hat beispielsweise die Preise des neuen iPhones im Vergleich zum Vorgänger leicht erhöht.

## Geschäftsmodell Werbung

Wie bekannt sein sollte, verdient Google sein Geld vorwiegend aus Werbeeinnahmen. Man kann bei Google einen Werbeplatz kaufen in dem man in Googles AdWords angibt, bei welchen Suchbegriffen die eigene Seite als Werbung eingeblendet werden soll. Dabei landen die Werbeeinblendungen entweder rechts von den Suchergebnissen oder genau darüber in einem leicht violettem Kästchen, wobei sie vom Aufbau her ganz normalen Suchergebnissen ähneln.       
Hierbei ist besonders problematisch, dass diese Werbekästchen sehr viel Platz der dargestellten Suchergebnisse einnehmen, teilweise über 50% des Bildschirms. Außerdem ist einer großen Anzahl Menschen nicht der Unterschied zwischen Werbung und Suchergebnissen bewusst. 

Hier hat man die Entwicklung dahingehend, dass dem Suchenden nicht die besten Suchergebnisse, sondern die erkaufte Werbung angezeigt wird. Ähnlich positioniert sich Google auch im mobilen Bereich. Zumindest wurde das Betriebssystem Android von Google nicht aus reiner Nächstenliebe gekauft, weiterentwickelt und kostenlos angeboten.       
Die Alternative wären Deals über die Verwendung von Google als Suchmaschine bei Mobilfunkanbietern oder Entwickler von mobilen Betriebssysteme gewesen, siehe Googles Spenden an Firefox. Mit Android als momentan am weitesten verbreitete System steht Google ziemlich gut da. Es kann Werbung auch auf mobilen Plattformen verbreiten und zudem eigene Produkte prominent direkt im System platzieren.

Wofür Microsoft noch rechtlich belangt wurde, nämlich einen vorinstallierten Browser in ihren Windows-Systemen anzubieten, ist mittlerweile bei Android und Apple mit Mac OS X und iOS Gang und Gäbe.      
Doch wenn man sich die Vormachtstellung von Google näher anschaut, wird einem schnell bewusst dass die Firma in *ihrem Markt* kaum noch Möglichkeiten zum Wachsen hat. Das ist nicht neu für Google, ständig experimentieren sie mit neuen Möglichkeiten ihre Einnahmen zu vergrößern. Doch die Methode von Google stinkt gewaltig.

## Ein Beispiel: Onlineshop für Kerzen

Das ist genau die Methode, die der Autor Philipp Klöckner schon mehrfach bemerkt und selbst miterlebt hat. Ich beschreibe das mal an einem Beispiel: Angenommen sie haben einen Onlineshop für Kerzen. Neben ihren Onlineshop gibt es noch ungefähr ein Dutzend weiterer solcher Onlineshops, aber ihrer ist zum einen Google-optimiert und hat zum anderen viele Kunden und Klickzahlen. Deshalb sind sie zurecht auf Rang 1 bei Google.       

Google weiß wie viele Besucher ihres Onlineshops von Google kamen und kann so ganz leicht Trends erkennen und analysieren. Das ist nicht weiter schlimm. Nun beschließt Google ein Update des Auswertungsalgorithmus einzuspielen, welches angeblich dieses und jenes an den Suchergebnissen für den Suchenden verbessert, aber in der Praxis dazu führt, das ihr Onlineshop und der ihrer Konkurrenten plötzlich um viele Ränge fallen.
Das kann ja passieren.       
Doch kurz danach beschließt Google ebenfalls ins Kerzenbusiness einzusteigen, weil sie anhand der gesammelten Daten herausgefunden haben, dass dieser Markt eventuell besonders attraktiv und gewinnversprechend ist. Google kauft einen ihrer Konkurrenten von den hinteren Plätzen, überarbeitet seine Seite und bringt kurze Zeit später einen neuen Google-Shop für Kerzen auf den Markt. Dieser unterscheidet sich zwar kaum von den anderen und er hat auch immer noch viel geringere Klickzahlen und Zugriffe, aber er wird in der Suche ganz oben gelistet. Das ist aber seltsam.

## Konkurrenz verdrängen

Dieses Verhalten von Google konnte man schon mehrfach beobachten, beispielsweise als sie DailyDeal und SparkBuy kauften und kurz zuvor sämtliche Preisvergleichseiten abgewertet hatten. Eine Idee wird von Google aufgekauft und dann vermarktet, wobei die Konkurrenz natürlich schlecht wegkommt.      
Das ist für eine Firma, ein Unternehmen mit dem Ziel Gewinn zu erzielen nichts überraschendes. Aber es kollidiert erheblich mit den theoretischen Absichten und Aufgaben einer Suchmaschine. Vor allem wenn Google neues Produkt schlechter als die der Konkurrenz ist. Das ist beispielsweise beim Vergleich der Suchvolumen zwischen Google Flights und *Kayk* oder Google Shopping und *Ladenzeile* deutlich zu sehen.

Bei Apple haben wir im Prinzip das gleiche Phänomen wenn es um den App Store geht. So haben beispielsweise die Apps von Apple mehr Möglichkeiten und Rechte auf der Plattform als die der Drittanbieter. Oder die oben genannte Tatsache, dass Apple viele seiner eigenen Apps fest auf den Geräten vorinstalliert. Es gibt aber einen erheblichen Unterschied zwischen Google und Apple.     
Als Entwickler und Nutzer hat man Alternativen, mehr oder weniger. Ich kann zu Android oder Windows Phone als Plattform wechseln. Jedoch das Internet mit Google als Torwächter ist nicht wechselbar. Wenn ich eine innovative Idee für einen Webdienst verwirklichen möchte, kann ich mich vor Google verbergen (robots.txt), aber dann bin ich für die meisten Internetnutzer ebenfalls unsichtbar. Zeige ich mich und buhle um die Aufmerksamkeit der Suchenden, registriert auch Google meine Anwesenheit und meine Idee. Und sie haben die Macht darüber, mich zu verdrängen.

Dagegen kann man nichts machen, weil es keine Beschwerdestelle gibt. Niemand schaut dem Konzern auf die Finger, niemand kann ihn maßregeln. Dabei gibt es keine Alternativen für die Menschen. Entweder man spielt mit Google, oder man lässt es sein.

Amazon macht es übrigens ganz ähnlich. Sie bieten externen Verkäufern durch den Marketplace eine Plattform und können somit sehen welche Marktsegmente sich wie entwickeln und erfolgreich sind und entsprechend bei sich selber Anpassungen vornehmen. So sparen sie sich die Mühen der eigenen Marktforschung.

Interessant finde ich die Überlegung über Google und die wichtigen Innovationen. Keine Frage, Google wird von vielen Menschen hoch geschätzt, es sei ein innovatives Vorzeigeunternehmen und hat das Internet wesentlich mitgestaltet. Auch wenn das in großen Teilen korrekt ist, muss man Google diesbezüglich mit einem kritischen Blick beäugen. 
Es zeichnet sich nämlich der Trend ab, dass Google fremde Innovationen übernimmt bzw. kopiert und mit Hilfe seiner Suchmaschinen-Macht besser am Markt zu positionieren. So wirkt Google mehr und mehr innovationhemmend.

## Google und Facebook verstaatlichen

Abschließend noch eine mögliche Konsequenz aus der großen Verbreitung von Google und auch Facebook. Denn im sozialen Bereich ist ohne Frage Facebook der Platzhirsch. So wie es heute neben Google keine alternative Suchmaschine gibt, wird es schon bald kein anderen Netzwerk als Facebook geben, zumindest keines, welches sich als sozial bezeichnen kann.

Wenn irgendwann Personalmanager potentielle Arbeitnehmer auf FB überprüfen, man keine Wohnung mehr mieten kann ohne dass der Vermieter einem nicht vorher unter die soziale Lupe genommen hat,... es gäbe noch viele weitere Beispiele. Dann muss man sich Gedanken darüber machen, wie viel Macht man diesen Unternehmen in die Hand gibt.     
Eine mögliche Konsequenz wäre eine Art Verstaatlichung, wie auch immer diese aussehen soll. Auf jeden Fall ist anhand von dem oberen Beispiel gezeigt, dass schon jetzt zu viel Macht in den Händen von Google liegt.

Eigentlich ist es interessant, dass zu viel Macht für ein Person, ein Land, ein Unternehmen, stets zu einem Missbrauch dieser Macht führt. Oder gibt es in der Geschichte Beispiele, bei denen das von Anfang bis zum Ende nicht der Fall war?