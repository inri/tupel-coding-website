---
layout: post
title: "Kurz zum Surface"
description: "Kurze Einschätzung zum Surface."
date: 2012-10-26 
category: it
tags: [Microsoft]
published: true
comments: false
share: true
---

Was ist nun aus der Ferne von diesem neuen Tablet zu halten? Erst einmal muss man Microsoft loben. Sie haben sich wirklich viele Gedanken gemacht und ihre eigene Lösung entwickelt. In allen Reviews wurde das Design gewürdigt und ich denke das ist ein deutliches Zeichen in Richtung der iPad-Kopien: Ja, man kann auch Tablets bauen, die nicht wie das iPad aussehen.

Nicht nur für die Kopierer ist das Surface beschämend, sondern auch für die anderen Tablets auf dem Markt, ausgenommen das iPad natürlich. Microsoft, eine Software-Firma, baut seinen ersten Computer und macht das wesentlich besser als es diverse Hardware-Hersteller seit dem iPad versucht haben. Meiner Meinung nach versagen die Hersteller seit 2,5 Jahren am Laufenden Bande. Lediglich Amazon und Google zusammen mit Asus zeigen, dass es besser geht.  

## Die Software der Medaille 

Das Surface ist ein tolles Gerät mit einigen Kinderkrankheiten. Aber das ist nur die eine Hälfte der Geschichte. Die andere ist für Microsoft wesentlich kritischer. Erst einmal das positive: Windows 8 lässt sich auf Tablets gut bedienen. Dafür war es auch gemacht und so sollte es auch sein.       
Die erste Verwirrung entsteht, wenn man den Unterschied zwischen Windows 8 und RT erklären muss. Ein großes, wenn nicht sogar das größte, Problem ist aber die mangelhafte Auswahl an Apps im Windows Store. 

Sicherlich werden in den nächsten Monaten zahlreiche neue erscheinen und auch die *Standard*-Tablet-Apps finden ihren Weg auf das Gerät. Aber wird Microsoft in absehbarer Zeit zum App Store aufschließen können? Als die iOS-Entwickler mit dem ersten iPad eine neue Größe vorgesetzt bekamen, wurden zunächst viele Apps auf einfache Art und Weise übertragen, vom iPhone zum iPad. Erst nach und nach entwickelten sich neue, eigenständige Apps, die tatsächlich genau und nur für das iPad konzipiert wurden.       
Wenn Microsoft in einem Jahr die meisten bekannten Apps in seinem Store vorweisen kann, werden noch bessere und noch schönere Apps für das iPad erschienen sein. Ich stelle mir wirklich die Frage, wie Microsoft diesen Vorsprung aufzuholen gedenkt.

## Eines für alles

Im Gegensatz zu Apple, nutzt Microsoft nur ein Betriebssystem für Tablet und Desktop PCs. Auch wenn die aktuellen Surfaces ARM-beschränkt sind, laufen Modern-Style-UI Apps hier und da. Aber ist das zielführend? Einerseits generiert Microsoft dadurch eine große Anzahl potentieller Kunden, andererseits wird es zu schlechten Apps führen.      
Wenn Programmierer faul sind (und das sind sie) und zunächst nur ein Version ihrer App erstellen, werden die Apps Potentiale auf einer der beiden Plattform verschenken müssen.     Es mag später möglich sein, generisches Design zu integrieren, aber das sind dann genau diese qualitativ hochwertigen Apps, die es im ersten Jahr nicht oder nur sehr selten geben wird. Hersteller wollen ihre Apps erst einmal auf die Plattform bringen und das muss auf Kosten der Surfaces oder Desktops gehen. 

Ich lasse mich gerne eines besseren belehren. Aktuell kann ich mir aber einfach nicht vorstellen, dass plötzlich tausende Windows-Entwickler quasi aus dem Nichts erscheinen. Ich meine, es gibt das Windows System schon eine ganze Weile, doch die Anzahl wirklich guter Programme für Privatanwender ist überschaubar. Ich bin mir gerade nicht sicher, ob ich diesen Post schon geschrieben habe oder nicht, es gibt meines Erachtens für jeden Bereich nur drei brauchbare Programme auf Windows. Ich würde sogar sagen, dass es für Mac OS X einige Programme gibt, die sich auf Windows einfach nicht finden lassen.       
Also auf der einen Seite steht das alte Windows, welches trotz seiner immensen Verbreitung in der Programm-Auswahl nicht überzeugend war, und auf der anderen Seite die vielfältige App-Landschaft des iPhones. Das sind die Startbedingungen der Tablet-Konkurrenten. 

Ich kann mir nicht vorstellen, dass es optimal für Windows 8 laufen wird. Ich würde ihnen ein erfolgreiches Tablet aber gönnen. So ist Apple wieder unter Zugzwang und bei Android sieht man hoffentlich auch Fortschritte.