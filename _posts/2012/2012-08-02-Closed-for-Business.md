---
layout: post
title: "Closed for Business"
description: "Was Matt Gemmel zur Android Plattform zu sagen hat."
date: 2012-08-02 
category: it
tags: [Android]
published: true
comments: false
share: true
---

Seit einigen Tagen wird das mobile Spiel Dead Trigger im Android Store kostenlos angeboten, der Grund dafür ist [laut Entwickler](http://www.theverge.com/2012/7/23/3177238/dead-trigger-free-android-piracy-rate) die Tatsache, dass es sowieso größtenteils raubkopiert wird. Im App Store von Apple kostet es weiterhin 1$, der gleiche Preis wie vorher im Android Store. Ich las einen [längeren Artikel von Matt Gemmell](http://mattgemmell.com/closed-for-business/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+mattgemmell%2Frss2+%28Matt+Legend+Gemmell+-+RSS2%29) und darin fanden sich viele gute Aussagen. 

Einige davon werfe ich einfach mal so hin, ohne Kontext, ohne Kommentar, aber übersetzt. Mir geht es nicht so sehr um die inhaltliche Diskussion, obwohl ich bei dem meisten zustimmen würde. Diese Sätze kommen in ihrer kurzen, ehrlichen Form wirklich so in seinem Text vor bzw. besteht sein Text fast nur aus ihnen.

*Ja, Android ist auf vielen Geräten. Das ist nett. Aber die wirklich wichtige Frage ist: Kannst du als Entwickler damit Geld verdienen?*

*Wenn du nicht wegen des Geldes im App Business bist, dann ist das großartig - Gratulation. Hier ist deine Haltestelle. Machs gut. Hab ein schönes Leben.*

*Android wurde so designed, dass es schwierig ist damit Geld zu machen und das Hauptproblem ist seine Offeneheit.*

*Eine App im Android Market zu kaufen ist genauso einfach wie in iOS [...]. Das ist also keine Piraterie aus Frustration.*

*Du suchst im Internet nach Kopien von Apps, kopierst sie auf dein (nicht gerootetes) Gerät und startest sie. Das System ist von Grund auf für Piraterie konzipiert.*

*Die Existenz von Piraterie ist nicht überraschend, sie ist unvermeidlich.*

*Da wird es diejenigen geben, die damit argumentieren, dass sie ihr Mobiltelefon kostenlos bekommen haben oder ihren Email Service. Nein hast du nicht. Nichts ist kostenlos. Es kostet mindestens Zeit und das bedeutet immer auch, dass es Geld gekostet hat (denn niemand verschenkt Zeit).*

*Irgendjemand hat die Pfeifen [?] bezahlt und üblicherweise bist du das gewesen - auch wenn du es nicht bemerkt hast.*

*Schlechtes Benehmen muss schwieriger sein als gutes Benehmen - und gutes Benehmen bedeutet für Software zu bezahlen.*

*Niemand denkt darüber nach, ob &quot;Wahl/quot; vielleicht ein schlechtes Wort ist.*

*Nerds sagen gerne, dass den Leute wichtig ist eine Wahl zu haben. Nerds liegen falsch. Nerds ist es wichtig eine Wahl zu haben, aber Nerds sind eine so kleine Minderheit, dass sich niemand dafür interessiert, was sie denken.*

*Android besteht aus viel zu viel Nerd Philosophy.*

*Eigene Anpassungen machen zu können ist vor allen den Leuten sehr wichtig, die auch sehr große Probleme mit dem haben, was sie selbst als mangelhaft und ineffizient betrachten. Diese Leute haben meist auch einen großen Mangel an Vorstellungskraft, dass andere eventuell nicht ihre Position teilen.*

*Sie brauchen immer eine Wahl.*

*Aber Freiheit ist nicht gut, wenn man zu viel davon hat. Genau wie mit Zucker, Wasser oder Luft.*

*Wir [kleine] Entwickler haben keinen reichen Papi wie Mozilla. Wir haben kein Betriebssystem für welches wir ein bezahltes Support Model anbieten können.*

*Wir Entwickler wollen einfach nur Apps machen, dann genug von ihnen verkaufen damit wir neue machen können.*

*Offen ist ein Ideal, so wie wahre Demokratie, welche sich warm und angenehm anfühlt, aber in der Praxis unmöglich ist.*

*Du bist nicht nur ein Nerd, der unbedingt die Möglichkeit haben muss die Ordner-Icons seines Telefon ändern zu können.*

*Du musst Rechnungen zahlen. Das Leben ist ernst. Wähle eine Plattform die das weiß.*

*Geschlossen ist besser für das Geschäft.*

**Nachtrag**     
Matt Gemmell hat sich mittlerweile aus der Softwareentwicklung zurückgezogen und schreibt hauptberuflich. 