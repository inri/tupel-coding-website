---
layout: post
title: "Ruhezustand beim MacBook steuern"
description: "Die Lösung für einige Seltsamkeiten beim MacBook ist das manuelle einstellen der StandBy Zeit."
date: 2012-12-26 
category: it
tags: [MacBook]
published: true
comments: false
share: true
---

Ich hatte schon über einige Seltsamkeiten meines MacBooks beim Verlassen des Ruhezustands geschrieben. Es sind genau zwei Dinge, die mich ein wenig stören und die ich jetzt eventuell gelöst habe. Im folgenden erkläre ich die Phänomene und natürlich auch die Lösungen.

## StandbyDelay

Man arbeitet mit dem MacBook und klappt es einfach nur zu. Kein Herunterfahren, einfach nur Schließen. Einige Stunde später möchte man es wieder benutzen, klappt es auf und muss einige Sekunden warten bevor der Bildschirm für die Login-Eingaben erscheint. Erst einmal ist es wichtig zu wissen: Das ist normal und vollkommen okay.      
Denn beim Zuklappen geht der Mac in den Ruhezustand. Die Daten bleiben dabei im Arbeitsspeicher und das sorgt dafür, dass beim erneuten Aufklappen sofort alle Programme wieder geöffnet sind.

Die beschriebene Situation tritt erst nach einigen Stunden ein. Und zwar kann der Mac mehr Strom sparen, wenn er den Arbeitsspeicher nicht mit Strom versorgen muss. Da die Daten aus dem Arbeitsspeicher gelöscht werden, wenn er kein Strom erhält, legt OS X die Daten auf dem Arbeitsspeicher auf der Festplatte ab und spart sich den Strom dafür. Holt man den Mac aus dem Ruhezustand, schreibt er die Daten von der Festplatte zurück in den Arbeitsspeicher und genau das dauert leider einige Sekunden.

Nun sollte man diese Funktion nicht deaktivieren, denn sie ist durchaus sinnvoll. Aber man kann die Zeit bestimmen, zu der die besagte Funktion tätig wird. Ich habe sie auf 6 Stunden gesetzt, weil ich öfter mal den Mac für zwei oder drei Stunden schließe. Initial ist die Zeit sehr gering, ich glaube eine Stunde oder so.

Der Befehl dafür ist:

	sudo pmet -a standbydelay [Zeit in Sekunden]

Zeit in Sekunden für 2 Stunden =&gt; 2 x 60 x 60 = 7200

## AutoPowerOffDelay

Grundsituation wie bei oben, nur dass nach dem erneuten Holen aus dem Ruhezustand der Mac entweder runtergefahren ist und neu gestartet werden muss oder das Runterfahren von irgendwelchen Programm verhindert wurde und entsprechende Meldungen erscheinen, was ich sehr nervig finde.

Verantwortlich dafür ist die Zeit bis zum automatischen Herunterfahren. Klar, wenn der Mac eine gewisse Zeit lang im Ruhezustand war, spart er am meisten Strom, wenn er heruntergefahren wird. Wenn kein Programm dies verhindert, ist es auch nicht weiter tragisch. Aber wiedermal ist die voreingestellte Zeit viel zu kurz für meinen Geschmack. 

Ich habe sie auf 12 Stunden gesetzt:

	sudo pmet -a autopoweroffdealy [Zeit in Sekunden]

Und mit dem Befehl

	pmset -g

kann man sich weitere Einstellungen anschauen.

## Vermutungen

Ich habe keines der beiden Phänomene bei meinem ersten MacBook bemerkt. Ersteres spürt man auch erst, wenn der Mac verdammt schnell aus dem Ruhezustand kommt. Jede Langsamkeit fühlt sich viel deutlicher an. Ich weiß auch nicht genau, wie *neu* das automatische Herunterfahren ist. 

Eine Garantie kann ich natürlich nicht geben. Das erste Problem scheint aber nicht mehr aufzutreten.