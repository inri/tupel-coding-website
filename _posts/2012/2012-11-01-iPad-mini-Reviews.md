---
layout: post
title: "iPad mini Reviews"
description: "Eine Zusammenfassung diverser iPad mini Reviews."
date: 2012-11-01 
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Der iPhoneBlog hat [eine längere Zusammenstellung](http://www.iphoneblog.de/2012/10/31/die-ersten-ipad-mini-reviews/) der englischsprachigen Reviews veröffentlicht. Die Tester hatten das iPad mini seit einer Woche in Benutzung und sind durch die Bank weg angetan. Die Hersteller der anderen 7 Zoll Tablets können froh sein, dass Apple 329€ verlangt und nicht mit 200€ in den Preiskampf einsteigt. Denn das wäre vielleicht fatal für sie gewesen.

## Leicht gegen Pixel

Sehr gelobt wurde das schlanke, leichte Design des kleinen iPads. Damit hebt es sich von den Konkurrenten ab, die dagegen eher wie Plastikspielzeuge wirken. Das Nexus 7 im Vergleich zum Fire HD gibt mir zumindest schon eine Ahnung von diesem Gefühl. Beide sind in der gleichen Preis- und Leistungsklasse, aber trotzdem unterschiedlich in der Verarbeitung.       
Dass das kleine iPad nun wesentlich mobiler ist, stach für die meisten Reviewer sogar den Nachteil des schlechteren Bildschirms aus. Es sei sehr schade, dass der Bildschirm nicht Retina sei, aber so schlecht ist er auch nicht. Kontrast und Farben sind gut und wenn man in den letzten Monaten nicht gerade ein Retina iPad benutzt hat, ist er vollkommen okay. 

John Gruber wunderte sich über Apples Entscheidung, das alte iPad 2 trotzdem noch anzubieten und seine Erklärung dafür klingt logisch: Es verkauft sich einfach noch sehr gut. Ich mag mein iPad 2 auch und kann nicht sagen, dass es in Sachen Leistung abgenommen hat. Der Vorteil eines neuen iPad besteht im Retina Display, mehr Leistung (die es für den Bildschirm aber auch braucht) und besseren Kameras. Aber scheinbar können viele Menschen auf diese Features verzichten und sparen lieber die 100€.      
Nun haben sie ein weiteres Gerät zur Auswahl, welches noch mal 70€ weniger kostet und technisch teils besser oder mindestens mit dem iPad 2 gleich auf ist. Ja, es hat einen kleineren Bildschirm, ist aber auch nur halb so schwer. Es werden die gleichen Inhalte wie auf dem iPad 2 dargestellt und das ist vollkommen ausreichend. 

Ich kann mir sehr gut vorstellen, dass Apple viele minis verkaufen wird.

## Das Ökosystem entscheidet

Schaut man sich nur allein die Hardware der 7 Zoller an, kann man verschiedene Kompromisse finden. Für 130€ weniger bekommt man ein klobiges Gerät, welches zwar einen besseren Bildschirm hat, aber eben auch mehr wiegt. Für 130€ mehr erhält man ein leichtes Gerät, dessen Verarbeitung beispiellos ist, eine sehr lange Akkulaufzeit hat und dessen Display der einzige Kritikpunkt ist. 

Aber entscheidend ist das System und nach wie vor sieht die Gleichung so aus: Apple schlägt Google schlägt Amazon. Ganz einfach, ganz simpel. Ja, ich bin davon überzeugt, dass in Sachen Apps für Tablets bei Android langsam der Stein ins Rollen kommt. Bei Amazon sehe ich weniger gute Chancen, denn ihre gewollte Abschottung von der Android-Welt zwingt Entwickler in den Amazon Store.        
Gute Apps werden folgen, aber zuerst auf dem iPad, später auf Android und erst danach auch bei Amazon.

## Wechsel zum iPad mini

Wirklich überrascht war ich von der Aussage, dass viele sofort zum iPad mini wechseln würden, wenn es ein Retina Bildschirm hätte. Aber auch diesen Makel würden die Reviewer in Kauf nehmen und sich lieber für das schlankere iPad entscheiden. 
Es ist ein tolles Gerät und iOS läuft auch wunderbar.      
Gewöhnungsbedürftig sind die dünnen Seitenränder, was dazu führt, dass die Daumen auf dem Bildschirm liegen. Doch die Software wurde entsprechend angepasst, so dass es zu fast keinen Problemen kam.      
Apps, auch Spiele, lassen sich auf dem iPad mini sogar besser bediene und erwecken den Eindruck, sie wurden für den kleinen Bildschirm konzipiert.

Hoffentlich kann ich auch bald eines in die Finger bekommen. Dann gibt es den großen Vergleich zwischen Nexus 7, Kindle Fire HD und iPad mini.