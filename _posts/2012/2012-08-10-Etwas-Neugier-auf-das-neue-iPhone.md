---
layout: post
title: "Etwas Neugier auf das neue iPhone"
description: ""
date: 2012-08-10 
category: it
tags: [iPhone]
published: true
comments: false
share: true
---

In etwas mehr als einem Monat soll Apple angeblich das neue iPhone, ein neues kleineres iPad und überhaupt die ganze iOS-Familie rundum erneuert vorstellen. Während ich gespannt auf das neue mini-iPad bin und auch das überarbeitete Design der iPods mein Interesse weckt, kann ich für das iPhone nur etwas Neugier aufbringen.

Das liegt einerseits an den zahlreichen Informationen, die schon seit Monaten durch das Netz schwirren und andererseits an meiner großen Zufriedenheit mit dem iPhone 4S. Die Ankündigung von Tim Cook, als er dieses Jahr auf der D-Konferenz war, die Geheimhaltung um neue Geräte von Apple zu vergrößern, kann zumindest ich noch nicht als *in die Tat umgesetzt* betrachten [^1].    
Es ist schon einige Wochen seit dem ersten iPhone-Außenhülle Leak her und seit dem kamen immer mal wieder neue Teile ans Licht, die scheinbar ganz gut zusammenpassen. Ob sich alles bewahrheitet soll wir am 12. September erfahren.

Das neue iPhone wird:  
   
* etwas höher sein und dadurch einen größeren Bildschirm haben (um die 4 Zoll)
* einige Millimeter dünner sein, da eine neue Bildschirmtechnik zu Einsatz kommt
* einen neuen Anschluss haben, vermutlich eine Art MagSafe (ich dachte ja, die benutzen den neuen MagSafe 2 Anschluss)
* die Kopfhörer neuerdings an der Unterseite anstöpseln
* und ein Aluminium-Design haben, statt der Glasrückseite

Mich lassen diese Neuerungen kalt. Mich lassen auch sämtliche Android-Flaggschiffe kalt, die ja vor allem mit CPU, GPU, RAM und 4,x-Zoll-Bildschirmen Werbung machen. Vor knapp zwei Jahren waren Mobiltelefone teils etwas schwachbrüstig. Da war die Ankündigung eines besseren Prozessors wirklich eine gute Neuigkeit. Aber was interessiert es mich heute?      
Mein Smartphone soll einen tollen Bildschirm haben, knackige Fotos schießen, flüssig bedienbar sein und etwas Akkulaufzeit mitbringen. Und natürlich auf einem System mit vielen und vor allem qualitativ hochwertigen Möglichkeiten in Form von Apps basieren! Und genau das alles habe ich schon. 

Über Design kann man streiten. Ich denke dass die Hardware in halbwegs aktuellen Mobiltelefonen top ist und alle Bedürfnisse abdecken kann. Wie gesagt, mein iPhone flutscht wie Seife und man merkt einen deutlichen Unterschied zum HTC Desire oder dem Nexus One, mit dem ich gerade programmiere.      
Ich bin auch sicher, dass das bei den Android-Flaggschiffen der Fall ist. Selbst wenn 2 GB RAM verbaut sind und zwei 2 GHz CPU in dem Gerät schlagen: Es wird nicht besser. Die Software ist in Zukunft das Ziel und darin sollten auch die Gerätehersteller mehr investieren. Wegen mir können sie ihre Designs auch gleich überdenken, aber in technischer Hinsicht läuft es momentan echt gut für uns Verbraucher.

Das iPhone 4S war eine solide Weiterentwicklung. Bessere Leistung, keine Antennenprobleme, sehr gute Kamera - alles okay. Ich habe keine Wünsche, aber dass die Entwicklung weitergeht war ja klar. Vor allem die iPhone 4 Käufer warten begierig auf das neue Gerät, ihre 2-Jahres-Verträge laufen bald aus.     
Ich sehe aktuell auch noch nicht **das** tolle Feature, wie es Siri beim 4S gewesen ist. Ob es Apple allein auf das Redesign beruhen lässt? Oder das NFC-Zeug? Mal sehen.

Man kann gespannt sein, aber das wirklich große Ding folgt wohl erst Ende des Jahres.

[^1]: Interessant ist die Tatsache, dass die meisten handfesten Informationen nur das iPhone betreffen. Ansonsten ist noch kaum etwas über das kleinere iPad oder die Überarbeitung der iPods bekannt. Vielleicht ist vieles doch geheimer als ich ahne.
