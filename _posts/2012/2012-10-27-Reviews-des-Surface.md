---
layout: post
title: "Reviews des Surface"
description: "Zwei Listen zu den Pros und Kontras des Surface von Microsoft."
date: 2012-10-27 
category: it
tags: [Microsoft, Surface]
published: true
comments: false
share: true
---

Seit Dienstagabend durften die ersten Reviews des Surface-Tablet von Microsoft veröffentlicht werden. Mittlerweile habe ich mich durch ein halbes Dutzend englischsprachiger Berichte gelesen und fasse die positiven und negativen Aspekte zusammen. 


### Pro

* Gute, qualitativ hochwertige Verarbeitung.
* Es fühlt sich dank der Verarbeitung wie etwas Neues und Anderes an. 
* Es ist kein iPad-Klon.
* Der Standfuß ist äußerst geschickt und mit Liebe zum Detail verbaut.
* Obwohl der Bildschirm keine Retina-Auflösung hat, sieht er ganz gut aus. Bei Helligkeit und Schwarzwerte ist er sehr gut. 
* Akkuleistung wie versprochen relativ hoch, 10 h.
* Das Touch Cover ist wirklich gut. Nach einiger Zeit gewöhnt man sich an das Format.
* Das Type Cover ist noch einen Tick besser als das Touch Cover.
* Windows 8 ist ideal für das Surface und Tablets generell.
* Das Gesamtpaket ist prinzipiell durchaus in der Lage ein Laptop zu ersetzen. 
* Das 16:9-Verhältnis des 10,6 Zoll Bildschirm ist besser für Filme und stellt logischerweise mehr Inhalte als das iPad dar.
* Die Performance ist insgesamt gut.
* Das Surface hat eine ähnlich gute Performance wie das iPad, im Gegensatz zu den meisten Android Tablets.
* Wenn die Apps einmal gestartet sind, lässt sich problemlos zwischen ihnen hin und her wechseln (Multitasking).
* Dass ein USB-Anschluss überhaupt verbaut ist, wird sehr gelobt.
* Der USB-Anschluss unterstützt erstaunlich viele Geräte (treibertechnisch).
* Ebenfalls der MicroSD-Slot.
* Ein 24W-Ladeadapter wird mitgeliefert. Der ist zwar etwas größer als die Netzteile der Konkurrenz, lädt das Surface aber wirklich sehr schnell auf.
* Egal von welchem Betriebssystem man kommt, man hat eine Lernkurve mit Windows 8, aber man kann sich doch daran gewöhnen.

### Kontra

* Der Bildschirm ist aber bei weitem nicht der beste auf dem Markt, das iPad hat bessere Farben.
* Die Akkuleistung ist geringer als versprochen und geringer als beim iPad (7 h vs. 10 h)
* Nur USB 2.0.
* Es gibt noch nicht Treiber für alle USB-Geräte wie beispielsweise Drucker. Da muss der Kunde erst einmal warten.
* Das Touch Cover ist so klein, dass besonders zu Beginn viele Tippfehler gemacht werden. Man gewöhnt sich nach einiger Zeit immer noch nicht ganz an die Größe.
* Kostenaufschlag für das Touch Cover als Zubehör ist relativ hoch (120$ und für das Type Cover sogar 130$).
* Das Trackpad auf den beiden Covers ist nicht besonders gut und sehr klein.
* Der Stromanschluss ist zwar magnetisch, aber sitzt nicht so fest und dockt so einfach an wie der MagSafe Anschluss von Apple (bei MacBooks).
* Das Touch Cover ist nur für ebene Untergründe gedacht. Auf dem Schoss kann man das Surface daher nicht gut bedienen. Auch der Ständer spielt da nicht mit.
* Auf Windows RT können nur Apps aus dem Windows Store installiert werden, keine x86 Programme. Die Ausnahme sind einige Programme von Microsoft. Ob Kunden das verstehen?
* Beim Aufklappen des Touch Covers dauert es einige Sekunden bis der Bildschirm wieder angeht. Das iPad ist schneller.
* Der Store ist noch zu leer. Es mangelt an den wichtigsten Apps (z.b. Soziales) und einige von denen, die es gibt, laufen nicht so toll (Kindle).
* Beide Kameras sind mies.
* HDMI Output hat noch Probleme mit dem 1080p. 
* Das Starten von Apps auf dem Surface dauert etwas zu lange, länger als auf dem iPad. Könnte vielleicht auch an der Software an sich liegen.
* Nur bei Office 2013 gab es Performance-Probleme. Bei schnellem Tippen stieg die Prozessorauslastung auf 50% an, wohingegen es beim normalen Notepad 10% waren. Das sagt mehr über Office aus als über den verbauten Tegra Chip.

Die Anzahl beider Seiten hält sich die Waage. Einige Punkte sind aber wichtiger als andere und der Kunde muss für sich auch Entscheidungen treffen. Insgesamt ist das Surface ein tolles Gerät und Windows 8 auch für Tablets geeignet. Einige Schwachpunkte werden mit der nächsten Surface-Version sicherlich behoben.      
Ein größeres Problem sehe ich im Fehlen der Drittanbieter-Apps. Da muss sehr schnell was passieren, denn es ist äußerst enttäuschend für die Kunden, wenn sie ein tolles Gerät in den Händen halten, aber nichts damit machen können. Dass das Surface das beste Tablet zum Schreiben längerer Texte ist, mag sein, doch sind die große Masse an potentiellen Kunden keine Journalisten oder Romanautoren. 