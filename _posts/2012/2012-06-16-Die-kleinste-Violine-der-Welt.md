---
layout: post
title: "Die kleinste Violine der Welt"
description: "Übersetzung eines Statements von John Gruber."
date: 2012-06-16 
category: it
tags: [Apple]
published: true
comments: false
share: true
---

Folgende Zeilen sind von [John Gruber](http://daringfireball.net/linked/2012/06/16/whats-that-sound). Bevor ich meinen längeren Artikel zu diesem Thema veröffentliche, muss ich dieses Statement übersetzen.

Ich habe gelacht.

Kyle Wiens von iFixit, [beklagt](http://www.wired.com/gadgetlab/2012/06/opinion-apple-retina-displa/all/1) das Design des neuen Retina MacBook Pro: 

*Wir haben ständig Hardware gewählt, die eher dünner als aufrüstbar ist. Aber irgendwann muss auch Schluss sein.*   
    
Hörst du das? Das ist die kleinste Violine der Welt und sie spielt ein trauriges Lied, nur für die Dritthersteller Reparatur- und Upgrade-Industrie. Und diese Violine wurde von Apple gemacht und man kann sie nicht auseinanderbauen.
