---
layout: post
title: "Dexter, Staffel 7 "
description: "Eine Zusammenfassung der äußerst gelungenen 7. Staffel von Dexter."
date: 2012-12-18 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2012-dexter-season-7-lg.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/home
---

Ich bin sehr begeistert von der aktuellen, 7. Staffel von Dexter und deshalb auch hier die Zusammenfassung.

## Das offenbarte Geheimnis

Nahtlos macht die neuste Staffel da weiter, wo Staffel sechs aufgehört hat. Debra ist geschockt, Dexter ist geschockt. Gemeinsam brennen sie die Kirche ab, wobei das Feuer nicht alle Beweise zerstört. Captain LaGuerta findet einen Blutträger und ist sich sicher, dass ihr Kollege und Freund Doakes nicht der Bay-Harbor-Metzger war. 

Kurz danach tötet Dexter unwissend ein Mitglied der russischen Mafia, was eine enorm gute Nebenhandlung lostritt. Doch zunächst muss Dexter seiner Halbschwester begreiflich machen was er tut und wieso. So richtig gut verdaut sie es aber nicht, denn in dieser Staffel schlittert Dexter auf dünnem Eis.      
Die russische Mafia kommt ihn auf die Fersen und es beginnt ein Katz- und Mausspiel mit dem Geliebten des Ermordeten. Dexter verliebt sich in die Mörderin Hannah, die er, auf *seinem* Tisch liegend, nicht töten kann. Dass er sie vor der Polizei deckt, missfällt Debra sehr. LaGuerta ermittelt weiter gegen Dexter, wobei seine Schwester sie behindert und Beweise gegen ihn verschwinden lassen kann. Hierbei wird schön deutlich, dass Dexter nicht so spurlos gearbeitet hat, wie er immer dachte.

Im großen Finale bekommt Dexter heraus, dass seine Freundin versucht hat Debra zu töten. Er gibt Debra Beweise gegen sie und sie muss ins Gefängnis. LaGuerta hat es nun auf Dexter abgesehen und stellt ihm eine Falle. Obwohl diese im letzten Augenblick misslingt, verhaftet sie Dexter muss ihn aber gehen lassen, da sie keine Beweise hat.      
Derweil entkommt Dexters Freundin durch einen Trick dem Gefängnis und hinterlässt ihm eine schwarze Orchidee neben der Haustür. Dexter sieht als einzigen Ausweg die Ermordung von LaGuerta und stellt ihr eine Falle. Bevor er sie jedoch tötet erscheint Debra in der Szene und entscheidet sich tränenreich für ihren Bruder. Und erschießt LaGuerta. 

## Meinung

Dies war eine grandiose Staffel! Dexter verliert die Kontrolle über sein Geheimnis und es macht unglaublichen Spaß ihm dabei zuzusehen. Aktuell kennen zwei Menschen die Wahrheit und das sind beides Frauen, die für ihn Morden würden. Der Charakter von Dexter hat sich sehr interessant entwickelt.      
Die letzte Staffel wird nächstes Jahr ausgestrahlt und uns hoffentlich zeigen, welche Auswirkungen die beiden Frauen in seinem Leben haben. Debra ist jetzt schon ein psychisches Wrack und nun ist sie nicht nur Mitwisserin, sondern sogar Mittäterin. Dexter wird seinen Drang zu Morden aber nicht unterdrücken können. Genauso weiß er auch, dass es für ihn oder seiner Schwester keine Flucht gibt.

Läuft am Ende alles auf ein Tod von Dexter hinaus? Ist dies die einzige Lösung? Oder bekommen Debra und er die Gerüchte und herumschwirrenden Beweise und Zeugen unter Kontrolle und entwerfen vielleicht gemeinsam einen neuen Kodex?

Eines steht fest: Man darf sich auf nächstes Jahr freuen!