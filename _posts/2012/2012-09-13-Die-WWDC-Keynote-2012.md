---
layout: post
title: "Die WWDC Keynote 2012"
description: "Eine Zusammenfassung über die Keynote und das iPhone 5."
date: 2012-09-13 
category: it
tags: [Apple, iPhone]
published: true
comments: false
share: true
image: 
  feature: 2012-apple-keynote-iphone-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Kurz vor 19 Uhr las ich einen Tweet von Tim Pritlove. Er schalte nun seine Push-Benachrichtigungen ab um einen spoilerfreien Abend zu haben. Ich hätte gerne mit Freunden zusammen gesessen und mit ihnen die Foto-Livestreams der Apple Keynote geschaut. Wegen mir auch Online per Videochat. Aber es sollte nicht sein. 


Die Möglichkeit, einfach keine Neuigkeiten an diesem Abend zu konsumieren und lieber auf das offizielle Video der Keynote zu warten, schien mir plötzlich eine gute Idee zu sein. Gedacht, getan.       
Leider wurde das Video erst kurz vor 2 Uhr nachts online gestellt. Jetzt frühstücke ich und schaue mir dabei die Apple Neuheiten an.

In den ersten Minuten stellte Tim Cook den neuen Apple Store in Barcelona vor, erzählte wie viel Liebe und Arbeit sie in ihn gesteckt haben und zeigte ein Video. Für manch einen mit etwas zu viel Pathos, aber man sollte die Wichtigkeit der Apple Stores nicht unterschätzen. Es ist ein Ort, wo sich *normale Menschen* Apple Produkte in einer tollen Atmosphäre anschauen können und stets Menschen um sich haben die ihre Fragen beantworten können.      
Ich war bei meinem Besuch im Apple Store Dresden von der Anzahl der Mitarbeiter beeindruckt. Ich würde sogar behaupten, dass es für PCs kein Äquivalent gibt. Man hat zwar überall Elektronikketten, aber weder kommen die mit der Anzahl ihrer Mitarbeiter noch mit der Atmosphäre in den Geschäften an Apple heran. Samsung beginnt ja nun auch schon die Apple Geschäfte zu kopieren und das wird seinen guten Grund haben.

## Harte Fakten

Apple hatte in den letzten drei Monaten den höchsten Marktanteil (27%) von Notebooks in Amerika, das ist eine gewaltige Marke und zeigt vor allem, dass die Macs ein wesentlich größeres Wachstum haben als PCs. Da sind die iPads noch gar nicht mit eingerechnet.        
Ihr Mountain Lion wurde schon 7 Mio. mal geladen, was ebenfalls Rekord für Apple ist. Da reden wir zwar nur von 16€ pro Kopie, aber es ist zum einen überhaupt eine Einnahmequelle und zum anderen geben die jährlichen Updates dem Benutzers das Gefühl, dass etwas passiert, dass es eine Entwicklung gibt. Mein Windows 7 sieht noch immer so aus wie 2009. Dazu später mal einige Überlegungen.

Das iPad stellt alles in den Schatten. Allein im letzten Quartal wurden 17 Mio. davon verkauft. Das sind laut Apple mehr iPads als jeder PC-Hersteller an PCs von seiner kompletten Produktlinie verkauft hat. Jeder der diese Tablets für ein Trend hält, sollte sich die Anzahl bisher verkaufter iPads vor Augen führen: 84 Millionen. Das iPad ist genauso nicht nur ein Trend wie der PC es vor 20 Jahren war.        
Hochinteressant ist das Scheitern aller anderen Tablet-Computer. Apple meint sogar, dass der Marktanteil vom iPad im Vergleich zum letzten Jahr zugenommen hat (von 62% auf 68%). Ich wüsste zwar gerne, was diese 38% anderer Tablets gewesen sein sollen, aber die Aussage der Zahlen ist klar. Vergleicht man den Web Traffic, räumt das iPad sogar 91% ab.

Wo große Zahlen herumgeworfen werden, kann der App Store nicht fehlen. Ich habe gerade nicht die Zahlen der letzten Keynote zur Hand, aber aktuell hat der App Store 700k Apps und 250k davon speziell für das iPad. Viel wichtiger ist die Information von Tim Cook, dass 90% all dieser Apps tatsächlich auch monatlich geladen werden und jeder Nutzer wohl durchschnittlich 100 Apps lädt. Ich werde mir die Keynote der Android Developer Konferenz noch einmal anschauen müssen.

## iPhone 5

Ein wenig albern war der Moment als das iPhone 5 das Licht der Welt erblickte. Nicht unbedingt, weil es einige Sekunden nur auf einem Podest stand, bevor auch das Video von ihm auf die Leinwand projiziert wurde. Sondern viel mehr, weil es für die meisten Zuschauer im Raum garantiert kein neuer Anblick ist.      
Das iPhone 5 ist dünner und leichter. Genauer gesagt jeweils ⅕ so dünn und leicht wie das 4S. Der Bildschirm ist *unüberraschenderweise* 4 Zoll groß, wobei er nur in der Höhe gewachsen ist, die Breite blieb gleich. 

Das ist in der Tat für einige Apps ziemlich praktisch. Man sieht mehr Termine im Kalender und kann ihn auch horizontal in der Wochenansicht betrachten. Selbstverständlich hat Apple alle seine Apps schon angepasst.      
Drittanbieter Apps, die noch nicht auf diese Bildschirmgröße angepasst sind, bekommen schwarze Balken, die laut Apple gar nicht zu bemerken sind. Der zusätzliche Platz durch die 176 Pixel ist auch wirklich nicht riesig.

Die Pixeldichte ist gleich geblieben, aber die Farben sollen wohl den sRGB-Raum abdecken. Das ist in der Tat sehr beachtlich. Dass der Bildschirm dünner ist, wusste man vorher auch schon.        
Über LTE wurde auch schon gemunkelt und es wird nicht nur vom neuen iPhone unterstützt, sondern ist sogar größtenteils *weltweit* verfügbar. Bei uns wird es von der Telekom angeboten. Das ist gut und schön, aber irgendwie auch armselig wenn wir landesweit nicht einmal eine gute 3G-Abdeckung haben.       
Der Chip ist natürlich auch neu und nennt sich A6. Die Geschwindigkeit des Chips und der Grafikeinheit haben sich verdoppelt und dabei ist er insgesamt 22% kleiner. Die bessere Performance soll man merken können, aber das würde ich eher anzweifeln. 

Bedeutend ist die Geschwindigkeit bei Spielen mit aufwendiger Grafik und genau deshalb stellte auch EA ihr neues Real Racing 3 vor. Ja, es sah wirklich gut aus. Sehr cool war ein neues Feature des Spiels, der zeitversetzte Multiplayer. Man fährt eine Strecke eben alleine und der andere Spieler kann später gegen die Aufzeichnung deines Rennens fahren, aber eben nicht nur nach der Zeit jagen, sondern die Aufzeichnung fährt mit und ist beeinflussbar. Hört sich interessant an.

Die Akkulaufzeit hat sich etwas verbessert. 5 Stunden längeres Standby (225 h), 1 Stunde länger 3G Telefonieren (9 h) und 1 Stunde mehr beim Surfen über Wifi (10 h). Leider nicht der von mir erhoffte Quantensprung.      
Die Kamera ist größtenteils, oberflächlich gleich geblieben, nur 25% kleiner geworden. Ich finde es sehr vernünftig von Apple, die Megapixel nicht weiter hochzutreiben. Sie haben weiter an der Verbesserung von Fotos bei wenig Licht gearbeitet und dem Fokussieren der Linsen. Fotos soll man nun schneller aufnehmen können (40% schneller). Auch der A6 unterstützt das Foto schießen. Vieles wird in diesem Bereich automatischer. Mal sehen was die neuen Android-Kameras von Samsung können.       
Sehr gespannt bin ich auf die Panoramafunktion. Einige Erfahrung habe ich mit solchen Apps ja schon gemacht. Auch die Front-Kamera ist nun mit 720p HD.

Ansonsten gibt es an allen Stellen Verbesserungen. Drei statt zwei Mikrofone, die nicht nur für das Telefonieren, sondern auch für die Spracherkennung nützlich sind. Kleine, aber besser klingende Lautsprecher. Für die Übertragung von Gesprächen kommt nun (die neue) Breitband-Technologie zum Einsatz, was die Stimmen natürlicher klingen lassen soll. Die Telekom ist zum Beispiel ein Partner dafür.     
Der neue Dock Connector ist bekanntermaßen auch kleiner und vor allem wohl auf das Aufladen ausgerichtet. Und man kann ihn in jeder Richtung ins iPhone stecken! USB kann das nicht.

Die iOS 6 Vorstellung habe ich übersprungen. Vieles war ja schon von der WWDC bekannt. Zum Abschluss gab es noch ein Apple-Video und das war es in der Tat. Parodien folgen garantiert. Ausführliche Gedanken von mir folgen noch separat.

## iTunes, iPods und EarPods

Auf den iOS Geräten gibt es mit iOS 6 ein neues Design für iTunes, aber das war ja schon bekannt. iTunes für den Mac wurde rundum erneuert, verbessert und vor allem vereinfacht. Es sieht wirklich nicht schlecht aus. Zu iTunes und Software wollte ich sowieso noch mal etwas mehr schreiben. Sehr gut ist die Integration der iCloud in iTunes. Alles was man je gekauft hat, kann man überall anhören, lesen und anschauen. Um das zu demonstrieren, wurde ein Film gewählt, der vorher auf dem iPad angefangen wurde zu gucken und nun auf dem Mac an der letzten Stelle fortgeführt wird. Und dafür wählte Apple einer der besten Szenen aus Avengers, als der Hulk Loki vermöbelt. Episch.             
Ob es das iTunes Update auch für den PC geben wird?

Die iPod Familie wurde ebenfalls erneuert und das wurde ja auch höchste Zeit. Der nano fand zurück zu seiner schlanken Form und hat nun wieder einen großen Bildschirm. Die Steuertasten befinden sich seitlich und sind schön groß. Ein Home-Button gibt es nun auch. Ob sich Fotos und Video auf ihm durchsetzen werden?        
Der neue iPod Touch ist nur einen Millimeter dicker als der nano. Das Design orientiert sich natürlich am iPhone 5 und erstaunlicherweise hat er den gleichen Bildschirm. Da er noch einen Tick dünner als das iPhone ist, wiegt er auch 25 Gramm weniger. Apple sieht in ihm nicht nur die Funktion eines Musik-Players, sondern auch einer *Spielekonsole* und trotzdem erhielt er nur den A5 Chip des iPhone 4S. Auch die neuerdings verbaute Kamera bietet nur 5 Megapixel, aber irgendwo muss es ja auch einen Unterschied in den Preisen geben.

Seltsam finde ich die Auswahl der GB-Größen. Der iPod Touch beginnt bei 32GB, das iPhone schon bei 16Gb bzw. nur noch 8GB bekommt man beim iPhone 4 und 4S. Besonders letzteres finde ich fast schon frech. Was möchte man denn mit nur 8 GB auf einem 4S? Nach 10 Apps, 200 Bilder und 5 HD-Videos (selbst aufgenommen) ist doch dann Schluss. Bei einem iPod Touch könnte man sich viel eher mit 16 GB anfreunden, aber dafür gibt es ja den alten iPod Touch.        
Die Preise sind bei Apple immer etwas seltsam, um es vorsichtig auszudrücken. So kostet das neue iPhone mit 16 GB 680€ und ein iPod Touch mit 32 GB *nur* 320€. Abstriche macht man beim Prozessor, Akku vermutlich, Telefonieren, der Kamera und evtl. RAM. Aber das sind so oder so über 300€ Differenz, zum 32 GB iPhone sogar über 400€. Ich vermute stark dass die iPods eine viel geringe Gewinnspanne haben.      
Wie auch immer, der iPod Touch kommt nun in 5 verschiedenen Farben und einem Trageband, genannt Loop. Der iPod Shuffle bekommt nur neue Farben und scheinbar keine Vergrößerung seines 2 GB Speichers. Da hat Apple einen ungewöhnlichen Sprung von 2 GB zu 16 GB des nano. Auch der Preis des Shuffles bleibt nach 2 Jahren gleich hoch. Schade.

Ebenfalls schon vorher bekannt waren die neuen Kopfhörer. Apple hat in den letzten Jahren hunderte Ohren gescannt, analysiert und nach Gemeinsamkeiten gesucht um die perfekten Kopfhörer zu designen. Interessant sind die insgesamt 4 verbauten Lautsprecher in ihnen. Wie gut sie wirklich sind, wird sich bestimmt bald zeigen.

## Foo Fighters - WTF?!

Das *One more Thing* ist zurück sozusagen.      
Ich hatte schon so ein Grummeln im Bauch, als Tim Cook eine Band ankündigte, die Arenen füllt und künsterisch hoch anerkannt ist. Aber das dann bei einer Apple Keynote in der Tat die Foo Fighters live spielen...wow.      
Sie gaben Times Like These, Hero und Walk akustisch zum besten. Ich bin wirklich positiv überrascht. 

## Fazit

Die Produkte sehen wie immer gut aus und plötzlich fühlt sich auch mein iPhone schwer und klobig an. So war es gestern noch nicht. Aber man kann auf jeden Fall gespannt auf die nächsten Tage sein, wenn die ersten Telefone ihre Besitzer finden. 
Die Keynote war zwar zwei Stunden lang, aber trotzdem nicht zu langatmig. Apple nimmt sich die Zeit und stellt die wichtigsten Neuerungen vor. Ich mag dieses geordnete sehr.
Und die Foo Fighters? Klasse.

Ich habe es auch nicht bereut die Nachrichten zu meiden. Diese twitterartigen Livestreams sind nichts halbes und nichts ganzes. Die musikalische Überraschung hätte ich mir beispielsweise verdorben und später sowieso angeschaut. 
Nun poste ich diesen Text und schaue mir mal die Meinungen der anderen Medien an.