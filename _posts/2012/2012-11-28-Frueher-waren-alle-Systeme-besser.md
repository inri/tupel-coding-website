---
layout: post
title: "Früher waren alle Systeme besser"
description: "Wieso Nerds unzufrieden mit der rasanten technischen Entwicklung sind."
date: 2012-11-28
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

**Früher war alles besser.**     
Das denken nicht nur ältere Menschen manchmal, sondern immer öfter begegnet mir dieses Mantra auch in meinem Bekanntenkreis.

So wie die Anhänger einer Musikgruppe über ein neues Album enttäuscht sein können und sich von ihrer Band verraten fühlen, so betrogen fühlen sich auch Nerds [^1], die eine gewisse Zeit lang mit einem System gearbeitet haben, welches plötzlich Veränderungen unterworfen ist. Mir fiel das besonders bei Apples OS X Update von Snow Leopard zu Lion auf.      
    
Dass solch konservatives Denken in einer doch recht modernen Branche vertreten ist, überrascht mich immer wieder. Neuerungen sind für sie erst einmal schlecht und kommen ungefragt. Der Hersteller hat Mist gebaut und natürlich funktioniert nichts mehr, so wie es der Nerd einst mühselig erlernt hat.

Ja, ich schimpfe auch oft genug und ab und zu machen Hersteller auch Fehler. Aber besonders die größeren Firmen wie Microsoft und Apple denken sich hoffentlich etwas bei ihren Änderungen. Grundsätzlich sind Veränderungen vor allem schwierig. Die Benutzer haben sich teils über Jahre an das System mit all seinen Schwächen gewöhnt. Perfekt war sicherlich noch keines.       
Wenn Neuerungen anstehen und die gewohnten Arbeitsabläufe eben nicht mehr wie gewohnt funktionieren, ist der Frust sehr schnell, sehr groß. Ich glaube besonders Nerds sind in dieser Hinsicht sensibel. Die Realität sieht so aus, dass die bestehenden Systeme immer benutzerfreundlicher werden, meistens auf Kosten der Komplexität bzw. Möglichkeiten. Damit meine ich noch nicht die mobilen Systeme, sondern die weit verbreiteten Desktopsysteme.

## Die Computerbranche verändert sich

In einem Review zum neuen Mountain Lion las ich folgendes Statement: *Die Nerds müssen sich damit abfinden, dass nicht mehr sie allein der Fokus der Hersteller sind. Jahrelang wurde Software und Hardware quasi nur für Nerds entwickelt, aber das ändert sich nun.*        
Diese Tendenz sehe ich ebenfalls. Die Hersteller wollen einfach mehr Menschen erreichen. Mehr Menschen ansprechen, die ihre Systeme auch tatsächlich und gerne benutzen. Menschen, deren Hobby nicht die Computertechnik ist. Menschen, die keine Zeit haben sich wochenlang in ein unintuitives System einzuarbeiten. Menschen die einfach keinen Frust im Umgang mit ihrer Technik erleben wollen. Und was ist daran so falsch?

Die moderne Technik ist zu einem elementaren Bereich unseres Lebens geworden und jeder Mensch sollte die Möglichkeit haben sie zu nutzen. Bei allem muss man eine Einstiegshürde überwinden. Will man Schnürsenkel benutzen, muss man das Binden lernen. Wenn es Mittel und Wege gibt die Einstiegshürde niedriger zu gestalten, dann sollte man das auch tun. Zum Beispiel in dem man seinem Kind einen Schnürsenkelbindenspruch beibringt, oder in dem man Computersysteme so gestaltet, dass sie in kürzerer Zeit besser verständlich und benutzbar sind.       
Hier könnte ich noch weitere Beispiele aufzählen, wie verschiedene Dinge unserer Zivilisation mit der Zeit vereinfacht wurden. Drehe ich meinen Kopf zur Seite, schaue ich die Heizung an und bin im Winter sehr froh, keine Kamin betreiben zu müssen. Was Autos angeht ist es genauso. Die wenigsten Menschen wären in der Lage ein Auto von vor z.b. 100 Jahren sicher zu fahren.

## Die Gewohnheit ist Kritik

Der Mensch ist ein Gewohnheitstier und das trifft auch auf die Technik und Systeme zu, die er nutzt. Ich vermute daher sehr stark, dass viel Kritik die von Nerds kommt und meistens im Zusammenhang mit größeren Veränderungen steht, einfach der erste Reflex der Unzufriedenheit ist.       
Das wird es leider immer geben und irgendwann werden auch mich Veränderungen aufregen. Wobei es bei einigen wirklich kein Wunder ist, wie das neue Windows 8 ganz gut zeigt.

Gewohnheit fühlt sich sicher und geborgen an, aber Veränderungen bringen uns weiter. Der Stillstand im technischen Bereich währt nicht lange. Und vielleicht ist die größte Kritik an Microsoft nicht ihr neues UI, sondern dass es erst Ende 2012 erscheint. Viel zu spät.

[^1]: Das Wort Nerds verwende ich stellvertretend für Menschen die viel Zeit mit Computertechnik verbringen.
