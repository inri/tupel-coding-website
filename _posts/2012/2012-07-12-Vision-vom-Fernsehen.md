---
layout: post
title: "Vision vom Fernsehen"
description: ""
date: 2012-07-12 
category: medien
tags: []
published: true
comments: false
share: true
---

Seit dem Tod von Steve Jobs kochen immer mal wieder die Gerüchte über ein Fernseher von Apple hoch. Als die ersten Vermutungen eine mögliche Vorstellung im Sommer in Betracht zogen, gaben die Firmenchefs traditioneller TV-Hersteller erste peinliche Statements ab. 


Apple könne in diesem Geschäftsfeld nicht erfolgreich sein und sowieso kein Produkt herausbringen, welches den Geräten der aktuellen Hersteller technisch überlegen sei.      
Ich vermute ja, es geht Apple, wie auch schon beim iPhone, gar nicht um eine technische Revolution. Im folgenden möchte ich einige aus meiner Sicht kritische Punkte zum Modell des Fernsehens, wie es seit Jahrzehnten existiert, benennen und erläutern, wie ich mir ein Lösung vorstelle. Das ist ein sehr subjektiver Text und er soll lediglich einige Konzepte skizzieren, die ich mir von Apple oder einem anderen Hersteller wünsche, dessen Fernsehgerät ich später kaufen soll.

## 80 Jahre Fernseher

Fernseher gibt es seit ungefähr 80 Jahren, doch das Prinzip hat sich kaum geändert. Noch immer werden verschiedene Programme gesendet, Fernseher in Wohnzimmern empfangen das und die Menschen vor der Glotze konsumieren. Mit der Zeit ist die Anzahl verfügbarer Sender gewachsen und unterschiedliche Modelle werden mittlerweile verwendet um diese zu finanzieren.      
Bezahlen die Steuerzahler durch die Abgabe GEZ die öffentlich-rechtlichen Sender, nehmen dagegen die privaten Sender vor allem durch Werbung Geld ein. Daneben gibt es auch Abomodelle wie Premiere bzw. Sky, bei denen man den Sender nur empfängt, wenn man extra dafür bezahlt. In Amerika ist dieses PayTV wesentlich verbreiteter. 

Technisch gab es ebenfalls diverse Veränderungen.      
Zuerst... 

* die Umstellung von Schwarzweiß zu Farbe
* später besserer Ton
* Übertragungsarten von Kabel, Satellit und so weiter
* von Röhre zu immer größer werdenden Flachbildschirmen
* VHS-Kassetten, DVD- und BluRay-Player
* Festplattenrecorder
* hochauflösende Qualität (HD) 
* und demnächst wohl immer mehr Verbindung zum Internet.
 
Das sind alles tolle Dinge, doch sie haben am Grundprinzip nichts geändert: Es wird geschaut, was einem vorgesetzt wird. Die Wahlmöglichkeiten gaben die verschiedenen Medien von VHS über DVD bis hin zu BluRay und Festplatte, doch keine von diesen Möglichkeiten kann man als bequem bezeichnen. 

Der Nutzer hat kaum eine Wahl. Sender haben Programme und feste Sendezeiten. Eine Wahlmöglichkeit gibt es nur durch Kauf/Ausleihen von Medien auf Datenträgern. Verpasste Sendungen sind meist verpasst oder können vielleicht Monate später mit etwas Glück auf Datenträger gekauft werden (Serien) oder man findet sie mal legal, mal nicht so legal im Internet. Bei den legalen Angeboten meist auch nur für eine begrenzte Zeit, wobei das wieder Aufwand bedeutet.

Eine Besserung ist im Bereich der öffentlich-rechtlichen Sender in Sicht. Sendungen werden gestreamt und man kann sie in ihren Mediatheken noch Tage später anschauen. Bisher gibt es zwar noch einen begrenzten Zeitraum dafür, aber das soll sich ändern.    
Das Leid mit Spielfilmen. Entweder Spielfilme im Kino anschauen,wenn sie denn gezeigt werden. Oder ein halbes Jahr + x Monate später für ein Vielfaches des Kinoeintritts kaufen. Oder zwei Jahre + x Monate später im *FreeTV* sehen.

Kein Mensch versteht diese Regelung und sie existiert auch nur um den Filmstudios und Verleihs die Gewinne zu sichern, glauben diese zumindest. Ich glaube diese Konzepte müssen überdacht werden und das passiert zur Zeit auch.

## Meine Vision eines Fernsehers 

Im Wohnzimmer vor der Couch steht der schlichte Bildschirm. Er ist lediglich am Strom und ans Internet angeschlossen. In seinem Inneren ist ein kleiner Computer verbaut, der z.b. dafür sorgt, dass meine abonnierten Serien und Filme automatisch geladen und bereit sind, wenn ich sie schauen will.     
Ich setze mich hin und nehme mein kleines Tablet. Auf dessen Oberfläche kann ich zwischen den verschiedenen Sendern wählen. Tippe ich auf einen, erscheint dieser auf den großen Fernseher. Während gerade das aktuelle Programm läuft, kann ich auf dem Tablet weiter suchen, was auf den anderen Sendern zur Zeit läuft und erhalte übersichtlich verschiedene Informationen. Kommt zum Beispiel gerade ein Spielfilm, kann ich mir eine Inhaltszusammenfassung, Kritiken oder kurze Trailer anschauen. Entschließe ich den Film zu schauen, beginnt er natürlich am Anfang.

Neben dem aktuellen Programm erhalte ich auch Informationen zu meinen Abos. Seit drei Wochen läuft eine neue SciFi Serie in Amerika. Aufgrund der Zeitverschiebung kann ich sie nicht live sehen, aber ich habe sie abonniert und erhalte gerade die Information, dass eine neue Folge bereit zum Anschauen ist. 

Genauso funktioniert das auch bei Spielfilmen weltweit. Kinos haben das Sonderrecht Spielfilme 4 Wochen lang zu zeigen und danach sind sie ganz normal käuflich zu erwerben. Das ist eine praktische Sache, denn in meinem Kleinstadtkino werden bei Weitem nicht alle Filme gezeigt und zum anderen finde ich einige Filme zwar interessant, möchte sie aber nicht auf der großen Leinwand und in 3D sehen. Ich kann einen Film im Vorfeld bestellen und bezahle dafür knapp den Kinopreis. Sobald die 4-Wochen-Frist vorbei ist, wird der Film geladen und ich kann ihn jederzeit ansehen. Über die genauen Preise kann man sich streiten.

Es gibt immer noch viele Sender und sie zeigen immer noch ein umfangreiches Programm. Nun habe ich aber die Wahl, ob ich mir das aktuelle Programm anschaue, oder eine verpasste Sendung von letzter Woche. Oder die Nachrichten von heute früh. Eine Sendung oder eine Serie abonnieren und später schauen kann man sowohl kostenlos als auch gegen einen Aufpreis. Das hängt von den Sendern und ihren Finanzierungsmodellen ab. Die Auswahl aller weltweiten Sender findet aber auf einem System statt, deshalb kann ich auch mal eine norwegische TV-Show genießen wenn mir danach ist.

Das Fernseher-System hat verschiedene soziale Dienste integriert. So kann ich z.b. auf umfangreiche Bewertungen verschiedener anderer Nutzer zurückgreifen, die schon das eine oder andere Mal genau meinen Geschmack getroffen haben. Wenn meine Freunde die Funktion freigeschaltet haben, können wir uns gegenseitig Filmtips schicken oder auch gemeinsam einen Film schauen und via Webcam darüber reden. *Social Viewing*. 

Es ist nur eine Vision. Aber ich denke, wir sind nicht allzu weit davon entfernt. Das schwierigste an der ganzen Geschichte ist das Verbinden der weltweiten Fernsehsender und Filmstudios. Genau das ist eine Sache, die ich besonders Apple zutraue. Zur Zeit trifft Tim Cook diverse Studios und ist in Verhandlungen. Ich bin gespannt was die Ergebnisse sein werden.      
Technisch habe ich eigentlich wenig zu Meckern. Die aktuellen Geräte sind groß und haben immer bessere Bildschirme. Ich wünsche mir die Verbindung zu anderen Geräten wie meinem iPad zum Beispiel. Zum einen als eine Art Fernbedienung und zum andere auch um andere Medien wie Fotos am Fernseher bequem zeigen zu können, was ja teilweise schon möglich ist.

Die lange Verfügbarkeit von Sendungen muss technisch realisiert werden, aber das ist kein Ding der Unmöglichkeit. Viel schlimmer für den Nutzer ist es zur Zeit, dass er einige Serien einfach nicht auf legalem Wege schauen kann. Oder Kinofilme erst Monate nach offiziellem Start in seinem Land schauen kann. Diese internationalen Grenzen müssen aufgehoben werden. Und natürlich brauchen die Haushalte viel bessere Internetanschlüsse. 

## Bessere Quoten &amp; iTV

Eine Anmerkung noch zum Schluss. In Deutschland wird der Erfolg von Fernsehsendern in Quoten gemessen. Diese Quoten werden aber auf eine steinzeitliche Art erhoben. In Deutschland stehen in 5600 Haushalten Quotenmesser und sammeln die repräsentativen Daten. Ich glaube, wenn das Fernsehen in Zukunft mehr online als offline stattfindet, wird man viel bessere Daten darüber erhalten, was denn eigentlich von den Menschen geschaut wird. Insgeheim erhoffe ich mir dadurch eine kleine Revolution der Fernsehsendungen.

Die Software-seitige Umsetzung eines System für Fernseher ist nicht das schwierigste. Ich erwarte von den Firmen, dass sie die Art, wie zur Zeit Fernsehen konsumiert wird, erheblich verbessern. Dazu gehört eine leichte Verfügbarkeit und kein umständlichen Medienträger wie DVD. Ich würde ja Hoffnungen auf Google setzen, aber ihr GoogleTV ist bisher nicht so richtig ausgereift und verbreitet. 
    
Ein Knackpunkt ist die Verfügbarkeit von verschiedenen Quellen. Amazon versucht ja auch mit dem Kindle Fire seine Kunden den Zugang zu Büchern, Musik und Filmen zu erleichtern, Google mit GooglePlay genauso. Aber meist sind diese Dienste vorerst nur in Amerika zu haben und dadurch unattraktiv. 
Das meiste, was das erste iPhone konnte, konnten auch die Smartphones vor dem iPhone schon. Ein Fernseher von Apple wird ebenfalls nicht vollständig neuartig sein, aber es wird vielleicht viele Dienste vereinen und den Benutzern bequem zur Verfügung stellen.      
**Allen Nutzern** und nicht nur den amerikanischen.

Was auch immer und wann auch immer es kommt, sollte es meiner Vision entsprechen, würde ich mich sehr freuen.