---
layout: post
title: "Sind 4+ Zoll ein Muss?"
description: "Wieso haben so viele aktuelle Smartphone Bildschirmgrößen jenseits der 4 Zoll?"
date: 2012-09-06 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Wenn man sich die aktuellen Flaggschiffe diverser Smartphone-Hersteller anschaut, stellt man fest, dass fast alle mit Bildschirmen zwischen 4,3 und 4,8 Zoll daherkommen. 


Samsung mit seinen S3 (4,8) und Galaxy Nexus (4,65), die neuen Nokias Lumia 920 (4,5) und Lumia 820 (4,3), Motorolas RAZR Maxx HD (4,7) und das neue iPhone soll ja auch um die 4 Zoll groß sein. Letzterem muss man zugute halten, dass das Gehäuse lediglich länger wird, die meisten anderen Telefone sind nicht nur hoch sondern auch breit.

Ich habe mich gefragt, wieso alle Hersteller auf diese Größen satteln? In Sachen Ergonomie spricht nicht besonders viel dafür Telefone auf den Markt zu bringen, die nur Männer benutzen können. Ich habe normalgroße Hände für einen Mann, aber konnte trotzdem nicht alle Ecken meines 4,3 Zoll HTC HD2 erreichen. Für meine Freundin ist ein 4,7 Zoll Smartphone schon ein kleines Tablet. 

#### Vielleicht gibt es technische Gründe?

Wie sah die Entwicklung der letzten Jahre aus? Prozessoren und Speicher wurden kleiner und gleichzeitig leistungsstärker. Einerseits wurde der Stromverbrauch auch verringert, aber im gleichem Maße? Immer noch benötigen mobile Geräte viel Strom und dementsprechend groß sind auch die Akkus.       
Meine spontane Theorie ist folgende: Obwohl Komponenten kleiner, besser und das bei möglichst gleichbleibendem Energieverbrauch sein sollen, geht das vermutlich doch nicht ganz auf. Zumindest nicht so sehr, dass alles inklusive Akku in einem 3,7 Zoll oder noch kleinerem Gehäuse passt. 

Falls jemand nichts zu tun hat, kann er ja die Flaggschiffe der letzten fünf Jahre in diversen Punkten (Leistung, Energieverbrauch, Größe, Akku,…) vergleichen. Ich wäre wirklich sehr gespannt auf die Ergebnisse. 