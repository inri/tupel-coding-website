---
layout: post
title: "Die Zukunft ist Google Glass?"
description: "Wie sinnvoll und zukunftsträchtig sind ist die Google Glass?"
date: 2012-11-30 
category: it
tags: [Google, Glass]
published: true
comments: false
share: true
image: 
  feature: 2012-google-glass-lg.jpg
  credit: Google
  creditlink: http://google.com
---

Hatte ich schon etwas ausführlicher über die Datenbrille von Google geschrieben? Die Google Glass? 


Dieses ganze Projekt hat zwei Seiten: Die Forschung an solchen Dingen ist wichtig und gut, aber Googles Vorstellungen bewegen sich noch außerhalb jeglicher Realität. Besonders suspekt war die Vorstellung auf der diesjährigen Entwickler-Keynote mit dem Fallschirmsprung. Streng genommen war da eine Kameras an einer Brille geklebt, die die übertragenen Bilder streamen konnte. So what? 

## Technische Fragen

Wie schon gesagt, das Forschen an solcher Technik ist wichtig und mit Sicherheit nützlich. Ich verstehe auch, dass Google viel daran gelegen ist, dass eine größere Anzahl an Menschen diese Technik testet und ausprobiert. Ich vermute mal, das ist ihr Beweggrund die Glass so anzupreisen und es scheint teilweise zu funktionieren.       
Wenn Informatik-Kommilitonen sich begeistert zu den Google Glass auf sozialen Netzwerken äußern, ist das ein Indiz dafür, dass es sich um *Nerd-Technik* handelt, die für normale Menschen vorerst noch uninteressant ist. 

Zu allererst stellen sich mir einige technische Fragen. Wie sieht die Akkulaufzeit aus, wenn die Brille so kleiner und zierlich ist? Mobiltelefone werden für Hände entworfen, eine universelle Brille wird etwas schwieriger. Man hat verschiedene Größen von Nasen, Ohren und Köpfen. Nicht umsonst gibt es bei Optikern so verdammt viele Brillen.       
Wie steuert man eine solche Brille? Bei Google passiert dies durch Gesten, die die Kamera der Brille interpretiert. Das Konzept ist sicherlich nicht schlecht, schließlich funktioniert es bei Microsofts Kinect  auch. Aber der [Forschungsblog](http://www.forschungs-blog.de/durchblick-durch-datenbrillen/) berichtet, dass man am Fraunhofer Institut ebenfalls an einer Datenbrille arbeitet und diese soll sich mit den Augen steuern lassen. Rein intuitiv scheint mir das auch der elegantere Weg zu sein, aber aktuell gibt es nur Prototypen.

## Augmented Reality

Angenommen der Akku hält, die Brille sitzt gut und die Steuerung geht auch einfach von der Hand: Was dann? Das Zauberwort nennt sich Augmented Reality, der Bildschirm ergänzt die Wirklichkeit, die man durch die Brille sieht. Das ist keinesfalls erst mit einer Datenbrille möglich, sondern schon mit unseren Mobiltelefonen. Kennt jemand eine richtig gute App für AR?       
Diese ergänzte Realität hat einige Schwierigkeiten, die damit zu tun haben, dass das Interagieren mit der Umgebung das Ziel der Technik ist. Beispielsweise eine simple Touristen App, die zu dem Bauwerk Informationen anzeigt, muss das Bauwerk anhand von Koordinaten und Bild erst einmal erkennen. Bilderkennung ist noch relativ schwierig, auch wenn durch die ungefähre Lage die Suche eingegrenzt werden kann. Wie bei der Stimmanaylse von Siri müsste diese Funktionalität ausgelagert werden, was wiederum eine zwingende Internetverbindung voraussetzt.       
Vorerst.

Auch wenn bei einer Brille technisch alles stimmen sollte, kann ich die aktuellen Probleme nicht unbeachtet lassen. Die Situation wäre folgende: Man rennt mit seinem Smartphone durch eine Stadt, muss GPS und mobiles Internet aktiviert haben und die App berechnet den Standort und die Gebäude um einem herum, das wird eine kurze Stadttour. Meine Zweifel, ob eine Brille das so viel besser bewerkstelligen kann, sind mehr als berechtigt.       
Google preist seine Glass als Gegenstand für jede Gelegenheit an. Dem Fallschirm-Video nach, sind die Google Glass verkabelt und vermutlich hängt am anderen Ende der Akku, Logik und Speicher. Das löst vielleicht Ressourcenproblem, aber unpraktisch ist es für den normalen Benutzer allemal. Wie eine Datenbrille sinnvoll eingesetzt werden kann (mit oder ohne Kabel) zeigt ebenfalls der [Forschungsblog](http://www.forschungs-blog.de/was-fur-datenbrillen-spricht-aus-der-sicht-eines-entwicklungsingenieurs/).    

## Fazit

Ich glaube bisher sind die Einsatzszenarien für normale Menschen nur schöne Theorien. Sinnvoll wäre eine Datenbrille jetzt schon für Behinderte und Menschen, die sie in ihren Berufen nutzen können. Und für Nerds, die einfach auf neue Technik stehen.       
Ich würde sogar so weit gehen und sagen, dass selbst Smartphones von vielen Menschen unter ihrem Potential genutzt werden. Von Tablet-PCs ganz zu schweigen. Eine schnelle Verbreitung wie beim iPad, würde ich aus diesem Grund bei den Datenbrillen in den nächsten 5 Jahren definitiv nicht sehen.
Aber Forschen und Entwickeln soll man trotzdem fleißig, auch wenn ich mir ein wenig mehr Realismus in der Einschätzung wünschen würde. 