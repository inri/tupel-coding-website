---
layout: post
title: "Unpacking Kindle Fire HD"
description: "Ein erster Eindruck zu Amazons Kindle Fire HD."
date: 2012-10-28 
category: it
tags: [Amazon]
published: true
comments: false
share: true
image: 
  feature: 2012-kindle-fire-hd-lg.jpg
  credit: Amazon
  creditlink: http://amazon.com
---

Für ein Softwareprojekt teste ich verschiedene Android Tablets. Das 7 Zoll Kindle Fire HD wurde von uns in Betracht gezogen. Meiner erster Eindruck: Der Kindle fühlt sich pummelig an. 


Im Vergleich zu meinem iPad 2 ist er es auch, denn hier stehen sich einmal 8,8 mm und 10,3 mm Dicke gegenüber. Dazu kommt der Eindruck, dass der Kindle zwar gut halb so groß wie das iPad ist, aber mit 400 g nicht halb so viel wiegt (300 g müssten es sein). Aber 400g lassen sich noch in einer Hand halten, zumindest für kurze Zeit.

Die Seitenränder sind dick genug für den Daumen. Die Rückseite ist gummiert und schön griffig. Die Buttons finde ich ein wenig zu klein und schwer zu erreichen, da sie sich quasi im Gehäuse befinden.       
Der 1280 x 800 Bildschirm ist ganz okay. Im Video sieht man, dass er etwas dunkler als das iPad 2 ist, aber dieser Unterschied wird wohl erst im Sonnenlicht interessant.
Man sieht auch sofort, dass die Pixeldichte höher als bei meinem alten 132 ppi iPad ist, aber verkleinert sich der normale Augenabstand von 30 cm / 20 cm auf weniger, kann man Pixel zählen.

## KindleOS

*Willkommen auf deinen Kindle, hier ist etwas Werbung für dich.*, deaktiviert man die Werbung gegen eine Gebühr von 15€ nicht, begrüßt einem beim Unlock-Bildschirm jedes Mal großflächig Reklame. Ich mag das nicht so.      
Unter der Haube steckt Android 4, aber ich nenne das System mal lieber KindleOS, denn mit Android hat es nicht viel zu tun. Das KindleOS hat keinen Homescreen, wie wir es von Android oder iOS kennen. Amazon entschied sich für eine 4-Teilung und so findet sich im oberen Viertel ein Suchfeld und die Menüleiste (Einkaufen, Spiele, Apps, Bücher, Musik,...), fast die Hälfte des Bildschirm ist mit den zuletzt benutzten Medien gefüllt (bei mir die Tagesschau App, Temple Run und einige Bücher) und darunter im letzten Drittel befindet sich die *Kunden haben auch gekauft*-Kategorie. Alles ist im Stil der verschiebbaren Cover gestaltet.

Ich mag diese Art der Navigation nicht besonders. Wenn man zum Beispiel eine App aus einer Unterkategorie lädt und dann auf zurück tippt, ist man wieder im obersten App-Store-Menü und muss sich durch die Kategorien navigieren. Unintuitiv.     
Viel schlimmer ist aber die Performance des Kindles. Beim Scrollen und Tippen gibt es eine spürbare Warte-Sekunde. Ob diese Verzögerung in den Apps auch auftritt, wird sich zeigen. 

Für 200€ ist der Kindle okay. Die Verarbeitung ist nicht so hoch wie beim iPad und die Software bei weitem nicht perfekt, aber der Preis ist eben unschlagbar. Ich bin auf das Nexus 7 gespannt und noch mehr auf das iPad mini. 