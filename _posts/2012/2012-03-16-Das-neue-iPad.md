---
layout: post
title: "Das neue iPad"
description: "Das neue iPad 3 ist eine tolle Evolution des iPad 2, besitzt aber durchaus einige ganz kleine Schwächen."
date: 2012-03-16
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Nein, ich habe das neue iPad, bzw. das 3. iPad, nicht gekauft und werde es hoffentlich die nächsten Monate nicht zu Gesicht bekommen! Erste Kommentare und Berichte lassen meine Befürchtungen wahr werden: Nachdem man den Bildschirm des neuen iPads gesehen hat, sieht jeder andere Bildschirm ärmlich dagegen aus. Außer der vom iPhone vielleicht, aber das ist ja auch kleiner.

## Halbperfekte Neuerungen

Weitere Informationen rund um das neue Tablet von Apple, wecken in mir Vorsicht. Scheinbar ist die Situation vergleichbar mit dem Erscheinen des ersten iPads. Keine Frage, das iPad 3 hat viele Neuerung und Verbesserungen:

* Retina Bildschirm
* 1 GB Arbeitsspeicher
* bessere Grafikverarbeitung im Prozessor
* bessere kamera (von 0,7 MP zu 5 MP)
* gleiche Akkulaufzeit (bei größerer Akkukapazität)

Aber es sind auch schon einige *Nachteile* bekannt geworden, wobei das auch nur Kritik auf sehr, sehr hohem Niveau ist. Stellte das iPad das erste gute Tablet überhaupt dar, war das iPad 2 eine perfekte Überarbeitung. Man hat die Schwächen des Vorgängers ausgebessert und alle Konkurrenten deklassiert. 
Das neue iPad ist keine Überarbeitung oder bloße Verbesserung, weil es eben keine Schwächen des iPad 2 ausbessern musste.        
Dass der Bildschirm der ersten iPads nicht besser war, ist keine Schwäche, weil die technischen Möglichkeiten zum einen nicht mehr hergaben und zum anderen der Bildschirm ja nicht schlecht war. 

Bei der ersten Rückkamera kann man vom Verbessern reden, wobei ich bis heute der festen Überzeugung bin, dass die Kamera nur als Scherz gemeint war.       
Bei dem neuen iPad würde ich von einer Evolution reden. Es ging nicht ums bloße Überarbeiten, sondern eher darum, die Idee eines Tablet-Computers zu fokussieren. Die wichtigste Komponente ist der Bildschirm. Aber ein neuer, wesentlich besserer Bildschirm führte zu neuen Herausforderungen, die meines Erachtens noch nicht perfekt gelöst sind. Oder besser gesagt: Noch nicht so perfekt gelöst sind, wie es beim iPad 2 geschah.

**Welche Schwächen besitzt das neue iPad:**

* Es ist einen Tick schwerer und dicker.
* Es wird spürbar warm.
* Die Ladezeit des Akkus ist sehr viel länger.
* LTE wird nicht in allen Ländern unterstützt (bzw. die Frequenzen).
* Leistung hat sich dem neuen Bildschirm angepasst, ist aber im Vergleich kaum gestiegen.

Wie ich schon sagt, Kritik auf hohem Niveau. Ich denke, dass wir in einem Jahr ein überarbeitetes iPad 3 sehen werden. Die Akkutechnik wird sich weiterentwickeln und somit wird das iPad dann wieder etwas dünner und leichter und nicht so schnell warm. Apple liefert einen neuen Ladeadapter aus. Neue LTE-Chips werden mehr Frequenzen auch außerhalb der USA unterstützen. Dazu verbessert sich die Kamera auf iPhone 4S Niveau und ein neuer Prozessor wird für mehr Power sorgen.

Ich verkaufe mein iPad 2 und freue mich auf die nächste Überarbeitung! Und hoffe, dass ich die nächsten Monate noch kein iPad 3 zu Gesicht bekomme...