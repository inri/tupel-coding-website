---
layout: post
title: "Paranormales SEO Update"
description: "Neue Erkenntnisse rund um Links und SEO."
date: 2012-11-13 
category: it
tags: [HPI, Studium, SEO]
published: true
comments: false
share: true
---

Die erste Protokollabgabe steht an und ich teile mal meine ersten Ergebnisse und Erkenntnisse mit. Nachdem ich die erste zeitlang alleine Nr. 1 bei Google und Bing war, gesellten sich nach einigen Wochen andere Websites hinzu und ich wurde tatsächlich verdrängt.     


Der Grund dafür liegt vermutlich zum einen an der besseren Domain des Mitstreiters, denn er hat alle drei Keywords darin, ich nur zwei. Und zum anderen hat er vielleicht mehr Links auf seine Seite als ich. Zumindest verlinkt er seine Seite über einige seiner Nutzer-Profile. Was soziale Netzwerke angeht, müssten wir gleichauf liegen.

Wo war ich eigentlich auf Linksjagd? Ich wollte meine Website bekannt machen und dachte, vielleicht verirren sich dann Links zu mir. Dem war nicht so. Eine andere Möglichkeit sind aber soziale Netze. So habe ich eine Google+ Seite erstellt, auf Twitter verlinkt und bei Facebook geteilt und später auch eine FB-Seite erstellt. Was man nicht alles für die Hochsommerliche Paranormale Insuffizienz tut. 

Ich bin mir nicht sicher, was die sozialen Netze wirklich gebracht haben. Auf Rang 1 bin ich nicht mehr gekommen. Gesammelt habe ich einige Tweets, Facebook-Like und + von Google. 

Ich muss erst einmal noch drüber nachdenken, wie sehr ich andere Bereiche meines digitalen Lebens vollspamme. Besonders seltsam ist die Tatsache, dass dieser Blog mit dem ersten Eintrag über das SEO Seminar auf Platz 2 bei Google liegt. Die Goolge+ Seite kommt nach meiner *richtigen* Seite gleich auf Platz 4, 6 und 7. Das lässt gewisse Schlüsse zu, wie Google seine eigenen Dienste bewertet. Wobei es auch sein kann, dass das Ranking bald wieder durchgeschüttelt wird, zumindest was die Seiten sozialer Netze angeht.     
Auch wenn ich nicht sicher bin, ob eine Verlinkung von diesem Blog auf meine hochsommerliche, paranormale Insuffizienz - Website nicht auch dem Blog zugute kommt bzw. die Keywords, die ich hier im Text verwende, tue ich es trotzdem.

**Nachtrag: Personalisiertes Google**           
Ludwig hat in den Kommentaren auf die personalisierte Suche von Google hingewiesen. Das verfälscht meine Suchergebnisse natürlich ein wenig und daher empfiehlt er den rankingCheck. Leider kann man nur eine Website testen und FB-Seiten oder meinen Blog gleich gar nicht. Die Alternative ist der String *&amp;pws=0,* den man einfach ans Ende der Suchanfrage kopiert.

Die Ergebnisse sehen demnach wie folgt aus:

Meine *richtige* Website: #4 (Impressum #37)      
Blog mit SEO-Eintrag: #2      
Google+ Seite: #6 und #7     
Die Facebook-Seite von Ludwig ist auf Platz 3, aber die zweite Facebook Seite findet sich nicht mehr unter den ersten 5 Seiten. Interessant.