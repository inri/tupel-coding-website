---
layout: post
title: "Hohe Erwartungen dämpfen"
description: "Zu hohe Erwartungen schmälern am Ende nur den Film, das Musikalbum, das neue iPhone,...."
date: 2012-12-02 
category: blog
tags: [Technologie]
published: true
comments: false
share: true
---

Ich war vor einigen Tagen im neuen James Bond Film (Skyfall) und als ich danach über meine Meinung und die vergangenen Bond-Filme nachdachte, bemerkte ich schmunzelnd das bekannte Prinzip, ich hatte nach dem Lesen der ganzen guten Rezensionen und Meinungen über den Film, einfach mehr erwartet. 


Der Film war zweifellos gut und mir fallen keine konkreten Kritikpunkte ein, aber nach der Vorstellung hatte ich das Gefühl dass etwas fehlte.

Genauso ging es mir auch bei *The Dark Knight Rises*, der vielleicht einige Schwächen hat und einige Aspekte, über die man sich streiten kann, aber insgesamt war es ein überdurchschnittlich guter Film. Doch auch da hat mir dieses kleine Quäntchen an letzter Überzeugung gefehlt, um ihn in den Himmel zu loben.

Anders erging es mir mit ihren Vorgängern, *Casino Royal* und *The Dark Knight*. Das eine war mein erster Bond im Kino und beim anderen lockte die Darstellung des Jokers vom verstorbenen Heath Ledger. Ich war schon sehr, sehr oft im Kino, aber habe nur eine Hand voll Erinnerungen an bestimmte Vorstellungen und Filme.       
Eine davon ist der Beginn von Casino Royal, welcher die mit Abstand beste Anfangs-Szene aller Bond-Filme ist, inklusive Titellied. Auch dass das Schauspiel von Ledger gut sei, ahnte man vorher, aber dass der gesamte Film so unglaublich fesselnd und mitreißend wird, hatte ich nicht erwartet. Und freute mich später umso mehr.

Natürlich funktioniert das nicht nur bei Filmen so gut, sondern bei allerlei Gelegenheiten. Bei mir persönlich wären beispielsweise auch die Geräte von Apple dabei. Erwartet hatte ich nicht unglaublich viel, schließlich loben sowieso nur Apple Fans die iDevices. Oder Musik ist quasi das klassische Beispiel in Sachen viel zu hohe Erwartungen.

Und mein Fazit? Meinungen sind wichtig und man sollte Meinungen Beachtung schenken. Aber zu viele oder zu intensive Meinungen verzerren das reale Bild. Wenn ich schon Superlative wie *beste* oder *schlechteste aller Zeiten* höre, will ich den Rest des Satzes meist nicht mehr hören.