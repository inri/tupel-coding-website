---
layout: post
title: "Schützenswert oder nicht?"
description: "Wann ist eine Designidee schützenswert?"
date: 2012-10-24 
category: it
tags: [Design]
published: true
comments: false
share: true
---

Als ich über [das Video vom Flick Scrolling](https://vimeo.com/49375288) gestolpert bin, habe ich mich gefragt, ob diese Idee schützenswert ist. Sie ist simpel wie genial: Statt dem schnellen, unkontrolliertem Scrollen, wird der Bildschirm jeweils nur bis zu dem angetippten Punkt gescrollt. 


Bei scrollbaren Listen wie beispielsweise der Facebook-Timeline, wird mit einem Fingerschubs bis zum nächsten Bild bzw. Eintrag gescrollt. Es ist wirklich sinnvoll und durchaus der bisherigen Scrollarten überlegen..

Aber was ist es wert, dass man es patentrechtlich schützen lässt? Man macht sich über Apples Patent auf das *Swipe to Unlock* lustig. Was ist daran schon eine neue, innovative Idee? Oder das Gummiband-Patent, welches Apple vor wenigen Tagen abgesprochen wurde (zieht man eine Liste über ihren letzten Eintrag hinaus und lässt sie los, springt sie gummiartig wieder zurück).      
Es gibt im UI-Bereich viele Konzepte und Ideen, die einerseits logisch und einfach erscheinen, aber andererseits erst von einer Firma (o.ä.) vorgestellt werden, bevor es andere später übernehmen? 

Ewige Patente sind nicht die Lösung, aber völlig schutzlos würde ich die Ideen trotzdem nicht lassen. Denn einige sind wirklich gut und verdienen zumindest für eine gewisse Zeit den Schutz vor Kopien.