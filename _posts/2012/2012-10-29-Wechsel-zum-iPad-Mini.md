---
layout: post
title: "Wechsel zum iPad Mini?"
description: "Lohnt sich der Wechsel von einem großen iPad zum kleineren iPad mini?"
date: 2012-10-29
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Man braucht kein iPad und ein iPad Mini zusammen. Denke ich. Aber lohnt ein Wechsel zum iPad Mini, wenn man wie ich ein iPad 2 besitzt?

**Das iPad mini hat:**

* die gleiche Auflösung wie das iPad 2, aber eine etwas höhere Pixeldichte (132 ppi vs. 163 ppi)
* den gleichen Prozessor wie das iPad 2 verbaut (A5)
* bessere Front und Backkamera (1,2 MP und 5 MP)
* einen Lightning-Anschluss
* nur eine 16,3 Watt Batterie (iPad 2 hat 25 Watt), verbraucht aber bestimmt weniger Energie
* eine geringere Größe und Gewicht (308 g vs. 601 g)
* einen geringeren Preis im Vergleich zum 16 GB iPad 2 (330€ vs. 400€). 

Ich überlege nun, ob sich ein Wechsel lohnen würde. In meinen Augen ist es wirklich eine Frage der Größe und des Gewichts. Reichen 7,9 Zoll aus oder wären 9,7 Zoll angenehmer, wobei man für die 1,7 Zoll mehr auch  gleich 300 Gramm mehr zu halten hat.

Für welche Apps, die ich mehr oder weniger regelmäßig nutze, wäre der große Bildschirm besser? Spiele sind ganz gut geeignet, nur leider spiele ich nicht sehr viel. Beim Surfen im Web wäre ein größerer Bildschirm sicherlich nicht nachteilhaft. Ich nutze gerne Paper und da ist ein größerer Bildschirm definitiv besser geeignet, weil man eben mehr Fläche hat. 

Mit dem 9,7 Zoll Bildschirm kann man ganz passabel Filme schauen. In den letzten 1,5 Jahren habe ich das aber weit weniger genutzt, als ich vorher gedacht hätte. Zu oft bin ich entweder in der Nähe meiner beiden normal großen Bildschirme, oder habe mein MacBook dabei.     
Viel Spaß macht auch das Bilder zeigen, denn die Bilder sehen einfach sehr gut aus. Aber auch bei dieser Funktion ist die Frage berechtigt: Nutze ich das so oft? Und sowieso: Braucht man das? 

Zum Großteil nutze ich das iPad um zu lesen und das ist mit dem etwas kleineren Bildschirm kein Problem. Ich hatte es schon geahnt, so richtig triftige Gründe für den großen Bildschirm fallen mir nicht ein bzw. sehe ich sie aktuell noch nicht. So gravierend ist der Größenunterschied ja auch nicht. 
Dagegen freu ich mich sehr auf das geringere Gewicht. 600 Gramm sind auch verhältnismäßig leicht, allerdings nicht mit einer Hand zu halten.

Ich werde mir so ein iPad Mini bei Gelegenheit mal live anschauen.