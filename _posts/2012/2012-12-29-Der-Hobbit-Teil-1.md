---
layout: post
title: "Der Hobbit: Eine unerwartete Reise"
description: "Ein kurzes Review zum ersten Hobbit Film."
date: 2012-12-29 
category: medien
tags: [Film]
feature: true
published: true
comments: false
share: true
image: 
  feature: 2012-der-hobbit-1-lg.jpg
  credit: New Line Cinema
  creditlink: http://www.thehobbit.com
---
Ich kam letzten Sonntag in den Genuss des neuen Peter Jackson Meisterwerks, allerdings nur in normalen 3D. Das HFR-Spektakel gönne ich mir in einigen Wochen und bin bereits jetzt sehr gespannt, ob ich den Unterschied sehen und schätzen werde.


Erst einmal zur Einstimmung:

  <iframe frameborder="no" height="166" scrolling="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F2643707" width="100%"></iframe>

Diese Melodie ist für mich der Inbegriff des Tolkien-Universums und sie passt auch sehr gut zum neuen Hobbit-Film, in welchem eine Streicher-lastigere Version zum Einsatz kommt. Dieser ist in der Tat äußerst fröhlich und oft geradezu komödiantisch. 

## Handlung des Buches

Mit meiner Vermutung bezüglich der Handlung des ersten Teils, hatte ich Recht. Etwas überrascht war ich über die wenigen zusätzlichen Elemente, die größtenteils aus Radagast und den Nekromanten bestanden. Sie legen den Grundstein für eine hoffentlich größere Nebenhandlung mit Gandalf im zweiten Teil.

Wie auch im Buch ist Gandalf der Held des ersten Teils. Ian McKellen stemmt schauspielerisch die Hauptarbeit, was den anderen des Ensembles aber nicht angelastet werden kann. Martin Freeman als Bilbo hat beispielsweise nur die Aufgabe mit der Situation überfordert zu sein, was sich erst am Ende des Films und den Ringfund ändert. Wie sehr die anderen Zwerge, neben Thorin, noch Persönlichkeit bekommen, bleibt abzuwarten. Ich kann noch nicht alle auseinanderhalten.

Technisch ist der Film *trotz* fehlender 24 Bildern pro Sekunde herausragend. Sehr angenehm empfand ich den Einsatz der dritten Dimension. Die Plastizität der Umgebung und Figuren ergab ein stimmungsvolles Ganzes, zuletzt gesehen bei Avatar. Wie oben schon erwähnt, freue ich mich schon auf die 48fps-Variante.      
Auch die Special Effects sind erstklassig. Gollum sieht einfach nur lebensecht aus und wer weiß, ständig erzählen sie uns dass Andy Serkis Gollum spielt und in Wirklichkeit haben sie selber einen gezüchtet! Bei 250 Millionen Dollar Budget durchaus möglich. 

## Müssen es drei Filme sein?

Diese Frage kann man frühestens in zwei Jahren beantworten. Ich habe mir noch einmal den ersten Herr der Ringe Film, *Die Gefährten*, angeschaut und das Tempo der Handlung scheint mir flüssiger zu sein. Wenn es in Moria das erste Mal so richtig zur Sache geht, bekommen es zeitlich auch die Zwerge mit den Orks zu tun. Davor passiert bis auf die Bekanntschaft mit den Trollen nichts, was es mit der Verfolgung der Ringgeister aufnehmen kann.       
Einige Minuten weniger wären nicht tragisch gewesen, aber so oder so ist das nur Meckern auf hohem Niveau. Diese Geschichte anzuschauen hat verdammt viel Spaß gemacht und Filme diesen Kalibers erscheinen nur sehr selten.

### Bonus: Meine Lieblingsszene

Am schönsten war das Gespräch zwischen Galadriel und Gandalf. Er versucht ihr zu erklären wieso er Bilbo mit auf diese Reise genommen hat und sagt ihr sinngemäß:

*Saruman glaubt dass nur große Macht das Böse zurückhalten kann. Aber das ist nicht das, was ich erlebt habe. Ich habe gesehen, dass die kleinen Dinge wie alltägliche Taten von einfachen Leuten die Dunkelheit auf Abstand halten. Einfache Taten aus Liebe. Und warum Bilbo Beutlin? Vielleicht weil ich Angst habe und er mir Kraft gibt.*

Das ist eine hervorragende Szene und jeder sollte mal einige Minuten über Gandalfs Worte nachdenken.