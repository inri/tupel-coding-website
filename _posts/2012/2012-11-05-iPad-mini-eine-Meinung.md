---
layout: post
title: "Das iPad mini und eine Meinung"
description: "Mein kleines Review zum neuen iPad mini."
date: 2012-11-05 
category: it
tags: [iPad]
published: true
comments: false
share: true
image: 
  feature: 2012-ipad-mini-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Rund um das neue iPad mini gibt es meines Erachtens zwei interessante Fragen: Ist es die 130€ Aufpreis im Vergleich zum Nexus 7 und Kindle Fire HD wert? Und: Für wen ist das iPad mini überhaupt interessant? Diese Fragen ziehen viele weitere nach sich, die ich hoffentlich beantworten kann.

## Qualität hat ihren Preis

Möchte Apple mit den billigen 7 Zoll Tablets konkurrieren? Nein. Wie auch bei anderen Geräteklassen bietet Apple ein Gerät mit einer vorzüglichen Qualität an. Das iPad mini ist dünner und leichter als die anderen Geräte und auch die Verarbeitung spielt in einer anderen Liga. Die dunkle Aluminium-Rückseite finde ich persönlich optisch ansprechender und ich bilde mir ein, sie ist einen Tick griffiger als die des iPad 2. Die Verarbeitung, das Gehäuse und vor allem die wirklich robusten Aluminium-Buttons heben sich in Sachen Qualität von den anderen Tablets ab. Ja sogar mein altes iPad wirkt alt und unelegant.

*Doch der Bildschirm ist schlechter!* - Das ist Notwendigkeit und Schönheitsmakel zugleich. Ein höherpixeliger Bildschirm würde die Entwickler zwingen die Grafiken und das Design von Apps anzupassen und man hätte erst einmal wesentlich weniger Apps für das mini zur Auswahl.    
In der Praxis ist der Makel kaum spürbar. Schaltet man das iPad mini an, fallen einem nicht sofort die Pixel im Akkusymbol rechts oben ins Auge (wie beim iPad 2). Sehr wohl lässt sich an der Schrift aber erkennen, dass kein Retina-Bildschirm vorliegt.   
Beispielsweise das Display von Microsofts Surface glänzt auch nicht mit der Anzahl der Pixel, dennoch wurde es von den Testern gelobt und das trifft beim mini auch zu. Farben und Kontrast sind einfach super, deshalb bleibt es für mich nur ein Makel, kein Ausschlusskriterium.

Ich bin vom iPad und Retina MacBook pixelverwöhnt, beim Tablet hatte ich die letzten Monate ein 130 ppi Display vor mir. So gesehen ist das iPad mini ein Fortschritt. Jemand, der mit Tablets oder hochauflösenden Bildschirmen bisher nicht viel zu tun hatte, wird ein gutes Display vorfinden.

## Gibs mir leichter

Ich gestehe: Ich liebe leichte Geräte. Das iPhone 5 bei der Vorstellung fand ich ganz gut, aber als ich es mal in der Hand hielt, wollte ich es haben. Immer noch greife ich mir das MacBook Air meiner Freundin, wenn wir im Bett liegen, obwohl mein Retina MacBook auch nicht schwer ist. Nachdem ich das Fire HD, das Nexus 7 und nun das iPad mini nacheinander begutachten konnte, weiß ich das mini zu schätzen.      
Mit der Eleganz dieses Gerätes, kann keiner konkurrieren. Das macht die anderen Tablets keinesfalls schlechter, sondern das mini einfach nur besser. Und es erklärt auch den Aufpreis, zum Teil. Man merkt einfach, dass das eine Gerät in der Produktion teurer war als die anderen beiden.

Ein weiterer Aspekt des Aufpreises ist die Motivation der Firmen. Amazon und Asus/Google verdienen am Verkauf ihrer 7 Zöller nicht besonders viel, denn dem einen geht es darum, eine Plattform für seine Inhalte bereitzustellen (Bücher, Musik, Filme) und der andere möchte einfach ein Tablet-Computer mit seinem System unter die Leute bringen. Und das geht zur Zeit einfach nur, wenn man sich preislich unter Apple positioniert, was das Microsoft Surface vermutlich bald bestätigen wird.      
Apple verdient hauptsächlich mit seinen Geräten Geld. Und ob dann 130€ Aufpreis wirklich zu viel sind, kann in Frage gestellt werden. Laut iSuppli liegen die Kosten des 16 GB mini bei 188$. Das sind für Apples Verhältnisse nicht besonders hohe Gewinne und Transport, Entwicklung, etc. kommen noch hinzu.

## Potential nach oben

Licht und Schatten liegen nah beieinander. Hier und da gibt es einige winzige verbesserungswürdige Aspekte am iPad mini, und damit meine ich nicht nur den Bildschirm. Den aber auch.

Retina Bildschirm, bitte! Wenn hoffentlich nächstes Jahr das iPad mini in Sachen Auflösung das große iPad einholt (2048 x 1536) sollte die Pixeldichte noch unter der des iPhones liegen, aber auf jeden Fall über der des iPads. Müsste man mal nachrechnen, könnte aber stimmen. Ich freue mich.      
Beim Nexus 7 ist die Tastatur besser als beim Fire HD und beide sind besser als die des minis. Da der Rand dünner als bei den Androiden ist, lassen sich die am Rand sitzenden Buttons schwerer erreichen. Der Daumen muss sich ganz schön verbiegen. Abhilfe schafft die geteilte Tastatur, was trotzdem eher nach Notlösung riecht. Die normale iPad-Tastatur ist für das mini nicht geeignet.       

Die Rückseite sieht schick aus, aber ist ein Fingerabdruck-Magnet erster Güte. Die neue Funktion, dass ein Finger am Rand des Displays ignoriert wird, funktioniert meistens sehr gut. Nur hier und da klappt es nicht immer.      
Eine bessere Kamera als beim iPad 2 ist nett, aber warum nicht gleich die neue iPhone Kamera mit 8 MP statt nur 5 MP? Gut, beim neuen iPad ist die Frage wirklich berechtigt, aber das iPad mini war vielleicht einfach zu dünn.       
Buttons anpassen? Bei einigen Buttons habe ich Schwierigkeiten mit der Treffsicherheit. Und in der Tat sind einige Buttons auf dem iPad mini in der Höhe kleiner als auf dem iPhone. Eventuell gewöhnt man sich daran oder Apple verstößt gegen seine eigenen UI-Richtlinien. 

Wie man auf den beiden Bildern erkennt, ist der Bibliothek-Button auf dem iPad mini am kleinsten. Meiner Meinung nach müsste Apple solche Unstimmigkeiten noch ausbessern. Das müssten Drittanbieter-Apps aber auch und das ist eigentlich nicht geplant. Aber vermutlich notwendig. 

(Nur so zur Info, die Kamera hat stets auf die Mitte des Bildes fokussiert, so dass das iPad mini auf dem oberen Bild so scharf ist, dass man die Pixel erkennt und das iPad 2 darunter so unscharf, dass es so aussieht, als hätte es keine sichtbaren Pixel. Das Gegenteil ist aber der Fall. Beim unteren Bild sieht man wiederum wie gut der Bildschirm des iPhones ist. Denn trotz Nahaufnahme sind keine Pixel zu erkennen.)

## Der Wechsel ist perfekt

Ich bin sehr angetan und frage mich: Wieso nicht eher? Ich glaube vor 2 Jahren wäre ein kleines iPad technisch noch nicht möglich gewesen und Jobs wollte es angeblich auch nicht. Die Technologien um Display, Chips und Akku haben sich inzwischen verbessert und lassen ein solch dünnes Gerät zu. Man könnte eventuell auch mit einigen Millimetern mehr in der Dicke leben (10,45 mm wie beim Nexus 7), aber wären es vor zwei Jahren nur einige gewesen?       
Die Antwort ist nein, wenn man sich Samsung Galaxy Tab anschaut. Erschienen im Oktober 2010, war es 40 g schwerer als das Nexus 7 und 70 g schwerer als das iPad mini und mit 11,98 mm deutlich dicker. Und bei 800€ UVP brauchen wir gar nicht weiter reden. 

Es hat also knapp zwei Jahre gedauert, solche Geräte mit dem richtigen Preis und den richtigen Maßen zu entwickeln. Leider. Hätte ich mich damals zwischen dem kleinen oder großen iPad entscheiden müssen, es wäre der kleine geworden. Vielleicht.
Viele mögliche Einsatzgebiete wären mir entweder gar nicht bewusst gewesen oder sogar falsche Erwartungen. Wer auf dem iPad Filme schauen möchte, Fotos bearbeiten oder generell künstlerisch tätig sein will, benötigt wohl eher einen größeren Bildschirm. Zum Lesen, Emails, soziale Netzwerke, Spiele und andere sinnvolle Kleinigkeiten, reicht auch ein kleinerer 8 Zoll Bildschirm vollkommen aus.

Oder anders gesagt: Der erhöhte Preis für ein großes iPad, sowohl Kosten als auch Gewicht und Ergonomie, rechtfertigt für mich persönlich nicht die Vorteile des größeren Bildschirms.      
Deshalb meine Antwort zu Frage vom Anfang:
      
**Das iPad ist für mich, für dich, für alle geeignet.**

Die erhöhte Mobilität ist ein größerer Gewinn als man denkt. Auch die 130€ Aufpreis sind gerechtfertigt. Man kauft ein besseres Gerät und den Zugang zu einer fantastischen Plattform.       
Meine Entscheidung ist gefallen. Mein altes iPad 2 wird verkauft und ich wechsle zum kleinen Bruder. Ich glaube durchaus, dass das kein unüblicher Schritt ist, einige amerikanische Reviewer meinten auch, dass ihnen das kleine besser gefällt und sie den makelhaften Bildschirm in Kauf nehmen.

Besitzern des iPad 3 / 4 würde ich davon abraten sich das mini genauer anzuschauen. Danach kann sich ein großes iPad nur noch alt und schwer anfühlen, was bei einem relativ neuen Gerät nicht so schön ist.