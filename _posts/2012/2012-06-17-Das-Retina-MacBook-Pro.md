---
layout: post
title: "Das Retina MacBook Pro"
description: "Kurzer Kommentar zum neuen Retina MacBook."
date: 2012-06-17 
category: it
tags: [MacBook]
published: true
comments: false
share: true
image: 
  feature: 2012-retina-macbook-lg.jpg
  credit: Apple
  creditlink: http://apple.com
---

Im ersten Moment war ich natürlich sehr begeistert von der Technik die im MacBook Pro steckt. Andererseits hat mich auch der Preis (2280€) überrascht. Zuerst. 


Wenn man die Sache nun genauer betrachtet, ist der Preis gar nicht astronomisch hoch. MacNews hatte das auch schon ausgerechnet: Wenn man das nicht-Retina MacBook Pro kauft und entsprechende Upgrades dazu bucht, also mehr RAM und SSD Speicher, und wiederum beim neuen MacBook die fehlenden Adapter dazu nimmt, kommt man auf den gleichen Preis bzw. ist das neue MacBook sogar einen Tick billiger und besser ausgestattet sowieso.

Ich denke einfach, für ein Gerät wie das MacBook Pro kommt eine gewisse Summe zusammen. Es ist ja auch nicht so gedacht, dass sich das jeder sofort kaufen kann. Das hochauflösende Display stellt einen großen Sprung in der Notebookechnik dar und dieser Sprung findet gerade erst statt.         
Nächstes Jahr oder noch später werden die anderen MacBooks, iMacs und Apple Displays mit entsprechenden Bildschirmen nachziehen und dann sinken auch die Preise. Langsam.

## Reparatur und Upgrade fast unmöglich

Dann noch einige Worte zur Kritik der verbauten Technik besonders in Bezug darauf, dass man ja nichts auswechseln oder selbst reparieren kann. fefe hatte sich darüber lustig gemacht und Apple User ausgelacht, aber die Medaille hat zwei Seiten. Ich denke, es ist vom Design her nicht möglich ein solches MacBook zu bauen, wenn alle Teile Standard-Größen haben.      
Ich bezweifle, dass Apple mit voller Absicht die Akkus hineingeklebt, einen anderen SSD Anschluss benutzt, und den RAM verlötet hat. Es wird einfach nicht anders gehen, sonst wird das Gerät wieder dicker, wie die alten MacBooks.

Ich frage mich außerdem: Was möchte man upgraden? Möchte man mehr als 256 GB SSD Speicher, dann muss man für einen neuen Speicherriegel auch um die 400€ bis 600€ bezahlen. Man kann also für die gleiche Summe einfach das reguläre Upgrade kaufen hat dann man mehr Speicher und eine bessere CPU.

Nicht zuletzt muss man sich fragen was man eigentlich möchte. Entweder einen richtig guten leistungsstarken Laptop mit brillantem Bildschirm, oder einen, an den man herum schrauben kann?       
Abgesehen von den Restriktionen, die mit Apple immer einhergehen, glaube ich kaum, dass so viele MacBook Pro Käufer unbedingt ihr Gerät selber upgraden wollen. Da regen sich ein paar Nerds auf und 99% der Käufer ist es egal.      

Vielleicht ärgern die anderen sich auch nur, dass die Self-Upgrade-freundlicheren Hersteller von Notebooks nichts auf die Reihe bekommen, was so ist wie das MacBook?