---
layout: post
title: "Buy Software, don't sell it!"
description: "Leute, kauft euch öfter mal eine App!"
date: 2012-11-27 
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Anfang Juli wurde ein Urteil des Europäischen Gerichtshof bezüglich des Weiterverkaufs von Software veröffentlicht. Insbesondere auch über den Online-Vertrieb erworbene Software soll man als Käufer auch *gebraucht* weiterverkaufen dürfen. 


Lediglich bei dem Erwerb von Gruppenlizenzen gelte das nicht.
Ich würde gerne genaueres über die Folgen bzw. der Umsetzung dieses Urteils wissen. Bisher sind keine Konsequenzen bekannt geworden. Mir stellen sich einige erhebliche Fragen, die auch schon im heise-Forum diskutiert wurden.

## Software im Wandel

Meiner Meinung nach ist es nicht ganz einfach ein Produkt wie Software mit *realen Produkten* zu vergleichen. Ich denke da zum einen an die App Stores von Google und Apple und zum anderen an Plattformen wie Steam. Sie stellen neue Konzepte dar, in welchen eben nicht mehr eine CD mit Lizenzschlüssel verkauft wird, sondern eine an einem bestimmten Konto gebundene Lizenz. Als Nutzer hat man jeweils ein Benutzerkonto pro Plattform und kauft sich Software einfach über einen Online-Store. Die gekaufte Software lässt sich nur nutzen bzw. aktivieren / herunterladen, wenn auf dem jeweiligen Gerät eine Onlineverbindung zum Store besteht und das Benutzerkonto des Nutzers angemeldet ist. Die meisten Menschen sind mit diesem Prinzip schon in Kontakt gekommen. 

Bei Steam startet das *Store-Programm* automatisch wenn man ein via Steam gekauftes Spiel starten möchte. Man wird eingeloggt und erst dann geht das Spiel los. Sollte kein Benutzerkonto aktiviert sein oder keine Verbindung bestehen, ist meist Schluss, je nach Spiel. Steam kann zwar auch offline betrieben werden, aber mindestens einmal muss das Spiel aktiviert werden und schließlich heruntergeladen.     
Im Offline-Modus gibt es einige Nachteile wie z.b. keine Updates. Ähnlich ist es bei den App Stores. Hierbei haben die Geräte (Mobiltelefone und Tablets) so restriktive Betriebssysteme, dass man nur über den jeweiligen Store Software auf ihnen installieren kann, die illegalen Raubkopie-Möglichkeiten mal ausgenommen. 

Ich habe nur einmal ein Spiel weiterverkauft (ein Anno 1503 oder so) und bisher keine Software gebraucht gekauft. Zugegeben, besonders auf der Windows-Plattform habe ich mich bzgl. Software nicht in Unkosten gestürzt, was zum einen an den irrsinnig teuren Preisen lag und auch an der leichten Möglichkeit Software illegal zu benutzen. Wenn ich mir den lächerlichen Kopierschutz von Adobes Softwarelösungen anschaue, dann kann man fast nur zur Erkenntnis gelangen, dass diese *Lücke* indirekt gewollt ist, aber das ist ein anderer Artikel.

Mit Android und besonders mit iOS hat sich mein Kaufverhalten geändert. Der Grund dafür sind die geringen Kosten. Ich habe mir noch keine App gekauft, die teurer als 4,99€ war, die meisten liegen eher zwischen 0,80€ und 2,40€. In einem älteren Artikel habe ich meine App-Käufe analysiert und gab dabei innerhalb von 11 Monaten pro Monat nicht einmal 4€ für Apps aus.      
Weitere Gründe für (m)ein gesteigertes Kaufverhalten sind natürlich auch die tollen, qualitativ hochwertigen Apps an sich und der bequeme Prozess im Store. Ähnlich würde ich es auch bei Steam begründen, da habe ich auch schon einiges an Geld gelassen, wobei ich fast nur zu den berühmten Holiday-Sales etwas kaufe, denn dann sind ziemlich viele gute Spiele, ziemlich preisgünstig. 

## Software als Dienstleistung

Ich würde nie auf den Gedanken kommen eine im App Store gekaufte App weiterzuverkaufen. Wenn ich in der Recht-Vorlesung richtig zugehört habe, kann man in einem Restaurant eine Speise auch nicht einfach zurückgeben, nur weil sie einem nicht schmeckt. Wenn sie nicht gerade versalzen, verbrannt oder sonst wie nicht genießbar ist, muss sie trotzdem bezahlt werden. Eine App ist zwar ein Produkt, aber meine Argumentation beruht eben darauf, dass ich eine App viel mehr als Dienstleistung betrachte, statt sie einem realen Produkt gleichzusetzen. 

Eine gute App oder generell ein gutes Programm zu schreiben erfordert Zeit. Doch bezahlt man bei Software nicht nur den initialen Prozess der Entwicklung, sondern ebenfalls die Weiterentwicklung und Verbesserung des Programms. Viele Apps erhalten Updates und werden von mal zu mal besser. Wie viel Mühe die Entwickler in die Updates stecken, obwohl sie dafür kein Geld erhalten, erstaunt mich immer wieder aufs Neue.
Viele Apps erleichtern mir mein Leben ungemein. Im Prinzip kann meine Argumentation in eine Diskussion über die Ansicht von Werten enden. Was ist mir was wert?       
Mit meiner Freundin führe ich relativ genau Buch darüber, wie viel wir gemeinsam im Laufe eines Monats für Nahrungsmittel ausgeben. Dagegen bewegen sich meine monatlichen App-Ausgaben im 100stel Bereich und bereichern mein Leben dennoch stets und stetig. Besonders die Preise für Apps auf den mobilen Plattformen sind wirklich gering. 

Ich wollte vor einigen Wochen die Daten auf einer MicroSD-Karte wiederherstellen. Schnell suchte ich ein Programm für Windows 7, lud es herunter und ließ es die Speicherkarte analysieren. Das Ergebnis war toll, aber als ich dem Programm befahl die geretteten Daten doch bitte zu speichern erschien eine freundliche Aufforderung eine Vollversion zu kaufen und diese sollte über 30€ kosten.

Für so ein lächerlich simples Programm soll man so viel Geld bezahlen? Selbstverständlich gibt es viele kostenlose Alternativen, aber diese Preisvorstellung sind keine Seltenheit. Im Mac App Store für Mac OS X Programme sind die Preise höher als bei iPhone und iPad. Es wird interessant sein, wie sich die Preise für Windows 8 Apps einpendeln werden.

## Weiterverkauf eine Frage von Zeit und Geld?

Der teilweise sehr große Preisunterschied von Apps für mobile und nicht-mobile Plattformen ist mir nicht ganz klar. Aber der Preis ist ein wichtiger Faktor für die Frage des Weiterverkaufs. Wer ernsthaft eine App für 2,40€ weiterverkaufen möchte, ist einfach ein Arschloch.      
Es gibt Beträge, die sind zu gering um den Kunden schützen zu wollen. Alles unter 10€ wäre gekauft und kann nicht zurückgegeben werden. War es ein Fehlkauf, dann kann der Kunde dies in einer Bewertung ausdrücken und immerhin auf ein Update hoffen.

Doch wie sähe das wohl bei Profi-Software aus? Möchte jemand sein 900€ teures Photoshop weiterverkaufen, weil er es in der Tat nicht mehr benutzt, kann das durchaus nachvollziehbar sein. Bei diesem Fall kommt ein weiterer Faktor mit ins Spiel: Zeit. Es ist ein immenser Unterschied, ob die Software gerade mal zwei Monate alt ist oder der Käufer sie schon seit einem Jahr nutzt. Nur weil Software keine Abnutzungserscheinung zeigt wie beispielsweise ein Autoreifen, muss sie trotzdem geschützt werden.

Man mag gewisse Parallelen zur Musik ziehen können, aber ein Musikstück wird einmal komponiert und eingespielt und danach verkauft. Ich denke erfolgreiche und gute Software setzt eine Softwarepflege voraus und diese kostet eben Geld.
Unternehmenssoftware der großen Softwarehersteller wird größtenteils über Lizenzen verkauft, also jeder Nutzer wird abgerechnet und nicht die Software als Produkt für die ganze Firma. So einfach.

## Fazit

Nur *teuere* Software dürfte innerhalb eines bestimmten Zeitraums wiederverkauft werden. Software sollte generell als Dienstleistung angesehen werden und weniger als Produkt.
Und die Menschen sollten mehr Software kaufen. Nicht unbedingt Spiele für 50€, sondern einfach mal einige Apps für wenige Euro. Unterstützt die Entwickler, seid neugierig und vielleicht gefällt euch ja, was ihr kauft.