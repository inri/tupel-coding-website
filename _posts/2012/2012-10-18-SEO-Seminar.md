---
layout: post
title: "SEO Seminar"
description: "Ich habe bereits zwei Domains für mein SEO Projekt reserviert. Der Wettbewerb ist eröffnet."
date: 2012-10-18 
category: it
tags: [HPI, Studium, SEO]
published: true
comments: false
share: true
---

Ich belege dieses Semester das *Search Engine Optimization* Seminar. Ein Teil des Seminars sind Vorträge über verschiedene Themen rund um SEO und ein zweiter Teil ist ein Wettbewerb.    


Die Dozenten haben sich eine Wortgruppe ausgedacht, die bei Google noch keinen Suchtreffer hat und nun baut jeder Student eine eigene Website und versucht sie so gut zu machen, dass sie am Ende des Seminars an oberster Stelle zu finden ist.

Als die Begriffe bzw. die Wortgruppe bekannt gegeben wurde, habe ich mir gleich zwei Domains reservieren lassen. Dummerweise habe ich nun noch etwas mehr gelesen und gemerkt, dass meine Domains nicht schlecht sind, denn sie enthalten die Suchbegriffe, aber dass es ohne Bindestriche noch einen Tick besser wäre.

Für die Begriffe *hochsommerliche paranormale Insuffizienz* habe ich die Domains *paranormale-insuffizienz.com* und *hochsommerliche-paranormale-insuffizienz.de* gewählt. Und tatsächlich findet Google aktuell (18.10.2012 - 15:45) für die Begriffe schon meine .com-Domain.

Nun heißt es mehr Theorie anlernen und richtig anwenden.