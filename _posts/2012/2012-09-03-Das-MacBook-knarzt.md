---
layout: post
title: "Das MacBook knarzt"
description: "Leider, leider knarzt mein MacBook und dem muss ich demnächst auf den Grund gehen."
date: 2012-09-03 
category: it
tags: [MacBook]
published: true
comments: false
share: true
---

Seit sechs Tagen habe ich das MacBook Pro Retina in Benutzung und muss leider, leider einen größeren Kritikpunkt besprechen. Er wird mich sogar dazu bringen am morgigen Tag einen Apple-Premium-Reseller-Halbgott-Laden in Potsdam aufzusuchen, mein Problem zu besprechen und, was ich für sehr wahrscheinlich halte, mein MacBook zurückzuschicken. 

Das wäre übrigens auch ein Tip von mir an jeden Menschen, der ein neues, technisches Gerät erwirbt: Nach einer Woche sollte man kurz in sich gehen und ganz wertfrei über sein Gerät nachdenken. Wertfrei einfach aus dem Grund, da man es sogar nach sehr viel weniger Zeit als einer Woche ins Herz schließen kann und offensichtliche Fehler übersieht. Wäre sehr ärgerlich, wenn ein anfangs unwichtiges Detail später den ganzen Spaß verdirbt.     
Nun aber zu meinem Problem, die Überschrift deutet es schon an, das Retina MacBook Pro knarzt etwas. 

Das war von Anfang an so, aber richtig aufgefallen ist es mir erst am Wochenende. Es reicht schon aus, wenn man es einfach trägt und dabei natürlich das Gehäuse in die Hände nehmen muss. Oder wenn man bei aufgeklapptem Bildschirm rechts und links neben dem Trackpad mit dem Finger drückt bzw. die Handballen beim Schreiben drücken. Es knarzt ein wenig. Nicht besonders laut, nicht gefährlich, aber es stört eben, wenn man ehrlich zu sich selbst ist. Bin ich der einzige mit diesem Problem, habe ich mich gefragt? Nein, natürlich nicht.

In englischsprachigen Foren fand ich einige Einträge dazu. Ich hätte es nicht besser beschreiben können, denn denen ging es genau so wie mir. Das MacBook, seine Leistung und sein Bildschirm wird geliebt, aber das Knarzen (engl. creak) stört einfach.      
Und einer brachte es auf den Punkt: Ein über 2000€ teures Gerät sollte nicht knarzen. Und von Apple gleich gar nicht. 
Bei einigen wurden die Schrauben auf der Rückseite von Apple Store Mitarbeitern festgezogen, da es superspezielle Apple-Schrauben sind, kann man das selbst vermutlich nicht, außer man besorgt sich den richtigen Schraubenzieher.        
Andere denken, es liegt an den nicht ausreichend geölten Federn, die auf dem Mainboard sitzen und Druck vom Gehäuse auffangen sollen. 

Ich kenne den Grund nicht, aber alle bekamen ihre Geräte problemlos ausgetauscht und waren danach sehr glücklich, da sie nun knarzfrei waren.

Ich wollte aber eigentlich nicht nur vom Knarzen schreiben. Bei einem Gerät das nur einen Bruchteil so teuer ist, würde man das vielleicht gar nicht kritisieren. Das Arbeiten mit dem MacBook macht riesig Spass.