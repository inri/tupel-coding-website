---
layout: post
title: "Unpacking Nexus 7"
description: "Ich bin positiv vom Nexus 7 überrascht."
date: 2012-11-03 
category: it
tags: [Android]
published: true
comments: false
share: true
---

Mein erster Eindruck nach dem Auspacken? Es fühlt sich in der Tat leichter und schmaler als das Fire HD an, was natürlich daran liegt, dass das Nexus 7 einfach leichter und schmaler ist.

## Elegantes Android

Aber schon nach den ersten 10 Minuten mit dem reinen Android manifestiert sich auch softwareseitig der Geschwindigkeitsunterschied.      
Ich persönlich mag die Rückseite des Nexus lieber als beim Fire HD, denn sie ist griffiger. Auch die Tastatur, die ich beim Fire HD voll in Ordnung fand, ist beim Nexus einfach besser. Und da Android 4.2 noch nicht installiert ist, wird sie bald vielleicht noch besser.

Das Gefühl, welches ich beim Erkunden beider Android-Tablets hatte, ist am besten folgendermaßen beschrieben: Nachdem ich das Fire HD ausgepackt und begutachtet hatte, dachte ich mir, dass es für die 200€ ein ziemlich gutes Stück Technik ist, was Amazon da gezaubert hat. Es hat seine Macken und hier und dort ist nicht alles perfekt, sowohl Soft- als auch Hardware, aber für 200€, was will man mehr?       
Und dann packte ich das Nexus 7 aus und war ziemlich erstaunt. Sowohl das System als auch das Gerät sind dem Kindle Fire HD deutlich überlegen. Und wiederum fragte ich mich, wie man so etwas für 200€ anbieten kann?

Ich freue mich sehr auf das iPad mini, welches ja deutlich teurer ist.

Den Platz am Gehäuse spart Google bzw. Asus ganz klar an den langen Seitenrahmen. Die sind so dünn, dass man keinen kompletten Daumen darauf ablegen kann. Ist das schlimm? Nach den ersten Stunden mit dem Nexus würde ich sagen, nein. Ein Langzeittest kommt vielleicht zu einem anderen Ergebnis.

Ich erkunde seit einigen Monaten Abstinenz mal wieder Android und bin höchst erfreut. Aber ein ausführlicher Bericht folgt.