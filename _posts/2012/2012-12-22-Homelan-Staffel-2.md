---
layout: post
title: "Homeland, Staffel 2"
description: "Ein Review der 2. Staffel von Homeland."
date: 2012-12-22 
category: medien
tags: [Serie]
published: true
comments: false
share: true
image: 
  feature: 2012-homeland-lg.jpg
  credit: Showtime
  creditlink: http://www.sho.com/sho/home
---

Ich wollte schon etwas über die erste Staffel von Homeland schreiben, aber da ich sie erst spät entdeckte, war sie schon veraltet. Und keine zwei Wochen nach dem grandiosen Finale dieser ersten Staffel begann auch schon die zweite Staffel für mich.       

Die hatte es natürlich nicht leicht. Wir Zuschauer und die Hauptperson, die CIA Analystin Carrie, wussten, dass der nach 8 Jahren aus der Gefangenschaft zurückgekehrte Sergeant Brody ein Agent des al-Qaida Netzwerkes ist und kurz davor war einen Anschlag zu verüben. Darum war die erste Staffel enorm spannend. Ständig fragte man sich, findet sie einen Beweis oder verrät er sich selbst? Ahnt seine Familie etwas? Dabei war besonders die Chemie zwischen den beiden Hauptdarstellern und generell die Figurenzeichnung enorm gut. Das erstaunt fast ein wenig, zumindest in Anbetracht des heiklen Themas Terrorismus.

Am Ende führte Brody sein Selbstmordattentat nicht aus und Carrie, die sowieso mit einer mentalen Krankheit zu kämpfen hatte, begibt sich in Behandlung. Niemand glaubt ihr. Bis zu Beginn der zweiten Staffel im Ausland eine Kopie des Abschiedsvideo von Brody gefunden wird, Bäm!

## In welche Richtung soll es gehen?

Die enorme Spannung der ersten kann die zweite Staffel leider nicht immer halten. Brody macht politische Karriere und wird zum Vizepräsidenten-Kandidaten. Dabei steht er aber weiter unter der Knute seines Peinigers Abu Nazir, wobei die CIA mittlerweile Brodys wahre Mission kennt und ihn unsanft dazu überredet ein Doppelagent zu sein.     
Daneben entwickeln sich aus den in der ersten Staffel angedeuteten Gefühlen zwischen Carrie und Brody tatsächlich richtige. Als Zuschauer ist man sich nie wirklich ganz sicher, ob sie sich ihm gegenüber aus Liebe so verhält oder aus beruflichen Pflichtbewusstsein. Brody soll Abu Nazir ans Messer liefern, was zwar funktioniert, aber insgesamt einfach zu leicht war, das empfand ich zumindest.

Und ich sollte Recht behalten. Die vorletzte Episode mutet wie das Staffelfinale an, denn Carrie findet den Terroristenanführer Abu Nazir und dieser wird erschossen. Ist am Ende alles gut, auch wenn der Vizepräsident gestorben ist? Nein.      
Die erste halbe Stunde des Staffelfinales wirkte auf mich wie die Ruhe vor dem Sturm. Eine drohende Gefahr für Brody wurde schnell abgewendet. Alle Handlungsstränge wurden entwirrt und es hätte ein sauberer Schluss sein können. Doch dann ging die Episode in die zweite Hälfte und es gab einen Knall. Was übrigens eine sehr vernünftige Entscheidung der Autoren gewesen ist.       
Ich begann diesen Absatz während des Anschauen zu schreiben und keine 5 Minuten nachdem ich diese Vorahnung formulierte, passierte es tatsächlich: Eine Bombe in Brodys Auto explodiert bei der Beerdigungszeremonie vom Vizepräsidenten. Und reißt um zweihundert Menschen in den Tod. 

Brody und Carrie überleben, da sie durch Zufall nicht in der Halle waren. Wusste Brody von dem Anschlag? Dem Zuschauer und auch Carrie wird schnell klar, dass er diesmal unschuldig ist. Doch die Bombe war in seinem Auto, man wird ihn suchen. Also fälscht Carrie ihm einen Pass und schickt ihn über Kanada ins Ausland. Sie selbst bleibt um die wahren Täter zu finden und ihn zu entlasten. Gleichzeitig läuft das Abschiedsvideo von ihm im Fernsehen, was natürlich den Verdacht verstärkt.

Ich kann mir sehr gut vorstellen, dass die kommende dritte Staffel wieder spannender wird. Brody muss vermutlich undercover unterwegs sein und Carrie erneut unwahrscheinliche Vermutungen beweisen.       
Die zweite Staffel war trotz den wenigen Längen sehr sehenswert. Schaut euch also unbedingt Homeland an!