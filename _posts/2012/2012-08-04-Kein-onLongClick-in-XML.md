---
layout: post
title: "Kein onLongClick in XML"
description: "Wieso kann ich einem Button keine onLonkClick im Code zuweisen?"
date: 2012-08-04
category: it
tags: [Android]
published: true
comments: false
share: true
---

In einer Android App, die ich gerade baue, muss ich sehr viele Buttons benutzen. Leider reagiert jeder dieser Buttons auf normale *onClicks* und *onLongClicks*, doch zu meinem Glück tun sie stets das gleiche. 


Man könnte jedem Button eine *onClickListener* und *onLongClickListener* geben, aber das bedeutet neben viel Code auch ein erhöhter Aufwand in der Initialisierung, was wiederum eine schlechtere Performance nach sich zieht.

Was den *onClickListener* angeht gibt es eine elegante Lösung. Statt einen *Listener* im Code einzurichten,  kann man in der XML-Datei die Methode festlegen, die bei einem Click aufgerufen werden soll.

	android:onClick = Methode

Ich weiß leider nicht wieso, aber diese Möglichkeit bietet nur *onClick*. Für die *onLongClicks* muss ich viel redundanten Code schreiben bzw. copypasten.

Ehrlich gesagt ärgert mich dieser Umstand sehr. Wir sind mittlerweile bei API 16 und es geht immer noch nicht. Meine Vermutung ist ja, dass es einen mehr oder weniger guten Grund dafür gibt, aber eine offizielle Antwort habe ich bisher nicht finden können.