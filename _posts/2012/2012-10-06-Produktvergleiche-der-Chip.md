---
layout: post
title: "Produktvergleiche der Chip"
description: "Ich schaute mir ein Video der Chip an und verstehe nicht, wie man Produkte so mies vergleichen kann."
date: 2012-10-06
category: it
tags: [Technologie]
published: true
comments: false
share: true
---

Das Angebot an Produkten in unserer kapitalistischen Welt zeichnet sich insbesondere durch Vielfalt aus. Ob Autos, Haushaltsgegenstände, Bücher, Instrumente oder technische Geräte wie Smartphones, fast immer bietet eine große Anzahl von Unternehmen das *gleiche* Produkt an. Dem Verbraucher wird die Ehre zuteil eine Wahl zu treffen.  

    
Damit der Kunde sich für das Produkt entscheidet, das für ihn am besten geeignet ist, stehen ihm viele Möglichkeiten zur Verfügung. 

Er kann sich selbst informieren (zeitintensiv). Er kann Bekannte und Freunde fragen (wenn man jemanden kennt, der sich auskennt). Er kann sich im Geschäft beraten lassen. Oder er holt sich Rat bei Zeitschriften und (Web-)Magazinen. Produkttests können in der Tat sehr hilfreich sein und insbesondere die angesprochenen professionellen Magazine haben viel Erfahrung und die notwendigen Hilfsmittel um ausführliche Testes durchzuführen. 

Ein solches Magazin ist die Chip. Sie tut sich beispielsweise durch das Einordnen von Produkten in Top-100 Listen hervor. Durch Zufall stolperte ich über ein [iPhone 5 gegen Galaxy S3 Video](http://www.chip.de/video/iPhone-5-gegen-Galaxy-S3-Video_57727030.html) und war wieder einmal froh dieses Magazin schon vor Jahren aus meinem RSS-Feed gelöscht zu haben.      
Meiner Meinung nach kann man solche Videos nicht einfach als Meinung über ein Produkt abtun. Das Zielpublikum der Chip ist ein eher einfacher und technikinteressierter Mensch. Jemand, der sich zum Beispiel ein neues Smartphone kaufen möchte und nach dem besten Gerät für sich sucht.      
Googelt man *gutes Handy* bekommt man als ersten Treffer die Chip. Bei *gutes Smartphone*  liegt sie immerhin auf Platz 2 und 3. 

Aber die Art und Weise wie die Chip Tests durchführt und Produkte bewertet, ist eben nicht der beste Weg für den Kunden. Ich will gar nicht auf die Top-100 Listen herumkritisieren. Es sollte jedem klar sein, dass sie nur als Anhaltspunkt verwendet werden sollten oder nur für ganz bestimmte Produkte (z.b. Festplatten zeichnen sich vorwiegend durch ihre technischen Werte aus und diese kann man in eine Reihenfolge bringen).      
Mich stören viel mehr solche Videos wie das oben verlinkte. Ein Redakteur stellt die Geräte vor und es wird eine Art produktnahe Bewertung vorgegaukelt. Dabei sind die meisten Informationen aus dem Video subjektiver Blödsinn. Ein Kunde kann vieles tun, aber er sollte sich nicht nach diesem Video richten. 

Ich fasse die Analyse aus dem iPhone und Galaxy S3 Vergleichs-Video mal zusammen:

* Sprachqualität, Performance und Kameraqualität nur mit minimalen Unterschieden
* iPhone Display ist mit Daumen gut bedienbar, beim S3 wird es schwieriger (oho!)
* iPhone Display heller, S3 Display hat satteres Schwarz und kräftigere Farben
* das S3 hat eine *spürbar längere Akkulaufzeit"* als das iPhone 5
* den S3 Akku kann man wechseln und eine MicroSD Karte einstecken, beim iPhone nicht
* das S3 hat einen MicroUSB Anschluss und mit Adapter kann man auch über HDMI an Fernseher anschließen, beim iPhone braucht man einen Adapter für alles wegen dem Lighting Anschluss
* iPhone LTE nur für Telekom, Samsung zieht beim S3 wohl bald nach und dann für alle Netze
* Vorteile des Android Systems sind Widgets, Schnellzugriff (Statusleiste oben) und Direkteinstellungen (demonstriert an Einstellungen für Browser), iPhone hat nichts und kann nichts davon
* neu an iOS 6 ist die Karten App mit *cooler 3D-Ansicht*, die aber noch nicht so gutes Material hat (blabla), das S3 hat die zuverlässige Karten App von Google
* Siri von iOS ist besser als S-Voice (*Abgründe tun sich auf*)

**Fazit:** Das S3 ist das minimal bessere Smartphone, aber beide sind an und für sich auf sehr hohem Niveau und wegen ihrer System auch sehr unterschiedlich.

In erster Linie offenbart die Chip mit diesem Video, dass sie keine Ahnung hat. Vielleicht kann man ihr auch unterstellen, dass sie das iPhone mit Absicht schlecht redet. Dazu einige Anmerkungen zu den oberen Punkten.     

Ich weiß nicht woher die Chip ihre Performance-Messungen hat, aber sowohl Benchmarks als auch in normaler Benutzung ist das iPhone 5 einfach das schnellste Smartphone zur Zeit. Mit Android 4 wurde zwar vieles verbessert, aber ob es tatsächlich an ein superflinkes iOS 6 herankommen kann? Vor allem gefühlt.       
Auch die Kamera im iPhone ist sehr, sehr schnell und sehr gut. Ich hatte noch nirgendwo gelesen, dass die Qualität der eines S3 gleichkommt.       
Das iPhone ist nicht nur kleiner, es wiegt auch wesentlich weniger. Das dürfte vor allem auch Frauen interessieren, die bei der Bedienung eines S3 sicherlich nicht nur *einige Schwierigkeiten* haben werden. Das Display des iPhones kann als Referenzbildschirm genutzt werden. Es ist einfach das beste mobile Display auf dem Markt. Die Farben des S3 sind nicht kräftiger sondern unnatürlich knallig. 

Dass das iPhone trotz seiner Größe und vor allem Dicke auf die (fast) gleiche Akkulaufzeit wie der Vorgänger kommt, ist weniger interessant als der Vergleich mit einem Riesen wie das S3 (in welchem ja auch mehr Akku passt). Und was ist bitte eine *spürbar längere Akkulaufzeit*?      
Wechselbarer Akku und SD-Karte? Da fehlt ja nur noch Flash, genauso hirnrissig wie die *guten Adapter* für MicroUSB und die *bösen Adapter* für Lightning. LTE? Geschenkt.      
Am Vergleich der Betriebssysteme kann man die große Recherchekunst der Chip beobachten. Android wird über den Klee gelobt, keine Kritik (außer an S-Voice) und bei iOS suchen sie sich ausgerechnet die Karten App zum Zeigen und kritisieren. Und die Sache mit den Direkteinstellungen ist doch wohl ein schlechter Witz.

Mit Sicherheit ist das S3 nicht das minimal bessere Smartphone, aber dieses Video ist nicht nur spürbar schlecht, sondern maximal schlecht. Und zum jetzigen Zeitpunkt hat es schon 120.000 Klicks. 

## Doofe Vergleiche

Monate nachdem die ersten Smartphone-Flagschiffe mit Android die Käufer überzeugten, dümpelten in den Top-100 Listen der Chip immer noch Nokia-Smartphones auf den obersten Plätzen vor sich hin. Das iPhone scheiterte schon immer an den seltsamen Eckpunkten der Chip-Tests.       
Auch dieses Video zeigt sehr deutlich wie schlecht die Menschen von der Chip informiert und beraten werden. Nein, damit meine ich nicht, dass das iPhone das bessere Smartphones sein muss. Man kann beide Smartphones mit ihren Systemen nicht auf diese Art und Weise gegeneinander antreten lassen und vergleichen. Jedes Gerät hat seine Nutzergruppe, aber das wird in dem Chip-Video einfach nicht deutlich. 

Ich denke trotzdem, dass das iPhone aufgrund seiner Größe und iOS für eine größere Zielgruppe optimaler ist als das S3. Aber dieser Betrachtungswinkel hat in dem Video keinen Platz. Doppelt ärgerlich ist die Tatsache, dass die Bewertung sich auf teils falsche Fakten stützt. Der Zuschauer bekommt von der Chip nicht nur eine zweifelhafte Empfehlung, sondern wird auch noch belogen.

Wie in vielen anderen Bereichen der Presse ist die Computerbranche auch schon sehr oberflächlich geworden. Leider verliert dabei vor allem der Kunde.

P.S. Ja, ich bereue es, diese Video angeschaut zu haben. Zweimal.