---
layout: post
title: "iOS Benutzung dominiert vor Android"
description: "Benutzen iOS oder Android Besitzer ihre Geräte öfter?"
date: 2012-11-26 
category: it
tags: [iOS]
published: true
comments: false
share: true
---

In den letzten Tagen schwirrten mal wieder Zahlen durch die Welt, dass der Anteil an Android Geräten wesentlich größer ist als der von iOS-Geräten. Ist Apple auf dem absteigenden Ast? Wird Android die Weltherrschaft übernehmen? Nein und nein. 

Zu wissen, dass da draußen viele Android Geräte sind, ist ganz gut. Überschnappen muss man aber nicht, denn sie werden anscheinend selten von ihren Besitzern genutzt. Leider kann man keine genauen Zahlen der allgemeinen Benutzung heranziehen, aber man kann sich zumindest anschauen, wie viele Menschen mit ihrem Gerät im Web surfen.      
Genau das hat [Michael Bartholomew von tenfingercrunch](http://www.tenfingercrunch.com/article/196/2012/11/26/mobile_market_share_not_equivalent_to_usage_share/) gemacht und zwar am vergangenen Freitag, dem großen Einkaufstag zu Thanksgiving, genannt Black Friday. Die genauen Zahlen finden sich auf seiner Seite, aber kurz gesagt: iOS Besitzer nutzen ihr Gerät 15,6 mal öfter um ins Internet zu gehen als Android Besitzer (global). Bei Online-Einkäufen waren es immerhin noch 5x so viele, natürlich in US.

Meine Vermutung ist: Die Verteilung bei der gesamten Nutzung geht noch deutlicher in Richtung iOS, denn das Surfen via Browser geht auf allen Plattform mittlerweile wunderbar. Aber das Nutzen von Apps und deshalb dem Gerät allgemein ist ein etwas anderes Blatt Papier. 