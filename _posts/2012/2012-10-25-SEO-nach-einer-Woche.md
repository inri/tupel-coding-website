---
layout: post
title: "SEO nach einer Woche"
description: "Erste Erkenntnisse nach einer Woche SEO Seminar."
date: 2012-10-25 
category: it
tags: [HPI, Studium, SEO]
published: true
comments: false
share: true
---

Vor knapp einer Woche habe ich meine SEO-Seite online gestellt. Sucht man heute nach den drei Begriffen findet man meine .com-Seite an erster und meinen Blog ab der 3. Stelle. Auf Nummer 2 hat sich eine neue eingeschlichen, die aktuell aber noch leerer ist als meine. 

Dabei muss ich zugeben, dass die neue Seite eine bessere Domain besitzt als ich. Einige Tage vor dem Seminar habe ich mir einige Dinge über SEO angelesen. Relativ wichtig sei dabei, dass die relevanten Suchbegriffe im Domain-Namen auftauchen. Damit die Domain, wenn sie schon sehr lang ist, zumindest leserlich bleibt, habe ich die Trennzeichen (*-*) zwischen den einzelnen Wörtern eingefügt. Das war nicht gut. Mittlerweile habe ich weiter gelesen und weiß nun, dass eine Domain ohne solcher Zeichen besser bewertet wird. 

Dass an erster Stelle meine .com-Seite auftaucht und nicht die lange .de-Seite, liegt daran, dass ich die .com-Domain zuerst erhalten und online gestellt habe. Wie genau die Bewertung abläuft, wenn mehrere Domains auf eine Seite zeigen, muss ich noch herausfinden.

## Blog

Überrascht war ich über die vielen Einträge auf meinem Blog. Dass der Eintrag über das SEO-Seminar erscheint, habe ich schon geahnt. Aber die anderen Ergebnisse sind auf die von mir verwendeten Tags zurückzuführen. Scheinbar werden alle Tags bei der Bewertung der einzelnen Seiten hinzugezogen, auch wenn der Blog-Eintrag auf der Seite diese Tags gar nicht nutzt. 

Sehr hoch scheint die Wertung des Blogs dennoch nicht zu sein, denn die fast leere Seite (sie nutzt die Keywords nur im Head und in einer h1-Überschrift) ist auf Platz 2.

Insgesamt ist das ganze sehr interessant, auch in Anbetracht meines Seminar-Themas: Keywords.