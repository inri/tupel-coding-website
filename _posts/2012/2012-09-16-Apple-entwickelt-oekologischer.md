---
layout: post
title: "Apple entwickelt ökologischer"
description: "Eine These zum ökologischen Aspekt von Apples Geräten."
date: 2012-09-16 
category: it
tags: [iPhone, Technologie]
published: true
comments: false
share: true
---

Mir kam ein interessanter Gedanke rund um die Kritik am neuen iPhone 5, dass es ja keine Innovationen hätte und technisch den anderen High-End Smartphones auf dem Markt unterlegen sei.

Ich beginne mit den Voraussetzungen, die ich an ein Smartphone der gehobenen Klasse ansetzen würde. Es sollte grundlegende Techniken und Funktionen problemlos beherrschen und des weiteren einfach und schnell zu Bedienen sein.      
Die eine Hälfte wird durch Kamera, Bildschirm, Telefonie und WLAN bzw. mobiles Internet gewährleistet. Die andere Hälfte ergibt sich aus der Software im Zusammenspiel mit der Hardware. Man kann über iOS als System sagen was man möchte, aber es läuft flüssig. Genauso die Apps die man installiert. Die Benutzung ist schnell und einfach, da kann sich niemand beschweren.      

Ich frage mich daher, wieso Apple beispielsweise mehr RAM verbauen sollte? Was ist der Nutzen? Der neue A6 Prozessor hat Apple genau nach seinen Bedürfnissen angepasst. Der Fokus liegt auf Performance und Energieeffizienz. Mag sein dass andere Smartphones Prozessoren verbaut habe, die auf dem Papier eine größere Zahl nach dem Komma stehen haben als der A6. Aber wenn das System samt seiner Apps schon superschnell läuft, wieso sollte man noch mehr Power hineinpumpen? Es macht ja doch keinen Unterschied

Ich bin immer noch der Meinung, dass eine Bildschirmgröße zwischen 3 und 4 Zoll der richtige Kompromiss für ein Mobiltelefon ist. Man kann mit einer Durchschnittshand alles bedienen und es lassen sich ausreichend viele Informationen auf dem Bildschirm darstellen.        
Meine Vermutung ist daher, dass der Grund für die vielen 4,x Zoll Bildschirme darin liegt, dass die Hersteller entweder einfach größere Zahlen vorweisen wollen oder dass sie mit der größeren Fläche auch eine größeres Gehäuse bei gleichbleibender Dicke erhalten. Und nur darin können sie die technischen High-End Komponenten packen, da in kleineren Gehäusen nicht genug Platz ist, oder sie nicht die Mühen aufwenden wollen um die notwendigen Verfahren dafür zu entwickeln.

## Die These

Ich stelle hiermit folgende These auf: Das Apple iPhone ist ökologischer als die meisten anderen Smartphones [^1]. 
Ich denke Apple verbaut nur das Optimum an Technik, das notwendig ist um den Benutzer die beste Benutzungserfahrung zu bieten.     
2 GB RAM benötigen mehr Rohstoffe als 1 GB. Wenn sie in der Praxis aber keinen Unterschied machen, wieso sollte man sie verbauen? Das gleiche trifft auch für große Bildschirme zu. Auch die sogenannten Innovationen wie NFC benötigen Rohstoffe in der Herstellung. Aber wenn sie dem Benutzer in der Praxis keinen Mehrwert bieten, weil die Technik nicht ausgereift oder viel zu wenig verbreitet ist, wieso sollten sie verbaut werden? 

Gleichzeitig spart Apple natürlich Geld und hat eine höhere Gewinnmarge. Daran ist per se nichts schlechtes, die anderen Hersteller wollen auch Gewinn machen. Eventuell können sie aber auf einige technische Leistungsmerkmale nicht verzichten, weil sonst die Benutzungserfahrung darunter leidet. Der Grund könnte bei Android zum Beispiel einfach das System an sich sein. Ich weiß, mit der neuen Android Version wird das wohl besser, aber dieser Hardwarekampf wird schon ewig geführt.

Es ist vorerst nur eine These. Es gibt noch viele weitere Aspekte zu beachten. Zum Beispiel: Wie ökologisch ist die Verwendung und Herstellung von Aluminium vs. Kunststoff? Ich würde sagen, letzteres ist weniger gut, aber ich weiß es nicht.      
Auch die unterschiedlichen Verhältnisse müssen beachtet werden. Ein Bauteil in einem iPhone hat eine wesentlich größere Auswirkung, da Apple nun mal verdammt viel davon verkauft.      
Aktuell arbeitet Apple sehr an seiner Transparenz bzgl. Arbeitsbedingungen in den Fabriken. Sie haben die Vorreiter-Pflicht, schließlich sind sie mit die größten Abnehmer, aber andere Firmen müssen auch bald nachziehen.

Wieder mal ein Wunsch von mir: Möge mal bitte jemand ökologische Profile von Smartphones erstellen.

[^1]: Das trifft vermutlich auch auf andere Geräte von Apple zu.