---
layout: post
title: "Wenig Interesse für Modellierung"
description: "Einige Worte zur Vorlesung Modellgetriebene Softwareentwicklung."
date: 2012-10-16 
category: it
tags: [HPI, Studium]
published: true
comments: false
share: true
---

Ich habe vor einigen Semestern das Seminar *Einführung in die Modellgetriebene Softwareentwicklung* belegt. Jeder Student erhielt ein Thema, schrieb ein Paper und hielt eine Präsentation dazu. Für das gleiche Vertiefungsgebiet fehlen mir noch einige Punkte und so war ich ganz glücklich, als ich die Vorlesung *Modellgetriebene Softwareentwicklung* auf dem Stundenplan für das Wintersemester entdeckte. Ich war einer der wenigen.

Zehn Minuten vor Vorlesungsbeginn betrat ich als erster den Hörsaal. Es kamen tatsächlich noch einige Studenten: drei weitere aus meinem Jahrgang (7. Semester im Bachelor), zwei jüngere Bachelorstudenten und ein Masterstudent, der die Vorlesung vermutlich gar nicht belegen kann. Der Dozent fragte uns, wer sich schon sicher ist, die Vorlesung zu belegen, nur wir vier meldeten uns. Die maximale Anzahl an Studenten war eigentlich 18.       
Dabei ist die Vorlesung gar nicht so uninteressant. Ursprünglich wurde sie nur im Master angeboten, aber die vermittelten Techniken sind durchaus für Bachelorstudenten schon interessant. Außerdem hat der Modellierungslehrstuhl abgesehen von Modellierung I und II nicht besonders viel für die Bachelorstudenten im Angebot, nur das Seminar und jetzt eben diese vertiefende Vorlesung.     
Ich vermute, in der nächsten Woche kommen noch einige Studenten hinzu. Denn die anderen Seminare haben auch nur begrenzte Kapazitäten und nicht jeder bekommt einen Platz. 

Lobend möchte ich noch die Organisation der Vorlesung erwähnen. In der ersten zwei Monaten gibt es nur die Vorlesung, zweimal wöchentlich. Danach findet der praktische Teil statt und in Gruppenarbeit wird ein kleines Projekt verwirklicht. Sinnvoll ist das ganz einfach aus dem Grund, weil wir mit einer Menge verschiedener Tools und Techniken arbeiten werden und würde man diese Übungen auf das gesamte Semester aufteilen, gäbe es nur kleinere, weniger interessante Übungen. Das Einarbeiten würde jeweils zu lange dauern. Alles hintereinander zu basteln, klingt für mich sinnvoller. 
Weniger schön finde ich die mündliche Prüfung am Ende. Aber man kann nicht alles haben.