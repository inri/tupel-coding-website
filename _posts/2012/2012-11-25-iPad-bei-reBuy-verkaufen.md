---
layout: post
title: "iPad bei reBuy verkaufen"
description: "Ich möchte mein iPad loswerden, aber nicht unbedingt bei ebay."
date: 2012-11-25 
category: it
tags: [iPad]
published: true
comments: false
share: true
---

Wie bereits angekündigt, habe ich mein altes iPad 2 verkauft. Für mich stellte sich im Vorfeld die Frage: Wo bekomme ich das meiste Geld dafür? eBay ist nicht notwendigerweise die beste Adresse, aber am Ende hängt es vom zu verkaufenden Artikel ab.

Im Vorfeld sollte man einige Wochen lang auskundschaften, was bei eBay für ein iPad so bezahlt wird. In meinem Fall war das für das iPad 2 Wifi, 64 GB, in schwarz im Durchschnitt 360€. Die Ausreißer muss man natürlich ignorieren, denn die Info, dass man für ein noch eingeschweißtes iPad 460€ bekommen kann, ist zwar interessant, aber für mein abgenutztes Gerät nicht relevant.       
Also 360€, meist mit Zubehör, was in meinem Fall ein Smart Cover, Bluetooth Tastatur und eine Filztasche wäre. Das erschien mir sehr gering. Dazu kommen die eBay-Gebühren, was bei 9% immerhin 32€ sind, was den Gewinn auf 330€ schmälert. 

## reBuy statt eBay

Welche Alternativen gibt es? Der Familien- und Bekanntenkreis war nicht interessiert. Andere Auktionsplattformen sind meist zu wenig besucht und ein Angebot auf dem Marketplace wollte ich ungern, denn der Verkauf kann lange dauern.       
Es etablieren sich mittlerweile Shops, die gebrauchte Produkte zu einem Festpreis kaufen und dann wieder verkaufen. Der prominenteste in Deutschland dürfte reBuy sein und genau dort habe ich mal nachgeschaut, was mein iPad denen noch wert ist. Interessanterweise lag der Preis für ein gebrauchtes Gerät im guten Zustand bei 300€. 

Ich schickte das iPad zur Durchsicht zu reBuy und man bot mir nur noch 260€ an, da mein iPad Kratzer auf der Rückseite und einen auf dem Bildschirm hat. 

Die Entscheidung war dann nicht mehr ganz so einfach: Entweder das sichere Geld nehmen und das Zubehör noch für einige Euros bei eBay loswerden (25 - 50€), oder das eBay-Risiko wählen und vielleicht zu wenig Gebote zu bekommen oder nach dem Verkauf Ärger mit einem unzufriedenen Käufer zu haben? Ich habe die eBay-Preise nochmals inspiziert, aber so richtig aussagekräftig sind sie für mich nicht, denn alle Angebote waren *nagelneue* iPads, scheinbar ohne Kratzer. Ein wenig schade ist es trotzdem.

## Wertverlust iPad vs. iPhone [Stand: 12.11.12]

Noch eine interessante Entdeckung an die Statistikfreunde. Wenn man ein Gerät für einen Preis A verkauft, ergibt die Different zwischen Kaufpreis C und A die tatsächlichen Kosten B. Logisch.        
Für den Fall, dass das iPad in der besten Qualität vorliegt, wären das 680€ - 330€ = 350€. Also hätte ich in den letzten Monaten insgesamt 350€ für mein iPad bezahlt. Die gleiche Rechnung kann ich auch für mein iPhone vornehmen (32 GB) und komme auf ähnliche Werte: 740€ - 390€ = 350€. reBuy zahlt für ein gute erhaltenes iPhone 4S nur noch 390€.  

Für beide Geräte würden meine Kosten je 350€ betragen. Der Unterschied ist nur, das iPad habe ich vor über 19 Monaten gekauft, das iPhone erst vor 13 Monaten. Oder anderes gesagt, für das iPad habe ich monatlich 18€ bezahlt, für das iPhone sogar 27€.       
Scheinbar ist der Wertverlust des iPhones wesentlich höher. Ich bin mir aber nicht ganz sicher, ob die Zahlen von reBuy repräsentativ sind. Ende September lag der iPhone Verkaufspreis bei 550€. Nach der Vorstellung des neuen iPhone 5 ist der Preis Ende Oktober um 100€ gesunken und dann sind wir immerhin bei ungefähr 450€, statt 390€. Der monatliche Preis liegt dann zwar immer noch über 20€, ist aber nicht so weit vom iPad entfernt. Und in den nächsten Monaten wird sich der Preis halten, so dass man im April auf einen ähnlichen Wert kommt.

Trotzdem, je populärer die Apple Geräte werden, desto kleiner wird der schöne Effekt des Werterhalts, weil das Angebot an gebrauchten Geräten größer wird. Auch schade.