---
layout: post
title: "Wetten Sack...?"
description: "Kurzes Review zur ersten und zweiten Wetten dass...? Sendung."
date: 2012-11-08 
category: medien
tags: [Fernsehen]
published: true
comments: false
share: true
---

Wow. Einfach nur wow. Mein Kommentar zur zweiten und ersten Wetten dass...?-Sendung mit Markus Lanz. Ich hatte am Wochenende zuerst die zweite gesehen und dann eine Aufzeichnung der ersten. Einiges hat mir gut gefallen und einiges weniger gut. Beginnen wir mit dem Guten.

## Gutes Konzept

Die neue Idee ist ja, dass jeder Stargast gleichzeitig ein Wettpate ist. Er stellt also die Wette des Kandidaten vor, drückt die Daumen und bei den anderen Wetten wettet er mit um Geld für seinen Schützling zu gewinnen. Dieses Konzept ist in der Tat näher an den Wetten dran, als es bei Gottschalk der Fall war. Dort kamen die Stars, es wurde gesmalltalkt, ein Auftritt gezeigt, dann mal gewettet, die Kandidaten verschwanden wieder hinter der Bühne und der Spaß ging in einer 4-stündigen Endlosschleife so weiter (30 - 50 Minuten pro Durchgang). 

Besonders in der zweiten Sendung hat man gemerkt, dass die Wettpaten wirklich mitfiebern und voll dabei sind. Dass sich dabei gewisse Längen einschleichen und der eine oder andere Gast etwas langweilt, ist eben so.      
Die Kommentare von Tom Hanks und Halle Berry würde ich nicht zu streng beurteilen. Sie kennen solche Art Sendung eben einfach nicht und waren über die Länge und den Humor verwundert. Der [DWDL](http://www.dwdl.de/meinungen/38258/hanks_wetten_dass_und_schlechter_journalismus/) schreibt einige interessante Dinge dazu.

## Holprige Umsetzung

Wie gut das Konzept in der Realität umgesetzt wurde, ist eine andere Geschichte. Ein Problem ist Lanz, der mir persönlich zu verkrampft und gezwungen gut gelaunt auftritt. Nicht umsonst schmunzelte man nach der ersten Sendung über seine miesen Witze und nach der letzten über die *Wow*s.      
Seine Fragen fand ich meist auch nicht passend. Ich wüsste gerne welche Promiklatsch-Seiten seine Redaktion durchforstet haben muss um auf solch alberne Fragen, ob denn Halle Berry auf den Geruch von gebratenem Fleisch steht, zu kommen. Zu seinem Glück muss man Lanz zugute halten, dass er fast nie unhöflich wirkt. Im Gegensatz zu Stefan Raab beispielsweise.

Ein weiterer Kritikpunkt ist die Technik. Bei den amerikanischen Gästen gab es wohl Probleme mit den Dolmetschern und den Hörgeräten. So etwas darf einfach nicht sein. Auch in der ersten Sendung gab es viele Tonfehler.
Generell ist der Ton nicht gut gemischt. Teils sind die falschen Leute laut gestellt und die Redenden hört man nicht. Da muss der ZDF noch dran arbeiten.

Aber was ich am schlimmsten fand: Die Vermeidung von Englisch. Bloß nichts auf Englisch sagen! Oh Gott!     
Damit werden die Gäste aber ausgegrenzt und es entsteht eine unnötige Differenz. Vor allem bei den peinlichen Fragen kommen einem die endlosen Minuten, bis der Dolmetscher alles übersetzt und der Gast antworten kann, ewig vor. 
Flüssiger wäre die Sendung mit den ausländischen Gästen, wenn man sich in deren Sprache oder zumindest auf Englisch unterhalten würde. So unglaublich oft kam das nun auch nicht vor und dafür kann der Übersetzer ja die Frage und Antwort übersetzen. Und für die Zuschauer am Fernseher direkt mit Untertitel.

## Weitermachen

Das ZDF sollte an der Technik arbeiten, die Gäste besser in die Sendung eingebunden werden und Lanz... ja, ach Lanz. Keine Ahnung. Bessere Fragen, mehr Ruhe.      
Aber an und für sich ist die Sendung nicht abgrundtief schlecht und das fanden viele Zuschauer ja auch.     

Und Tom Hanks war super und hatte viel Humor. 