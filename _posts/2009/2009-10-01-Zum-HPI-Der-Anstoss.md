---
layout: post
title: "Zum HPI - Der Anstoss"
description: "Die kurze Geschichte darüber wie ich auf das HPI aufmerksam geworden bin."
date: 2009-10-01
tags: [HPI, Studium, Persönlich] 
published: true
category: blog
comments: false
share: true
---

Es gibt sicherlich viele Möglichkeiten auf das Hasso-Plattner-Institut aufmerksam zu werden. Zeitungsartikel, Internet, Bekannte oder ein Blog zum Beispiel. Es gibt die unterschiedlichsten Geschichten von anderen Studenten.  
Ich bin durch meinen Zivildienst auf das HPI gestoßen. 


Das ist in gewisser Weise ironisch, denn dieser Dienst trug dazu bei, dass eine langjährige Beziehung in die Brüche ging, mir aber dadurch gleichzeitig auch die Möglichkeit eröffnet wurde, ein Studium in einer Ex-Freundin-freien Stadt zu beginnen bzw. erst einmal zu suchen. Ursprünglich sollte es Medieninformatik in Dresden werden, das war seit zwei Jahren die klare Linie. Doch es kam kurzfristig anders.

Zu den Umständen: Als Zivi in einer Psychiatrie hat man relativ nahen Kontakt zu den Patienten. Man muss sich mit einigen beschäftigen, anderen helfen, und man ist irgendwie das Mädchen für alles. Ich war in einer &quot;offenen Psychiatrie&quot;, dort waren die Patienten freiwillig zur Behandlung. Demnach hatten einige von ihnen &quot;kleinere&quot; Probleme, waren ansonsten aber völlig normal und ich verstand mich gut mit ihnen. So auch mit Frau K.  
Sie war mittleren Alters, sehr intelligent, aber auch schizophren. Mit Medikamenten erreicht man viel und nach einigen Wochen ihrer Medikamenteneinnahme, war sie richtig umgänglich und wir bastelten u.a. an einen neuen Therapieplan für die Station. Sie merkte auch irgendwann, dass ich mich mit Computern gut auskannte und fragte mich auch nach meinem Studiumwunsch.

*Also Informatik wollen Sie studieren, Zivi Ingo?* 

-*Ja. Sozusagen.* 

*Kennen Sie das Hasso-Plattner-Institut in Potsdam, dort könnten Sie auch Informatik studieren.*

-*Was für ein Institut? Davon habe ich noch nie gehört.*

*Na das HPI, erkundigen Sie sich ruhig mal, Zivi Ingo.*

Natürlich tat ich das, fand das HPI und war beeindruckt. Aber auch eingeschüchtert, schließlich ist das Institut eine feine Adresse. Würden die jemanden wie mich überhaupt aufnehmen, bin ich ein &quot;begabter junger Mensch&quot;?  
Dieser Anstoß traf mich ein knappes halbes Jahr vor Ablauf der Bewerbungsfrist, noch lange bevor ich mir die Sache ernsthaft durch den Kopf gingen ließ. Dresden und die damals noch nicht Ex-Freundin waren der Plan. Wir wollten zusammen in Dresden wohnen und studieren. Wie sollte ich ihr da Potsdam erklären?

Aus Spaß meldete ich mich trotzdem als Studiuminteressierter auf der Website an und vergaß danach wieder das Institut.