---
layout: post
title: "Zum HPI - Die Email"
description: "Der vielleicht wichtigste Moment, das Absenden der Bewerbung."
date: 2009-10-14
tags: [HPI, Studium, Persönlich] 
published: true
category: blog
comments: false
share: true
---

Ich war zwar als Studiuminteressierter beim HPI eingetragen, dachte jedoch nicht weiter an eine Bewerbung. Es wäre ja kein Problem gewesen die Bewerbungsunterlagen schon fertig zu machen und später abzuschicken. Doch so optimistisch war ich nicht und es gab ja auch noch den zu bewältigenden Zivildienst und das anstehende Medieninformatik-Studium in Dresden.  

Einige Monate später, nachdem ich dem HPI meine E-Mail-Adresse hinterlassen hatte, bekam ich Anfang Mai eine Email mit der Aufforderung mich doch am HPI zu bewerben. Die Frist lief noch bis zum 31. Mai. Ich grübelte wieder nach.    
  
Man wollte natürlich ein Abitur-Zeugnis von mir, die Gesamtnote ging aber nur 51% in die Bewertung der Bewerbung ein. Daneben waren Mathe, Deutsch, Informatik und Englisch sehr wichtig. Für die letzten Prozentpunkte dieses Bewerbungsverfahren musste man ein schriftliches Motivationschreiben verfassen. Wieso das HPI und wieso gerade ich? So wirklich sichere Antworten hatte ich nicht auf diese Fragen, aber Schreiben fällt mir nicht so schwer und außerdem hatte ich genau herausgearbeitet, was man von mir wissen möchte und was ich am optimalsten darauf antworten könnte.
  
Mein Bruder (damals 8. Klasse) las mein Motivationschreiben und sagte nur *&quot;Ich würde dich nehmen!&quot;*.  
Braucht man mehr Zuversicht? Ich sammelte die restlichen Unterlagen zusammen und schickte sie ab. *Just for Fun*, ohne große Hoffnungen. Wie auch, bei einem knappen 1er-Abitur und nur mittelmäßigen Noten in Mathe? Ich und begabt? Gut genug und qualifiziert für das HPI? Vermutlich nicht.