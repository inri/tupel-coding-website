---
layout: post
title: "Zum HPI - Der Brief"
description: "Wie ich die Zusage vom HPI bekam."
date: 2009-10-18
tags: [HPI, Studium, Persönlich] 
published: true
category: blog
comments: false
share: true
---

Ende Mai war Bewerbungsschluss beim HPI und Mitte August sollte vom HPI und all den anderen Unis die Zusagen oder Absagen verschickt werden. Darauf stellte man sich ein und wartete gespannt. 


Ich schrieb mich in Dresden ein (NC gab es dort für Medieninformatik nicht), und bewarb mich bei einigen Unis für Informatik, Lehramt Deutsch und in Berlin für Skandinavistik. Wieso, weshalb und warum ist jetzt egal.

Ich jobbte den Juli über für meinen Vater, erstellte eine Präsentation für sein Programm. Immer so gegen Mittag ging ich mal vor die Tür und holte Post. Am 29. Juli war ein Brief vom Hasso-Plattner-Institut dabei. Mein Herz schlug schneller und meine Gedanken rasten. In erster Linie war ich gespannt und fieberte die Mitte des Augusts entgegen und nun, zwei Wochen eher, kam dieser Brief? Enttäuschung machte sich bei mir breit, eine Absage. Was kann es sonst sein?    

Ich rupfte den Brief auf und las die erste Zeile:

*&quot;...ich gratuliere Ihnen sehr herzlich zur Aufnahme in unseren...&quot;*

**Oh.**    
**Ohje!**     
Es war tatsächlich eine Zusage. Ich kann nun Student am HPI werden. Sofort rief ich meine Eltern an und berichtete. Mein Vater fand die richtigen Worte. *Ach du Scheiße.*

Doch damit war es nicht getan. Ich hatte nicht mit einer Zusage gerechnet und Dresden war auch kein Thema mehr, schließlich wartete keine Freundin dort auf mich. Mein Favorit war mittlerweile Skandinavistik in Berlin gewesen. Was sollte ich nun machen? Berlin hat nicht gerade den Ruf seine Zu- und Absagen zeitig zu verschicken und so wartete ich auch eine ganze Weile darauf.    
Einige Tage nahm ich mir zum Überlegen Zeit und dann kam ich zum einzigen und besten Schluss: Ich schickte dem HPI auch eine Zusage.

Nach und nach trudelten für alle anderen Bewerbungen die Zusagen ein. Teilweise ziemlich spät. Das zeigt sehr schön, dass das Procedere unserer Unis ziemlich schlecht ist. Man verschickt haufenweise Bewerbungen und nimmt dann doch nur eine an, klar. So bekommen die Unis bis zur ersten Frist viele Absagen und schicken dann in der zweiten Runde ihre Zusagen raus und so geht das über einige Wochen. Manch einer bekommt sein Studienplatz doch noch und andere warten vergeblich. Eine zentrale Bewerbung wäre besser, aber das ist wohl zu kompliziert. 